#include "stdafx.h"
#include "StateMachine.h"
#include "Defines.h"

HRESULT CStateMachine::Ready_StateMachine(CIFSMUsable* pOwner)
{
	if (pOwner == nullptr)
		return E_FAIL;

	m_pOwner = pOwner;
	m_pCurState = nullptr;

	return NOERROR;
}

short CStateMachine::Update()
{
	if (m_pCurState == nullptr || m_pOwner == nullptr)
		return -1;

	m_pCurState->Update(m_pOwner);

	return 0;
}

HRESULT CStateMachine::ChangeState(CState* pState)
{
	if (pState == nullptr || m_pOwner == nullptr || m_pCurState == nullptr)
		return E_FAIL;

	m_pPrevState = m_pCurState;

	m_pNextState = pState;

	m_pCurState->Exit(m_pOwner);

	m_pCurState = pState;

	m_pCurState->Enter(m_pOwner);

	return NOERROR;
}

HRESULT CStateMachine::SetCurrentState(CState* pState)
{
	if (pState == nullptr)
		return E_FAIL;

	m_pCurState = pState;

	return NOERROR;
}

CStateMachine* CStateMachine::Create(CIFSMUsable* pOwner)
{
	CStateMachine* pInstance = new CStateMachine();

	if (FAILED(pInstance->Ready_StateMachine(pOwner)))
	{
		cout<<"Fail To Create StateMachine"<<endl;
		return nullptr;
	}

	return pInstance;
}

void CStateMachine::Free()
{
}
