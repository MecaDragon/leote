#include "stdafx.h"
#include "GameObjectState.h"

//idle
IMPLEMENT_STATE(Idle)
void Idle::Enter(CIFSMUsable* pOwner) { pOwner->Idle_Enter(); }
void Idle::Update(CIFSMUsable* pOwner) { pOwner->Idle_Update(); }
void Idle::Exit(CIFSMUsable* pOwner) { pOwner->Idle_Exit(); }

IMPLEMENT_STATE(Eat)
void Eat::Enter(CIFSMUsable* pOwner) { pOwner->Eat_Enter(); }
void Eat::Update(CIFSMUsable* pOwner) { pOwner->Eat_Update(); }
void Eat::Exit(CIFSMUsable* pOwner) { pOwner->Eat_Exit(); }

IMPLEMENT_STATE(Nod)
void Nod::Enter(CIFSMUsable* pOwner) { pOwner->Nod_Enter(); }
void Nod::Update(CIFSMUsable* pOwner) { pOwner->Nod_Update(); }
void Nod::Exit(CIFSMUsable* pOwner) { pOwner->Nod_Exit(); }

//run
IMPLEMENT_STATE(Run)
void Run::Enter(CIFSMUsable* pOwner) { pOwner->Run_Enter(); }
void Run::Update(CIFSMUsable* pOwner) { pOwner->Run_Update(); }
void Run::Exit(CIFSMUsable* pOwner) { pOwner->Run_Exit(); }

//escape
IMPLEMENT_STATE(EscapeFence)
void EscapeFence::Enter(CIFSMUsable* pOwner) { pOwner->EscapeFence_Enter(); }
void EscapeFence::Update(CIFSMUsable* pOwner) { pOwner->EscapeFence_Update(); }
void EscapeFence::Exit(CIFSMUsable* pOwner) { pOwner->EscapeFence_Exit(); }