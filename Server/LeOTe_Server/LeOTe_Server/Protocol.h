#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

constexpr int MAX_ID_LEN = 50;

#define SERVER_PORT 9000

constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUF_SIZE = 1024;
constexpr auto MAX_NAME_LEN = 50;
constexpr auto MAX_USER = 10000;

constexpr auto MAX_CHICKEN = 20;
constexpr auto CHICKENSPEED = 6.f;

//Packet Type

#define S_LOGO			0
#define S_BOARDMAP		1
#define S_FALLINGMAP	2
#define S_GGOGGOMAP		3
#define S_BULLFIGHTMAP	4
#define S_ONEMINDMAP	5
#define S_PRIZEMAP		6

//C2S == Client To Server
#define C2S_LOGIN			1
#define C2S_WAITROOMENTER	2
#define C2S_MATCHINGSTART	3
#define C2S_ROOMOUT			4
#define C2S_MOVE			5
#define C2S_LEAVEPLAYER		6
#define C2S_SCENECHANGE		7

//board map
#define C2S_BOARDMAPREADY	8
#define C2S_ORDER			9
#define C2S_JUMPDICE		10
#define C2S_DICEMOVEEND		11
#define C2S_CURRENTORDER	12
#define C2S_DICEENTER		13
#define C2S_COINEVENT		14
#define C2S_BUTTONEVENT		15
#define C2S_DICENUMDOWN		16
#define C2S_PLAYERSCOL		17

//falling map
#define C2S_FALLINGMAPREADY 18 
#define C2S_ANIMATION		19 //애니메이션 idle로 전환
#define C2S_DELETEBLOCK		20
#define C2S_PLAYERDEAD		21

//ggoggomap 
#define C2S_MINIGAMELOADING 22

//onemind
#define C2S_ONEMINDDIR		23 //플레이어 direction
//#define C2S_ONEMINDKEY		24 //플레이어의 키입력
#define C2S_ONEMINDMOVE		24 //플레이어의 이동 및 점프
#define C2S_ONEMINDLIGHT	25 //불켰다 키기
#define C2S_ONEMINDANIM		26 //한 마음맵 애니메이션

#define C2S_SHOWMAP			27 //맵 보는 중

#define C2S_STARTTIMER		28 //시작 초세기 타이머 시작 요청
#define C2S_ENDTIMER		29 //종료 초세기 타이머 

#define C2S_MATCHTIE		30 //동점 주사위 요청

#define C2S_EVENT			31 //단추, 상점 도착
#define C2S_EVENTSELECT		32 //단추 구입, 혹은 아이템 구입 여부

#define C2S_ITEMUSE			33 //아이템 사용

#define C2S_TESTTIERANK		34 //보드맵에서 동점자띄우기위한 치트 패킷

#define C2S_SPECIALEVENT	35 //보드맵 이벤트, 함정 발판 (상점, 단추 제외)
#define C2S_TELEPORT		36 //이벤트 발판으로 인한 텔레포트
/////////////////////////

#define S2C_SCENECHANGE 0

//S2C == Server To Client
#define S2C_LOGIN_OK	1
#define S2C_LOGIN_FAIL	2 
#define S2C_ENTERPLAYER	3
#define S2C_LEAVEPLAYER 4
#define S2C_ROOMCREATE	5
#define S2C_ROOMENTER	6
#define S2C_ROOMOUT		7
#define S2C_MOVE		8

//board map
#define S2C_BOARDMAPREADY	9
#define S2C_ORDER			10
#define S2C_JUMPDICE		11
#define S2C_DICEMOVEEND		12
#define S2C_CURRENTORDER	13
#define S2C_DICEENTER		14
#define S2C_COINEVENT		15
#define S2C_BUTTONEVENT		16
#define S2C_DICENUMDOWN		17
#define S2C_PLAYERCOL		18

//falling map
#define S2C_FALLINGMAPREADY 19
#define S2C_ANIMATION		20 //애니메이션 idle로 전환
#define S2C_DELETEBLOCK		21
#define S2C_PLAYERDEAD		22
#define S2C_MINIGAMEEND		23

#define BLOCK_BLUE			0
#define	BLOCK_RED			1
#define	BLOCK_YELLOW		2

//ggoggomap 
#define S2C_CHICKENINIT		24 // 치킨 초기 위치, 방향, 상태
#define S2C_CHICKEN			25 // 치킨 방향 및 위치, 상태
#define S2C_CHICKENSTATE	26 // 상태만 바뀌었을 때

#define S2C_TIMESECONDS		27 // 시간 초 보내기
#define S2C_MINIGAMELOADING 28 // 미니게임 로딩 완료 보내기

//bullfight
#define S2C_POTION			29 // 물약 떨구기 
#define S2C_BIGGER			30 // 몸 커지기 
#define S2C_SMALLER			31 // 몸 작아지기 
#define S2C_PUNCH			32 // 플레이어 날아가기

//onemind
#define S2C_ROLL			33 //각자의 역할
#define S2C_ONEMINDDIR		34 //플레이어 direction
#define S2C_ONEMINDMOVE		35 //한 마음맵 이동, 점프
#define S2C_ONEMINDLIGHT	36 //한 마음맵 불껏다키기
#define S2C_BATTERY			37 //한 마음맵 배터리 보내기
#define S2C_ONEMINDANIM		38 //한 마음맵 애니메이션

#define S2C_ONEMINDRESPAWN	39 //한 마음맵 리스폰
#define S2C_ONEMINDSAVE		40 //한 마음맵 세이브포인트(눈사람충돌)

//match
#define S2C_MATCHING		41 //매칭 성공

//rank
#define S2C_BOARDMAPRANK	42 //단추, 코인에따른 순위

//map
#define S2C_SHOWMAP			43 //맵 보는 중

#define S2C_STARTTIMER		44 //시작 후 3초 보내기
#define S2C_ENDTIMER		45 //종료 후 3초 보내기

#define S2C_TIEPLAYER		45 //동점자 여부 보내기
#define S2C_MATCHTIE		46 //동점 주사위 보내기

#define S2C_MINIGAMENDCOIN	47 //미니게임 종료 후 코인 주기

#define S2C_EVENT			48 //단추, 상점 도착
#define S2C_EVENTSELECT		49 //단추, 아이템 구입 여부

#define S2C_ITEMUSE			50 //아이템 사용

#define S2C_TESTTIERANK		51 //보드맵에서 동점자띄우기위한 치트 패킷

#define S2C_SPECIALEVENT	52 //함정, 이벤트 발판(단추,상점X)

#define S2C_TELEPORT		53 //이벤트 발판으로 인한 텔레포트
//Packets
#pragma pack(push, 1)
struct packet_login {
	unsigned char	size;
	char	type;
	int		id;
	char	name[MAX_ID_LEN];
};

struct packet_login_result {
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_enter {
	unsigned char	size;
	char	type;
	int		id;
	char	name[MAX_ID_LEN];
	char	skinid;
};
struct packet_leave {
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_rank {
	unsigned char	size;
	char	type;
	unsigned char matchtie; //0-동점자 없음 1-1등2등 동점 2- 2등3등 동점 3- 1등 2등 3등 동점
	int		id1;
	int		id2;
	int		id3;
};

struct packet_roomcreate {
	unsigned char	size;
	char	type;
	int		id;
	int		roomid;
};

struct packet_roomenter {
	unsigned char	size;
	char	type;
	int		id;
	int		roomid;
};

struct packet_playerdead {
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_move {
	unsigned char	size;
	char	type;
	int		id;
	float	mx;		//이동후 좌표
	float	my;		//이동후 좌표
	float	mz;		//이동후 좌표
	float	lookx;		//방향
	float	looky;		//방향
	float	lookz;		//방향
	float   TimeDelta;
};

struct packet_boardmap_ready { //보드맵 로딩 완료
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_order {
	unsigned char	size;
	char	type;
	int		id;
	int		order;	//순서
	float	 x;
	float	 y;
	float	 z;
};

struct packet_jumpdice {
	unsigned char	size;
	char	type;
	int		id;
	int		dicenum;
};

struct packet_dicemoveend {
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_currentorder {
	unsigned char	size;
	char	type;
	int		id;
	int		currentorder;
	int		currentplatform;
};

struct packet_diceenter {
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_coin {
	unsigned char	size;
	char	type;
	int		id;
	bool	add; //증가여부
	int		num; //갯수
};

struct packet_button {
	unsigned char	size;
	char	type;
	int		id;
	bool	add; //증가여부
	int		num; //갯수
};

struct packet_dicenumdown {
	unsigned char	size;
	char	type;
	int		id;
	int		num; //주사위 숫자
};

struct packet_playercollision {
	unsigned char	size;
	char	type;
	int		id;
	float	shiftx;
	float	shifty;
	float	shiftz;
};

struct packet_animation {
	unsigned char	size;
	char	type;
	int		id;
	int		anim; //
};

struct packet_scenechange {
	unsigned char	size;
	char	type;
	int		id;
	int		map;
	float	x; //초기위치
	float	y; //초기위치
	float	z; //초기위치
	float   lookx; //초기 방향
	float   looky; //초기 방향
	float   lookz; //초기 방향
};

struct packet_deleteblock {
	unsigned char	size;
	char	type;
	int		blockid;
	char	blockcolor;
};

struct packet_minigameend {
	unsigned char	size;
	char	type;
	int		id;//승리한 플레이어 인덱스, -2: 협동게임 패배, -3: 협동게임  승리
};

constexpr auto C_IDLE = 0;
constexpr auto C_EAT = 1;
constexpr auto C_NOD = 2;
constexpr auto C_RUN = 3;
constexpr auto C_ESCAPE = 4;

struct packet_Initchicken {
	unsigned char	size;
	char	type;
	char	c_id; //chicken id
	char	state; //C_IDlE....
	float	angle; //첫 시작 각도
	float	x, y, z; //첫위치
};

struct packet_chicken {
	unsigned char	size;
	char	type;
	char	c_id; //chicken id
	char	state; //C_IDlE....
	float	x, y, z; //위치
	float	lookx, looky, lookz; //look
};

struct packet_chickenstate {
	unsigned char	size;
	char	type;
	char	c_id; //chicken id
	char	state; //C_IDlE....
};

struct packet_timeseconds { //이 패킷이 오면 1초가 지났다는 뜻
	unsigned char	size;
	char	type;
	unsigned short	seconds;
};

struct packet_loadingsuccess {
	unsigned char	size;
	char	type;
	short	scene;
};

struct packet_potion { //물약, 위치
	unsigned char	size;
	char	type;
	float x;
	float y;
	float z;
};

struct packet_scale { //플레이어 몸집 
	unsigned char	size;
	char	type;
	int		id;
};

struct packet_punch { //플레이어 충돌 후 날아가기
	unsigned char	size;
	char	type;
	int		id;
};

#define ROLL_MOVE	0
#define ROLL_LIGHT	1
#define ROLL_DIR	2

struct packet_roll { //각자의 역할 보내기
	unsigned char	size;
	char	type;
	int		id;
	int		roll;
};

struct packet_onemindKey { //클라에서 무슨 키 눌렀는 지 
	unsigned char	size;
	char	type;
	int		id;
	bool	w;
	bool	a;
	bool	s;
	bool	d;
	bool	space;
	float	Delta;  //속도 * 시간 * 가속도
};

struct packet_onemindmove {
	unsigned char	size;
	char	type;
	float	mx;		//이동후 좌표
	float	my;		//이동후 좌표 
	float	mz;		//이동후 좌표
};

struct packet_onemindlook { //클라에서 look은 그대로 보내줌 
	unsigned char	size;
	char	type;
	float	lookx;		//이동후 좌표
	float	looky;		//이동후 좌표
	float	lookz;		//이동후 좌표
};

struct packet_onemindlight {
	unsigned char	size;
	char	type;
	bool	light;
};

struct packet_onemindbattery {
	unsigned char size;
	char type;
	char hp; //배터리 체력, 8초
};

struct packet_matching {
	unsigned char size;
	char type;
	int	id;
	char skinid;
};

struct packet_respawn {
	unsigned char size;
	char	type;
	float	x;
	float	y;
	float	z;
};

struct packet_savepoint {
	unsigned char size;
	char	type;
	char	savepoint;
};

struct packet_showmap {
	unsigned char size;
	char	type;
	bool    show; //false 안봄, true 봄
};

struct packet_starttimeask { //클라로부터 시작 타이머 시작 요청
	unsigned char size;
	char	type;
	int		id;
};

struct packet_starttimer {
	unsigned char size;
	char	type;
	char	time; //3 2 1 
};

struct packet_istied { //동점자 여부 
	unsigned char size;
	char	type;
	short	tied; // 1 동점자 있음 2 동점자 없음
	int		id1;//동점자 1
	int		id2;//동점자 2
};

struct packet_matchtie { //동점 시 주사위 돌리기
	unsigned char size;
	char	type;
	int		id;
	int		dicenum;
};

#define E_BUTTON	0
#define E_SHOP		1

struct packet_event { //단추, 상점 도착
	unsigned char size;
	char	type;
	int		id;
	char	E_event;
};

enum ITEM {
	ITEM_MINIDICE, ITEM_BUTTON, ITEM_PLUS3,ITEM_PLUS5, ITEM_END
};

struct packet_eventselect { //단추, 아이템 구입 여부
	unsigned char size;
	char	type;
	int		id;
	bool	buy; //구입 여부
	char	item; //아이템
	short	coin; //C2S - 재화가격 S2C - 현재코인수
	short	button; // S2C - 현재 단추 수
};

struct packet_itemuse {
	unsigned char size;
	char	type;
	int		id;
	char	item; //사용한 아이템
	char	invenindex; //인벤토리인덱스
	char	dice; //미니다이스 시 미니다이스 숫자
};

struct packet_testtie {
	unsigned char size;
	char	type;
	int		id;
	short	coin; //서버에서 1등 코인 가겨 보냄
	short	button;  //서버에서 1등 단추 갯수 보냄
};

struct packet_specialevent {
	unsigned char size;
	char	type;
	int		id;
	char	spacialevent; //발생한 이벤트
};

struct packet_teleport {
	unsigned char size;
	char	type;
	int		id;
	float	x;
	float	y;
	float	z;
};
#pragma pack (pop)


#endif // !__PROTOCOL_