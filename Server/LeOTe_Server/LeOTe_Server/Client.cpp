#include "stdafx.h"
#include "Client.h"

Client::Client()
{
	id = -1;
	connected = false;
	m_prev_recv = 0;				//지금까지 받은 양(데이터크기)
	x = -9.f;
	y = 13.6f;
	z = -80.5f;

	coin = 10;
	button = 0;
	roomindex = -1;

	map = MAP_END;

	coin = 10;
	button = 0;
	rank = 1;
}

Client::~Client()
{
}

void Client::Init_PlayerInfo()
{
	minigamedead = false;
	coin = 20;
	button = 0;
	rank = 1;

	x = -9.f;
	y = 13.6f;
	z = -80.5f;
}

void Client::SetID(const int& user_id)
{
	id = user_id;
}

void Client::SetConnect(const bool& c)
{
	connected = c;
}

void Client::SetName(const char n[])
{
	strcpy_s(name, n);
	name[MAX_NAME_LEN] = NULL;
}

void Client::SetPrevRecvd(const int& prevrecved)
{
	m_prev_recv = prevrecved;
}

void Client::SetStatus(const PLAYERSTATUS& s)
{
	status = s;
}

void Client::SetRoomIndex(const int& index)
{
	roomindex = index;
}

void Client::SetPosition(const float& px, const float& py, const float& pz)
{
	x = px;
	y = py;
	z = pz;
}

void Client::SetDirection(const float& px, const float& py, const float& pz)
{
	x = px;
	y = py;
	z = pz;
}

void Client::SetPlayerMap(const PLAYERINMAP& m)
{
	map = m;
}

void Client::SetLook(float x, float y, float z)
{
	dx = x;
	dy = y;
	dz = z;
}

void Client::SetButton(int num, bool add)
{
	if (add)
		button += num;
	else
	{
		button -= num;
		if (button < 0)
			button = 0;
	}
}

void Client::SetCoin(int num, bool add)
{
	if (add)
		coin += num;
	else
	{
		coin -= num;
		if (coin < 0)
			coin = 0;
	}
}

void Client::SetTestCoin(int num)
{
	coin = num;
}

void Client::SetTestButton(int num)
{
	button = num;
}

void Client::SetMiniGameDead(bool d)
{
	minigamedead = d;
}

void Client::Set_Skinid(char sid)
{
	skinid = sid;
}

const int& Client::GetID()
{
	return id;
}

const bool& Client::GetConnected()
{
	return connected;
}

const char* Client::GetName()
{
	return name;
}

const int& Client::GetPrevRecved()
{
	return m_prev_recv;
}

const PLAYERSTATUS& Client::GetStatus()
{
	return status;
}

const int Client::GetRoomIndex()
{
	return roomindex;
}

const void Client::GetPosition(float& px, float& py, float& pz)
{
	px = x;
	py = y;
	pz = z;
}

const void Client::GetDirection(float& px, float& py, float& pz)
{
	px = x;
	py = y;
	pz = z;
}

const PLAYERINMAP& Client::GetPlayerMap()
{
	return map;
}

const void Client::GetLook(float& x, float& y, float& z)
{
	x = dx;
	y = dy;
	z = dz;
}

const int& Client::GetCoin()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return coin;
}

const int& Client::GetButton()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return button;
}

const int& Client::GetRank()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return rank;
}

const bool& Client::GetMiniGameDead()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return minigamedead;
}

const char& Client::Get_Skinid()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return skinid;
}

