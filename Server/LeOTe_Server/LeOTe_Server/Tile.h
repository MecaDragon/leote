#pragma once

#define TYOFFSET 0.5f;

#define TSCALEX 1.5f
#define TSCALEY 0.8f
#define TSCALEZ 1.5f

class CTile
{
public:
	explicit CTile();
	CTile(const CTile& tile);
	virtual ~CTile();

public:
	const void GetPosition(float& px, float& py, float& pz) { px = x; py = y; pz = z; }
	void SetPostion(const float& px, const float& py, const float& pz) { x = px; y = py; z = pz; }

	void SetTileIndex(const short& i) { tileindex = i; }
	const short& GetTileIndex() { return tileindex; }

	void SetTileColor(const int& c) { tilecolor = c; }
	const int& GetTileColor() { return tilecolor; }

	const bool& isDeleted() { return deleted; }
	void SetDeleted(const bool& b) { deleted = b; }
	mutex m_lock;

private:
	short tileindex = 200;
	int tilecolor = -1;
	bool deleted = false;
	float x, y, z;

};

