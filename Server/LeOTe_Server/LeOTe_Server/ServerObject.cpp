#include "stdafx.h"
#include "ServerObject.h"
#include "Client.h"
#include "Room.h"
#include "Tile.h"
#include "Chicken.h"
#include <algorithm>
#include <time.h>

SOCKET CServer::listen_socket;		//listen 소켓
HANDLE CServer::Iocp;				//iocp 핸들
map<int, Client> CServer::clients;	//클라이언트 객체들
map<int, CRoom> CServer::roomsector;//대기실 별 클라이언트 객체들

EX_OVER CServer::accept_overlapped; //Accept용

priority_queue<timer> CServer::timer_queue;
mutex CServer::timer_lock;
thread  CServer::time_thread;

//int CServer::roomnum = 0;

CServer* CServer::m_pInstance = nullptr;
bool CServer::waitroom = true; //테스트용 변수, 대기실 사용 여부

priority_queue<waiting_player> CServer::waiting_player_queue;//매칭
mutex CServer::matching_lock;

priority_queue<timer> CServer::starttimer_queue; //동시시작 3초 세는 용도 타이머
mutex CServer::starttimer_lock;

void display_error(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	cout << msg;
	wcout << L" 에러 " << lpMsgBuf << endl;
	LocalFree(lpMsgBuf);
}

void do_accept(SOCKET s_socket, EX_OVER* a_over)
{
	SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	memset(&a_over->m_over, 0, sizeof(a_over->m_over));
	DWORD num_byte;
	int addr_size = sizeof(SOCKADDR_IN) + 16;
	a_over->m_csocket = c_socket;
	AcceptEx(s_socket, c_socket, a_over->m_netbuf, 0, addr_size, addr_size, &num_byte, &a_over->m_over);
}

void CServer::do_recv(int p_id)
{
	Client& pl = clients[p_id];
	EX_OVER& r_over = pl.m_recv_over;
	//r_over.m_op = OP_RECV;
	memset(&r_over.m_over, 0, sizeof(r_over.m_over));
	r_over.m_wsabuf[0].buf = reinterpret_cast<CHAR*>(r_over.m_netbuf) + pl.m_prev_recv;
	r_over.m_wsabuf[0].len = MAX_BUF_SIZE - pl.m_prev_recv;
	DWORD r_flag = 0;
	WSARecv(pl.socket, r_over.m_wsabuf, 1, 0, &r_flag, &r_over.m_over, 0);
}

CServer::CServer()
{
}

CServer::~CServer()
{
}

void CServer::Initialize_clients()
{
	//미니게임 씬 별로 클라이언트 초기화
}

void CServer::Server_Initialize()
{
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);

	listen_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN server_addr;
	memset(&server_addr, 9, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(SERVER_PORT);
	server_addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	::bind(listen_socket, reinterpret_cast<sockaddr*>(&server_addr), sizeof(server_addr));

	listen(listen_socket, SOMAXCONN);

	//IOCP 핸들 생성
	Iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);

	CreateIoCompletionPort(reinterpret_cast<HANDLE>(listen_socket), Iocp, 999, 0);

	EX_OVER a_over;
	a_over.m_op = OP_ACCEPT;
	do_accept(listen_socket, &a_over);

	vector<thread> worker_threads;

	for (int i = 0; i < 4; ++i)
	{
		worker_threads.emplace_back(Server_Update);
	}

	thread match = thread(do_matching);
	match.join();

	for (auto& th : worker_threads)
		th.join();
}

void CServer::Server_Update()
{
	while (true)
	{
		DWORD num_byte;
		ULONG_PTR i_key;
		WSAOVERLAPPED* over;

		//blocking, 클라에서 데이버 안보내면 다음 줄로 진행X
		GetQueuedCompletionStatus(Iocp, &num_byte, &i_key, &over, INFINITE);
		int key = static_cast<int>(i_key);
		EX_OVER* ex_over = reinterpret_cast<EX_OVER*>(over);

		switch (ex_over->m_op)
		{
		case OP_RECV:
		{
			unsigned char* ps = ex_over->m_netbuf;
			int remain_data = num_byte + clients[key].m_prev_recv;
			while (remain_data > 0) {
				int packet_size = ps[0];
				if (packet_size > remain_data) break;
				Process_packet(key, ps);
				remain_data -= packet_size;
				ps += packet_size;
			}
			if (remain_data > 0)
				memcpy(ex_over->m_netbuf, ps, remain_data);
			clients[key].m_prev_recv = remain_data;
			do_recv(key);
		}
		break;
		case OP_SEND:
		{
			if (num_byte != ex_over->m_wsabuf[0].len)
				Disconnect(key);
			delete ex_over;
			break;
		}
		break;
		case OP_CHICKEN:
		{

		}
		break;
		case OP_ACCEPT:
		{
			SOCKET c_socket = ex_over->m_csocket;
			int id = 0;

			for (auto& client : clients)
			{
				lock_guard<mutex>(client.second.c_lock);
				if (false == client.second.GetConnected())
					break;
				++id;

			}
			cout << id << "번째 클라이언트 입장" << endl;

			if (MAX_USER == id) {
				cout << "Max User Limit Error" << endl;
				closesocket(c_socket);
			}
			else {
				clients[id].c_lock.lock();
				clients[id].SetID(id);
				clients[id].m_prev_recv = 0;
				clients[id].SetConnect(true);
				clients[id].m_recv_over.m_op = OP_RECV;
				clients[id].socket = c_socket;
				clients[id].SetStatus(PLAYERSTATUS::PLAYER_WAIT);
				clients[id].c_lock.unlock();

				if (!waitroom && id == 0) {
					roomsector[0].r_lock.lock();
					roomsector[0].SetRoomNumber(0);
					roomsector[0].SetPlayerID(id);
					roomsector[0].SetRoomStatus(ROOMSTATUS::ROOMWAIT);
					roomsector[0].r_lock.unlock();
					clients[id].SetRoomIndex(0);

				}
				else if (!waitroom && id != 0)
				{
					roomsector[0].r_lock.lock();
					roomsector[0].SetPlayerID(id);
					roomsector[0].r_lock.unlock();
					clients[id].SetRoomIndex(0);
				}
				else if (waitroom) //대기실로 이동 -> 게임 준비시 매칭 
				{
					//여기서 방을 생성하지 않음
				}

				//소켓 생성 후 iocp에 등록
				CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), Iocp, id, 0);

				do_recv(id);
				do_accept(listen_socket, ex_over);
			}
		}
		break;
		default:
			break;
		}
	}
}

void CServer::Send_packet(int user_id, void* data)
{
	EX_OVER* s_over = new EX_OVER;

	unsigned char packet_size = reinterpret_cast<unsigned char*>(data)[0];
	s_over->m_op = OP_SEND;
	memset(&s_over->m_over, 0, sizeof(s_over->m_over));
	memcpy(s_over->m_netbuf, data, packet_size);
	s_over->m_wsabuf[0].buf = reinterpret_cast<char*>(s_over->m_netbuf);
	s_over->m_wsabuf[0].len = packet_size;

	WSASend(clients[user_id].socket, s_over->m_wsabuf, 1, 0, 0, &s_over->m_over, 0);
}

void CServer::Process_packet(int user_id, unsigned char* buf)
{
	switch (buf[1])
	{
	case C2S_LOGIN:
	{
		packet_login p = *(reinterpret_cast<packet_login*>(buf));
		Enter_game(user_id, p.name);
	}
	break;
	//case C2S_ROOMCREATE:
	//{
	//	//클라에서 사이즈 타입, 아이디는 채워서 보낼 것
	//	Create_Room(user_id, buf);
	//}
	//break;
	//case C2S_ROOMENTER:
	//{
	//	//클라에서 방 번호, 아이디는 채워서 보낼 것
	//	Enter_Room(user_id, buf);
	//}
	//break;
	case C2S_WAITROOMENTER:
	{

	}
	break;
	case C2S_MATCHINGSTART:
	{
		matching_request(user_id, buf);
	}
	break;
	case C2S_ROOMOUT:
	{
		//방을 나갔을 때 
	}
	break;
	case C2S_MOVE:
	{
		Move_Player(user_id, buf);
	}
	break;
	case C2S_LEAVEPLAYER:
	{
		//플레이어가 접속을 종료 했을 때
		Disconnect(user_id);
	}
	break;
	case C2S_BOARDMAPREADY:
	{
		Player_ReadyStatue(user_id, C2S_BOARDMAPREADY, buf);
	}
	break;
	case C2S_ORDER:
	{

	}
	break;
	case C2S_JUMPDICE:
	{
		Send_DiceNumandJumpState(user_id, buf);
	}
	break;
	case C2S_DICEMOVEEND:
	{
		Send_DiceMoveEnd(user_id, buf);
	}
	break;
	case C2S_CURRENTORDER:
	{
		Send_CurrentOrder(user_id, buf);
	}
	break;
	case C2S_DICEENTER:
	{
		Send_DiceEnter(user_id, buf);
	}
	break;
	case C2S_COINEVENT:
	{
		Send_CoinEvent(user_id, buf);
	}
	break;
	case C2S_BUTTONEVENT:
	{
		Send_ButtonEvent(user_id, buf);
	}
	break;
	case C2S_DICENUMDOWN:
	{
		Send_DiceNumDown(user_id, buf);
	}
	break;
	case C2S_PLAYERSCOL:
	{
		Send_PlayerCollision(user_id, buf);
	}
	break;
	case C2S_SCENECHANGE:
	{
		Init_SceneChange(user_id, buf);
	}
	break;
	//falling
	case C2S_FALLINGMAPREADY:
	{

	}
	break;
	case C2S_ANIMATION:
	{
		Send_PlayerAnimstate(user_id, buf);
	}
	break;
	case C2S_DELETEBLOCK:
	{
		//Send_DeleteBlock(user_id, buf);
	}
	break; 
	case C2S_PLAYERDEAD:
	{
		Send_PlayerDead(user_id, buf);
	}
	break;
	case C2S_MINIGAMELOADING:
	{
		Send_MinigameLoading(user_id, buf);
	}
	break;
	case C2S_ONEMINDDIR:
	{
		OneMind_MoveDirection(user_id, buf);
	}
	break;
	//case C2S_ONEMINDKEY:
	//{
	//	OneMind_Move(user_id, buf);
	//}
	//break;
	case C2S_ONEMINDMOVE:
	{
		OneMind_Move(user_id, buf);
	}
	break;
	case C2S_ONEMINDLIGHT:
	{
		OneMind_Light(user_id, buf);
	}
	break;
	case C2S_ONEMINDANIM:
	{
		OneMind_Anim(user_id, buf);
	}
	break;
	case C2S_SHOWMAP:
	{
		Show_Map(user_id, buf);
	}
	break;
	case C2S_STARTTIMER:
	{
		Start_Ask(user_id, buf);
	}
	break;
	case C2S_MATCHTIE: //동점자 주사위 요청
	{
		MatchTie(user_id, buf);
	}
	break;
	case C2S_EVENT:
	{
		Boardmap_Event(user_id, buf);
	}
	break;
	case C2S_EVENTSELECT:
	{
		Boardmap_EventSelect(user_id, buf);
	}
	break;
	case C2S_ITEMUSE:
	{
		Boardmap_ItemUse(user_id, buf);
	}
	break;
	case C2S_TESTTIERANK:
	{
		TestTie_Rank(user_id, buf);
	}
	break;
	case C2S_SPECIALEVENT:
	{
		Boardmap_SpacialEvent(user_id, buf);
	}
	break;
	case C2S_TELEPORT:
	{
		SpecialEvent_Teleport(user_id, buf);
	}
	break;
	default:
		cout << "Unknown Packet Type Error" << endl;
		DebugBreak(); //여기서 멈춰라
		exit(-1);
	}
}

void CServer::Send_leave_packet(int user_id, int left_id)
{
	packet_leave p;
	p.id = left_id; 
	p.size = sizeof(p);
	p.type = S2C_LEAVEPLAYER;

	Send_packet(user_id, &p);
}

void CServer::Disconnect(int user_id)
{
	int roomnumber = clients[user_id].GetRoomIndex();

	cout << clients[user_id].GetName() <<" "<<user_id <<"번 접속 종료" << endl;
	clients[user_id].c_lock.lock();
	clients[user_id].SetConnect(false);
	clients[user_id].SetStatus(PLAYER_WAIT);
	clients[user_id].SetRoomIndex(-1);
	clients[user_id].c_lock.unlock();


	for (int i = 0; i < 3; ++i) {

		int playerID = roomsector[roomnumber].GetPlayerID(i);

		if (user_id == playerID)
			continue;

		if (playerID != -1) {
			if (clients[playerID].GetConnected()) {
				Send_leave_packet(playerID, user_id);
				clients[playerID].c_lock.lock();
				clients[playerID].SetStatus(PLAYER_WAIT);
				clients[playerID].SetRoomIndex(-1);
				clients[playerID].c_lock.unlock();
			}
		}
	}

	roomsector.erase(roomnumber);
}

void CServer::Send_player_order_packet(int user_id)
{
	int roomnumber = clients[user_id].GetRoomIndex();
	int playerID[3];

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		playerID[i] = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();
	}

	packet_order p;
	ZeroMemory(&p, sizeof(p));
	p.size = sizeof(p);
	p.type = S2C_ORDER;

	for (int i = 0; i < 3; ++i)
	{
		int clientid = playerID[i];

		for (int j = 0; j < 3; ++j)
		{
			p.id = playerID[j];
			p.order = j; //들어온 순서대로 순서, 임시

			switch (j)
			{
			case 0:
			{
				//-9.f, 13.6f, -80.5f
				p.x = -8.5f;
				p.y = 13.6f;
				p.z = -80.f;
			}
			break;
			case 1:
			{
				p.x = -9.f;
				p.y = 13.6f;
				p.z = -80.5f;
			}
			break;
			case 2:
			{
				p.x = -9.5f;
				p.y = 13.6f;
				p.z = -80.f;
			}
			break;
			default:
				break;
			}
			Send_packet(clientid, &p);
			
		}
	}
}

void CServer::Send_DiceNumandJumpState(int user_id, unsigned char* buf)
{
	srand((unsigned)time(NULL));

	packet_jumpdice p = *reinterpret_cast<packet_jumpdice*>(buf);
	p.type = S2C_JUMPDICE;

	p.dicenum = rand() % 6 + 1;

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_DiceMoveEnd(int user_id, unsigned char* buf)
{
	packet_dicemoveend p = *reinterpret_cast<packet_dicemoveend*>(buf);
	p.type = S2C_DICEMOVEEND;

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_CurrentOrder(int user_id, unsigned char* buf)
{
	packet_currentorder p = *reinterpret_cast<packet_currentorder*>(buf);
	p.type = S2C_CURRENTORDER;
	
	if (p.currentorder < 2)
		++p.currentorder;
	else
		p.currentorder = 0;

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_DiceEnter(int user_id, unsigned char* buf)
{
	packet_diceenter p = *reinterpret_cast<packet_diceenter*>(buf);
	p.type = S2C_DICEENTER;

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_CoinEvent(int user_id, unsigned char* buf)
{
	packet_coin p = *reinterpret_cast<packet_coin*>(buf);
	p.type = S2C_COINEVENT;

	clients[user_id].c_lock.lock();
	clients[user_id].SetCoin(p.num, p.add);
	clients[user_id].c_lock.unlock();

	p.num = clients[user_id].GetCoin();

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}

	Rank_Update(user_id);
}

bool cmpScore(pair<int, int> left, pair<int, int> right) { //내림차순
	return left.second > right.second;
}

void CServer::Rank_Update(int user_id) 
{
	packet_rank pr;
	pr.size = sizeof(packet_rank);
	pr.type = S2C_BOARDMAPRANK;

	int roomnumber = clients[user_id].GetRoomIndex();
	
	vector<pair<int, int>> score;//id, score

	//순위 매기기
	for (int i = 0; i < 3; ++i)
	{
		int id = roomsector[roomnumber].GetPlayerID(i);

		if (id != -1) {
			int playerbuttonnum = 0; //플레이어들의 단추개수
			int playercoinnum = 0; //플레이어들의 코인개수

			clients[id].c_lock.lock();
			playercoinnum =  clients[id].GetCoin();
			playerbuttonnum = clients[id].GetButton();
			clients[id].c_lock.unlock();

			score.emplace_back(make_pair(id, playerbuttonnum * 10000 + playercoinnum));
		}
	}

	//vector 정렬을 통해 정렬 내림차순 -> 100 50 0
	sort(score.begin(), score.end(), cmpScore);
	
	pr.matchtie = 0;
	if (score[0].second == score[1].second) //1등 2등 동점
	{
		pr.matchtie = 1;
	}
	if (score[1].second == score[2].second) //2등 3등 동점
	{
		pr.matchtie = 2;
	}
	if ((score[0].second == score[1].second) && (score[1].second == score[2].second))
	{
		pr.matchtie = 3;
	}
	pr.matchtie; //0-동점자 없음 1-1등2등 동점 2- 2등3등 동점 3- 1등 2등 3등 동점

	int i = 0;
	for (auto p : score) {
		switch (i)
		{
		case 0: {
			pr.id1 = p.first; //1등
		}
		break;
		case 1:
		{
			pr.id2 = p.first; //2등
		}
		break;
		case 2:
		{
			pr.id3 = p.first; //3등
		}
		break;
		}
		i++;
	}
	
	for (int i = 0; i < 3; ++i)
	{
		int id = roomsector[roomnumber].GetPlayerID(i);

		if (id != -1) {
			Send_packet(id, &pr);
		}
	}
}

void CServer::TestTie_Rank(int user_id, unsigned char* buf)
{
	packet_testtie p = *reinterpret_cast<packet_testtie*>(buf);
	p.type = S2C_TESTTIERANK;

	int roomnumber = clients[user_id].GetRoomIndex();

	clients[user_id].c_lock.lock();
	clients[user_id].SetTestButton(p.button);
	clients[user_id].SetTestCoin(p.coin);
	clients[user_id].c_lock.unlock();

	for (int i = 0; i < 3; ++i)
	{
		int id = roomsector[roomnumber].GetPlayerID(i);

		if (id != -1 && id != user_id)
		{
			Send_packet(id, &p);
		}
	}

	Rank_Update(user_id);
}

void CServer::Show_Map(int user_id, unsigned char* buf)
{
	packet_showmap ps = *reinterpret_cast<packet_showmap*>(buf);
	ps.type = S2C_SHOWMAP;

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i)
	{
		int id = roomsector[roomnumber].GetPlayerID(i);

		if (id != -1 && id!= user_id)
		{
			Send_packet(id, &ps);
		}
	}
}

void CServer::Start_Ask(int user_id, unsigned char* buf)
{
	int roomnumber = clients[user_id].GetRoomIndex();

	roomsector[roomnumber].sask_lock.lock();
	int ask = roomsector[roomnumber].Add_StartAsk();
	roomsector[roomnumber].sask_lock.unlock();

	if (ask == 3)
	{
		thread st = thread(do_Starttimer);
		st.detach();

		startingtimer(user_id, EVENT_TYPE::TIME_COUNT_SECONDS, 1000);

		roomsector[roomnumber].sask_lock.lock();
		roomsector[roomnumber].Set_StartAsk(0);
		roomsector[roomnumber].sask_lock.unlock();
	}
}

void CServer::MatchTie(int user_id, unsigned char* buf)
{
	packet_matchtie p = *reinterpret_cast<packet_matchtie*>(buf);

	int roomnuber = clients[user_id].GetRoomIndex();

	p.type = S2C_MATCHTIE;
	p.id = user_id;
	p.dicenum = roomsector[roomnuber].Get_MatchTieDice();

	for (int i = 0; i < 3; ++i)
	{
		int playerid = roomsector[roomnuber].GetPlayerID(i);

		if (playerid != -1)
		{
			Send_packet(playerid, &p);
		}
	}
}

void CServer::Boardmap_Event(int user_id, unsigned char* buf)
{
	packet_event p = *reinterpret_cast<packet_event*>(buf);
	p.type = S2C_EVENT;
	
	int roomnumber = clients[user_id].GetRoomIndex();

	//단추 혹은 상점 도착
	for (int i = 0; i < 3; ++i)
	{
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1 && user_id != playernum)
		{
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Boardmap_EventSelect(int user_id, unsigned char* buf)
{
	packet_eventselect p = *reinterpret_cast<packet_eventselect*>(buf);
	p.type = S2C_EVENTSELECT;

	int roomnumber = clients[user_id].GetRoomIndex();

	//단추 혹은 상점 도착 후 구매 여부

	if (p.buy)
	{
		switch (p.item)
		{
		case ITEM_MINIDICE: 
		{
			clients[user_id].c_lock.lock();
			clients[user_id].SetCoin(p.coin, false);
			p.coin = clients[user_id].GetCoin();
			clients[user_id].c_lock.unlock();
			p.button = 0;
		}
			break;
		case ITEM_BUTTON:
		{
			clients[user_id].c_lock.lock();
			clients[user_id].SetCoin(p.coin, false);
			p.coin = clients[user_id].GetCoin();
			clients[user_id].SetButton(1, true);
			p.button = clients[user_id].GetButton();
			clients[user_id].c_lock.unlock();
		}
		break;
		case ITEM_PLUS3:
		{
			clients[user_id].c_lock.lock();
			clients[user_id].SetCoin(p.coin, false);
			p.coin = clients[user_id].GetCoin();
			clients[user_id].c_lock.unlock();
			p.button = 0;
		}
			break;
		case ITEM_PLUS5:
		{
			clients[user_id].c_lock.lock();
			clients[user_id].SetCoin(p.coin, false);
			p.coin = clients[user_id].GetCoin();
			clients[user_id].c_lock.unlock();
			p.button = 0;
		}
			break;
		case ITEM_END:
			break;
		default:
			break;
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1 && user_id != playernum)
		{
			Send_packet(playernum, &p);
		}
	}

	Rank_Update(user_id);
}

void CServer::Boardmap_ItemUse(int user_id, unsigned char* buf)
{
	packet_itemuse p = *reinterpret_cast<packet_itemuse*>(buf);
	p.type = S2C_ITEMUSE;

	int roomnumber = clients[user_id].GetRoomIndex();

	srand((unsigned)time(NULL));

	switch (p.item)
	{
	case ITEM_MINIDICE:
		p.dice = ((rand() % 3) + 1); // 1 2 3
		break;
	case ITEM_PLUS3:
		p.dice = 0;
		break;
	case ITEM_PLUS5:
		p.dice = 0;
		break;
	case ITEM_END:
		break;
	default: 
		break;
	}

	for (int i = 0; i < 3; ++i)
	{
		int playerid = roomsector[roomnumber].GetPlayerID(i);

		if (playerid != -1)
		{
			Send_packet(playerid, &p);
		}
	}

}

void CServer::Boardmap_SpacialEvent(int user_id, unsigned char* buf)
{
	packet_specialevent p = *reinterpret_cast<packet_specialevent*>(buf);
	p.type = S2C_SPECIALEVENT;
	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i)
	{
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1 && playernum != user_id) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::SpecialEvent_Teleport(int user_id, unsigned char* buf)
{
	packet_teleport p = *reinterpret_cast<packet_teleport*>(buf);
	p.type = S2C_TELEPORT;
	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i)
	{
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1 && playernum != user_id) {
			Send_packet(playernum, &p);
		}
	}

}

void CServer::Send_ButtonEvent(int user_id, unsigned char* buf)
{
	packet_button p = *reinterpret_cast<packet_button*>(buf);
	p.type = S2C_BUTTONEVENT;

	clients[user_id].c_lock.lock();
	clients[user_id].SetButton(p.num, p.add);
	clients[user_id].c_lock.unlock();

	p.num = clients[user_id].GetButton();

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}

	Rank_Update(user_id);
}

void CServer::Send_DiceNumDown(int user_id, unsigned char* buf)
{
	packet_dicenumdown p = *reinterpret_cast<packet_dicenumdown*>(buf);
	p.type = S2C_DICENUMDOWN;
	
	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1 && playernum != user_id) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_PlayerCollision(int user_id, unsigned char* buf)
{
	packet_playercollision p = *reinterpret_cast<packet_playercollision*>(buf);
	p.type = S2C_PLAYERCOL;

	int roomnumber = clients[user_id].GetRoomIndex();

	float x, y, z;
	clients[p.id].pos_lock.lock();
	clients[p.id].GetPosition(x, y, z);
	clients[p.id].pos_lock.unlock();

	x += p.shiftx;
	y += p.shifty;
	z += p.shiftz;

	clients[p.id].pos_lock.lock();
	clients[p.id].SetPosition(x, y, z);
	clients[p.id].pos_lock.unlock();

	p.shiftx = x;
	p.shifty = y;
	p.shiftz = z;

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1 && playernum != user_id) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_PlayerAnimstate(int user_id, unsigned char* buf)
{
	packet_animation p = *reinterpret_cast<packet_animation*>(buf);
	p.type = S2C_ANIMATION;

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1 && playernum != user_id) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_DeleteBlock(int user_id, CTile* tile)
{
	packet_deleteblock p;
	ZeroMemory(&p, sizeof(p));
	p.type = S2C_DELETEBLOCK;
	p.size = sizeof(packet_deleteblock);
	p.blockcolor = tile->GetTileColor();
	p.blockid = tile->GetTileIndex();

	int roomnumber = clients[user_id].GetRoomIndex();

	for (int i = 0; i < 3; ++i) {
		//roomsector[roomnumber].r_lock.lock();
		int playernum = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playernum != -1) {
			Send_packet(playernum, &p);
		}
	}
}

void CServer::Send_PlayerDead(int user_id, unsigned char* buf)
{
	packet_playerdead p = *reinterpret_cast<packet_playerdead*>(buf);

	int roomnumber = clients[user_id].GetRoomIndex();

	roomsector[roomnumber].s_lock.lock();
	roomsector[roomnumber].AddMinigameDeadNum(); //죽은플레이어 +1
	roomsector[roomnumber].s_lock.unlock();

	clients[p.id].s_lock.lock();
	clients[p.id].SetMiniGameDead(true);
	clients[p.id].s_lock.unlock();

	packet_playerdead pd;
	pd.size = sizeof(packet_playerdead);
	pd.type = S2C_PLAYERDEAD;
	pd.id = user_id;

	for (int i = 0; i < 3; ++i) {
		int playernum = roomsector[roomnumber].GetPlayerID(i);

		if (playernum != -1)
		{
			Send_packet(playernum, &pd);
		}
	}

	roomsector[roomnumber].s_lock.lock();
	int deadnum = roomsector[roomnumber].GetMinigameDeadNum();
	roomsector[roomnumber].s_lock.unlock();
	if (deadnum == 2)
	{
		packet_minigameend p2;
		p2.size = sizeof(packet_minigameend);
		p2.type = S2C_MINIGAMEEND;

		for (int i = 0; i < 3; ++i) {
			int playernum = roomsector[roomnumber].GetPlayerID(i);

			if (playernum != -1)
			{
				clients[playernum].s_lock.lock();
				if (!clients[playernum].GetMiniGameDead()) {
					clients[playernum].s_lock.unlock();
					p2.id = playernum;
					break;
				}
				clients[playernum].s_lock.unlock();
			}
		}	

		//코인보내기 -> 미니게임 보상
		packet_coin pc;
		pc.type = S2C_MINIGAMENDCOIN;
		pc.size = sizeof(packet_coin);
		pc.id = p2.id;//승리자
		pc.add = true;

		clients[pc.id].c_lock.lock();
		clients[pc.id].SetCoin(10, pc.add);
		pc.num = clients[pc.id].GetCoin();
		clients[pc.id].c_lock.unlock();

		int roomnumber2 = clients[pc.id].GetRoomIndex();

		for (int i = 0; i < 3; ++i) {
			int playernum = roomsector[roomnumber2].GetPlayerID(i);

			if (playernum != -1) {
				Send_packet(playernum, &pc);
			}
		}

		Rank_Update(user_id);

		//게임종료 보내기
		for (int i = 0; i < 3; ++i) {
			int playernum = roomsector[roomnumber].GetPlayerID(i);

			if (playernum != -1)
			{
				Send_packet(playernum, &p2);
			}
		}

		roomsector[roomnumber].s_lock.lock();
		roomsector[roomnumber].SetMinigameDeadNum(0);
		roomsector[roomnumber].s_lock.unlock();

		for (int i = 0; i < 3; ++i) {
			int playernum = roomsector[roomnumber].GetPlayerID(i);

			clients[playernum].s_lock.lock();
			clients[playernum].SetMiniGameDead(false);
			clients[playernum].s_lock.unlock();
		}
	}

}

void CServer::Send_MinigameLoading(int user_id, unsigned char* buf)
{
	packet_loadingsuccess p = *reinterpret_cast<packet_loadingsuccess*>(buf);
	p.type = S2C_MINIGAMELOADING;
	
	int roomnumber = clients[user_id].GetRoomIndex();

	roomsector[roomnumber].s_lock.lock();
	int currentScene = roomsector[roomnumber].GetCurrentScene();
	roomsector[roomnumber].s_lock.unlock();

	switch (p.scene)
	{
	case S_FALLINGMAP:
	{
		roomsector[roomnumber].l_lock.lock();
		int loadnum = roomsector[roomnumber].Add_LoadingCount();
		roomsector[roomnumber].l_lock.unlock();

		if (loadnum >= 3) {
			roomsector[roomnumber].l_lock.lock();
			roomsector[roomnumber].Set_Loading(true);
			roomsector[roomnumber].Set_LoadingCount(0);
			roomsector[roomnumber].l_lock.unlock();

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &p);
				}
			}

		}
	}
		break;
	case S_GGOGGOMAP:
	{
		roomsector[roomnumber].l_lock.lock();
		int loadnum = roomsector[roomnumber].Add_LoadingCount();
		roomsector[roomnumber].l_lock.unlock();

		if (loadnum >= 3) {
			roomsector[roomnumber].l_lock.lock();
			roomsector[roomnumber].Set_Loading(true);
			roomsector[roomnumber].Set_LoadingCount(0);
			roomsector[roomnumber].l_lock.unlock();

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &p);
				}
			}
			

			for (int i = 0; i < MAX_CHICKEN; ++i)
			{
				add_event(user_id, EVENT_TYPE::NPC_ISSTOP, 500 + rand() % 1000, i);
			}

			add_event(user_id, EVENT_TYPE::TIME_COUNT_SECONDS, 1000);
		}
	}
		break;
	case S_BULLFIGHTMAP:
	{
		roomsector[roomnumber].l_lock.lock();
		int loadnum = roomsector[roomnumber].Add_LoadingCount();
		roomsector[roomnumber].l_lock.unlock();

		if (loadnum >= 3) {
			roomsector[roomnumber].l_lock.lock();
			roomsector[roomnumber].Set_Loading(true);
			roomsector[roomnumber].Set_LoadingCount(0);
			roomsector[roomnumber].l_lock.unlock();

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &p);
				}
			}

			//물약 떨구기 
			add_event(user_id, EVENT_TYPE::POTION_CREATE, 500);
		}
	}
	break;
	case S_ONEMINDMAP:
	{
		roomsector[roomnumber].l_lock.lock();
		int loadnum = roomsector[roomnumber].Add_LoadingCount();
		roomsector[roomnumber].l_lock.unlock();

		if (loadnum >= 3) {
			roomsector[roomnumber].l_lock.lock();
			roomsector[roomnumber].Set_Loading(true);
			roomsector[roomnumber].Set_LoadingCount(0);
			roomsector[roomnumber].l_lock.unlock();

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &p);
				}
			}

			add_event(user_id, EVENT_TYPE::TIME_COUNT_SECONDS, 1000);
		}
	}
	break;
	case S_PRIZEMAP:
	{
		roomsector[roomnumber].l_lock.lock();
		int loadnum = roomsector[roomnumber].Add_LoadingCount();
		roomsector[roomnumber].l_lock.unlock();

		if (loadnum >= 3) {

			roomsector[roomnumber].l_lock.lock();
			roomsector[roomnumber].Set_Loading(true);
			roomsector[roomnumber].Set_LoadingCount(0);
			roomsector[roomnumber].l_lock.unlock();

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &p);
				}
			}
		}
	}
	break;
	default:
		break;
	}
}

void CServer::OneMind_MoveDirection(int user_id, unsigned char* buf)
{
	packet_onemindlook dir = *reinterpret_cast<packet_onemindlook*>(buf);
	
	int roomnumber = clients[user_id].GetRoomIndex();

	dir.type = S2C_ONEMINDDIR;

	for (int i = 0; i < 3; ++i)
	{
		int playerid = roomsector[roomnumber].GetPlayerID(i);

		if (playerid != -1 && playerid != user_id)
		{
			Send_packet(playerid, &dir);
		}
	}
}

void CServer::OneMind_Move(int user_id, unsigned char* buf)
{
	//이동담당 클라가 대표로 위치패킷 보냄
	packet_onemindmove move = *reinterpret_cast<packet_onemindmove*>(buf);

	int roomnumber = clients[user_id].GetRoomIndex();

	roomsector[roomnumber].p_lock.lock();
	roomsector[roomnumber].Set_RobotPosition(move.mx, move.my, move.mz);
	roomsector[roomnumber].p_lock.unlock();

	move.type = S2C_ONEMINDMOVE;

	for (int i = 0; i < 3; ++i)
	{
		int playerid = roomsector[roomnumber].GetPlayerID(i);

		if (playerid != -1 && playerid != user_id)
		{
			Send_packet(playerid, &move);
		}
	}

	float x, y, z;
	roomsector[roomnumber].p_lock.lock();
	roomsector[roomnumber].Get_RobotPosition(x, y, z);
	roomsector[roomnumber].p_lock.unlock();

	roomsector[roomnumber].respawn_lock.lock();
	char savepoint = roomsector[roomnumber].Get_Respawn();
	roomsector[roomnumber].respawn_lock.unlock();

	//리스폰
	if (y < -5.f) //밑으로 추락
	{
		if (savepoint == 0)
		{
			packet_respawn pr;
			pr.size = sizeof(packet_respawn);
			pr.type = S2C_ONEMINDRESPAWN;
			pr.x = 0.f;
			pr.y = 0.65f;
			pr.z = 0.f;

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &pr);
				}
			}
			return;
		}
		else if (savepoint == 1)
		{
			packet_respawn pr;
			pr.size = sizeof(packet_respawn);
			pr.type = S2C_ONEMINDRESPAWN;
			pr.x = 1.2f;
			pr.y = 0.65f;
			pr.z = -31.9f;

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &pr);
				}
			}
			return;
		}
		else if (savepoint == 2)
		{
			packet_respawn pr;
			pr.size = sizeof(packet_respawn);
			pr.type = S2C_ONEMINDRESPAWN;
			pr.x = 1.2f;
			pr.y = 0.65f;
			pr.z = -87.2f;

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &pr);
				}
			}
			return;
		}
	}

	roomsector[roomnumber].snowman_lock.lock();
	bool snow1 = roomsector[roomnumber].Get_Snowman(0);
	bool snow2 = roomsector[roomnumber].Get_Snowman(1);
	roomsector[roomnumber].snowman_lock.unlock();

	//눈사람과의 충돌
	if (savepoint != 1 && !snow1) {

		if(Check_NearPlayer(x, y, z, 1.2f, 0.65f,-31.9f, 2.f))
		{
			roomsector[roomnumber].respawn_lock.lock();
			roomsector[roomnumber].Set_Respawn(1);
			roomsector[roomnumber].respawn_lock.unlock();

			roomsector[roomnumber].snowman_lock.lock();
			roomsector[roomnumber].Set_Snowman(true, 0);
			roomsector[roomnumber].snowman_lock.unlock();

			packet_savepoint ps;
			ps.size = sizeof(packet_savepoint);
			ps.type = S2C_ONEMINDSAVE;
			ps.savepoint = 1;

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &ps);
				}
			}
		}
	}
	if (savepoint != 2 && !snow2) {

		if (Check_NearPlayer(x, y, z, 1.2f, 0.65f, -87.2f, 2.f))
		{
			roomsector[roomnumber].respawn_lock.lock();
			roomsector[roomnumber].Set_Respawn(2);
			roomsector[roomnumber].respawn_lock.unlock();

			roomsector[roomnumber].snowman_lock.lock();
			roomsector[roomnumber].Set_Snowman(true, 1);
			roomsector[roomnumber].snowman_lock.unlock();

			packet_savepoint ps;
			ps.size = sizeof(packet_savepoint);
			ps.type = S2C_ONEMINDSAVE;
			ps.savepoint = 2;

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					Send_packet(playerid, &ps);
				}
			}
		}
		
	}

	roomsector[roomnumber].goal_lock.lock();
	bool isgoal = roomsector[roomnumber].Get_isGoal();
	roomsector[roomnumber].goal_lock.unlock();
	//종료 지점에 도착
	if (!isgoal) {
		if (Check_NearPlayer(x, y, z, 2.5f, 0.65f, -148.5f, 4.f))
		{
			roomsector[roomnumber].goal_lock.lock();
			roomsector[roomnumber].Set_isGoal(true);
			roomsector[roomnumber].goal_lock.unlock();

			add_event(user_id, EVENT_TYPE::EVENT_END, 10);

			//코인 지급
			packet_coin pc;
			pc.type = S2C_MINIGAMENDCOIN;
			pc.size = sizeof(packet_coin);
			pc.add = true;

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);
				if (playerid != -1)
				{
					pc.id = playerid;
					clients[playerid].c_lock.lock();
					clients[playerid].SetCoin(10, pc.add);
					pc.num = clients[pc.id].GetCoin();
					clients[playerid].c_lock.unlock();

					for (int j = 0; j < 3; ++j) { //승리자 코인 보내기
						int playerid2 = roomsector[roomnumber].GetPlayerID(j);
						if (playerid2 != -1)
						{
							Send_packet(playerid2, &pc);
						}
					}
				}
			}

			Rank_Update(user_id);

			packet_minigameend p2;
			p2.size = sizeof(packet_minigameend);
			p2.type = S2C_MINIGAMEEND;
			p2.id = -3; //전체 승리

			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);
				if (playerid != -1)
				{
					Send_packet(playerid, &p2);
				}
			}

			
		}
	}
}

void CServer::OneMind_Light(int user_id, unsigned char* buf)
{
	packet_onemindlight l = *reinterpret_cast<packet_onemindlight*>(buf);

	int roomnumber = clients[user_id].GetRoomIndex();

	l.type = S2C_ONEMINDLIGHT;

	roomsector[roomnumber].light_lock.lock();
	roomsector[roomnumber].Set_Lighton(l.light);
	roomsector[roomnumber].light_lock.unlock();

	for (int i = 0; i < 3; ++i)
	{
		int playerid = roomsector[roomnumber].GetPlayerID(i);

		if (playerid != -1) //불키는 건 모두에게 보내기
		{
			Send_packet(playerid, &l);
		}
	}

	//켜졌으면 타이머로 게이지 줄이기
}

void CServer::OneMind_Anim(int user_id, unsigned char* buf)
{
	packet_animation p = *reinterpret_cast<packet_animation*>(buf);

	int roomnumber = clients[user_id].GetRoomIndex();

	p.type = S2C_ONEMINDANIM;

	for (int i = 0; i < 3; ++i)
	{
		int playerid = roomsector[roomnumber].GetPlayerID(i);

		if (playerid != -1 && playerid != user_id) //움직인 플레이어 애니메이션 보내기
		{
			Send_packet(playerid, &p);
		}
	}
}

void CServer::Init_SceneChange(int user_id, unsigned char* buf)
{
	packet_scenechange p = *reinterpret_cast<packet_scenechange*>(buf);
	p.type = S2C_SCENECHANGE;
	int roomnumber = clients[user_id].GetRoomIndex();

	switch (p.map)
	{
	case S_BOARDMAP:
	{
		roomsector[roomnumber].s_lock.lock();
		roomsector[roomnumber].AddSceneChangeReady();
		roomsector[roomnumber].s_lock.unlock();

		clients[p.id].pos_lock.lock();
		clients[p.id].SetPosition(p.x, p.y, p.z);
		clients[p.id].SetLook(p.lookx, p.looky, p.lookz);
		clients[p.id].pos_lock.unlock();

		//3명의 플레이어가 들어오면
		roomsector[roomnumber].s_lock.lock();
		int count = roomsector[roomnumber].GetSceneChangeReady();
		roomsector[roomnumber].s_lock.unlock();
		if (count >= 3)
		{
			int playernum[3];
			for (int i = 0; i < 3; ++i)
			{
				playernum[i] = roomsector[roomnumber].GetPlayerID(i);
			}
			
			for (int i = 0; i < 3; ++i)
			{
				for (int j = 0; j < 3; ++j)
				{
					p.id = playernum[j];

					clients[playernum[j]].pos_lock.lock();
					clients[playernum[j]].GetPosition(p.x, p.y, p.z);
					clients[playernum[j]].GetLook(p.lookx, p.looky, p.lookz);
					clients[playernum[j]].pos_lock.unlock();

					Send_packet(playernum[i], &p);
				}
			}

			roomsector[roomnumber].s_lock.lock();
			roomsector[roomnumber].SetCurrentScene(S_BOARDMAP);
			roomsector[roomnumber].InitSceneChange();
			roomsector[roomnumber].s_lock.unlock();

			roomsector[roomnumber].l_lock.lock();
			roomsector[roomnumber].Set_Loading(false); //미니게임용 
			roomsector[roomnumber].l_lock.unlock();
		}
	}
	break;
	case S_FALLINGMAP:
	{
		//후에 플레이어 위치 조정하기
		roomsector[roomnumber].s_lock.lock();
		roomsector[roomnumber].AddSceneChangeReady(); 
		roomsector[roomnumber].s_lock.unlock();

		int playernum[3];
		for (int i = 0; i < 3; ++i)
		{
			playernum[i] = roomsector[roomnumber].GetPlayerID(i);
		}

		//3명의 플레이어가 들어오면
		if (roomsector[roomnumber].GetSceneChangeReady() >= 3)
		{
			for (int i = 0; i < 3; ++i){ //3명의 정보를 한번에 보내기 위함
			
				clients[playernum[i]].s_lock.lock();
				clients[playernum[i]].SetMiniGameDead(false);
				clients[playernum[i]].s_lock.unlock();

				for (int j = 0; j < 3; ++j) {
					p.x = 11.8047f - (4.f - 4.f * j);
					p.y = 16.7839f;
					p.z = 12.2751f;
					p.id = playernum[j];
					Send_packet(playernum[i], &p);

				}
			}

			roomsector[roomnumber].InitTiles();

			roomsector[roomnumber].st_lock.lock();
			roomsector[roomnumber].Set_StartTime(4);
			roomsector[roomnumber].st_lock.unlock();

			roomsector[roomnumber].s_lock.lock();
			roomsector[roomnumber].SetMinigameDeadNum(0);
			roomsector[roomnumber].InitSceneChange();
			roomsector[roomnumber].SetCurrentScene(S_FALLINGMAP);
			roomsector[roomnumber].s_lock.unlock();
		}
	}
	break;
	case S_GGOGGOMAP:
	{
		
		roomsector[roomnumber].s_lock.lock();
		int pnum = roomsector[roomnumber].AddSceneChangeReady(); //GGOGGOMAP으로 가고싶다고 요청한 플레이어 수
		roomsector[roomnumber].s_lock.unlock();

		if (roomsector[roomnumber].GetSceneChangeReady() >= 3)
		{
			//방의 플레이어 인덱스는 게임시작 후 바뀔일이 없으므로 lock 없어도됨
			int playernum[3];
			for (int i = 0; i < 3; ++i)
			{
				playernum[i] = roomsector[roomnumber].GetPlayerID(i);
			}

			//닭 정보 보내기
			roomsector[roomnumber].InitChickens();

			for (int i = 0; i < MAX_CHICKEN; ++i)
			{
				CChicken* chick = roomsector[roomnumber].GetChicken()[i];

				for (int j = 0; j < 3; ++j) {
					packet_Initchicken p;
					p.c_id = i;
					p.type = S2C_CHICKENINIT;
					p.size = sizeof(packet_Initchicken);
					
					chick->c_lock.lock();
					p.angle = chick->GetAngle();
					p.state = chick->GetState();
					chick->GetPosition(p.x, p.y, p.z);
					chick->c_lock.unlock();

					Send_packet(playernum[j], &p);
				}
			}

			//플레이어 정보 보내기
			for (int i = 0; i < 3; ++i) //3명의 정보를 한번에 보내기 위함
			{
				for (int j = 0; j < 3; ++j) {
					p.x = (j + 1) * 10 - 20;
					p.y = 0;
					p.z = 0;
					p.id = playernum[j];
					Send_packet(playernum[i], &p);

				}
			}

			roomsector[roomnumber].st_lock.lock();
			roomsector[roomnumber].Set_StartTime(4);
			roomsector[roomnumber].st_lock.unlock();

			timer_lock.lock();
			while (!timer_queue.empty()) //타이머 큐 비우기
			{
				timer_queue.pop();
			}
			timer_lock.unlock();

			time_thread = thread(do_timer);
			time_thread.detach();

			roomsector[roomnumber].t_lock.lock();
			roomsector[roomnumber].Set_PlayTime(0);
			roomsector[roomnumber].t_lock.unlock();

			roomsector[roomnumber].s_lock.lock();
			roomsector[roomnumber].SetMinigameDeadNum(0);
			roomsector[roomnumber].InitSceneChange();
			roomsector[roomnumber].SetCurrentScene(S_GGOGGOMAP);
			roomsector[roomnumber].s_lock.unlock();


		}
	}
	break;
	case S_BULLFIGHTMAP:
	{
		roomsector[roomnumber].s_lock.lock();
		int pnum = roomsector[roomnumber].AddSceneChangeReady(); //투우맵으로 가고싶다고 요청한 플레이어 수
		roomsector[roomnumber].s_lock.unlock();

		if (roomsector[roomnumber].GetSceneChangeReady() >= 3)
		{
			//방의 플레이어 인덱스는 게임시작 후 바뀔일이 없으므로 lock 없어도됨
			int playernum[3];
			for (int i = 0; i < 3; ++i)
			{
				playernum[i] = roomsector[roomnumber].GetPlayerID(i);
			}

			//플레이어 정보 보내기
			for (int i = 0; i < 3; ++i) //3명의 정보를 한번에 보내기 위함
			{
				clients[playernum[i]].s_lock.lock();
				clients[playernum[i]].SetMiniGameDead(false);
				clients[playernum[i]].s_lock.unlock();

				for (int j = 0; j < 3; ++j) {
					p.x = (j + 1) * 10 - 20;
					p.y = 0;
					p.z = 0;
					p.id = playernum[j];
					Send_packet(playernum[i], &p);

				}
			}

			roomsector[roomnumber].st_lock.lock();
			roomsector[roomnumber].Set_StartTime(4);
			roomsector[roomnumber].st_lock.unlock();

			roomsector[roomnumber].p_lock.lock();
			roomsector[roomnumber].Set_PotionPosition(0.f, 0.f, 0.f);
			roomsector[roomnumber].Set_PotionCreate(false);
			roomsector[roomnumber].p_lock.unlock();

			timer_lock.lock();
			while (!timer_queue.empty()) //타이머 큐 비우기
			{
				timer_queue.pop();
			}
			timer_lock.unlock();

			time_thread = thread(do_timer);
			time_thread.detach();

			roomsector[roomnumber].s_lock.lock();
			roomsector[roomnumber].SetMinigameDeadNum(0);
			roomsector[roomnumber].InitSceneChange();
			roomsector[roomnumber].SetCurrentScene(S_BULLFIGHTMAP);
			roomsector[roomnumber].s_lock.unlock();
		}
	}
	break;
	case S_ONEMINDMAP:
	{
		roomsector[roomnumber].s_lock.lock();
		int pnum = roomsector[roomnumber].AddSceneChangeReady(); //셋맘맵으로 가고싶다고 요청한 플레이어 수
		roomsector[roomnumber].s_lock.unlock();

		if (roomsector[roomnumber].GetSceneChangeReady() >= 3)
		{
			roomsector[roomnumber].p_lock.lock();
			roomsector[roomnumber].Init_RobotPosition(); //처음 로봇 위치 초기화
			roomsector[roomnumber].p_lock.unlock();

			roomsector[roomnumber].t_lock.lock();
			roomsector[roomnumber].Set_PlayTime(0);
			roomsector[roomnumber].t_lock.unlock();

			//방의 플레이어 인덱스는 게임시작 후 바뀔일이 없으므로 lock 없어도됨
			
			packet_roll pr;
			pr.size = sizeof(packet_roll);
			pr.type = S2C_ROLL;

			//플레이어 정보 보내기
			int playernum[3];

			vector<int> rrandom;//역할랜덤하게 주는 용도

			for (int i = 0; i < 3; ++i)
			{
				playernum[i] = roomsector[roomnumber].GetPlayerID(i);
				rrandom.push_back(i);
			}
		
			random_device rd;
			mt19937 g(rd());
			
			shuffle(rrandom.begin(), rrandom.end(), g);

			for (int i = 0; i < 3; ++i) //플레이어 역할 보내기
			{
				pr.roll = rrandom[i];
				pr.id = playernum[i];

				clients[playernum[i]].s_lock.lock();
				clients[playernum[i]].SetMiniGameDead(false);
				clients[playernum[i]].s_lock.unlock();

				for (int j = 0; j < 3; ++j) { //3명의 플레이어의 역할을 모두 보내줘야함 각각의 플레이어에게
					Send_packet(playernum[j], &pr);
				}
			}

			roomsector[roomnumber].st_lock.lock();
			roomsector[roomnumber].Set_StartTime(4);
			roomsector[roomnumber].st_lock.unlock();

			roomsector[roomnumber].battery_lock.lock();
			roomsector[roomnumber].Set_Batteryhp(90.f);
			roomsector[roomnumber].battery_lock.unlock();

			roomsector[roomnumber].light_lock.lock();
			roomsector[roomnumber].Set_Lighton(false);
			roomsector[roomnumber].light_lock.unlock();

			roomsector[roomnumber].respawn_lock.lock();
			roomsector[roomnumber].Set_Respawn(0);
			roomsector[roomnumber].respawn_lock.unlock();

			roomsector[roomnumber].snowman_lock.lock();
			roomsector[roomnumber].Set_Snowman(false, 0);
			roomsector[roomnumber].Set_Snowman(false, 1);
			roomsector[roomnumber].snowman_lock.unlock();

			roomsector[roomnumber].goal_lock.lock();
			roomsector[roomnumber].Set_isGoal(false);
			roomsector[roomnumber].goal_lock.unlock();

			timer_lock.lock();
			while (!timer_queue.empty()) //타이머 큐 비우기
			{
				timer_queue.pop();
			}
			timer_lock.unlock();

			time_thread = thread(do_timer);
			time_thread.detach();

			roomsector[roomnumber].s_lock.lock();
			roomsector[roomnumber].SetMinigameDeadNum(0);
			roomsector[roomnumber].InitSceneChange();
			roomsector[roomnumber].SetCurrentScene(S_ONEMINDMAP);
			roomsector[roomnumber].s_lock.unlock();
		}
	}
	break;
	case S_PRIZEMAP:
	{
		roomsector[roomnumber].s_lock.lock();
		int pnum = roomsector[roomnumber].AddSceneChangeReady(); //단상맵으로 가고싶다고 요청한 플레이어 수
		roomsector[roomnumber].s_lock.unlock();

		if (roomsector[roomnumber].GetSceneChangeReady() >= 3)
		{

			vector<pair<int, int>> score;//id, score

			//순위 매기기
			for (int i = 0; i < 3; ++i)
			{
				int id = roomsector[roomnumber].GetPlayerID(i);

				if (id != -1) {
					int playerbuttonnum = 0; //플레이어들의 단추개수
					int playercoinnum = 0; //플레이어들의 코인개수

					clients[id].c_lock.lock();
					playercoinnum = clients[id].GetCoin();
					playerbuttonnum = clients[id].GetButton();
					clients[id].c_lock.unlock();

					score.emplace_back(make_pair(id, playerbuttonnum * 10000 + playercoinnum));
				}
			}

			//vector 정렬을 통해 정렬 내림차순 -> 100 50 0
			sort(score.begin(), score.end(), cmpScore);

			//1등 2등 스코어가 동점인지 비교
			int cnt = 0;
			int scores[2];
			int ids[2];
			for (auto p : score)
			{
				if (cnt == 2)
					break;
				ids[cnt] = p.first;
				scores[cnt++] = p.second;
			}
			
			packet_istied pi;
			pi.size = sizeof(packet_istied);
			pi.type = S2C_TIEPLAYER;

			short tied = 2;
			if (scores[0] == scores[1]) { //동점자 있음
				tied = 1;
				pi.id1 = ids[0];
				pi.id2 = ids[1];
			}
			else {
				pi.id1 = -1;
				pi.id2 = -1;
			}

			//test
			//short tied = 1;
			//pi.id1 = ids[0];
			//pi.id2 = ids[1];

			pi.tied = tied;
			
			for (int i = 0; i < 3; ++i)
			{
				int playerid = roomsector[roomnumber].GetPlayerID(i);

				if (playerid != -1)
				{
					clients[playerid].c_lock.lock();
					clients[playerid].SetStatus(PLAYER_WAIT);
					clients[playerid].c_lock.unlock();
					Send_packet(playerid, &pi);
				}
			}

			roomsector[roomnumber].Init_tieDice();

			roomsector[roomnumber].s_lock.lock();
			roomsector[roomnumber].InitSceneChange();
			roomsector[roomnumber].SetCurrentScene(S_PRIZEMAP);
			roomsector[roomnumber].s_lock.unlock();

		}
	}
	break;
	default:
		break;
	}

}

void CServer::Enter_game(int user_id, char name[])
{
	//대기실 시 수정 필요 현재 메뉴의 방 목록둘을 보내줘야함.

	//이름 중복 검사.
	if (!Check_OverlappedName(name)) //중복이지 않다.
	{
		//로그인 성공
		clients[user_id].c_lock.lock();
		clients[user_id].SetName(name);
		clients[user_id].SetStatus(PLAYERSTATUS::PLAYER_WAIT);
		clients[user_id].c_lock.unlock();

		Send_login_packet(user_id, S2C_LOGIN_OK);


		if (!waitroom) { //대기실 적용 안함
			for (auto& client : clients)
			{
				if (user_id == client.second.GetID())
					continue;

				client.second.c_lock.lock();

				if (client.second.GetConnected() == true && client.second.GetStatus() == PLAYERSTATUS::PLAYER_WAIT) {
					//이미 접속한 플레이어 정보를 나에게 보냄
					Send_enter_packet(user_id, client.second.GetID());
					//접속해있던 플레이어에게 나의 정보를 보냄
					Send_enter_packet(client.second.GetID(), user_id);
				}

				client.second.c_lock.unlock();
			}
		}
		else
		{
			//대기실 적용 했을 시 같은 방에 있는 지도 확인
		}

	}

	else
	{
		Send_login_packet(user_id, S2C_LOGIN_FAIL);
	}

}

int CServer::Get_FreeRoomNumber()
{
	while (true) {
		int randnum = rand() % 1000;

		auto iter = roomsector.find(randnum);

		if (iter == roomsector.end())
			return randnum;
	}
	return -1;
}

//void CServer::Create_Room(int user_id, unsigned char* buf)
//{
//	packet_roomcreate p = *(reinterpret_cast<packet_roomcreate*>(buf));
//	p.roomid = roomnum++;
//	p.size = sizeof(packet_roomcreate);
//	p.type = S2C_ROOMCREATE;
//	p.id = user_id;
//
//	//방 정보를 업데이트한다.
//	CRoom room;
//	roomsector[p.roomid].SetRoomNumber(p.roomid);
//	roomsector[p.roomid].SetPlayerID(p.id);
//	roomsector[p.roomid].SetRoomStatus(ROOMSTATUS::ROOMWAIT);
//	clients[user_id].SetRoomIndex(p.roomid);
//
//	//방을 만든 사람에게 패킷을 보낸다.
//	Send_packet(user_id, &p);
//
//	//방번호를 메뉴에있던 플레이어들에게 방 목록을 띄우기 위해 보내준다.
//	for (auto& client : clients)
//	{
//		if (client.first == user_id)
//			continue;
//
//		if (client.second.GetStatus() == PLAYER_WAIT)
//			Send_packet(client.first, &p);
//	}
//}

//void CServer::Enter_Room(int user_id, unsigned char* buf)
//{
//	packet_roomenter p = *(reinterpret_cast<packet_roomenter*>(buf));
//
//	//방에 플레이어가 들어왔다.
//	//방에 있던 플레이어들에게 들어온 플레이어 정보를 보낸다.
//	//들어온 플레이어에게 방에있던 플레이어들의 정보를 보낸다.
//	
//	for (int i = 0; i < 3; ++i)
//	{
//		if (roomsector[p.roomid].GetPlayerID(i) != -1)
//		{
//			//방에있던 플레이어에게 들어온 플레이어 정보 전송
//			Send_packet(roomsector[p.roomid].GetPlayerID(i), &p);
//
//			//들어온 플에이어에게 방에있던 플레이어 정보 전송
//			packet_roomenter r;
//			r.size = sizeof(packet_roomenter);
//			r.type = S2C_ROOMENTER;
//			r.roomid = p.roomid;
//			r.id = roomsector[p.roomid].GetPlayerID(i);
//
//			Send_packet(user_id, &r);
//		}
//	}
//
//	//들어온 플레이어 정보를 방에 업데이트
//	roomsector[p.roomid].SetPlayerID(user_id);
//}

void CServer::Move_Player(int user_id, unsigned char* buf)
{
	packet_move move = *reinterpret_cast<packet_move*>(buf);

	move.type = S2C_MOVE;
	move.size = sizeof(packet_move);
	move.id = user_id;

	clients[user_id].pos_lock.lock();
	clients[user_id].SetPosition(move.mx, move.my, move.mz);
	clients[user_id].pos_lock.unlock();

	//같이 게임 중인 다른 플레이어에게 클라이언트가 움직인 것 보내기
	int roomnumber = clients[user_id].GetRoomIndex(); //움직인 플레이어가 있는 방 번호

	for (int i = 0; i < 3; ++i)
	{
		//해당 방에있는 플레이어 인덱스 추출
		//roomsector[roomnumber].r_lock.lock();
		int playerinroom = roomsector[roomnumber].GetPlayerID(i);
		//roomsector[roomnumber].r_lock.unlock();

		if (playerinroom == user_id)
			continue;

		if (playerinroom != -1)
		{
			Send_packet(playerinroom, &move);
		}
	}

	//떨어져유 블록 삭제 패킷
	roomsector[roomnumber].s_lock.lock();
	int curscene = roomsector[roomnumber].GetCurrentScene();
	roomsector[roomnumber].s_lock.unlock();

	if (curscene == S_FALLINGMAP)
	{
		roomsector[roomnumber].l_lock.lock();
		bool loading = roomsector[roomnumber].Get_Loading();
		roomsector[roomnumber].l_lock.unlock();

		if (!loading) //미니게임 모두 로딩  했는지
			return;

		float px, py, pz;
		clients[user_id].pos_lock.lock();
		clients[user_id].GetPosition(px, py, pz);
		clients[user_id].pos_lock.unlock();

		BoundingBox player;
		BoundingBox block;

		player.centerx = px;
		player.centery = py - 0.35f;
		player.centerz = pz;

		player.scalex = PSCALEX;
		player.scaley = PSCALEY * 1.25f;
		player.scalez = PSCALEZ;

		block.scalex = TSCALEX;
		block.scaley = TSCALEY;
		block.scalez = TSCALEZ;

		int x = player.centerx / (TSCALEX * 2.f); //x 인덱스
		int y = (33.f - player.centerz) / (TSCALEZ * 2.f); //y 인덱스

		int index = x + (y * 12); // 12 - 타일 내의 인덱스 가로 갯수 33 0 30 1 27 2 24 3 0 11

		if (index > 143 || index < 0)
			return;
		bool isDeleted = false;

		if (BLUEY - 5.f < player.centery)
		{
			//블루
			//roomsector[roomnumber].bb_lock.lock();
			CTile* tile2 = roomsector[roomnumber].GetBlueTiles()[index];
			//roomsector[roomnumber].bb_lock.unlock();

			tile2->m_lock.lock();
			isDeleted = tile2->isDeleted();
			tile2->m_lock.unlock();

			if (!isDeleted) {
				tile2->GetPosition(block.centerx, block.centery, block.centerz);
				if (Check_AABBCollision(player, block))
				{
					tile2->m_lock.lock();
					tile2->SetDeleted(true);
					tile2->m_lock.unlock();

					Send_DeleteBlock(user_id, tile2);
				}
			}

			if (index % 12 != 0) //왼쪽있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetBlueTiles()[index - 1];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index % 12 != 11) //오른쪽 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetBlueTiles()[index + 1];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index > 11) //위 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetBlueTiles()[index - 12];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index < 132) //아래 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetBlueTiles()[index + 12];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}
		}

		else if (REDY - 5.f < player.centery)
		{
			//레드
			//roomsector[roomnumber].rb_lock.lock();
			CTile* tile2 = roomsector[roomnumber].GetRedTiles()[index];
			//roomsector[roomnumber].rb_lock.unlock();

			tile2->m_lock.lock();
			isDeleted = tile2->isDeleted();
			tile2->m_lock.unlock();
			if (!isDeleted) {
				tile2->GetPosition(block.centerx, block.centery, block.centerz);
				if (Check_AABBCollision(player, block))
				{
					tile2->m_lock.lock();
					tile2->SetDeleted(true);
					tile2->m_lock.unlock();

					Send_DeleteBlock(user_id, tile2);
				}
			}

			if (index % 12 != 0) //왼쪽있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetRedTiles()[index - 1];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index % 12 != 11) //오른쪽 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetRedTiles()[index + 1];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index > 11) //위 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetRedTiles()[index - 12];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index < 132) //아래 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetRedTiles()[index + 12];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}
		}

		else if (YELLOWY - 5.f < player.centery)
		{
			//옐로
			//tile->m_lock.lock();
			CTile* tile2 = roomsector[roomnumber].GetYellowTiles()[index];
			//tile->m_lock.unlock();

			tile2->m_lock.lock();
			isDeleted = tile2->isDeleted();
			tile2->m_lock.unlock();
			if (!isDeleted) {
				tile2->GetPosition(block.centerx, block.centery, block.centerz);
				if (Check_AABBCollision(player, block))
				{
					tile2->m_lock.lock();
					tile2->SetDeleted(true);
					tile2->m_lock.unlock();

					Send_DeleteBlock(user_id, tile2);
				}
			}

			if (index % 12 != 0) //왼쪽있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetYellowTiles()[index - 1];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index % 12 != 11) //오른쪽 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetYellowTiles()[index + 1];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index > 11) //위 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetYellowTiles()[index - 12];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}

			if (index < 132) //아래 있음
			{
				//tile->m_lock.lock();
				CTile* tile = roomsector[roomnumber].GetYellowTiles()[index + 12];
				//tile->m_lock.unlock();

				tile->m_lock.lock();
				isDeleted = tile->isDeleted();
				tile->m_lock.unlock();
				if (!isDeleted) {
					tile->GetPosition(block.centerx, block.centery, block.centerz);
					if (Check_AABBCollision(player, block))
					{
						tile->m_lock.lock();
						tile->SetDeleted(true);
						tile->m_lock.unlock();

						Send_DeleteBlock(user_id, tile);
					}
				}
			}
		}
	}
	//꼬꼬맵 플레이어 움직임에 따른 ai 행동
	else if (curscene == S_GGOGGOMAP)
	{
		roomsector[roomnumber].l_lock.lock();
		bool loading = roomsector[roomnumber].Get_Loading();
		roomsector[roomnumber].l_lock.unlock();

		if (!loading) //미니게임 모두 로딩  했는지
			return;

		add_event(user_id, EVENT_TYPE::NPC_CHICKEN, 10);
	}
	else if (curscene == S_BULLFIGHTMAP)
	{
		roomsector[roomnumber].l_lock.lock();
		bool loading = roomsector[roomnumber].Get_Loading();
		roomsector[roomnumber].l_lock.unlock();

		if (!loading) //미니게임 모두 로딩 했는지
			return;

		roomsector[roomnumber].p_lock.lock();
		bool potC = roomsector[roomnumber].Get_PotionCreate();
		roomsector[roomnumber].p_lock.unlock();

		if (!potC) //맵에 포션이 없다.
		{
			//커진 플레이어와 충돌 
			roomsector[roomnumber].big_lock.lock();
			int bigplayer = roomsector[roomnumber].Get_BiggerPlayer();
			roomsector[roomnumber].big_lock.unlock();

			if (bigplayer < 0 || bigplayer == user_id) //큰 플레이어가 없음. 혹은 나 자신임 
				return;

			clients[user_id].s_lock.lock();
			bool dead = clients[user_id].GetMiniGameDead();
			clients[user_id].s_lock.unlock();

			if (dead) //이미 펀치맞아 죽엇음
				return;

			float px, py, pz;
			clients[user_id].pos_lock.lock();
			clients[user_id].GetPosition(px, py, pz);
			clients[user_id].pos_lock.unlock();

			float bx, by, bz;
			clients[bigplayer].pos_lock.lock();
			clients[bigplayer].GetPosition(bx, by, bz);
			clients[bigplayer].pos_lock.unlock();

			BoundingBox playerbox;
			BoundingBox bigplayerbox;

			playerbox.centerx = px;
			playerbox.centery = py - 0.35f;
			playerbox.centerz = pz;

			playerbox.scalex = PSCALEX;
			playerbox.scaley = PSCALEY * 1.25f;
			playerbox.scalez = PSCALEZ;

			bigplayerbox.centerx = bx;
			bigplayerbox.centery = by - 0.35f;
			bigplayerbox.centerz = bz;
			
			bigplayerbox.scalex = PSCALEX * 2.5f;
			bigplayerbox.scaley = PSCALEY * 1.25f * 2.5f;
			bigplayerbox.scalez = PSCALEZ * 2.5f;

			if (Check_AABBCollision(playerbox, bigplayerbox)) //큰 곰과 충돌
			{
				packet_punch pp;
				pp.id = user_id;
				pp.size = sizeof(packet_punch);
				pp.type = S2C_PUNCH;

				for (int i = 0; i < 3;  ++i) {
					int playerid = roomsector[roomnumber].GetPlayerID(i);

					if (playerid != -1)
					{
						Send_packet(playerid, &pp);
					}
				}

				clients[user_id].s_lock.lock();
				clients[user_id].SetMiniGameDead(true);
				clients[user_id].s_lock.unlock();

				roomsector[roomnumber].s_lock.lock();
				roomsector[roomnumber].AddMinigameDeadNum();
				int deadnum = roomsector[roomnumber].GetMinigameDeadNum();
				roomsector[roomnumber].s_lock.unlock();

				if (deadnum == 2)
				{
					//코인지급
					packet_coin pc;
					pc.type = S2C_MINIGAMENDCOIN;
					pc.size = sizeof(packet_coin);
					pc.id = bigplayer;//승리자
					pc.add = true;

					clients[pc.id].c_lock.lock();
					clients[pc.id].SetCoin(10, pc.add);
					pc.num = clients[pc.id].GetCoin();
					clients[pc.id].c_lock.unlock();

					int roomnumber2 = clients[pc.id].GetRoomIndex();

					for (int i = 0; i < 3; ++i) {
						int playerid = roomsector[roomnumber2].GetPlayerID(i);

						if (playerid != -1) {
							Send_packet(playerid, &pc);
						}
					}

					Rank_Update(user_id);

					packet_minigameend pm;
					pm.size = sizeof(packet_minigameend);
					pm.type = S2C_MINIGAMEEND;
					pm.id = bigplayer; //현재 큰사람이 승리자 
					
					for (int i = 0; i < 3; ++i) {
						int playerid = roomsector[roomnumber].GetPlayerID(i);

						if (playerid != -1)
						{
							Send_packet(playerid, &pm);
						}
					}
				}
			}
		}
		else //맵에 포션이 있다.
		{
			float px, py, pz;
			clients[user_id].pos_lock.lock();
			clients[user_id].GetPosition(px, py, pz);
			clients[user_id].pos_lock.unlock();

			float tx, ty, tz;
			roomsector[roomnumber].p_lock.lock();
			roomsector[roomnumber].Get_PotionPosition(tx, ty, tz);
			roomsector[roomnumber].p_lock.unlock();

			if (Check_NearPlayer(px, py, pz, tx, ty, tz, 2.f)) //포션을 먹음 
			{
				roomsector[roomnumber].p_lock.lock();
				roomsector[roomnumber].Set_BiggerPlayer(user_id);
				roomsector[roomnumber].Set_PotionCreate(false);
				roomsector[roomnumber].p_lock.unlock();

				packet_scale ps;
				ps.size = sizeof(packet_scale);
				ps.type = S2C_BIGGER;
				ps.id = user_id;

				for (int i = 0; i < 3; ++i)
				{
					int playerinroom = roomsector[roomnumber].GetPlayerID(i);

					if (playerinroom != -1)
					{
						Send_packet(playerinroom, &ps);
					}
				}

				add_event(user_id, PLAYER_SMALLER, 10000); //10초 뒤 작아짐
				add_event(user_id, POTION_CREATE, 12000); // 12초 뒤 다시 포션이 생김
			}

		}
	}
}

bool CServer::Check_AABBCollision(const BoundingBox& b1, const BoundingBox& b2)
{
	Vector3 b1Min, b1Max;
	Vector3 b2Min, b2Max;
	//b1
	b1Min.x = b1.centerx - b1.scalex;
	b1Min.y = b1.centery - b1.scaley;
	b1Min.z = b1.centerz - b1.scalez;

	b1Max.x = b1.centerx + b1.scalex;
	b1Max.y = b1.centery + b1.scaley;
	b1Max.z = b1.centerz + b1.scalez;
	//b2
	b2Min.x = b2.centerx - b2.scalex;
	b2Min.y = b2.centery - b2.scaley;
	b2Min.z = b2.centerz - b2.scalez;

	b2Max.x = b2.centerx + b2.scalex;
	b2Max.y = b2.centery + b2.scaley;
	b2Max.z = b2.centerz + b2.scalez;

	if (max(b1Min.x, b2Min.x) > min(b1Max.x, b2Max.x))
		return false;

	if (max(b1Min.y, b2Min.y) > min(b1Max.y, b2Max.y))
		return false;

	if (max(b1Min.z, b2Min.z) > min(b1Max.z, b2Max.z))
		return false;

	return true;
}

bool CServer::Check_NearPlayer(float x1, float z1, float x2, float z2, float dist)
{
	return sqrt((x1 - x2) * (x1 - x2) + (z1 - z2) * (z1 - z2)) < dist;
}

bool CServer::Check_NearPlayer(float x1, float y1, float z1, float x2, float y2, float z2, float dist)
{
	return  sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2)) < dist;;
}

void CServer::add_event(int id, EVENT_TYPE etype, int delay_ms, int target)
{
	timer event{ id, etype, chrono::system_clock::now() + chrono::milliseconds(delay_ms), target };
	timer_lock.lock();
	timer_queue.push(event);
	timer_lock.unlock();

}

void CServer::do_timer()
{
	using namespace chrono;

	while (true)
	{
		timer_lock.lock();
		if (timer_queue.empty()) {
			timer_lock.unlock();
			continue;
		}

		if (timer_queue.top().exec_time > system_clock::now()) {
			timer_lock.unlock();
			this_thread::sleep_for(10ms);
		}
		else {
			auto ev = timer_queue.top();
			timer_queue.pop();
			timer_lock.unlock();
			switch (ev.e_type)
			{
				case TIME_COUNT_SECONDS:
				{
					int roomnumber = clients[ev.obj_id].GetRoomIndex();

					short secnds = 0;

					packet_timeseconds p;
					p.size = sizeof(packet_timeseconds);
					p.type = S2C_TIMESECONDS;

					roomsector[roomnumber].s_lock.lock();
					int curscene = roomsector[roomnumber].GetCurrentScene();
					roomsector[roomnumber].s_lock.unlock();

					roomsector[roomnumber].t_lock.lock();
					secnds = roomsector[roomnumber].Get_PlayTime();
					roomsector[roomnumber].Set_PlayTime(++secnds);
					roomsector[roomnumber].t_lock.unlock();
					p.seconds = secnds;

					//send time
					for (int i = 0; i < 3; ++i)
					{
						int playerid = roomsector[roomnumber].GetPlayerID(i);
						if (playerid != -1)
						{
							Send_packet(playerid, &p);
						}
					}
					switch (curscene)
					{
						case S_FALLINGMAP:
							break;
						case S_GGOGGOMAP:
						{
							if (secnds == 40)
							{
								//게임 종료
								packet_minigameend p2;
								p2.size = sizeof(packet_minigameend);
								p2.type = S2C_MINIGAMEEND;
								p2.id = -2; //패배 시간초과

								for (int i = 0; i < 3; ++i)
								{
									int playerid = roomsector[roomnumber].GetPlayerID(i);
									if (playerid != -1)
									{
										Send_packet(playerid, &p2);
									}
								}

								roomsector[roomnumber].c_lock.lock();
								roomsector[roomnumber].DeleteChickens();
								roomsector[roomnumber].c_lock.unlock();

								timer_lock.lock();
								while (!timer_queue.empty())
								{
									timer_queue.pop();
								}
								timer_lock.unlock();
								return;//스레드 종료

							}
						}
						break;
						case S_BULLFIGHTMAP:
						{

						}
						break;
						case S_ONEMINDMAP:
						{
							roomsector[roomnumber].light_lock.lock();
							bool lighton = roomsector[roomnumber].Get_Lighton();
							roomsector[roomnumber].light_lock.unlock();

							if (lighton) //불 켜짐
							{
								roomsector[roomnumber].battery_lock.lock();
								char hp = roomsector[roomnumber].Get_Batteryhp();
								roomsector[roomnumber].battery_lock.unlock();

								hp -= 5;
								hp = max(hp, 0);

								roomsector[roomnumber].battery_lock.lock();
								roomsector[roomnumber].Set_Batteryhp(hp);
								roomsector[roomnumber].battery_lock.unlock();

								if ((hp % 30) == 0) // 90(3칸) 60(2칸) 30(1칸) 0
								{
									packet_onemindbattery pb;
									pb.size = sizeof(packet_onemindbattery);
									pb.type = S2C_BATTERY;
									pb.hp = hp;

									for (int i = 0; i < 3; ++i)
									{
										int playerid = roomsector[roomnumber].GetPlayerID(i);
										if(playerid != -1)
										{
											Send_packet(playerid, &pb);
										}
									}
								}
							}
							else //불 꺼짐
							{
								roomsector[roomnumber].battery_lock.lock();
								char hp = roomsector[roomnumber].Get_Batteryhp();
								roomsector[roomnumber].battery_lock.unlock();

								hp += 5;
								hp = min(hp, 90);

								roomsector[roomnumber].battery_lock.lock();
								roomsector[roomnumber].Set_Batteryhp(hp);
								roomsector[roomnumber].battery_lock.unlock();

								if ((hp % 30) == 0) // 90(3칸) 60(2칸) 30(1칸) 0
								{
									packet_onemindbattery pb;
									pb.size = sizeof(packet_onemindbattery);
									pb.type = S2C_BATTERY;
									pb.hp = hp;

									for (int i = 0; i < 3; ++i)
									{
										int playerid = roomsector[roomnumber].GetPlayerID(i);
										if (playerid != -1)
										{
											Send_packet(playerid, &pb);
										}
									}
								}
							}

							if (secnds == 150)
							{
								packet_minigameend p2;
								p2.size = sizeof(packet_minigameend);
								p2.type = S2C_MINIGAMEEND;
								p2.id = -2; //패배 시간초과

								for (int i = 0; i < 3; ++i)
								{
									int playerid = roomsector[roomnumber].GetPlayerID(i);
									if (playerid != -1)
									{
										Send_packet(playerid, &p2);
									}
								}

								timer_lock.lock();
								while (!timer_queue.empty())
								{
									timer_queue.pop();
								}
								timer_lock.unlock();
								return;//스레드 종료
							}
						}
						break;
						default:
							break;
					}
					add_event(ev.obj_id, EVENT_TYPE::TIME_COUNT_SECONDS, 1000);
				}
				break;
				case NPC_CHICKEN:
				{
					int roomnumber = clients[ev.obj_id].GetRoomIndex();
					//ai지정
					float px, py, pz; //플레이어 위치 
					clients[ev.obj_id].pos_lock.lock();
					clients[ev.obj_id].GetPosition(px, py, pz);
					clients[ev.obj_id].pos_lock.unlock();

					roomsector[roomnumber].c_lock.lock();
					CChicken** chicks = roomsector[roomnumber].GetChicken();
					roomsector[roomnumber].c_lock.unlock();

					int escapenum = 0;

					for (int i = 0; i < MAX_CHICKEN; ++i)
					{
						if (chicks[i] == nullptr)
							break;

						int c_state;
						float cx, cy, cz;

						chicks[i]->pos_lock.lock();
						chicks[i]->GetPosition(cx, cy, cz);
						chicks[i]->pos_lock.unlock();

						chicks[i]->c_lock.lock();
						c_state = chicks[i]->GetState();
						chicks[i]->c_lock.unlock();

						if (c_state == C_ESCAPE) {
							++escapenum;
							continue;
						}
						//닭이 해당 플레이어 주변에 있는 지 검사
						if (Check_NearPlayer(px, pz, cx, cz)) //주변에 있다.
						{
							float lookx = cx - px;
							float looky = 0.f;
							float lookz = cz - pz;

							float length = sqrt(lookx * lookx + looky * looky + lookz * lookz);

							float normalx = lookx / length;
							float normaly = looky / length;
							float normalz = lookz / length;

							float resultposx = cx + normalx * 0.04f * 5.f * CHICKENSPEED;
							float resultposy = cy + normaly * 0.04f * 5.f * CHICKENSPEED;
							float resultposz = cz + normalz * 0.04f * 5.f * CHICKENSPEED;


							if (resultposx >= 13.5f)
								resultposx = 13.4f;
							else if (resultposx <= -13.5f)
								resultposx = -13.4f;

							if (resultposz >= 7.5f) {
								if (resultposx <= -4.3f || resultposx >= 4.8f) {
									resultposz = 7.4f;
								}
							}
							else if (resultposz <= -8.5f) {
								resultposz = -8.4f;
							}

							//구석
							if (resultposx >= 13.4f && resultposz >= 7.4f) //우측상단
							{
								if (px < cx) //플레이어가 좌측에 있다 -> 밑으로
								{
									normalx = 0.f;
									normaly = 0.f;
									normalz = -1.f;
								}
								else //플레이어가 우측에 있다. -> 좌측으로
								{
									normalx = -1.f;
									normaly = 0.f;
									normalz = 0.f;
								}

								resultposx = cx + normalx * 0.04f * 5.f * CHICKENSPEED;
								resultposy = cy + normaly * 0.04f * 5.f * CHICKENSPEED;
								resultposz = cz + normalz * 0.04f * 5.f * CHICKENSPEED;
							}
							else if (resultposx <= -13.4f && resultposz >= 7.4f) //좌측상단
							{
								if (px > cx) //플레이어가 우측에 있다 -> 밑으로
								{
									normalx = 0.f;
									normaly = 0.f;
									normalz = -1.f;
								}
								else //플레이어가 좌측에 있다. -> 우측으로
								{
									normalx = 1.f;
									normaly = 0.f;
									normalz = 0.f;
								}

								resultposx = cx + normalx * 0.04f * 5.f * CHICKENSPEED;
								resultposy = cy + normaly * 0.04f * 5.f * CHICKENSPEED;
								resultposz = cz + normalz * 0.04f * 5.f * CHICKENSPEED;
							}
							else if (resultposx <= -13.4f && resultposz <= -8.4f) //좌측하단
							{
								if (px < cx) //플레이어가 좌측에 있다 -> 우측으로
								{
									normalx = 1.f;
									normaly = 0.f;
									normalz = 0.f;
								}
								else //플레이어가 우측에 있다. -> 위로
								{
									normalx = 0.f;
									normaly = 0.f;
									normalz = 1.f;
								}

								resultposx = cx + normalx * 0.04f * 5.f * CHICKENSPEED;
								resultposy = cy + normaly * 0.04f * 5.f * CHICKENSPEED;
								resultposz = cz + normalz * 0.04f * 5.f * CHICKENSPEED;
							}
							else if (resultposx >= 13.4f && resultposz <= -8.4f) //우측하단
							{
								if (px > cx) //플레이어가 우측에 있다 -> 좌측으로
								{
									normalx = -1.f;
									normaly = 0.f;
									normalz = 0.f;
								}
								else //플레이어가 좌측에 있다. -> 위로
								{
									normalx = 0.f;
									normaly = 0.f;
									normalz = 1.f;
								}

								resultposx = cx + normalx * 0.04f * 5.f * CHICKENSPEED;
								resultposy = cy + normaly * 0.04f * 5.f * CHICKENSPEED;
								resultposz = cz + normalz * 0.04f * 5.f * CHICKENSPEED;
							}

							c_state = C_RUN;
							if (cx > -4.1f && cx < 4.6f) {
								if (cz >= 7.4f)
								{
									c_state = C_ESCAPE;
									normalx = 0.f;
									normaly = 0.f;
									normalz = 1.f;

									resultposx = cx + normalx * 0.04f * 32.f * CHICKENSPEED;
									resultposy = cy + normaly * 0.04f * 32.f * CHICKENSPEED;
									resultposz = cz + normalz * 0.04f * 32.f * CHICKENSPEED;
								}
							}

							chicks[i]->c_lock.lock();
							chicks[i]->SetState(c_state);
							chicks[i]->c_lock.unlock();

							chicks[i]->pos_lock.lock();
							chicks[i]->SetPosition(resultposx, resultposy, resultposz);
							chicks[i]->SetLookDirection(normalx, normaly, normalz);
							chicks[i]->pos_lock.unlock();

							packet_chicken pchick;
							pchick.c_id = i;
							pchick.size = sizeof(packet_chicken);
							pchick.type = S2C_CHICKEN;
							pchick.x = resultposx;
							pchick.y = resultposy;
							pchick.z = resultposz;
							pchick.lookx = normalx;
							pchick.looky = normaly;
							pchick.lookz = normalz;
							pchick.state = c_state;

							//닭 상태 보내기
							for (int j = 0; j < 3; ++j)
							{
								int playerinroom = roomsector[roomnumber].GetPlayerID(j);

								if (playerinroom != -1)
								{
									Send_packet(playerinroom, &pchick);
								}
							}

							chicks[i]->c_lock.lock();
							chicks[i]->stopmove = true;
							chicks[i]->c_lock.unlock();
						}
					}
					if (escapenum == MAX_CHICKEN)//모두 탈출
					{
						//코인지급
						packet_coin pc;
						pc.type = S2C_MINIGAMENDCOIN;
						pc.size = sizeof(packet_coin);
						pc.add = true;

						for (int i = 0; i < 3; ++i)
						{
							int playerid = roomsector[roomnumber].GetPlayerID(i);
							if (playerid != -1)
							{
								pc.id = playerid;
								clients[playerid].c_lock.lock();
								clients[playerid].SetCoin(10, pc.add);
								pc.num = clients[pc.id].GetCoin();
								clients[playerid].c_lock.unlock();

								for (int j = 0; j < 3; ++j) { //승리자 코인 보내기
									int playerid2 = roomsector[roomnumber].GetPlayerID(j);
									if (playerid2 != -1)
									{
										Send_packet(playerid2, &pc);
									}
								}
							}
						}

						Rank_Update(ev.obj_id);

						//게임 종료
						packet_minigameend p2;
						p2.size = sizeof(packet_minigameend);
						p2.type = S2C_MINIGAMEEND;
						p2.id = -3; //전체 승리

						for (int i = 0; i < 3; ++i)
						{
							int playerid = roomsector[roomnumber].GetPlayerID(i);
							if (playerid != -1)
							{
								Send_packet(playerid, &p2);
							}
						}

						roomsector[roomnumber].c_lock.lock();
						roomsector[roomnumber].DeleteChickens();
						roomsector[roomnumber].c_lock.unlock();

						timer_lock.lock();
						while (!timer_queue.empty())
						{
							timer_queue.pop();
						}
						timer_lock.unlock();
						return;//스레드 종료
					}

				}
				break;
				case NPC_RANDOMMOVE:
				{
					int roomnumber = clients[ev.obj_id].GetRoomIndex();

					roomsector[roomnumber].r_lock.lock();
					CChicken* chicks = roomsector[roomnumber].GetChicken()[ev.target_id];
					roomsector[roomnumber].r_lock.unlock();

					float cx, cy, cz;

					chicks->c_lock.lock();
					int c_state = chicks->GetState();
					chicks->c_lock.unlock();

					chicks->pos_lock.lock();
					chicks->GetPosition(cx, cy, cz);
					chicks->pos_lock.unlock();

					if (c_state == C_ESCAPE)
						break;

					float px, py, pz; //플레이어 위치 
					clients[ev.obj_id].pos_lock.lock();
					clients[ev.obj_id].GetPosition(px, py, pz);
					clients[ev.obj_id].pos_lock.unlock();

					if (Check_NearPlayer(px, pz, cx, cz)) //주변에 있다.
						break;

					float lookx = float(rand() % 101) / 100.f;
					float looky = 0.f;
					float lookz = float(rand() % 101) / 100.f;

					float length = sqrt(lookx * lookx + looky * looky + lookz * lookz);

					float normalx = lookx / length;
					float normaly = looky / length;
					float normalz = lookz / length;

					float resultposx = cx + normalx * 0.04f * 10.f * CHICKENSPEED;
					float resultposy = cy + normaly * 0.04f * 10.f * CHICKENSPEED;
					float resultposz = cz + normalz * 0.04f * 10.f * CHICKENSPEED;

					if (resultposx >= 13.5f)
						resultposx = 13.4f;
					else if (resultposx <= -13.5f)
						resultposx = -13.4f;

					if (resultposz >= 7.5f) {
						if (resultposx <= -4.1f || resultposx >= 4.6f) {
							resultposz = 7.4f;
						}
					}
					else if (resultposz <= -8.5f) {
						resultposz = -8.4f;
					}

					packet_chicken pchick;
					pchick.c_id = ev.target_id;
					pchick.size = sizeof(packet_chicken);
					pchick.type = S2C_CHICKEN;
					pchick.x = resultposx;
					pchick.y = resultposy;
					pchick.z = resultposz;
					pchick.lookx = normalx;
					pchick.looky = normaly;
					pchick.lookz = normalz;
					pchick.state = C_RUN;

					//닭 상태 보내기
					for (int j = 0; j < 3; ++j)
					{
						int playerinroom = roomsector[roomnumber].GetPlayerID(j);

						if (playerinroom != -1)
						{
							Send_packet(playerinroom, &pchick);
						}
					}
					
					chicks->r_lock.lock();
					chicks->SetRandomMoved(false);
					chicks->r_lock.unlock();
				}
				break;
				case NPC_ISSTOP:
				{
					int roomnumber = clients[ev.obj_id].GetRoomIndex();

					roomsector[roomnumber].r_lock.lock();
					CChicken* chicks = roomsector[roomnumber].GetChicken()[ev.target_id];
					roomsector[roomnumber].r_lock.unlock();

					chicks->c_lock.lock();
					int stopmove = chicks->stopmove;
					int c_state = chicks->GetState();
					chicks->c_lock.unlock();
					
					if (c_state == C_ESCAPE)
						break;

					if (stopmove)
					{
						chicks->c_lock.lock();
						chicks->stopmove = false;
						chicks->c_lock.unlock();
						add_event(ev.obj_id, EVENT_TYPE::NPC_ISSTOP, 1500, ev.target_id);
					}

					else
					{
						chicks->c_lock.lock();
						chicks->stopmove = true;
						chicks->c_lock.unlock();	

						add_event(ev.obj_id, EVENT_TYPE::NPC_RANDOMMOVE, 500 + rand() % 500, ev.target_id);
						add_event(ev.obj_id, EVENT_TYPE::NPC_ISSTOP, 1500, ev.target_id);

					}
				}
				break;
				case POTION_CREATE:
				{
					int roomnumber = clients[ev.obj_id].GetRoomIndex();

					//-35 ~ 35
					//-20 ~ 20
	
					float x, y, z;
					y = 0.0f;
					x = -35.f + float(rand() % 70); 
					z = -20 + float(rand() % 40);

					roomsector[roomnumber].p_lock.lock();
					roomsector[roomnumber].Set_PotionPosition(x, y, z);
					roomsector[roomnumber].Set_PotionCreate(true);
					roomsector[roomnumber].p_lock.unlock();

					packet_potion p;
					p.size = sizeof(packet_potion);
					p.type = S2C_POTION;
					p.x = x;
					p.y = y;
					p.z = z;
					
					//포션 생성
					for (int j = 0; j < 3; ++j)
					{
						int playerinroom = roomsector[roomnumber].GetPlayerID(j);

						if (playerinroom != -1)
						{
							Send_packet(playerinroom, &p);
						}
					}
				}
				break;
				case PLAYER_SMALLER:
				{
					int roomnumber = clients[ev.obj_id].GetRoomIndex();

					roomsector[roomnumber].p_lock.lock();
					roomsector[roomnumber].Set_BiggerPlayer(-1);
					roomsector[roomnumber].p_lock.unlock();

					packet_scale p;
					p.size = sizeof(packet_potion);
					p.type = S2C_SMALLER;
					p.id = ev.obj_id;

					//플레이어 작아지게 하기 
					for (int j = 0; j < 3; ++j)
					{
						int playerinroom = roomsector[roomnumber].GetPlayerID(j);

						if (playerinroom != -1)
						{
							Send_packet(playerinroom, &p);
						}
					}

				}
				break;
				case EVENT_END:
				{
					timer_lock.lock();
					while (!timer_queue.empty())
					{
						timer_queue.pop();
					}
					timer_lock.unlock();

					return;//스레드 종료
				}
				break;
				default:
					break;
			}
		}
	}
}

void CServer::startingtimer(int id,EVENT_TYPE etype,int delay_ms, int target)
{
	timer event{ id, etype, chrono::system_clock::now() + chrono::milliseconds(delay_ms), target };
	starttimer_lock.lock();
	starttimer_queue.push(event);
	starttimer_lock.unlock();
}

void CServer::do_Starttimer()
{
	using namespace chrono;

	while (true)
	{
		starttimer_lock.lock();
		if (starttimer_queue.empty()) {
			starttimer_lock.unlock();
			continue;
		}

		if (starttimer_queue.top().exec_time > system_clock::now()) {
			starttimer_lock.unlock();
			this_thread::sleep_for(10ms);
		}
		else {
			auto ev = starttimer_queue.top();
			starttimer_queue.pop();
			starttimer_lock.unlock();
			switch (ev.e_type)
			{
				case TIME_COUNT_SECONDS:
				{
					int roomnumber= clients[ev.obj_id].GetRoomIndex();

					roomsector[roomnumber].st_lock.lock();
					short time = roomsector[roomnumber].Get_StartTime(); 
					roomsector[roomnumber].Set_StartTime(--time); // 3 2 1 0/
					roomsector[roomnumber].st_lock.unlock();

					packet_starttimer ps;
					ps.size = sizeof(packet_starttimer);
					ps.type = S2C_STARTTIMER;
					ps.time = time;

					for (int i = 0; i < 3; ++i)
					{
						int playerid = roomsector[roomnumber].GetPlayerID(i);
						if (playerid != -1)
						{
							Send_packet(playerid, &ps);
						}
					}

					if (time == 0)
					{
						roomsector[roomnumber].st_lock.lock();
						roomsector[roomnumber].Set_StartTime(4);
						roomsector[roomnumber].st_lock.unlock();

						starttimer_lock.lock();
						while (!starttimer_queue.empty())
						{
							starttimer_queue.pop();
						}
						starttimer_lock.unlock();
						return;
					}

					startingtimer(ev.obj_id, EVENT_TYPE::TIME_COUNT_SECONDS, 1000);
				}
				break;
			}
		}
	}
}

void CServer::add_matching(int id)
{
	waiting_player p{ id, chrono::system_clock::now() };
	matching_lock.lock();
	waiting_player_queue.push(p);
	matching_lock.unlock();
}

void CServer::do_matching()
{
	using namespace chrono;

	while (true)
	{
		matching_lock.lock();
		if (waiting_player_queue.empty()) {
			matching_lock.unlock();
			this_thread::sleep_for(10ms);
			continue;
		}

		if (waiting_player_queue.size() < 3) {
			matching_lock.unlock();
			this_thread::sleep_for(10ms);
		}

		else {
			//매칭 신청한 플레이어가 3명이상이 됨 -> 3명씩 위에서부터(가장 매칭을 오래기다린 사람부터)
			matching_lock.unlock();

			waiting_player player[3];
			PLAYERSTATUS st = PLAYERSTATUS::PLAYER_END;

			for (int i = 0; i < 3; ++i) {
				matching_lock.lock();
				player[i] = waiting_player_queue.top();
				matching_lock.unlock();

				//만약 그 사람이 매칭을 취소한 사람 혹은 나간 사람이면 빠져나가기
				clients[player[i].player_id].c_lock.lock();
				st = clients[player[i].player_id].GetStatus();
				clients[player[i].player_id].c_lock.unlock();

				if (st == PLAYERSTATUS::PLAYER_WAIT) { // 나간 사람 큐에서 삭제 후 다시 3명모였는 지 검사
					matching_lock.lock();
					waiting_player_queue.pop();
					matching_lock.unlock();
					break;
				}
			}

			if (st == PLAYERSTATUS::PLAYER_WAIT)
				continue;

			else if (st == PLAYERSTATUS::PLAYER_MATCHING)//세명 다 매칭 중 나가지 않았음
			{
				matching_lock.lock();
				for (int i = 0; i < 3; ++i) {
					player[i] = waiting_player_queue.top();
					waiting_player_queue.pop();
				}
				matching_lock.unlock();
			}
			

			int roomcount = Get_FreeRoomNumber();

			roomsector[roomcount].r_lock.lock();
			roomsector[roomcount].SetRoomNumber(roomcount);
			roomsector[roomcount].SetRoomStatus(ROOMSTATUS::ROOMPLAY);
			roomsector[roomcount].r_lock.unlock();

			for (int i = 0; i < 3; ++i)
			{
				roomsector[roomcount].r_lock.lock();
				roomsector[roomcount].SetPlayerID(player[i].player_id);
				roomsector[roomcount].r_lock.unlock();

				clients[player[i].player_id].c_lock.lock();
				clients[player[i].player_id].SetStatus(PLAYERSTATUS::PLAYER_GAME);
				clients[player[i].player_id].SetRoomIndex(roomcount);
				clients[player[i].player_id].Init_PlayerInfo();
				clients[player[i].player_id].c_lock.unlock();

				//매칭 성공 패킷 보내기 ->클라에서 보드맵으로 씬 이동
				packet_matching pm;
				pm.type = S2C_MATCHING;
				pm.size = sizeof(packet_matching);
				pm.id = player[i].player_id;
				pm.skinid = 0;

				Send_packet(player[i].player_id, &pm);

				for (int j = 0; j < 3; ++j)
				{
					if (player[i].player_id == player[j].player_id)
						continue;
					Send_enter_packet(player[i].player_id, player[j].player_id);
				}
			}

			cout << roomcount<< "번 방 매칭 완료" << endl;
		}
	}
}

void CServer::matching_request(int user_id, unsigned char* buf)
{
	packet_matching p = *reinterpret_cast<packet_matching*>(buf);
	clients[user_id].c_lock.lock();
	int status = clients[user_id].GetStatus();
	clients[user_id].Set_Skinid(p.skinid);
	clients[user_id].c_lock.unlock();

	if (status == PLAYERSTATUS::PLAYER_MATCHING || status == PLAYERSTATUS::PLAYER_GAME)
		return;

	cout << clients[user_id].GetName() << "님 매칭 요청" << endl;
	clients[user_id].c_lock.lock();
	clients[user_id].SetStatus(PLAYER_MATCHING);
	clients[user_id].c_lock.unlock();
	add_matching(user_id);
}



void CServer::Player_ReadyStatue(int user_id, int datatype, unsigned char* buf)
{
	//클라이언트가 보드맵에 로딩이 끝나고 도착했음을 보낸 것을 받았음
	//3명 모두 보냈다면 3명의 클라이언트에게 도착했음을 전송
	
	switch (datatype)
	{
	case C2S_BOARDMAPREADY:
	{
		packet_boardmap_ready p = *reinterpret_cast<packet_boardmap_ready*>(buf);

		clients[user_id].c_lock.lock();
		clients[user_id].SetPlayerMap(PLAYERINMAP::BOARDMAP);
		clients[user_id].c_lock.unlock();

		int roomnumber = clients[user_id].GetRoomIndex();
		bool allready = true;

		for (int i = 0; i < 3; ++i) {
			int playerid = roomsector[roomnumber].GetPlayerID(i);
			
			if (playerid == -1)
				return;

			if (clients[playerid].GetPlayerMap() != PLAYERINMAP::BOARDMAP) {
				allready = false;
				return;
			}
		}

		//모두 보드맵에 잘 왔으면
		if (allready)
		{
			Send_player_order_packet(user_id);
		}
	}
		break;
	default:
		break;
	}

}

void CServer::Send_login_packet(int user_id, char login_state)
{
	packet_login_result packet;
	ZeroMemory(&packet, sizeof(packet));

	packet.size = sizeof(packet);
	packet.type = login_state;
	packet.id = user_id;

	Send_packet(user_id, &packet);
}

void CServer::Send_enter_packet(int user_id, int send_id)
{
	packet_enter packet;
	ZeroMemory(&packet, sizeof(packet));

	packet.id = send_id;
	strcpy_s(packet.name, clients[send_id].GetName());
	packet.size = sizeof(packet);
	packet.type = S2C_ENTERPLAYER;
	packet.skinid = clients[send_id].Get_Skinid();
	Send_packet(user_id, &packet);
}

bool CServer::Check_OverlappedName(const char name[])
{
	for (auto& client : clients)
	{
		//닉네임 중복이 되는지 검사. 중복이면 true
		if (!strcmp(client.second.GetName(), name))
			return true;
	}

	return false;
}
