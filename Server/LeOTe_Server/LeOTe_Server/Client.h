#pragma once

#define PYOFFSET 1.3f

#define PSCALEX 0.4f
#define PSCALEY 0.35f
#define PSCALEZ 1.3f

enum PLAYERSTATUS {
	PLAYER_WAIT,PLAYER_MATCHING, PLAYER_GAME, PLAYER_END
};

enum PLAYERINMAP {
	BOARDMAP, MINIGAME1, MINIGAME2, MINIGAME3, MINIGAME4, MINIGAME5, MINIGAME6, 
	MINIGAME7, MINIGAME8 ,MAP_END
};

struct Vector3 {
	float x, y, z;

	static Vector3 CrossProduct(Vector3 a, Vector3 b) {
		Vector3 result;

		result.x = a.y * b.z - a.z * b.y;
		result.y = a.z * b.x - a.x * b.z;
		result.z = a.x * b.y - a.y * b.x;

		return result;
	}

	static Vector3 ScalarProduct(Vector3 a, float b)
	{
		Vector3 result;

		result.x = a.x * b;
		result.y = a.y * b;
		result.z = a.z * b;

		return result;
	}

};

class Client
{
public:
	explicit Client();
	virtual ~Client();

public:
	void Init_PlayerInfo();
	//set
	void SetID(const int& user_id);
	void SetConnect(const bool& c);
	void SetName(const char n[]);
	void SetPrevRecvd(const int& prevrecved);
	void SetStatus(const PLAYERSTATUS& s);
	void SetRoomIndex(const int& index);
	void SetPosition(const float& px, const float& py, const float& pz);
	void SetDirection(const float& px, const float& py, const float& pz);
	void SetPlayerMap(const PLAYERINMAP& m);
	void SetLook(float x, float y, float z);

	void SetButton(int num, bool add = 1);//갯수, add - 증가여부 / 0 - 감소 1- 증가
	void SetCoin(int num,  bool add = 1);//갯수, add - 증가여부 / 0 - 감소 1- 증가
	void SetTestCoin(int num); //테스트용 코인 삽입
	void SetTestButton(int num); //테스트용 단추 삽입
	void SetMiniGameDead(bool d);
	void Set_Skinid(char sid);

	//get
	const int&		GetID();
	const bool&		GetConnected();
	const char*		GetName();
	const int&		GetPrevRecved();
	const PLAYERSTATUS& GetStatus();
	const int		GetRoomIndex();
	const void		GetPosition(float& x, float& y, float& z);
	const void		GetDirection(float& x, float& y, float& z);
	const PLAYERINMAP& GetPlayerMap();
	const void		GetLook(float& x, float& y, float& z);
	
	const int&		GetCoin();
	const int&		GetButton();
	const int&		GetRank();
	const bool& GetMiniGameDead();
	const char& Get_Skinid();

public:
	//network value
	EX_OVER			m_recv_over;
	unsigned char	m_prev_recv;

	mutex			c_lock; //기본 셋팅
	mutex			s_lock; //상태 lock
	mutex			pos_lock; //위치, 룩 set lock, 

	SOCKET			socket;

private:
	int				id;							//고유 아이디
	char			skinid;						//플레이어 스킨 아이디
	int				roomindex;					//들어가 있는 방 번호
	bool			connected;					//게임 접속 여부
	PLAYERSTATUS	status;
	char			name[MAX_NAME_LEN];
	float			x;
	float			y;
	float			z;
	float			dx;
	float			dy;
	float			dz;
	int				coin;
	int				button;
	int				rank;
	//애니메이션 변수 추가하기

	PLAYERINMAP		map;
	bool			minigamedead = false;
};

