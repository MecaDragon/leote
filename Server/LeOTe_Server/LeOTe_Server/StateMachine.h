#pragma once
#include "State.h"

class CStateMachine final
{
private:
	explicit CStateMachine() = default;
public:
	virtual ~CStateMachine() = default;

public:
	HRESULT Ready_StateMachine(CIFSMUsable* pOwner);

public:
	CState* GetPrevState() { return m_pPrevState; }
	CState* GetCurrentState() { return m_pCurState; }
	CState* GetNextState() { return m_pNextState; }

public:
	short Update();
	HRESULT ChangeState(CState* pState);
	HRESULT SetCurrentState(CState* pState);

private:
	CState* m_pPrevState = nullptr;
	CState* m_pNextState = nullptr;
	CState* m_pCurState = nullptr;
	CIFSMUsable* m_pOwner = nullptr;

public:
	static CStateMachine* Create(CIFSMUsable* pOwner);
	virtual void Free();
};
