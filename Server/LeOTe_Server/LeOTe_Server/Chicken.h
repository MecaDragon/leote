#pragma once

#include "IFSMUsable.h"
#include"StateMachine.h"
#include "GameObjectState.h"

#define CHICKENSCALE 0.4f;

class CChicken : public CIFSMUsable 
{
public:
	explicit CChicken();
	virtual ~CChicken();

	//set
	void SetPosition(float px, float py, float pz);
	void SetLookDirection(float x, float y, float z);
	//void SetInitState(CState* pState);
	void SetState(char s);
	void SetInitAngle(float angle);
	void SetRandomMoved(bool rand) { isRandommoved = rand; }

	//get
	void GetPosition(float& px, float& py, float& pz);
	void GetLookDirection(float& x, float& y, float& z);
	const float& GetAngle();
	const char& GetState();
	const bool& GetRandomMoved() {	return isRandommoved; }

	//state
	//CState* Get_CurrentState();
	//HRESULT Change_State(CState* pState);

	//HRESULT Ready_StateMachine(CState* pState);

	void Initialize();
	void Update();

//public:
//	virtual void Idle_Enter() override;
//	virtual void Idle_Update() override;
//	virtual void Idle_Exit() override;
//
//	virtual void Eat_Enter() override;
//	virtual void Eat_Update() override;
//	virtual void Eat_Exit() override;
//
//	virtual void Nod_Enter() override;
//	virtual void Nod_Update() override;
//	virtual void Nod_Exit() override;
//
//	//run
//	virtual void Run_Enter() override;
//	virtual void Run_Update() override;
//	virtual void Run_Exit() override;
//
//	//escapde
//	virtual void EscapeFence_Enter() override;
//	virtual void EscapeFence_Update() override;
//	virtual void EscapeFence_Exit() override;

public:
	mutex c_lock; //기본 락, 상태 락
	mutex pos_lock; //위치, 룩 락
	mutex r_lock; //무작위 움직임 플래그 락

	bool stopmove = false;

private:
	float x, y, z;
	float lookx, looky, lookz;
	char state = -1;

	bool isRandommoved = false;
	//init look
	float initAngle;
};

