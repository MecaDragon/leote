#include "stdafx.h"
#include "IFSMUsable.h"
#include "State.h"
#include "StateMachine.h"

HRESULT CIFSMUsable::Change_State(CState* pState)
{
	if (m_pState_Machine == nullptr)
		return E_FAIL;

	m_pState_Machine->ChangeState(pState);

	return NOERROR;
}

CState* CIFSMUsable::Get_CurrentState()
{
	if (m_pState_Machine == nullptr)
		return nullptr;

	return m_pState_Machine->GetCurrentState();
}

HRESULT CIFSMUsable::Enqueue_Action(CState* pAction)
{
	m_ActionQueue.push(pAction);

	return NOERROR;
}

HRESULT CIFSMUsable::Clear_ActionQueue()
{
	while (!m_ActionQueue.empty())
		m_ActionQueue.pop();

	return NOERROR;
}

CState* CIFSMUsable::Dequeue_Action()
{
	if (m_ActionQueue.empty())
		return nullptr;

	CState* nextState = m_ActionQueue.front();
	m_ActionQueue.pop();

	return nextState;
}

CState* CIFSMUsable::Get_NextAction()
{
	if (m_ActionQueue.empty())
		return nullptr;

	return m_ActionQueue.front();
}

HRESULT CIFSMUsable::Reset_ActionQueue(double TimeDelta)
{
	m_fQueueTime += (float)TimeDelta;

	if (m_fQueueTime > m_fQueueDuration)
	{
		m_fQueueTime = 0.f;

		if (!m_ActionQueue.empty())
			Clear_ActionQueue();
	}

	return NOERROR;
}