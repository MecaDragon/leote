#include "stdafx.h"
#include "Chicken.h"

CChicken::CChicken()
{
	Initialize();
}

CChicken::~CChicken()
{
}

void CChicken::SetPosition(float px, float py, float pz)
{
	x = px;
	y = py;
	z = pz;
}

void CChicken::SetLookDirection(float x, float y, float z)
{
	lookx = x;
	looky = y;
	lookz = z;
}

//void CChicken::SetInitState(CState* pState)
//{
//	//Change_State(pState);
//}

void CChicken::SetState(char s)
{
	state = s;
}

void CChicken::SetInitAngle(float angle)
{
	initAngle = angle;
}

void CChicken::GetPosition(float& px, float& py, float& pz)
{
	px = x;
	py = y;
	pz = z;
}

void CChicken::GetLookDirection(float& x, float& y, float& z)
{
	x = lookx;
	y = looky;
	z = lookz;
}

const float& CChicken::GetAngle()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return initAngle;
}

const char& CChicken::GetState()
{
	// TODO: 여기에 return 문을 삽입합니다.
	return state;
}

//CState* CChicken::Get_CurrentState()
//{
//	if (nullptr == m_pState_Machine)
//		return nullptr;
//
//	return m_pState_Machine->GetCurrentState();
//}
//
//HRESULT CChicken::Change_State(CState* pState)
//{
//	if (nullptr == m_pState_Machine || nullptr == pState)
//		return E_FAIL;
//
//	m_pState_Machine->ChangeState(pState);
//	return NOERROR;
//}
//
//HRESULT CChicken::Ready_StateMachine(CState* pState)
//{
//	m_pState_Machine = CStateMachine::Create(this);
//	
//	if (m_pState_Machine == nullptr)
//		return E_FAIL;
//
//	m_pState_Machine->SetCurrentState(pState);
//	m_pState_Machine->ChangeState(pState);
//	return NOERROR;
//}

void CChicken::Initialize()
{
	//Ready_StateMachine(Idle::Instance());
}

void CChicken::Update()
{
	m_pState_Machine->Update();
}

//idle
//void CChicken::Idle_Enter()
//{
//}
//
//void CChicken::Idle_Update()
//{
//}
//
//void CChicken::Idle_Exit()
//{
//}
//
//void CChicken::Eat_Enter()
//{
//}
//
//void CChicken::Eat_Update()
//{
//}
//
//void CChicken::Eat_Exit()
//{
//}
//
//void CChicken::Nod_Enter()
//{
//}
//
//void CChicken::Nod_Update()
//{
//}
//
//void CChicken::Nod_Exit()
//{
//}
//
////run
//void CChicken::Run_Enter()
//{
//}
//
//void CChicken::Run_Update()
//{
//}
//
//void CChicken::Run_Exit()
//{
//}
//
////escape
//void CChicken::EscapeFence_Enter()
//{
//}
//
//void CChicken::EscapeFence_Update()
//{
//}
//
//void CChicken::EscapeFence_Exit()
//{
//}
