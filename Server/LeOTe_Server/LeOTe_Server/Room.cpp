#include "stdafx.h"
#include "Room.h"
#include "Tile.h"
#include "Chicken.h"

CRoom::CRoom()
{
	roomnumber = -1;
	for (int i = 0; i < 3; ++i)
		playerid[i] = -1;
	playernum = 0;
	roomstatus = ROOMEND;

	InitTiles();
}

CRoom::~CRoom()
{
	DeleteTiles();
}

void CRoom::SetRoomNumber(const int& num)
{
	roomnumber = num;
}

void CRoom::SetPlayerID(const int& id)
{
	if (playernum >= 3)
	{
		cout << "플레이어 3명 다 찼다" << endl;
		return;

	}

	for (int i = 0; i < 3; ++i) {
		if (playerid[i] == -1) {
			playerid[i] = id;
			break;
		}
	}
	++playernum;
}

void CRoom::SetRoomStatus(ROOMSTATUS status)
{
	roomstatus = status;
}

void CRoom::RoomOut(const int& id)
{
	for (int i = 0; i < 3; ++i)
	{
		if (playerid[i] == id)
			playerid[i] = -1;
	}
	--playernum;
	if (playernum == 0)
		roomstatus = ROOMEND;

	else if (playernum < 3)
		roomstatus = ROOMWAIT;
}

const int& CRoom::GetRoomNumber()
{
	return roomnumber;
}

const int CRoom::GetPlayerID(int index)
{
	return playerid[index];
}

const ROOMSTATUS& CRoom::GetRoomStatus()
{
	return roomstatus;
}

void CRoom::InitTiles()
{
	//falling
	int i = 0;
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < 12; ++x) {
			CTile* tile = new CTile;
			tile->SetPostion(x * 3.f + 1.5f, 15.0f, 33.f - float(y * 3)); // 0, 0/ 3, 0/ 6, 0
			tile->SetTileColor(BLOCK_BLUE);
			tile->SetTileIndex(i);
			BlueTile[i++] = tile;
		}
	}

	i = 0;
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < 12; ++x) {
			CTile* tile = new CTile;
			tile->SetPostion(x * 3.f + 1.5f, 0.0f, 33.f - float(y * 3));
			tile->SetTileColor(BLOCK_RED);
			tile->SetTileIndex(i);
			RedTile[i++] = tile;
		}
	}

	i = 0;
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < 12; ++x) {
			CTile* tile = new CTile;
			tile->SetPostion(x * 3.f + 1.5f, -15.0f, 33.f - float(y * 3));
			tile->SetTileColor(BLOCK_YELLOW);
			tile->SetTileIndex(i);
			YellowTile[i++] = tile;
		}
	}
}

void CRoom::DeleteTiles()
{
	for (int i = 0; i < 144; ++i)
	{
		if (BlueTile[i] != nullptr) {
			delete BlueTile[i];
			BlueTile[i] = nullptr;
		}

		if (RedTile[i] != nullptr) {
			delete RedTile[i];
			RedTile[i] = nullptr;
		}

		if (YellowTile[i] != nullptr) {
			delete YellowTile[i];
			YellowTile[i] = nullptr;
		}
	}
}

void CRoom::InitChickens()
{
	for (int i = 0; i < MAX_CHICKEN; ++i)
	{
		CChicken* chick = new CChicken;
		chick->SetPosition((float)(rand() % 26 - 13), 0.5f, (float)(rand() % 14 - 7));
		chick->SetLookDirection(0.f, 0.f, 1.f);
		chick->SetInitAngle(float(rand() % 360));

		//idle random
		chick->SetState(rand() % 3); //idle:0 eat:1 nod:2

		Chickens[i] = chick;
	}
}

void CRoom::DeleteChickens()
{
	for (int i = 0; i < MAX_CHICKEN; ++i)
	{
		if (Chickens[i] != nullptr) {
			delete Chickens[i];
			Chickens[i] = nullptr;
		}
	}

}

void CRoom::Init_tieDice()
{
	tieDice.clear();
	vector<int>().swap(tieDice);

	for (int i = 0; i < 6; ++i)
		tieDice.push_back(i + 1);

	random_device rd;
	mt19937 g(rd());

	shuffle(tieDice.begin(), tieDice.end(), g);
}
