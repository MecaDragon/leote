#pragma once
#include<iostream>
#include<WS2tcpip.h>
#include<MSWSock.h>
#include<list>
#include<vector>
#include<map>
#include<mutex>
#include <algorithm>
#include <random>
#include<thread>
#include "Protocol.h"
#pragma comment(lib,"WS2_32.lib")
#pragma comment(lib, "mswsock.lib")
using namespace std;

#include<queue>

enum OP_TYPE
{
	OP_RECV, OP_SEND, OP_ACCEPT, OP_CHICKEN ,OP_END
};


//Overlapped 구조체 확장
struct EX_OVER {
	WSAOVERLAPPED	m_over;
	WSABUF			m_wsabuf[1];
	unsigned char	m_netbuf[MAX_BUF_SIZE];
	OP_TYPE			m_op;
	SOCKET			m_csocket;
};