#pragma once
//방의 상태 - 플레이어가 오길 기다리는 중, 플레이어가 모두 옴, 게임이 진행 중, 방이 만들어지지 않음
enum ROOMSTATUS {
	ROOMWAIT, ROOMPLAY, ROOMEND
};
class CTile;
class CChicken;

#define BLUEY 15.f
#define REDY 0.f
#define YELLOWY -15.f

#define POTIONSCALE 4.f
#define MAXBATTERYHP 90
class CRoom
{
public:
	explicit CRoom();
	virtual ~CRoom();

public:
	void SetRoomNumber(const int& num);
	void SetPlayerID(const int& id);
	void SetRoomStatus(ROOMSTATUS status);
	void RoomOut(const int& id);

	const int& GetRoomNumber();
	const int GetPlayerID(int index);
	const ROOMSTATUS& GetRoomStatus();

	const int& GetMinigameDeadNum() { return minigamedeadnum; }
	void SetMinigameDeadNum(int num) { minigamedeadnum = num; }
	void AddMinigameDeadNum() { ++minigamedeadnum; }

	const int& GetSceneChangeReady() { return scenechangeReady; }
	int AddSceneChangeReady() { return ++scenechangeReady; }
	void InitSceneChange() { scenechangeReady = 0; }
	const int& GetCurrentScene() { return currentScene; }
	void SetCurrentScene(int sceneid) { currentScene = sceneid; }

	//falling map
	void InitTiles();
	void DeleteTiles();

	CTile** GetBlueTiles() { return BlueTile; }
	CTile** GetRedTiles() { return RedTile; }
	CTile** GetYellowTiles() { return YellowTile; }

	//ggoggomap
	void InitChickens();
	void DeleteChickens();

	const short& Get_PlayTime() { return minigame_playtime; }
	void Set_PlayTime(short time) { minigame_playtime = time; }

	CChicken** GetChicken() { return Chickens; }

	const bool& Get_Loading() { return minigame_loading; }
	void Set_Loading(bool time) { minigame_loading = time; }

	const short& Get_LoadingCount() { return loading_count; }
	void Set_LoadingCount(short count) { loading_count = count; }
	const short& Add_LoadingCount() { return ++loading_count; }

	//bullfight
	void Get_PotionPosition(float& x, float& y, float& z) { x = potionx; y = potiony; z = potionz; }
	void Set_PotionPosition(const float x, const float y, const float z) { potionx = x; potiony = y; potionz = z; }

	void Set_PotionCreate(bool potion) { potionCreated = potion; }
	const bool& Get_PotionCreate() { return potionCreated; }

	void Set_BiggerPlayer(int id) { biggerid = id; }
	const int& Get_BiggerPlayer() { return biggerid; }

	//onemind 
	void Init_RobotPosition() { robotx = 0.f; roboty = 0.65f; robotz = 0.f; }
	void Get_RobotPosition(float& x, float& y, float& z) { x = robotx; y = roboty; z = robotz; }
	void Set_RobotPosition(const float x, const float y, const float z) { robotx = x; roboty = y; robotz = z; }

	void Set_Lighton(bool light) { lighton = light; }
	const bool& Get_Lighton() { return lighton; }

	void Set_Batteryhp(char hp) { batteryhp = hp; }
	const char& Get_Batteryhp() { return batteryhp; }

	void Set_Respawn(char r) { respawn = r; }
	const char& Get_Respawn() { return respawn; }

	void Set_Snowman(bool snow, int index) { snowman[index] = snow; }
	bool Get_Snowman(int index) { return snowman[index]; }

	bool Get_isGoal() { return goal; }
	void Set_isGoal(bool g) { goal = g; }

	//시작시간
	void Set_StartTime(short time) { starttime = time; }
	const short& Get_StartTime() { return starttime; }

	int Add_StartAsk() { return ++startask; }
	void Set_StartAsk(int ask) { startask = ask; }

	int Get_MatchTieDice() { int back = tieDice.back();	tieDice.pop_back();	return back;}

	void Init_tieDice();
public:
	mutex r_lock;//기본 방번호, 플레이어번호락
	mutex c_lock;//닭 가져오기 락

	mutex t_lock; //시간 락
	mutex s_lock; //현재 씬 락
	mutex l_lock; //로딩 락
	mutex p_lock; //포션 락, 로봇 락
	mutex big_lock; //투우맵 큰 로봇 락
	mutex battery_lock; //한마음맵 배터리 락
	mutex light_lock; //한마음맵 조명 락
	mutex respawn_lock; //한 마음맵 리스폰 락
	mutex snowman_lock; //눈사람 락
	mutex goal_lock; //한 마음맵 골인 락
	mutex st_lock;//시작 시간 락
	mutex sask_lock; //시작 요청 락
private:
	ROOMSTATUS roomstatus;
	int playernum = 0;	//플레이어 수
	int roomnumber;		//방번호
	int playerid[3];	//방에있는 플레이어 아이디

	int currentScene = S_LOGO;//현재 씬

	int minigamedeadnum = 0; //죽은 플레이어 수 

	//secene
	int scenechangeReady = 0;

	//fallingmap
	CTile* BlueTile[144];
	CTile* RedTile[144];
	CTile* YellowTile[144];

	//ggoggomap
	CChicken* Chickens[MAX_CHICKEN];
	short loading_count = 0;
	bool minigame_loading = false;
	short minigame_playtime = 0;
	
	//bullfight
	int biggerid = -1; //커진 플레이어 인덱스

	//포션위치
	float potionx = 0.f;
	float potiony = 0.f;
	float potionz = 0.f;

	bool potionCreated = false; //포션 생성 여부

	//onemind 
	float robotx = 0.f;
	float roboty = 0.65f;
	float robotz = 0.f;

	bool lighton = false; //불을 킨 상태
	char batteryhp = 90.f;

	char respawn = 0; //처음 /1번째 /2번째
	bool snowman[2] = { false, false }; //눈사람과 한 번이라도 충돌 했는가
	bool goal = false; //onemind 골인 여부

	//시작 시간
	int startask = 0; //시작 요청 
	short starttime = 4;

	//prize 동점자 주사위
	vector<int> tieDice;
};

