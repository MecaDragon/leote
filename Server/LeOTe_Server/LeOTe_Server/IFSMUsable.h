#pragma once

class CState;
class CStateMachine;

class CIFSMUsable
{
public:
	CIFSMUsable() = default;
	~CIFSMUsable() = default;

public:
	HRESULT			Change_State(CState* pState);
	CState* Get_CurrentState();

protected:
	queue<CState*>	m_ActionQueue;
	CStateMachine* m_pState_Machine = nullptr;
	float			m_fQueueDuration = 0.5f;
	float			m_fQueueTime = 0.f;

protected:
	HRESULT		Enqueue_Action(CState* pAction);
	HRESULT		Clear_ActionQueue();
	CState*		Dequeue_Action();
	CState*		Get_NextAction();
	HRESULT		Reset_ActionQueue(double TimeDelta);

public:
	//idle
	virtual void Idle_Enter() {};
	virtual void Idle_Update() {};
	virtual void Idle_Exit() {};

	virtual void Eat_Enter() {};
	virtual void Eat_Update() {};
	virtual void Eat_Exit() {};

	virtual void Nod_Enter() {};
	virtual void Nod_Update() {};
	virtual void Nod_Exit() {};

	//run
	virtual void Run_Enter() {};
	virtual void Run_Update() {};
	virtual void Run_Exit() {};

	//escapde
	virtual void EscapeFence_Enter() {};
	virtual void EscapeFence_Update() {};
	virtual void EscapeFence_Exit() {};
};

