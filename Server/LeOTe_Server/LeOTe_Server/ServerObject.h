#ifndef __SERVER_H__
#define __SERVER_H__
#include "Defines.h"

class CTile;
class Client;
class CRoom;

enum EVENT_TYPE {
	TIME_COUNT_SECONDS, NPC_ISSTOP, NPC_RANDOMMOVE, NPC_CHICKEN, POTION_CREATE, PLAYER_SMALLER, EVENT_END
};

struct timer {
	int obj_id;
	EVENT_TYPE e_type;
	chrono::system_clock::time_point exec_time; //시간순으로 정렬될 예정
	int target_id; 

	constexpr bool operator < (const timer& l) const
	{
		return exec_time > l.exec_time;
	}
};

struct waiting_player {
	int player_id;
	chrono::system_clock::time_point exec_time; //접속 순으로 정렬

	constexpr bool operator < (const waiting_player& p) const {
		return exec_time > p.exec_time;
	 }
};
 
class CServer final
{
private:
	CServer(const CServer& obj);
	CServer& operator=(const CServer& obj);
private:
	CServer();
	~CServer();
public:
	static CServer* GetInstance()
	{
		if (nullptr == m_pInstance)
		{
			m_pInstance = new CServer;
		}
		return m_pInstance;
	}
	void DestroyInstance()
	{
		if (m_pInstance)
		{
			delete m_pInstance;
			m_pInstance = nullptr;
		}
	}

public:
	void Initialize_clients();

	//server function
	void Server_Initialize();
	static void Server_Update();

	//send
	static void Send_packet(int user_id, void* data);

	//recv
	static void Process_packet(int user_id, unsigned char*);
	static void do_recv(int p_id);
	//leave
	static void Send_leave_packet(int user_id, int left_id);
	static void Disconnect(int user_id);

private:
	static void Enter_game(int user_id, char name[]);
	static int Get_FreeRoomNumber();

	//recv
	//static void Create_Room(int user_id, unsigned char*);
	//static void Enter_Room(int user_id, unsigned char*);

	static void Move_Player(int user_id, unsigned char*);

	static void Player_ReadyStatue(int user_id, int datatype, unsigned char*);

	//send
	static void Send_login_packet(int user_id, char login_state); 
	static void Send_enter_packet(int user_id, int send_id);//보내질 아이디, 보낼 아이디
	static bool Check_OverlappedName(const char name[]);

	static void Send_player_order_packet(int user_id);//boardmap - order

	static void Send_DiceNumandJumpState(int user_id, unsigned char*);

	static void Send_DiceMoveEnd(int user_id, unsigned char*);

	static void Send_CurrentOrder(int user_id, unsigned char*);

	static void Send_DiceEnter(int user_id, unsigned char*);

	static void Send_CoinEvent(int user_id, unsigned char*);

	static void Send_ButtonEvent(int user_id, unsigned char*);

	static void Send_DiceNumDown(int user_id, unsigned char*);

	static void Send_PlayerCollision(int user_id, unsigned char*);

	static void Send_PlayerAnimstate(int user_id, unsigned char*);

	static void Init_SceneChange(int user_id, unsigned char*);

	static void Send_DeleteBlock(int user_id, CTile* tile);

	static void Send_PlayerDead(int user_id, unsigned char*);

	static void Send_MinigameLoading(int user_id, unsigned char*);

	//onemind
	static void OneMind_MoveDirection(int user_id, unsigned char*);
	static void OneMind_Move(int user_id, unsigned char*);
	static void OneMind_Light(int user_id, unsigned char*);
	static void OneMind_Anim(int user_id, unsigned char*);

	//check
	static bool Check_AABBCollision(const BoundingBox& b1, const BoundingBox& b2);
	static bool Check_NearPlayer(float x1,float z1, float x2, float z2, float dist = 3.4f);
	static bool Check_NearPlayer(float x1, float y1, float z1, float x2, float y2, float z2, float dist = 3.4f);

	//timer
	static void add_event(int id, EVENT_TYPE etype, int delay_ms, int target = 0);
	static void do_timer();
	
	static void startingtimer(int id, EVENT_TYPE etype, int delay_ms, int target = 0);
	static void do_Starttimer();

	//matching
	static void add_matching(int id);
	static void do_matching();
	
	static void matching_request(int user_id, unsigned char* buf);

	//rank
	static void Rank_Update(int user_id);
	//test tie
	static void TestTie_Rank(int user_id, unsigned char* buf);

	//showmap
	static void Show_Map(int user_id, unsigned char* buf);

	//start
	static void Start_Ask(int user_id, unsigned char* buf); //3 초 세기 요청

	//matchdie 동점자 주사위 요청
	static void MatchTie(int user_id, unsigned char* buf); //동점자 주사위 요청

	//event
	static void Boardmap_Event(int user_id, unsigned char* buf);// 보드맵 이벤트
	static void Boardmap_EventSelect(int user_id, unsigned char* buf);// 보드맵 이벤트 선택 여부

	//item
	static void Boardmap_ItemUse(int user_id, unsigned char* buf); //아이템 사용
	
	//spacialevent
	static void Boardmap_SpacialEvent(int user_id, unsigned char* buf); //상점, 단추 X 이벤트함정발판
	static void SpecialEvent_Teleport(int user_id, unsigned char* buf); //보드맵 이벤트 텔레포트
private:
	static CServer* m_pInstance;

private:
	static SOCKET listen_socket;	//listen 소켓
	static HANDLE Iocp;				//iocp 핸들
	
	static map<int, CRoom> roomsector;		//대기실 방 번호 및 플레이어 아이디 관리 객체
	static map<int, Client> clients;		//클라이언트 아이디, 클라이언트 객체들

	//Accept용 객체들
	static EX_OVER accept_overlapped;

	//test
	static bool waitroom; //대기실 생략 여부 false 시 생략	

	//timer queue - AI
	static priority_queue<timer> timer_queue;
	static mutex timer_lock;

	static thread time_thread;

	static priority_queue<waiting_player> waiting_player_queue;
	static mutex matching_lock;

	//start timer - 3초 세기 
	static priority_queue<timer> starttimer_queue; 
	static mutex starttimer_lock;
};

#endif