#pragma once

#include "IFSMUsable.h"

class CState
{
public:
	CState() = default;
	virtual ~CState() = default;

public:
	virtual void Enter(CIFSMUsable*) = 0;
	virtual void Update(CIFSMUsable*) = 0;
	virtual void Exit(CIFSMUsable*) = 0;
};