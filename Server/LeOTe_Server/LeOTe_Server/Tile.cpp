#include "stdafx.h"
#include "Tile.h"

CTile::CTile()
{
    tileindex = -1;
    tilecolor = -1;
    x = 0.f;
    y = 0.f;
    z = 0.f;
}

CTile::CTile(const CTile& tile)
{
    tileindex = tile.tileindex;
    tilecolor = tile.tilecolor;
    x = tile.x;
    y = tile.y;
    z = tile.z;
}

CTile::~CTile()
{
}