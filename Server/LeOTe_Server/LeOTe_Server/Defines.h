#pragma once
struct BoundingBox
{
	float centerx, centery, centerz;
	float scalex, scaley, scalez;
};
#define NO_COPY_STATE(STATENAME)								\
private:														\
	STATENAME() = default;										\
	STATENAME& operator=(const STATENAME&) = default;			\
	STATENAME(const STATENAME&) = default;						

#define DECLARE_STATE(STATENAME, OWNER)							\
class STATENAME : public CState									\
{																\
public:															\
	static	STATENAME* Instance();								\
	void	Enter(OWNER*); 										\
	void	Update(OWNER*);										\
	void	Exit(OWNER*); 										\
NO_COPY_STATE(STATENAME)										\
};

#define IMPLEMENT_STATE(STATENAME)								\
STATENAME* STATENAME::Instance()								\
{																\
	static STATENAME instance;									\
	return &instance;											\
}