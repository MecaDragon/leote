#include "stdafx.h"
#include "GameObject.h"

CGameObject::CGameObject()
{
}

CGameObject::~CGameObject()
{
}

void CGameObject::UpdateRect()
{
	m_Rect.left = LONG(m_Info.fx - (LONG(m_Info.fsizex) >> 1));
	m_Rect.top = LONG(m_Info.fy - (LONG(m_Info.fsizey) >> 1));
	m_Rect.right = LONG(m_Info.fx + (LONG(m_Info.fsizex) >> 1));
	m_Rect.bottom = LONG(m_Info.fy + (LONG(m_Info.fsizey) >> 1));
}
