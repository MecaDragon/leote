#include "stdafx.h"
#include "MainProg.h"
#include "TimerManager.h"
#include "SceneManager.h"
#include "ServerManager.h"

CMainProg::CMainProg()
{
}

CMainProg::~CMainProg()
{
}

void CMainProg::Initialize()
{
	m_hDC = GetDC(g_hWnd);

	CServerManager::GetInstance()->Init_Connect();
	CSceneManager::GetInstance()->SceneChange(SCENEID::SCENE_MENU);
}

void CMainProg::Update(double DeltaTime)
{
	CSceneManager::GetInstance()->Update(DeltaTime);
}

void CMainProg::LateUpdate(double DeltaTime)
{
	CSceneManager::GetInstance()->LateUpdate(DeltaTime);
	CServerManager::GetInstance()->Recv_Packet();
}

void CMainProg::Render()
{
	CSceneManager::GetInstance()->Render(m_hDC);

	//FPS ���
	++iFps;

	if (1.f <= m_fTimeCount)
	{
		swprintf_s(m_szFps, L"Diepo_FPS: %d", iFps);
		iFps = 0;
		m_fTimeCount = 0.f;
	}
	SetWindowText(g_hWnd, m_szFps);
}

void CMainProg::Release()
{
	CSceneManager::GetInstance()->DestroyInstance();
	CServerManager::GetInstance()->DestroyInstance();

	ReleaseDC(g_hWnd, m_hDC);
}
