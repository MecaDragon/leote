#ifndef __SERVERMGR__
#define __SERVERMGR__
#define _CRT_SECURE_NO_WARNINGS         // 최신 VC++ 컴파일 시 경고 방지
#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#include<WS2tcpip.h>
#include<MSWSock.h>
#pragma comment(lib,"WS2_32.lib")
#pragma comment(lib, "mswsock.lib")

#include<thread>
#include "Define.h"

#include "../../LeOTe_Server/LeOTe_Server/Protocol.h"

#define SERVER_IP "127.0.0.1"

class CServerManager
{
	DECLARE_SINGLETON(CServerManager);
private:
	CServerManager();
	~CServerManager();

public:
	//server
	void Init_Connect();

	const char* GetName() { return name; }

	void Send_Packet(int pType, void* packet);
	void Recv_Packet();
	void Process_Packet(char* buf);

	void err_quit(const WCHAR* msg);
	void err_display(const WCHAR* msg);

	void Send_Name();

	//get
	const int& GetServerIndex() { return id; }
	//set
private:


private:
	static int id;
	char name[MAX_NAME_LEN];
	static SOCKET sock;
	SOCKADDR_IN serveraddr;
};

#endif // !__SERVERMGR__
