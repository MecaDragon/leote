#include "stdafx.h"
#include "ServerManager.h"
#include "SceneManager.h"
IMPLEMENT_SINGLETON(CServerManager);
SOCKET CServerManager::sock;
int CServerManager::id;

CServerManager::CServerManager()
{
}

CServerManager::~CServerManager()
{
}

void CServerManager::Init_Connect()
{
	cout << "닉네임 입력: ";
	cin >> name;

	WSADATA wsa;
	if (WSAStartup(MAKEWORD(1, 1), &wsa) != 0)
		return;

	sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, 0);
	if (sock == INVALID_SOCKET)
		err_quit(L"socket()");

	unsigned long noblock = 1;
	int nRet = ioctlsocket(sock, FIONBIO, &noblock);

	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	//serveraddr.sin_addr.s_addr = inet_addr(serverip);
	serveraddr.sin_port = htons(SERVER_PORT);
	inet_pton(AF_INET, SERVER_IP, &serveraddr.sin_addr);

	int retval = 0;
	retval = connect(sock, (SOCKADDR*)&serveraddr, sizeof(serveraddr));

	//0반환시 연결 성공 그러나..
	if (retval != 0)
	{
		int err = WSAGetLastError(); //가장 최근에 발생한 소켓 함수 에러를 알려줌
		if (err != EINPROGRESS && err != EWOULDBLOCK && err != WSAEWOULDBLOCK) {
			cout << "connect() Failed : " << err << endl;
			return;
		}

		fd_set set;
		FD_ZERO(&set);
		timeval timeout = { 30, 0 }; //30초 동안 기다려본다. 연결이 됐는지.
		FD_SET(sock, &set);

		int result = select(0, NULL, &set, NULL, &timeout);

		if (result == 0) { //성공 시 준비된 fd 개수 반환
			cout << "select / connect() Failed : timeout" << WSAGetLastError() << endl;
			return;
		}
		else if (result < 0)
		{
			cout << "select / connect() Failed : err" << WSAGetLastError() << endl;
			return;
		}

		err = 0;
		socklen_t len = sizeof(err);
		if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)&err, &len) < 0 || err != 0) {
			cout << "getsockopt() error: " << err << endl;
			return;
		}
	}

	cout << "연결 성공" << endl;

}
void CServerManager::Send_Name()
{
	packet_login p;
	ZeroMemory(&p, sizeof(p));
	p.type = C2S_LOGIN;
	p.size = sizeof(packet_login);
	strcpy_s(p.name, name);
	Send_Packet(C2S_LOGIN, &p);
}
void CServerManager::err_quit(const WCHAR* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}

void CServerManager::err_display(const WCHAR* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	cout << "[" << msg << "]" << (char*)lpMsgBuf << endl;
	LocalFree(lpMsgBuf);
}

void CServerManager::Send_Packet(int pType, void* packet)
{
	int retval;
	switch (pType)
	{
	case C2S_LOGIN:
	{
		packet_login p = *reinterpret_cast<packet_login*>(packet);
		int retval = send(sock, (char*)&p, sizeof(p), 0);

		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_LOGIN send() err");
			return;
		}
	}
	break;
	case C2S_ROOMCREATE:
	{

	}
	break;
	case C2S_ROOMENTER:
	{

	}
	break;
	case C2S_ROOMOUT:
	{

	}
	break;
	case C2S_MOVE:
	{

	}
	break;
	default:
		break;
	}
}

void CServerManager::Recv_Packet()
{
	char buf[MAX_BUF_SIZE];
	char* packet = buf;
	int received;
	int left = 2; //size, type

	bool init_come = true;
	while (left > 0 || init_come)
	{
		received = recv(sock, packet, left, 0);
		if (received == SOCKET_ERROR)
			return;
		else if (received == 0)
			break;

		if (init_come)
		{
			left = buf[0] - received;
			packet += received;
			init_come = false;
		}
		else
		{
			left -= received;
			packet += received;

			if (left == 0) {
				Process_Packet(buf);
				return;
			}
		}
	}

}

void CServerManager::Process_Packet(char* buf)
{
	switch (buf[1])
	{
	case S2C_LOGIN_OK:
	{
		packet_login_result p = *reinterpret_cast<packet_login_result*>(buf);
		id = p.id;
		cout << "Login_OK" << endl;
		cout << "name: " << name << endl;

		CSceneManager::GetInstance()->SceneChange(SCENE_STAGE);
	}
	break;
	case S2C_LOGIN_FAIL:
	{
		cout << "Login_FAIL" << endl;
		cout << name<<"은 중복된 이름입니다." << endl;
		cout << "닉네임 입력: ";
		cin >> name;

		Send_Name();
	}
	break;
	case S2C_ENTERPLAYER:
	{
		packet_enter p = *reinterpret_cast<packet_enter*>(buf);
		cout << "새로운 플레이어 입장: " << p.name << endl;
	}
	break;
	case S2C_LEAVEPLAYER:
	{

	}
	break;
	case S2C_ROOMCREATE:
	{

	}
	break;
	case S2C_ROOMENTER:
	{

	}
	break;
	case S2C_ROOMOUT:
	{

	}
	break;
	case S2C_MOVE:
	{

	}
	break;
	default:
		break;
	}
}


