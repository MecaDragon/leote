#ifndef __SCENE__
#define __SCENE__

class CScene
{
public:
	explicit CScene();
	virtual ~CScene();

	virtual void Initialize() = 0;
	virtual void Update(double DeltaTime) = 0;
	virtual void LateUpdate(double DeltaTime) = 0;
	virtual void Render(HDC hDC) = 0;
	virtual void Release() = 0;
};

#endif // !__SCENE__
