#ifndef __PLAYER__
#define __PLAYER__

#include "GameObject.h"

class CPlayer : public CGameObject
{
public:
	explicit CPlayer();
	virtual ~CPlayer();

public:
	// CGameObject을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual int Update(double DeltaTime) override;
	virtual void LateUpdate(double DeltaTime) override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
	
	//set
	void SetNickname(const string& name) { nickname = name; }
	void SetServerIndex(const char& index) { m_serverindex = index;}
	//get
	const string& GetNickname() { return nickname; }
	const int& GetServerIndex() { return m_serverindex; }

	void KeyInput(double DeltaTime);
private:
	HBRUSH hBrush, oldBrush; // 색 나타내기용
	HPEN hPen, oldPen; // 색 나타내기용

	float m_fSpeed = 100.f;
	string nickname; // 플레이어 이름

	int m_serverindex = -1; // 서버상의 인덱스

	//이동량
	double moveDelta = 0.f;
	float PlayerX, PlayerY, PlayerZ;

};

#endif // !__PLAYER__
