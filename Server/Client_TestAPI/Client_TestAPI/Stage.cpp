#include "stdafx.h"
#include "Stage.h"

CStage::CStage()
{
}

CStage::~CStage()
{
}

void CStage::Initialize()
{
}

void CStage::Update(double DeltaTime)
{
}

void CStage::LateUpdate(double DeltaTime)
{
}

void CStage::Render(HDC hDC)
{
}

void CStage::Release()
{
}
