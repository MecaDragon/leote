#include "stdafx.h"
#include "Player.h"
#include "ServerManager.h"
#include <math.h>
CPlayer::CPlayer()
{
	Initialize();
}

CPlayer::~CPlayer()
{
	Release();
}

void CPlayer::Initialize()
{
	m_Info.fx = 0.f;
	m_Info.fy = 0.f;
	m_Info.fsizex = 50.f;
	m_Info.fsizey = 50.f;

	PlayerX = 0.f;
	PlayerY = 0.f;
	PlayerZ = 0.f;
	moveDelta = 0.f;
}

int CPlayer::Update(double DeltaTime)
{
	KeyInput(DeltaTime);

	return 0;
}

void CPlayer::LateUpdate(double DeltaTime)
{
}

void CPlayer::Render(HDC hDC)
{
	hPen = CreatePen(PS_SOLID, 5, RGB(76, 75, 74));
	oldPen = (HPEN)SelectObject(hDC, hPen);

	hBrush = (HBRUSH)CreateSolidBrush(RGB(233, 65, 6));
	oldBrush = (HBRUSH)SelectObject(hDC, hBrush);

	Ellipse(hDC, m_Rect.left, m_Rect.top, m_Rect.right, m_Rect.bottom);

	SelectObject(hDC, oldPen);
	DeleteObject(hPen);
	SelectObject(hDC, oldBrush);
	DeleteObject(hBrush);
}

void CPlayer::Release()
{
}

void CPlayer::KeyInput(double DeltaTime)
{
	if (m_serverindex == CServerManager::GetInstance()->GetServerIndex())
	{
		float moving = DeltaTime * m_fSpeed;

		if (GetAsyncKeyState('w') & 0x8001 || GetAsyncKeyState('W') & 0x8001)
		{
			PlayerY -= moving;
			m_Info.fy -= moving;
		}
		if (GetAsyncKeyState('s') & 0x8001 || GetAsyncKeyState('S') & 0x8001)
		{
			PlayerY += moving;
			m_Info.fy += moving;
		}
		if (GetAsyncKeyState('a') & 0x8001 || GetAsyncKeyState('A') & 0x8001)
		{
			PlayerX -= moving;
			m_Info.fx -= moving;
		}
		if (GetAsyncKeyState('d') & 0x8001 || GetAsyncKeyState('D') & 0x8001)
		{
			PlayerX += moving;
			m_Info.fx += moving;
		}

		moveDelta += DeltaTime;

		if (moveDelta >= 0.05f)
		{
			cout << "x: " << m_Info.fx << ", y: " << m_Info.fy << endl;
			moveDelta = 0.f;
			packet_move p;
			p.id = m_serverindex;
			p.type = C2S_MOVE;
			p.size = sizeof(packet_move);
			p.speed = m_fSpeed;
			p.mx = PlayerX;
			p.my = PlayerY;
			p.mz = PlayerZ;
			p.dx = PlayerX/sqrtf(pow(PlayerX, 2) + pow(PlayerY, 2) + pow(PlayerZ, 2));
			p.dy = PlayerY / sqrtf(pow(PlayerX, 2) + pow(PlayerY, 2) + pow(PlayerZ, 2));
			p.dz = PlayerZ / sqrtf(pow(PlayerX, 2) + pow(PlayerY, 2) + pow(PlayerZ, 2));
			PlayerX = 0.f;
			PlayerY = 0.f;
			PlayerZ = 0.f;

			CServerManager::GetInstance()->Send_Packet(C2S_MOVE, &p);
		}
	}
}
