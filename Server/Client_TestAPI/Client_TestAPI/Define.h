#ifndef __DEFINES__
#define __DEFINES__

#define WINDOWX 1280
#define WINDOWY 720
#define OBJ_DEAD 1
#define OBJ_NOEVENT 0

extern HWND g_hWnd;

#define MSG_BOX(_TEXT) MessageBox(NULL, TEXT(_TEXT),L"System Message",MB_OK)

#define NO_COPY(ClassName)						\
private:										\
	ClassName(const ClassName& Obj);			\
	ClassName& operator=(const ClassName& Obj);

#define DECLARE_SINGLETON(ClassName)		\
		NO_COPY(ClassName)					\
public:										\
	static ClassName* GetInstance()			\
	{										\
		if(nullptr == m_pInstance)			\
		{									\
			m_pInstance = new ClassName;	\
		}									\
		return m_pInstance;					\
	}										\
	void DestroyInstance()					\
	{										\
		if(m_pInstance)						\
		{									\
			delete m_pInstance;				\
			m_pInstance = nullptr;			\
		}									\
	}										\
private:									\
	static ClassName*	m_pInstance;

#define IMPLEMENT_SINGLETON(ClassName)		\
ClassName* ClassName::m_pInstance = nullptr;

class CTag_Finder
{
public:
	explicit CTag_Finder(const wchar_t* pTag)
		:m_pTargetTag(pTag) {}
	~CTag_Finder() {}
public:
	template<typename T>
	bool operator()(const T& pair)
	{
		if (0 == lstrcmpW(m_pTargetTag, pair.first))
			return true;

		return false;
	}
private:
	const wchar_t* m_pTargetTag = nullptr;
};

template<class T>
bool Safe_Release(T& pInstance)
{
	if (pInstance != nullptr)
	{
		if (SUCCEEDED(pInstance->Release()))
		{
			pInstance = nullptr;
			return true;
		}
		else
		{
			MessageBox(NULL, L"Safe_Release Failed", L"System Message", MB_OK);
			return false;
		}
	}

	return true;
}

enum SCENEID {SCENE_MENU, SCENE_STAGE, SCENE_END};
enum OBJECTTYPE { PLAYER, MONSTER, MOUSE, TYPE_END };

typedef struct Information {
	Information() {	}
	Information(float x, float y, float xsize, float ysize)
		:fx(x), fy(y), fsizex(xsize), fsizey(ysize)
	{}
	float fx;
	float fy;
	float fsizex;
	float fsizey;
	float fspeed;
}INFO;
#endif // !__DEFINES__
