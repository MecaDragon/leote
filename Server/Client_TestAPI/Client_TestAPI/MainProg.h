#ifndef __MAINPROG__
#define __MAINPROG__
class CTimerManager;
class CMainProg final
{
public:
	explicit CMainProg();
	virtual ~CMainProg();

public:
	void Initialize();
	void Update(double DeltaTime);
	void LateUpdate(double DeltaTime);
	void Render();
	void Release();

private:
	HDC m_hDC;

	//fps
	TCHAR m_szFps[128];
	int iFps;
	float m_fTimeCount;

	CTimerManager* m_TimeMgr; //Ÿ�̸�

};

#endif // !__MAINPROG__
