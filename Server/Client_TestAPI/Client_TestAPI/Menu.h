#ifndef __MENU__
#define __MENU__

#include "Scene.h"

class CMenu : public CScene
{
public:
	explicit CMenu();
	virtual ~CMenu();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update(double DeltaTime) override;
	virtual void LateUpdate(double DeltaTime) override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
private:
	bool b;
};

#endif // !__MENU__

