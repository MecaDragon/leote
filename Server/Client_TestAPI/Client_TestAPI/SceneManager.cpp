#include "stdafx.h"
#include "SceneManager.h"
#include "Menu.h"
#include "Stage.h"

IMPLEMENT_SINGLETON(CSceneManager)

CSceneManager::CSceneManager()
	:m_pScene(nullptr)
	,m_eCurScene(SCENE_END)
	,m_eNextScene(SCENE_END)
{
}

CSceneManager::~CSceneManager()
{
	Release();
}

void CSceneManager::Update(double DeltaTime)
{
	if (nullptr != m_pScene)
		m_pScene->Update(DeltaTime);
}

void CSceneManager::LateUpdate(double DeltaTime)
{
	if (nullptr != m_pScene)
		m_pScene->LateUpdate(DeltaTime);
}

void CSceneManager::Render(HDC hDC)
{
	if (nullptr != m_pScene)
		m_pScene->Render(hDC);
}

void CSceneManager::Release()
{
	if (m_pScene != nullptr)
	{
		delete m_pScene;
		m_pScene = nullptr;
	}
}

void CSceneManager::SceneChange(SCENEID eID)
{
	m_eNextScene = eID;

	if (m_eNextScene != m_eCurScene)
	{
		Release(); //���� �� ����
		switch (m_eNextScene)
		{
		case SCENE_MENU:
			m_pScene = new CMenu;
			break;
		case SCENE_STAGE:
			m_pScene = new CStage;
			break;
		case SCENE_END:
			break;
		default:
			break;
		}

		m_pScene->Initialize();
		m_eCurScene = m_eNextScene;
	}
}

