#include "stdafx.h"
#include "Timer.h"
#include "Define.h"

CTimer::CTimer()
{
	ZeroMemory(&m_FrameTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_LastTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_FixTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_CpuTick, sizeof(LARGE_INTEGER));
}

CTimer::~CTimer()
{
}

HRESULT CTimer::Initialize_Timer()
{
	QueryPerformanceCounter(&m_FrameTime);
	QueryPerformanceCounter(&m_LastTime);
	QueryPerformanceCounter(&m_FixTime);
	QueryPerformanceFrequency(&m_CpuTick);

	return NOERROR;
}

double CTimer::Get_TimeDelta()
{
	QueryPerformanceCounter(&m_FrameTime);

	if (m_FrameTime.QuadPart - m_FixTime.QuadPart >= m_CpuTick.QuadPart)
	{
		QueryPerformanceFrequency(&m_CpuTick);
		m_FixTime = m_FrameTime;
	}

	m_TimeDelta = ((double)m_FrameTime.QuadPart - (double)m_LastTime.QuadPart) / (double)m_CpuTick.QuadPart;
	m_LastTime = m_FrameTime;

	return m_TimeDelta;
}

HRESULT CTimer::Release()
{
	delete this;

	return NOERROR;
}

CTimer* CTimer::Create()
{
	CTimer* pInstance = new CTimer;

	if (FAILED(pInstance->Initialize_Timer())) 
	{
		Safe_Release(pInstance);
		MSG_BOX("CMainApp Created Failed");
		return nullptr;
	}

	return pInstance;
}
