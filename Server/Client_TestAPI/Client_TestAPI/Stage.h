#ifndef __STAGE__
#define __STAGE__

#include"Scene.h"
class CStage : public CScene
{
public:
	explicit CStage();
	virtual ~CStage();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update(double DeltaTime) override;
	virtual void LateUpdate(double DeltaTime) override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

};

#endif // !__STAGE__
