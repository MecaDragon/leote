#ifndef __GAMEOBJECT__
#define __GAMEOBJECT__

#include "stdafx.h"

class CComponent;
class CCollider;
class CTransform;

class CGameObject
{
protected:
	explicit CGameObject();
	explicit CGameObject(const CGameObject& rhs);
	virtual ~CGameObject();

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);

	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

	OBJECT_TYPE		GetType() { return m_oType; }
	OBJECT_STATE	GetState() { return m_oState; }

	void		SetType(OBJECT_TYPE type) { m_oType = type; }
	void		SetState(OBJECT_STATE state) { m_oState = state; }

	XMFLOAT3 get_ObjPosition() { return m_xmObjPosition; }
	void	set_ObjPosition(XMFLOAT3 xmObjPosition) { m_xmObjPosition = xmObjPosition; }
	CTransform* get_ObjTransform() { return m_pTransformCom; }

protected:
	HRESULT Add_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, const wchar_t* pComponentTag, CComponent** ppComponent, void* pArg = nullptr);
	HRESULT Compute_ViewZ(XMFLOAT3 vWorldPos);
	CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz);

private:
	CComponent* Find_Component(const wchar_t* pComponentTag);

private:
	map<const wchar_t*, CComponent*> m_mapComponents;
	typedef map<const wchar_t*, CComponent*> COMPONENTS;

	float m_fViewZ;

protected:
	OBJECT_TYPE		m_oType = OBJECT_TYPE::OBJTYPE_FIXED;
	OBJECT_STATE	m_oState = OBJECT_STATE::OBJSTATE_GROUND;

	XMFLOAT3 m_xmObjPosition = XMFLOAT3(0.f, 0.f, 0.f);

	CTransform* m_pTransformCom = nullptr;

public:
	virtual CGameObject* Clone_GameObject(void* pArg) = 0;
	virtual HRESULT Release();

public:
	float Get_ViewZ() const { return m_fViewZ; }


public:
	virtual void OnCollision(CCollider& collisionTarget, double TimeDelta) {}

};

struct ObjWorld
{
	XMFLOAT3 position;
	XMFLOAT3 rotation;
	XMFLOAT3 scale;
	int tag; // 아무거나 구분짓고 싶을 때 쓰고 싶어서 만듦
}typedef ObjWorld;

#endif