#ifndef __UI__
#define __UI__

#include "stdafx.h"
#include "GameObject.h"

class CTransform;
class CPlane_Buffer;
class CShader;
class CTexture;
class CRenderer;

class CUI : public CGameObject
{
private:
	explicit CUI();
	explicit CUI(const CUI& rhs);
	virtual ~CUI(void) = default;

public:
	HRESULT Initialize_GameObject_Prototype(SCENEID SceneName, wstring TexComName, wstring ShaderComName = L"Component_Shader_UI");
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	virtual CGameObject* Clone_GameObject(void* pArg);

	static CUI* Create(SCENEID SceneName, wstring TexComName, wstring ShaderComName = L"Component_Shader_UI");

	HRESULT Release();

	HRESULT SetTexture(const wchar_t* texturename, const wchar_t* componentname = L"Com_Texture2");

	void SetHide(bool hide) { m_bHide = hide; }
	bool GetHide() { return m_bHide; }

	void SetTimerUI(float Time, XMFLOAT3 position);
	void SetScale(XMFLOAT3 scale);
	bool GetTimerHide() { return m_bTimerHide; }

	void SetTexIndex(int index) { m_iTexindex = index; }
	int GetTexIndex() { return m_iTexindex; }
	
	CTransform* getTransform() { return m_pTransformCom; }

	void setdead() { m_bDead = true; }

private:
	HRESULT Add_Component();

private:
	CTransform* m_pTransformCom = nullptr;
	CPlane_Buffer* m_pPlane_BufferCom = nullptr;
	CShader* m_pShaderCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	wstring m_TexComName;
	wstring m_ShaderComName;
	SCENEID m_SceneName;

	bool m_bHide = false;
	bool m_bTimerHide = false;

	float m_fTimerHide = 0.f;
	float m_fTimer = 0.f;

	int m_iTexindex = 0;

	bool m_bDead = false;
};

#endif