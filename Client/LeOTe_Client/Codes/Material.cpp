#include "Management.h"
#include "Material.h"

CMaterial::CMaterial()
{
}

CMaterial::CMaterial(const CMaterial& rhs)
{
}

CMaterial::~CMaterial(void)
{
}

HRESULT CMaterial::Initialize_Component_Prototype()
{


	return NOERROR;
}

HRESULT CMaterial::Initialize_Component(void* pArg)
{
	m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	return NOERROR;
}

CComponent* CMaterial::Clone_Component(void* pArg)
{
	CMaterial* pInstance = new CMaterial(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CMaterial Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CMaterial* CMaterial::Create()
{
	CMaterial* pInstance = new CMaterial();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CMaterial Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

void CMaterial::SetAmbient(float x, float y, float z, float w)
{
	m_xmf4x4Material.m_xmf4Ambient.x = x;
	m_xmf4x4Material.m_xmf4Ambient.y = y;
	m_xmf4x4Material.m_xmf4Ambient.z = z;
	m_xmf4x4Material.m_xmf4Ambient.w = w;
}

void CMaterial::SetAmbient(XMFLOAT4 xmf4Ambient)
{
	SetAmbient(xmf4Ambient.x, xmf4Ambient.y, xmf4Ambient.z, xmf4Ambient.w);
}

void CMaterial::SetAmbientRGBA(int r, int g, int b, int a)
{
	m_xmf4x4Material.m_xmf4Ambient.x = (float)r / 256;
	m_xmf4x4Material.m_xmf4Ambient.y = (float)g / 256;
	m_xmf4x4Material.m_xmf4Ambient.z = (float)b / 256;
	m_xmf4x4Material.m_xmf4Ambient.w = (float)a / 256;
}

void CMaterial::SetDiffuse(float x, float y, float z, float w)
{
	m_xmf4x4Material.m_xmf4Diffuse.x = x;
	m_xmf4x4Material.m_xmf4Diffuse.y = y;
	m_xmf4x4Material.m_xmf4Diffuse.z = z;
	m_xmf4x4Material.m_xmf4Diffuse.w = w;
}

void CMaterial::SetDiffuse(XMFLOAT4 xmf4Diffuse)
{
	SetDiffuse(xmf4Diffuse.x, xmf4Diffuse.y, xmf4Diffuse.z, xmf4Diffuse.w);
}

void CMaterial::SetDiffuseRGBA(int r, int g, int b, int a)
{
	m_xmf4x4Material.m_xmf4Diffuse.x = (float)r / 256;
	m_xmf4x4Material.m_xmf4Diffuse.y = (float)g / 256;
	m_xmf4x4Material.m_xmf4Diffuse.z = (float)b / 256;
	m_xmf4x4Material.m_xmf4Diffuse.w = (float)a / 256;
}

void CMaterial::SetEmissive(float x, float y, float z, float w)
{
	m_xmf4x4Material.m_xmf4Emissive.x = x;
	m_xmf4x4Material.m_xmf4Emissive.y = y;
	m_xmf4x4Material.m_xmf4Emissive.z = z;
	m_xmf4x4Material.m_xmf4Emissive.w = w;
}

void CMaterial::SetEmissive(XMFLOAT4 xmf4Emissive)
{
	SetEmissive(xmf4Emissive.x, xmf4Emissive.y, xmf4Emissive.z, xmf4Emissive.w);
}

void CMaterial::SetEmissiveRGBA(int r, int g, int b, int a)
{
	m_xmf4x4Material.m_xmf4Emissive.x = (float)r / 256;
	m_xmf4x4Material.m_xmf4Emissive.y = (float)g / 256;
	m_xmf4x4Material.m_xmf4Emissive.z = (float)b / 256;
	m_xmf4x4Material.m_xmf4Emissive.w = (float)a / 256;
}

void CMaterial::SetSpecular(float x, float y, float z, float w)
{
	m_xmf4x4Material.m_xmf4Specular.x = x;
	m_xmf4x4Material.m_xmf4Specular.y = y;
	m_xmf4x4Material.m_xmf4Specular.z = z;
	m_xmf4x4Material.m_xmf4Specular.w = w;
}

void CMaterial::SetSpecular(XMFLOAT4 xmf4Specular)
{
	SetSpecular(xmf4Specular.x, xmf4Specular.y, xmf4Specular.z, xmf4Specular.w);
}

void CMaterial::SetSpecularRGBA(int r, int g, int b, int a)
{
	m_xmf4x4Material.m_xmf4Specular.x = (float)r / 256;
	m_xmf4x4Material.m_xmf4Specular.y = (float)g / 256;
	m_xmf4x4Material.m_xmf4Specular.z = (float)b / 256;
	m_xmf4x4Material.m_xmf4Specular.w = (float)a / 256;
}

XMFLOAT4 CMaterial::GetAmbient()
{
	return(m_xmf4x4Material.m_xmf4Ambient);
}

XMFLOAT4 CMaterial::GetDiffuse()
{
	return(m_xmf4x4Material.m_xmf4Diffuse);
}

XMFLOAT4 CMaterial::GetEmissive()
{
	return(m_xmf4x4Material.m_xmf4Emissive);
}

XMFLOAT4 CMaterial::GetSpecular()
{
	return m_xmf4x4Material.m_xmf4Specular;
}

MATERIAL CMaterial::GetMaterialMat()
{
	return m_xmf4x4Material;
}

HRESULT CMaterial::Release()
{

	delete this;

	return NOERROR;
}
