#include "Shader.h"
#include "Management.h"
#include "stdafx.h"
#include "Physic.h"

#define MAX_VEL 10
#define MAX_ACC 10
#define FRICTION 1.1f

CPhysic::CPhysic()
{
}

CPhysic::CPhysic(const CPhysic& rhs)
{
}

CPhysic::~CPhysic(void)
{
}

HRESULT CPhysic::Initialize_Component_Prototype()
{


	return NOERROR;
}

HRESULT CPhysic::Initialize_Component(void* pArg)
{
	m_xmVel = { 0.f,0.f,0.f }; // x - right, y - up, z - look
	m_xmAcc = { 0.f,0.f,0.f };
	m_fMass = 0.f;
	m_bApplyPhysics = true;
	return NOERROR;
}

CComponent* CPhysic::Clone_Component(void* pArg)
{
	CPhysic* pInstance = new CPhysic(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CPhysic Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CPhysic* CPhysic::Create()
{
	CPhysic* pInstance = new CPhysic();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CPhysic Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

XMFLOAT3 CPhysic::UpdatePhysics(XMFLOAT3 position, XMFLOAT3 rightvector, XMFLOAT3 upvector, XMFLOAT3 lookvector, XMFLOAT3 force, OBJECT_STATE state, double elapsedTimeInSec)
{
	float t = elapsedTimeInSec;
	float tt = elapsedTimeInSec * elapsedTimeInSec;

	XMFLOAT3 acc{ 0.f, 0.f, 0.f };

	rightvector = Vector3::Normalize(rightvector);
	upvector = Vector3::Normalize(upvector);
	lookvector = Vector3::Normalize(lookvector);
	//Calc X axis
	//friction 
	if ((fabs(m_xmVel.x) > 0.f || (fabs(m_xmVel.z) > 0.f)) && state == OBJECT_STATE::OBJSTATE_GROUND)
	{
		if (fabs(m_xmVel.x) > 0.f)
		{
			//calce temporary
			acc.x = force.x / m_fMass;

			//sum with object'acc
			acc.x += m_xmAcc.x;

			//calc friction
			acc.x += m_xmVel.x / fabs(m_xmVel.x) * FRICTION * (-GRAVITY);

			if (acc.x > MAX_ACC)
				acc.x = MAX_ACC;

			//check wrong friction direction
			float tempVelX = m_xmVel.x + acc.x * t;
			if (m_xmVel.x * tempVelX < 0.f)
			{
				m_xmVel.x = 0.f;
			}
			else
			{
				m_xmVel.x = tempVelX;
			}
		}
		else
		{
			acc.x = force.x / m_fMass;
			m_xmVel.x = m_xmVel.x + acc.x * t;
		}

		if (fabs(m_xmVel.z) > 0.f)
		{
			//calce temporary
			acc.z = force.z / m_fMass;

			//sum with object'acc
			acc.z += m_xmAcc.z;

			//calc friction
			acc.z += m_xmVel.z / fabs(m_xmVel.z) * FRICTION * (-GRAVITY);

			if (acc.z > MAX_ACC)
				acc.z = MAX_ACC;

			float tempVelZ = m_xmVel.z + acc.z * t;
			if (m_xmVel.z * tempVelZ < 0.f)
			{
				m_xmVel.z = 0.f;
			}
			else
			{
				m_xmVel.z = tempVelZ;
			}
		}
		else
		{
			acc.z = force.z / m_fMass;
			m_xmVel.z = m_xmVel.z + acc.z * t;
		}
		m_xmVel.y = 0.f;
	}
	else
	{
		//calce temporary
		acc.x = force.x / m_fMass;
		m_xmVel.x = m_xmVel.x + acc.x * t;

		acc.z = force.z / m_fMass;
		m_xmVel.z = m_xmVel.z + acc.z * t;
	}

	//Calc Y axis
	if (state == OBJECT_STATE::OBJSTATE_GROUND || state == OBJECT_STATE::OBJSTATE_FLY)
	{
		acc.y += m_xmAcc.y;

		//Jump!
		acc.y += force.y / m_fMass;
	}
	if (state == OBJECT_STATE::OBJSTATE_FALLING || state == OBJECT_STATE::OBJSTATE_FLY)
	{
		acc.y -= GRAVITY;
	}

	m_xmVel.y = m_xmVel.y + acc.y * t;

	if (m_xmVel.x > MAX_VEL)
		m_xmVel.x = MAX_VEL;

	if (m_xmVel.z > MAX_VEL)
		m_xmVel.z = MAX_VEL;

	if (m_xmVel.x < -MAX_VEL)
		m_xmVel.x = -MAX_VEL;

	if (m_xmVel.z < -MAX_VEL)
		m_xmVel.z = -MAX_VEL;

	//update position
	XMFLOAT3 rightposition	= Vector3::Add(Vector3::ScalarProduct(rightvector, m_xmVel.x * t, false), Vector3::ScalarProduct(rightvector, 0.5f * acc.x * tt, false));
	XMFLOAT3 upposition		= Vector3::Add(Vector3::ScalarProduct(upvector, m_xmVel.y * t, false), Vector3::ScalarProduct(upvector, 0.5f * acc.y * tt, false));
	XMFLOAT3 lookposition	= Vector3::Add(Vector3::ScalarProduct(lookvector, m_xmVel.z * t, false), Vector3::ScalarProduct(lookvector, 0.5f * acc.z * tt, false));
	XMFLOAT3 shift = Vector3::Add(rightposition, Vector3::Add(upposition, lookposition));
	//m_PositionX = m_PositionX + m_VelX * t + 0.5f * accX * tt;
	//m_PositionY = m_PositionY + m_VelY * t + 0.5f * accY * tt;
	return shift;
	//return XMFLOAT3(0,0.1,0);
}

void CPhysic::SetVel(float x, float y, float z)
{
	m_xmVel.x = x;
	m_xmVel.y = y;
	m_xmVel.z = z;
}

void CPhysic::SetAcc(float x, float y, float z)
{
	m_xmAcc.x = x;
	m_xmAcc.y = y;
	m_xmAcc.z = z;
}

void CPhysic::SetMass(float mass)
{
	m_fMass = mass;
}

void CPhysic::SetApplyPhysics(bool bPhy)
{
	m_bApplyPhysics = bPhy;
}

XMFLOAT3 CPhysic::GetVel()
{
	return m_xmVel;
}

XMFLOAT3 CPhysic::GetAcc()
{
	return m_xmAcc;
}

float CPhysic::GetMass()
{
	return m_fMass;
}

bool CPhysic::GetApplyPhysics()
{
	return m_bApplyPhysics;
}

HRESULT CPhysic::Release()
{

	delete this;

	return NOERROR;
}
