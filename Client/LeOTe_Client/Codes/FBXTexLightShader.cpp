#include "FBXTexLightShader.h"

CFBXTexLightShader::CFBXTexLightShader()
	: CShader()
{
}

CFBXTexLightShader::CFBXTexLightShader(const CFBXTexLightShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

CFBXTexLightShader::~CFBXTexLightShader(void)
{
}

HRESULT CFBXTexLightShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"FBXTex Shader PipelineState");
	return NOERROR;
}

HRESULT CFBXTexLightShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CFBXTexLightShader::Clone_Component(void* pArg)
{
	CFBXTexLightShader* pInstance = new CFBXTexLightShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CFBXTexLightShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CFBXTexLightShader* CFBXTexLightShader::Create()
{
	CFBXTexLightShader* pInstance = new CFBXTexLightShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CFBXTexLightShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CFBXTexLightShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/FBXTexture.hlsl", "VSFBXTextureLight", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CFBXTexLightShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/FBXTexture.hlsl", "PSFBXTextureLight", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CFBXTexLightShader::Create_InputLayout()
{
	UINT nInputElementDescs;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 5;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TANNGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;


    return(d3dInputLayoutDesc);
}

D3D12_RASTERIZER_DESC CFBXTexLightShader::Create_RasterizerState()
{
	D3D12_RASTERIZER_DESC d3dRasterizerDesc;
	::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0;
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(d3dRasterizerDesc);
}

D3D12_BLEND_DESC CFBXTexLightShader::Create_BlendState()
{
	D3D12_BLEND_DESC d3dBlendDesc;
	::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
	d3dBlendDesc.AlphaToCoverageEnable = FALSE;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = TRUE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	return(d3dBlendDesc);
}

HRESULT CFBXTexLightShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
