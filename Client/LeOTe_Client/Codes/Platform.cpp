#include "stdafx.h"
#include "Platform.h"
#include "Management.h"
#include "ShadowShader.h"
#include "Renderer.h"
#include "Camera_Third.h"

CPlatform::CPlatform()
	: CGameObject()
{

}

CPlatform::CPlatform(const CPlatform& rhs)
	: CGameObject(rhs)
{
}

HRESULT CPlatform::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CPlatform::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);

	m_pMaterialCom->SetDiffuseRGBA(145, 235, 63, 256);
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_xmDefaultPos = objw->position;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
	}
	m_pTransformCom->SetScale(4.f, 1.f, 4.f);
	return NOERROR;
}

short CPlatform::Update_GameObject(double TimeDelta)
{

	if (m_isStartHide == true)
	{
		m_pTransformCom->MoveUp(TimeDelta * -0.2f);

		if (m_pTransformCom->GetPosition().y < 12.f)
		{
			m_isStartHide = false;
			m_isHideEnd = true;
		}
	}

	return short();
}

short CPlatform::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CPlatform::Render_Shadow()
{
	/*
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pMeshCom->Render();
	*/
	return NOERROR;
}

HRESULT CPlatform::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);
	m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CPlatform::Add_Component()
{
	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Platform_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXOBJ", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CPlatform::SetUp_ConstantTable()
{
	return NOERROR;
}

CPlatform* CPlatform::Create()
{
	CPlatform* pInstance = new CPlatform();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CPlatform Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CPlatform::Clone_GameObject(void* pArg)
{
	CPlatform* pInstance = new CPlatform(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CPlatform Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CPlatform::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pMaterialCom);
	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}

void CPlatform::setDefaultPos()
{
	m_pTransformCom->SetPosition(m_xmDefaultPos);
}
