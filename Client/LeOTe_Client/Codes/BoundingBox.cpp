#include "BoundingBox.h"
#include "Shader.h"
#include "Transform.h"
#include "Management.h"
#include "Cube_Buffer.h"
#include "Texture.h"
#include "Material.h"
#include "Renderer.h"

CBoundingBox::CBoundingBox()
{
}

CBoundingBox::CBoundingBox(const CBoundingBox& rhs)
{
}

HRESULT CBoundingBox::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CBoundingBox::Initialize_GameObject(void* pArg)
{
	ID3D12Device* pDevice = CDeviceManager::GetInstance()->Get_Device();

	if(FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
	}
	return NOERROR;
}

short CBoundingBox::Update_GameObject(double TimeDelta)
{

	return 0;
}

short CBoundingBox::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return 0;
}

HRESULT CBoundingBox::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);
	m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());
	m_pTextureCom->UpdateShaderVariables(pCommandList);

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pCube_BufferCom->Render_Buffer();
	return NOERROR;
}

CGameObject* CBoundingBox::Clone_GameObject(void* pArg)
{
	CBoundingBox* pInstance = new CBoundingBox(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CBoundingBox Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CBoundingBox* CBoundingBox::Create()
{
	CBoundingBox* pInstance = new CBoundingBox();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CBoundingBox Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CBoundingBox::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pCube_BufferCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pMaterialCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}

HRESULT CBoundingBox::Add_Component()
{
	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_BoundingBox", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_BoundingBoxBuffer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Cube_Buffer", L"Com_Cube_Buffer", (CComponent**)&m_pCube_BufferCom)))
		return E_FAIL;
	
	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_Dice", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}
