#ifndef __CAMERA_FIXED__
#define __CAMERA_FIXED__

#include "stdafx.h"
#include "Camera.h"

class CCamera_Fixed : public CCamera
{	
private:
	explicit CCamera_Fixed();
	explicit CCamera_Fixed(const CCamera_Fixed& rhs);
	virtual ~CCamera_Fixed() = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

public:
	static CCamera_Fixed* Create();

	// CCamera을(를) 통해 상속됨
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	HRESULT Release();

	void settingCamera(XMFLOAT3 Position = XMFLOAT3(0.f, 25.f, 15.f), XMFLOAT3 LookAt = XMFLOAT3(0.f, 0.f, -1.f), XMFLOAT3 Up = XMFLOAT3(0.f, 1.f, 0.f));

};

#endif