#include "Camera_Obj.h"
#include "KeyManager.h"
#include "DeviceManager.h"
#include "Transform.h"
#include "GameObject.h"
#include "Management.h"

CCamera_Obj::CCamera_Obj() : CCamera()
{
	m_pTargetObj = nullptr;
}

CCamera_Obj::CCamera_Obj(const CCamera_Obj& rhs) : CCamera(rhs)
{
}

HRESULT CCamera_Obj::Initialize_GameObject_Prototype()
{
	m_pd3dcbCamera->SetName(L"CCamera_Obj Resource");
	return NOERROR;
}

HRESULT CCamera_Obj::Initialize_GameObject(void* pArg)
{
	m_pd3dcbCamera->SetName(L"CCamera_Obj Resource");
	return NOERROR;
}

short CCamera_Obj::Update_GameObject(double TimeDelta)
{
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();
	CManagement* pManagement = CManagement::GetInstance();
	GenerateFrustum();
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	CTransform* pTargetTrans = m_pTargetObj->get_ObjTransform();

	if (m_pTargetObj && pTargetTrans)
	{
		XMFLOAT4X4 xmf4x4Rotate = Matrix4x4::Identity();
		XMFLOAT3 xmf3Up = XMFLOAT3(0.f, 1.f, 0.f);
		XMFLOAT3 xmf3Right, xmf3Look;

		xmf3Right = pTargetTrans->GetRight();
		xmf3Look = pTargetTrans->GetLook();

		xmf4x4Rotate._11 = xmf3Right.x; xmf4x4Rotate._21 = xmf3Up.x; xmf4x4Rotate._31 = xmf3Look.x;
		xmf4x4Rotate._12 = xmf3Right.y; xmf4x4Rotate._22 = xmf3Up.y; xmf4x4Rotate._32 = xmf3Look.y;
		xmf4x4Rotate._13 = xmf3Right.z; xmf4x4Rotate._23 = xmf3Up.z; xmf4x4Rotate._33 = xmf3Look.z;

		XMFLOAT3 xmf3Offset = Vector3::TransformCoord(m_xmf3Offset, xmf4x4Rotate);
		XMFLOAT3 xmf3Position = Vector3::Add(pTargetTrans->GetPosition(), xmf3Offset);
		XMFLOAT3 xmf3Direction = Vector3::Subtract(xmf3Position, m_tCameraInfo.xmPosition);
		float fLength = Vector3::Length(xmf3Direction);
		xmf3Direction = Vector3::Normalize(xmf3Direction);
		float fDistance = fLength * 1.f;
		if (fDistance > fLength) fDistance = fLength;
		if (fLength < 0.01f) fDistance = fLength;
		if (fDistance > 0)
		{
			m_tCameraInfo.xmPosition = Vector3::Add(m_tCameraInfo.xmPosition, xmf3Direction, fDistance);
			SetLookAt(pTargetTrans);
		}
	}
	
	Set_Position(m_tCameraInfo.xmPosition);
	return 0;
}

void CCamera_Obj::Rotate(float x, float y, float z)
{
	if (x != 0.0f)
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tCameraInfo.xmRight), XMConvertToRadians(x));
		m_tCameraInfo.xmLook = Vector3::TransformNormal(m_tCameraInfo.xmLook, xmmtxRotate);
		m_tCameraInfo.xmUp = Vector3::TransformNormal(m_tCameraInfo.xmUp, xmmtxRotate);
		m_tCameraInfo.xmRight = Vector3::TransformNormal(m_tCameraInfo.xmRight, xmmtxRotate);
	}
	if ((y != 0.0f))
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tCameraInfo.xmUp), XMConvertToRadians(y));
		m_tCameraInfo.xmLook = Vector3::TransformNormal(m_tCameraInfo.xmLook, xmmtxRotate);
		m_tCameraInfo.xmUp = Vector3::TransformNormal(m_tCameraInfo.xmUp, xmmtxRotate);
		m_tCameraInfo.xmRight = Vector3::TransformNormal(m_tCameraInfo.xmRight, xmmtxRotate);
	}
	if ((z != 0.0f))
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tCameraInfo.xmLook), XMConvertToRadians(z));
		m_tCameraInfo.xmPosition = Vector3::Subtract(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt);
		m_tCameraInfo.xmPosition = Vector3::TransformCoord(m_tCameraInfo.xmPosition, xmmtxRotate);
		m_tCameraInfo.xmPosition = Vector3::Add(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt);
		m_tCameraInfo.xmLook = Vector3::TransformNormal(m_tCameraInfo.xmLook, xmmtxRotate);
		m_tCameraInfo.xmUp = Vector3::TransformNormal(m_tCameraInfo.xmUp, xmmtxRotate);
		m_tCameraInfo.xmRight = Vector3::TransformNormal(m_tCameraInfo.xmRight, xmmtxRotate);
	}
}

void CCamera_Obj::SetLookAt(CTransform* pTargetTrans)
{
	if (!pTargetTrans)
		return;

	XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
	XMFLOAT4X4 mtxLookAt = Matrix4x4::LookAtLH(m_tCameraInfo.xmPosition, pTargetTrans->GetPosition(), upVector);
	Set_ViewMatrix(mtxLookAt);
	m_tCameraInfo.xmRight = XMFLOAT3(mtxLookAt._11, mtxLookAt._21, mtxLookAt._31);
	m_tCameraInfo.xmUp = XMFLOAT3(mtxLookAt._12, mtxLookAt._22, mtxLookAt._32);
	m_tCameraInfo.xmLook = XMFLOAT3(mtxLookAt._13, mtxLookAt._23, mtxLookAt._33);
}

short CCamera_Obj::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CCamera_Obj::Render_GameObject()
{
	return NOERROR;
}

CCamera_Obj* CCamera_Obj::Create()
{
	CCamera_Obj* pInstance = new CCamera_Obj();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CCamera_Obj Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CCamera_Obj::Clone_GameObject(void* pArg)
{
	CCamera_Obj* pInstance = new CCamera_Obj(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CCamera_Obj Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CCamera_Obj::Release()
{
	CCamera::Release();

	delete this;

	return NOERROR;
}
