#ifndef __UISHADER__
#define __UISHADER__

#include "Shader.h"

class CUIShader final : public CShader
{
private:
	explicit CUIShader();
	explicit CUIShader(const CUIShader& rhs);
	virtual ~CUIShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CUIShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_BLEND_DESC Create_BlendState();

};

#endif
