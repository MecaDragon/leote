#include "Layer.h"
#include "Player.h"
#include "ServerManager.h"

CLayer::CLayer()
{
}

CLayer::~CLayer()
{
}

HRESULT CLayer::Initialize_Layer()
{
	return NOERROR;
}

HRESULT CLayer::Add_GameObject(CGameObject* pGameObject)
{
	if (nullptr == pGameObject)
		return E_FAIL;

	m_listGameObj.push_back(pGameObject);

	return NOERROR;
}

short CLayer::Update_Layer(double TimeDelta)
{
	int iEvent = 0;

	list<CGameObject*>::iterator iter;

	for (iter = m_listGameObj.begin(); iter != m_listGameObj.end();)
	{
		if ((*iter) != nullptr)
		{
			iEvent = (*iter)->Update_GameObject(TimeDelta);

			if (iEvent == -1) // delete
			{
				(*iter)->Release();
				iter = m_listGameObj.erase(iter);				
			}
			else
			{
				++iter;
			}			

		}
	}

	//for (auto& pGameObj : m_listGameObj)
	//{
	//	if (nullptr != pGameObj)
	//	{
	//		iEvent = pGameObj->Update_GameObject(TimeDelta);

	//		if (iEvent == -1)
	//		{
	//			
	//		}

	//		if (iEvent != 0)
	//			return iEvent;
	//	}
	//}

	return 0;
}

short CLayer::LateUpdate_Layer(double TimeDelta)
{
	int iEvent = 0;

	for (auto& pGameObj : m_listGameObj)
	{
		if (nullptr != pGameObj)
		{
			iEvent = pGameObj->LateUpdate_GameObject(TimeDelta);

			if (iEvent != 0)
				return iEvent;
		}
	}

	return 0;
}

HRESULT CLayer::Render_ShadowMap()
{
	HRESULT hResult;
	for (auto& pGameObj : m_listGameObj)
	{
		if (nullptr != pGameObj)
		{
			hResult = pGameObj->Render_Shadow();

			if (FAILED(hResult))
				return E_FAIL;
		}
	}

	return NOERROR;
}

list<CGameObject*>& CLayer::Get_GameObject()
{
	return m_listGameObj;
}

void CLayer::Update_PlayerPosition(packet_move p)
{
	for (auto& player : m_listGameObj)
	{
		CPlayer* temp = dynamic_cast<CPlayer*>(player);

		if (temp->Get_ClientIndex() == CServerManager::GetInstance()->Get_ClientIndex())
			continue;

		if (temp->Get_ClientIndex() == p.id)
			temp->SetLerpStart(p);
		
	}
}

void CLayer::Set_PlayerOrder(packet_order p, CPlayer** players)
{
	int id = 1;
	for (auto& player : m_listGameObj)
	{
		CPlayer* temp = dynamic_cast<CPlayer*>(player);
		players[id++] = temp;

		if (temp->Get_ClientIndex() == p.id) {
			temp->SetPlayerOrder(p.order);
			temp->m_tPlayerInfo.xmPosition = XMFLOAT3{ p.x,p.y,p.z };
		}
	}
}

CGameObject* CLayer::Search_Player(int id)
{
	for (auto& player : m_listGameObj)
	{
		CPlayer* temp = dynamic_cast<CPlayer*>(player);

		if (temp->Get_ClientIndex() == id)
			return player;
	}

	return nullptr;
}

CLayer* CLayer::Create()
{
	CLayer* pInstance = new CLayer();

	if (FAILED(pInstance->Initialize_Layer()))
	{
		MSG_BOX("CLayer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CLayer::Release()
{
	for (auto& pGameObject : m_listGameObj)
		Safe_Release(pGameObject);

	m_listGameObj.clear();

	delete this;

	return NOERROR;
}
