#ifndef __GRASSPLANE__
#define __GRASSPLANE__

#include "stdafx.h"
#include "GameObject.h"

class CTransform;
class CPlane_Buffer;
class CShader;
class CTexture;
class CRenderer;

class CGrassPlane : public CGameObject
{
private:
	explicit CGrassPlane();
	explicit CGrassPlane(const CGrassPlane& rhs);
	virtual ~CGrassPlane(void) = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	virtual CGameObject* Clone_GameObject(void* pArg);

	static CGrassPlane* Create();

	HRESULT Release();

private:
	HRESULT Add_Component();

private:
	CTransform* m_pTransformCom = nullptr;
	CPlane_Buffer* m_pPlane_BufferCom = nullptr;
	CShader* m_pShaderCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

};

#endif