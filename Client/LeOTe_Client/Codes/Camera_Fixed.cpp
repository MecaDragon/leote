#include "Camera_Fixed.h"
#include "KeyManager.h"
#include "DeviceManager.h"
#include "Transform.h"

CCamera_Fixed::CCamera_Fixed() : CCamera()
{
}

CCamera_Fixed::CCamera_Fixed(const CCamera_Fixed& rhs) : CCamera(rhs)
{
}

HRESULT CCamera_Fixed::Initialize_GameObject_Prototype()
{
	m_pd3dcbCamera->SetName(L"CCamera_Fixed Resource");
	return NOERROR;
}

HRESULT CCamera_Fixed::Initialize_GameObject(void* pArg)
{
	m_pd3dcbCamera->SetName(L"CCamera_Fixed Resource");
	return NOERROR;
}

short CCamera_Fixed::Update_GameObject(double TimeDelta)
{
	GenerateFrustum();
	return 0;
}

short CCamera_Fixed::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CCamera_Fixed::Render_GameObject()
{
	return NOERROR;
}

CCamera_Fixed* CCamera_Fixed::Create()
{
	CCamera_Fixed* pInstance = new CCamera_Fixed();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CCamera_Fixed Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CCamera_Fixed::Clone_GameObject(void* pArg)
{
	CCamera_Fixed* pInstance = new CCamera_Fixed(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CCamera_Fixed Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CCamera_Fixed::Release()
{
	CCamera::Release();

	delete this;

	return NOERROR;
}

void CCamera_Fixed::settingCamera(XMFLOAT3 Position, XMFLOAT3 LookAt, XMFLOAT3 Up)
{
	m_tCameraInfo.xmPosition = Position;
	m_tCameraInfo.xmLookAt = LookAt;
	m_tCameraInfo.xmUp = Up;
	m_matView = Matrix4x4::LookAtLH(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt, m_tCameraInfo.xmUp);
	Set_ViewMatrix(m_matView);
}
