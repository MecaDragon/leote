#include "Buffer.h"

CBuffer::CBuffer()
{
}

CBuffer::CBuffer(const CBuffer& rhs)
	: m_d3dIndexBufferView(rhs.m_d3dIndexBufferView)
	, m_d3dPrimitiveTopology(rhs.m_d3dPrimitiveTopology)
	, m_d3dVertexBufferView(rhs.m_d3dVertexBufferView)
	, m_nBaseVertex(rhs.m_nBaseVertex)
	, m_nIndices(rhs.m_nIndices)
	, m_nOffset(rhs.m_nOffset)
	, m_nSlot(rhs.m_nSlot)
	, m_nStartIndex(rhs.m_nStartIndex)
	, m_nStride(rhs.m_nStride)
	, m_nVertices(rhs.m_nVertices)
	, m_pd3dIndexBuffer(rhs.m_pd3dIndexBuffer)
	, m_pd3dIndexUploadBuffer(rhs.m_pd3dIndexUploadBuffer)
	, m_pd3dVertexBuffer(rhs.m_pd3dVertexBuffer)
	, m_pd3dVertexUploadBuffer(rhs.m_pd3dVertexUploadBuffer)
{
}

HRESULT CBuffer::Initialize_Component_Prototype()
{
	return NOERROR;
}

HRESULT CBuffer::Initialize_Component(void* pArg)
{
	return NOERROR;
}

void CBuffer::Release_UploadBuffer()
{
	if(m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->Release(); 

	if(m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->Release();

	//m_pd3dVertexUploadBuffer = nullptr;
	//m_pd3dIndexUploadBuffer = nullptr;
}
