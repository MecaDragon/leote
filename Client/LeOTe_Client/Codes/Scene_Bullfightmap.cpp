#include "Scene_Bullfightmap.h"
#include "Scene_Boardmap.h"
#include "Scene_WaitingRoom.h"
#include "Management.h"
#include "TeddyBear.h"
#include "Physic.h"
#include "Player.h"
#include "BoundingBox.h"
#include "Font.h"
#include "BillBoardObject.h"
#include "Camera_Third.h"
#include "TileLarge.h"
#include "StaticObject.h"

extern bool G_SC;

extern CPlayer* Player;
extern CPlayer* Players[3];

extern CTeddyBear* pTeddyBear[2];

CScene_Bullfightmap::CScene_Bullfightmap()
{
}

CScene_Bullfightmap::~CScene_Bullfightmap()
{
}

CGameObject* CScene_Bullfightmap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position.x = x;
	temp.position.y = y;
	temp.position.z = z;

	temp.rotation.x = rx;
	temp.rotation.y = ry;
	temp.rotation.z = rz;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BULLFIGHTMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

CGameObject* CScene_Bullfightmap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position = transform;
	temp.rotation = rotation;
	temp.scale = scale;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BULLFIGHTMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

HRESULT CScene_Bullfightmap::Initialize_Scene(CScene_Boardmap* pSceneBoardmap)
{
	m_pScene_Boardmap = pSceneBoardmap;

	moveDelta = 0.f;

	m_SceneId = SCENEID::SCENE_BULLFIGHTMAP;

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);

	CManagement* pManagement = CManagement::GetInstance();

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");

	CreateObject(L"Layer_BackGround", L"GameObject_SkyBox", 0.f, -30.f, 0.f, -90.f, 0.f, 0.f);

	CreateObject(L"Layer_BackGround", L"GameObject_Caeser", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ClothUpper", XMFLOAT3(-9.0, 60.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.0118267321586609, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand02", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand02Piece", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand03", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand03Piece", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand04", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_BasicStand04004", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock002", XMFLOAT3(8.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock003", XMFLOAT3(78.4, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock004", XMFLOAT3(6.4, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock005", XMFLOAT3(-29.4, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock007", XMFLOAT3(-10.8, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock008", XMFLOAT3(25.7, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SunBlock001", XMFLOAT3(0.0, -1.3, 40.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.0102879130840302, 0.0102879154682159, 0.0102879154682159));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane2", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.13f, 0.1f, 0.13f));
	CreateObject(L"Layer_BackGround", L"GameObject_Caeser_Stairs", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_MiniGate.001", XMFLOAT3(-72.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_MiniGate.002", XMFLOAT3(-72.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_MiniGate.003", XMFLOAT3(-72.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_MiniGateStand001", XMFLOAT3(-72.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_MiniGateWood001", XMFLOAT3(-57.4, 0.0, 0.0), XMFLOAT3(270.0, 90.0, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_MiniGateWood002", XMFLOAT3(57.4, 0.0, 0.0), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(4.9, 3.0, -29.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.00800000011920929, 0.00800000250339508, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(17.9, 3.0, -28.4), XMFLOAT3(270.0, 352.0, 0.0), XMFLOAT3(0.00800000369548798, 0.00800000190734863, 0.00800000011920929));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-57.3, 3.0, -6.0), XMFLOAT3(270.0, 72.4, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(51.2, 3.0, -16.4), XMFLOAT3(270.0, 311.9, 0.0), XMFLOAT3(0.00800000548362732, 0.0080000102519989, 0.00800000905990601));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(41.2, 3.0, -22.9), XMFLOAT3(270.0, 331.6, 0.0), XMFLOAT3(0.00800000369548798, 0.00800000190734863, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(57.2, 3.0, 5.8), XMFLOAT3(270.0, 252.4, 0.0), XMFLOAT3(0.01, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-29.9, 3.0, 26.4), XMFLOAT3(270.0, 164.9, 0.0), XMFLOAT3(0.00800000905990601, 0.00799999713897705, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(29.8, 3.0, -26.6), XMFLOAT3(270.0, 344.9, 0.0), XMFLOAT3(0.00800000369548798, 0.00800000488758087, 0.00800000011920929));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(51.2, 3.0, 16.1), XMFLOAT3(270.0, 225.1, 0.0), XMFLOAT3(0.00800000190734863, 0.00800000190734863, 0.00800000011920929));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(41.1, 3.0, 22.6), XMFLOAT3(270.0, 199.6, 0.0), XMFLOAT3(0.00800000846385956, 0.00800000131130219, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(29.7, 3.0, 26.2), XMFLOAT3(270.0, 193.0, 0.0), XMFLOAT3(0.00800000429153442, 0.00800000071525574, 0.00800000011920929));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(6.0, 3.0, 29.0), XMFLOAT3(270.0, 182.8, 0.0), XMFLOAT3(0.01, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(17.8, 3.0, 28.0), XMFLOAT3(270.0, 186.2, 0.0), XMFLOAT3(0.00800000548362732, 0.00800000548362732, 0.00800000011920929));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-51.3, 3.0, -16.3), XMFLOAT3(270.0, 45.1, 0.0), XMFLOAT3(0.00800000071525574, 0.00800000011920929, 0.00800000309944153));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-17.9, 3.0, -28.2), XMFLOAT3(270.0, 6.2, 0.0), XMFLOAT3(0.00799999952316284, 0.00800000369548798, 0.00800000309944153));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-29.8, 3.0, -26.4), XMFLOAT3(270.0, 13.0, 0.0), XMFLOAT3(0.00800000369548798, 0.00799999952316284, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-41.3, 3.0, -22.8), XMFLOAT3(270.0, 19.6, 0.0), XMFLOAT3(0.00799999892711639, 0.00800000071525574, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-57.5, 3.0, 6.0), XMFLOAT3(270.0, 106.4, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000011920929, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-51.3, 3.0, 16.2), XMFLOAT3(270.0, 131.9, 0.0), XMFLOAT3(0.00800000548362732, 0.00800000071525574, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-41.3, 3.0, 22.7), XMFLOAT3(270.0, 151.6, 0.0), XMFLOAT3(0.00800000846385956, 0.00800000250339508, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-18.0, 3.0, 28.2), XMFLOAT3(270.0, 172.0, 0.0), XMFLOAT3(0.00800000667572021, 0.00800000190734863, 0.00800000250339508));
	CreateObject(L"Layer_BackGround", L"GameObject_Flag_01", XMFLOAT3(-6.1, 3.0, 29.0), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(-56.6, 7.0, 0.0), XMFLOAT3(270.0, 90.0, 0.0), XMFLOAT3(0.0120000004768372, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(0.0, 9.8, 33.0), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.0120000052452087, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(55.7, 20.0, -32.4), XMFLOAT3(270.0, 323.4, 0.0), XMFLOAT3(0.0120000004768372, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(26.6, 20.0, -42.7), XMFLOAT3(270.0, 350.7, 0.0), XMFLOAT3(0.0120000004768372, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(-26.5, 20.0, 42.7), XMFLOAT3(270.0, 170.7, 0.0), XMFLOAT3(0.0120000064373016, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(26.4, 20.0, 42.8), XMFLOAT3(270.0, 187.3, 0.0), XMFLOAT3(0.0120000040531158, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(56.6, 7.0, 0.0), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.012000002861023, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(-26.3, 20.0, -42.8), XMFLOAT3(270.0, 7.3, 0.0), XMFLOAT3(0.0120000004768372, 0.0100000011920929, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(-56.0, 20.0, -32.3), XMFLOAT3(270.0, 36.4, 0.0), XMFLOAT3(0.0120000004768372, 0.0100000011920929, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(0.1, 20.0, -44.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.0120000004768372, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(0.0, 1.1, 26.8), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.00600000262260437, 0.0050000011920929, 0.0050000011920929));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(56.1, 20.0, 32.2), XMFLOAT3(270.0, 216.4, 0.0), XMFLOAT3(0.0120000052452087, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_GoldenLionhead", XMFLOAT3(-55.7, 20.0, 32.5), XMFLOAT3(270.0, 143.4, 0.0), XMFLOAT3(0.0120000040531158, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Column", XMFLOAT3(56.5, 0.0, 3.1), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000047683716));
	CreateObject(L"Layer_BackGround", L"GameObject_Column", XMFLOAT3(-56.3, 0.0, 3.1), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000047683716));
	CreateObject(L"Layer_BackGround", L"GameObject_Column", XMFLOAT3(-56.3, 0.0, -3.1), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000047683716));
	CreateObject(L"Layer_BackGround", L"GameObject_Column", XMFLOAT3(56.5, 0.0, -3.1), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000047683716));
	CreateObject(L"Layer_BackGround", L"GameObject_CaeserPilar", XMFLOAT3(-3.0, 3.0, 27.6), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.005, 0.005, 0.005));
	CreateObject(L"Layer_BackGround", L"GameObject_CaeserPilar", XMFLOAT3(3.0, 3.0, 27.6), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.005, 0.005, 0.005));
	CreateObject(L"Layer_BackGround", L"GameObject_Accessories", XMFLOAT3(-3.0, 3.0, 27.6), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.00999999880790711, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Accessories", XMFLOAT3(3.0, 3.0, 27.6), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.00999999880790711, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Plane.002", XMFLOAT3(0.0, 4.3, 2.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-56.1, 2.4, -7.8), XMFLOAT3(270.0, 254.3, 0.0), XMFLOAT3(0.0108540010452271, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-54.7, 2.4, -11.6), XMFLOAT3(270.0, 239.6, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-52.5, 2.4, -14.8), XMFLOAT3(270.0, 225.8, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-49.8, 2.4, -17.4), XMFLOAT3(270.0, 213.9, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-46.5, 2.4, -19.5), XMFLOAT3(270.0, 206.1, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-43.0, 2.4, -21.4), XMFLOAT3(270.0, 206.1, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-39.4, 2.4, -23.0), XMFLOAT3(270.0, 198.2, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-35.7, 2.4, -24.3), XMFLOAT3(270.0, 194.6, 0.0), XMFLOAT3(0.00999999880790711, 0.0100000011920929, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-31.9, 2.4, -25.3), XMFLOAT3(270.0, 190.8, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-28.1, 2.4, -26.2), XMFLOAT3(270.0, 190.8, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-24.2, 2.4, -27.1), XMFLOAT3(270.0, 189.2, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-20.3, 2.4, -27.8), XMFLOAT3(270.0, 185.3, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-16.4, 2.4, -28.2), XMFLOAT3(270.0, 183.6, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-12.5, 2.4, -28.5), XMFLOAT3(270.0, 179.7, 0.0), XMFLOAT3(0.0100000059604645, 0.0100000059604645, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-8.6, 2.4, -28.7), XMFLOAT3(270.0, 179.7, 0.0), XMFLOAT3(0.0100000059604645, 0.0100000059604645, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-4.7, 2.4, -28.7), XMFLOAT3(270.0, 176.3, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-0.8, 2.4, -28.7), XMFLOAT3(270.0, 176.3, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(3.1, 2.4, -28.5), XMFLOAT3(270.0, 174.7, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(7.0, 2.4, -28.3), XMFLOAT3(270.0, 173.5, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(10.9, 2.4, -28.0), XMFLOAT3(270.0, 173.5, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(14.8, 2.4, -27.8), XMFLOAT3(270.0, 173.5, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(18.7, 2.4, -27.5), XMFLOAT3(270.0, 173.5, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(22.6, 2.4, -27.1), XMFLOAT3(270.0, 169.2, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(26.4, 2.4, -26.5), XMFLOAT3(270.0, 167.6, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(30.2, 2.4, -25.8), XMFLOAT3(270.0, 167.6, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(34.1, 2.4, -24.9), XMFLOAT3(270.0, 162.3, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(37.9, 2.4, -23.9), XMFLOAT3(270.0, 162.3, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(41.6, 2.4, -22.5), XMFLOAT3(270.0, 153.1, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(45.1, 2.4, -20.7), XMFLOAT3(270.0, 145.6, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(48.3, 2.4, -18.5), XMFLOAT3(270.0, 140.4, 0.0), XMFLOAT3(0.01, 0.0100000047683716, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(51.3, 2.4, -15.8), XMFLOAT3(270.0, 130.9, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(53.8, 2.4, -12.8), XMFLOAT3(270.0, 123.1, 0.0), XMFLOAT3(0.00999999880790711, 0.0100000011920929, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(55.7, 2.4, -9.4), XMFLOAT3(270.0, 110.1, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(56.7, 2.4, -5.5), XMFLOAT3(270.0, 94.9, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(56.7, 2.4, 0.1), XMFLOAT3(270.0, 90.8, 0.0), XMFLOAT3(0.017751796245575, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(56.6, 2.4, 5.5), XMFLOAT3(270.0, 74.3, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(55.4, 2.4, 9.3), XMFLOAT3(270.0, 66.7, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(53.7, 2.4, 12.8), XMFLOAT3(270.0, 58.2, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(51.3, 2.4, 15.8), XMFLOAT3(270.0, 40.3, 0.0), XMFLOAT3(0.01, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(48.3, 2.4, 18.4), XMFLOAT3(270.0, 32.3, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(45.0, 2.4, 20.5), XMFLOAT3(270.0, 28.9, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(41.4, 2.4, 22.3), XMFLOAT3(270.0, 20.4, 0.0), XMFLOAT3(0.01, 0.0100000059604645, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(37.8, 2.4, 23.8), XMFLOAT3(270.0, 18.8, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(34.0, 2.4, 25.1), XMFLOAT3(270.0, 13.2, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(30.2, 2.4, 26.0), XMFLOAT3(270.0, 9.6, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(26.3, 2.4, 26.8), XMFLOAT3(270.0, 7.0, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(22.4, 2.4, 27.4), XMFLOAT3(270.0, 6.0, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(18.5, 2.4, 27.9), XMFLOAT3(270.0, 3.8, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(14.6, 2.4, 28.2), XMFLOAT3(270.0, 2.7, 0.0), XMFLOAT3(0.01, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(10.6, 2.4, 28.5), XMFLOAT3(270.0, 0.4, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(6.7, 2.4, 28.6), XMFLOAT3(270.0, 356.4, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(4.5, 2.4, 28.7), XMFLOAT3(270.0, 266.4, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-4.5, 2.4, 28.8), XMFLOAT3(270.0, 86.4, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-3.8, 2.4, 28.8), XMFLOAT3(270.0, 356.4, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(0.2, 2.4, 27.1), XMFLOAT3(270.0, 356.4, 0.0), XMFLOAT3(0.0232822799682617, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-56.8, 2.4, 0.1), XMFLOAT3(270.0, 267.6, 0.0), XMFLOAT3(0.0295846176147461, 0.0100000047683716, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-56.0, 2.4, 8.0), XMFLOAT3(270.0, 289.7, 0.0), XMFLOAT3(0.0129740023612976, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-54.2, 2.4, 12.0), XMFLOAT3(270.0, 297.3, 0.0), XMFLOAT3(0.0100000023841858, 0.0100000059604645, 0.0100000071525574));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-52.0, 2.4, 15.1), XMFLOAT3(270.0, 306.0, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-49.2, 2.4, 17.9), XMFLOAT3(270.0, 321.2, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-45.9, 2.4, 20.1), XMFLOAT3(270.0, 326.5, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-42.5, 2.4, 21.9), XMFLOAT3(270.0, 333.8, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-38.9, 2.4, 23.3), XMFLOAT3(270.0, 339.1, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-35.2, 2.4, 24.4), XMFLOAT3(270.0, 340.8, 0.0), XMFLOAT3(0.0100000011920929, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-31.4, 2.4, 25.5), XMFLOAT3(270.0, 343.3, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-27.5, 2.4, 26.4), XMFLOAT3(270.0, 346.2, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-23.7, 2.4, 27.1), XMFLOAT3(270.0, 347.8, 0.0), XMFLOAT3(0.01, 0.0100000059604645, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-19.8, 2.4, 27.6), XMFLOAT3(270.0, 351.2, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000047683716, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-15.9, 2.4, 28.0), XMFLOAT3(270.0, 352.3, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000023841858));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-11.9, 2.4, 28.4), XMFLOAT3(270.0, 353.0, 0.0), XMFLOAT3(0.01, 0.0100000035762787, 0.0100000047683716));
	CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(-8.0, 2.4, 28.6), XMFLOAT3(270.0, 356.4, 0.0), XMFLOAT3(0.00999999940395355, 0.0100000047683716, 0.0100000047683716));

	pMagicCircle = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_MagicCircle", XMFLOAT3(0.f, 0.1f, 0.f), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.01, 0.01, 0.01)));
	pMagicCircle->SetRender(false);
	pPosion = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_Item", XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02)));

	//CreateObject(L"Layer_ColObject", L"GameObject_ColosseumPiece_01", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0110000050067902, 0.0100000023841858, 0.0100000023841858));
	//pBoundingBox[0] = dynamic_cast<CBoundingBox*>(CreateObject(L"Layer_BackGround", L"GameObject_BoundingBox", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.6f, 2.4f, 1.4f)));
	//pBoundingBox[1] = dynamic_cast<CBoundingBox*>(CreateObject(L"Layer_BackGround", L"GameObject_BoundingBox", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.5f, 0.3f, 0.5f)));
	::Player->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.f, 0.f);
	Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);
	Player->SetPlayerForce(0.f, 0.f, 0.f);
	Player->m_pPhysicCom->SetVel(0.f, 0.f, 0.f);
	Player->m_pPhysicCom->SetAcc(0.f, 0.f, 0.f);
	m_listColObject = CObjectManager::GetInstance()->Find_Layer(SCENE_BULLFIGHTMAP, L"Layer_ColObject")->Get_GameObject();

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BULLFIGHTMAP, L"Layer_BackGround", L"GameObject_BullFont", (CGameObject**)&m_pEndText)))
		return E_FAIL;
	m_pEndText->SetText(L"", XMFLOAT2(0.45f, 0.45f), XMFLOAT2(2.f, 2.f));
	m_pEndText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BULLFIGHTMAP, L"Layer_BackGround", L"GameObject_BullFont", (CGameObject**)&m_pStatTimeText)))
		return E_FAIL;
	m_pStatTimeText->SetText(L"", XMFLOAT2(0.5f, 0.38f), XMFLOAT2(2.f, 2.f));
	m_pStatTimeText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_pStatTimeText->SetHide(true);

	CreateObject(L"Layer_BackGround", L"GameObject_Run", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
	CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BULLFIGHTMAP, L"Layer_BackGround", L"GameObject_BullFont", (CGameObject**)&pButtonText)))
		return E_FAIL;
	pButtonText->SetText(L"Arrow", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
	pButtonText->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	//CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	loadcomplete = false;

	CManagement::GetInstance()->GetShadowMap()->SetDirectionPos(XMFLOAT3(10.f, 100.f, 0.f));
	CManagement::GetInstance()->GetShadowMap()->SetTargetPos(XMFLOAT3(0.f, 0.f, 0.f));

	if (!SERVERCONNECT)
		gameStarted = true;
	
	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Bullfightmap", 1.f, 0, true); //배경음악 재생
	ShowCursor(false);

	return NOERROR;

}

short CScene_Bullfightmap::KeyEvent(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
		return 0;

	if (!gameStarted)
	{
		return 0;
	}

	if (gameOver)
		return 0;

	if (Player->GetDeath())
		return 0;

	CKeyManager* pKeyMgr = CKeyManager::GetInstance();


	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
	Player->SetPlayerForceX(0.f);
	Player->SetPlayerForceZ(0.f);
	if (Player->GetController() == false) {
		Player->xmf3Shift = xmf3Shift;
		return 0;
	}

	static bool isClip = true;

	if (GetAsyncKeyState(VK_ESCAPE) & 0x8000)
	{
		if (isClip)
			isClip = false;
		else
			isClip = true;
	}

	if (isClip)
	{
		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos, centerPt;
		centerPt.x = WINDOWX / 2;
		centerPt.y = WINDOWY / 2;

		GetCursorPos(&ptCursorPos);
		cxDelta = (float)(ptCursorPos.x - centerPt.x) / 3.0f;
		cyDelta = (float)(ptCursorPos.y - centerPt.y) / 3.0f;
		SetCursorPos(centerPt.x, centerPt.y);
		if (cxDelta || cyDelta)
		{
			Player->Rotate(cyDelta, cxDelta, 0.0f);
			/*
			if (pKeyMgr->KeyPressing(KEY_RBUTTON))
				Rotate(cyDelta, cxDelta, 0.0f);
			else
				Rotate(cyDelta, 0.0f, -cxDelta);
			*/
		}
	}

	float forceAmount = 300.f;
	if (pKeyMgr->KeyPressing(KEY_W))
	{
		Player->GivePlayerForce(0.f, 0.f, forceAmount);
		//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmLook, PLAYERSPEED * TimeDelta);
	}
	if (pKeyMgr->KeyPressing(KEY_S))
	{
		Player->GivePlayerForce(0.f, 0.f, -forceAmount);
		//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmLook, -PLAYERSPEED * TimeDelta);
	}
	if (pKeyMgr->KeyPressing(KEY_A))
	{
		Player->GivePlayerForce(-forceAmount, 0.f, 0.f);
		//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmRight, -PLAYERSPEED * TimeDelta);
	}
	if (pKeyMgr->KeyPressing(KEY_D))
	{
		Player->GivePlayerForce(forceAmount, 0.f, 0.f);
		//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmRight, PLAYERSPEED * TimeDelta);
	}
	if (pKeyMgr->KeyPressing(KEY_UP))
	{
		Player->GivePlayerForce(0.f, 10, 0.f);
		//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmUp, PLAYERSPEED * TimeDelta);
	}
	else if (pKeyMgr->KeyPressing(KEY_DOWN))
	{
		Player->GivePlayerForce(0.f, -10, 0.f);
		//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmUp, -PLAYERSPEED * TimeDelta);
	}

	if(Vector3::Length(xmf3Shift) > 0)
		Player->xmf3Shift = xmf3Shift;

	//키입력이 없으면서 진행중인 애니메이션이 idle이 아니면 Player의 m_IsMoving을 false로 바꾸도록하는 패킷 전송

	if (pKeyMgr->KeyPressing(KEY_F1))
	{
		if (SERVERCONNECT)
		{
			if (!Player->GetDeath())
			{
				Player->SetDeath(true);

				packet_scenechange p;
				p.id = Player->Get_ClientIndex();
				p.size = sizeof(packet_scenechange);
				p.type = C2S_SCENECHANGE;
				p.x = Player->Get_CurrentPlatformPos().x;
				p.y = Player->Get_CurrentPlatformPos().y;
				p.z = Player->Get_CurrentPlatformPos().z;
				p.lookx = Player->Get_BeforeLookVector().x;
				p.looky = Player->Get_BeforeLookVector().y;
				p.lookz = Player->Get_BeforeLookVector().z;
				p.map = S_BOARDMAP;
				CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);
			}
		}
		else
		{
			Scene_Change(SCENE_BOARDMAP);
		}
	}


	return NOERROR;
}

short CScene_Bullfightmap::Update_Scene(double TimeDelta)
{
	if (pMagicCircle->GetRender() == true)
	{
		pMagicCircle->m_pTransformCom->Rotate(0.f, 1.f, 0.f);
	}

	if (SERVERCONNECT == true)
	{
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->m_AnimState == ANIM_STANDUP)
			{
				if (pMagicCircle->GetRender() == false)
				{
					pMagicCircle->m_pTransformCom->SetPosition(Players[i]->m_tPlayerInfo.xmPosition.x, 0.1f, Players[i]->m_tPlayerInfo.xmPosition.z);
					pMagicCircle->SetRender(true);
				}
				break;
			}

			if (i == 2)
			{
				if (pMagicCircle->GetRender())
					pMagicCircle->SetRender(false);
			}
		}
	}

	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
	{
		if (m_pLoading == nullptr)
		{
			m_pLoading = CLoading::Create(SCENE_WAITINGROOM);
		}
		else
		{
			if (m_pLoading->Get_Finish())
			{
				CManagement* pManagement = CManagement::GetInstance();

				pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

				if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_BULLFIGHTMAP)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
					return -1;
			}
		}
		return 0;
	}

	if (!m_bStartAsk)
	{
		cameraAnimation(TimeDelta);
	}

	if (!loadcomplete) {

		m_iStartTime = CServerManager::GetInstance()->Get_StartTime();

		if (m_iStartTime > 0)
			m_pStatTimeText->SetTextString(to_wstring(m_iStartTime));
		else {
			packet_loadingsuccess p2;
			p2.size = sizeof(packet_loadingsuccess);
			p2.type = C2S_MINIGAMELOADING;
			p2.scene = S_BULLFIGHTMAP;
			CServerManager::GetInstance()->SendData(C2S_MINIGAMELOADING, &p2);
			CServerManager::GetInstance()->Set_StartTime(3);

			loadcomplete = true;

			m_pStatTimeText->SetTextString(L"");
		}
	}

	if (m_bEndAsk)
	{
		int m_iEndTime = CServerManager::GetInstance()->Get_StartTime();

		if (m_iEndTime == 0) {

			packet_scenechange p;
			p.id = Player->Get_ClientIndex();
			p.size = sizeof(packet_scenechange);
			p.type = C2S_SCENECHANGE;
			p.x = Player->Get_CurrentPlatformPos().x;
			p.y = Player->Get_CurrentPlatformPos().y;
			p.z = Player->Get_CurrentPlatformPos().z;
			p.lookx = Player->Get_BeforeLookVector().x;
			p.looky = Player->Get_BeforeLookVector().y;
			p.lookz = Player->Get_BeforeLookVector().z;
			p.map = S_BOARDMAP;
			CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

			CServerManager::GetInstance()->Set_StartTime(3);
		}
	}

	if (CServerManager::GetInstance()->Get_BoardmapReady() == 3)
	{
		CServerManager::GetInstance()->Set_Boardmapinit();
		Scene_Change(SCENE_BOARDMAP);
		return 0;
	}

	if (CServerManager::GetInstance()->Get_MinigameReady())
	{
		gameStarted = true;
		CServerManager::GetInstance()->Set_MinigameReady(false);
	}

	if (CServerManager::GetInstance()->Get_MiniWinner() >= 0)
	{
		if (Player->GetDeath())
		{
			if (GoUp) //죽고나서 위로 다 승천 했으면 
			{
				if (!m_bEndAsk) //종료 3초 요청
				{
					m_iEndTime = 3;
					m_bEndAsk = true;

					packet_starttimeask ps;
					ps.size = sizeof(packet_starttimeask);
					ps.type = C2S_STARTTIMER;
					ps.id = Player->Get_ClientIndex();

					CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
				}

				winner = CServerManager::GetInstance()->Get_MiniWinner();

				Player->xmf3Shift = { 0.f,0.f,0.f };

				//미니게임 승자 ui 띄우기

				for (int i = 0; i < 3; ++i)
				{
					if (Players[i]->Get_ClientIndex() == winner)
					{
						wstring w = L"";
						string name = Players[i]->Get_Name();

						w.assign(name.begin(), name.end());
						w += L" Win";
						m_pEndText->SetTextString(w);
						Players[i]->SetWinner(true);
					}
					else
					{
						Players[i]->SetWinner(false);
					}

					Players[i]->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
					Players[i]->SetPlayerForce(0.f, 0.f, 0.f);
					Players[i]->m_pPhysicCom->SetAcc(0.f, 0.f, 0.f);
					Players[i]->m_pPhysicCom->SetVel(0.f, 0.f, 0.f);
				}
				gameOver = true;
				//Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

				CServerManager::GetInstance()->Set_MiniWinner(-1);
			}
		}
		else { //죽지 않았는데 승자가나옴 -> 내가 우승
			if (!m_bEndAsk) //종료 3초 요청
			{
				m_iEndTime = 3;
				m_bEndAsk = true;

				packet_starttimeask ps;
				ps.size = sizeof(packet_starttimeask);
				ps.type = C2S_STARTTIMER;
				ps.id = Player->Get_ClientIndex();

				CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
			}

			winner = CServerManager::GetInstance()->Get_MiniWinner();

			Player->xmf3Shift = { 0.f,0.f,0.f };
			Player->SetSmall(true);

			for (int i = 0; i < 3; ++i)
			{
				if (Players[i]->Get_ClientIndex() == winner)
				{
					wstring w = L"";
					string name = Players[i]->Get_Name();

					w.assign(name.begin(), name.end());
					w += L" Win";
					m_pEndText->SetTextString(w);
					Players[i]->SetWinner(true);
				}
				else
				{
					Players[i]->SetWinner(false);
				}

				Players[i]->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
				Players[i]->SetPlayerForce(0.f, 0.f, 0.f);
				Players[i]->m_pPhysicCom->SetAcc(0.f, 0.f, 0.f);
				Players[i]->m_pPhysicCom->SetVel(0.f, 0.f, 0.f);
			}

			if (Player->Get_ClientIndex() == winner)
			{
				CSoundManager::GetInstance()->Play_Sound(L"Mingame_Win");
			}
			else
			{
				CSoundManager::GetInstance()->Play_Sound(L"Minigame_Lose");
			}

			gameOver = true;
			//Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

			CServerManager::GetInstance()->Set_MiniWinner(-1);
		}
	}

	if (!gameOver)
	{
		if (SERVERCONNECT) {
			if (CServerManager::GetInstance()->Get_PotionCreate())
			{
				pPosion->m_pTransformCom->SetPosition(CServerManager::GetInstance()->Get_PotionPosition());
			}
			else {
				pPosion->m_pTransformCom->SetPosition(0.f, -1000.f, 0.f);
			}

			if (gameStarted) {
				moveDelta += TimeDelta;

				if (moveDelta > MOVEPACKETDELTA)
				{
					packet_move p;
					ZeroMemory(&p, sizeof(p));
					p.type = C2S_MOVE;
					p.size = sizeof(packet_move);
					p.id = Player->Get_ClientIndex();
					p.TimeDelta = TimeDelta;

					XMFLOAT3 look = Vector3::Normalize(Player->GetLookVector());

					p.mx = Player->m_tPlayerInfo.xmPosition.x;
					p.my = Player->m_tPlayerInfo.xmPosition.y;
					p.mz = Player->m_tPlayerInfo.xmPosition.z;

					p.lookx = look.x;
					p.looky = look.y;
					p.lookz = look.z;

					CServerManager::GetInstance()->SendData(C2S_MOVE, &p);
					moveDelta = 0.f;

				}
			}
		}
	}
	

	if (Player->GetDeath())
	{
		if (!gameOver) {
			GoUp = false;
			Player->Rotate(0.f, 10.f, 0.f);
			if (Player->GetPosition().y > 20.f) //플레이어가 올라가는 중이면 올라간 후에 보내기
			{
				GoUp = true;
				return 0;
			}
		}
	}

	//Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);
	if(Player->GetController() && gameOver == false)
		Player->xmf3Shift = Player->m_pPhysicCom->UpdatePhysics(Player->m_tPlayerInfo.xmPosition, Player->m_tPlayerInfo.xmRight,
			Player->m_tPlayerInfo.xmUp, Player->m_tPlayerInfo.xmLook, Player->GetPlayerForce(), Player->GetState(), TimeDelta);


	BoundingBox xmBoundingBox;
	XMFLOAT3 tmp;
	tmp = Player->m_tPlayerInfo.xmPosition;
	xmBoundingBox = Player->Get_TeddyBear()->m_xmBoundingBox;
	Player->Get_TeddyBear()->m_pTransformCom->SetPosition(Vector3::Add(Player->Get_TeddyBear()->m_pTransformCom->GetPosition(), Player->xmf3Shift));
	xmBoundingBox.Transform(xmBoundingBox, XMLoadFloat4x4(&Player->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
	
	Player->m_tPlayerInfo.xmPosition = tmp;

	for (auto ColObject : m_listColObject)
	{
		CStaticObject* cs = dynamic_cast<CStaticObject*>(ColObject);

		if (xmBoundingBox.Intersects(cs->GetBoudingBox()))
		{
			Player->xmf3Shift = XMFLOAT3(0.f,0.f,0.f);
			break;
		}
	}
	return 0;
}

short CScene_Bullfightmap::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_Bullfightmap::Render_Scene()
{
	return NOERROR;
}

HRESULT CScene_Bullfightmap::Scene_Change(SCENEID Scene)
{
	CManagement* pManagement = CManagement::GetInstance();

	Player->SetBig(false);
	Player->SetSmall(false);
	Player->SetScale(XMFLOAT3(2.f, 2.f, 2.f));

	if (nullptr == pManagement)
		return -1;

	if (!SERVERCONNECT) {
		::Player->m_tPlayerInfo.xmPosition = ::Player->Get_CurrentPlatformPos();
		::Player->m_tPlayerInfo.xmLook = ::Player->Get_BeforeLookVector();
		::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
		::Player->m_AnimState = ANIM_IDLE;
		::Player->SetAnimation("idle1");
	}
	else
	{
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
			{
				CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
				dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);
			}

			//Players[i]->m_AnimState = ANIM_IDLE;
			Players[i]->SetAnimation("run");

		}
	}

	CManagement::GetInstance()->GetShadowMap()->SetDirectionPos(XMFLOAT3(89.8112, 100.2391, -97.7247));
	CManagement::GetInstance()->GetShadowMap()->SetTargetPos(XMFLOAT3(-14.0604, 15.6328, 8.74705));

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Boardmap", 1.f, 0, true); //배경음악 재생
	ShowCursor(true);

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");

	pManagement->Set_CurrentSceneId(Scene);

	if (FAILED(pManagement->Initialize_Current_Scene(m_pScene_Boardmap)))
		return -1;

	if (FAILED(pManagement->Clear_Scene(SCENE_BULLFIGHTMAP)))
		return -1;
}

CScene_Bullfightmap* CScene_Bullfightmap::Create(CScene_Boardmap* pSceneBoardmap)
{
	CScene_Bullfightmap* pInstance = new CScene_Bullfightmap();

	if (pInstance->Initialize_Scene(pSceneBoardmap))
	{
		MSG_BOX("CScene_Bullfightmap Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

short CScene_Bullfightmap::cameraAnimation(double TimeDelta)
{
	m_fCameratime += TimeDelta;

	CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
	pCameraThird->GetPlayer()->Rotate(0.f, 21 * TimeDelta, 0.f);
	pCameraThird->SetLookAt(XMFLOAT3(20.f, 10.f, 30.f - m_fCameratime * 5.f));
	pCameraThird->SetOffset(XMFLOAT3(50.f, 100.f, 25.f));

	if (m_fCameratime > 10.f)
	{
		pCameraThird->SetOffset(XMFLOAT3(0.0f, 7.2f, -12.0f));
		m_bStartAsk = true;
		m_pStatTimeText->SetHide(false);
		if (SERVERCONNECT) {
			//게임 시작 완료 패킷 서버에 보내기
			packet_starttimeask ps;
			ps.size = sizeof(packet_starttimeask);
			ps.type = C2S_STARTTIMER;
			ps.id = Player->Get_ClientIndex();

			CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
			CSoundManager::GetInstance()->Play_Sound(L"shout");
		}
	}

	return 0;
}

HRESULT CScene_Bullfightmap::Release()
{
	delete this;

	return NOERROR;
}
