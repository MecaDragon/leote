#include "BillBoardShader.h"

CBillBoardShader::CBillBoardShader()
	: CShader()
{
}

CBillBoardShader::CBillBoardShader(const CBillBoardShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

CBillBoardShader::~CBillBoardShader(void)
{
}

HRESULT CBillBoardShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState(D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT);
	m_pPipelineState->SetName(L"BillBoardShaderr PipelineState");
	return NOERROR;
}

HRESULT CBillBoardShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CBillBoardShader::Clone_Component(void* pArg)
{
	CBillBoardShader* pInstance = new CBillBoardShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CBillBoardShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CBillBoardShader* CBillBoardShader::Create()
{
	CBillBoardShader* pInstance = new CBillBoardShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CBillBoardShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CBillBoardShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Billboard.hlsl", "VSBillboard", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CBillBoardShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Billboard.hlsl", "PSBillboard", "ps_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CBillBoardShader::Create_GeometryShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Billboard.hlsl", "GSBillboard", "gs_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CBillBoardShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 3;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TEXTURE", 0, DXGI_FORMAT_R32_UINT, 0, 20, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

D3D12_RASTERIZER_DESC CBillBoardShader::Create_RasterizerState()
{
	D3D12_RASTERIZER_DESC d3dRasterizerDesc;
	::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0;
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(d3dRasterizerDesc);
}

HRESULT CBillBoardShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
