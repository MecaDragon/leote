//------------------------------------------------------- ----------------------
// File: Object.h
//-----------------------------------------------------------------------------

#pragma once

#include "Mesh.h"
#include "Camera.h"
#include "Component.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CPhysic final : public CComponent
{
private:
	explicit CPhysic();
	explicit CPhysic(const CPhysic& rhs);
	virtual ~CPhysic(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CPhysic* Create();

	XMFLOAT3 GetVel();
	XMFLOAT3 GetAcc();
	float GetMass();
	bool GetApplyPhysics();

	void SetVel(float x, float y, float z);
	void SetAcc(float x, float y, float z);
	void SetMass(float mass);
	XMFLOAT3 UpdatePhysics(XMFLOAT3 position, XMFLOAT3 rightvector, XMFLOAT3 upvector, XMFLOAT3 lookvector, XMFLOAT3 force, OBJECT_STATE state, double elapsedTimeInSec);
	void SetApplyPhysics(bool bPhy);

	HRESULT Release();

private:
	XMFLOAT3 m_xmVel; // x - right, y - up, z - look
	XMFLOAT3 m_xmAcc;
	float m_fMass;
	bool m_bApplyPhysics;
};

