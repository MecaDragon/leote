#include "Player.h"
#include "PipeLine.h"
#include "DeviceManager.h"
#include "Management.h"
#include "ServerManager.h"
#include "Physic.h"
#include "StaticObject.h"

extern bool G_SC;

CPlayer::CPlayer()
{
}

CPlayer::CPlayer(const CPlayer& rhs)
	: m_iClientIndex(-1), m_playerorder(-1)
{
	LerpTimeDelta = 0.f;
	Add_Component();
	//m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 40.f, -80.f);
	m_xmForce = XMFLOAT3{ 0.f, 0.f, 0.f };
	m_pPhysicCom->SetMass(20.f);

	bCoin = 10;
	bButton = 0;
	bRank = 1;

	m_xmScale = XMFLOAT3(2.f, 2.f, 2.f);
}

CPlayer::~CPlayer()
{
}

HRESULT CPlayer::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CPlayer::Initialize_GameObject(void* pArg)
{
	return NOERROR;
}

short CPlayer::Update_GameObject(double TimeDelta)
{
	m_dTimeDelta = TimeDelta;
	if (m_bIsPlayer || SERVERCONNECT == false) {
		if (m_oState == OBJECT_STATE::OBJSTATE_FLY)
		{
			if (m_AnimState == ANIM_JUMP)
			{
				SetAnimation("jump");
			}
		}
		else if (m_oState == OBJECT_STATE::OBJSTATE_FALLING)
		{
			if (m_AnimState == ANIM_FREEFALL)
			{
				SetAnimation("freefall");
			}
		}
		else if (m_oState == OBJECT_STATE::OBJSTATE_EMO)
		{
			if (m_AnimState == ANIM_STANDUP)
			{
				SetAnimation("standup");
			}
			else if (m_AnimState == ANIM_SIT)
			{
				SetAnimation("sit");
			}
			else if (m_AnimState == ANIM_SAD)
			{
				SetAnimation("idlesad");
			}
			else if (m_AnimState == ANIM_FALL)
			{
				SetAnimation("fall2");
			}
		}
		else if(m_oState == OBJECT_STATE::OBJSTATE_GROUND)
		{
			if (xmf3Shift.x != 0 || xmf3Shift.z != 0)
			{
				SetAnimation("run");
			}
			else
			{
				if(m_iSkinid == 0 || m_iSkinid == 1)
					SetAnimation("idle1");
				else if(m_iSkinid == 2)
					SetAnimation("idle2");
				else if (m_iSkinid == 3)
					SetAnimation("idle3");
			}
		}
	}
	else {
		if (m_AnimState == ANIM_JUMP)
		{
			SetAnimation("jump");
		}
		else if (m_AnimState == ANIM_FREEFALL)
		{
			SetAnimation("freefall");
		}
		else if (m_AnimState == ANIM_RUN)
		{
			SetAnimation("run");
		}
		else if(m_AnimState == ANIM_IDLE)
		{
			if (m_iSkinid == 0 || m_iSkinid == 1)
				SetAnimation("idle1");
			else if (m_iSkinid == 2)
				SetAnimation("idle2");
			else if (m_iSkinid == 3)
				SetAnimation("idle3");
		}
		else if (m_AnimState == ANIM_STANDUP)
		{
			SetAnimation("standup");
		}
		else if (m_AnimState == ANIM_SIT)
		{
			SetAnimation("sit");
		}
		else if (m_AnimState == ANIM_SAD)
		{
			SetAnimation("idlesad");
		}
		else if (m_AnimState == ANIM_FALL)
		{
			SetAnimation("fall2");
		}
	}

	if (!SERVERCONNECT || m_iClientIndex == CServerManager::GetInstance()->Get_ClientIndex() || m_bIsPlayer)
		m_tPlayerInfo.xmPosition = Vector3::Add(m_tPlayerInfo.xmPosition, xmf3Shift);

	ScaleChange(TimeDelta);
	Lerping_Position(TimeDelta);
	OnPrepareRender();
	return 0;
}

void CPlayer::Rotate(float x, float y, float z)
{
	if (x != 0.0f)
	{
		m_fPitch += x;
		if (m_fPitch > +89.0f) { x -= (m_fPitch - 89.0f); m_fPitch = +89.0f; }
		if (m_fPitch < -89.0f) { x -= (m_fPitch + 89.0f); m_fPitch = -89.0f; }
	}
	if (y != 0.0f)
	{
		m_fYaw += y;
		if (m_fYaw > 360.0f) m_fYaw -= 360.0f;
		if (m_fYaw < 0.0f) m_fYaw += 360.0f;
	}
	if (z != 0.0f)
	{
		m_fRoll += z;
		if (m_fRoll > +20.0f) { z -= (m_fRoll - 20.0f); m_fRoll = +20.0f; }
		if (m_fRoll < -20.0f) { z -= (m_fRoll + 20.0f); m_fRoll = -20.0f; }
	}
	if (fabs(x) < fabs(y) && y != 0.0f)
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tPlayerInfo.xmUp), XMConvertToRadians(y));
		m_tPlayerInfo.xmLook = Vector3::TransformNormal(m_tPlayerInfo.xmLook, xmmtxRotate);
		m_tPlayerInfo.xmRight = Vector3::TransformNormal(m_tPlayerInfo.xmRight, xmmtxRotate);
	}
	

	m_tPlayerInfo.xmLook = Vector3::Normalize(m_tPlayerInfo.xmLook);
	m_tPlayerInfo.xmRight = Vector3::CrossProduct(m_tPlayerInfo.xmUp, m_tPlayerInfo.xmLook, true);
	m_tPlayerInfo.xmUp = Vector3::CrossProduct(m_tPlayerInfo.xmLook, m_tPlayerInfo.xmRight, true);
}

void CPlayer::SetAnimation(std::string ClipName)
{
	if (m_pObject)
	{
		if (!SERVERCONNECT) {
			if (ClipName == "walk")
			{
				m_AnimState = ANIM_WALK;
			}
			else if (ClipName == "jump")
			{
				m_AnimState = ANIM_JUMP;

				if (m_pObject->IsEndAnimation())
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}

			}
			else if (ClipName == "run")
			{
				m_AnimState = ANIM_RUN;
				for (int i = 0; i < MAX_DUST; ++i)
				{
					if (m_SmokeObejct[i])
					{
						if (m_SmokeObejct[i]->GetRender())
						{
							m_SmokeObejct[i]->m_pTransformCom->SetScale(0.98f, 0.98f, 0.98f);
							m_dSmokeTime[i] += m_dTimeDelta;

							if (m_dSmokeTime[i] > 0.1f)
							{
								int next = i + 1;
								if (next >= MAX_DUST)
									next = 0;

								if (m_SmokeObejct[next]->GetRender() == false)
								{
									m_SmokeObejct[next]->SetRender(true);
									XMFLOAT3 shift = Vector3::Add(XMFLOAT3(0.f, 0.05f, 0.f), m_tPlayerInfo.xmLook, -0.5f);
									m_SmokeObejct[next]->m_pTransformCom->SetPosition(Vector3::Add(m_tPlayerInfo.xmPosition, shift));
									m_SmokeObejct[next]->m_pTransformCom->SetLook(0.f, 0.f, 1.f);
									m_SmokeObejct[next]->m_pTransformCom->SetRight(1.f, 0.f, 0.f);
									m_SmokeObejct[next]->m_pTransformCom->SetUp(0.f, 1.f, 0.f);
									m_SmokeObejct[next]->m_pTransformCom->SetScale(0.07, 0.07, 0.07);
								}
							}

							if (m_dSmokeTime[i] > 0.5f)
							{
								m_dSmokeTime[i] = 0.f;
								m_SmokeObejct[i]->SetRender(false);
							}
						}
					}
				}
			}
			else if (ClipName == "idle1")
			{
				m_AnimState = ANIM_IDLE;
			}
			else if (ClipName == "freefall")
			{
				m_AnimState = ANIM_FREEFALL;
			}
			else if (ClipName == "standup")
			{
				m_AnimState = ANIM_STANDUP;
				if (m_pObject->IsEndAnimation())
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}
			else if (ClipName == "sit")
			{
				m_AnimState = ANIM_SIT;
				if (m_pObject->IsEndAnimation())
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}
			else if (ClipName == "idlesad")
			{
				m_AnimState = ANIM_SAD;
				if (m_pObject->IsEndAnimation())
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}
			else if (ClipName == "fall2")
			{
				m_AnimState = ANIM_FALL;
				if (m_pObject->IsEndAnimation())
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}

			if (m_tPlayerInfo.m_clipName != ClipName)
			{
				if (m_tPlayerInfo.m_clipName == "run")
				{
					for (int i = 0; i < MAX_DUST; ++i)
					{
						if (m_SmokeObejct[i])
						{
							m_SmokeObejct[i]->SetRender(false);
						}
					}
				}

				if (ClipName == "run")
				{
					for (int i = 0; i < MAX_DUST; ++i)
					{
						if (m_SmokeObejct[i])
						{
							XMFLOAT3 shift = Vector3::Add(XMFLOAT3(0.f, 0.f, 0.f), m_tPlayerInfo.xmLook, -0.5f);
							m_SmokeObejct[i]->m_pTransformCom->SetPosition(Vector3::Add(m_tPlayerInfo.xmPosition, shift));
							m_SmokeObejct[i]->m_pTransformCom->SetLook(Vector3::ScalarProduct(m_tPlayerInfo.xmLook, -1.f, true));
							m_SmokeObejct[i]->m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_SmokeObejct[i]->m_pTransformCom->GetLook(), true));
							m_SmokeObejct[i]->m_pTransformCom->SetUp(0.f, 1.f, 0.f);
							m_SmokeObejct[i]->m_pTransformCom->SetScale(0.03, 0.03, 0.03);
						}
					}
					if(m_SmokeObejct[0])
						m_SmokeObejct[0]->SetRender(true);
				}
				m_tPlayerInfo.m_clipName = ClipName;
				m_pObject->ChangeAnimation(ClipName);
			}
		}
		else {
			if (ClipName == "walk")
			{
				m_AnimState = ANIM_WALK;
			}
			else if (ClipName == "jump")
			{
				m_AnimState = ANIM_JUMP;

				if (m_pObject->IsEndAnimation() && m_bIsPlayer)
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}

			}
			else if (ClipName == "run")
			{
				m_AnimState = ANIM_RUN;
				for (int i = 0; i < MAX_DUST; ++i)
				{
					if (m_SmokeObejct[i])
					{
						if (m_SmokeObejct[i]->GetRender())
						{
							m_SmokeObejct[i]->m_pTransformCom->SetScale(0.98f, 0.98f, 0.98f);
							m_dSmokeTime[i] += m_dTimeDelta;

							if (m_dSmokeTime[i] > 0.1f)
							{
								int next = i + 1;
								if (next >= MAX_DUST)
									next = 0;

								if (m_SmokeObejct[next]->GetRender() == false)
								{
									m_SmokeObejct[next]->SetRender(true);
									XMFLOAT3 shift = Vector3::Add(XMFLOAT3(0.f, 0.05f, 0.f), m_tPlayerInfo.xmLook, -0.5f);
									m_SmokeObejct[next]->m_pTransformCom->SetPosition(Vector3::Add(m_tPlayerInfo.xmPosition, shift));
									m_SmokeObejct[next]->m_pTransformCom->SetLook(0.f, 0.f, 1.f);
									m_SmokeObejct[next]->m_pTransformCom->SetRight(1.f, 0.f, 0.f);
									m_SmokeObejct[next]->m_pTransformCom->SetUp(0.f, 1.f, 0.f);
									m_SmokeObejct[next]->m_pTransformCom->SetScale(0.07, 0.07, 0.07);
								}
							}

							if (m_dSmokeTime[i] > 0.5f)
							{
								m_dSmokeTime[i] = 0.f;
								m_SmokeObejct[i]->SetRender(false);
							}
						}
					}
				}
			}
			else if (ClipName == "idle1" || ClipName == "idle2" || ClipName == "idle3")
			{
				m_AnimState = ANIM_IDLE;
			}
			else if (ClipName == "freefall")
			{
				m_AnimState = ANIM_FREEFALL;
			}
			else if (ClipName == "standup")
			{
				m_AnimState = ANIM_STANDUP;
				if (m_pObject->IsEndAnimation() && m_bIsPlayer)
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}
			else if (ClipName == "sit")
			{
				m_AnimState = ANIM_SIT;
				if (m_pObject->IsEndAnimation() && m_bIsPlayer)
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}
			else if (ClipName == "idlesad")
			{
				m_AnimState = ANIM_SAD;
				if (m_pObject->IsEndAnimation() && m_bIsPlayer)
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}
			else if (ClipName == "fall2")
			{
				m_AnimState = ANIM_FALL;
				if (m_pObject->IsEndAnimation())
				{
					if (m_iSkinid == 0 || m_iSkinid == 1)
						SetAnimation("idle1");
					else if (m_iSkinid == 2)
						SetAnimation("idle2");
					else if (m_iSkinid == 3)
						SetAnimation("idle3");
					m_oState = OBJECT_STATE::OBJSTATE_GROUND;
				}
			}

			if (m_tPlayerInfo.m_clipName != ClipName)
			{

				if (m_tPlayerInfo.m_clipName == "run")
				{
					for (int i = 0; i < MAX_DUST; ++i)
					{
						if (m_SmokeObejct[i])
						{
							m_SmokeObejct[i]->SetRender(false);
						}
					}
				}

				if (ClipName == "run")
				{
					for (int i = 0; i < MAX_DUST; ++i)
					{
						if (m_SmokeObejct[i])
						{
							XMFLOAT3 shift = Vector3::Add(XMFLOAT3(0.f, 0.05f, 0.f), m_tPlayerInfo.xmLook, -0.5f);
							m_SmokeObejct[i]->m_pTransformCom->SetPosition(Vector3::Add(m_tPlayerInfo.xmPosition, shift));
							m_SmokeObejct[i]->m_pTransformCom->SetLook(Vector3::ScalarProduct(m_tPlayerInfo.xmLook, -1.f, true));
							m_SmokeObejct[i]->m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_SmokeObejct[i]->m_pTransformCom->GetLook(), true));
							m_SmokeObejct[i]->m_pTransformCom->SetUp(0.f, 1.f, 0.f);
							m_SmokeObejct[i]->m_pTransformCom->SetScale(0.03, 0.03, 0.03);
						}
					}
					if (m_SmokeObejct[0])
						m_SmokeObejct[0]->SetRender(true);
				}

				m_tPlayerInfo.m_clipName = ClipName;
				m_pObject->ChangeAnimation(ClipName);

				if (m_bIsPlayer)
				{
					if (!m_bSanta)
					{
						if (CManagement::GetInstance()->Get_CurrentSceneId() != SCENEID::SCENE_WAITINGROOM){
							packet_animation p;
							p.anim = m_AnimState;
							p.id = m_iClientIndex;
							p.size = sizeof(packet_animation);
							p.type = C2S_ANIMATION;

							CServerManager::GetInstance()->SendData(C2S_ANIMATION, &p);
						}
					}
					else
					{
						if (m_cOnemindRoll == ROLL_MOVE)
						{
							packet_animation p;
							p.anim = m_AnimState;
							p.id = 0;
							p.size = sizeof(packet_animation);
							p.type = C2S_ONEMINDANIM;

							CServerManager::GetInstance()->SendData(C2S_ONEMINDANIM, &p);
						}
					}
				}
			}
		}
	}
}

void CPlayer::SetAnimationState(char AnimState)
{
	m_AnimState = AnimState;
}

void CPlayer::GivePlayerForce(float x, float y, float z) 
{ 
	m_xmForce.x += x;
	m_xmForce.y += y;
	m_xmForce.z += z;
	if (m_xmForce.x >= 300)
		m_xmForce.x = 300;

	if (m_xmForce.x <= -300)
		m_xmForce.x = -300;

	if (m_xmForce.z >= 300)
		m_xmForce.z = 300;

	if (m_xmForce.z <= -300)
		m_xmForce.z = -300;
}

void CPlayer::GivePlayerForceX(float x)
{
	m_xmForce.x += x;
}

void CPlayer::GivePlayerForceY(float y)
{
	m_xmForce.y += y;
}

void CPlayer::GivePlayerForceZ(float z)
{
	m_xmForce.z += z;
}

void CPlayer::SetPlayerForceX(float x)
{
	m_xmForce.x = x;
}

void CPlayer::SetPlayerForceY(float y)
{
	m_xmForce.y = y;
}

void CPlayer::SetPlayerForceZ(float z)
{
	m_xmForce.z = z;
}

void CPlayer::SetPlayerForce(float x, float y, float z)
{
	m_xmForce.x = x;
	m_xmForce.y = y;
	m_xmForce.z = z;
}

void CPlayer::GivePlayerAcc(float acc)
{
	if (m_fAcc + acc < PLAYERSPEED) {
		m_fAcc += acc;
	}
	else {
		m_fAcc = PLAYERSPEED;
	}
}

void CPlayer::OnPrepareRender()
{
	if (m_pObject)
	{
		m_pObject->m_pTransformCom->SetUp(XMFLOAT3(0.f,1.f,0.f));
		m_pObject->m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_tPlayerInfo.xmLook, true));
		m_pObject->m_pTransformCom->SetLook(m_tPlayerInfo.xmLook);
		m_pObject->m_pTransformCom->SetPosition(m_tPlayerInfo.xmPosition.x, m_tPlayerInfo.xmPosition.y, m_tPlayerInfo.xmPosition.z);
		m_pObject->m_pTransformCom->Rotate(-90.0f, 0.0f, 0.f);
		m_pObject->m_pTransformCom->SetScale(m_xmScale.x, m_xmScale.y, m_xmScale.z);
	}
}

short CPlayer::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CPlayer::Render_GameObject()
{
	return NOERROR;
}

CPlayer* CPlayer::Create()
{
	CPlayer* pInstance = new CPlayer();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CPlayer::Clone_GameObject(void* pArg)
{
	CPlayer* pInstance = new CPlayer(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

void CPlayer::SetLerpStart(packet_move p)
{
	lerping = true;

	StartPos = m_tPlayerInfo.xmPosition;
	StartLook = m_tPlayerInfo.xmLook;

	DestPos = XMFLOAT3{ p.mx, p.my, p.mz };
	DestLook = XMFLOAT3{ p.lookx, p.looky, p.lookz };

	LerpTimeDelta = 0.f;

}

void CPlayer::Lerping_Position(double TimeDelta)
{
	if (!lerping) {
		return;
	}

	LerpTimeDelta += (float)(TimeDelta) / MOVEPACKETDELTA;

	XMVECTOR pos = XMVectorLerp(XMLoadFloat3(&StartPos), XMLoadFloat3(&DestPos), LerpTimeDelta);
	XMStoreFloat3(&m_tPlayerInfo.xmPosition, pos);

	XMVECTOR look = XMVectorLerp(XMLoadFloat3(&StartLook), XMLoadFloat3(&DestLook), LerpTimeDelta);
	XMStoreFloat3(&m_tPlayerInfo.xmLook, look);

	m_tPlayerInfo.xmUp = { 0.f,1.f,0.f };
	m_tPlayerInfo.xmRight = Vector3::CrossProduct(m_tPlayerInfo.xmUp, m_tPlayerInfo.xmLook);

	if (LerpTimeDelta >= 1.f)
	{
		LerpTimeDelta = 0.f;
		lerping = false;
	}


}

bool CPlayer::IsPlayerAnimEnd()
{
	if (m_pObject)
	{
		return m_pObject->IsEndAnimation();
	}
	return false;
}

bool CPlayer::IsPlayerAnimMiddle()
{
	if (m_pObject)
	{
		return m_pObject->IsMiddleAnimation();
	}
	return false;
}

void CPlayer::SetSkinid(int skin)
{
	if (m_pObject)
	{
		m_iSkinid = skin;
		m_pObject->m_iSkinid = skin;
	}
}

void CPlayer::SetSmokeObject(CStaticObject* smoke[])
{
	for (int i = 0; i < MAX_DUST; ++i)
	{
		m_SmokeObejct[i] = smoke[i];
	}
}

HRESULT CPlayer::Add_Component()
{
	// For.Com_Physic
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Physic", L"Com_Physic", (CComponent**)&m_pPhysicCom)))
		return E_FAIL;
	return NOERROR;
}

void CPlayer::SetObject(CTeddyBear* m_Object)
{
	m_pObject = m_Object;
}

void CPlayer::SetCamera(CCamera* m_Camera)
{
	m_pCamera = m_Camera;
}

void CPlayer::CreateShaderVariables()
{
	
}

void CPlayer::SetBig(bool flag)
{ 
	m_bBig = flag;
	if (m_bBig)
	{
		m_AnimState = ANIM_STANDUP;
		m_oState = OBJECT_STATE::OBJSTATE_EMO;
	}
}

void CPlayer::SetSmall(bool flag)
{ 
	m_bSmall = flag; 
	if (m_bSmall)
	{
		m_AnimState = ANIM_SIT;
		m_oState = OBJECT_STATE::OBJSTATE_EMO;
	}
}

void CPlayer::ScaleChange(double TimeDelta)
{
	if (m_bBig)
	{
		scaleTime += TimeDelta;
		m_bControll = false;
		if (scaleTime > 0.1f)
		{
			GiveScale(0.25f);
			if (m_xmScale.x >= 5.f)
			{
				SetScale(XMFLOAT3(5.f, 5.f, 5.f));
				m_bBig = false;
				m_bControll = true;
			}
			scaleTime = 0.f;
		}
	}

	if (m_bSmall)
	{
		scaleTime += TimeDelta;
		m_bControll = false;
		if (scaleTime > 0.1f)
		{
			GiveScale(-0.25f);
			if (m_xmScale.x <= 2.f)
			{
				SetScale(XMFLOAT3(2.f, 2.f, 2.f));
				m_bSmall = false;
				m_bControll = true;
			}
			scaleTime = 0.f;
		}
	}
}

HRESULT CPlayer::Release()
{

	delete this;

	return NOERROR;
}
