#include "stdafx.h"
#include "ChickenCoop.h"
#include "Management.h"
#include "Material.h"
#include "ShadowShader.h"
#include "Renderer.h"

CChickenCoop::CChickenCoop()
	: CGameObject()
{

}

CChickenCoop::CChickenCoop(const CChickenCoop& rhs)
	: CGameObject(rhs)
{
}

HRESULT CChickenCoop::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CChickenCoop::Initialize_GameObject(void * pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMaterialCom->SetDiffuseRGBA(186, 178, 151, 256);
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
	}
	return NOERROR;
}

short CChickenCoop::Update_GameObject(double TimeDelta)
{

	return short();
}

short CChickenCoop::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CChickenCoop::Render_Shadow()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());
	
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pMeshCom->Render();

	return NOERROR;
}

HRESULT CChickenCoop::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);
	m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CChickenCoop::Add_Component()
{
	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_GGOGGOMAP, L"Component_ChickenCoop_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Shader_FBXOBJ", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CChickenCoop::SetUp_ConstantTable()
{
	return NOERROR;
}

CChickenCoop* CChickenCoop::Create()
{
	CChickenCoop*		pInstance = new CChickenCoop();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CChickenCoop Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject * CChickenCoop::Clone_GameObject(void * pArg)
{
	CChickenCoop*		pInstance = new CChickenCoop(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CChickenCoop Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CChickenCoop::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pMaterialCom);
	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
