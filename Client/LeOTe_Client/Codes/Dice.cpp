#include "stdafx.h"
#include "Dice.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"
#include "Player.h"


CDice::CDice()
	: CGameObject()
{

}

CDice::CDice(const CDice& rhs)
	: CGameObject(rhs)
{
}

HRESULT CDice::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CDice::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;

		m_iCoinType = objw->tag;

		m_xmObjPosition = objw->position;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
	}

	m_pTransformCom->SetScale(0.0008f, 0.0008f, 0.0008f);

	return NOERROR;
}

short CDice::Update_GameObject(double TimeDelta)
{
	if (m_bMiniDice == true)
	{
		if (m_bBuyDice == true)
		{
			m_fTimer += TimeDelta;		
			if (m_fTimer < 1.5f)
			{
				m_xmObjPosition.y -= TimeDelta * 1.8f;
				XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
			else if (m_fTimer >= 1.5f && m_fTimer < 2.5f)
			{
				XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
			else
			{
				XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
				m_xmObjPosition.y -= TimeDelta * 1.8f;

				m_xmScale.x -= TimeDelta * 0.001f;
				m_xmScale.y -= TimeDelta * 0.001f;
				m_xmScale.z -= TimeDelta * 0.001f;

				if (m_xmScale.x < 0.f)
				{
					m_bBuyDice = false;
					m_xmObjPosition.y = 1000.f;
					m_xmScale = XMFLOAT3(0.01f, 0.01f, 0.01f);
					m_fTimer = 0.f;
				}
			}

		}
	}

	return short();
}

short CDice::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	if (m_bMiniDice)
	{
		m_pTransformCom->SetUp(XMFLOAT3(0.f, 1.f, 0.f));
		m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_xmLook, true));
		m_pTransformCom->SetLook(m_xmLook);
		m_pTransformCom->SetPosition(m_xmObjPosition.x, m_xmObjPosition.y, m_xmObjPosition.z);
		m_pTransformCom->SetScale(m_xmScale.x, m_xmScale.y, m_xmScale.z);
		m_pTransformCom->Rotate(0.f, 0.f, 0.f);
	}

	return short();
}

HRESULT CDice::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_DIFFUSE, m_nTexIndex);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CDice::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Dice_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXTex", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_Dice", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CDice::SetUp_ConstantTable()
{
	return NOERROR;
}

CDice* CDice::Create()
{
	CDice* pInstance = new CDice();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CDice Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CDice::Clone_GameObject(void* pArg)
{
	CDice* pInstance = new CDice(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CDice Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CDice::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
