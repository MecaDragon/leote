#include "stdafx.h"
#include "Hexagon.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"
#include "ShadowShader.h"

CHexagon::CHexagon()
	: CGameObject()
{

}

CHexagon::CHexagon(const CHexagon& rhs)
	: CGameObject(rhs)
	, m_strTexCom(rhs.m_strTexCom)
{
}

HRESULT CHexagon::Initialize_GameObject_Prototype(wstring TexComStr)
{
	m_strTexCom = TexComStr;

	return NOERROR;
}

HRESULT CHexagon::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };

	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_xmObjPosition = objw->position;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
		//m_pTransformCom->SetScale(0.367f, 0.367f, 0.367f);
	}

	return NOERROR;
}

short CHexagon::Update_GameObject(double TimeDelta)
{


	return short();
}

short CHexagon::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CHexagon::Render_Shadow()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform);

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	if (m_pTextureCom)
		m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();

	return NOERROR;
}

HRESULT CHexagon::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CHexagon::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_ONEMINDMAP, L"Component_SnowHex_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader //Component_Shader_FBXTexLight
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXTexLight", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_ONEMINDMAP, m_strTexCom.c_str(), L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CHexagon::SetUp_ConstantTable()
{
	return NOERROR;
}

CHexagon* CHexagon::Create(wstring TexComStr)
{
	CHexagon* pInstance = new CHexagon();

	if (pInstance->Initialize_GameObject_Prototype(TexComStr))
	{
		MSG_BOX("CHexagon Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CHexagon::Clone_GameObject(void* pArg)
{
	CHexagon* pInstance = new CHexagon(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CHexagon Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CHexagon::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
