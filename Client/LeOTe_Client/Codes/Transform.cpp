#include "Shader.h"
#include "Management.h"
#include "stdafx.h"
#include "Transform.h"

CTransform::CTransform()
{
}

CTransform::CTransform(const CTransform& rhs)
{
}

CTransform::~CTransform(void)
{
}

HRESULT CTransform::Initialize_Component_Prototype()
{
	

	return NOERROR;
}

HRESULT CTransform::Initialize_Component(void* pArg)
{
	m_xmf4x4World = Matrix4x4::Identity();
	m_xmf4x4ParentsWorld = Matrix4x4::Identity();
	return NOERROR;
}

CComponent* CTransform::Clone_Component(void* pArg)
{
	CTransform* pInstance = new CTransform(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CTransform Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CTransform* CTransform::Create()
{
	CTransform* pInstance = new CTransform();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CTransform Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

void CTransform::SetPosition(float x, float y, float z)
{
	m_xmf4x4World._41 = x;
	m_xmf4x4World._42 = y;
	m_xmf4x4World._43 = z;
}

void CTransform::SetPositionY(float y)
{
	m_xmf4x4World._42 = y;
}

void CTransform::SetPosition(XMFLOAT3 xmf3Position)
{
	SetPosition(xmf3Position.x, xmf3Position.y, xmf3Position.z);
}

void CTransform::SetScale(float x, float y, float z)
{
	XMMATRIX mtxScale = XMMatrixScaling(x, y, z);
	m_xmf4x4World = Matrix4x4::Multiply(mtxScale, m_xmf4x4World);
}

void CTransform::SetRight(float x, float y, float z)
{
	m_xmf4x4World._11 = x;
	m_xmf4x4World._12 = y;
	m_xmf4x4World._13 = z;
}

void CTransform::SetUp(float x, float y, float z)
{
	m_xmf4x4World._21 = x;
	m_xmf4x4World._22 = y;
	m_xmf4x4World._23 = z;
}

void CTransform::SetLook(float x, float y, float z)
{
	m_xmf4x4World._31 = x;
	m_xmf4x4World._32 = y;
	m_xmf4x4World._33 = z;
}

void CTransform::SetRight(XMFLOAT3 xmf3Right)
{
	SetRight(xmf3Right.x, xmf3Right.y, xmf3Right.z);
}

void CTransform::SetLook(XMFLOAT3 xmf3Look)
{
	SetLook(xmf3Look.x, xmf3Look.y, xmf3Look.z);
}

void CTransform::SetUp(XMFLOAT3 xmf3Up)
{
	SetUp(xmf3Up.x, xmf3Up.y, xmf3Up.z);
}

XMFLOAT3 CTransform::GetPosition()
{
	return(XMFLOAT3(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43));
}

XMFLOAT3 CTransform::GetLook()
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._31, m_xmf4x4World._32, m_xmf4x4World._33)));
}

XMFLOAT3 CTransform::GetUp()
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23)));
}

XMFLOAT3 CTransform::GetRight()
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._11, m_xmf4x4World._12, m_xmf4x4World._13)));
}

XMFLOAT4X4 CTransform::GetWorldMat()
{
	return m_xmf4x4World;
}

void CTransform::SetWorldMat(XMFLOAT4X4 worldMat)
{
	m_xmf4x4World = worldMat;
}

XMFLOAT4X4 CTransform::GetParentsWorldMat()
{
	return m_xmf4x4ParentsWorld;
}

void CTransform::SetParentsWorldMat(XMFLOAT4X4 worldMat)
{
	m_xmf4x4ParentsWorld = worldMat;
}

void CTransform::MoveStrafe(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Right = GetRight();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Right, fDistance);
	CTransform::SetPosition(xmf3Position);
}

void CTransform::MoveUp(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Up = GetUp();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Up, fDistance);
	CTransform::SetPosition(xmf3Position);
}

void CTransform::MoveForward(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Look = GetLook();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Look, fDistance);
	CTransform::SetPosition(xmf3Position);
}

void CTransform::Rotate(float fPitch, float fYaw, float fRoll)
{
	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(fPitch), XMConvertToRadians(fYaw), XMConvertToRadians(fRoll));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

void CTransform::Rotate(XMFLOAT3 *pxmf3Axis, float fAngle)
{
	XMMATRIX mtxRotate = XMMatrixRotationAxis(XMLoadFloat3(pxmf3Axis), XMConvertToRadians(fAngle));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

void CTransform::Rotate(XMFLOAT4 *pxmf4Quaternion)
{
	XMMATRIX mtxRotate = XMMatrixRotationQuaternion(XMLoadFloat4(pxmf4Quaternion));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

HRESULT CTransform::Release()
{

	delete this;

	return NOERROR;
}
