#include "PlatformManager.h"


CPlatformManager::CPlatformManager()
{

}

HRESULT CPlatformManager::Initialize_PlatformManager()
{
    return NOERROR;
}



CPlatformManager* CPlatformManager::Create()
{
    CPlatformManager* pInstance = new CPlatformManager();

    if (pInstance->Initialize_PlatformManager())
    {
        MSG_BOX("CPlatformManager Created Failed");
        Safe_Release(pInstance);
    }

    return pInstance;
}

HRESULT CPlatformManager::Release()
{
    m_vecPlatform.clear();

    delete this;

    return NOERROR;
}