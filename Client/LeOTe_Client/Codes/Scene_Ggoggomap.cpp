#include "Scene_Ggoggomap.h"
#include "Scene_Boardmap.h"
#include "Scene_WaitingRoom.h"
#include "Management.h"
#include "TeddyBear.h"
#include "Physic.h"
#include "Player.h"
#include "BoundingBox.h"
#include "Font.h"
#include "BillBoardObject.h"
#include "Camera_Fixed.h"
#include "Camera_Third.h"
#include "UI.h"
#include "CollisionManager.h"
#include "StaticObject.h"

extern bool G_SC;

extern CPlayer* Player;
extern CPlayer* Players[3];

extern CTeddyBear* pTeddyBear[2];

CScene_Ggoggomap::CScene_Ggoggomap()
{
}

CScene_Ggoggomap::~CScene_Ggoggomap()
{
}
//
//void IntersectObject(CPlayer* pHit, CPlayer* pTeddy, float TimeDelta)
//{
//	BoundingBox xmBoundingBox[2];
//	xmBoundingBox[0] = pHit->Get_TeddyBear()->m_xmBoundingBox;
//	xmBoundingBox[1] = pTeddy->Get_TeddyBear()->m_xmBoundingBox;
//	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&pHit->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
//	xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&pTeddy->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
//
//	if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
//	{
//		XMFLOAT3 xmLookVector = Vector3::Normalize(Vector3::Subtract(pTeddy->GetPosition(), pHit->GetPosition()));
//		pTeddy->xmf3Shift = Vector3::Add(XMFLOAT3(0.f, 0.f, 0.f), xmLookVector, PLAYERSPEED * TimeDelta);
//	}
//}
//
//void IntersectObjectTest(CPlayer* pHit, CTeddyBear* pTeddy, float TimeDelta)
//{
//	BoundingBox xmBoundingBox[2];
//	xmBoundingBox[0] = pHit->Get_TeddyBear()->m_xmBoundingBox;
//	xmBoundingBox[1] = pTeddy->m_xmBoundingBox;
//	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&pHit->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
//	xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&pTeddy->m_pTransformCom->GetWorldMat()));
//
//	if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
//	{
//		XMFLOAT3 xmLookVector = Vector3::Normalize(Vector3::Subtract(pTeddy->m_pTransformCom->GetPosition(), pHit->GetPosition()));
//		xmLookVector.y = 0.f;
//		pTeddy->m_pTransformCom->SetPosition(Vector3::Add(pTeddy->m_pTransformCom->GetPosition(), xmLookVector, PLAYERSPEED * TimeDelta));
//	}
//}
//

CGameObject* CScene_Ggoggomap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position.x = x;
	temp.position.y = y;
	temp.position.z = z;

	temp.rotation.x = rx;
	temp.rotation.y = ry;
	temp.rotation.z = rz;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

CGameObject* CScene_Ggoggomap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position = transform;
	temp.rotation = rotation;
	temp.scale = scale;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

HRESULT CScene_Ggoggomap::Initialize_Scene(CScene_Boardmap* pSceneBoardmap)
{
	m_pScene_Boardmap = pSceneBoardmap;

	moveDelta = 0.f;

	m_SceneId = SCENEID::SCENE_GGOGGOMAP;

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);

	CManagement* pManagement = CManagement::GetInstance();

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
	CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));

	pCameraFixed->settingCamera(XMFLOAT3(0.f, 15.f, 15.f), XMFLOAT3(0.f, 0.f, -1.f), XMFLOAT3(0.f, 1.f, 0.f));

	CreateObject(L"Layer_BackGround", L"GameObject_SkyBox", 29.7602, 15.1374, -32.9217, -90.f, 0.f, 0.f);

	/*CTileLarge* pPlane = dynamic_cast<CTileLarge*>(CreateObject(L"Layer_BackGround", L"GameObject_TileLarge", XMFLOAT3(0.0, -0.1, 0.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(7.0, 0.1, 7.0)));
	pPlane->SetMaterialDiffuseRGBA(255,204,105,256);*/
	//CreateObject(L"Layer_BackGround", L"GameObject_TileLarge", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(7.0, 7.0, 7.0));

	if (!SERVERCONNECT) {
		for (int i = 0; i < 20; ++i)
		{
			if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_Chicken", nullptr, ::Player)))
				return E_FAIL;
		}
	}
	//if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_GrassPlane", nullptr)))
	//	return E_FAIL;
	
	CreateObject(L"Layer_BackGround", L"GameObject_GroundFlat", XMFLOAT3(0.0, -0.5, -32.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_GroundFlat", XMFLOAT3(-37.1, -0.5, 0.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_GroundFlat", XMFLOAT3(38.4, -0.5, -32.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_GroundFlat", XMFLOAT3(0.0, -0.5, 30.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_GroundFlat", XMFLOAT3(-40.4, -0.5, -32.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_GroundFlat", XMFLOAT3(0.0, -0.5, 0.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(21.8, -0.2, -23.0), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(-13.9, 0.0, -4.9), XMFLOAT3(270.0, 90.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(21.8, -0.2, -41.5), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(-9.6, 0.0, 8.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(26.1, -0.2, -18.7), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(14.4, 0.0, 4.0), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(-9.6, 0.0, -9.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(10.1, 0.0, 8.4), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(14.4, 0.0, -4.8), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(34.9, -0.2, -18.7), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(43.8, -0.2, -18.7), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(-13.9, 0.0, 3.9), XMFLOAT3(270.0, 90.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(21.8, -0.2, -32.2), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence", XMFLOAT3(10.1, 0.0, -9.3), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence2", XMFLOAT3(-5.1, 0.0, 12.7), XMFLOAT3(270.0, 90.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));
	CreateObject(L"Layer_BackGround", L"GameObject_Fence2", XMFLOAT3(5.6, 0.0, 12.8), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));

	CreateObject(L"Layer_BackGround", L"GameObject_DirtSkirt", XMFLOAT3(46.0, -0.1, 16.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.05, 0.01, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtSkirt", XMFLOAT3(46.0, -0.1, -4.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.05, 0.01, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtSkirt", XMFLOAT3(46.0, -0.1, 6.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.05, 0.01, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtSkirt", XMFLOAT3(46.0, -0.1, -14.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.05, 0.01, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtRow", XMFLOAT3(21.0, -0.2, 6.5), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtRow", XMFLOAT3(30.0, -0.2, 6.5), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtRow", XMFLOAT3(21.0, -0.2, -14.3), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtRow", XMFLOAT3(30.0, -0.2, -3.9), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtRow", XMFLOAT3(21.0, -0.2, -3.9), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_DirtRow", XMFLOAT3(30.0, -0.2, -14.3), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_Barn", XMFLOAT3(1.8, 0.2, -14.6), XMFLOAT3(270.0, 270.0, 0.0), XMFLOAT3(0.01, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_VegaRow02", XMFLOAT3(30.6, -0.2, -14.4), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_VegaRow02", XMFLOAT3(21.2, -0.2, -14.4), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_VegaRow03", XMFLOAT3(21.2, -0.3, -4.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_VegaRow03", XMFLOAT3(30.7, -0.3, -4.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-16.8, 0.3, 10.5), XMFLOAT3(0.0, 56.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-30.0, 0.1, -5.5), XMFLOAT3(0.0, 56.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-16.9, 0.3, 4.6), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-22.3, 0.3, 15.6), XMFLOAT3(0.0, 56.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-8.4, -0.3, -24.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-17.4, -0.5, -16.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-22.6, -0.5, -26.1), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-35.7, 0.1, -14.1), XMFLOAT3(0.0, 56.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-1.8, -0.3, -26.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(10.3, -0.3, -24.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-27.9, 0.1, 1.7), XMFLOAT3(0.0, 56.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_TreeApple", XMFLOAT3(-25.9, 0.3, 9.4), XMFLOAT3(0.0, 56.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_Silo", XMFLOAT3(-20.6, -0.7, -11.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.01, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Silo", XMFLOAT3(-20.6, -0.7, -6.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.01, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(33.5, -0.8, 15.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(22.6, -0.8, 13.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(25.0, -0.8, 15.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(33.5, -0.8, 7.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(25.0, -0.8, 11.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(22.6, -0.8, 15.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(25.0, -0.8, 7.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(22.6, -0.8, 7.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(30.8, -0.8, 15.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(30.8, -0.8, 7.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(30.8, -0.8, 13.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(27.9, -0.8, 15.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(25.0, -0.8, 13.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(27.9, -0.8, 7.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(27.9, -0.8, 11.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(33.5, -0.8, 11.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(33.5, -0.8, 13.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(30.8, -0.8, 11.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(27.9, -0.8, 13.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_PlantCorn", XMFLOAT3(22.6, -0.8, 11.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_BaleSquare", XMFLOAT3(27.0, 0.2, -21.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_BaleSquare", XMFLOAT3(24.0, 0.2, -21.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_BaleSquare", XMFLOAT3(24.0, 1.3, -23.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_BaleSquare", XMFLOAT3(27.0, 0.2, -23.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_Windmill", XMFLOAT3(12.2, -1.3, -13.2), XMFLOAT3(0.0, 23.5, 0.0), XMFLOAT3(0.015, 0.01, 0.015));
	//CreateObject(L"Layer_BackGround", L"GameObject_Ground", XMFLOAT3(0.5, 0.1, 2.2), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.016, 0.01, 0.045));
	//CreateObject(L"Layer_BackGround", L"GameObject_Ground", XMFLOAT3(0.5, 0.1, 21.8), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.016, 0.01, 0.045));
	CreateObject(L"Layer_BackGround", L"GameObject_PickUpCar", XMFLOAT3(-11.8, -1.1, 14.5), XMFLOAT3(0.0, 46.2, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_SunFlower", XMFLOAT3(16.8, -0.9, 5.2), XMFLOAT3(0.0, 330.7, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_SunFlower", XMFLOAT3(15.3, -0.7, 6.1), XMFLOAT3(0.0, 36.1, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_SunFlower", XMFLOAT3(16.0, -0.9, 6.1), XMFLOAT3(0.0, 356.5, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_SunFlower", XMFLOAT3(16.1, -0.4, 5.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_SunFlower", XMFLOAT3(17.1, -1.0, 5.8), XMFLOAT3(0.0, 346.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_Flower", XMFLOAT3(16.0, 0.3, -5.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_Flower", XMFLOAT3(16.0, 0.3, 2.8), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.02, 0.02, 0.02));	
	CreateObject(L"Layer_BackGround", L"GameObject_WaterTower", XMFLOAT3(-10.1, -1.3, -14.6), XMFLOAT3(0.0, 320.1, 0.0), XMFLOAT3(0.015, 0.015, 0.015));
	CreateObject(L"Layer_BackGround", L"GameObject_Well", XMFLOAT3(6.9, 0.0, 10.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(1.5, 1.5, 1.5));

	m_Windmill = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_WindmillBlade", XMFLOAT3(12.9, 8.3, -11.9), XMFLOAT3(0.0, 23.5, 0.0), XMFLOAT3(0.015, 0.01, 0.015)));

	CreateObject(L"Layer_BackGround", L"GameObject_StopWatch", XMFLOAT3(0.55f, 0.91f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.06f, 0.1f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_GGoGGoFont", (CGameObject**)&m_pTimeText)))
		return E_FAIL;
	m_pTimeText->SetText(L"40", XMFLOAT2(0.82f, 0.012f), XMFLOAT2(1.f, 1.f));
	m_pTimeText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_GGoGGoFont", (CGameObject**)&m_pEndText)))
		return E_FAIL;
	m_pEndText->SetText(L"", XMFLOAT2(0.45f, 0.45f), XMFLOAT2(2.f, 2.f));
	m_pEndText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_GGoGGoFont", (CGameObject**)&m_pStatTimeText)))
		return E_FAIL;
	m_pStatTimeText->SetText(L"", XMFLOAT2(0.5f, 0.38f), XMFLOAT2(2.f, 2.f));
	m_pStatTimeText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	CreateObject(L"Layer_BackGround", L"GameObject_GGoGGoIcon", XMFLOAT3(0.8f, 0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.07f, 0.07f, 0.1f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_GGoGGoFont", (CGameObject**)&m_pChickenCountText)))
		return E_FAIL;
	m_pChickenCountText->SetText(L"20", XMFLOAT2(0.95f, 0.012f), XMFLOAT2(1.f, 1.f));
	m_pChickenCountText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	pBoundingBox[0] = dynamic_cast<CBoundingBox*>(CreateObject(L"Layer_BackGround", L"GameObject_BoundingBox", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.8f, 1.2f, 0.7f)));
	pBoundingBox[1] = dynamic_cast<CBoundingBox*>(CreateObject(L"Layer_BackGround", L"GameObject_BoundingBox", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.5f, 0.3f, 0.5f)));

	for (int i = 0; i < GGOUI_MAX; ++i)
	{
		pGGoGGoUI[i] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_GGoGGoPlus", XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.07f, 0.1f, 0.1f));
		pGGoGGoUI[i]->SetHide(true);
	}
	
	/*if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pHerePlayer)))
		return E_FAIL;
	pHerePlayer->SetSize(XMFLOAT2(1.0f, 1.0f));
	pHerePlayer->SetTexture(L"Component_Texture_HerePlayer", SCENEID::SCENE_STATIC);
	pHerePlayer->SetClipSize(XMFLOAT2(1.f, 1.f));
	pHerePlayer->SetStartPos(XMFLOAT2(0.f, 0.f));
	pHerePlayer->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));*/

	CreateObject(L"Layer_BackGround", L"GameObject_Run", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
	CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_GGoGGoFont", (CGameObject**)&pButtonText)))
		return E_FAIL;
	pButtonText->SetText(L"Arrow", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
	pButtonText->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	//CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

	m_pArrowHead = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_ArrowHead", XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(-90.f, 0.f, 0.f), XMFLOAT3(0.27f, 0.3f, 0.2f)));

	::Player->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.f, 0.f);

	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	if (!SERVERCONNECT)
		gameStarted = true;

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Ggoggomap", 1.f, 0, true); //배경음악 재생
	ShowCursor(false);

	return NOERROR;
}

short CScene_Ggoggomap::KeyEvent(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
		return 0;

	if (!gameStarted)
	{
		return 0;
	}

	if (gameEnd)
		return 0;

	CKeyManager* pKeyMgr = CKeyManager::GetInstance();

	static bool isClip = false;

	if (GetAsyncKeyState(VK_F2) & 0x8000)
	{
		if (isClip)
			isClip = false;
		else
			isClip = true;
	}

	if (isClip)
	{
		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos, centerPt;
		centerPt.x = WINDOWX / 2;
		centerPt.y = WINDOWY / 2;

		GetCursorPos(&ptCursorPos);
		cxDelta = (float)(ptCursorPos.x - centerPt.x) / 3.0f;
		cyDelta = (float)(ptCursorPos.y - centerPt.y) / 3.0f;
		SetCursorPos(centerPt.x, centerPt.y);
		if (cxDelta || cyDelta)
		{
			Player->Rotate(cyDelta, cxDelta, 0.0f);
			/*
			if (pKeyMgr->KeyPressing(KEY_RBUTTON))
				Rotate(cyDelta, cxDelta, 0.0f);
			else
				Rotate(cyDelta, 0.0f, -cxDelta);
			*/
		}
	}

	XMFLOAT3 axisX = XMFLOAT3(1.f, 0.f, 0.f);
	XMFLOAT3 axisZ = XMFLOAT3(0.f, 0.f, 1.f);

	XMFLOAT3 axisXZ = XMFLOAT3(1.f, 0.f, 1.f);
	XMFLOAT3 axisXmZ = XMFLOAT3(-1.f, 0.f, 1.f);
	XMFLOAT3 axisXZm = XMFLOAT3(1.f, 0.f, -1.f);
	XMFLOAT3 axisXmZm = XMFLOAT3(-1.f, 0.f, -1.f);

	XMFLOAT3 axisXm = XMFLOAT3(-1.f, 0.f, 0.f);
	XMFLOAT3 axisZm = XMFLOAT3(0.f, 0.f, -1.f);

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
	Player->SetPlayerForce(0.f, 0.f, 0.f);
	float forceAmount = 300.f;
	if (pKeyMgr->KeyPressing(KEY_W))
	{
		//Player->GivePlayerForce(0.f, 0.f, forceAmount);

		if (pKeyMgr->KeyPressing(KEY_A))
			movePlayer(axisXZm, axisXmZm, xmf3Shift, TimeDelta);
		else if (pKeyMgr->KeyPressing(KEY_D))
			movePlayer(axisXmZm, axisXmZ, xmf3Shift, TimeDelta);
		else
			movePlayer(axisZm, axisXm, xmf3Shift, TimeDelta);	
	}
	else if (pKeyMgr->KeyPressing(KEY_S))
	{
		//Player->GivePlayerForce(0.f, 0.f, -forceAmount);

		if(pKeyMgr->KeyPressing(KEY_A))
			movePlayer(axisXZ, axisXZm, xmf3Shift, TimeDelta);
		else if(pKeyMgr->KeyPressing(KEY_D))
			movePlayer(axisXmZ, axisXZ, xmf3Shift, TimeDelta);
		else
			movePlayer(axisZ, axisX, xmf3Shift, TimeDelta);
	}
	else if (pKeyMgr->KeyPressing(KEY_A))
	{
		//Player->GivePlayerForce(-forceAmount, 0.f, 0.f);
		if (pKeyMgr->KeyPressing(KEY_S))
			movePlayer(axisXZ, axisXZm, xmf3Shift, TimeDelta);
		else if(pKeyMgr->KeyPressing(KEY_W))
			movePlayer(axisXZm, axisXmZm, xmf3Shift, TimeDelta);
		else
			movePlayer(axisX, axisZm, xmf3Shift, TimeDelta);
		
	}
	else if (pKeyMgr->KeyPressing(KEY_D))
	{
		//Player->GivePlayerForce(forceAmount, 0.f, 0.f);
		if (pKeyMgr->KeyPressing(KEY_W))
			movePlayer(axisXmZm, axisXmZ, xmf3Shift, TimeDelta);
		else if(pKeyMgr->KeyPressing(KEY_S))
			movePlayer(axisXmZ, axisXZ, xmf3Shift, TimeDelta);
		else
			movePlayer(axisXm, axisZ, xmf3Shift, TimeDelta);

	}
	else
		::Player->m_AnimState = ANIM_IDLE;


	Player->xmf3Shift = xmf3Shift;

	if (pKeyMgr->KeyPressing(KEY_F1))
	{
		if (SERVERCONNECT)
		{
			if (!Player->GetDeath())
			{
				Player->SetDeath(true);

				packet_scenechange p;
				p.id = Player->Get_ClientIndex();
				p.size = sizeof(packet_scenechange);
				p.type = C2S_SCENECHANGE;
				p.x = Player->Get_CurrentPlatformPos().x;
				p.y = Player->Get_CurrentPlatformPos().y;
				p.z = Player->Get_CurrentPlatformPos().z;
				p.lookx = Player->Get_BeforeLookVector().x;
				p.looky = Player->Get_BeforeLookVector().y;
				p.lookz = Player->Get_BeforeLookVector().z;
				p.map = S_BOARDMAP;
				CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);
				CCollisionManager::GetInstance()->clearColliderArray();
				
			}
		}
		else
		{
			Scene_Change(SCENE_BOARDMAP);
			CCollisionManager::GetInstance()->clearColliderArray();
		}
	}


	return NOERROR;
}

short CScene_Ggoggomap::Update_Scene(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
	{
		if (m_pLoading == nullptr)
		{
			m_pLoading = CLoading::Create(SCENE_WAITINGROOM);
		}
		else
		{
			if (m_pLoading->Get_Finish())
			{
				CManagement* pManagement = CManagement::GetInstance();

				pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

				if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_GGOGGOMAP)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
					return -1;
			}
		}
		return 0;
	}

	XMFLOAT3 xmPlayerPostion = Player->GetPosition();
	xmPlayerPostion.y += 2.8f;

	m_pArrowHead->m_pTransformCom->Rotate(0.f, 0.f, TimeDelta * 50.f);
	m_pArrowHead->m_pTransformCom->SetPosition(xmPlayerPostion);
	//pHerePlayer->SetPostion(xmPlayerPostion);

	if (CServerManager::GetInstance()->Get_CreatedChickens() == 20) {
		if (!loadcomplete) {

			m_iStartTime = CServerManager::GetInstance()->Get_StartTime();

			if (m_iStartTime > 0)
				m_pStatTimeText->SetTextString(to_wstring(m_iStartTime));
			else {
				packet_loadingsuccess p2;
				p2.size = sizeof(packet_loadingsuccess);
				p2.type = C2S_MINIGAMELOADING;
				p2.scene = S_GGOGGOMAP;
				CServerManager::GetInstance()->SendData(C2S_MINIGAMELOADING, &p2);
				CServerManager::GetInstance()->Set_StartTime(3);

				loadcomplete = true;

				m_pStatTimeText->SetTextString(L"");
			}
		}
	}
	
	//종료 3초
	if (m_bEndAsk)
	{
		int m_iEndTime = CServerManager::GetInstance()->Get_StartTime();

		if (m_iEndTime == 0) {

			packet_scenechange p;
			p.id = Player->Get_ClientIndex();
			p.size = sizeof(packet_scenechange);
			p.type = C2S_SCENECHANGE;
			p.x = Player->Get_CurrentPlatformPos().x;
			p.y = Player->Get_CurrentPlatformPos().y;
			p.z = Player->Get_CurrentPlatformPos().z;
			p.lookx = Player->Get_BeforeLookVector().x;
			p.looky = Player->Get_BeforeLookVector().y;
			p.lookz = Player->Get_BeforeLookVector().z;
			p.map = S_BOARDMAP;
			CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

			CServerManager::GetInstance()->Set_StartTime(3);
		}
	}

	m_Windmill->m_pTransformCom->Rotate(0.f, 0.f, 1.f);
	if (gameStarted)
	{
		wstring temp;
		temp = to_wstring(CServerManager::GetInstance()->GetChickenNum());
		m_pChickenCountText->SetTextString(temp);

		wstring time;
		short count = 40 - CServerManager::GetInstance()->Get_PlayTime();
		time = to_wstring(count);

		m_pTimeText->SetTextString(time);

		//int time = 10;
		//temp = to_wstring(time);
		//m_pTimeText->SetTextString(temp);
	}

	if (CServerManager::GetInstance()->Get_MinigameReady())
	{
		gameStarted = true;
		CServerManager::GetInstance()->Set_MinigameReady(false);
	}

	if (CServerManager::GetInstance()->Get_BoardmapReady() == 3)
	{
		CServerManager::GetInstance()->Set_Boardmapinit();
		Scene_Change(SCENE_BOARDMAP);
		return 0;
	}

	if (gameEnd)
		return 0;

	UITime += TimeDelta;
	if (UITime > 1.2)
	{
		if (UIlist.empty() == false)
		{
			int index = UIlist.back();
			UIlist.pop();
			float randx = -0.3f;
			int x = rand() % 7;
			randx += (float)(x) / float(10);
			pGGoGGoUI[index]->SetTimerUI(1.f, XMFLOAT3(randx, -0.5f, 0.f));
		}
		UITime = 0.f;
	}

	if (SERVERCONNECT) {
		if (gameStarted)
		{
			if (CServerManager::GetInstance()->GetEscapeUIOn())
			{
				//ui 생성 코드 
				if (UIlist.empty())
					UITime = 2.f;
				UIlist.push(UIindex);
				UIindex++;
				if (UIindex >= 10)
					UIindex = 0;
				CServerManager::GetInstance()->SetEscapeUIOn(false);
			}

			moveDelta += TimeDelta;

			if (moveDelta > MOVEPACKETDELTA&& isMoving)
			{
				packet_move p;
				ZeroMemory(&p, sizeof(p));
				p.type = C2S_MOVE;
				p.size = sizeof(packet_move);
				p.id = Player->Get_ClientIndex();
				p.TimeDelta = TimeDelta;

				XMFLOAT3 look = Vector3::Normalize(Player->GetLookVector());

				p.mx = Player->m_tPlayerInfo.xmPosition.x;
				p.my = Player->m_tPlayerInfo.xmPosition.y;
				p.mz = Player->m_tPlayerInfo.xmPosition.z;

				p.lookx = look.x;
				p.looky = look.y;
				p.lookz = look.z;

				CServerManager::GetInstance()->SendData(C2S_MOVE, &p);
				moveDelta = 0.f;

				isMoving = false;
			}
		}
	}

	if (CServerManager::GetInstance()->Get_MiniWinner() == -2)
	{
		gameEnd = true;
		Player->xmf3Shift = { 0.f,0.f,0.f };

		for (int i = 0; i < 3; ++i)
		{
			Players[i]->SetWinner(false);
		}

		if (!m_bEndAsk) //종료 3초 요청
		{
			m_iEndTime = 3;
			m_bEndAsk = true;

			packet_starttimeask ps;
			ps.size = sizeof(packet_starttimeask);
			ps.type = C2S_STARTTIMER;
			ps.id = Player->Get_ClientIndex();

			CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
		}

		//미니게임 패배
		CServerManager::GetInstance()->Set_MiniWinner(-1);
		m_pEndText->SetTextString(L"Lose");
		CSoundManager::GetInstance()->Play_Sound(L"Minigame_Lose");

	}
	else if (CServerManager::GetInstance()->Get_MiniWinner() == -3)
	{
		gameEnd = true;
		Player->xmf3Shift = { 0.f,0.f,0.f };

		for (int i = 0; i < 3; ++i)
		{
			Players[i]->SetWinner(true);
		}

		if (!m_bEndAsk) //종료 3초 요청
		{
			m_iEndTime = 3;
			m_bEndAsk = true;

			packet_starttimeask ps;
			ps.size = sizeof(packet_starttimeask);
			ps.type = C2S_STARTTIMER;
			ps.id = Player->Get_ClientIndex();

			CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
		}
		//미니게임 승리
		CServerManager::GetInstance()->Set_MiniWinner(-1);
		m_pEndText->SetTextString(L"Win");
		CSoundManager::GetInstance()->Play_Sound(L"Mingame_Win");
	}
	if (pBoundingBox[0] && pTeddyBear[0])
	{
		XMFLOAT3 xmPosition = pTeddyBear[0]->m_pTransformCom->GetPosition();
		pBoundingBox[0]->m_pTransformCom->SetPosition(xmPosition.x, xmPosition.y + 1.3f, xmPosition.z);
	}

	//Player->xmf3Shift = Player->m_pPhysicCom->UpdatePhysics(Player->m_tPlayerInfo.xmPosition, Player->m_tPlayerInfo.xmRight,
	//	Player->m_tPlayerInfo.xmUp, Player->m_tPlayerInfo.xmLook, Player->GetPlayerForce(), Player->GetState(), TimeDelta);

	return 0;
}

short CScene_Ggoggomap::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_Ggoggomap::Render_Scene()
{
	return NOERROR;
}

HRESULT CScene_Ggoggomap::Scene_Change(SCENEID Scene)
{
	CManagement* pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return -1;

	if (!SERVERCONNECT) {
		::Player->m_tPlayerInfo.xmPosition = ::Player->Get_CurrentPlatformPos();
		::Player->m_tPlayerInfo.xmLook = ::Player->Get_BeforeLookVector();
		::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
		::Player->m_AnimState = ANIM_IDLE;
		::Player->SetAnimation("idle1");
	}
	else
	{
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
			{
				CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
				dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);
			}

			//Players[i]->m_AnimState = ANIM_IDLE;
			Players[i]->SetAnimation("run");
			Players[i]->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

		}
	}

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Boardmap", 1.f, 0, true); //배경음악 재생
	ShowCursor(true);

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");

	pManagement->Set_CurrentSceneId(Scene);

	if (FAILED(pManagement->Initialize_Current_Scene(m_pScene_Boardmap)))
		return -1;

	if (FAILED(pManagement->Clear_Scene(SCENE_GGOGGOMAP)))
		return -1;
}

CScene_Ggoggomap* CScene_Ggoggomap::Create(CScene_Boardmap* pSceneBoardmap)
{
	CScene_Ggoggomap* pInstance = new CScene_Ggoggomap();

	if (pInstance->Initialize_Scene(pSceneBoardmap))
	{
		MSG_BOX("CScene_Ggoggomap Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_Ggoggomap::Release()
{
	delete this;

	return NOERROR;
}

void CScene_Ggoggomap::movePlayer(XMFLOAT3 forwardDir, XMFLOAT3 dotProduct, XMFLOAT3& XMShift, double TimeDelta)
{
	XMFLOAT3 xmf3dot;
	XMStoreFloat3(&xmf3dot, XMVector3AngleBetweenVectors(XMLoadFloat3(&forwardDir), XMLoadFloat3(&Player->m_tPlayerInfo.xmLook)));
	float tmpangle = XMConvertToDegrees(xmf3dot.y);

	float fDot = Vector3::DotProduct(Player->m_tPlayerInfo.xmLook, dotProduct);

	if (tmpangle > 7.f)
	{
		if (fDot > 0)
			::Player->Rotate(0.f, -800.f * TimeDelta, 0.f);
		else
			::Player->Rotate(0.f, 800.f * TimeDelta, 0.f);
	}
	else
	{		
		::Player->m_tPlayerInfo.xmLook = Vector3::Normalize(forwardDir);
		
	}
	
	XMFLOAT3 excessVector;
	XMFLOAT3 slidingVector = Vector3::Normalize(slidingPlayer(excessVector));

	//if (slidingVector.x == 0.f && slidingVector.z == 0.f)
		XMShift = Vector3::Add(XMShift, Player->m_tPlayerInfo.xmLook, PLAYERSPEED * TimeDelta);
	//else
		//XMShift = Vector3::Add(XMShift, slidingVector, PLAYERSPEED * TimeDelta);

	isMoving = true;

	::Player->m_AnimState = ANIM_RUN;
}

XMFLOAT3 CScene_Ggoggomap::slidingPlayer(XMFLOAT3& xmExcess)
{
	XMFLOAT3 PlayerPos = ::Player->m_tPlayerInfo.xmPosition;
	XMFLOAT3 slidingVector = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 temp = XMFLOAT3(0.f, 0.f, 0.f);

	if (PlayerPos.x >= 13.5f || PlayerPos.x <= -13.5f)
	{		
		XMFLOAT3 normal = XMFLOAT3(0.f, 0.f, 1.f);
		XMFLOAT3 look = ::Player->m_tPlayerInfo.xmLook;
		look.x *= -1.f;
		look.y *= -1.f;
		look.z *= -1.f;
		slidingVector = Vector3::CrossProduct(look, normal);
		slidingVector = Vector3::CrossProduct(normal, slidingVector);
		slidingVector = Vector3::Add(::Player->m_tPlayerInfo.xmLook, slidingVector);

		slidingVector = Vector3::Normalize(slidingVector);
		//Player->m_tPlayerInfo.xmLook = slidingVector;

		if (PlayerPos.x >= 13.5f)
			temp.x = PlayerPos.x - 13.5f;
		else if(PlayerPos.x <= -13.5f)
			temp.x = PlayerPos.x - (-13.5f);
	}
	if (PlayerPos.z >= 7.5f || PlayerPos.z <= -8.5f)
	{
		XMFLOAT3 normal = XMFLOAT3(1.f, 0.f, 0.f);
		XMFLOAT3 look = ::Player->m_tPlayerInfo.xmLook;
		look.x *= -1.f;
		look.y *= -1.f;
		look.z *= -1.f;
		slidingVector = Vector3::CrossProduct(look, normal);
		slidingVector = Vector3::CrossProduct(normal, slidingVector);
		slidingVector = Vector3::Add(::Player->m_tPlayerInfo.xmLook, slidingVector);

		slidingVector = Vector3::Normalize(slidingVector);
		//Player->m_tPlayerInfo.xmLook = slidingVector;

		if (PlayerPos.z >= 7.5f)
			temp.z = PlayerPos.z - 7.5f;
		else if (PlayerPos.z <= -8.5f)
			temp.z = PlayerPos.z - (-8.5f);
	}

	::Player->m_tPlayerInfo.xmPosition = Vector3::Subtract(::Player->m_tPlayerInfo.xmPosition, temp);

	xmExcess = temp;

	return slidingVector;
}
