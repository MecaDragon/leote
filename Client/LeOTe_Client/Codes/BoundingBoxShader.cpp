#include "BoundingBoxShader.h"

CBoudingBoxShader::CBoudingBoxShader()
	: CShader()
{
}

CBoudingBoxShader::CBoudingBoxShader(const CBoudingBoxShader& rhs)
	: CShader(rhs)
{
}

CBoudingBoxShader::~CBoudingBoxShader(void)
{
}

HRESULT CBoudingBoxShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"BoudingBox Shader PipelineState");
	return NOERROR;
}

HRESULT CBoudingBoxShader::Initialize_Component(void* pArg)
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"BoudingBox Shader PipelineState");
	return NOERROR;
}

CComponent* CBoudingBoxShader::Clone_Component(void* pArg)
{
	CBoudingBoxShader* pInstance = new CBoudingBoxShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CBoudingBoxShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CBoudingBoxShader* CBoudingBoxShader::Create()
{
	CBoudingBoxShader* pInstance = new CBoudingBoxShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CBoudingBoxShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_RASTERIZER_DESC CBoudingBoxShader::Create_RasterizerState()
{
	D3D12_RASTERIZER_DESC d3dRasterizerDesc;
	::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0;
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(d3dRasterizerDesc);
}

D3D12_SHADER_BYTECODE CBoudingBoxShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shaders.hlsl", "VSTextured", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CBoudingBoxShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shaders.hlsl", "PSTextured", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CBoudingBoxShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 2;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

HRESULT CBoudingBoxShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
