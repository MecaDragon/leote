#include "stdafx.h"
#include "ShadowMap.h"
#include "DeviceManager.h"
#include "DescriptorHeapManager.h"
#include "Light.h"
#include "Player.h"
 
extern CPlayer* Player;

CShadowMap::CShadowMap(UINT width, UINT height)
{
	md3dDevice = CDeviceManager::GetInstance()->Get_Device();

	mWidth = width;
	mHeight = height;

	mViewport = { 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
	mScissorRect = { 0, 0, (int)width, (int)height };
	m_pLightCamera = CCamera_Light::Create();
	BuildResource();
}

CShadowMap::CShadowMap(const CShadowMap& rhs)
	: md3dDevice(rhs.md3dDevice)
	, mViewport(rhs.mViewport)
	, mScissorRect(rhs.mScissorRect)
	, mWidth(rhs.mWidth)
	, mHeight(rhs.mHeight)
	, mFormat(rhs.mFormat)
	, mhCpuSrv(rhs.mhCpuSrv)
	, mhGpuSrv(rhs.mhGpuSrv)
	, mhCpuDsv(rhs.mhCpuDsv)
	, mShadowMap(rhs.mShadowMap)
	, mSrvDescriptorHeap(rhs.mSrvDescriptorHeap)
	, mNullSrv(rhs.mNullSrv)
{
}

CShadowMap::~CShadowMap()
{
}

UINT CShadowMap::Width()const
{
    return mWidth;
}

UINT CShadowMap::Height()const
{
    return mHeight;
}

ID3D12Resource*  CShadowMap::Resource()
{
	return mShadowMap;
}

CD3DX12_GPU_DESCRIPTOR_HANDLE CShadowMap::Srv()const
{
	return mhGpuSrv;
}

CD3DX12_CPU_DESCRIPTOR_HANDLE CShadowMap::Dsv()const
{
	return mhCpuDsv;
}

D3D12_VIEWPORT CShadowMap::Viewport()const
{
	return mViewport;
}

D3D12_RECT CShadowMap::ScissorRect()const
{
	return mScissorRect;
}

void CShadowMap::CreateLightCamera(int nWidth, int nHeight)
{
	m_pLightCamera = CCamera_Light::Create();
}

HRESULT CShadowMap::BuildDescriptors()
{
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	CD3DX12_CPU_DESCRIPTOR_HANDLE CpuDescriptorHandle = CDescriptorHeapManager::GetInstance()->get_CpuDescriptorHandle();
	CD3DX12_GPU_DESCRIPTOR_HANDLE GpuDescriptorHandle = CDescriptorHeapManager::GetInstance()->get_GpuDescriptorHandle();
	CD3DX12_CPU_DESCRIPTOR_HANDLE CpuDsvDescriptorHandle = CDescriptorHeapManager::GetInstance()->get_CpuDsvDescriptorHandle();

	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	srvDesc.Texture2D.PlaneSlice = 0;
	md3dDevice->CreateShaderResourceView(mShadowMap, &srvDesc, CpuDescriptorHandle);
	//m_d3dSrvShadowMapGPUHandle = GpuDescriptorHandle;
	CpuDescriptorHandle.Offset(1, g_CbvSrvDescriptorIncrementSize);

	m_d3dSrvShadowMapGPUHandle = GpuDescriptorHandle;
	GpuDescriptorHandle.Offset(1, g_CbvSrvDescriptorIncrementSize);

	CDescriptorHeapManager::GetInstance()->set_CpuDescriptorHandle(CpuDescriptorHandle);
	CDescriptorHeapManager::GetInstance()->set_GpuDescriptorHandle(GpuDescriptorHandle);

	// �׸��� ���̰�
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvDesc.Texture2D.MipSlice = 0;

	md3dDevice->CreateDepthStencilView(mShadowMap, &dsvDesc, CpuDsvDescriptorHandle);
	m_d3dDsvShadowMapCPUHandle = CpuDsvDescriptorHandle;

	//CpuDsvDescriptorHandle.Offset(1, g_DsvDescriptorIncrementSize);
	//CDescriptorHeapManager::GetInstance()->set_CpuDsvDescriptorHandle(CpuDsvDescriptorHandle);
	return NOERROR;
}

void CShadowMap::OnResize(UINT newWidth, UINT newHeight)
{
	if((mWidth != newWidth) || (mHeight != newHeight))
	{
		mWidth = newWidth;
		mHeight = newHeight;

		BuildResource();

		// New resource, so we need new descriptors to that resource.
		BuildDescriptors();
	}
}

void CShadowMap::DrawStart_ShadowMap()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	ID3D12DescriptorHeap* pSrvDescriptorHeap = CDescriptorHeapManager::GetInstance()->get_srvDescriptorHeap();
	pCommandList->SetDescriptorHeaps(1, &pSrvDescriptorHeap);

	pCommandList->SetGraphicsRootDescriptorTable(14, m_d3dSrvShadowMapGPUHandle);

	if (m_pLightCamera && Player)
	{
		XMFLOAT3 LightPos = Player->GetPosition();

		LightPos.y = 1000.0f;
		LightPos.z -= 1000.0f;

		m_pLightCamera->LookAt({ LightPos },
			{ Player->GetPosition().x,Player->GetPosition().y,Player->GetPosition().z },
			Player->GetUpVector());

		m_pLightCamera->UpdateViewMatrix();

		pCommandList->RSSetViewports(1, &mViewport);
		pCommandList->RSSetScissorRects(1, &mScissorRect);

		m_pLightCamera->GenerateViewMatrix();

		m_pLightCamera->Update_GameObject(0.1f);
	}
	
	pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mShadowMap,
		D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_DEPTH_WRITE));

	pCommandList->ClearDepthStencilView(m_d3dDsvShadowMapCPUHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	pCommandList->OMSetRenderTargets(0, NULL, FALSE, &m_d3dDsvShadowMapCPUHandle);
}

void CShadowMap::DrawEnd_ShadowMap()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	// Change back to GENERIC_READ so we can read the texture in a shader.
	pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mShadowMap,
		D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_GENERIC_READ));
}

HRESULT CShadowMap::Release()
{

	if (mShadowMap)
	{
		mShadowMap->Unmap(0, nullptr);
		mShadowMap->Release();
	}

	delete this;

	return NOERROR;
}

CShadowMap* CShadowMap::Create(UINT width, UINT height)
{
	CShadowMap* pInstance = new CShadowMap(width, height);

	if (FAILED(pInstance->BuildDescriptors()))
	{
		MSG_BOX("CShadowMap Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
 

void CShadowMap::BuildResource()
{
	D3D12_HEAP_PROPERTIES d3dHeapPropertiesDesc;
	::ZeroMemory(&d3dHeapPropertiesDesc, sizeof(D3D12_HEAP_PROPERTIES));
	d3dHeapPropertiesDesc.Type = D3D12_HEAP_TYPE_DEFAULT;
	d3dHeapPropertiesDesc.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	d3dHeapPropertiesDesc.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	d3dHeapPropertiesDesc.CreationNodeMask = 1;
	d3dHeapPropertiesDesc.VisibleNodeMask = 1;

	D3D12_RESOURCE_DESC d3dResourceDesc;
	ZeroMemory(&d3dResourceDesc, sizeof(D3D12_RESOURCE_DESC));

	d3dResourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	d3dResourceDesc.Alignment = 0;
	d3dResourceDesc.Width = mWidth;
	d3dResourceDesc.Height = mHeight;
	d3dResourceDesc.DepthOrArraySize = 1;
	d3dResourceDesc.MipLevels = 1;
	d3dResourceDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	d3dResourceDesc.SampleDesc.Count = 1;
	d3dResourceDesc.SampleDesc.Quality = 0;
	d3dResourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	d3dResourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE d3dClear;
	d3dClear.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dClear.DepthStencil.Depth = 1.0f;
	d3dClear.DepthStencil.Stencil = 0;

	md3dDevice->CreateCommittedResource(&d3dHeapPropertiesDesc,
		D3D12_HEAP_FLAG_NONE,
		&d3dResourceDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		&d3dClear,
		IID_PPV_ARGS(&mShadowMap)
	);
}

void CShadowMap::SetDirectionPos(XMFLOAT3 pos)
{
	m_pLightCamera->SetDicrtionPos(pos);
}

void CShadowMap::SetTargetPos(XMFLOAT3 pos)
{
	m_pLightCamera->SetTargetPos(pos);
}
