#include "GrassPlaneTexShader.h"

CGrassPlaneTexShader::CGrassPlaneTexShader()
	: CShader()
{
}

CGrassPlaneTexShader::CGrassPlaneTexShader(const CGrassPlaneTexShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

CGrassPlaneTexShader::~CGrassPlaneTexShader(void)
{
}

HRESULT CGrassPlaneTexShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"CGrassPlaneTexShader PipelineState");
	return NOERROR;
}

HRESULT CGrassPlaneTexShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CGrassPlaneTexShader::Clone_Component(void* pArg)
{
	CGrassPlaneTexShader* pInstance = new CGrassPlaneTexShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CGrassPlaneTexShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGrassPlaneTexShader* CGrassPlaneTexShader::Create()
{
	CGrassPlaneTexShader* pInstance = new CGrassPlaneTexShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CGrassPlaneTexShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CGrassPlaneTexShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/GrassPlaneTexture.hlsl", "VSGrassPlane", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CGrassPlaneTexShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/GrassPlaneTexture.hlsl", "PSGrassPlane", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CGrassPlaneTexShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 2;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

HRESULT CGrassPlaneTexShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
