#include "Management.h"
#include "dxgidebug.h"
#include "Scene_Boardmap.h"

IMPLEMENT_SINGLETON(CManagement)

extern bool G_SC;

CManagement::~CManagement()
{
}

HRESULT CManagement::Initialize_Management(_uint iSceneIndex)
{
	m_pDeviceManager = CDeviceManager::GetInstance();
	m_pObjectManager = CObjectManager::GetInstance();
	m_pComponentManager = CComponentManager::GetInstance();
	m_pPipeLine = CPipeLine::GetInstance();
	m_pServerManager = CServerManager::GetInstance();
	m_pCameraManager = CCameraManager::GetInstance();
	m_pDescriptorHeapManager = CDescriptorHeapManager::GetInstance();
	m_pCollisionManager = CCollisionManager::GetInstance();
	m_pSoundManager = CSoundManager::GetInstance();

	m_pObjectManager->Initialize_ObjectManager(iSceneIndex);
	m_pComponentManager->Initialize_ComponentManager(iSceneIndex);
	m_pSoundManager->Initialize_SoundManager();

	Initialize_Device();

	Create_RootSignature();

	CreateFbxSdkManager();
	return NOERROR;
}

HRESULT CManagement::Initialize_Device()
{
	if (FAILED(m_pDeviceManager->Initialize_Device())) {
		MSG_BOX("Initialize_Device Failed");
		return E_FAIL;
	}

	return NOERROR;
}

HRESULT CManagement::Create_RootSignature()
{
	D3D12_DESCRIPTOR_RANGE pd3dDescriptorRanges[10];

	pd3dDescriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[0].NumDescriptors = 1;
	pd3dDescriptorRanges[0].BaseShaderRegister = 6; //t6: gtxtAlbedoTexture
	pd3dDescriptorRanges[0].RegisterSpace = 0;
	pd3dDescriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[1].NumDescriptors = 1;
	pd3dDescriptorRanges[1].BaseShaderRegister = 7; //t7: gtxtSpecularTexture
	pd3dDescriptorRanges[1].RegisterSpace = 0;
	pd3dDescriptorRanges[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[2].NumDescriptors = 1;
	pd3dDescriptorRanges[2].BaseShaderRegister = 8; //t8: gtxtNormalTexture
	pd3dDescriptorRanges[2].RegisterSpace = 0;
	pd3dDescriptorRanges[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[3].NumDescriptors = 1;
	pd3dDescriptorRanges[3].BaseShaderRegister = 9; //t9: gtxtMetallicTexture
	pd3dDescriptorRanges[3].RegisterSpace = 0;
	pd3dDescriptorRanges[3].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[4].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[4].NumDescriptors = 1;
	pd3dDescriptorRanges[4].BaseShaderRegister = 10; //t10: gtxtEmissionTexture
	pd3dDescriptorRanges[4].RegisterSpace = 0;
	pd3dDescriptorRanges[4].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[5].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[5].NumDescriptors = 1;
	pd3dDescriptorRanges[5].BaseShaderRegister = 11; //t11: gtxtDetailAlbedoTexture
	pd3dDescriptorRanges[5].RegisterSpace = 0;
	pd3dDescriptorRanges[5].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[6].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[6].NumDescriptors = 1;
	pd3dDescriptorRanges[6].BaseShaderRegister = 12; //t12: gtxtDetailNormalTexture
	pd3dDescriptorRanges[6].RegisterSpace = 0;
	pd3dDescriptorRanges[6].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[7].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[7].NumDescriptors = 1;
	pd3dDescriptorRanges[7].BaseShaderRegister = 13; //t13: gtxtSkyBoxTexture
	pd3dDescriptorRanges[7].RegisterSpace = 0;
	pd3dDescriptorRanges[7].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[8].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[8].NumDescriptors = 1;
	pd3dDescriptorRanges[8].BaseShaderRegister = 15; //t15: gtxtFontTexture
	pd3dDescriptorRanges[8].RegisterSpace = 0;
	pd3dDescriptorRanges[8].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[9].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[9].NumDescriptors = 1;
	pd3dDescriptorRanges[9].BaseShaderRegister = 16; //t16: gtxtShadowMap
	pd3dDescriptorRanges[9].RegisterSpace = 0;
	pd3dDescriptorRanges[9].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	D3D12_ROOT_PARAMETER pd3dRootParameters[15];

	pd3dRootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[0].Descriptor.ShaderRegister = 0; //Camera
	pd3dRootParameters[0].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	pd3dRootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[1].Descriptor.ShaderRegister = 1; //GameObject
	pd3dRootParameters[1].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	pd3dRootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[2].Descriptor.ShaderRegister = 2; //Shadow
	pd3dRootParameters[2].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	pd3dRootParameters[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[3].Descriptor.ShaderRegister = 3; //Lights
	pd3dRootParameters[3].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	pd3dRootParameters[4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[4].Descriptor.ShaderRegister = 4; //Empty
	pd3dRootParameters[4].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	pd3dRootParameters[5].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[5].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[5].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[0]);
	pd3dRootParameters[5].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[6].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[6].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[6].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[1]);
	pd3dRootParameters[6].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[7].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[7].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[7].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[2]);
	pd3dRootParameters[7].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[8].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[8].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[8].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[3]);
	pd3dRootParameters[8].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[9].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[9].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[9].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[4]);
	pd3dRootParameters[9].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[10].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[10].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[10].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[5]);
	pd3dRootParameters[10].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[11].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[11].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[11].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[6]);
	pd3dRootParameters[11].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[12].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[12].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[12].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[7]);
	pd3dRootParameters[12].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[13].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[13].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[13].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[8]);
	pd3dRootParameters[13].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[14].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[14].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[14].DescriptorTable.pDescriptorRanges = &(pd3dDescriptorRanges[9]);
	pd3dRootParameters[14].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	CD3DX12_STATIC_SAMPLER_DESC pd3dSamplerDescs[8];

	//gsamPointWrap
	pd3dSamplerDescs[0] =
	{ 0,								// shaderRegister
	D3D12_FILTER_MIN_MAG_MIP_POINT,		// filter
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_WRAP };	// addressW

	//gsamPointClamp
	pd3dSamplerDescs[1] =
	{ 1,								// shaderRegister
	D3D12_FILTER_MIN_MAG_MIP_POINT,		// filter
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP }; // addressW

	//gsamLinearWrap
	pd3dSamplerDescs[2] =
	{ 2,								// shaderRegister
	D3D12_FILTER_MIN_MAG_MIP_LINEAR,	// filter
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_WRAP };	// addressW

	//gsamLinearClamp
	pd3dSamplerDescs[3] =
	{ 3,								// shaderRegister
	D3D12_FILTER_MIN_MAG_MIP_LINEAR,	// filter
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP }; // addressW

	//gsamAnisotropicWrap
	pd3dSamplerDescs[4] =
	{ 4,								// shaderRegister
	D3D12_FILTER_ANISOTROPIC,			// filter
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_WRAP,	// addressW
	0.0f,								// mipLODBias
	8 };								// maxAnisotropy

	//gsamAnisotropicClamp
	pd3dSamplerDescs[5] =
	{ 5,								// shaderRegister
	D3D12_FILTER_ANISOTROPIC,			// filter
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_CLAMP,	// addressW
	0.0f,								// mipLODBias
	8 };                                // maxAnisotropy

	//gsamFontWrap
	pd3dSamplerDescs[6] =
	{ 6,								// shaderRegister
	D3D12_FILTER_MIN_MAG_MIP_POINT,			// filter
	D3D12_TEXTURE_ADDRESS_MODE_BORDER,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_BORDER,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_BORDER,	// addressW
	0.0f,								// mipLODBias
	0 };                                // maxAnisotropy

	//gsamShadow
	pd3dSamplerDescs[7] =
	{ 7,								// shaderRegister
	D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT,			// filter
	D3D12_TEXTURE_ADDRESS_MODE_BORDER,	// addressU
	D3D12_TEXTURE_ADDRESS_MODE_BORDER,	// addressV
	D3D12_TEXTURE_ADDRESS_MODE_BORDER,	// addressW
	0.0f,								// mipLODBias
	16,
	D3D12_COMPARISON_FUNC_LESS_EQUAL ,
	D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK };                                // maxAnisotropy

	D3D12_ROOT_SIGNATURE_FLAGS d3dRootSignatureFlags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
	D3D12_ROOT_SIGNATURE_DESC d3dRootSignatureDesc;
	::ZeroMemory(&d3dRootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
	d3dRootSignatureDesc.NumParameters = _countof(pd3dRootParameters);
	d3dRootSignatureDesc.pParameters = pd3dRootParameters;
	d3dRootSignatureDesc.NumStaticSamplers = _countof(pd3dSamplerDescs);
	d3dRootSignatureDesc.pStaticSamplers = pd3dSamplerDescs;
	d3dRootSignatureDesc.Flags = d3dRootSignatureFlags;

	ID3DBlob* pd3dSignatureBlob = NULL;
	ID3DBlob* pd3dErrorBlob = NULL;
	D3D12SerializeRootSignature(&d3dRootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &pd3dSignatureBlob, &pd3dErrorBlob);

	HRESULT r = m_pDeviceManager->Get_Device()->CreateRootSignature(0, pd3dSignatureBlob->GetBufferPointer(), pd3dSignatureBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)&m_pRootSignature);
	if (pd3dSignatureBlob) pd3dSignatureBlob->Release();
	if (pd3dErrorBlob) pd3dErrorBlob->Release();


	return NOERROR;
}


void CManagement::CreateFbxSdkManager()
{
	m_pfbxSdkManager = FbxManager::Create();
	FbxIOSettings* pfbxIOSettings = FbxIOSettings::Create(m_pfbxSdkManager, IOSROOT);
	m_pfbxSdkManager->SetIOSettings(pfbxIOSettings);
	FbxString fbxstrPath = ::FbxGetApplicationDirectory();
	m_pfbxSdkManager->LoadPluginsDirectory(fbxstrPath.Buffer());
}

HRESULT CManagement::Initialize_Current_Scene(CScene* pScene)
{
	if (nullptr == pScene)
		return E_FAIL;

	// 새로운 씬으로 초기화하기 전에 지워주기
	if (m_pCurrent_Scene != nullptr && m_pCurrent_Scene->Get_SceneId() != SCENEID::SCENE_WAITINGROOM && pScene->Get_SceneId() == SCENEID::SCENE_BOARDMAP)
	{
		dynamic_cast<CScene_Boardmap*>(pScene)->BackTotheBoardmap(m_pCurrent_Scene->Get_SceneId());
	}
	if (m_pCurrent_Scene != nullptr && m_pCurrent_Scene->Get_SceneId() != SCENEID::SCENE_BOARDMAP)
	{
		Safe_Release(m_pCurrent_Scene);
	}

	m_pCurrent_Scene = pScene;

	return NOERROR;
}

short CManagement::Update_Management(double TimeDelta)
{
	if (m_pCurrent_Scene == nullptr || m_pObjectManager == nullptr)
		return -1;

	if (-1 == m_pCurrent_Scene->KeyEvent(TimeDelta))
		return -1;

	if (-1 == m_pCameraManager->Update_CameraManager(TimeDelta))
		return -1;

	if (-1 == m_pCurrent_Scene->Update_Scene(TimeDelta))
		return -1;

	if (-1 == m_pObjectManager->Update_ObjectManager(TimeDelta))
		return -1;

	if (-1 == m_pSoundManager->Update_SoundManager(m_pCameraManager->GetCurrentCamera()))
		return -1;

	if (GetAsyncKeyState(VK_F3) & 0x8000)
	{
		DestroyWindow(g_hWnd);
	}

	return 0;
}

short CManagement::LateUpdate_Management(double TimeDelta)
{
	if (m_pCurrent_Scene == nullptr || m_pObjectManager == nullptr)
		return -1;

	if (-1 == m_pCameraManager->LateUpdate_CameraManager(TimeDelta))
		return -1;

	if (-1 == m_pCurrent_Scene->LateUpdate_Scene(TimeDelta))
		return -1;

	if (-1 == m_pObjectManager->LateUpdate_ObjectManager(TimeDelta))
		return -1;

	if(!SERVERCONNECT)
		m_pCollisionManager->checkCollision(TimeDelta);

	return 0;
}

HRESULT CManagement::Render_Shadow()
{
	m_pDeviceManager->Get_CommandList(COMMAND_RENDER)->SetGraphicsRootSignature(m_pRootSignature);
	if (m_pShadowMap) {
		m_pShadowMap->DrawStart_ShadowMap();
		m_pObjectManager->Render_ShadowMap();
		m_pShadowMap->DrawEnd_ShadowMap();
	}

	return NOERROR;
}

HRESULT CManagement::Render_Management()
{
	m_pDeviceManager->Get_CommandList(COMMAND_RENDER)->SetGraphicsRootSignature(m_pRootSignature);

	m_pPipeLine->Set_ViewportsAndScissorRects(m_pDeviceManager->Get_CommandList(COMMAND_RENDER));
	m_pPipeLine->UpdateShaderVariables(m_pDeviceManager->Get_CommandList(COMMAND_RENDER));

	m_pLight->UpdateShaderVariables(m_pDeviceManager->Get_CommandList(COMMAND_RENDER));

	//m_pDescriptorHeapManager->reset_GpuDescriptorHandle();

	return NOERROR;
}
 
HRESULT CManagement::Clear_Scene(_uint SceneID)
{
	if (nullptr == m_pObjectManager ||
		nullptr == m_pComponentManager)
		return E_FAIL;

	// 특정 씬을 위해 생성했던 객체, 레이어들을 삭제한다.
	if (FAILED(m_pObjectManager->Clear_ObjectManager(SceneID)))
		return E_FAIL;

	// 특정 씬을 위해 생성했던 객체 원형들을 삭제한다.
	if (FAILED(m_pObjectManager->Clear_ObjectPrototype(SceneID)))
		return E_FAIL;

	// 특정 씬을 위해 생성했던 컴포넌트 원형들을 삭제한다.
	if (FAILED(m_pComponentManager->Clear_ComponentManager(SceneID)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CManagement::Release_Mangers()
{
	if ((CKeyManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CKeyManager DestroyInstance Failed");

	if ((CTimerManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CTimerManager DestroyInstance Failed");

	if ((CObjectManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CObjectManager DestroyInstance Failed");

	if ((CComponentManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CComponentManager DestroyInstance Failed");

	if ((CCameraManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CCameraManager DestroyInstance Failed");

	if ((CPipeLine::GetInstance()->DestroyInstance()))
		MSG_BOX("CPipeLine DestroyInstance Failed");

	if ((CDeviceManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CDeviceManager DestroyInstance Failed");

	if ((CDescriptorHeapManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CDescriptorHeapManager DestroyInstance Failed");

	if ((CCollisionManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CCollisionManager DestroyInstance Failed");

	if ((CSoundManager::GetInstance()->DestroyInstance()))
		MSG_BOX("CSoundManager DestroyInstance Failed");


	return NOERROR;
}

HRESULT CManagement::Release()
{
	Safe_Release(m_pCurrent_Scene);

	for (int i = 0; i < SCENE_END; ++i)
		Clear_Scene(i);

	Release_Mangers();

	m_pRootSignature->Release();

	m_pLight->Release();
	//DX12 메모리 누수 확인
	IDXGIDebug1* pdxgiDebug = nullptr;
	DXGIGetDebugInterface1(0, __uuidof(IDXGIDebug1), (void**)&pdxgiDebug);
	HRESULT hResult = pdxgiDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_DETAIL);
	pdxgiDebug->Release();


	delete this;

	return NOERROR;
}

HRESULT CManagement::Add_Prototype_GameObj(const wchar_t* pPrototypeTag, CGameObject* pPrototype)
{
	if (nullptr == m_pObjectManager)
		return E_FAIL;

	return m_pObjectManager->Add_Prototype_GameObj(pPrototypeTag, pPrototype);
}

HRESULT CManagement::Add_GameObjectToLayer(_uint iSceneIndex, const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, CGameObject** ppOutput, void* pArg)
{
	if (nullptr == m_pObjectManager)
		return E_FAIL;

	return m_pObjectManager->Add_GameObjectToLayer(iSceneIndex, pLayerTag, pPrototypeTag, ppOutput, pArg);
}

HRESULT CManagement::Add_Prototype_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent* pPrototype)
{
	if (nullptr == m_pComponentManager)
		return E_FAIL;

	return m_pComponentManager->Add_Prototype_Component(iSceneIndex, pPrototypeTag, pPrototype);
}

HRESULT CManagement::Clone_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent** ppComponent, void* pArg)
{
	if (nullptr == m_pComponentManager)
		return E_FAIL;

	return m_pComponentManager->Clone_Component(iSceneIndex, pPrototypeTag, ppComponent, pArg);
}

HRESULT CManagement::Create_Light_Material()
{
	m_pLight = CLight::Create();

	return NOERROR;
}

HRESULT CManagement::Create_ShadowMap()
{
	m_pShadowMap = CShadowMap::Create(4096, 4096);

	return NOERROR;
}

