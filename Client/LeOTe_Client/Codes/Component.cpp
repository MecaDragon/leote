#include "Component.h"

CComponent::CComponent()
    : m_bClone(false)
{
}

CComponent::CComponent(const CComponent& rhs)
    : m_bClone(true)
{
}

HRESULT CComponent::Initialize_Component_Prototype()
{
    return NOERROR;
}

HRESULT CComponent::Initialize_Component(void* pArg)
{
    return NOERROR;
}

HRESULT CComponent::Release()
{
    delete this;

    return NOERROR;
}
