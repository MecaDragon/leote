#ifndef __FBXANIMSHADER__
#define __FBXANIMSHADER__

#include "Shader.h"

class CFBXANIMShader final : public CShader
{
private:
	explicit CFBXANIMShader();
	explicit CFBXANIMShader(const CFBXANIMShader& rhs);
	virtual ~CFBXANIMShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CFBXANIMShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();

};

#endif
