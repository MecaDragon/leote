#ifndef __SCENE_LOGO__
#define __SCENE_LOGO__

#include "stdafx.h"
#include "Scene.h"
#include "Loading.h"

class CManagement;
class CScene_Logo : public CScene
{
private:
	explicit CScene_Logo();
	virtual ~CScene_Logo();

public:
	virtual HRESULT Initialize_Scene();
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();

private:
	HRESULT Ready_GameObject_Prototype();
	HRESULT Ready_Layer_BackGround(const wchar_t* pLayerTag);
	HRESULT Ready_Light_Material();
	HRESULT Ready_Component_Static();

private:
	CLoading* m_pLoading = nullptr;

	CManagement* m_pManagement = nullptr;

public:
	static CScene_Logo* Create();
	HRESULT Release();

};

#endif