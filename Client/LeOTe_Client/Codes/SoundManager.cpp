#include "SoundManager.h";
#include <fstream>
#include <iostream>
#include <atlstr.h>
#include <conio.h>
#include "fmod.hpp"
#include "fmod_errors.h"
#include "Camera.h"
#pragma  comment( lib, "fmodex_vc.lib" )

IMPLEMENT_SINGLETON(CSoundManager)

CSoundManager::CSoundManager()
{

}

CSoundManager::~CSoundManager()
{

}

HRESULT CSoundManager::Initialize_SoundManager()
{
	// 시스템 초기화
	FMOD::System_Create(&m_pSystem);
	m_pSystem->init(32, FMOD_INIT_NORMAL, NULL);

	// 분류 채널 설정
	m_pSystem->createChannelGroup("BGM", &m_pChannelGroup[(_uint)CHANNELID::BGM]);
	m_pSystem->createChannelGroup("SE", &m_pChannelGroup[(_uint)CHANNELID::SE]);
	m_pSystem->createChannelGroup("ENV", &m_pChannelGroup[(_uint)CHANNELID::ENV]);
	m_pSystem->createChannelGroup("VOICE", &m_pChannelGroup[(_uint)CHANNELID::VOICE]);

	// 분류 채널을 마스터 채널에 투입
	FMOD::ChannelGroup* ppGroup = nullptr;
	m_pSystem->getMasterChannelGroup(&ppGroup);
	ppGroup->addGroup(m_pChannelGroup[(_uint)CHANNELID::BGM]);
	ppGroup->addGroup(m_pChannelGroup[(_uint)CHANNELID::SE]);
	ppGroup->addGroup(m_pChannelGroup[(_uint)CHANNELID::ENV]);
	ppGroup->addGroup(m_pChannelGroup[(_uint)CHANNELID::VOICE]);

	// 초기 볼륨 설정 및 적용
	m_fVolume[(_uint)CHANNELID::BGM] = 0.5f;
	m_fVolume[(_uint)CHANNELID::SE] = 0.4f;
	m_fVolume[(_uint)CHANNELID::ENV] = 0.4f;
	m_fVolume[(_uint)CHANNELID::VOICE] = 0.5f;
	m_pChannelGroup[(_uint)CHANNELID::BGM]->setVolume(m_fVolume[(_uint)CHANNELID::BGM]);
	m_pChannelGroup[(_uint)CHANNELID::SE]->setVolume(m_fVolume[(_uint)CHANNELID::SE]);
	m_pChannelGroup[(_uint)CHANNELID::ENV]->setVolume(m_fVolume[(_uint)CHANNELID::ENV]);
	m_pChannelGroup[(_uint)CHANNELID::VOICE]->setVolume(m_fVolume[(_uint)CHANNELID::VOICE]);

	// 3D 설정
	m_pSystem->set3DSettings(0.f, 1.f, 0.2f);	// 도플러 사용안함	

	return NOERROR;
}

short CSoundManager::Update_SoundManager(CCamera* pCamera)
{
	if (pCamera)
	{
		CCamera::CAMERA_INFO tCameraInfo = pCamera->get_CameraInfo();
		m_vListenerUp = { tCameraInfo.xmUp.x, tCameraInfo.xmUp.y, tCameraInfo.xmUp.z };
		m_vListenerLook = { tCameraInfo.xmLook.x, tCameraInfo.xmLook.y, tCameraInfo.xmLook.z };
		m_vListenerPos = { tCameraInfo.xmPosition.x, tCameraInfo.xmPosition.y, tCameraInfo.xmPosition.z };
		m_pSystem->set3DListenerAttributes(0, &m_vListenerPos, nullptr, &m_vListenerLook, &m_vListenerUp);	// 도플러 사용 안함
	}

	m_pSystem->update();

	// 시작 시 잡음이 생김. 이거에 대한 해결책. https://qa.fmod.com/t/audio-popping-issue-when-sound-is-first-played/12486
	for (auto iter : m_vecStartChannel)
		iter->setPaused(false);

	m_vecStartChannel.clear();

	Play_2D_List();
	Play_3D_List();

	return 0;
}

HRESULT CSoundManager::Release()
{
	for (auto& iter : m_vecCustomRollOffLevel)
	{
		if (iter != nullptr)
		{
			delete[] iter;
			iter = nullptr;
		}
	}

	m_vecCustomRollOffLevel.clear();

	// 사운드 정보 해제
	for (auto& iter : m_mapSound)
	{
		iter.second.pChannelGroup->release();
		iter.second.pSound->release();
	}

	m_mapSound.clear();

	// 분류 채널 해제
	for (_uint i = 0; i < (_uint)CHANNELID::IDEND; ++i)
		m_pChannelGroup[i]->release();

	// 시스템 해제
	m_pSystem->close();
	m_pSystem->release();

	delete this;

	return NOERROR;
}

void CSoundManager::Play_Sound(wstring wstrSoundKey, float fVolume, int iFadeInSec, bool bLoop)
{
	// 사운드 찾기
	auto& iter = m_mapSound.find(wstrSoundKey.c_str());
	if (m_mapSound.end() == iter)
	{
		m_mapSound[wstrSoundKey].pSound;
		MSG_BOX("StartSound - 찾으려는 사운드가 없습니다.");
		return;
	}

	// 재생
	FMOD::Channel* pNewChannel = nullptr;
	m_pSystem->playSound(iter->second.pSound, iter->second.pChannelGroup, false, &pNewChannel);
	pNewChannel->setPaused(true);
	m_vecStartChannel.push_back(pNewChannel);

	// 페이드인 설정
	if (iFadeInSec > 0)
	{
		unsigned long long dspclock;
		int iRate;

		m_pSystem->getSoftwareFormat(&iRate, nullptr, nullptr);
		pNewChannel->getDSPClock(nullptr, &dspclock);

		pNewChannel->addFadePoint(dspclock, 0.f);
		pNewChannel->addFadePoint(dspclock + (iRate * iFadeInSec), 1.f);
	}

	// 반복 설정
	if (bLoop)
		pNewChannel->setMode(FMOD_LOOP_NORMAL);

	// 볼륨
	if (fVolume != 1.f)
		pNewChannel->setVolume(fVolume);
}

void CSoundManager::Play_Sound_3D(wstring wstrSoundKey, XMFLOAT3 xmPos, float fVolume, FMOD::Channel** pChannel)
{
	// 사운드 찾기
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
	{
		//MSG_BOX("StartSound - 찾으려는 사운드가 없습니다.");
		return;
	}

	// 재생
	FMOD::Channel* pNewChannel = nullptr;
	m_pSystem->playSound(iter->second.pSound, iter->second.pChannelGroup, false, &pNewChannel);
	pNewChannel->setPaused(true);
	m_vecStartChannel.push_back(pNewChannel);
	FMOD_VECTOR vSpeakerPos = { xmPos.x, xmPos.y, xmPos.z };
	pNewChannel->set3DAttributes(&vSpeakerPos, nullptr);

	if (pChannel != nullptr)
		*pChannel = pNewChannel;

	if (fVolume != 1.f)
		pNewChannel->setVolume(fVolume);
}

void CSoundManager::Play_Sound_List(vector<wstring>& vecSoundKey)
{
	if (vecSoundKey.empty())
		return;

	m_vecPlay2DList.push_back(make_pair(vector<wstring>(), 0));
	m_vecPlay2DList.back().first.assign(vecSoundKey.begin(), vecSoundKey.end());

	// 첫번째 재생
	auto iter = m_mapSound.find(*(vecSoundKey.begin()));
	if (iter == m_mapSound.end())
		return;

	FMOD::Channel* pNewChannel = nullptr;
	m_pSystem->playSound(iter->second.pSound, iter->second.pChannelGroup, false, &pNewChannel);
	pNewChannel->setPaused(true);
	m_vecStartChannel.push_back(pNewChannel);
}

void CSoundManager::Play_Sound_3D_List(vector<wstring>& vecSoundKey, XMFLOAT3 xmPos)
{
	if (vecSoundKey.empty())
		return;

	m_vecPlay3DList.push_back(PLAY3DLISTINFO());
	m_vecPlay3DList.back().vecList.assign(vecSoundKey.begin(), vecSoundKey.end());
	m_vecPlay3DList.back().iPlayIndex = 0;
	m_vecPlay3DList.back().vPos = { xmPos.x, xmPos.y, xmPos.z };

	// 첫번째 재생
	auto iter = m_mapSound.find(*(vecSoundKey.begin()));
	if (iter == m_mapSound.end())
		return;

	FMOD::Channel* pNewChannel = nullptr;
	m_pSystem->playSound(iter->second.pSound, iter->second.pChannelGroup, false, &pNewChannel);
	pNewChannel->setPaused(true);
	m_vecStartChannel.push_back(pNewChannel);
	FMOD_VECTOR vSpeakerPos = { xmPos.x, xmPos.y, xmPos.z };
	pNewChannel->set3DAttributes(&vSpeakerPos, nullptr);
}

void CSoundManager::Pause_Channel(CHANNELID eChannelIndex)
{
	m_pChannelGroup[(_uint)eChannelIndex]->setPaused(true);
}

void CSoundManager::Resume_Channel(CHANNELID eChannelIndex)
{
	m_pChannelGroup[(_uint)eChannelIndex]->setPaused(false);
}

void CSoundManager::Stop_Channel(CHANNELID eChannelIndex)
{
	m_pChannelGroup[(_uint)eChannelIndex]->stop();
}

void CSoundManager::Pause_Sound(wstring wstrSoundKey)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	iter->second.pChannelGroup->setPaused(true);
}

void CSoundManager::Resume_Sound(wstring wstrSoundKey)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	iter->second.pChannelGroup->setPaused(false);
}

void CSoundManager::Stop_Sound(wstring wstrSoundKey, int iFadeOutSec)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	// 페이드아웃 설정
	if (iFadeOutSec > 0)
	{
		unsigned long long dspclock;
		int iRate;

		m_pSystem->getSoftwareFormat(&iRate, nullptr, nullptr);
		iter->second.pChannelGroup->getDSPClock(nullptr, &dspclock);

		iter->second.pChannelGroup->addFadePoint(dspclock, 1.f);
		iter->second.pChannelGroup->addFadePoint(dspclock + (iRate * iFadeOutSec), 0.f);
		iter->second.pChannelGroup->setDelay(0, dspclock + (iRate * iFadeOutSec), true);	// 끝나는 시간에 정지
	}
	else
		iter->second.pChannelGroup->stop();
}

void CSoundManager::SetSpeed_Sound(wstring wstrSoundKey, float fSpeed)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	iter->second.pChannelGroup->setPitch(fSpeed);
}

void CSoundManager::SetLoopPoints_Sound(wstring wstrSoundKey, _uint iStartMS, _uint iEndMS)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	iter->second.pSound->setLoopPoints(iStartMS, FMOD_TIMEUNIT_MS, iEndMS, FMOD_TIMEUNIT_MS);
}

void CSoundManager::Stop_All()
{
	FMOD::ChannelGroup* ppGroup = nullptr;
	m_pSystem->getMasterChannelGroup(&ppGroup);
	ppGroup->stop();

	for (auto& iter : m_vecPlay2DList)
		iter.first.clear();
	m_vecPlay2DList.clear();

	for (auto& iter : m_vecPlay3DList)
		iter.vecList.clear();
	m_vecPlay3DList.clear();
}

float CSoundManager::Get_Volume(CHANNELID eChannelIndex)
{
	float fVolume = 0.f;
	m_pChannelGroup[(_uint)eChannelIndex]->getVolume(&fVolume);
	return fVolume;
}

void CSoundManager::Set_Volume(CHANNELID eChannelIndex, float fVolume)
{
	m_pChannelGroup[(_uint)eChannelIndex]->setVolume(fVolume);
}

void CSoundManager::Set_Position(wstring wstrSoundKey, XMFLOAT3 xmPos)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	int iCount = 0;
	iter->second.pChannelGroup->getNumChannels(&iCount);
	if (iCount == 0)
		return;

	FMOD::Channel* pChannel = nullptr;
	iter->second.pChannelGroup->getChannel(0, &pChannel);
	FMOD_VECTOR vSpeakerPos = { xmPos.x, xmPos.y, xmPos.z };
	pChannel->set3DAttributes(&vSpeakerPos, nullptr);
}

void CSoundManager::Set_PlaySecPosition(wstring wstrSoundKey, _uint iPosMS)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	int iChannelCount;
	iter->second.pChannelGroup->getNumChannels(&iChannelCount);
	for (int i = 0; i < iChannelCount; ++i)
	{
		FMOD::Channel* pChannel = nullptr;
		iter->second.pChannelGroup->getChannel(i, &pChannel);
		pChannel->setPosition(iPosMS, FMOD_TIMEUNIT_MS);
	}
}

bool CSoundManager::Is_Playing(wstring wstrSoundKey)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return false;

	bool bPlaying;
	iter->second.pChannelGroup->isPlaying(&bPlaying);
	return bPlaying;
}

_uint CSoundManager::Get_Length(wstring wstrSoundKey)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return 0;

	_uint iLength = 0;
	iter->second.pSound->getLength(&iLength, FMOD_TIMEUNIT_MS);
	return iLength;
}

void CSoundManager::Set_3DCustomRolloff(wstring wstrSoundKey, vector<pair<float, float>>& vecLevel)
{
	auto iter = m_mapSound.find(wstrSoundKey);
	if (iter == m_mapSound.end())
		return;

	iter->second.pSound->setMode(FMOD_3D | FMOD_3D_CUSTOMROLLOFF);

	int iCount = (int)vecLevel.size();
	FMOD_VECTOR* pCurve = new FMOD_VECTOR[iCount];
	for (int i = 0; i < iCount; ++i)
	{
		pCurve[i].x = vecLevel[i].first;
		pCurve[i].y = vecLevel[i].second;
		pCurve[i].z = 0.f;
	}

	// x는 거리, y는 볼륨 그 사이는 Linear로 처리됨
	// 예시
	//  { 0.0f,  1.0f, 0.0f },
	//	{ 2.0f, 0.2f, 0.0f },
	//	{ 20.0f, 0.0f, 0.0f }

	iter->second.pSound->set3DCustomRolloff(pCurve, iCount);

	m_vecCustomRollOffLevel.push_back(pCurve);
}

HRESULT CSoundManager::AddSoundFile(wstring wstrFilePath, wstring wstrSoundKey, CHANNELID eChannelIndex, bool b3D, float fDefaultSpeed)
{
	if (m_mapSound.end() != m_mapSound.find(wstrSoundKey))
	{
		wstring wstrFileSaveInfo = L"AddSoundFile - 이미 있는 사운드 키값입니다 - " + wstrSoundKey;
		MSG_BOX_WSTR(wstrFileSaveInfo.c_str());
		return E_FAIL;
	}

	// char* 변환
	string str = CW2A(wstrFilePath.c_str());

	// 사운드
	FMOD::Sound* pSound = nullptr;
	FMOD_RESULT eRes;
	if (b3D)
		eRes = m_pSystem->createSound(str.c_str(), FMOD_3D, 0, &pSound);
	else
		eRes = m_pSystem->createSound(str.c_str(), FMOD_DEFAULT, 0, &pSound);

	if (eRes == FMOD_OK)
	{
		m_mapSound.emplace(wstrSoundKey, SOUNDINFO());
		m_mapSound[wstrSoundKey].pSound = pSound;
		m_pSystem->createChannelGroup(str.c_str(), &m_mapSound[wstrSoundKey].pChannelGroup);
		m_pChannelGroup[(_uint)eChannelIndex]->addGroup(m_mapSound[wstrSoundKey].pChannelGroup);
		m_mapSound[wstrSoundKey].pChannelGroup->setPitch(fDefaultSpeed);
		return S_OK;
	}
	else
		return E_FAIL;
}

HRESULT CSoundManager::AddSoundFile_Folder(wstring wstrFileName, wstring wstrFolderPath, CHANNELID eChannelIndex, bool b3D, float fDefaultSpeed)
{
	FindFiles(wstrFileName, wstrFolderPath, eChannelIndex, b3D, fDefaultSpeed);

	return S_OK;
}

void CSoundManager::Play_2D_List()
{
	for (auto& iter = m_vecPlay2DList.begin(); iter != m_vecPlay2DList.end();)
	{
		if (false == Is_Playing(iter->first[iter->second]))
		{
			++(iter->second);
			if (iter->second == iter->first.size())
			{
				iter = m_vecPlay2DList.erase(iter);
				continue;
			}
			else
			{
				auto iter2 = m_mapSound.find(iter->first[iter->second]);
				if (iter2 == m_mapSound.end())
					return;

				FMOD::Channel* pNewChannel = nullptr;
				m_pSystem->playSound(iter2->second.pSound, iter2->second.pChannelGroup, false, &pNewChannel);
			}
		}
		++iter;
	}
}

void CSoundManager::Play_3D_List()
{
	for (auto& iter = m_vecPlay3DList.begin(); iter != m_vecPlay3DList.end();)
	{
		if (false == Is_Playing(iter->vecList[iter->iPlayIndex]))
		{
			++(iter->iPlayIndex);
			if (iter->iPlayIndex == iter->vecList.size())
			{
				iter = m_vecPlay3DList.erase(iter);
				continue;
			}
			else
			{
				auto iter2 = m_mapSound.find(iter->vecList[iter->iPlayIndex]);
				if (iter2 == m_mapSound.end())
					return;

				FMOD::Channel* pNewChannel = nullptr;
				m_pSystem->playSound(iter2->second.pSound, iter2->second.pChannelGroup, false, &pNewChannel);
				pNewChannel->set3DAttributes(&iter->vPos, nullptr);
			}
		}
		++iter;
	}
}

void CSoundManager::FindFiles(const wstring& rFile, const wstring& rFolder, CHANNELID eChannelIndex, bool b3D, float fDefaultSpeed)
{
	HANDLE hFileSearch;
	WIN32_FIND_DATA wfd;
	TCHAR AbsolutePath[_MAX_PATH] = L"";
	char CheckMesh[MAX_PATH] = "";

	WideCharToMultiByte(CP_ACP,
		0,
		rFile.c_str(),
		(int)rFile.length() + 1,
		CheckMesh, MAX_PATH, 0, 0);

	char full[_MAX_PATH] = "";

	_fullpath(full, CheckMesh, _MAX_PATH);

	MultiByteToWideChar(CP_ACP,
		0,
		full,
		(int)strlen(full) + 1,
		AbsolutePath,
		MAX_PATH);

	hFileSearch = FindFirstFile(AbsolutePath, &wfd);
	if (hFileSearch == INVALID_HANDLE_VALUE)
		return;

	vector<wstring> vecSubFolders;

	do
	{
		wstring wstrFileNameOriginal = wfd.cFileName;
		// Dot 폴더 제외
		if (wstrFileNameOriginal == L"." || wstrFileNameOriginal == L"..")
			continue;
		// 숨김파일 제외
		if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN)
			continue;
		// 하위 폴더는 목록에 넣고 넘기기
		if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
		{
			vecSubFolders.push_back(wstrFileNameOriginal);
			continue;
		}

		wstring wstrFullPath = rFolder + wstrFileNameOriginal;
		CString cstrFileName = wfd.cFileName;
		TCHAR szRelative[_MAX_PATH] = L"";
		lstrcpy(szRelative, cstrFileName);
		//PathRemoveFileSpec(szRelative);
		PathRemoveExtension(szRelative);
		//wstring wstrFileName = PathFindFileName(szRelative);
		wstring wstrFileName = szRelative;

		if (FAILED(AddSoundFile(wstrFullPath, wstrFileName, eChannelIndex, b3D, fDefaultSpeed)))
		{
			wstring wstrMessage = L"소리 삽입 실패 - " + wstrFullPath + L" // " + wstrFileName;
			MSG_BOX_WSTR(wstrMessage.c_str());
		}

	} while (FindNextFile(hFileSearch, &wfd));

	for (auto& iter : vecSubFolders)
		FindFiles(rFolder + iter + L"/*.*", rFolder + iter + L"/", eChannelIndex, b3D, fDefaultSpeed);

	vecSubFolders.clear();
}