#include "DescriptorHeapManager.h"

IMPLEMENT_SINGLETON(CDescriptorHeapManager)

CDescriptorHeapManager::CDescriptorHeapManager()
{
}

CDescriptorHeapManager::~CDescriptorHeapManager()
{
}

HRESULT CDescriptorHeapManager::Create_DescriptorHeap(_uint nDescriptor, ID3D12Device* pDevice)
{
	D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
	srvHeapDesc.NumDescriptors = nDescriptor;
	srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	if (FAILED(pDevice->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(&m_pSrvDescriptorHeap))))
		return E_FAIL;

	m_CpuDescriptorHandle = m_pSrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	m_GpuDescriptorHandle = m_pSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart();

	return NOERROR;
}

HRESULT CDescriptorHeapManager::Create_DsvDescriptorHeap(_uint nDescriptor, ID3D12Device* pDevice)
{
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
	dsvHeapDesc.NumDescriptors = nDescriptor;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dsvHeapDesc.NodeMask = 0;

	if (FAILED(pDevice->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&m_pDsvDescriptorHeap))))
		return E_FAIL;

	m_CpuDsvDescriptorHandle = m_pDsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

	return NOERROR;
}

HRESULT CDescriptorHeapManager::Release()
{
	m_pSrvDescriptorHeap->Release();
	m_pDsvDescriptorHeap->Release();

	return NOERROR;
}

