#ifndef __SHADER__
#define __SHADER__

#include "Component.h"

class CShader : public CComponent
{
public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CShader* Create();

	virtual void SetPipeline(ID3D12GraphicsCommandList* pd3dCommandList, int nPipelineState = 0);

	HRESULT Release();

protected:
	explicit CShader();
	explicit CShader(const CShader& rhs);
	virtual ~CShader(void);

	virtual ID3D12PipelineState* Create_PipeLineState(D3D12_PRIMITIVE_TOPOLOGY_TYPE d3dPrimitiveTopology = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE);
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_GeometryShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_HullShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_DomainShader(ID3DBlob** ppBlob);
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();
	virtual D3D12_BLEND_DESC Create_BlendState();
	virtual D3D12_DEPTH_STENCIL_DESC Create_DepthStencilState();
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();

	D3D12_SHADER_BYTECODE CompileShaderFromFile(const wchar_t* pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob** ppd3dShaderBlob);

	ID3D12PipelineState* m_pPipelineState = nullptr;

public:
	ID3D12Resource* CreateShaderVariables(ID3D12Resource* pd3dcbResource, _uint icbSize);
	void UpdateShaderVariables(CB_GAMEOBJECT_INFO* pcbGameObjInfo, XMFLOAT4X4 matWorld, XMFLOAT4X4 matTexTransform, MATERIAL matMaterial = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) });

};

#endif
