#include "Scene_Boardmap.h"
#include "Scene_Fallingmap.h"
#include "Scene_Ggoggomap.h"
#include "Scene_Bullfightmap.h"
#include "Scene_OneMindMap.h"
#include "Scene_WaitingRoom.h"
#include "Scene_Prize.h"
#include "Management.h"
#include "TeddyBear.h"
#include "Player.h"
#include "BoundingBox.h"
#include "Font.h"
#include "UI.h"
#include "BillBoardObject.h"
#include "CoinEffect.h"
#include "Camera_Third.h"
#include "Camera_Obj.h"
#include "Dice.h"
#include "BlackBackground.h"
#include "StaticObject.h"
#include "Balloon.h"
#include "Button.h"
#include "Camera_Fixed.h"
#include "Bird.h"
#include "Ghost.h"

extern bool G_SC;

extern CPlayer* Player;
extern CPlayer* Players[3];

CTeddyBear* pTeddyBear[2] = { nullptr, };

double timetest = 0;
double timetest2 = 0;

CScene_Boardmap::CScene_Boardmap()
{
}

CScene_Boardmap::~CScene_Boardmap()
{
}

bool IntersectObject(CPlayer* pHit, CPlayer* pPulledPlayer, float TimeDelta, XMFLOAT3& shift = XMFLOAT3(0.f, 0.f, 0.f))
{
	BoundingBox xmBoundingBox[2];
	xmBoundingBox[0] = pHit->Get_TeddyBear()->m_xmBoundingBox;
	xmBoundingBox[1] = pPulledPlayer->Get_TeddyBear()->m_xmBoundingBox;
	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&pHit->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
	xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&pPulledPlayer->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));

	if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
	{
		XMFLOAT3 xmLookVector = Vector3::Normalize(Vector3::Subtract(pPulledPlayer->GetPosition(), XMFLOAT3(pHit->GetPosition().x + 0.3f, pHit->GetPosition().y, pHit->GetPosition().z + 0.3f)));
		xmLookVector.y = 0.f;
		pPulledPlayer->xmf3Shift = Vector3::Add(XMFLOAT3(0.f, 0.f, 0.f), xmLookVector, PLAYERSPEED * TimeDelta);
		pPulledPlayer->m_tPlayerInfo.xmPosition = Vector3::Add(pPulledPlayer->m_tPlayerInfo.xmPosition, pPulledPlayer->xmf3Shift);
		shift = pPulledPlayer->xmf3Shift; //이동량
		pPulledPlayer->xmf3Shift = { 0.f,0.f,0.f };
		return true;
	}
	return false;
}

void IntersectObjectTest(CPlayer* pHit, CTeddyBear* pTeddy, float TimeDelta)
{
	BoundingBox xmBoundingBox[2];
	xmBoundingBox[0] = pHit->Get_TeddyBear()->m_xmBoundingBox;
	xmBoundingBox[1] = pTeddy->m_xmBoundingBox;
	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&pHit->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
	xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&pTeddy->m_pTransformCom->GetWorldMat()));

	if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
	{
		XMFLOAT3 xmLookVector = Vector3::Normalize(Vector3::Subtract(pTeddy->m_pTransformCom->GetPosition(), pHit->GetPosition()));
		xmLookVector.y = 0.f;
		pTeddy->m_pTransformCom->SetPosition(Vector3::Add(pTeddy->m_pTransformCom->GetPosition(), xmLookVector, PLAYERSPEED * TimeDelta));
	}
}

CGameObject* CScene_Boardmap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz, int _tag)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position.x = x;
	temp.position.y = y;
	temp.position.z = z;

	temp.rotation.x = rx;
	temp.rotation.y = ry;
	temp.rotation.z = rz;

	temp.tag = _tag;

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENEID::SCENE_BOARDMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

CGameObject* CScene_Boardmap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale, SCENEID scene)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position = transform;
	temp.rotation = rotation;
	temp.scale = scale;
	if (FAILED(pManagement->Add_GameObjectToLayer(scene, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

HRESULT CScene_Boardmap::Initialize_Scene()
{
	m_pPlatformManager = CPlatformManager::Create();

	moveDelta = 0.f;

	m_SceneId = SCENEID::SCENE_BOARDMAP;

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);

	CManagement* pManagement = CManagement::GetInstance();

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");
	

	CreateObject(L"Layer_BackGround", L"GameObject_SkyBox", 29.7602, 15.1374, -32.9217, -90.f, 0.f, 0.f);



	if (SERVERCONNECT == false)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_STATIC, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&pTeddyBear[1])))
			return E_FAIL;
		pTeddyBear[1]->m_pTransformCom->SetPosition(XMFLOAT3(-9.f, 13.6f, -71.f));
	}

	pDice = dynamic_cast<CDice*>(CreateObject(L"Layer_BackGround", L"GameObject_Dice", 0, 10000000, 0, -90.f, 0, 0));
	m_pMiniDice = dynamic_cast<CDice*>(CreateObject(L"Layer_BackGround", L"GameObject_Dice", XMFLOAT3(0, 10000000, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0006f, 0.0006f, 0.0006f)));
	m_pMiniDice->setTexIndex(1);
	m_pMiniDice->setMiniDice(true);

	m_pUsingMiniDice = dynamic_cast<CDice*>(CreateObject(L"Layer_BackGround", L"GameObject_Dice", XMFLOAT3(0, 10000000, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0007f, 0.0007f, 0.0007f)));
	m_pUsingMiniDice->setTexIndex(1);

	ObjWorld temp;
	temp.position = XMFLOAT3(0.f, 0.f, 0.f);
	temp.rotation = XMFLOAT3(0.f, 0.f, 0.f);
	temp.scale = XMFLOAT3(0.8f, 1.2f, 0.7f);
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BoundingBox", (CGameObject**)&pBoundingBox[0], &temp)))
		return E_FAIL;

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BoundingBox", (CGameObject**)&pBoundingBox[1], &temp)))
		return E_FAIL;

	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(26, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(46, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(56, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(26, 1.7, -77.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(26, 1.7, -67.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(26, 1.7, -57.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(56, 1.7, -40.01), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -77.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -67.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -57.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -47.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(-13.98001, 1.7, -77.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(-13.98001, 1.7, -67.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(-13.98001, 1.7, -57.99), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(36, 1.7, -30), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -32.1), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(16, 1.7, -50), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -22.1), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -12.1), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(26, 1.7, -2.079998), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(46, 1.7, -2.08), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(56, 1.7, -2.079998), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(36, 1.7, -2.08), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(-13.98001, 1.7, -22.1), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(-13.98001, 1.7, -12.1), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -2.099998), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(66, 1.7, -40.01), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(6, 1.7, -50), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(16, 1.7, -2.08), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(6, 1.7, -2.08), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(16, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(6, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(16, 1.7, -40), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(16, 1.7, -30), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(6, 1.7, -30), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(-14, 1.7, -39.7), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(25, 13.4, -11.8), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(15, 13.4, -11.8), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(25, 13.4, -21.8), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_Wall", XMFLOAT3(15, 13.4, -21.8), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalfTower", XMFLOAT3(-3.9, 1.700001, -75.87001), XMFLOAT3(0, 90, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalfTower", XMFLOAT3(66, 1.7, -75.89999), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalfTower", XMFLOAT3(66, 1.7, -2.099998), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalfTower", XMFLOAT3(-3.980015, 1.7, -2.099998), XMFLOAT3(0, 180, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(26, 1.7, -39.69), XMFLOAT3(0, 90, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(36, 1.7, -39.71), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(76, 1.7, -28.01), XMFLOAT3(0, 180, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(-13.98001, 1.7, -48), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(36, 1.7, -85.88), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(16, 1.7, -20), XMFLOAT3(0, 90, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(-14, 1.7, -31.3), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallHalf", XMFLOAT3(26, 1.7, -50), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCorner", XMFLOAT3(46, 1.7, -49.69), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCorner", XMFLOAT3(46, 1.7, -20.01), XMFLOAT3(0, 180, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerTopCorner", XMFLOAT3(71.1, 14.65, -91), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerTopCorner", XMFLOAT3(-18.88001, 14.65, -81), XMFLOAT3(0, 90, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerTopCorner", XMFLOAT3(81.1, 14.65, 3.170002), XMFLOAT3(0, 270, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerTopCorner", XMFLOAT3(-8.880014, 14.65, 13.2), XMFLOAT3(0, 180, 0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareMidWindow", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareMidWindow", XMFLOAT3(6.3, 46.0, -20.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareMidWindow", XMFLOAT3(6.3, 35.9, -20.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareMidWindow", XMFLOAT3(26.3, 26.5, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareMidWindow", XMFLOAT3(6.3, 26.5, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquare", XMFLOAT3(25.9, 13.7, -39.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquare", XMFLOAT3(6.0, 13.7, -39.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquare", XMFLOAT3(-4.4, 13.7, -39.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.2, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareTopRoofHigh", XMFLOAT3(6.5, 65.1, -21.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareTopRoofHigh", XMFLOAT3(26.3, 46.9, -40.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareArch", XMFLOAT3(26.1, 13.8, -75.1), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareArch", XMFLOAT3(56.1, 13.8, -39.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareArch", XMFLOAT3(7.3, 25.6, -18.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.5, 1.0, 0.8));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareArch", XMFLOAT3(13.1, 25.6, -38.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.5, 1.0, 0.8));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareTop", XMFLOAT3(7.7, 34.6, -16.5), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(2.2, 1.0, 0.6));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareTop", XMFLOAT3(11.3, 34.6, -38.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(2.0, 1.0, 0.6));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareTop", XMFLOAT3(6.1, 36.7, -39.8), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareRoof", XMFLOAT3(25.8, 24.1, -75.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareRoof", XMFLOAT3(56.3, 24.1, -39.6), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerBase", XMFLOAT3(-4.0, 26.6, -40.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerBase", XMFLOAT3(-4.0, 26.6, -40.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerBalcony", XMFLOAT3(-4.0, 39.8, -40.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerTopRoof", XMFLOAT3(-4.0, 51.9, -40.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_TowerSquareMidOpen", XMFLOAT3(6.3, 56.2, -20.9), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(35.0, 13.4, -21.8), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(-5.0, 13.4, -21.8), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(24.2, 24.9, -1.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(35.0, 13.4, -1.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(5.0, 24.9, -1.8), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(24.2, 24.9, -21.8), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(-5.0, 13.4, -1.8), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));
	CreateObject(L"Layer_BackGround", L"GameObject_WallCornerHalf", XMFLOAT3(5.0, 24.9, -21.8), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.0, 1.0, 1.0));

	//바깥쪽
		//Left 0~9
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -80.5, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -71, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -63, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -55, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -45, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -35, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -25, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -15, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, -6, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", -9, 13.6, 2.83, 0, 0, 0)));
	//Top 10~16
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 1, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 11, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 21, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 41, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 51, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 61, 13.6, 2.83, 0, 0, 0)));
	//Right 17~26
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, 2.83, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -6, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -15, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -25, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -35, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -45, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -55, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -63, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -71, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 71, 13.6, -80.5, 0, 0, 0)));
	//Bottom 27~33
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 61, 13.6, -80.5, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 51, 13.6, -80.5, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 41, 13.6, -80.5, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, -80.5, 0, 0, 0))); //30
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 21, 13.6, -80.5, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 11, 13.6, -80.5, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 1, 13.6, -80.5, 0, 0, 0)));
	//안쪽 34~41
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 21, 13.6, -35, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 21, 13.6, -25, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, -25, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 41, 13.6, -25, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 41, 13.6, -35, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 41, 13.6, -45, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, -45, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 21, 13.6, -45, 0, 0, 0)));
	//Left 밑에 다리 42 43
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 1, 13.6, -45, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 11, 13.6, -45, 0, 0, 0)));

	//Right 다리 44 45
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 61, 13.6, -35, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 51, 13.6, -35, 0, 0, 0)));

	//Bottom 다리 46 47 48
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, -71, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, -63, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 31, 13.6, -55, 0, 0, 0)));

	//Left 위에 다리 49 50
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 1, 13.6, -25, 0, 0, 0)));
	m_pPlatformManager->Add_Platform(dynamic_cast<CPlatform*>(CreateObject(L"Layer_BackGround", L"GameObject_Platform", 11, 13.6, -25, 0, 0, 0)));

	m_pPlatformManager->Get_Platform(4)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(4)->Set_NextPlatformIndex(42);

	m_pPlatformManager->Get_Platform(5)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(5)->Set_NextPlatformIndex(4);

	m_pPlatformManager->Get_Platform(43)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(43)->Set_NextPlatformIndex(41);

	m_pPlatformManager->Get_Platform(6)->Set_IsSinglePlatform(false);
	m_pPlatformManager->Get_Platform(6)->Set_NextPlatformIndex(5, 1);
	m_pPlatformManager->Get_Platform(6)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(6)->Set_NextPlatformIndex2(7, 2);

	m_pPlatformManager->Get_Platform(49)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(49)->Set_NextPlatformIndex(6);

	m_pPlatformManager->Get_Platform(50)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(50)->Set_NextPlatformIndex(49);

	m_pPlatformManager->Get_Platform(30)->Set_IsSinglePlatform(false);
	m_pPlatformManager->Get_Platform(30)->Set_NextPlatformIndex(29, 1);
	m_pPlatformManager->Get_Platform(30)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(30)->Set_NextPlatformIndex2(31, 2);

	m_pPlatformManager->Get_Platform(21)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(21)->Set_NextPlatformIndex(44);

	for (int i = 29; i > 21; --i)
	{
		m_pPlatformManager->Get_Platform(i)->Set_IsNextPlatformContinue(false);
		m_pPlatformManager->Get_Platform(i)->Set_NextPlatformIndex(i - 1);
	}

	for (int i = 39; i > 35; --i)
	{
		m_pPlatformManager->Get_Platform(i)->Set_IsNextPlatformContinue(false);
		m_pPlatformManager->Get_Platform(i)->Set_NextPlatformIndex(i - 1);
	}

	m_pPlatformManager->Get_Platform(34)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(34)->Set_NextPlatformIndex(41);

	m_pPlatformManager->Get_Platform(35)->Set_IsSinglePlatform(false);
	m_pPlatformManager->Get_Platform(35)->Set_NextPlatformIndex(34, 1);
	m_pPlatformManager->Get_Platform(35)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(35)->Set_NextPlatformIndex2(50, 0);

	m_pPlatformManager->Get_Platform(40)->Set_IsSinglePlatform(false);
	m_pPlatformManager->Get_Platform(40)->Set_NextPlatformIndex(39, 0);
	m_pPlatformManager->Get_Platform(40)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(40)->Set_NextPlatformIndex2(48, 2);

	m_pPlatformManager->Get_Platform(48)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(48)->Set_NextPlatformIndex(47);

	m_pPlatformManager->Get_Platform(47)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(47)->Set_NextPlatformIndex(46);

	m_pPlatformManager->Get_Platform(46)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(46)->Set_NextPlatformIndex(30);

	m_pPlatformManager->Get_Platform(41)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(41)->Set_NextPlatformIndex(40);

	m_pPlatformManager->Get_Platform(45)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(45)->Set_NextPlatformIndex(38);

	m_pPlatformManager->Get_Platform(33)->Set_IsNextPlatformContinue(false);
	m_pPlatformManager->Get_Platform(33)->Set_NextPlatformIndex(0);


	m_pPlatformManager->Get_Platform(1)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(5)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(7)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(9)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(12)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(15)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(17)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(19)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(23)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(25)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(30)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(32)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(35)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(37)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(40)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(41)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(42)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(44)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(45)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);
	m_pPlatformManager->Get_Platform(46)->Set_PlatformColor(CPlatform::platform_blue, 70, 163, 255, 256);

	m_pPlatformManager->Get_Platform(2)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(13)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(20)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(28)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(33)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(38)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(48)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);
	m_pPlatformManager->Get_Platform(50)->Set_PlatformColor(CPlatform::platform_red, 255, 81, 81, 256);

	::Player->m_tPlayerInfo.xmPosition = m_pPlatformManager->
		Get_Platform(::Player->Get_CurrentPlatformIndex())->m_pTransformCom->GetPosition();

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Button", (CGameObject**)&m_pButton, m_pPlatformManager)))
		return E_FAIL;

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Button", (CGameObject**)&m_pStoreButton, m_pPlatformManager)))
		return E_FAIL;

	m_pStoreButton->setStoreButton(true);

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Balloon", (CGameObject**)&m_pBalloon, m_pPlatformManager)))
		return E_FAIL;
	
	m_pBalloon->setButton(m_pButton);

	XMFLOAT3 storePos[8];
	for (int i = 0; i < 8; i++)
	{
		storePos[i] = m_pPlatformManager->Get_Platform(m_storeIndex[i])->m_pTransformCom->GetPosition();
		m_pPlatformManager->Get_Platform(m_storeIndex[i])->Set_PlatformColor(CPlatform::platform_store, 145, 235, 63, 256);
		storePos[i].y -= 0.09f;

		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Store", nullptr, &storePos[i])))
			return E_FAIL;
	}

	XMFLOAT3 EventPos[8];
	for (int i = 0; i < 8; i++)
	{
		EventPos[i] = m_pPlatformManager->Get_Platform(m_eventIndex[i])->m_pTransformCom->GetPosition();
		m_pPlatformManager->Get_Platform(m_eventIndex[i])->Set_PlatformColor(CPlatform::platform_event, 145, 235, 63, 256);
		EventPos[i].y -= 0.09f;

		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Event", nullptr, &EventPos[i])))
			return E_FAIL;
	}


	pButtonIcon[0] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_ClothesButton", XMFLOAT3(0.1f, 0.8f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.035f, 0.04f, 0.1f));
	pCoinIcon[0] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_StarCoin", XMFLOAT3(0.1f, 0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.035f, 0.04f, 0.1f));
	pPlayerIcon[0] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_PlayerIcon", XMFLOAT3(-0.03f, 0.85f, 0.f) ,XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.07f, 0.1f, 0.1f));
	pPlayerIcon[0]->SetTexIndex(Player->GetSkinid());
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pStarText[0])))
		return E_FAIL;
	pStarText[0]->SetText(L"10", XMFLOAT2(0.585f, 0.027f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pClothesButtonText[0])))
		return E_FAIL;
	pClothesButtonText[0]->SetText(L"0", XMFLOAT2(0.585f, 0.077f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pRankText[0])))
		return E_FAIL;
	pRankText[0]->SetText(L"1st", XMFLOAT2(0.46f, 0.129f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pNameText[0])))
		return E_FAIL;
	pNameText[0]->SetText(L"Player1", XMFLOAT2(0.535f, 0.129f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pLineText[0])))
		return E_FAIL;
	pLineText[0]->SetText(L"_________________", XMFLOAT2(0.457f, 0.103f), XMFLOAT2(0.5f, 0.5f));


	pButtonIcon[1] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_ClothesButton", XMFLOAT3(0.45f, 0.8f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.035f, 0.04f, 0.1f));
	pCoinIcon[1] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_StarCoin", XMFLOAT3(0.45f, 0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.035f, 0.04f, 0.1f));
	pPlayerIcon[1] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_PlayerIcon", XMFLOAT3(0.32f, 0.85f, 0.f), XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.07f, 0.1f, 0.1f));
	pPlayerIcon[1]->SetTexIndex(3);
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pStarText[1])))
		return E_FAIL;
	pStarText[1]->SetText(L"10", XMFLOAT2(0.76f, 0.027f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pClothesButtonText[1])))
		return E_FAIL;
	pClothesButtonText[1]->SetText(L"0", XMFLOAT2(0.76f, 0.077f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pRankText[1])))
		return E_FAIL;
	pRankText[1]->SetText(L"2nd", XMFLOAT2(0.635f, 0.13f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pNameText[1])))
		return E_FAIL;
	pNameText[1]->SetText(L"Player2", XMFLOAT2(0.710f, 0.129f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pLineText[1])))
		return E_FAIL;
	pLineText[1]->SetText(L"_________________", XMFLOAT2(0.632f, 0.103f), XMFLOAT2(0.5f, 0.5f));


	pButtonIcon[2] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_ClothesButton", XMFLOAT3(0.8f, 0.8f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.035f, 0.04f, 0.1f));
	pCoinIcon[2] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_StarCoin", XMFLOAT3(0.8f, 0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.035f, 0.04f, 0.1f));
	pPlayerIcon[2] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_PlayerIcon", XMFLOAT3(0.67f, 0.85f, 0.f), XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.07f, 0.1f, 0.1f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pStarText[2])))
		return E_FAIL;
	pStarText[2]->SetText(L"10", XMFLOAT2(0.935f, 0.025f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pClothesButtonText[2])))
		return E_FAIL;
	pClothesButtonText[2]->SetText(L"0", XMFLOAT2(0.935f, 0.077f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pRankText[2])))
		return E_FAIL;
	pRankText[2]->SetText(L"3rd", XMFLOAT2(0.81f, 0.13f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pLineText[2])))
		return E_FAIL;
	pLineText[2]->SetText(L"_________________", XMFLOAT2(0.807f, 0.103f), XMFLOAT2(0.5f, 0.5f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pNameText[2])))
		return E_FAIL;
	pNameText[2]->SetText(L"Player3", XMFLOAT2(0.885f, 0.129f), XMFLOAT2(0.7f, 0.7f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pBillBoardObject[0])))
		return E_FAIL;
	pBillBoardObject[0]->SetSize(XMFLOAT2(1.0f, 1.0f));
	pBillBoardObject[0]->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pBillBoardObject[0]->SetClipSize(XMFLOAT2(0.1917f, 0.0909090909090f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&m_pItemDiceBill)))
		return E_FAIL;
	m_pItemDiceBill->SetSize(XMFLOAT2(1.0f, 1.0f));
	m_pItemDiceBill->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	m_pItemDiceBill->SetClipSize(XMFLOAT2(0.1917f, 0.0909090909090f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&m_pPlusBill)))
		return E_FAIL;
	m_pPlusBill->SetSize(XMFLOAT2(0.5f, 0.5f));
	m_pPlusBill->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	m_pPlusBill->SetTexture(L"Component_Texture_Plus");
	m_pPlusBill->SetClipSize(XMFLOAT2(1.f, 1.f));
	m_pPlusBill->SetStartPos(XMFLOAT2(0.f, 0.f));
	
	for (int i = 0; i < 3; ++i) {
		pSocreBoadButtonIcon[i] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_ClothesButton", XMFLOAT3(0.2f, 0.6f - float(i * 0.55f), 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.07f, 0.08f, 0.1f));
		pSocreBoadCoinIcon[i] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_StarCoin", XMFLOAT3(0.5f, 0.6f - float(i * 0.55f), 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.07f, 0.08f, 0.1f));
		pSocreBoadPlayerIcon[i] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_PlayerIcon", XMFLOAT3(-0.45f, 0.6f - float(i * 0.55f), 0.f), XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.14f, 0.2f, 0.1f));
		pSocreBoadPlayerIcon[i]->SetTexIndex(Player->GetSkinid());
		pSocreBoadStamp[i] = (CUI*)CreateObject(L"Layer_BackGround", L"GameObject_Stamp", XMFLOAT3(-0.9f, 0.6f - float(i * 0.55f), 0.f), XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(0.06f, 0.06f, 0.1f));
		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pSocreBoadStarText[i])))
			return E_FAIL;
		pSocreBoadStarText[i]->SetText(L"10", XMFLOAT2(0.81f, 0.155f + float(i * 0.28f)), XMFLOAT2(1.4f, 1.4f));
		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pSocreBoadClothesButtonText[i])))
			return E_FAIL;
		pSocreBoadClothesButtonText[i]->SetText(L"0", XMFLOAT2(0.66f, 0.155f + float(i * 0.28f)), XMFLOAT2(1.4f, 1.4f));
		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pSocreBoadRankText[i])))
			return E_FAIL;
		pSocreBoadRankText[i]->SetText(L"1st", XMFLOAT2(0.1f, 0.15f + float(i * 0.28f)), XMFLOAT2(1.4f, 1.4f));
		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pSocreBoadNameText[i])))
			return E_FAIL;
		pSocreBoadNameText[i]->SetText(L"Player1", XMFLOAT2(0.37f, 0.15f + float(i * 0.28f)), XMFLOAT2(1.4f, 1.4f));
		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pSocreBoadPlusText[i])))
			return E_FAIL;
		pSocreBoadPlusText[i]->SetText(L"+10", XMFLOAT2(0.87f, 0.155f + float(i * 0.28f)), XMFLOAT2(1.4f, 1.4f));
	}


	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pConfetti)))
		return E_FAIL;
	pConfetti->SetSize(XMFLOAT2(3.5f, 3.5f));
	pConfetti->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pConfetti->SetTexture(L"Component_Texture_Confetti");
	pConfetti->SetClipSize(XMFLOAT2(0.125f, 0.125f));
	pConfetti->SetStartPos(XMFLOAT2(0.f, 0.f));
	pConfetti->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pBillBoardObject[1])))
		return E_FAIL;
	pBillBoardObject[1]->SetSize(XMFLOAT2(3.5f, 3.5f));
	pBillBoardObject[1]->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pBillBoardObject[1]->SetTexture(L"Component_Texture_SmokeSprite");
	pBillBoardObject[1]->SetClipSize(XMFLOAT2(0.25f, 0.25f));
	pBillBoardObject[1]->SetStartPos(XMFLOAT2(0.f, 0.f));
	pBillBoardObject[1]->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));

	/*if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pHerePlayer)))
		return E_FAIL;
	pHerePlayer->SetSize(XMFLOAT2(1.0f, 1.0f));
	pHerePlayer->SetTexture(L"Component_Texture_HerePlayer", SCENEID::SCENE_STATIC);
	pHerePlayer->SetClipSize(XMFLOAT2(1.f, 1.f));
	pHerePlayer->SetStartPos(XMFLOAT2(0.f, 0.f));
	pHerePlayer->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));*/

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pBilStore)))
		return E_FAIL;
	pBilStore->SetSize(XMFLOAT2(2.f, 2.f));
	pBilStore->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pBilStore->SetTexture(L"Component_Texture_ShopSprite");
	pBilStore->SetClipSize(XMFLOAT2(0.3333f, 1.f));
	pBilStore->SetStartPos(XMFLOAT2(0.f, 0.f));
	pBilStore->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pBilStore->SetIndexX(-1);

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&pBilMap)))
		return E_FAIL;
	pBilMap->SetSize(XMFLOAT2(2.f, 2.f));
	pBilMap->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pBilMap->SetTexture(L"Component_Texture_MapSprite");
	pBilMap->SetClipSize(XMFLOAT2(0.3333f, 1.f));
	pBilMap->SetStartPos(XMFLOAT2(0.f, 0.f));
	pBilMap->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
	pBilMap->SetIndexX(-1);

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&m_pPlus3Icon)))
		return E_FAIL;
	m_pPlus3Icon->SetSize(XMFLOAT2(1.3f, 1.3f));
	m_pPlus3Icon->SetTexture(L"Component_Texture_Plus3Icon");
	m_pPlus3Icon->SetClipSize(XMFLOAT2(1.f, 1.f));
	m_pPlus3Icon->SetStartPos(XMFLOAT2(0.f, 0.f));
	m_pPlus3Icon->SetPostion(XMFLOAT3(0.f, 1000.f, 0.f));
	m_pPlus3Icon->setStoreItem(true);

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_BillBoard", (CGameObject**)&m_pPlus5Icon)))
		return E_FAIL;
	m_pPlus5Icon->SetSize(XMFLOAT2(1.3f, 1.3f));
	m_pPlus5Icon->SetTexture(L"Component_Texture_Plus5Icon");
	m_pPlus5Icon->SetClipSize(XMFLOAT2(1.f, 1.f));
	m_pPlus5Icon->SetStartPos(XMFLOAT2(0.f, 0.f));
	m_pPlus5Icon->SetPostion(XMFLOAT3(0.f, 1000.f, 0.f));
	m_pPlus5Icon->setStoreItem(true);



	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pTurnText)))
		return E_FAIL;
	pTurnText->SetText(L"", XMFLOAT2(0.27f, 0.42f), XMFLOAT2(2.0f, 2.0f));

	pBlack = dynamic_cast<CBlackBackground*>(CreateObject(L"Layer_BackGround", L"GameObject_BlackBackground", XMFLOAT3(0.0f, 0.0f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(1.0f, 0.3f, 0.1f)));
	pScoreBlack = dynamic_cast<CBlackBackground*>(CreateObject(L"Layer_BackGround", L"GameObject_BlackBackground", XMFLOAT3(0.0f, 0.0f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 0.9f, 0.1f)));

	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(21.0, 13.58, -35.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(41.0, 13.58, -35.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.02));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(31.0, 13.58, -62.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.035));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(16.1, 13.58, -25.0), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.049));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(16.1, 13.58, -45.0), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.049));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(55.4, 13.58, -35.0), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.03));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(31.1, 13.58, 2.8), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.08));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(31.1, 13.58, -80.5), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.08));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(-9.0, 13.58, -38.6), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.082));
	CreateObject(L"Layer_BackGround", L"GameObject_UnityPlane", XMFLOAT3(70.9, 13.58, -38.6), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0004, 0.01, 0.082));

	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(66.9, 13.58, -35.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(25.9, 13.58, 2.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(16.8, 13.58, -25.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(21.0, 13.58, -29.5), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(41.0, 13.58, -29.9), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(45.6, 13.58, -35.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(-9.0, 13.58, -29.1), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(-9.0, 13.58, -20.6), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(70.9, 13.58, -40.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(70.9, 13.58, -30.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(35.3, 13.58, -80.5), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(26.9, 13.58, -80.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(26.9, 13.58, -80.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(31.0, 13.58, -76.0), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(35.0, 13.58, -45.0), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(31.0, 13.58, -48.6), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(21.0, 13.58, -40.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(25.8, 13.58, -45.0), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(-9.0, 13.58, -40.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(-4.2, 13.58, -45.0), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));
	CreateObject(L"Layer_BackGround", L"GameObject_Arrow", XMFLOAT3(-9.0, 13.58, -50.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.002, 0.01, 0.003));

	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight01", XMFLOAT3(60.2, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight01", XMFLOAT3(82.7, 1.7, -38.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight01", XMFLOAT3(82.7, 1.7, -77.9), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight01", XMFLOAT3(-22.2, 1.7, -68.7), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight01", XMFLOAT3(-22.2, 1.7, -19.0), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight01", XMFLOAT3(20.4, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(62.5, 1.7, -91.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(52.5, 1.7, -91.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(42.5, 1.7, -91.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(32.5, 1.7, -91.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(22.5, 1.7, -91.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(12.5, 1.7, -91.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(82.7, 1.7, -28.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(82.7, 1.7, -68.0), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(-22.2, 1.7, -78.7), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(50.2, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(-22.2, 1.7, -28.9), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight02", XMFLOAT3(10.4, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight03", XMFLOAT3(82.7, 1.7, -18.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight03", XMFLOAT3(82.7, 1.7, -58.0), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight03", XMFLOAT3(-22.2, 1.7, -88.7), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight03", XMFLOAT3(40.2, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight03", XMFLOAT3(-22.2, 1.7, -38.9), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight03", XMFLOAT3(0.5, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight05", XMFLOAT3(70.2, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight05", XMFLOAT3(82.7, 1.7, -8.4), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight05", XMFLOAT3(82.7, 1.7, -48.2), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight05", XMFLOAT3(30.4, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight05", XMFLOAT3(-22.2, 1.7, -48.7), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight05", XMFLOAT3(-9.4, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight04", XMFLOAT3(-22.2, 1.7, -58.7), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverStraight04", XMFLOAT3(-19.4, 1.7, 15.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(92.7, 1.7, 3.5), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(80.1, 1.7, 15.7), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(82.6, 1.7, -87.6), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(70.4, 1.7, -99.8), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(-9.8, 1.7, -101.2), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(2.5, 1.7, -89.0), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(-19.7, 1.7, -8.9), XMFLOAT3(0.0, 180.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_RiverCorner05", XMFLOAT3(-31.9, 1.7, 3.3), XMFLOAT3(0.0, 270.0, 0.0), XMFLOAT3(1.f, 1.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_GrassPlane", XMFLOAT3(49.1, 13.4, -15.1), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.036, 0.01, 0.03));
	CreateObject(L"Layer_BackGround", L"GameObject_GrassTile", XMFLOAT3(80.2, 1.7, 13.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(10.0, 1.0, 10.3));
	CreateObject(L"Layer_BackGround", L"GameObject_WaterTile2", XMFLOAT3(192.2, -1.7, 109.3), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(50.0, 1.0, 50.0));

	m_pArrowHead = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_ArrowHead", XMFLOAT3(0.f, 0.f, 0.f), XMFLOAT3(-90.f, 0.f, 0.f), XMFLOAT3(0.27f, 0.3f, 0.2f)));
	
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Bird", (CGameObject**)&pBird)))
		return E_FAIL;
	pBird->m_pTransformCom->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Ghost", (CGameObject**)&pGhost)))
		return E_FAIL;
	pGhost->m_pTransformCom->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));

	pLastTurn = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_LastTurn", XMFLOAT3(0.f, 0.5f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.44f, 0.15f, 0.1f)));
	pLastTurn->SetHide(true);

	pJumpUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_Jump", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	pJumpUI->SetHide(true);
	pRoadSignUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_RoadSign", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	pRoadSignUI->SetHide(true);
	pMapUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_MapIcon", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	pMapUI->SetHide(true);
	pDiceUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_DiceIcon", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	pDiceUI->SetHide(true);
	pWhiteUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f)));
	pWhiteUI->SetHide(true);
	pWhiteUI2 = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f)));
	pWhiteUI2->SetHide(true);
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pKeyText)))
		return E_FAIL;
	pKeyText->SetText(L"", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
	pKeyText->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pKeyText2)))
		return E_FAIL;
	pKeyText2->SetText(L"", XMFLOAT2(0.855f, 0.935f), XMFLOAT2(0.5f, 0.5f));
	pKeyText2->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	pKeyWhiteUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f)));
	pKeyWhiteUI->SetHide(true);
	pKeyWhiteUI2 = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f)));
	pKeyWhiteUI2->SetHide(true);

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&m_pPrice)))
		return E_FAIL;
	wstring str = to_wstring(m_nItemPrice[0]) + L" Coin";
	m_pPrice->SetText(str, XMFLOAT2(0.78f, 0.5f), XMFLOAT2(0.75f, 0.75f));
	m_pPrice->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_pPrice->SetHide(true);

	for (int i = 0; i < 3; ++i)
	{
		pSmokeObject[i] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_Smoke", XMFLOAT3(-9.0, -50.f, -50.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.08, 0.08, 0.08), SCENEID::SCENE_STATIC));
	}
	Player->SetSmokeObject(pSmokeObject);

	Player->bButton = 0;
	Player->bCoin = 20;
	Player->Set_CurrentPlatformIndex(0);
	Player->Set_CurrentPlatformPos(m_pPlatformManager->Get_Platform(0)->m_pTransformCom->GetPosition());

	if(!SERVERCONNECT)
		Player->bCoin = 50;

	if (SERVERCONNECT == true)
	{
		for (int i = 0; i < 3; ++i)
		{
			pSmokeObject2[i] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_Smoke", XMFLOAT3(-9.0, -50.f, -50.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.08, 0.08, 0.08), SCENEID::SCENE_STATIC));
		}
		for (int i = 0; i < 3; ++i)
		{
			pSmokeObject3[i] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_Smoke", XMFLOAT3(-9.0, -50.f, -50.2), XMFLOAT3(0.0, 90.0, 0.0), XMFLOAT3(0.08, 0.08, 0.08), SCENEID::SCENE_STATIC));
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		m_pItems[i] = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", m_strItemsName[i].c_str(), XMFLOAT3(0.1f + (i * 0.22f), 0.25f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.105f, 0.18f, 0.1f)));
		m_pItems[i]->SetHide(true);
	}

	m_pItems[0]->SetTexIndex(1);

	m_pBuyButton = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_BuyButton", XMFLOAT3(0.5f, -0.05f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	m_pXButton = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_xButton", XMFLOAT3(0.8f, -0.05f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));

	m_pBuyButtonforChat = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_BuyButton", XMFLOAT3(-0.08, -0.14f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	m_pXButtonforChat = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_xButton", XMFLOAT3(0.08, -0.14f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));

	m_pBuyButton->SetHide(true);
	m_pXButton->SetHide(true);

	m_pBuyButtonforChat->SetHide(true);
	m_pXButtonforChat->SetHide(true);

	m_pInvenXButton = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_xButton", XMFLOAT3(0.5f, -0.35f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f)));
	m_pInvenXButton->SetHide(true);

	m_pChatBoxPurchase = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_chatBox_Purchase", XMFLOAT3(0.f, 0.14f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.44f, 0.15f, 0.1f)));
	m_pChatBoxCoin = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_chatBox_Coin", XMFLOAT3(0.f, 0.14f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.44f, 0.15f, 0.1f)));
	
	m_pChatBoxPurchase->SetHide(true);
	m_pChatBoxCoin->SetHide(true);

	pMiniTutoUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_MiniTuto", XMFLOAT3(0.0, 0.f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(1.f, 1.f, 0.1f)));
	pMiniTutoUI->SetHide(true);
	pMiniTutoUI->SetTexIndex(1);
	float tmp = -0.03f;
	for (int i = 0; i < 9; i++)
	{
		tmp += 0.09;

		if (i % 3 == 0)
			tmp += 0.087;

		m_xmUIPos[i] = XMFLOAT3(-0.16f + tmp, 0.6f, 0.f);
	}

	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	m_pLoading = CLoading::Create(SCENE_FALLINGMAP);
	//m_pLoading = CLoading::Create(SCENE_GGOGGOMAP);
	//m_pLoading = CLoading::Create(SCENE_BULLFIGHTMAP);
	//m_pLoading = CLoading::Create(SCENE_ONEMINDMAP);
	//m_pLoading = CLoading::Create(SCENE_BOARDMAP);
	//m_pLoading = CLoading::Create(SCENE_PRIZE);

	m_SceneChange = SCENE_FALLINGMAP;

	HideScoreBoadUI(true);
	if(SERVERCONNECT == true)
		HidePlayerUI(true);

	m_listMyInven.clear();

	for (int i = 0; i < 3; i++)
		m_listInven[i].clear();

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Boardmap", 1.f, 0, true); //배경음악 재생

	return NOERROR;
}

short CScene_Boardmap::KeyEvent(double TimeDelta)
{
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();
	if (!SERVERCONNECT) {
		if (pKeyMgr->KeyPressing(KEY_SHIFT))
		{
			//Scene_Change(SCENE_FALLINGMAP, this);
			//Scene_Change(SCENE_GGOGGOMAP, this);
			Scene_Change(m_SceneChange, this);
			//Scene_Change(SCENE_ONEMINDMAP, this);
		}
	}

	if (SERVERCONNECT)
	{
		m_fTieCoolTime += TimeDelta;
		if (pKeyMgr->KeyPressing(KEY_SHIFT))
		{
			if (m_fTieCoolTime > 2.f)
			{
				packet_testtie p;

				for (int i = 0; i < 3; ++i)
				{
					if (Players[i]->bRank == 1)
					{
						p.coin = Players[i]->bCoin;
						p.button = Players[i]->bButton;
						break;
					}
				}
				
				Player->bCoin = p.coin;
				Player->bButton = p.button;

				p.size = sizeof(packet_testtie);
				p.type = C2S_TESTTIERANK;
				p.id = Player->Get_ClientIndex();
				
				CServerManager::GetInstance()->SendData(C2S_TESTTIERANK, &p);

				m_fTieCoolTime = 0.f;
			}
		}
	}
	
	return NOERROR;
}

short CScene_Boardmap::Update_Scene(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
	{
		if (m_pDisconLoading == nullptr)
		{
			m_pDisconLoading = CLoading::Create(SCENE_WAITINGROOM);
		}
		else
		{
			if (m_pDisconLoading->Get_Finish())
			{
				CManagement* pManagement = CManagement::GetInstance();

				pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

				if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
					return -1;
			}
		}
		return 0;
	}

	CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();

	m_pArrowHead->m_pTransformCom->Rotate(0.f, 0.f, TimeDelta * 50.f);

	if (m_bEndCameraAnim == false)
	{
		cameraAnimation(TimeDelta);
		return 0;
	}

	if (pMiniTutoUI->GetHide() == false)
	{
		m_fMiniTutoTime += TimeDelta;
		if (m_fMiniTutoTime > 6.f)
		{
			m_fMiniTutoTime = 0.f;
			ReadyChangeScene();
			pMiniTutoUI->SetHide(true);
			HidePlayerUI(false);
		}
	}
	/*
	if (pKeyMgr->KeyDown(KEY_A))
	{
		CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
		pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));
		pCameraFixed->settingCamera(XMFLOAT3(30.f, 80.f, -140.f), XMFLOAT3(30.f, 13.f, -13.f), XMFLOAT3(0.f, 1.f, 0.f));

		HidePlayerUI(true);
		HideScoreBoadUI(false);
	}
	*/
	short result = buttonNpcFunc(TimeDelta);

	if (SERVERCONNECT)
	{
		int nBoardmapEvent = CServerManager::GetInstance()->Get_BoardmapEvent();

		if (nBoardmapEvent >= 0)
		{
			if (nBoardmapEvent == E_BUTTON)
			{
				// 단추 구매 말풍선
				if (CServerManager::GetInstance()->Get_ItemBuy() > 0)
				{

					if (CServerManager::GetInstance()->Get_ItemBuy() == 1) // 구매
					{
						if (CServerManager::GetInstance()->Get_ItemType() == ITEM::ITEM_BUTTON)
						{
							
							if (m_bCheckButton == false)
							{
								CSoundManager::GetInstance()->Play_Sound(L"Buy1");
								m_pButton->setBuyButton(true);
								m_bCheckButton = true;
							}

							bool bBuyButtonEnd = m_pButton->getBuyButtonEnd();

							if (bBuyButtonEnd == true)
							{
								m_pButton->setBuyButtonEnd(false);
								m_isStartMoveBalloon = true;
								m_bCheckButton = false;

								m_nButtonRound++;

								if (m_nButtonRound > 3)
									m_nButtonRound = 0;

								CServerManager::GetInstance()->Set_BoardmapEvent(-1);
								CServerManager::GetInstance()->Set_ItemBuy(0);
								CServerManager::GetInstance()->Set_ItemType(ITEM::ITEM_END);
							}
						}


					}
					else if (CServerManager::GetInstance()->Get_ItemBuy() == 2) // 구매 안함
					{
						CServerManager::GetInstance()->Set_BoardmapEvent(-1);
						CServerManager::GetInstance()->Set_ItemBuy(0);
						CServerManager::GetInstance()->Set_ItemType(ITEM::ITEM_END);
					}
				}
			}
			else if (nBoardmapEvent == E_SHOP)
			{
				if (m_bShowStoreBubble == true)
				{
					pBilStore->SetIndexX(0);
					m_bShowStoreBubble = false;
				}

				if (CServerManager::GetInstance()->Get_ItemBuy() > 0)
				{
					if (CServerManager::GetInstance()->Get_ItemBuy() == 1) // 구매
					{
						_uint nItemType = CServerManager::GetInstance()->Get_ItemType();
						if (m_bBuyItemMotion == false)
						{
							m_bBuyItemMotion = true;
							SetBuyItem(nItemType);
							m_bPurchase = true;					
						}
						
						ShowBuyItemMotion(TimeDelta, nItemType);

						if (m_bPurchase == false && m_bBuyItemMotion == true)
						{
							m_bBuyItemMotion = false;
							pBilStore->SetIndexX(-1);
							m_bShowStoreBubble = true;
							CServerManager::GetInstance()->Set_BoardmapEvent(-1);
							CServerManager::GetInstance()->Set_ItemBuy(0);
							CServerManager::GetInstance()->Set_ItemType(ITEM::ITEM_END);
						}
						
					}
					else if (CServerManager::GetInstance()->Get_ItemBuy() == 2) // 구매 안함
					{
						pBilStore->SetIndexX(-1);
						CServerManager::GetInstance()->Set_BoardmapEvent(-1);
						CServerManager::GetInstance()->Set_ItemBuy(0);
						CServerManager::GetInstance()->Set_ItemType(ITEM::ITEM_END);
					}
				}
			}
			return 0;
		}
	}

	if (result == 0)
		return 0;

	if (m_bNoUpdate)
	{
		m_dScoreTime += TimeDelta;
		m_fScorePlusTime += TimeDelta;

		if (m_dScoreTime > 3.f)
		{
			if (m_fScorePlusTime > 0.1f)
			{
				for (int i = 0; i < 3; ++i)
				{
					if (m_iScorePlus[i] > 0)
					{			
						if (m_bScoreSound == false)
						{
							m_nScoreSound = i;
							m_bScoreSound = true;
						}
						pSocreBoadPlusText[i]->SetTextString(L"+" + to_wstring(m_iScorePlus[i]));
						pSocreBoadStarText[i]->SetTextString(to_wstring(Rankers[i]->bCoin - m_iScorePlus[i]));
						m_iScorePlus[i]--;
					}
					else
					{
						pSocreBoadStarText[i]->SetTextString(to_wstring(Rankers[i]->bCoin));
						pSocreBoadPlusText[i]->SetTextString(L"");
					}
				}
				m_fScorePlusTime = 0.f;
				if (m_nScoreSound != -1)
				{	
					CSoundManager::GetInstance()->Play_Sound(L"Coin_Get");

					if (m_iScorePlus[m_nScoreSound] == 0)
					{
						m_nScoreSound = -1;
					}
				}
			}
		}

		if (m_dScoreTime > 7.f)
		{
			m_dScoreTime = 0.f;
			m_fScorePlusTime = 0.f;
			m_bNoUpdate = false;
			m_bScoreSound = false;
			ShowTurnUI();
		}
		return 0;
	}

	if (CServerManager::GetInstance()->Get_isSpecialEvent() > 0)
	{
		m_iArriveEvent = CServerManager::GetInstance()->Get_isSpecialEvent();
		CServerManager::GetInstance()->Set_isSpecialEvent();
	}

	if (m_iArriveEvent != EVENT_NO)
	{
		CPlayer* eventPlayer = nullptr;

		XMFLOAT3 playerPos;
		XMFLOAT3 playerLook;
		XMFLOAT3 playerRight;
		if (SERVERCONNECT)
		{
			for (int i = 0; i < 3; ++i)
			{
				if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
				{
					eventPlayer = Players[i];
				}
			}

			if (m_bEventIsPlayer)
				eventPlayer = Player;

			playerPos = eventPlayer->m_tPlayerInfo.xmPosition;
			playerLook = eventPlayer->m_tPlayerInfo.xmLook;
			playerRight = eventPlayer->m_tPlayerInfo.xmRight;
		}
		else
		{
			playerPos = Player->m_tPlayerInfo.xmPosition;
			playerLook = Player->m_tPlayerInfo.xmLook;
			playerRight = Player->m_tPlayerInfo.xmRight;
		}

		pCameraThird->SetLookAt(XMFLOAT3(playerPos.x, playerPos.y + 2.f, playerPos.z));
		pCameraThird->SetOffset(XMFLOAT3(0.f, 2.f, -5.0f));
		m_fEventTime += TimeDelta;

		switch (m_iArriveEvent)
		{
		case EVENT_MOVE:
		{
			if (m_bBirdSound == false)
			{
				CSoundManager::GetInstance()->Play_Sound(L"bird");
				m_bBirdSound = true;
			}

			if (m_fEventTime < 3.f)
			{
				pBird->m_pTransformCom->SetUp(XMFLOAT3(0.f, 1.f, 0.f));
				pBird->m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), XMFLOAT3(-playerLook.x, -playerLook.y, -playerLook.z), true));
				pBird->m_pTransformCom->SetLook(XMFLOAT3(-playerLook.x, -playerLook.y, -playerLook.z));
				pBird->m_pTransformCom->SetPosition(Vector3::Add(XMFLOAT3(playerPos.x, playerPos.y + m_fEventMonsterY, playerPos.z), playerLook));
				pBird->m_pTransformCom->Rotate(-90.0f, 0.0f, 0.f);
				pBird->m_pTransformCom->SetScale(2.f, 2.f, 2.f);

				m_fEventMonsterY -= 1.5f * TimeDelta;
			}

			if (m_fEventTime > 7.f)
			{
				if (m_bEventIsPlayer || SERVERCONNECT == false)
				{
					MovePlayerPlatform(Player, m_beforebuttonIndex[m_nButtonRound]);
					Player->m_tPlayerInfo.xmLook = m_lookbutton[m_nButtonRound];
				}
				m_iArriveEvent = EVENT_END;
				m_fEventTime = 0.f;
				m_bBirdSound = false;
				pBird->m_pTransformCom->SetPosition(0.f, 0.f, 0.f);
				/*
				if (pKeyMgr->KeyDown(KEY_SPACE))
				{
					MovePlayerPlatform(Player, m_beforebuttonIndex[m_nButtonRound]);
					m_iArriveEvent = EVENT_NO;
				}
				*/
			}
			break;
		}
		case EVENT_LOST:
		{

			if (m_fEventTime < 3.f)
			{
				//pGhost->m_pTransformCom->SetPosition(Vector3::Add(XMFLOAT3(playerPos.x, playerPos.y + m_fEventMonsterY, playerPos.z), playerLook));

				pGhost->m_pTransformCom->SetUp(XMFLOAT3(0.f, 1.f, 0.f));
				pGhost->m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), XMFLOAT3(-playerLook.x, -playerLook.y, -playerLook.z), true));
				pGhost->m_pTransformCom->SetLook(XMFLOAT3(-playerLook.x, -playerLook.y, -playerLook.z));
				pGhost->m_pTransformCom->SetPosition(Vector3::Add(XMFLOAT3(playerPos.x, playerPos.y + m_fEventMonsterY, playerPos.z), playerLook));
				pGhost->m_pTransformCom->Rotate(-90.0f, 0.0f, 0.f);
				pGhost->m_pTransformCom->SetScale(2.f, 2.f, 2.f);

				m_fEventMonsterY -= 2.f * TimeDelta;
			}

			if (m_fEventTime > 3.5f)
			{
				if (m_bScared)
				{
					if (m_bEventIsPlayer || SERVERCONNECT == false)
					{
						Player->SetAnimation("fall2");
						Player->SetState(OBJECT_STATE::OBJSTATE_EMO);
					}
					pGhost->ChangeAnimation("attack");
					CSoundManager::GetInstance()->Play_Sound(L"Ghost");
					m_bScared = false;
				}
			}

			if (m_fEventTime > 7.f)
			{
				if (m_bEventIsPlayer || SERVERCONNECT == false)
				{
					if (Player->bButton > 0)
					{
						packet_button pb;
						pb.id = Player->Get_ClientIndex();
						pb.size = sizeof(packet_button);
						pb.type = C2S_BUTTONEVENT;
						pb.num = 1;
						pb.add = false;

						CServerManager::GetInstance()->SendData(C2S_BUTTONEVENT, &pb);
					}
					else
					{
						packet_coin pc;
						pc.id = Player->Get_ClientIndex();
						pc.size = sizeof(packet_coin);
						pc.type = C2S_COINEVENT;
						pc.num = 6;
						pc.add = false;

						CServerManager::GetInstance()->SendData(C2S_COINEVENT, &pc);
					}
				}

				m_bScared = true;
				m_iArriveEvent = EVENT_END;
				m_fEventTime = 0.f;
				pGhost->m_pTransformCom->SetPosition(0.f, 0.f, 0.f);
			}
			break;
		}
		case EVENT_SWAP:
		{
			break;
		}
		case EVENT_END:
		{
			m_iArriveEvent = EVENT_NO;
			if (m_bEventIsPlayer)
			{
				TurnEnd(TimeDelta);
				m_bEventIsPlayer = false;
			}
			break;
		}
		default:
			m_iArriveEvent = EVENT_NO;
			break;
		}
		return 0;
	}
	else
	{
		m_fEventMonsterY = 10.f;
	}

	m_dMSTime += TimeDelta;
	if (m_dMSTime > 0.6f)
	{
		m_dMSTime = 0.f;
		if (SERVERCONNECT == true) {
			XMFLOAT3 xmPlayerPostion = XMFLOAT3{ 0.f,0.f,0.f };
			XMFLOAT3 shift = Vector3::Add(CServerManager::GetInstance()->Get_CurPlayer()->GetRightVector(), XMFLOAT3(0.f, 2.7f, 0.f));
			XMFLOAT3 postion = XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3f, xmPlayerPostion.z);

			if (pBilStore->GetIndexX() != -1) {
				pBilStore->SetPostion(postion);
				pBilStore->GiveIndexX(1);
				if (pBilStore->GetIndexX() > 2)
					pBilStore->SetIndexX(0);
			}

			if (pBilMap->GetIndexX() != -1) {
				pBilMap->SetPostion(postion);
				pBilMap->GiveIndexX(1);
				if (pBilMap->GetIndexX() > 2)
					pBilMap->SetIndexX(0);
			}
		}
	}

	if (m_bArriveStore == true)
	{
		XMFLOAT3 playerPos = Player->m_tPlayerInfo.xmPosition;

		pCameraThird->SetLookAt(XMFLOAT3(playerPos.x - 3.f, playerPos.y + 2.f, playerPos.z));
		pCameraThird->SetOffset(XMFLOAT3(-3.f, 1.f, 5.0f));

		POINT ptCursorPos;

		GetCursorPos(&ptCursorPos);
		::ScreenToClient(g_hWnd, &ptCursorPos);

		if (m_bShowCoinChatBox == true)
		{
			m_fStoreTimer += TimeDelta;

			if (m_fStoreTimer > 1.f)
			{
				m_fStoreTimer = 0.f; 

				m_bShowCoinChatBox = false;
				m_pChatBoxCoin->SetHide(true);

				m_pBuyButton->SetHide(false);
				m_pXButton->SetHide(false);

				m_pBuyButton->SetTexIndex(0);
				m_pXButton->SetTexIndex(0);

				for (int i = 0; i < 4; ++i)
					m_pItems[i]->SetHide(false);

				m_pItems[0]->SetTexIndex(1);

				m_pPrice->SetHide(false);
				wstring str = to_wstring(m_nItemPrice[0]) + L" Coin";
				m_pPrice->SetText(str, XMFLOAT2(0.78f, 0.5f), XMFLOAT2(0.75f, 0.75f));
			}
		}


		if (pKeyMgr->KeyDown(KEY_LBUTTON))
		{
			if (m_bPushBuyButton == false)
			{
				for (int i = 0; i < 4; ++i)
				{
					if (ptCursorPos.x >= 640 + (140 * i) && ptCursorPos.x <= 765 + (140 * i) && ptCursorPos.y >= 210 && ptCursorPos.y <= 330)
					{
						CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
						m_pItems[i]->SetTexIndex(1);
						m_nSelectItem = i;
						wstring str = to_wstring(m_nItemPrice[i]) + L" Coin";
						m_pPrice->SetText(str, XMFLOAT2(0.78f, 0.5f), XMFLOAT2(0.75f, 0.75f));
					}
					else
					{
						m_pItems[i]->SetTexIndex(0);
					}
				}

				if (ptCursorPos.x >= 932 && ptCursorPos.x <= 985 && ptCursorPos.y >= 352 && ptCursorPos.y <= 404) // 구매함
				{
					CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
					m_pBuyButton->SetTexIndex(1);
					m_pXButton->SetTexIndex(0);

					if (Player->bCoin >= m_nItemPrice[m_nSelectItem])
					{

						m_pChatBoxPurchase->SetHide(false); // 구매하시겠습니까?
						m_pBuyButtonforChat->SetHide(false);
						m_pXButtonforChat->SetHide(false);

						m_pBuyButtonforChat->SetTexIndex(0);
						m_pXButtonforChat->SetTexIndex(0);

						m_pBuyButton->SetHide(true);
						m_pXButton->SetHide(true);

						for (int i = 0; i < 4; ++i)
							m_pItems[i]->SetHide(true);

						m_pPrice->SetHide(true);

						m_bPushBuyButton = true;
					}
					else
					{
						m_pChatBoxCoin->SetHide(false);

						m_pBuyButton->SetHide(true);
						m_pXButton->SetHide(true);

						for (int i = 0; i < 4; ++i)
							m_pItems[i]->SetHide(true);

						m_pPrice->SetHide(true);

						m_bShowCoinChatBox = true;

						//m_pItems[m_nSelectItem]->SetTexIndex(1);

					}
				}
				else if (ptCursorPos.x >= 1125 && ptCursorPos.x <= 1178 && ptCursorPos.y >= 352 && ptCursorPos.y <= 404) // 구매하지 않음
				{
					CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
					m_pBuyButton->SetTexIndex(0);
					m_pXButton->SetTexIndex(1);

					m_bArriveStore = false;

						packet_eventselect pe;
					pe.type = C2S_EVENTSELECT;
					pe.size = sizeof(packet_eventselect);
					pe.id = Player->Get_ClientIndex();
					pe.buy = false;
					pe.coin = 0;
					pe.button = 0;
					pe.item = m_nSelectItem;

					CServerManager::GetInstance()->SendData(C2S_EVENTSELECT, &pe);
				}
			}
			else
			{
				if (ptCursorPos.x >= 560 && ptCursorPos.x <= 616 && ptCursorPos.y >= 385 && ptCursorPos.y <= 434) // 구매
				{
					CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
					CSoundManager::GetInstance()->Play_Sound(L"Buy1");
					Player->bCoin -= m_nItemPrice[m_nSelectItem];
					if (m_nSelectItem == ITEM::ITEM_BUTTON)
					{
						Player->bButton += 1;
					}

					m_pBuyButtonforChat->SetTexIndex(1);
					m_pXButtonforChat->SetTexIndex(0);
					m_pChatBoxPurchase->SetHide(true);
					m_bArriveStore = false;
					m_bPurchase = true;
					m_bPushBuyButton = false;

					SetBuyItem(m_nSelectItem);

					packet_eventselect pe;
					pe.type = C2S_EVENTSELECT;
					pe.size = sizeof(packet_eventselect);
					pe.id = Player->Get_ClientIndex();
					pe.buy = true;
					pe.coin = m_nItemPrice[m_nSelectItem];
					pe.button = 0;
					pe.item = m_nSelectItem;

					CServerManager::GetInstance()->SendData(C2S_EVENTSELECT, &pe);
				}
				else if (ptCursorPos.x >= 663 && ptCursorPos.x <= 718 && ptCursorPos.y >= 385 && ptCursorPos.y <= 434) // 구매안해
				{
					CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
					m_pBuyButton->SetTexIndex(0);
					m_pXButton->SetTexIndex(0);
					m_pChatBoxPurchase->SetHide(true);
					m_bPushBuyButton = false;

					m_pBuyButtonforChat->SetHide(true);
					m_pXButtonforChat->SetHide(true);

					m_pBuyButton->SetHide(false);
					m_pXButton->SetHide(false);

					for (int i = 0; i < 4; ++i)
						m_pItems[i]->SetHide(false);

					m_pPrice->SetHide(false);
					wstring str = to_wstring(m_nItemPrice[m_nSelectItem]) + L" Coin";
					m_pPrice->SetText(str, XMFLOAT2(0.78f, 0.5f), XMFLOAT2(0.75f, 0.75f));

					m_pItems[m_nSelectItem]->SetTexIndex(1);

				}
			}

		}

		
		if (m_bArriveStore == false)
		{
			m_pBuyButton->SetTexIndex(0);
			m_pXButton->SetTexIndex(0);

			m_pBuyButton->SetHide(true);
			m_pXButton->SetHide(true);

			m_pBuyButtonforChat->SetHide(true);
			m_pXButtonforChat->SetHide(true);

			m_pBuyButtonforChat->SetTexIndex(0);
			m_pXButtonforChat->SetTexIndex(0);

			for (int i = 0; i < 4; ++i)
			{
				m_pItems[i]->SetHide(true);
				m_pItems[i]->SetTexIndex(0);
			}

			m_pItems[0]->SetTexIndex(1);

			m_pPrice->SetHide(true);
			wstring str = to_wstring(m_nItemPrice[0]) + L" Coin";
			m_pPrice->SetText(str, XMFLOAT2(0.78f, 0.5f), XMFLOAT2(0.75f, 0.75f));
		}


		return 0;
	}

	XMFLOAT3 xmPlayerPostion = XMFLOAT3{ 0.f,0.f,0.f };

	short return1 = ShowBuyItemMotion(TimeDelta, m_nSelectItem);

	if (return1 == 0)
		return 0;	

	//이름
	if (SERVERCONNECT) {
		if (CServerManager::GetInstance()->IsAbleNickNameInit())
		{
			for (int i = 0; i < 3; ++i) {
				wstring w = L"";
				string name = Players[i]->Get_Name();

				w.assign(name.begin(), name.end());
				pNameText[i]->SetTextString(w);
			}
			if (Player != nullptr) {
				if (Player->GetPlayerOrder() != 0)
					m_bOrderView = false;
				else
					m_bOrderView = true;
			}
			CServerManager::GetInstance()->SetAbleNickNameInit(false);
		}
	}

	ItemUsed();

	if (SERVERCONNECT) {

		moveDelta += TimeDelta;

		if (moveDelta > MOVEPACKETDELTA / 2.f && m_bIsMove)
		{
			Send_MovePacket(TimeDelta);
		}

		if (CServerManager::GetInstance()->Get_PlayerNum() != 3)
			return 0;
		else
		{
			if (m_iTurn == -1)
			{
				m_iTurn = 0;
				HidePlayerUI(false);
				pBlack->SetHide(false);
				if (m_iTurn >= 8)
					pLastTurn->SetHide(false);
				for (int i = 0; i < 3; ++i)
				{
					Rankers.push_back(Players[i]);
					pPlayerIcon[i]->SetTexIndex(Players[i]->GetSkinid());
					Players[i]->SetSkinid(Players[i]->GetSkinid());
					if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder()) {
						wstring w = L"";
						string name = Players[i]->Get_Name();

						w.assign(name.begin(), name.end());
						w += L" turn";

						pTurnText->SetTextString(w);
					}
					Players[1]->SetSmokeObject(pSmokeObject2);
					Players[2]->SetSmokeObject(pSmokeObject3);

					Players[i]->setPlayerIndex(i);
				}
				if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
				{
					pDiceUI->SetHide(false);
					pWhiteUI->SetHide(false);
					pKeyText->SetTextString(L"Enter");
					pKeyWhiteUI->SetHide(false);
				}
			}
		}
	}


	if (pKeyMgr->KeyDown(KEY_1))
	{
		CManagement::GetInstance()->Clear_Scene(m_SceneChange);
		m_pLoading = CLoading::Create(SCENE_FALLINGMAP);
		m_SceneChange = SCENE_FALLINGMAP;
	}

	if (pKeyMgr->KeyDown(KEY_2))
	{
		CManagement::GetInstance()->Clear_Scene(m_SceneChange);
		m_pLoading = CLoading::Create(SCENE_GGOGGOMAP);
		m_SceneChange = SCENE_GGOGGOMAP;
	}

	if (pKeyMgr->KeyDown(KEY_3))
	{
		CManagement::GetInstance()->Clear_Scene(m_SceneChange);
		m_pLoading = CLoading::Create(SCENE_BULLFIGHTMAP);
		m_SceneChange = SCENE_BULLFIGHTMAP;
	}

	if (pKeyMgr->KeyDown(KEY_4))
	{
		CManagement::GetInstance()->Clear_Scene(m_SceneChange);
		m_pLoading = CLoading::Create(SCENE_ONEMINDMAP);
		m_SceneChange = SCENE_ONEMINDMAP;
	}

	if (pKeyMgr->KeyDown(KEY_5))
	{
		CManagement::GetInstance()->Clear_Scene(m_SceneChange);
		m_pLoading = CLoading::Create(SCENE_PRIZE);
		m_SceneChange = SCENE_PRIZE;
	}

	if (m_bMapCamera)
	{
		XMFLOAT3 xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
		if (pKeyMgr->KeyPressing(KEY_W))
		{
			xmf3Shift = Vector3::Add(xmf3Shift, XMFLOAT3(0.f, 0.f, 1.f), PLAYERSPEED * TimeDelta);
		}
		if (pKeyMgr->KeyPressing(KEY_S))
		{
			xmf3Shift = Vector3::Add(xmf3Shift, XMFLOAT3(0.f, 0.f, 1.f), -PLAYERSPEED * TimeDelta);
		}
		if (pKeyMgr->KeyPressing(KEY_A))
		{
			xmf3Shift = Vector3::Add(xmf3Shift, XMFLOAT3(1.f, 0.f, 0.f), -PLAYERSPEED * TimeDelta);
		}
		if (pKeyMgr->KeyPressing(KEY_D))
		{
			xmf3Shift = Vector3::Add(xmf3Shift, XMFLOAT3(1.f, 0.f, 0.f), PLAYERSPEED * TimeDelta);
		}

		m_xmMapCameraPos = Vector3::Add(m_xmMapCameraPos, xmf3Shift);
		pCameraFixed->settingCamera(XMFLOAT3(m_xmMapCameraPos.x, m_xmMapCameraPos.y + 50, m_xmMapCameraPos.z), m_xmMapCameraPos, XMFLOAT3(0.f, 0.f, 1.f));
	}

	if (SERVERCONNECT)
	{
		xmPlayerPostion = CServerManager::GetInstance()->Get_CurPlayer()->GetPosition();

		if (CServerManager::GetInstance()->Get_DiceEnter())
		{
			if (m_bMiniDiceSet == false)
				pDice->m_pTransformCom->SetPosition(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
			else
				m_bMiniDiceSet = false;
			
			CServerManager::GetInstance()->Set_DiceEnter(false);
			m_bDiceOn = true;
			CSoundManager::GetInstance()->Play_Sound(L"DiceRotate", 1.0f, 0, true);

			pCameraThird->SetOffset(XMFLOAT3(-4.0f, 2.2f, 10.0f));

			pBlack->SetHide(true);
			if (m_iTurn >= 8)
				pLastTurn->SetHide(true);
			pTurnText->SetTextString(L"");
		}
	}
	else
		xmPlayerPostion = Player->GetPosition();

	if (pKeyMgr->KeyDown(KEY_ENTER) && !m_bDiceOn && !m_bMoveTeddyBear && m_iDiceNumber == 0)
	{
 		pBlack->SetHide(true);
		if (m_iTurn >= 8)
			pLastTurn->SetHide(true);
		pTurnText->SetTextString(L"");
		m_nUsingItem = -1;

		if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder() || !SERVERCONNECT)
		{
			m_bShowMyInven = true;
			m_pArrowHead->SetRender(false);
		}
		pCameraThird->SetOffset(XMFLOAT3(-4.0f, 2.2f, 10.0f));
	}

	if (m_bShowMyInven == true)
	{
		short result3 = ShowMyInven(TimeDelta);

		if (result3 == 0)
			return 0;
	}

	if (m_bMyInvenEnd == true)
	{
		m_bMyInvenEnd = false;

		if (SERVERCONNECT) {
			if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder() && m_bOrderView) {

				packet_diceenter p;
				p.id = Player->Get_ClientIndex();
				p.size = sizeof(p);
				p.type = C2S_DICEENTER;
				CServerManager::GetInstance()->SendData(C2S_DICEENTER, &p);

				m_bOrderView = false;
				m_bDiceOn = true;
				CSoundManager::GetInstance()->Play_Sound(L"DiceRotate", 1.0f, 0, true);

				pDice->m_pTransformCom->SetPosition(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));

				pDiceUI->SetHide(true);

				pJumpUI->SetHide(false);
				pKeyText->SetTextString(L"Space");
			}
			else
			{
				MiniDiceSet();
			}
		}
		else
		{
			//Player->m_AnimState = ANIM_JUMP;
			m_bDiceOn = true;
			CSoundManager::GetInstance()->Play_Sound(L"DiceRotate", 1.0f, 0, true);
			pDice->m_pTransformCom->SetPosition(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));

			MiniDiceSet();

			pDiceUI->SetHide(true);

			pJumpUI->SetHide(false);
			pKeyText->SetTextString(L"Space");
		}
	}

	if (m_bDiceOn)
	{
		XMFLOAT3 xmPlayerPos = xmPlayerPostion;
		xmPlayerPos.y += 3.2f;
		pCameraThird->SetLookAt(xmPlayerPos);
	}

	if (SERVERCONNECT)
	{
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->m_AnimState == ANIM_RUN)
			{
				pCameraThird->SetOffset(XMFLOAT3(-6.0f, 7.2f, 10.0f));
			}
		}
	}

	if (CKeyManager::GetInstance()->KeyDown(KEY_SPACE) && m_bMoveTeddyBear == false && m_iDiceNumber == 0) // 주사위를 굴리는 행위
	{
		if (m_bDiceOn)
		{
			Player->m_AnimState = ANIM_JUMP;
			Player->SetState(OBJECT_STATE::OBJSTATE_FLY);
			if (SERVERCONNECT) {
				packet_jumpdice p;
				p.size = sizeof(p);
				p.id = Player->Get_ClientIndex();
				p.type = C2S_JUMPDICE;
				p.dicenum = -1;

				CServerManager::GetInstance()->SendData(C2S_JUMPDICE, &p);
			}

			pJumpUI->SetHide(true);
			pWhiteUI->SetHide(true);
			pKeyText->SetTextString(L"");
			pKeyWhiteUI->SetHide(true);
			m_bUsingItemMotion = false;
		}
	}


	if (m_bUsingItemMotion == true)
	{
		ShowUsingItemMotion(TimeDelta);
		return 0;
	}

	if (m_bShowItemDiceNum == true)
	{
		ShowItemDiceNumber(TimeDelta);
		return 0;
	}
	else
	{
		pBillBoardObject[0]->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
		m_pItemDiceBill->SetPostion(XMFLOAT3(1000, 1000, 1000));
		m_pPlusBill->SetPostion(XMFLOAT3(1000, 1000, 1000));
	}

	if (m_bMoveTeddyBear == true)
	{
		m_pArrowHead->SetRender(true);
		MoveToPlatform(TimeDelta);
		pCameraThird->SetOffset(XMFLOAT3(-6.0f, 7.2f, 10.0f));
	}
	else if (m_bMoveTeddyBear == false && m_iDiceNumber != 0 && (!SERVERCONNECT || Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())) // 두 갈래 길 선택
	{
		pCameraThird->SetOffset(XMFLOAT3(0.0f, 7.2f, -12.0f));

		if (GetKeyState(VK_LEFT) & 0x8000 && (m_iNextPlatformDir == 1 || m_iNextPlatformDir2 == 1))
		{
			if (m_bMapCamera == false)
			{
				m_bSelectPlatform = true;
				m_iSelectPlatformDir = 1;
				m_bMoveTeddyBear = true;
			}
		}
		else if (GetKeyState(VK_RIGHT) & 0x8000 && (m_iNextPlatformDir == 2 || m_iNextPlatformDir2 == 2))
		{
			if (m_bMapCamera == false)
			{
				m_bSelectPlatform = true;
				m_iSelectPlatformDir = 2;
				m_bMoveTeddyBear = true;
			}
		}
		else if (GetKeyState(VK_UP) & 0x8000 && (m_iNextPlatformDir == 0 || m_iNextPlatformDir2 == 0))
		{
			if (m_bMapCamera == false)
			{
				m_bSelectPlatform = true;
				m_iSelectPlatformDir = 0;
				m_bMoveTeddyBear = true;
			}
		}
		else if (CKeyManager::GetInstance()->KeyDown(KEY_M))
		{
			if (m_bMapCamera == false)
			{
				m_bMapCamera = true;
				CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
				pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));
				m_xmMapCameraPos = Player->GetPosition();
				pCameraFixed->settingCamera(XMFLOAT3(m_xmMapCameraPos.x, m_xmMapCameraPos.y + 50, m_xmMapCameraPos.z), m_xmMapCameraPos, XMFLOAT3(0.f, 0.f, 1.f));

				packet_showmap ps;
				ps.size = sizeof(packet_showmap);
				ps.type = C2S_SHOWMAP;
				ps.show = true;
				CServerManager::GetInstance()->SendData(C2S_SHOWMAP, &ps);
			}
			else
			{
				m_bMapCamera = false;
				CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");
				CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
				dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);
				pCameraThird->SetOffset(XMFLOAT3(0.0f, 7.2f, -11.0f));

				packet_showmap ps;
				ps.size = sizeof(packet_showmap);
				ps.type = C2S_SHOWMAP;
				ps.show = false;
				CServerManager::GetInstance()->SendData(C2S_SHOWMAP, &ps);
			}
		}
	}
	else
	{
		if (SERVERCONNECT) {
			if (CServerManager::GetInstance()->Get_CurPlayer()->Get_TeddyBear()->mClipName == "jump")
			{
				m_bJumped = true;
				if (m_bDiceOn == true)
				{
					if (CServerManager::GetInstance()->Get_CurPlayer()->IsPlayerAnimMiddle())
					{
						pDice->m_pTransformCom->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));						
						pBillBoardObject[1]->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
						pBillBoardObject[1]->SetIndexX(0);
						pBillBoardObject[1]->SetIndexY(0);

						if (m_nUsingItem == 0)
						{
							m_pUsingMiniDice->m_pTransformCom->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));
							pBillBoardObject[1]->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
						}
						m_bDiceOn = false;
						CSoundManager::GetInstance()->Stop_Sound(L"DiceRotate");
						CSoundManager::GetInstance()->Play_Sound(L"DiceBomb");
					}
				}
			}
			else if (CServerManager::GetInstance()->Get_CurPlayer()->m_AnimState == ANIM_IDLE)
			{
				if (m_bJumped == true)
				{
					if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_PrevOrder()) {
					
						if (m_nUsingItem != -1)
						{
							m_bShowItemDiceNum = true;
							XMFLOAT3 xmCameraRight = CCameraManager::GetInstance()->GetCurrentCamera()->get_CameraInfo().xmRight;
							XMFLOAT3 PlayerPos = Player->m_tPlayerInfo.xmPosition;
							XMFLOAT3 Pos = XMFLOAT3(PlayerPos.x, PlayerPos.y + 3.3, PlayerPos.z);
							XMFLOAT3 right = Vector3::Normalize(xmCameraRight);
							XMFLOAT3 dicePos = Vector3::Add(Pos, right, 0.7f);
							XMFLOAT3 miniDicePos = Vector3::Subtract(Pos, right, 0.7f);

							m_iDiceNumber = CServerManager::GetInstance()->Get_DiceNum();

							pBillBoardObject[0]->SetPostion(dicePos);
							m_pItemDiceBill->SetPostion(miniDicePos);
							pBillBoardObject[0]->SetIndexY(m_iDiceNumber - 1);
							m_pItemDiceBill->SetIndexY(m_nItemDiceNum - 1);

							m_pPlusBill->SetPostion(Pos);
						}
						else
						{
							m_iDiceNumber = CServerManager::GetInstance()->Get_DiceNum();
							m_bMoveTeddyBear = true;
						}
						pBillBoardObject[1]->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
					}
					else
					{
						if (m_nUsingItem != -1)
						{
							m_bShowItemDiceNum = true;

							XMFLOAT3 PlayerPos = CServerManager::GetInstance()->Get_CurPlayer()->m_tPlayerInfo.xmPosition;
							XMFLOAT3 xmCameraRight = CCameraManager::GetInstance()->GetCurrentCamera()->get_CameraInfo().xmRight;
							XMFLOAT3 Pos = XMFLOAT3(PlayerPos.x, PlayerPos.y + 3.3, PlayerPos.z);
							XMFLOAT3 right = Vector3::Normalize(xmCameraRight);
							XMFLOAT3 dicePos = Vector3::Add(Pos, right, 0.7f);
							XMFLOAT3 miniDicePos = Vector3::Subtract(Pos, right, 0.7f);

							m_iDiceNumber = CServerManager::GetInstance()->Get_DiceNum();

							pBillBoardObject[0]->SetPostion(dicePos);
							m_pItemDiceBill->SetPostion(miniDicePos);
							pBillBoardObject[0]->SetIndexY(m_iDiceNumber - 1);
							m_pItemDiceBill->SetIndexY(m_nItemDiceNum - 1);

							m_pPlusBill->SetPostion(Pos);
						}
						else
						{
							m_iDiceNumber = CServerManager::GetInstance()->Get_DiceNum();

						}
						pBillBoardObject[1]->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
						m_bOtherDicenum = true;
					}

					m_bJumped = false;
				}
			}
		}
		else
		{
			if (Player->Get_TeddyBear()->mClipName == "jump")
			{
				if (m_bDiceOn == true)
				{
					if (Player->IsPlayerAnimMiddle())
					{
						pDice->m_pTransformCom->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));
						pBillBoardObject[1]->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
						pBillBoardObject[1]->SetIndexX(0);
						pBillBoardObject[1]->SetIndexY(0);
						pConfetti->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
						pConfetti->SetIndexX(0);
						pConfetti->SetIndexY(3);

						if (m_nUsingItem == 0)
						{
							m_pUsingMiniDice->m_pTransformCom->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));
							pBillBoardObject[1]->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
							pConfetti->SetPostion(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
						}
						m_bDiceOn = false;
						CSoundManager::GetInstance()->Stop_Sound(L"DiceRotate");
						CSoundManager::GetInstance()->Play_Sound(L"DiceBomb");
					}
				}
				else
				{
					if (Player->IsPlayerAnimEnd())
					{
						m_iDiceNumber = rand() % 6 + 1;
						if (m_nUsingItem != -1)
						{
							m_bShowItemDiceNum = true;
							
							XMFLOAT3 PlayerPos = Player->m_tPlayerInfo.xmPosition;
							XMFLOAT3 Pos = XMFLOAT3(PlayerPos.x, PlayerPos.y + 3.3, PlayerPos.z);
							XMFLOAT3 xmCameraRight = CCameraManager::GetInstance()->GetCurrentCamera()->get_CameraInfo().xmRight;
							XMFLOAT3 right = Vector3::Normalize(xmCameraRight);
							XMFLOAT3 dicePos = Vector3::Add(Pos, right, 0.7f);
							XMFLOAT3 miniDicePos = Vector3::Subtract(Pos, right, 0.7f);
							pBillBoardObject[0]->SetPostion(dicePos);
							m_pItemDiceBill->SetPostion(miniDicePos);
							pBillBoardObject[0]->SetIndexY(m_iDiceNumber - 1);
							m_pItemDiceBill->SetIndexY(m_nItemDiceNum - 1);

							m_pPlusBill->SetPostion(Pos);
						}
						else
							m_bMoveTeddyBear = true;

						pBillBoardObject[1]->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
						pConfetti->SetPostion(XMFLOAT3(0.f, 0.f, 0.f));
					}
				}
			}
		}
	}


	for (int i = 0; i < 2; i++)
	{
		if (pBoundingBox[i] && pTeddyBear[i])
		{
			XMFLOAT3 xmPosition = pTeddyBear[i]->m_pTransformCom->GetPosition();
			pBoundingBox[i]->m_pTransformCom->SetPosition(xmPosition.x, xmPosition.y + 0.7f, xmPosition.z);
			//pBoundingBox->m_pTransformCom->SetPosition(pTeddyBear->m_xmBoundingBox.Center);
		}
	}
	if (m_bOtherDicenum) {
		if (!(m_iDiceNumber = CServerManager::GetInstance()->Get_DiceNum())) //숫자가 0이면 false(다움직임)
			m_bOtherDicenum = false;
	}
	
	if (pDice && m_pUsingMiniDice)
	{
		//pDice->m_pTransformCom->SetPosition(XMFLOAT3(xmPlayerPostion.x, xmPlayerPostion.y + 3.3, xmPlayerPostion.z));
		if ((int)timetest % 2 == 0)
		{
			pDice->m_pTransformCom->Rotate(6.f, 6.f, 0.f);
			m_pUsingMiniDice->m_pTransformCom->Rotate(6.f, 6.f, 0.f);
		}
		else
		{
			pDice->m_pTransformCom->Rotate(0.f, 6.f, 6.f);
			m_pUsingMiniDice->m_pTransformCom->Rotate(0.f, 6.f, 6.f);
		}
	}
	
	pBillBoardObject[0]->SetIndexY(m_iDiceNumber - 1);
	timetest2 += TimeDelta;
	if (timetest2 > 0.05f)
	{
		timetest2 = 0;
		if (pBillBoardObject[1]->GetIndexY() != -1)
		{
			pBillBoardObject[1]->GiveIndexX(1);
			if (pBillBoardObject[1]->GetIndexX() > 3)
			{
				pBillBoardObject[1]->SetIndexX(0);
				pBillBoardObject[1]->GiveIndexY(1);
				if (pBillBoardObject[1]->GetIndexY() > 3)
				{
					pBillBoardObject[1]->SetIndexX(-1);
					pBillBoardObject[1]->SetIndexY(-1);
				}
			}
		}
	}

	if (pConfetti->GetIndexY() != -1)
	{
		pConfetti->GiveIndexX(1);
		if (pConfetti->GetIndexX() > 8)
		{
			pConfetti->SetIndexX(0);
			pConfetti->GiveIndexY(1);
			if (pConfetti->GetIndexY() > 8)
			{
				pConfetti->SetIndexX(-1);
				pConfetti->SetIndexY(-1);
			}
		}
	}

	// 플레이어 UI 관련
	if (!SERVERCONNECT) {
		pStarText[0]->SetTextString(to_wstring(Player->bCoin));
		pClothesButtonText[0]->SetTextString(to_wstring(Player->bButton));
		switch (Player->bRank)
		{
		case 1:
			pRankText[0]->SetTextString(L"1st");
			break;
		case  2:
			pRankText[0]->SetTextString(L"2nd");
			break;
		case 3:
			pRankText[0]->SetTextString(L"3rd");
			break;
		default:
			pRankText[0]->SetTextString(L"??");
			break;
		}
	}
	else
	{
		if (CServerManager::GetInstance()->IsCoinEvent() == 1) //코인 상승
		{
			CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);
			CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPostion.x, xmPlayerPostion.y + 7.f, xmPlayerPostion.z, -90.f, 0.f, 0.f);
			CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPostion.x, xmPlayerPostion.y + 8.5f, xmPlayerPostion.z, -90.f, 0.f, 0.f);
			CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPostion.x, xmPlayerPostion.y + 10.f, xmPlayerPostion.z, -90.f, 0.f, 0.f);
			CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

			CServerManager::GetInstance()->SetCoinEvent(0);
		}
		else if (CServerManager::GetInstance()->IsCoinEvent() == 2) //코인 하락
		{
			CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);
			CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPostion.x, xmPlayerPostion.y + 2.f, xmPlayerPostion.z, -90.f, 0.f, 0.f, 1);
			CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPostion.x, xmPlayerPostion.y + 1.f, xmPlayerPostion.z, -90.f, 0.f, 0.f, 1);
			CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPostion.x, xmPlayerPostion.y - 0.f, xmPlayerPostion.z, -90.f, 0.f, 0.f, 1);
			CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

			CServerManager::GetInstance()->SetCoinEvent(0);
		}

		for (int i = 0; i < 3; ++i)
		{
			if (!Players[i]) //nullptr
				continue;

			pStarText[i]->SetTextString(to_wstring(Players[i]->bCoin));
			pClothesButtonText[i]->SetTextString(to_wstring(Players[i]->bButton));

			switch (Players[i]->bRank)
			{
			case 1:
				pRankText[i]->SetTextString(L"1st");
				break;
			case  2:
				pRankText[i]->SetTextString(L"2nd");
				break;
			case 3:
				pRankText[i]->SetTextString(L"3rd");
				break;
			default:
				pRankText[i]->SetTextString(L"??");
				break;
			}
		}
	}


	xmPlayerPostion = Player->GetPosition();
	xmPlayerPostion.y += 2.8f;
	m_pArrowHead->m_pTransformCom->SetPosition(xmPlayerPostion);
	//pHerePlayer->SetPostion(xmPlayerPostion);

	CurrentShowMap = CServerManager::GetInstance()->Get_ShowMap();
	if (beforeShowMap != CurrentShowMap)
	{
		if (CurrentShowMap) //맵 키기
		{
			pBilMap->SetIndexX(0);
			//pBillBoardObject[0]->SetIndexX(-1);
		}
		else //맵끄기
		{
			pBilMap->SetIndexX(-1);
			//pBillBoardObject[0]->SetIndexX(0);
		}
		beforeShowMap = CurrentShowMap;
	}

	if (SERVERCONNECT) {
		Camera_Change(TimeDelta);
	}

	return 0;
}

short CScene_Boardmap::LateUpdate_Scene(double TimeDelta)
{

	return 0;
}

HRESULT CScene_Boardmap::Render_Scene()
{
	return NOERROR;
}

CScene_Boardmap* CScene_Boardmap::Create()
{
	CScene_Boardmap* pInstance = new CScene_Boardmap();

	if (pInstance->Initialize_Scene())
	{
		MSG_BOX("CScene_Boardmap Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_Boardmap::Release()
{
	Safe_Release(m_pPlatformManager);

	delete this;

	return NOERROR;
}

HRESULT CScene_Boardmap::BackTotheBoardmap(SCENEID sceneId)
{
	if (SERVERCONNECT)
	{
		m_bNoUpdate = true;
		for (int i = 0; i < 3; ++i)
		{
			Player->SetDeath(false);
			Players[i]->SetState(OBJECT_STATE::OBJSTATE_GROUND);
			Players[i]->SetAnimation("idle1");
		}

		CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
		pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));
		pCameraFixed->settingCamera(XMFLOAT3(30.f, 80.f, -140.f), XMFLOAT3(30.f, 13.f, -13.f), XMFLOAT3(0.f, 1.f, 0.f));

		HidePlayerUI(true);
		HideScoreBoadUI(false);
		SetScoreBoardUI();

		m_iTurn++;
		if (m_iTurn >= 8)
		{
			m_SceneChange = SCENE_PRIZE;
		}

		switch (m_SceneChange)
		{
		case SCENE_BOARDMAP:
			break;
		case SCENE_FALLINGMAP:
		{
			m_pLoading = CLoading::Create(SCENE_FALLINGMAP);
		}
		break;
		case SCENE_GGOGGOMAP:
		{
			m_pLoading = CLoading::Create(SCENE_GGOGGOMAP);
		}
		break;
		case SCENE_BULLFIGHTMAP:
		{
			m_pLoading = CLoading::Create(SCENE_BULLFIGHTMAP);
		}
		break;
		case SCENE_ONEMINDMAP:
		{
			m_pLoading = CLoading::Create(SCENE_ONEMINDMAP);
		}
		break;
		case SCENE_PRIZE:
		{
			m_pLoading = CLoading::Create(SCENE_PRIZE);
		}
		break;
		case SCENE_WAITINGROOM:
		{
			m_pLoading = CLoading::Create(SCENE_WAITINGROOM);
		}
		break;
		default:
			break;
		}
	}
	return NOERROR;
}

HRESULT CScene_Boardmap::Scene_Change(SCENEID Scene, CScene_Boardmap* pSceneBoardmap)
{
	CManagement* pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return -1;
	//m_pLoading = CLoading::Create(Scene);

	pManagement->Set_CurrentSceneId(Scene);

	switch (Scene)
	{
	case SCENE_BOARDMAP:
		break;
	case SCENE_FALLINGMAP:
	{
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_Fallingmap::Create(this))))
			return -1;
		break;
	}
	case SCENE_GGOGGOMAP:
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_Ggoggomap::Create(this))))
			return -1;
		break;
	case SCENE_BULLFIGHTMAP:
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_Bullfightmap::Create(this))))
			return -1;
		break;
	case SCENE_ONEMINDMAP:
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_OneMindMap::Create(this))))
			return -1;
		break;
	default:
		break;
	}

	XMFLOAT3 playerPos = m_pPlatformManager->Get_Platform(::Player->Get_CurrentPlatformIndex())->m_pTransformCom->GetPosition();

	//::Player->Set_BeforeLookVector(::Player->m_tPlayerInfo.xmLook);

	CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
	pCameraThird->SetOffset(XMFLOAT3(0.0f, 7.2f, -12.0f));

	/*if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
		return -1;*/

	if (Scene == SCENE_PRIZE)
	{
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_Prize::Create())))
			return -1;

		if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
			return -1;
	}
	else if (Scene == SCENE_WAITINGROOM)
	{
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
			return -1;

		if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
			return -1;
	}
}

short CScene_Boardmap::MoveToPlatform(double TimeDelta)
{
	if (SERVERCONNECT)
	{
		if (Player->GetPlayerOrder() != CServerManager::GetInstance()->Get_CurrentOrder())
			return 0;
	}

	int currentPlatformIndex = ::Player->Get_CurrentPlatformIndex(); // 현재 플랫폼 인덱스
	CPlatform* pCurrentPlatform = m_pPlatformManager->Get_Platform(currentPlatformIndex); // 현재 플랫폼을 가져옴

	int nextPlatformIndex, nextPlatformIndex2;

	if (pCurrentPlatform->Get_IsNextPlatformContinue() == false && pCurrentPlatform->Get_IsSinglePlatform() == true) // 다음 플랫폼이 현재 플랫폼+1번이 아닌 경우
	{
		nextPlatformIndex = pCurrentPlatform->Get_NextPlatformIndex();
		m_bSelectPlatform = false;
	}
	else if (pCurrentPlatform->Get_IsSinglePlatform() == false) // 다음 플랫폼이 한 개가 아닌 경우
	{
		if (m_bSelectPlatform == true) // 플랫폼을 선택했다면
		{
			if (m_iNextPlatformDir == m_iSelectPlatformDir)
				nextPlatformIndex = pCurrentPlatform->Get_NextPlatformIndex(); // 일단 현재 플랫폼+1번이 아닌 다른 길로 가게 함	
			else if (m_iNextPlatformDir2 == m_iSelectPlatformDir)
				nextPlatformIndex = pCurrentPlatform->Get_NextPlatformIndex2(); // 두번째 다른 길로 가게 함
			else
				nextPlatformIndex = currentPlatformIndex + 1;
			pRoadSignUI->SetHide(true);
			pMapUI->SetHide(true);
			pWhiteUI->SetHide(true);
			pWhiteUI2->SetHide(true);
			pKeyText->SetTextString(L"");
			pKeyText2->SetTextString(L"");
			pKeyWhiteUI->SetHide(true);
			pKeyWhiteUI2->SetHide(true);
		}
		else // 그렇지 않다면 일단 나감
		{
			dynamic_cast<CCamera_Third*>(CCameraManager::GetInstance()->Find_Camera("Camera_Third"))->SetOffset(XMFLOAT3(0.0f, 7.2f, -12.0f));

			m_iNextPlatformDir = m_iNextPlatformDir2 = pCurrentPlatform->Get_NextPlatformDir();

			if (pCurrentPlatform->Get_IsNextPlatformContinue() == false)
				m_iNextPlatformDir2 = pCurrentPlatform->Get_NextPlatformDir2();

			m_bMoveTeddyBear = false;
			::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
			::Player->SetPlayerAcc(0.f);
			Send_MovePacket(TimeDelta);

			packet_dicemoveend p;
			p.id = Player->Get_ClientIndex();
			p.size = sizeof(p);
			p.type = C2S_DICEMOVEEND;
			CServerManager::GetInstance()->SendData(C2S_DICEMOVEEND, &p);

			pRoadSignUI->SetHide(false);
			pWhiteUI->SetHide(false);
			pKeyText->SetTextString(L"Arrow");
			pKeyWhiteUI->SetHide(false);

			pMapUI->SetHide(false);
			pWhiteUI2->SetHide(false);
			pKeyText2->SetTextString(L"M");
			pKeyWhiteUI2->SetHide(false);
			return 0;
		}

	}
	else // 다음 플랫폼이 한개이고 현재 플랫폼+1번인 경우
	{
		nextPlatformIndex = currentPlatformIndex + 1;
		m_bSelectPlatform = false;
	}

	CPlatform* pNextPlatform = m_pPlatformManager->Get_Platform(nextPlatformIndex); // next 플랫폼을 가져옴

	XMFLOAT3 currentPlatformPos, nextPlatformPos, direction;

	currentPlatformPos = pCurrentPlatform->m_pTransformCom->GetPosition();
	nextPlatformPos = pNextPlatform->m_pTransformCom->GetPosition();
	nextPlatformPos.y = 0.f;

	XMFLOAT3 xmLength, xmPlayerPos;
	xmPlayerPos = ::Player->m_tPlayerInfo.xmPosition;
	xmPlayerPos.y = 0.f;

	// 넥스트 플랫폼으로 향하는 방향벡터 구하기
	XMStoreFloat3(&direction, XMLoadFloat3(&nextPlatformPos) - XMLoadFloat3(&xmPlayerPos));

	direction = Vector3::Normalize(direction);
	direction.y = 0.f;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
	::Player->GivePlayerAcc(1.f);
	float acc = ::Player->GetPlayerAcc();
	xmf3Shift = Vector3::Add(xmf3Shift, direction, acc * TimeDelta);
	::Player->xmf3Shift = xmf3Shift;
	//IntersectObjectTest(::Player, pTeddyBear[1], TimeDelta);
	m_bIsMove = true;

	XMFLOAT3 xmPlayerLook = Vector3::Normalize(::Player->m_tPlayerInfo.xmLook);

	XMFLOAT3 xmf3dot;
	XMStoreFloat3(&xmf3dot, XMVector3AngleBetweenVectors(XMLoadFloat3(&xmPlayerLook), XMLoadFloat3(&direction)));
	float tmpangle = XMConvertToDegrees(xmf3dot.y);

	XMFLOAT3 xmf3Cross = Vector3::CrossProduct(xmPlayerLook, direction);
	float fDot = Vector3::DotProduct(xmf3Cross, ::Player->m_tPlayerInfo.xmUp);

	if (tmpangle >= 2.f)
	{
		if (fDot > 0) // 왼
			::Player->Rotate(0.f, 100.f * TimeDelta, 0.f);
		else if (fDot < 0) // 오
			::Player->Rotate(0.f, -100.f * TimeDelta, 0.f);
	}
	else
		::Player->m_tPlayerInfo.xmLook = direction;

	if (SERVERCONNECT)
	{
		for (int i = 0; i < 3; ++i) {
			if (Player->Get_ClientIndex() == Players[i]->Get_ClientIndex())
				continue;
			XMFLOAT3 shift;
			if (IntersectObject(Player, Players[i], TimeDelta, shift))
			{
				//충돌
				packet_playercollision p;
				p.size = sizeof(packet_playercollision);
				p.id = Players[i]->Get_ClientIndex();
				p.type = C2S_PLAYERSCOL;
				p.shiftx = shift.x;
				p.shifty = shift.y;
				p.shiftz = shift.z;
				CServerManager::GetInstance()->SendData(C2S_PLAYERSCOL, &p);
			}
		}
	}

	XMFLOAT3 xmTemp1 = nextPlatformPos;
	XMFLOAT3 xmTemp2 = xmPlayerPos;
	xmTemp1.y = 0.f;
	xmTemp2.y = 0.f;

	XMStoreFloat3(&xmLength, XMLoadFloat3(&xmTemp1) - XMLoadFloat3(&xmTemp2));
	float fLength = Vector3::Length(xmLength);

	if (fLength < 0.2f)
	{
		::Player->Set_CurrentPlatformIndex(nextPlatformIndex);
		--m_iDiceNumber;
		m_fLength = 10000.f;

		if (nextPlatformIndex == m_buttonIndex[m_nButtonRound])
		{
			m_pChatBoxPurchase->SetHide(false);

			m_pBuyButtonforChat->SetHide(false);
			m_pXButtonforChat->SetHide(false);

			m_pBuyButtonforChat->SetTexIndex(0);
			m_pXButtonforChat->SetTexIndex(0);

			m_isBuyButton = true;
			m_bMoveTeddyBear = false;
			::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
			::Player->m_tPlayerInfo.xmPosition.x = nextPlatformPos.x;
			::Player->m_tPlayerInfo.xmPosition.z = nextPlatformPos.z;

			m_iDiceNumber++;

			packet_event p;
			p.size = sizeof(packet_event);
			p.type = C2S_EVENT;
			p.id = Player->Get_ClientIndex();
			p.E_event = E_BUTTON;

			CServerManager::GetInstance()->SendData(C2S_EVENT, &p);
		}

		//주사위 숫자 줄어드는 패킷 전송
		packet_dicenumdown p;
		p.id = Player->Get_ClientIndex();
		p.size = sizeof(packet_dicenumdown);
		p.type = C2S_DICENUMDOWN;
		p.num = m_iDiceNumber;

		CServerManager::GetInstance()->SendData(C2S_DICENUMDOWN, &p);
	}
	else
	{
		m_fLength = fLength;
	}



	if (m_iDiceNumber == 0)
	{
		for (int i = 0; i < 8; i++)
		{
			if (nextPlatformIndex == m_storeIndex[i]) // 상점 발판 왔을 때
			{
				m_bArriveStore = true;

				m_bMoveTeddyBear = false;
				::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

				m_pBuyButton->SetHide(false);
				m_pXButton->SetHide(false);

				for (int i = 0; i < 4; ++i)
					m_pItems[i]->SetHide(false);

				m_pPrice->SetHide(false);

				packet_event p;
				p.size = sizeof(packet_event);
				p.type = C2S_EVENT;
				p.id = Player->Get_ClientIndex();
				p.E_event = E_SHOP;

				CServerManager::GetInstance()->SendData(C2S_EVENT, &p);

				break;
			}
		}

		for (int i = 0; i < 8; ++i)
		{
			if (nextPlatformIndex == m_eventIndex[i]) // 이벤트 발판 왔을 때
			{
				//m_iArriveEvent = EVENT_LOST;
				m_iArriveEvent = rand() % 2 + 1;
				m_bEventIsPlayer = true;
				m_bMoveTeddyBear = false;
				::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

				if(m_iArriveEvent == 1)
					CSoundManager::GetInstance()->Play_Sound(L"bird");

				packet_specialevent pt;
				pt.size = sizeof(packet_specialevent);
				pt.type = C2S_SPECIALEVENT;
				pt.id = Player->Get_ClientIndex();
				pt.spacialevent = m_iArriveEvent;

				CServerManager::GetInstance()->SendData(C2S_SPECIALEVENT, &pt);
			}
		}

		int nCurrentIndex = ::Player->Get_CurrentPlatformIndex();
		CPlatform* pPlatform = m_pPlatformManager->Get_Platform(nCurrentIndex);
		if (nextPlatformIndex == m_buttonIndex[m_nButtonRound])
		{

		}
		else if (pPlatform->Get_PlatformColor() == CPlatform::platform_blue)	//-9, 16.6, -80.5
		{
			if (!SERVERCONNECT) {
				CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);
				CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPos.x, xmPlayerPos.y + 7.f, xmPlayerPos.z, -90.f, 0.f, 0.f);
				CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPos.x, xmPlayerPos.y + 8.5f, xmPlayerPos.z, -90.f, 0.f, 0.f);
				CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPos.x, xmPlayerPos.y + 10.f, xmPlayerPos.z, -90.f, 0.f, 0.f);
				CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);
			}

			packet_coin p;
			p.type = C2S_COINEVENT;
			p.size = sizeof(p);
			p.id = CServerManager::GetInstance()->Get_ClientIndex();
			p.add = true;
			p.num = 3;
			CServerManager::GetInstance()->SendData(C2S_COINEVENT, &p);
		}
		else if (pPlatform->Get_PlatformColor() == CPlatform::platform_red)
		{
			if (!SERVERCONNECT) {
				CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);
				CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPos.x, xmPlayerPos.y + 2.f, xmPlayerPos.z, -90.f, 0.f, 0.f, 1);
				CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPos.x, xmPlayerPos.y + 1.f, xmPlayerPos.z, -90.f, 0.f, 0.f, 1);
				CreateObject(L"Layer_BackGround", L"GameObject_CoinEffect", xmPlayerPos.x, xmPlayerPos.y - 0.f, xmPlayerPos.z, -90.f, 0.f, 0.f, 1);
				CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);
			}

			Player->SetAnimation("idlesad");
			Player->SetState(OBJECT_STATE::OBJSTATE_EMO);

			packet_coin p;
			p.type = C2S_COINEVENT;
			p.size = sizeof(p);
			p.id = CServerManager::GetInstance()->Get_ClientIndex();
			p.add = false;
			p.num = 3;
			CServerManager::GetInstance()->SendData(C2S_COINEVENT, &p);
		}

		m_bMoveTeddyBear = false;
		::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
		::Player->SetPlayerAcc(0.f);
		m_fLength = 10000.f;

		if (SERVERCONNECT) {
			if (m_iArriveEvent == EVENT_NO) // 현재 진행중인 이벤트가 없다면
			{
				TurnEnd(TimeDelta); // 턴을 넘겨라
			}
		}
		else
		{
			pBlack->SetHide(false);
			if(m_iTurn >= 8)
				pLastTurn->SetHide(false);
			pTurnText->SetTextString(L"Player Turn");
			pDiceUI->SetHide(false);
			pWhiteUI->SetHide(false);
			pKeyText->SetTextString(L"Enter");
			pKeyWhiteUI->SetHide(false);
		}
	}

	return 0;
}

void CScene_Boardmap::Send_ReadyState()
{
	packet_boardmap_ready p;
	p.id = CServerManager::GetInstance()->Get_ClientIndex();
	p.size = sizeof(p);
	p.type = C2S_BOARDMAPREADY;

	CServerManager::GetInstance()->SendData(C2S_BOARDMAPREADY, &p);
}

void CScene_Boardmap::Send_MovePacket(double TimeDelta)
{
	packet_move p;
	ZeroMemory(&p, sizeof(p));
	p.type = C2S_MOVE;
	p.size = sizeof(packet_move);
	p.id = Player->Get_ClientIndex();
	p.TimeDelta = TimeDelta;

	XMFLOAT3 look = Vector3::Normalize(Player->GetLookVector());

	p.mx = Player->m_tPlayerInfo.xmPosition.x;
	p.my = Player->m_tPlayerInfo.xmPosition.y;
	p.mz = Player->m_tPlayerInfo.xmPosition.z;

	p.lookx = look.x;
	p.looky = look.y;
	p.lookz = look.z;

	CServerManager::GetInstance()->SendData(C2S_MOVE, &p);
	moveDelta = 0.f;
	m_bIsMove = false;
}

void CScene_Boardmap::Camera_Change(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_CurrentOrder() != CServerManager::GetInstance()->Get_PrevOrder()) {
		m_bOrderView = false;
		m_OrderTime += TimeDelta;
		if (m_OrderTime < 2)
			return;

		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder()) {

				/*	int currentPlatfornIdx = Players[i]->Get_CurrentPlatformIndex();
					XMFLOAT3 xmPlaytformPos = m_pPlatformManager->Get_Platform(currentPlatfornIdx)->m_pTransformCom->GetPosition();
					Players[i]->m_tPlayerInfo.xmPosition = xmPlaytformPos;*/
				m_nUsingItem = -1;
				m_bMoveTeddyBear = false;

				CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
				dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);
				CServerManager::GetInstance()->Set_CurPlayer(Players[i]);
				CServerManager::GetInstance()->Set_PrevOrder(CServerManager::GetInstance()->Get_CurrentOrder());
				m_OrderTime = 0;
				m_bOrderView = true;

				if (Players[i]->GetPlayerOrder() == 0)
				{
					if (m_SceneChange == SCENE_PRIZE)
					{
						ReadyChangeScene();
					}
					else {
						pMiniTutoUI->SetHide(false);
						pMiniTutoUI->SetTexIndex(m_SceneChange - 4);
						HidePlayerUI(true);
					}
				}
				else
				{
					pBlack->SetHide(false);
					if (m_iTurn >= 8)
						pLastTurn->SetHide(false);
					wstring w = L"";
					string name = Players[i]->Get_Name();

					w.assign(name.begin(), name.end());
					w += L" turn";

					pTurnText->SetTextString(w);

					if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
					{
						pDiceUI->SetHide(false);
						pWhiteUI->SetHide(false);
						pKeyText->SetTextString(L"Enter");
						pKeyWhiteUI->SetHide(false);
					}
				}

				return;
			}
		}
	}

}

short CScene_Boardmap::cameraAnimation(double TimeDelta)
{
	m_fCameratime += TimeDelta;

	CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
	pCameraThird->GetPlayer()->Rotate(0.f, 21 * TimeDelta, 0.f);
	pCameraThird->SetLookAt(XMFLOAT3(20.f, 10.f, 30.f - m_fCameratime * 5.f));
	pCameraThird->SetOffset(XMFLOAT3(50.f, 30.f, 30.f));

	if (m_fCameratime > 10.f)
	{
		m_bEndCameraAnim = true;
		pCameraThird->SetOffset(XMFLOAT3(0.0f, 7.2f, -12.0f));
		pCameraThird->GetPlayer()->m_tPlayerInfo.xmLook = XMFLOAT3(0.f, 0.f, 1.f);

		if (SERVERCONNECT) {
			//게임 시작 완료 패킷 서버에 보내기
			Send_ReadyState();
		}
	}

	return 0;
}

short CScene_Boardmap::buttonNpcFunc(double TimeDelta)
{
	bool bHideEnd = m_pPlatformManager->Get_Platform(m_buttonIndex[m_nButtonRound])->getHideEnd();
	bool bStartEnd = m_pButton->getStartEnd();

	if (bHideEnd == true && bStartEnd == true && m_isStartMoveBalloon == true)
	{
		m_isStartMoveBalloon = false;
		m_pPlatformManager->Get_Platform(m_buttonIndex[m_nButtonRound])->setHideEnd(false);
		m_pButton->setStartEnd(false);

		CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");
		CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
		CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
		pCameraThird->SetOffset(XMFLOAT3(-4.0f, 2.2f, 10.0f));
	}

	bool bBuyButtonEnd = m_pButton->getBuyButtonEnd();
	if ((bBuyButtonEnd == true || m_bnBuyingButton == true) && m_isBuyButton == true)
	{
		if (bBuyButtonEnd == true)
		{
			m_pButton->setBuyButtonEnd(false);
			m_isStartMoveBalloon = true;

			m_nButtonRound++;

			if (m_nButtonRound > 3)
				m_nButtonRound = 0;
		}

		m_isBuyButton = false;
		m_bMoveTeddyBear = true;
		m_bnBuyingButton = false;
		m_isPushedButton = false;

	}

	if (m_isBuyButton)
	{
		Player->m_tPlayerInfo.xmLook = XMFLOAT3(0.f, 0.f, 1.f);

		CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");
		CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
		CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
		pCameraThird->SetOffset(XMFLOAT3(0.f, 7.f, 15.0f));

		POINT ptCursorPos;

		GetCursorPos(&ptCursorPos);
		::ScreenToClient(g_hWnd, &ptCursorPos);

		if (CKeyManager::GetInstance()->KeyDown(KEY_LBUTTON))
		{
			if (ptCursorPos.x >= 560 && ptCursorPos.x <= 616 && ptCursorPos.y >= 385 && ptCursorPos.y <= 434)
			{
				CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
				CSoundManager::GetInstance()->Play_Sound(L"Buy1");
				m_pXButtonforChat->SetTexIndex(0);
				m_pBuyButtonforChat->SetTexIndex(1);
				m_bClickedButton = true;
			}
			else if (ptCursorPos.x >= 663 && ptCursorPos.x <= 718 && ptCursorPos.y >= 385 && ptCursorPos.y <= 434)
			{
				CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
				m_pBuyButtonforChat->SetTexIndex(0);
				m_pXButtonforChat->SetTexIndex(1);
				m_bClickedButton = true;
			}
		}	
		else if (m_isPushedButton == false && m_bClickedButton == true)
		{
			m_isPushedButton = true;
			m_bClickedButton = false;

			if (m_pBuyButtonforChat->GetTexIndex() == 1) // 단추 구매
			{
				if (Player->bCoin >= 10)
				{
					Player->bCoin -= 10;
					Player->bButton += 1;

					m_pButton->setBuyButton(true);

					if (SERVERCONNECT)
					{
						packet_eventselect p;
						p.button = 0;
						p.coin = 5;
						p.buy = true;
						p.id = Player->Get_ClientIndex();
						p.item = ITEM_BUTTON;
						p.size = sizeof(packet_eventselect);
						p.type = C2S_EVENTSELECT;

						CServerManager::GetInstance()->SendData(C2S_EVENTSELECT, &p);
					}
				}
				else // 돈없어서 단추 구매 못 해
				{
					m_pChatBoxCoin->SetHide(false);
				}
			}
			else if (m_pXButtonforChat->GetTexIndex() == 1) // 단추 구매 안해
			{
				// 단추 구매 안해
				m_bnBuyingButton = true;

				if (SERVERCONNECT)
				{
					packet_eventselect p;
					p.button = 0;
					p.coin = 5;
					p.buy = false;
					p.id = Player->Get_ClientIndex();
					p.item = ITEM_BUTTON;
					p.size = sizeof(packet_eventselect);
					p.type = C2S_EVENTSELECT;

					CServerManager::GetInstance()->SendData(C2S_EVENTSELECT, &p);
				}
			}

			m_pChatBoxPurchase->SetHide(true);

			m_pBuyButtonforChat->SetHide(true);
			m_pXButtonforChat->SetHide(true);

		}

		if (m_pChatBoxCoin->GetHide() == false)
		{
			m_fChatBoxTimer += TimeDelta;
			if (m_fChatBoxTimer > 1.f)
			{
				m_pChatBoxCoin->SetHide(true);
				m_fChatBoxTimer = 0.f;
				m_bnBuyingButton = true;

				if (SERVERCONNECT)
				{
					packet_eventselect p;
					p.coin = 5;
					p.buy = false;
					p.id = Player->Get_ClientIndex();
					p.item = ITEM_BUTTON;
					p.size = sizeof(packet_eventselect);
					p.type = C2S_EVENTSELECT;

					CServerManager::GetInstance()->SendData(C2S_EVENTSELECT, &p);
				}
			}
		}


		return 0;
	}

	if (m_isStartMoveBalloon)
	{
		CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Obj");
		CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Obj");
		CCamera_Obj* pCameraObj = dynamic_cast<CCamera_Obj*>(pCamera);
		pCameraObj->SetTargetObj(m_pBalloon);
		pCameraObj->SetOffset(XMFLOAT3(-20.0f, -10.f, 15.0f));

		m_pBalloon->setDestIndex(m_buttonIndex[m_nButtonRound]);

		_uint beforeButtonRound = m_nButtonRound;
		if (beforeButtonRound == 0)
			beforeButtonRound = 3;
		else
			--beforeButtonRound;

		m_pPlatformManager->Get_Platform(m_buttonIndex[beforeButtonRound])->setDefaultPos();

		return 0;
	}

	return 1;
}

void CScene_Boardmap::HidePlayerUI(bool hide)
{
	for (int i = 0; i < 3; ++i) {
		pButtonIcon[i]->SetHide(hide);
		pCoinIcon[i]->SetHide(hide);
		pPlayerIcon[i]->SetHide(hide);
		pStarText[i]->SetHide(hide);
		pClothesButtonText[i]->SetHide(hide);
		pRankText[i]->SetHide(hide);
		pNameText[i]->SetHide(hide);
		pLineText[i]->SetHide(hide);
	}
}

void CScene_Boardmap::HideScoreBoadUI(bool hide)
{
	for (int i = 0; i < 3; ++i) {
		pSocreBoadStarText[i]->SetHide(hide);
		pSocreBoadClothesButtonText[i]->SetHide(hide);
		pSocreBoadRankText[i]->SetHide(hide);
		pSocreBoadNameText[i]->SetHide(hide);
		pSocreBoadPlayerIcon[i]->SetHide(hide);
		pSocreBoadButtonIcon[i]->SetHide(hide);
		pSocreBoadCoinIcon[i]->SetHide(hide);
		pSocreBoadPlusText[i]->SetHide(hide);
		pSocreBoadStamp[i]->SetHide(hide);
	}
	pScoreBlack->SetHide(hide);
}

void CScene_Boardmap::SetScoreBoardUI()
{
	sort(Rankers.begin(), Rankers.end(), [](const CPlayer* a, const CPlayer* b) {
		return  a->bRank < b->bRank;
		});

	for (int i = 0; i < 3; ++i) {
		pSocreBoadStarText[i]->SetTextString(to_wstring(Rankers[i]->bCoin));
		pSocreBoadClothesButtonText[i]->SetTextString(to_wstring(Rankers[i]->bButton));
		if (Rankers[i]->bRank == 1)
		{
			pSocreBoadRankText[i]->SetTextString(L"1st");
		}
		else if (Rankers[i]->bRank == 2)
		{
			pSocreBoadRankText[i]->SetTextString(L"2nd");
		}
		else
		{
			pSocreBoadRankText[i]->SetTextString(L"3rd");
		}
		wstring w = L"";
		string name = Rankers[i]->Get_Name();
		w.assign(name.begin(), name.end());
		pSocreBoadNameText[i]->SetTextString(w);
		pSocreBoadPlayerIcon[i]->SetTexIndex(Rankers[i]->GetSkinid());

		if (Rankers[i]->GetWinner())
		{
			pSocreBoadStarText[i]->SetTextString(to_wstring(Rankers[i]->bCoin - 10));
			pSocreBoadStamp[i]->SetHide(false);
			pSocreBoadPlusText[i]->SetTextString(L"+10");
			m_iScorePlus[i] = 10;
		}
		else
		{
			pSocreBoadStamp[i]->SetHide(true);
			pSocreBoadPlusText[i]->SetHide(true);
			m_iScorePlus[i] = 0;
		}
	}
}

void CScene_Boardmap::ShowTurnUI()
{
	for (int i = 0; i < 3; ++i)
	{
		if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder()) {
			CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
			dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);

			pBlack->SetHide(false);
			if (m_iTurn >= 8)
				pLastTurn->SetHide(false);
			wstring w = L"";
			string name = Players[i]->Get_Name();

			w.assign(name.begin(), name.end());
			w += L" turn";

			pTurnText->SetTextString(w);
		}
	}

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");

	if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
	{
		pDiceUI->SetHide(false);
		pWhiteUI->SetHide(false);
		pKeyText->SetTextString(L"Enter");
		pKeyWhiteUI->SetHide(false);

		CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
		dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);
		CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");
	}

	HidePlayerUI(false);
	HideScoreBoadUI(true);
}

void CScene_Boardmap::SetBuyItem(_uint nBuyItem)
{
	XMFLOAT3 playerPos;

	if (SERVERCONNECT)
		playerPos = CServerManager::GetInstance()->Get_CurPlayer()->m_tPlayerInfo.xmPosition;
	else
		playerPos = Player->m_tPlayerInfo.xmPosition;

	if (nBuyItem == 0)
	{
		playerPos.y += 6.f;
		m_pMiniDice->set_ObjPosition(playerPos);
		m_pMiniDice->setScale(XMFLOAT3(0.0007f, 0.0007f, 0.0007f));
		m_pMiniDice->setBuyDice(true);
	}
	else if (nBuyItem == 1)
	{
		playerPos.y += 6.f;
		m_pStoreButton->set_ObjPosition(playerPos);
		m_pStoreButton->setScale(XMFLOAT3(0.14f, 0.14f, 0.14f));
		m_pStoreButton->setBuyStoreButton(true);
	}
	else if (nBuyItem == 2)
	{
		playerPos.y += 6.f;
		m_pPlus3Icon->SetPostion(playerPos);
		m_pPlus3Icon->SetSize(XMFLOAT2(1.3f, 1.3f));
		m_pPlus3Icon->setBuyItem(true);
	}
	else if (nBuyItem == 3)
	{
		playerPos.y += 6.f;
		m_pPlus5Icon->SetPostion(playerPos);
		m_pPlus5Icon->SetSize(XMFLOAT2(1.3f, 1.3f));
		m_pPlus5Icon->setBuyItem(true);
	}
}

short CScene_Boardmap::ShowBuyItemMotion(double TimeDelta, _uint nBuyItem)
{
	CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
	
	CPlayer* pPlayer = nullptr;
	if (SERVERCONNECT)
		pPlayer = CServerManager::GetInstance()->Get_CurPlayer();
	else
		pPlayer = Player;

	_uint nPlayerIndex = pPlayer->getPlayerIndex();

	if (SERVERCONNECT)
	{
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->Get_ClientIndex() == pPlayer->Get_ClientIndex())
			{
				nPlayerIndex = i;
			}
		}
	}
	

	if (m_bPurchase == true) // 구매 후 먹는 모션
	{
		pCameraThird->SetOffset(XMFLOAT3(0.f, 5.f, 10.0f));
		if (nBuyItem == 0)
		{
			if (m_pMiniDice->getBuyDice() == false)
			{
				m_fStoreEndTimer += TimeDelta;
			}
		}
		else if (nBuyItem == 1)
		{
			if (m_pStoreButton->getBuyStoreButton() == false)
			{
				m_fStoreEndTimer += TimeDelta;
			}
		}
		else if (nBuyItem == 2)
		{
			if (m_pPlus3Icon->getBuyItem() == false)
			{
				m_fStoreEndTimer += TimeDelta;
			}
		}
		else if (nBuyItem == 3)
		{
			if (m_pPlus5Icon->getBuyItem() == false)
			{
				m_fStoreEndTimer += TimeDelta;				
			}
		}

		if (m_fStoreEndTimer > 1.f)
		{
			m_bPurchase = false;
			m_fStoreEndTimer = 0.f;
			
			if (nBuyItem != 1)
			{
				
				CUI* pUI = nullptr;
				CUI* pUI2 = nullptr;
				if (m_listInven[nPlayerIndex].size() >= 3)
				{
					pUI = m_listInven[nPlayerIndex].front();
					m_listInven[nPlayerIndex].pop_front();
					m_listInven[nPlayerIndex].push_back(pUI);

					if (pPlayer->Get_ClientIndex() == Player->Get_ClientIndex())
					{
						pUI2 = m_listMyInven.front();
						m_listMyInven.pop_front();
						m_listMyInven.push_back(pUI2);
					}
				}
				else
				{
					pUI = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_Inven", XMFLOAT3(0.f, -0.25f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.04f, 0.07f, 0.1f)));
					m_listInven[nPlayerIndex].push_back(pUI);

					if (pPlayer->Get_ClientIndex() == Player->Get_ClientIndex())
					{
						pUI2 = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_Inven", XMFLOAT3(-1.f, -1.f, -1.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.13f, 0.22f, 0.1f)));
						m_listMyInven.push_back(pUI2);
					}
				}
				
				if (nBuyItem == 0)
				{
					pUI->SetTexIndex(0);
					pUI->SetHide(false);
					if (pPlayer->Get_ClientIndex() == Player->Get_ClientIndex())
					{
						pUI2->SetTexIndex(0);
						pUI2->SetHide(false);
					}
				}
				else if (nBuyItem == 2)
				{
					pUI->SetTexIndex(1);
					pUI->SetHide(false);
					if (pPlayer->Get_ClientIndex() == Player->Get_ClientIndex())
					{
						pUI2->SetTexIndex(1);
						pUI2->SetHide(false);
					}
				}
				else if (nBuyItem == 3)
				{
					pUI->SetTexIndex(2);
					pUI->SetHide(false);
					if (pPlayer->Get_ClientIndex() == Player->Get_ClientIndex())
					{
						pUI2->SetTexIndex(2);
						pUI2->SetHide(false);
					}
				}

				
			}

			DrawInven();

			m_nSelectItem = 0;
		}

		return 0;
	}

	return 1;

}

void CScene_Boardmap::ReadyChangeScene()
{
	CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	::Player->Set_BeforeLookVector(::Player->m_tPlayerInfo.xmLook);
	::Player->Set_CurrentPlatformPos(Player->m_tPlayerInfo.xmPosition);

	switch (m_SceneChange)
	{
	case SCENE_BOARDMAP:
		break;
	case SCENE_FALLINGMAP:
	{
		dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);
		//플레이어 위치 Falling Map으로 수정 및 패킷 보내기
		packet_scenechange p;
		p.id = Player->Get_ClientIndex();
		p.size = sizeof(packet_scenechange);
		p.type = C2S_SCENECHANGE;
		p.map = S_FALLINGMAP;
		CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

		Player->SetDeath(false);
		Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);

		for (int i = 0; i < 3; ++i)
		{
			Players[i]->xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
		}

		//다음 미니게임 맵이름으로 바꾸기
		m_SceneChange = SCENE_GGOGGOMAP; //이거를 바꾸면 주사우 ㅣ3번 굴리고 떨어져유 맵 다음 미니게임으로 가짐
		Scene_Change(SCENE_FALLINGMAP, this);
	}
	break;
	case SCENE_GGOGGOMAP:
	{
		dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);
		//플레이어 위치 GGo GGo 맵으로 수정 및 패킷 보내기
		CServerManager::GetInstance()->Set_PlayTime(0);

		packet_scenechange p;
		p.id = Player->Get_ClientIndex();
		p.size = sizeof(packet_scenechange);
		p.type = C2S_SCENECHANGE;
		p.map = S_GGOGGOMAP;
		CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

		Player->SetDeath(false);
		Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);

		for (int i = 0; i < 3; ++i)
		{
			Players[i]->xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
		}

		//다음 미니게임 맵이름으로 바꾸기 나중에
		m_SceneChange = SCENE_BULLFIGHTMAP;
		Scene_Change(SCENE_GGOGGOMAP, this);
	}
	break;
	case SCENE_BULLFIGHTMAP:
	{
		dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);
		//플레이어 위치 GGo GGo 맵으로 수정 및 패킷 보내기
		CServerManager::GetInstance()->Set_PlayTime(0);
		CServerManager::GetInstance()->Set_PotionCreate(false);

		packet_scenechange p;
		p.id = Player->Get_ClientIndex();
		p.size = sizeof(packet_scenechange);
		p.type = C2S_SCENECHANGE;
		p.map = S_BULLFIGHTMAP;
		CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

		Player->SetDeath(false);
		Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);

		for (int i = 0; i < 3; ++i)
		{
			Players[i]->xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
		}
		//다음 미니게임 맵이름으로 바꾸기 나중에
		m_SceneChange = SCENE_ONEMINDMAP;
		Scene_Change(SCENE_BULLFIGHTMAP, this);
	}
	break;
	case SCENE_ONEMINDMAP:
	{
		dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);

		CServerManager::GetInstance()->Set_PlayTime(0);

		packet_scenechange p;
		p.id = Player->Get_ClientIndex();
		p.size = sizeof(packet_scenechange);
		p.type = C2S_SCENECHANGE;
		p.map = S_ONEMINDMAP;
		CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

		Player->SetDeath(false);
		Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);
		//다음 미니게임 맵이름으로 바꾸기 나중에

		for (int i = 0; i < 3; ++i)
		{
			Players[i]->xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
			Players[i]->Set_OneMindRoll(-1);
		}
		m_SceneChange = SCENE_FALLINGMAP;
		Scene_Change(SCENE_ONEMINDMAP, this);
	}
	break;
	case SCENE_PRIZE:
	{
		packet_scenechange p;
		p.id = Player->Get_ClientIndex();
		p.size = sizeof(packet_scenechange);
		p.type = C2S_SCENECHANGE;
		p.map = S_PRIZEMAP;
		CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

		m_SceneChange = SCENE_PRIZE;
		Scene_Change(SCENE_PRIZE, this);
	}
	break;
	case SCENE_END:
		break;
	default:
		break;
	}
}

short CScene_Boardmap::ShowMyInven(double TimeDelta)
{
	POINT ptCursorPos;

	GetCursorPos(&ptCursorPos);
	::ScreenToClient(g_hWnd, &ptCursorPos);

	CPlayer* pPlayer = nullptr;
	if (SERVERCONNECT)
		pPlayer = CServerManager::GetInstance()->Get_CurPlayer();
	else
		pPlayer = Player;

	_uint nPlayerIndex = pPlayer->getPlayerIndex();

	if (m_listMyInven.size() == 0)
	{
		m_bShowMyInven = false;
		m_bClickedInvenX = false;
		m_fInvenSpeed = 1.f;
		m_bMyInvenEnd = true;
		m_pInvenXButton->SetHide(true);

		for (CUI* pUI : m_listMyInven)
			pUI->getTransform()->SetPositionY(-1.2f);

		if (m_nUsingItem != -1)
		{
			m_bUsingItemMotion = true;
			DrawInven();

			if (SERVERCONNECT) {
				packet_itemuse pi;
				pi.id = Player->Get_ClientIndex();
				pi.size = sizeof(packet_itemuse);
				pi.type = C2S_ITEMUSE;
				pi.dice = 0;
				pi.item = m_nUsingItem;
				pi.invenindex = m_nEraseIndex;

				CServerManager::GetInstance()->SendData(C2S_ITEMUSE, &pi);
			}
		}

		return 1;
	}
	else if (m_listMyInven.front()->getTransform()->GetPosition().y >= -0.8f && m_pInvenXButton->GetHide() == true)
	{
		m_pInvenXButton->SetHide(false);
		for (CUI* pUI : m_listMyInven)
			pUI->getTransform()->SetPositionY(-0.8f);
	}
	else if (m_listMyInven.front()->getTransform()->GetPosition().y <= -1.2f && m_bClickedInvenX == true)
	{
		m_bShowMyInven = false;
		m_bClickedInvenX = false;
		m_fInvenSpeed = 1.f;
		m_bMyInvenEnd = true;
		m_pInvenXButton->SetHide(true);

		for (CUI* pUI : m_listMyInven)
			pUI->getTransform()->SetPositionY(-1.2f);

		if (m_nUsingItem != -1)
		{
			m_bUsingItemMotion = true;
			DrawInven();

			if (SERVERCONNECT) {
				packet_itemuse pi;
				pi.id = Player->Get_ClientIndex();
				pi.size = sizeof(packet_itemuse);
				pi.type = C2S_ITEMUSE;
				pi.dice = 0;
				pi.item = m_nUsingItem;
				pi.invenindex = m_nEraseIndex;

				CServerManager::GetInstance()->SendData(C2S_ITEMUSE, &pi);
			}
		}
		return 1;
	}

	if (CKeyManager::GetInstance()->KeyDown(KEY_LBUTTON) && m_pInvenXButton->GetHide() == false && m_nUsingItem == -1)
	{
		if (ptCursorPos.x >= 932 && ptCursorPos.x <= 988 && ptCursorPos.y >= 462 && ptCursorPos.y <= 511) // x button
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
			m_pInvenXButton->SetHide(true);
			m_bClickedInvenX = true;
			m_fInvenSpeed *= -1.f;
		}
		
		m_nEraseIndex = 0;
		auto iter2 = m_listInven[nPlayerIndex].begin();
		for (auto iter = m_listMyInven.begin(); iter != m_listMyInven.end();)
		{
			float fx = ptCursorPos.x - (477.f + (163.f * m_nEraseIndex));
			float fy = ptCursorPos.y - 637.f;
			float dist = sqrtf(fx * fx + fy * fy);
			m_nUsingItem = -1;
			if (dist <= 72.f)
			{
				CUI* pUI = (*iter);
				
				if (pUI->GetTexIndex() == 0)
					m_nUsingItem = 0;
				else if (pUI->GetTexIndex() == 1)
					m_nUsingItem = 2;
				else if (pUI->GetTexIndex() == 2)
					m_nUsingItem = 3;
				iter = m_listMyInven.erase(iter);			
				pUI->setdead();

				if (!SERVERCONNECT)
				{
					CUI* pUI2 = (*iter2);
					iter2 = m_listInven[nPlayerIndex].erase(iter2);
					pUI2->setdead();
				}

				m_fInvenSpeed *= -1.f;
				m_bClickedInvenX = true;
				DrawInven();
				
				break;
			}
			else
			{
				iter++;

				if (!SERVERCONNECT)
					iter2++;
			}
			++m_nEraseIndex;

		}
	}
	
	if (m_pInvenXButton->GetHide() == true || m_bClickedInvenX == true)
	{
		for (CUI* pUI : m_listMyInven)
		{
			pUI->getTransform()->MoveUp(TimeDelta * 2.f * m_fInvenSpeed);
		}
	}

	return 0;
}

short CScene_Boardmap::ShowUsingItemMotion(double TimeDelta)
{
	if (m_nUsingItem == 0) // mini dice
	{
		if (!SERVERCONNECT)
			m_nItemDiceNum = rand() % 3 + 1;
	}
	else if (m_nUsingItem == 2) // +3
	{
		if (!SERVERCONNECT)
			m_nItemDiceNum = 3;
	}
	else if (m_nUsingItem == 3) // +5
	{
		if (!SERVERCONNECT)
			m_nItemDiceNum = 5;
	}

	m_bUsingItemMotion = false;

	return 0;
}

short CScene_Boardmap::DrawInven()
{
	_uint index = 0;
	for (int i = 0; i < 3; ++i)
	{
		for (CUI* pUI : m_listInven[i])
		{
			if (pUI != nullptr)
			{
				pUI->getTransform()->SetPosition(m_xmUIPos[index]);
			}

			index++;
		}
		index = (i + 1) * 3;
	}

	int iCount = 0;
	for (CUI* pUI : m_listMyInven)
	{
		pUI->getTransform()->SetPosition(XMFLOAT3(-0.25f + (0.25 * iCount), -1.2f, 0.f)); // y : -0.8f
		iCount++;
	}


	return 0;
}

void CScene_Boardmap::ItemUsed()
{
	if (SERVERCONNECT) {
		//아이템이 사용된 경우 한 번만 둘어옴
		if (CServerManager::GetInstance()->Get_isItemUse()) //아이템이 사용 됐는가? true -> 사용됐다.
		{
			m_nUsingItem = CServerManager::GetInstance()->Get_UsedItemType(); //사용한 아이템 종류
			int inventory = CServerManager::GetInstance()->Get_InventoryIndex(); //사용한 아이템 인벤토리 인덱스
			//사용한 플레이어는 현재 차례인 플레이어 CServerManager::GetInstance()->Get_CurPlayer();
			//혹시라도 안먹힐 경우 아이템 사용한 사람 인덱스 CServerManager::GetInstance()->Get_UsedPlayerIndex();

			CPlayer* pPlayer = CServerManager::GetInstance()->Get_CurPlayer();
			_uint nIndex = pPlayer->getPlayerIndex();

			int count = 0;
			for (auto iter = m_listInven[nIndex].begin(); iter != m_listInven[nIndex].end();)
			{
				if (count == inventory)
				{
					CUI* pUI = (*iter);
					iter = m_listInven[nIndex].erase(iter);
					pUI->setdead();
					break;
				}
				else
				{
					++iter;
				}
				count++;
			}

			DrawInven();

			switch (m_nUsingItem)
			{
			case ITEM_MINIDICE://사용된 아이템이 미니주사위
			{
				m_nItemDiceNum = CServerManager::GetInstance()->Get_MiniDiceNum(); //미니주사위 숫자 넘겨주기
				CServerManager::GetInstance()->Set_MiniDiceNum(0); //숫자넘겨준 후 다음을 위해 0으로 초기화			
			}
			break;
			case ITEM_PLUS3: //사용된 아이템이 +3
			{
				m_nItemDiceNum = 3;
			}
			break;
			case ITEM_PLUS5: //사용된 아이템이 +5
			{
				m_nItemDiceNum = 5;
			}
			break;
			case ITEM_END:
				break;
			default:
				break;
			}

			m_bUsingItemMotion = true;
			m_bMyInvenEnd = true;

			//초기화
			CServerManager::GetInstance()->Set_InventoryIndeX(-1);
			CServerManager::GetInstance()->Set_UsedItemType(ITEM::ITEM_END);
			CServerManager::GetInstance()->Set_isItemUse(false); //get_isItemUse 한번만 들어오도록 false초기화
		}
	}
}

void CScene_Boardmap::MovePlayerPlatform(CPlayer* movePlayer, int PlatformIndex)
{
	movePlayer->Set_CurrentPlatformIndex(PlatformIndex);
	movePlayer->m_tPlayerInfo.xmPosition = m_pPlatformManager->
		Get_Platform(movePlayer->Get_CurrentPlatformIndex())->m_pTransformCom->GetPosition();

	packet_teleport p;
	p.id = movePlayer->Get_ClientIndex();
	p.type = C2S_TELEPORT;
	p.size = sizeof(packet_teleport);
	p.x = movePlayer->m_tPlayerInfo.xmPosition.x;
	p.y = movePlayer->m_tPlayerInfo.xmPosition.y;
	p.z = movePlayer->m_tPlayerInfo.xmPosition.z;

	CServerManager::GetInstance()->SendData(C2S_TELEPORT, &p);
		

}

void CScene_Boardmap::TurnEnd(double TimeDelta)
{
	Send_MovePacket(TimeDelta);

	packet_dicemoveend p;
	p.id = Player->Get_ClientIndex();
	p.size = sizeof(p);
	p.type = C2S_DICEMOVEEND;
	CServerManager::GetInstance()->SendData(C2S_DICEMOVEEND, &p);

	packet_currentorder p2;
	p2.id = Player->Get_ClientIndex();
	p2.size = sizeof(p2);
	p2.type = C2S_CURRENTORDER;
	p2.currentorder = CServerManager::GetInstance()->Get_CurrentOrder();
	p2.currentplatform = Player->Get_CurrentPlatformIndex();
	CServerManager::GetInstance()->SendData(C2S_CURRENTORDER, &p2);
}

void CScene_Boardmap::MiniDiceSet()
{
	if (m_nUsingItem == 0)
	{
		CPlayer* pPlayer;
		if (SERVERCONNECT)
			pPlayer = CServerManager::GetInstance()->Get_CurPlayer();
		else
			pPlayer = Player;
	 	XMFLOAT3 PlayerPos = pPlayer->m_tPlayerInfo.xmPosition;
		XMFLOAT3 Pos = XMFLOAT3(PlayerPos.x, PlayerPos.y + 3.3, PlayerPos.z);
		XMFLOAT3 right = Vector3::Normalize(CCameraManager::GetInstance()->GetCurrentCamera()->get_CameraInfo().xmRight);
		XMFLOAT3 dicePos = Vector3::Add(Pos, right, 0.7f);
		XMFLOAT3 miniDicePos = Vector3::Subtract(Pos, right, 0.7f);
		pDice->m_pTransformCom->SetPosition(dicePos);
		m_pUsingMiniDice->m_pTransformCom->SetPosition(miniDicePos);

		m_bMiniDiceSet = true;
	}
}

void CScene_Boardmap::ShowItemDiceNumber(double TimeDelta)
{
	m_fItemDiceTimer += TimeDelta;
	CPlayer* pPlayer;
	if (SERVERCONNECT)
		pPlayer = CServerManager::GetInstance()->Get_CurPlayer();
	else
		pPlayer = Player;
	XMFLOAT3 PlayerPos = pPlayer->m_tPlayerInfo.xmPosition;
	XMFLOAT3 Pos = pBillBoardObject[0]->GetPostion();
	XMFLOAT3 Pos1 = m_pItemDiceBill->GetPostion();
	XMFLOAT3 right = Vector3::Normalize(CCameraManager::GetInstance()->GetCurrentCamera()->get_CameraInfo().xmRight);
	XMFLOAT3 dicePos = Vector3::Subtract(Pos, right, 0.5f * TimeDelta);
	XMFLOAT3 miniDicePos = Vector3::Add(Pos1, right, 0.5f * TimeDelta);

	if (m_fItemDiceTimer > 0.7f && m_fItemDiceTimer <= 1.4f)
	{
		pBillBoardObject[0]->SetPostion(dicePos);
		m_pItemDiceBill->SetPostion(miniDicePos);
		m_pPlusBill->SetPostion(XMFLOAT3(1000.f, 1000.f, 1000.f));
	}
	else if (m_fItemDiceTimer > 1.4f && m_fItemDiceTimer <= 1.9f)
	{			
		if (SERVERCONNECT)
			m_iDiceNumber = CServerManager::GetInstance()->Get_DiceNum() + m_nItemDiceNum;
		else
			m_iDiceNumber = m_iDiceNumber + m_nItemDiceNum;

		pBillBoardObject[0]->SetPostion(XMFLOAT3(PlayerPos.x, PlayerPos.y + 3.3f, PlayerPos.z));
		pBillBoardObject[0]->SetIndexY(m_iDiceNumber - 1);
		m_pItemDiceBill->SetPostion(XMFLOAT3(1000.f, 1000.f, 1000.f));
	}
	else if (m_fItemDiceTimer > 1.9f)
	{
		m_fItemDiceTimer = 0.f;

		if (Player->GetPlayerOrder() == CServerManager::GetInstance()->Get_PrevOrder() || !SERVERCONNECT)
		{
			m_bMoveTeddyBear = true;
		}
		m_bShowItemDiceNum = false;
		m_nItemDiceNum = 0;

		if (SERVERCONNECT)
			CServerManager::GetInstance()->Set_DiceNum(m_iDiceNumber);
	}

}
