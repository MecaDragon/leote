#ifndef __Camera_Light__
#define __Camera_Light__

#include "stdafx.h"
#include "Camera.h"

struct VS_CB_LIGHT_CAMERA_INFO
{
	XMFLOAT4X4	m_xmf4x4ViewProjection;
	XMFLOAT4X4	m_xmf4x4ShadowTransform;
};

class CCamera_Light : public CCamera
{	
private:
	bool isClip = true;
	XMFLOAT3 m_xmf3Offset;
	ID3D12Resource* m_pd3dcbLightCamera;
	VS_CB_LIGHT_CAMERA_INFO* m_pcbMappedLightCamera;
	XMFLOAT3 m_xmTargetPos = XMFLOAT3(-14.0604, 15.6328, 8.74705);
	XMFLOAT3 m_xmDicrtionPos = XMFLOAT3(89.8112, 100.2391, -97.7247);

private:
	explicit CCamera_Light();
	explicit CCamera_Light(const CCamera_Light& rhs);
	virtual ~CCamera_Light() = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

public:
	static CCamera_Light* Create();
	void SetOffset(XMFLOAT3 xmf3Offset) { m_xmf3Offset = xmf3Offset; }
	// CCamera을(를) 통해 상속됨
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	void SetTargetPos(XMFLOAT3 targetpos) { m_xmTargetPos = targetpos; }
	void SetDicrtionPos(XMFLOAT3 directionpos) { m_xmDicrtionPos = directionpos; }

	HRESULT Release();

};

#endif