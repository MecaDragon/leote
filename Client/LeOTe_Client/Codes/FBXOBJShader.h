#ifndef __FBXOBJSHADER__
#define __FBXOBJSHADER__

#include "Shader.h"

class CFBXOBJShader final : public CShader
{
private:
	explicit CFBXOBJShader();
	explicit CFBXOBJShader(const CFBXOBJShader& rhs);
	virtual ~CFBXOBJShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CFBXOBJShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();

};

#endif
