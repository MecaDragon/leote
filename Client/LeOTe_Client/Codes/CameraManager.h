#ifndef __CAMERAMANAGER__
#define __CAMERAMANAGER__

#include "stdafx.h"

class CCamera;
class CCameraManager final
{
	DECLARE_SINGLETON(CCameraManager)

private:
	explicit CCameraManager();
	virtual ~CCameraManager();

public:
	HRESULT Initialize_CameraManager(_uint iNumScenes);
	short Update_CameraManager(double TimeDelta);
	short LateUpdate_CameraManager(double TimeDelta);

	HRESULT Release();

public:
	HRESULT Add_Camera(CCamera* pCamera, string strTag);
	CCamera* Find_Camera(string strTag);
	HRESULT Set_CurrentCamera(string strTag);
	HRESULT Delete_Camera(string strTag);
	CCamera* GetCurrentCamera() { return m_pCurrentCamera; }

private:
	map<string, CCamera*> m_mapCamera;
	CCamera* m_pCurrentCamera;


};

#endif // !__CAMERAMANAGER__

