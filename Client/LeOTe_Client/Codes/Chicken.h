#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CCollider;
class CSphereCollider;
class CTexture;
class CMaterial;
class CShadowShader;

class CPlayer;

class CChicken final : public CGameObject
{
private:
	explicit CChicken();
	explicit CChicken(const CChicken& rhs);
	virtual ~CChicken() = default;
public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

	virtual void OnCollision(CCollider& collisionTarget, double TimeDelta);

	void ChangeAnimation(string sClipName);
	bool IsEndAnimation() { return m_aimationEnd; }
	bool IsMiddleAnimation() { return m_aimationMiddle; }

	//server
	void SetChickenState(char state);
	void SetLerpStart(packet_chicken p);
	void updateLerp(double TimeDelta);

private:
	XMFLOAT3 m_xmLerpStartPos = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmLerpStartLook = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmLerpDestPos = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmLerpDestLook = XMFLOAT3(0.f, 0.f, 0.f);

	bool m_isLerping = false;
	float m_fLerpTimeDelta = 0.f;

public:
	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	BoundingBox m_xmBoundingBox;
	float RedenrTimeDelta = 0.01f;

	std::string mClipName;
	std::string mNextClipName;
private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CTexture* m_pNormalTextureCom = nullptr;
	CCollider* m_pSphereColliderCom = nullptr;

	FbxScene* m_pfbxScene = NULL;
	SkinnedData mSkinnedInfo;
	std::unique_ptr<SkinnedModelInstance> mSkinnedModelInst;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CharacterConstants* skinnedConstants = nullptr;
	std::vector<CharacterVertex> outSkinnedVertices;
	std::vector<std::uint32_t> outIndices;
	std::vector<Material> outMaterial;

	ID3D12Resource* m_pd3dVertexBuffer = NULL;
	ID3D12Resource* m_pd3dVertexUploadBuffer = NULL;

	ID3D12Resource* m_pd3dIndexBuffer = NULL;
	ID3D12Resource* m_pd3dIndexUploadBuffer = NULL;

	D3D12_VERTEX_BUFFER_VIEW		m_d3dVertexBufferView;
	D3D12_INDEX_BUFFER_VIEW			m_d3dIndexBufferView;

	D3D12_PRIMITIVE_TOPOLOGY		m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT							m_nSlot = 0;
	UINT							m_nVertices = 0;
	UINT							m_nStride = 0;
	UINT							m_nOffset = 0;

	UINT							m_nIndices = 0;
	UINT							m_nStartIndex = 0;
	int								m_nBaseVertex = 0;


	bool m_aimationEnd = false;
	bool m_aimationMiddle = false;

	CPlayer* m_pPlayer = nullptr;
	float m_fSpeed = 1.f;
	float m_fTimer = 0.f;
	float m_fTimerLimit = 0.f;
	string m_animArray[5];
	int m_iRotate = 0;

	XMFLOAT3 m_xmLook = XMFLOAT3(0.f, 0.f, 0.f);

	float m_fAngle = 0.f;
	float m_fLimitAngle = 0.f;

	bool m_isRotating = false;
	bool m_bCheckx = false;
	bool m_bCheckz = false;

	float m_fDot = 0.f;
	XMFLOAT3 m_xmSlidingVector = XMFLOAT3(0.f, 0.f, 0.f);

	//server
	int m_iIndex = -1;
	char m_cState = -1;
private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();

	float slidingVector(XMFLOAT3 xmNormal, XMFLOAT3 xmLook, XMFLOAT3& outputSlidingVector);

	void checkPlayerCollision(double TimeDelta, string& nextAnimation);

public:
	static CChicken* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();
};