#ifndef __BillBoardShader__
#define __BillBoardShader__

#include "Shader.h"

class CBillBoardShader final : public CShader
{
private:
	explicit CBillBoardShader();
	explicit CBillBoardShader(const CBillBoardShader& rhs);
	virtual ~CBillBoardShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CBillBoardShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_GeometryShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();

};

#endif
