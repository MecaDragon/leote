//------------------------------------------------------- ----------------------
// File: Object.h
//-----------------------------------------------------------------------------

#pragma once

#include "FbxManager.h"
#include "Component.h"

class CAnimationController  final : public CComponent
{
private:
	explicit CAnimationController();
	explicit CAnimationController(const CAnimationController& rhs);
	virtual ~CAnimationController(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CAnimationController* Create();
	void SetFBXScene(FbxScene* pfbxScene);
	void SetAnimationStack(FbxScene* pfbxScene, int nAnimationStack);

	void AdvanceTime(float fElapsedTime);
	FbxTime GetCurrentTime() { return(m_pfbxCurrentTimes[m_nAnimationStack]); }

	void SetPosition(int nAnimationStack, float fPosition);
	void Animate(FbxScene* pfbxScene, float fTimeElapsed);

	HRESULT Release();

public:
    float 							m_fTime = 0.0f;

	int 							m_nAnimationStacks = 0;
	FbxAnimStack 					**m_ppfbxAnimationStacks = NULL;

	int 							m_nAnimationStack = 0;

	FbxTime							*m_pfbxStartTimes = NULL;
	FbxTime							*m_pfbxStopTimes = NULL;

	FbxTime							*m_pfbxCurrentTimes = NULL;
};

