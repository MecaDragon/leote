#ifndef __OBJECTMANAGER__
#define __OBJECTMANAGER__

#include "stdafx.h"
#include "Layer.h"

class CObjectManager final
{
	DECLARE_SINGLETON(CObjectManager)

private:
	explicit CObjectManager();
	virtual ~CObjectManager();

public:
	HRESULT Initialize_ObjectManager(_uint iNumScenes);
	HRESULT Add_Prototype_GameObj(const wchar_t* pPrototypeTag, CGameObject * pPrototype);
	HRESULT Add_GameObjectToLayer(_uint iSceneIndex, const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, CGameObject** ppOutput, void* pArg);
	short Update_ObjectManager(double TimeDelta);
	short LateUpdate_ObjectManager(double TimeDelta);

	HRESULT Render_ShadowMap();

	HRESULT Clear_ObjectManager(_uint iSceneIndex);
	HRESULT Clear_ObjectPrototype(_uint iSceneIndex);

	CLayer* Find_Layer(_uint iSceneIndex, const wchar_t* pLayerTag);


private:
	CGameObject* Find_Prototype(const wchar_t* pPrototypeTag);

	HRESULT Release();
	
private:
	unordered_map<const wchar_t*, CGameObject*>*	m_pPrototype;
	typedef unordered_map<const wchar_t*, CGameObject*>	PROTOTYPES;

	unordered_map<const wchar_t*, CLayer*>* m_pLayers = nullptr;
	typedef unordered_map<const wchar_t*, CLayer*>		LAYERS;

	_uint		m_iNumScenes = 0;


};

#endif // !__OBJECTMANAGER__

