#include "PipeLine.h"

IMPLEMENT_SINGLETON(CPipeLine)

CPipeLine::CPipeLine()
{
}

CPipeLine::~CPipeLine()
{
}

void CPipeLine::Set_ViewMatrix(const XMFLOAT4X4& Matrix)
{
	m_ViewMatrix = Matrix;
	m_ViewInverseMatrix = Matrix4x4::Inverse(m_ViewMatrix);
}

void CPipeLine::Set_ProjectionMatrix(PROJECTION_TYPE eProjType)
{
	if (eProjType == PERSPECTIVE_PROJ)
		m_ProjectionMatrix = m_PerspectiveProjMatrix;
	else if (eProjType == ORTHOGRAPHIC_PROJ)
		m_ProjectionMatrix = m_OrthographicProjMatrix;
}

void CPipeLine::Set_ViewportsAndScissorRects(ID3D12GraphicsCommandList* pCommandList)
{
	pCommandList->RSSetViewports(1, &m_Viewport);
	pCommandList->RSSetScissorRects(1, &m_ScissorRect);
}

void CPipeLine::UpdateShaderVariables(ID3D12GraphicsCommandList* pCommandList)
{
	XMFLOAT4X4 matView;
	XMStoreFloat4x4(&matView, XMMatrixTranspose(XMLoadFloat4x4(&m_ViewMatrix)));
	::memcpy(&m_pcbMappedCamera->m_xmf4x4View, &matView, sizeof(XMFLOAT4X4));

	XMFLOAT4X4 xmf4x4Projection;
	XMStoreFloat4x4(&xmf4x4Projection, XMMatrixTranspose(XMLoadFloat4x4(&m_ProjectionMatrix)));
	::memcpy(&m_pcbMappedCamera->m_xmf4x4Projection, &xmf4x4Projection, sizeof(XMFLOAT4X4));

	::memcpy(&m_pcbMappedCamera->m_xmf3Position, &m_CameraPosition, sizeof(XMFLOAT3));
	
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbCamera->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(0, d3dGpuVirtualAddress);
}

HRESULT CPipeLine::Release()
{

	if (m_pd3dcbCamera)
	{
		m_pd3dcbCamera->Release();
	}
	delete this;

	return NOERROR;
}
