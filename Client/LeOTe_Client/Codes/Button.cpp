#include "stdafx.h"
#include "Button.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"
#include "ShadowShader.h"
#include "PlatformManager.h"

CButton::CButton()
	: CGameObject()
{

}

CButton::CButton(const CButton& rhs)
	: CGameObject(rhs)
{
}

HRESULT CButton::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CButton::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	if (pArg)
	{
		m_pPlatformManager = (CPlatformManager*)pArg;
	}

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	XMFLOAT3 pos = m_pPlatformManager->Get_Platform(m_buttonIndex[m_nButtonRound])->m_pTransformCom->GetPosition();
	pos.y = 13.6f;
	m_xmObjPosition = pos;
	m_pTransformCom->SetPosition(m_xmObjPosition);
	m_pTransformCom->SetScale(m_xmScale.x, m_xmScale.y, m_xmScale.z);	

	return NOERROR;
}

short CButton::Update_GameObject(double TimeDelta)
{
	if (m_bStart == true)
	{
		m_fTimer += (float)(TimeDelta * m_fSpeed * m_nUpDown);

		m_xmObjPosition.y = 15.6f + m_fTimer;
		m_xmScale.x = 0.01f + m_fTimer * 0.1f;
		m_xmScale.y = 0.01f + m_fTimer * 0.1f;
		m_xmScale.z = 0.01f + m_fTimer * 0.1f;

		if (m_xmObjPosition.y > 18.f)
		{
			m_bStart = false;
			m_bStartEnd = true;
		}
	}

	
	if (m_bBuyButton == true)
	{
		m_fTimer2 += TimeDelta;
		if (m_fTimer2 < 0.6f)
		{
			m_xmLook = XMFLOAT3(0.f, 0.f, 1.f);
			m_xmObjPosition.y -= TimeDelta * 1.3f;
		}
		else if (0.6f <= m_fTimer2 && m_fTimer2 < 1.1f)
		{
			m_xmLook = XMFLOAT3(0.f, 0.f, 1.f);
			m_xmObjPosition.y += TimeDelta * 1.3f;
		}
		else
		{
			XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
			m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			m_xmObjPosition.y -= TimeDelta * 0.6f;

			m_xmScale.x -= TimeDelta * 0.1f;
			m_xmScale.y -= TimeDelta * 0.1f;
			m_xmScale.z -= TimeDelta * 0.1f;

			if (m_xmScale.x < 0.01f)
			{
				m_bBuyButton = false;
				m_bBuyButtonEnd = true;
				m_xmObjPosition.y = 15.6f;
				++m_nButtonRound;
				if (m_nButtonRound >= 4)
					m_nButtonRound = 0;
				XMFLOAT3 pos = m_pPlatformManager->Get_Platform(m_buttonIndex[m_nButtonRound])->m_pTransformCom->GetPosition();
				m_xmObjPosition.x = pos.x;
				m_xmObjPosition.z = pos.z;
				m_xmScale = XMFLOAT3(0.01f, 0.01f, 0.01f);
				m_fTimer = 0.f;
			}
		}


	}
	else
	{
		XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 80.f));
		m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
	}


	if (m_bStoreButton == true)
	{
		if (m_bBuyStoreButton == true)
		{
			m_fTimer += TimeDelta;
			if (m_fTimer < 1.5f)
			{
				m_xmObjPosition.y -= TimeDelta * 1.8f;
				XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
			else if (m_fTimer >= 1.5f && m_fTimer < 2.5f)
			{
				XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
			else
			{
				XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 220.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
				m_xmObjPosition.y -= TimeDelta * 1.8f;

				m_xmScale.x -= TimeDelta * 0.16f;
				m_xmScale.y -= TimeDelta * 0.16f;
				m_xmScale.z -= TimeDelta * 0.16f;

				if (m_xmScale.x < 0.f)
				{
					m_bBuyStoreButton = false;
					m_xmObjPosition.y = 1000.f;
					m_xmScale = XMFLOAT3(0.01f, 0.01f, 0.01f);
					m_fTimer = 0.f;
				}
			}

		}
	}

	return short();
}

short CButton::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	m_pTransformCom->SetUp(XMFLOAT3(0.f, 1.f, 0.f));
	m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_xmLook, true));
	m_pTransformCom->SetLook(m_xmLook);
	m_pTransformCom->SetPosition(m_xmObjPosition.x, m_xmObjPosition.y, m_xmObjPosition.z);
	m_pTransformCom->SetScale(m_xmScale.x, m_xmScale.y, m_xmScale.z);
	m_pTransformCom->Rotate(-0.f, 0.f, 0.f);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CButton::Render_Shadow()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform);

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	if (m_pTextureCom)
		m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();

	return NOERROR;
}

HRESULT CButton::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CButton::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Button_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader //Component_Shader_FBXTexLight
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXTexMLight", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_Button", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

CButton* CButton::Create()
{
	CButton* pInstance = new CButton();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CButton Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CButton::Clone_GameObject(void* pArg)
{
	CButton* pInstance = new CButton(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CButton Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CButton::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
