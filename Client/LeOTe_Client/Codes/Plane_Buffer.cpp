#include "Plane_Buffer.h"
#include "Management.h"

CPlane_Buffer::CPlane_Buffer()
{
}

CPlane_Buffer::CPlane_Buffer(const CPlane_Buffer& rhs)
	: CBuffer(rhs)
{
}

HRESULT CPlane_Buffer::Initialize_Component_Prototype()
{

	m_nVertices = 6;
	m_nStride = sizeof(Vertex_Tex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	Vertex_Tex vertices[6] =
	{
		Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
		Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) }),
		Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),

		Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
		Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),
		Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) }),
	};

	m_pd3dVertexBuffer = CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
		vertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);


	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	return NOERROR;
}

HRESULT CPlane_Buffer::Initialize_Component(void* pArg)
{
	if(m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->AddRef();

	if(m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->AddRef();

	if(m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->AddRef();

	if(m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->AddRef();

	m_pd3dVertexBuffer->SetName(L"Plane Buffer m_pd3dVertexBuffer");
	m_pd3dVertexUploadBuffer->SetName(L"Plane Buffer m_pd3dVertexUploadBuffer");
	//m_pd3dIndexBuffer->SetName(L"Plane Buffer m_pd3dIndexBuffer");
	//m_pd3dIndexUploadBuffer->SetName(L"Plane Buffer m_pd3dIndexUploadBuffer");
	return NOERROR;
}

HRESULT CPlane_Buffer::Render_Buffer()
{
	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	
	pd3dCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);

	if (m_d3dIndexBufferView.SizeInBytes != 0)
	{
		pd3dCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
		pd3dCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	}
	else
		pd3dCommandList->DrawInstanced(m_nVertices, 1, m_nOffset, 0);


	return NOERROR;
}

CComponent* CPlane_Buffer::Clone_Component(void* pArg)
{
	CPlane_Buffer* pInstance = new CPlane_Buffer(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CPlane_Buffer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CPlane_Buffer* CPlane_Buffer::Create()
{
	CPlane_Buffer* pInstance = new CPlane_Buffer();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CPlane_Buffer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CPlane_Buffer::Release()
{
	CBuffer::Release_UploadBuffer();

	if(m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->Release();

	if(m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->Release();


	delete this;

	return NOERROR;
}
