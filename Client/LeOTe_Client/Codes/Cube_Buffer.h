#ifndef __CUBE_BUFFER__
#define __CUBE_BUFFER__

#include "stdafx.h"
#include "Buffer.h"

class CCube_Buffer : public CBuffer
{
public:
	enum CUBE_TYPE { CUBE_COLOR, CUBE_TEX };

private:
	explicit CCube_Buffer();
	explicit CCube_Buffer(const CCube_Buffer& rhs);
	virtual ~CCube_Buffer(void) = default;

public:
	virtual HRESULT Initialize_Component_Prototype(CUBE_TYPE eCubeType);
	virtual HRESULT Initialize_Component(void* pArg);

	HRESULT Render_Buffer();

	virtual CComponent* Clone_Component(void* pArg);

	static CCube_Buffer* Create(CUBE_TYPE eCubeType);

	HRESULT Release();

private:
	

};

#endif