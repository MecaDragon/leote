#include "Camera_Light.h"
#include "KeyManager.h"
#include "DeviceManager.h"
#include "Transform.h"
#include "Management.h"

CCamera_Light::CCamera_Light() : CCamera()
{
}

CCamera_Light::CCamera_Light(const CCamera_Light& rhs) : CCamera(rhs)
{
}

HRESULT CCamera_Light::Initialize_GameObject_Prototype()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	UINT ncbElementBytes = ((sizeof(VS_CB_LIGHT_CAMERA_INFO) + 255) & ~255); //256�� ���
	m_pd3dcbLightCamera = ::CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), pCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

	m_pd3dcbLightCamera->Map(0, NULL, (void**)&m_pcbMappedLightCamera);
	
	m_pd3dcbCamera->SetName(L"Camera_Light Resource");
	return NOERROR;
}

HRESULT CCamera_Light::Initialize_GameObject(void* pArg)
{
	SetOffset(XMFLOAT3(0.0f, 500.0f, -400.0f));
	m_tCameraInfo.xmLook = XMFLOAT3(1.0f, -1.0f, 0.0f);
	m_tCameraInfo.xmRight = XMFLOAT3(0.0f, 0.0f, -1.0f);
	m_tCameraInfo.xmUp = Vector3::CrossProduct(m_tCameraInfo.xmLook, m_tCameraInfo.xmRight, true);
	m_tCameraInfo.xmPosition = XMFLOAT3(0.0f, 0.0f, 0.0f);

	m_pd3dcbCamera->SetName(L"Camera_Light Resource");
	return NOERROR;
}

short CCamera_Light::Update_GameObject(double TimeDelta)
{
	/*
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMFLOAT4X4 T(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, -0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.0f, 1.0f);
	XMFLOAT4X4 xmf4x4ShadowTransform = Matrix4x4::Multiply(m_xmf4x4ViewProjection, T);

	XMStoreFloat4x4(&m_pcbMappedLightCamera->m_xmf4x4ViewProjection, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4ViewProjection)));
	XMStoreFloat4x4(&m_pcbMappedLightCamera->m_xmf4x4ShadowTransform, XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4ShadowTransform)));

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbLightCamera->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(2, d3dGpuVirtualAddress);
	*/

	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	float Radius = 150.f;
	// Only the first "main" light casts a shadow.
	XMVECTOR lightDir = XMLoadFloat3(&CManagement::GetInstance()->GetLight()->GetLights().m_pLights[2].m_xmf3Direction);
	//XMVECTOR lightPos = -2.0f * Radius * lightDir;
	XMVECTOR lightPos = XMLoadFloat3(&m_xmDicrtionPos);
	XMVECTOR targetPos = XMVectorSet(m_xmTargetPos.x, m_xmTargetPos.y, m_xmTargetPos.z, 0.0f);
	XMVECTOR lightUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMMATRIX lightView = XMMatrixLookAtLH(lightPos, targetPos, lightUp);

	// Transform bounding sphere to light space.
	XMFLOAT3 sphereCenterLS;
	XMStoreFloat3(&sphereCenterLS, XMVector3TransformCoord(targetPos, lightView));

	// Ortho frustum in light space encloses scene.
	float l = sphereCenterLS.x - Radius;
	float b = sphereCenterLS.y - Radius;
	float n = sphereCenterLS.z - Radius;
	float r = sphereCenterLS.x + Radius;
	float t = sphereCenterLS.y + Radius;
	float f = sphereCenterLS.z + Radius;

	XMFLOAT4X4 lightProj;
	XMStoreFloat4x4(&lightProj, XMMatrixOrthographicOffCenterLH(l, r, b, t, n, f));

	// Transform NDC space [-1,+1]^2 to texture space [0,1]^2
	XMFLOAT4X4 T(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, -0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.0f, 1.0f);
	XMFLOAT4X4 LV;
	XMStoreFloat4x4(&LV, lightView);
	XMFLOAT4X4 VP = Matrix4x4::Multiply(LV, lightProj);
	XMFLOAT4X4 VPT = Matrix4x4::Multiply(VP, T);

	XMStoreFloat4x4(&m_pcbMappedLightCamera->m_xmf4x4ViewProjection, XMMatrixTranspose(XMLoadFloat4x4(&VP)));
	XMStoreFloat4x4(&m_pcbMappedLightCamera->m_xmf4x4ShadowTransform, XMMatrixTranspose(XMLoadFloat4x4(&VPT)));

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbLightCamera->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(2, d3dGpuVirtualAddress);

	return 0;
}

short CCamera_Light::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CCamera_Light::Render_GameObject()
{
	return NOERROR;
}

CCamera_Light* CCamera_Light::Create()
{
	CCamera_Light* pInstance = new CCamera_Light();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CCamera_Light Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CCamera_Light::Clone_GameObject(void* pArg)
{
	CCamera_Light* pInstance = new CCamera_Light(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CCamera_Light Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CCamera_Light::Release()
{
	CCamera::Release();

	delete this;

	return NOERROR;
}
