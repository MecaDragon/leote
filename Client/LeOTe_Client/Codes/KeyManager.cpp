#include "KeyManager.h"

IMPLEMENT_SINGLETON(CKeyManager)

CKeyManager::CKeyManager()
	:m_dwCurKey(0), m_dwKeyDown(0), m_dwKeyUp(0)
{
}

CKeyManager::~CKeyManager()
{
}

void CKeyManager::UpdateKey()
{
	m_dwCurKey = 0;

	//Mouse
	if (GetAsyncKeyState(VK_LBUTTON) & 0x8000)
		m_dwCurKey |= KEY_LBUTTON;
	if (GetAsyncKeyState(VK_RBUTTON) & 0x8000)
		m_dwCurKey |= KEY_RBUTTON;

	//Keyboard
	if (GetAsyncKeyState(VK_UP) & 0x8000)
		m_dwCurKey |= KEY_UP;
	if (GetAsyncKeyState(VK_DOWN) & 0x8000)
		m_dwCurKey |= KEY_DOWN;
	if (GetAsyncKeyState(VK_LEFT) & 0x8000)
		m_dwCurKey |= KEY_LEFT;
	if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
		m_dwCurKey |= KEY_RIGHT;

	if (GetAsyncKeyState('W') & 0x8000)
		m_dwCurKey |= KEY_W;
	if (GetAsyncKeyState('A') & 0x8000)
		m_dwCurKey |= KEY_A;
	if (GetAsyncKeyState('S') & 0x8000)
		m_dwCurKey |= KEY_S;
	if (GetAsyncKeyState('D') & 0x8000)
		m_dwCurKey |= KEY_D;
	if (GetAsyncKeyState('M') & 0x8000)
		m_dwCurKey |= KEY_M;

	if (GetAsyncKeyState('1') & 0x8000)
		m_dwCurKey |= KEY_1;
	if (GetAsyncKeyState('2') & 0x8000)
		m_dwCurKey |= KEY_2;
	if (GetAsyncKeyState('3') & 0x8000)
		m_dwCurKey |= KEY_3;
	if (GetAsyncKeyState('4') & 0x8000)
		m_dwCurKey |= KEY_4;
	if (GetAsyncKeyState('5') & 0x8000)
		m_dwCurKey |= KEY_5;

	if (GetAsyncKeyState(VK_SPACE) & 0x8000)
		m_dwCurKey |= KEY_SPACE;

	if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
		m_dwCurKey |= KEY_SHIFT;

	if (GetAsyncKeyState(VK_F1) & 0x8000)
		m_dwCurKey |= KEY_F1;

	if (GetAsyncKeyState(VK_RETURN) & 0x8000)
		m_dwCurKey |= KEY_ENTER;
}

bool CKeyManager::KeyDown(DWORD dwCurKey)
{
	if (!(m_dwKeyDown & dwCurKey) && (m_dwCurKey & dwCurKey))
	{
		m_dwKeyDown |= dwCurKey;
		return true;
	}

	//���󺹱�
	else if ((m_dwKeyDown & dwCurKey) && !(m_dwCurKey & dwCurKey))
	{
		m_dwKeyDown ^= dwCurKey;
		return false;
	}

	return false;
}

bool CKeyManager::KeyUp(DWORD dwCurKey)
{
	if ((m_dwKeyUp & dwCurKey) && !(m_dwCurKey & dwCurKey))
	{
		m_dwKeyUp |= dwCurKey;
		return true;
	}

	//���󺹱�
	else if (!(m_dwKeyUp & dwCurKey) && (m_dwCurKey & dwCurKey))
	{
		m_dwKeyUp ^= dwCurKey;
		return false;
	}

	return false;
}

bool CKeyManager::KeyPressing(DWORD dwCurKey)
{
	if (m_dwCurKey & dwCurKey)
		return true;
	return false;
}

bool CKeyManager::KeyCombined(DWORD dwFirstKey, DWORD dwSecondKey)
{
	if (KeyPressing(dwSecondKey) && KeyPressing(dwFirstKey))
		return true;

	return false;
}

HRESULT CKeyManager::Release()
{
	delete this;

	return NOERROR;
}
