#ifndef __MANAGEMENT__
#define __MANAGEMENT__

#include "stdafx.h"
#include "DeviceManager.h"
#include "TimerManager.h"
#include "KeyManager.h"
#include "ObjectManager.h"
#include "ComponentManager.h"
#include "PipeLine.h"
#include "Scene.h"
#include "Light.h"
#include "ServerManager.h"
#include "ShadowMap.h"
#include "CameraManager.h"
#include "DescriptorHeapManager.h"
#include "CollisionManager.h"
#include "SoundManager.h"

class CManagement final
{
	DECLARE_SINGLETON(CManagement)

private:
	explicit CManagement() {};
	virtual ~CManagement();

public:
	HRESULT Initialize_Management(_uint iSceneIndex);
	HRESULT Initialize_Current_Scene(CScene* pScene);

private:
	HRESULT Initialize_Device();
	HRESULT Create_RootSignature();
	void CreateFbxSdkManager();

public:
	short Update_Management(double TimeDelta);
	short LateUpdate_Management(double TimeDelta);
	HRESULT Render_Shadow();
	HRESULT Render_Management();

	HRESULT Clear_Scene(_uint SceneID);
	HRESULT Release_Mangers();

	HRESULT Release();

public: // ObjectManager
	HRESULT Add_Prototype_GameObj(const wchar_t* pPrototypeTag, CGameObject* pPrototype);
	HRESULT Add_GameObjectToLayer(_uint iSceneIndex, const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, CGameObject** ppOutput, void* pArg = nullptr);

public: // ComponentManager
	HRESULT Add_Prototype_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent* pPrototype);
	HRESULT Clone_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent** ppComponent, void* pArg = nullptr);

public: // PipeLine
	void Set_ViewMatrix(const XMFLOAT4X4& Matrix) { m_pPipeLine->Set_ViewMatrix(Matrix); }
	void Set_ProjectionMatrix(PROJECTION_TYPE eProjType) { m_pPipeLine->Set_ProjectionMatrix(eProjType); }
	void Set_ScissorRect(const D3D12_RECT& ScissorRect) { m_pPipeLine->Set_ScissorRect(ScissorRect); }
	void Set_Viewport(const D3D12_VIEWPORT& Viewport) { m_pPipeLine->Set_Viewport(Viewport); }
	void Set_MappedCamera(VS_CB_CAMERA_INFO * pMappedCamera) { m_pPipeLine->Set_MappedCamera(pMappedCamera); }
	void Set_Position(const XMFLOAT3& position) { m_pPipeLine->Set_Position(position); }
	void Set_CameraResource(ID3D12Resource* pd3dcbCamera) { m_pPipeLine->Set_CameraResource(pd3dcbCamera); }

	XMFLOAT4X4 Get_ViewMatrix() { return m_pPipeLine->Get_ViewMatrix(); }
	XMFLOAT4X4 Get_ViewInverseMatrix() { return m_pPipeLine->Get_ViewInverseMatrix(); }
	XMFLOAT4X4 Get_ProjectionMatrix() { return m_pPipeLine->Get_ProjectionMatrix(); }

public: // Light
	HRESULT Create_Light_Material();
	HRESULT Create_ShadowMap();
	CLight* GetLight() { return m_pLight; }

public: // RootSignaure
	ID3D12RootSignature* Get_RootSignature() { return m_pRootSignature; }
	FbxManager* Get_RootSdkMananger() { return m_pfbxSdkManager; }

	//CurrentScene
	SCENEID Get_CurrentSceneId() { return m_eCurrentId; }
	void Set_CurrentSceneId(SCENEID eSceneId) { m_eCurrentId = eSceneId; }
	CShadowMap* GetShadowMap() { return m_pShadowMap; }

public: // input Id
	void Set_InputID(string inputID) { m_strInputId = inputID; }
	string Get_InputID() { return m_strInputId; }

	SCENEID Get_LoadingSceneId() { return m_eLoadingScene; }
	void Set_LoadingSceneId(SCENEID eSceneId) { m_eLoadingScene = eSceneId; }

private:
	CDeviceManager* m_pDeviceManager = nullptr;
	CObjectManager* m_pObjectManager = nullptr;
	CComponentManager* m_pComponentManager = nullptr;
	CServerManager* m_pServerManager = nullptr;
	CCameraManager* m_pCameraManager = nullptr;
	CDescriptorHeapManager* m_pDescriptorHeapManager = nullptr;
	CCollisionManager* m_pCollisionManager = nullptr;
	CSoundManager* m_pSoundManager = nullptr;
	CPipeLine* m_pPipeLine = nullptr;
	CScene* m_pCurrent_Scene = nullptr;

	ID3D12RootSignature* m_pRootSignature = nullptr;
	FbxManager* m_pfbxSdkManager = nullptr;

	CLight* m_pLight = nullptr;

	SCENEID m_eCurrentId = SCENE_LOGO;

	CShadowMap* m_pShadowMap = nullptr;

	string m_strInputId = " ";

	SCENEID	m_eLoadingScene = SCENE_LOGO;
};

#endif // !__MANAGEMENT__

