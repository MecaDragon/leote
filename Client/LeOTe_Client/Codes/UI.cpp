#include "UI.h"
#include "Shader.h"
#include "Transform.h"
#include "Management.h"
#include "Plane_Buffer.h"
#include "Texture.h"
#include "Renderer.h"

CUI::CUI()
{
}

CUI::CUI(const CUI& rhs)
	: m_SceneName(rhs.m_SceneName)
	, m_TexComName(rhs.m_TexComName)
	, m_ShaderComName(rhs.m_ShaderComName)
{
}

HRESULT CUI::Initialize_GameObject_Prototype(SCENEID SceneName, wstring TexComName, wstring ShaderComName)
{
	m_SceneName = SceneName;
	m_TexComName = TexComName;
	m_ShaderComName = ShaderComName;

	return NOERROR;
}

HRESULT CUI::Initialize_GameObject(void* pArg)
{
	ID3D12Device* pDevice = CDeviceManager::GetInstance()->Get_Device();

	if(FAILED(Add_Component()))
		return E_FAIL;
// map the resource heap to get a gpu virtual address to the beginning of the heap
	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
	}
	m_pGameObjResource->SetName(L"UI Object m_pGameObjResource");
	return NOERROR;
}

short CUI::Update_GameObject(double TimeDelta)
{
	if (m_bTimerHide)
	{
		m_fTimer += TimeDelta;
		m_pTransformCom->SetPosition(m_pTransformCom->GetPosition().x, m_pTransformCom->GetPosition().y + 0.01, m_pTransformCom->GetPosition().z);
		if (m_fTimer > m_fTimerHide)
		{
			m_bHide = true;
			m_fTimer = 0.f;
			m_fTimerHide = 0.f;
			m_bTimerHide = false;
		}
	}

	if (m_bDead == true)
		return -1;

	return 0;
}

short CUI::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return 0;
}

HRESULT CUI::Render_GameObject()
{
	if (m_bHide == false)
	{
		ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
		m_pShaderCom->SetPipeline(pCommandList);
		if (m_iTexindex == 0)
		{
			m_pTextureCom->UpdateShaderVariables(pCommandList);
		}
		else
		{
			m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_DIFFUSE, m_iTexindex);
		}

		m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform);

		D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
		pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

		m_pPlane_BufferCom->Render_Buffer();
	}
	return NOERROR;
}

CGameObject* CUI::Clone_GameObject(void* pArg)
{
	CUI* pInstance = new CUI(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CUI Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CUI* CUI::Create(SCENEID SceneName, wstring TexComName, wstring ShaderComName)
{
	CUI* pInstance = new CUI();

	if (pInstance->Initialize_GameObject_Prototype(SceneName, TexComName, ShaderComName))
	{
		MSG_BOX("CUI Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CUI::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pPlane_BufferCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}

HRESULT CUI::SetTexture(const wchar_t *texturename, const wchar_t* componentname)
{
	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(m_SceneName, texturename, componentname, (CComponent**)&m_pTextureCom)))
		return E_FAIL;
	return NOERROR;
}

void CUI::SetTimerUI(float Time, XMFLOAT3 position)
{
	m_pTransformCom->SetPosition(position);
	m_bHide = false;
	m_fTimer = 0.f;
	m_fTimerHide = Time;
	m_bTimerHide = true;
}

void CUI::SetScale(XMFLOAT3 scale)
{
	m_pTransformCom->SetScale(scale.x, scale.y, scale.z);
}

HRESULT CUI::Add_Component()
{

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, m_ShaderComName.c_str(), L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_PlaneBuffer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Plane_Buffer", L"Com_Plane_Buffer", (CComponent**)&m_pPlane_BufferCom)))
		return E_FAIL;
	
	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(m_SceneName, m_TexComName.c_str(), L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}
