#ifndef __DESCRIPTORHEAPMANAGER__
#define __DESCRIPTORHEAPMANAGER__

#include "stdafx.h"
#include "Defines.h"

class CDescriptorHeapManager final
{
	DECLARE_SINGLETON(CDescriptorHeapManager)

public:
	explicit CDescriptorHeapManager();
	virtual ~CDescriptorHeapManager();

public:
	HRESULT Create_DescriptorHeap(_uint nDescriptor, ID3D12Device* pDevice);
	HRESULT Create_DsvDescriptorHeap(_uint nDescriptor, ID3D12Device* pDevice);
	HRESULT Release();

	ID3D12DescriptorHeap* get_srvDescriptorHeap() { return m_pSrvDescriptorHeap; }
	ID3D12DescriptorHeap* get_dsvDescriptorHeap() { return m_pDsvDescriptorHeap; }

	CD3DX12_CPU_DESCRIPTOR_HANDLE get_CpuDescriptorHandle() { return m_CpuDescriptorHandle; }
	CD3DX12_GPU_DESCRIPTOR_HANDLE get_GpuDescriptorHandle() { return m_GpuDescriptorHandle; }

	CD3DX12_CPU_DESCRIPTOR_HANDLE get_CpuDsvDescriptorHandle() { return m_CpuDsvDescriptorHandle; }

	void set_CpuDescriptorHandle(CD3DX12_CPU_DESCRIPTOR_HANDLE handle) { m_CpuDescriptorHandle = handle; }
	void set_GpuDescriptorHandle(CD3DX12_GPU_DESCRIPTOR_HANDLE handle) { m_GpuDescriptorHandle = handle; }
	void set_CpuDsvDescriptorHandle(CD3DX12_CPU_DESCRIPTOR_HANDLE handle) { m_CpuDsvDescriptorHandle = handle; }

	void reset_CpuDescriptorHandle() { m_CpuDescriptorHandle = m_pSrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(); }
	void reset_GpuDescriptorHandle() { m_GpuDescriptorHandle = m_pSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart(); }
	void reset_CpuDsvDescriptorHandle() { m_CpuDsvDescriptorHandle = m_pDsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(); }

private:
	ID3D12DescriptorHeap* m_pSrvDescriptorHeap = nullptr;
	ID3D12DescriptorHeap* m_pDsvDescriptorHeap = nullptr;

	CD3DX12_CPU_DESCRIPTOR_HANDLE m_CpuDescriptorHandle;
	CD3DX12_GPU_DESCRIPTOR_HANDLE m_GpuDescriptorHandle;

	CD3DX12_CPU_DESCRIPTOR_HANDLE m_CpuDsvDescriptorHandle;
};

#endif