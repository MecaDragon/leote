#ifndef __CAMERA__
#define __CAMERA__

#include "stdafx.h"
#include "Defines.h"
#include "GameObject.h"

class CPipeLine;
class CTransform;
class CShader;
class CCamera : public CGameObject
{
public:
	typedef struct tagCamerainfo
	{
		XMFLOAT3 xmPosition = { 0.f, 0.f, 0.f };
		XMFLOAT3 xmRight = { 1.f, 0.f, 0.f };
		XMFLOAT3 xmUp = { 0.f, 1.f, 0.f };
		XMFLOAT3 xmLook = { 0.f, 0.f, 1.f };

		XMFLOAT3 xmLookAt = { 0.f, 0.f, 0.f };

		float fFovY = 60.f;
		float fAspect = (float)WINDOWX/WINDOWY;
		float fNear = 0.2f;
		float fFar = 50000.f;

	}CAMERA_INFO;

protected:
	explicit CCamera();
	explicit CCamera(const CCamera& rhs);
	virtual ~CCamera();

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	virtual CGameObject* Clone_GameObject(void* pArg) = 0;

protected:
	HRESULT Add_Component();

	HRESULT Set_ViewMatrix(const XMFLOAT4X4& matView);
	HRESULT Set_ProjectionMatrix(PROJECTION_TYPE eProjType);
	HRESULT Set_PerspectiveProjMatrix(const XMFLOAT4X4& Matrix);
	HRESULT Set_OrthographicProjMatrix(const XMFLOAT4X4& Matrix);
	HRESULT Set_ScissorRect(const D3D12_RECT& ScissorRect);
	HRESULT Set_Viewport(const D3D12_VIEWPORT& Viewport);
	HRESULT Set_MappedCamera(VS_CB_CAMERA_INFO* pMappedCamera);
	HRESULT Set_Position(const XMFLOAT3& position);
	HRESULT Set_CameraResource(ID3D12Resource* pd3dcbCamera);

	XMFLOAT4X4 Set_PerspectiveFovLH();
	XMFLOAT4X4 Set_OrthogrphicLH();

	void CreateShaderVariables();

public:
	HRESULT Release();
	void LookAt(const XMFLOAT3& pos, const XMFLOAT3& target, const XMFLOAT3& up);
	void LookAt(FXMVECTOR pos, FXMVECTOR target, FXMVECTOR worldUp);
	void UpdateViewMatrix();
	void GenerateViewMatrix();
	void GenerateFrustum();
	BoundingFrustum GetFrustum() { m_xmFrustum; };
	bool IsInFrustum(BoundingBox& xmBoundingBox);

	CAMERA_INFO get_CameraInfo() { return m_tCameraInfo; }

private:
	CPipeLine* m_pPipeLine = nullptr;

protected:
	CTransform* m_pTransformCom = nullptr;
	CShader* m_pShaderCom = nullptr;

	ID3D12Resource* m_pd3dcbCamera = nullptr;
	VS_CB_CAMERA_INFO* m_pcbMappedCamera = nullptr;

	D3D12_VIEWPORT	m_Viewport;
	D3D12_RECT m_ScissorRect;

	CAMERA_INFO m_tCameraInfo;

	XMFLOAT4X4 m_matView;
	XMFLOAT4X4 m_PerspectiveProjMatrix;
	XMFLOAT4X4 m_OrthographicProjMatrix;
	XMFLOAT4X4 m_xmf4x4ViewProjection;

	BoundingFrustum m_xmFrustum;
};

#endif