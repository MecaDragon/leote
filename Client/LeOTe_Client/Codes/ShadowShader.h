#ifndef __SHADOWSHADER__
#define __SHADOWSHADER__

#include "Shader.h"

class CShadowShader final : public CShader
{
private:
	explicit CShadowShader();
	explicit CShadowShader(const CShadowShader& rhs);
	virtual ~CShadowShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CShadowShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_BLEND_DESC Create_BlendState();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();
	virtual D3D12_DEPTH_STENCIL_DESC Create_DepthStencilState();
	virtual ID3D12PipelineState* Create_PipeLineState();

};

#endif
