#include "stdafx.h"
#include "Defines.h"
#include "GameObject.h"
#include "TeddyBear.h"
#include "Camera.h"

#define MAX_DUST 3

enum ANIMSTATE { ANIM_IDLE, ANIM_WALK, ANIM_JUMP, ANIM_RUN, ANIM_FREEFALL, ANIM_STANDUP, ANIM_SIT, ANIM_SAD, ANIM_FALL, ANIM_END };

class CPipeLine;
class CTransform;
class CShader;
class CPhysic;
class CCamera;
class CStaticObject;
class CUI;

class CPlayer : public CGameObject
{
public:
	typedef struct Playerinfo
	{
		XMFLOAT3 xmPosition = { -5.479, 14.2574, 2.47423 };
		XMFLOAT3 xmRight = { 1.f, 0.f, 0.f };
		XMFLOAT3 xmUp = { 0.f, 1.f, 0.f };
		XMFLOAT3 xmLook = { 0.f, 0.f, 1.f };
		string m_clipName = "run";
	}PLAYER_INFO;
	float           			m_fPitch = 0.0f;
	float           			m_fYaw = 0.0f;
	float           			m_fRoll = 0.0f;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0); // 이동량
	char m_AnimState = ANIM_IDLE; // 애니메이션 상태

	//보드맵 관련
	int bCoin;		// 플레이어가 가지고 있는 코인 갯수
	int bButton;	// 플레이어가 가지고 있는 단추 갯수
	int bRank;		// 플레이어 현재 순위

protected:
	explicit CPlayer();
	explicit CPlayer(const CPlayer& rhs);
	virtual ~CPlayer();

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	static CPlayer* Create();
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	void SetCamera(CCamera* m_Camera);
	void SetObject(CTeddyBear* m_Object);
	void Rotate(float x, float y, float z);
	void OnPrepareRender();

	XMFLOAT3 GetPosition() { return(m_tPlayerInfo.xmPosition); }
	XMFLOAT3 GetLookVector() { return(m_tPlayerInfo.xmLook); }
	XMFLOAT3 GetUpVector() { return(m_tPlayerInfo.xmUp); }
	XMFLOAT3 GetRightVector() { return(m_tPlayerInfo.xmRight); }

	bool GetDeath() { return m_bDeath; }
	void SetDeath(bool death) { m_bDeath = death; }

	void Set_ClientIndex(int iIndex) { m_iClientIndex = iIndex; }
	int Get_ClientIndex() { return m_iClientIndex; }

	void Set_Name(string strName) { m_strName = strName; }
	string Get_Name() { return m_strName; }

	void SetLerpStart(packet_move p);

	void Lerping_Position(double TimeDelta);
	void SetAnimation(std::string ClipName);
	void SetAnimationState(char AnimState);

	CTeddyBear* Get_TeddyBear() { return m_pObject; }
	bool IsPlayerAnimEnd();
	bool IsPlayerAnimMiddle();

	void GivePlayerForce(float x, float y, float z);
	void GivePlayerForceX(float x);
	void GivePlayerForceY(float y);
	void GivePlayerForceZ(float z);
	void SetPlayerForceX(float x);
	void SetPlayerForceY(float y);
	void SetPlayerForceZ(float z);
	void SetPlayerForce(float x, float y, float z);
	XMFLOAT3 GetPlayerForce() { return m_xmForce; }

	void SetPlayerAcc(float acc) { m_fAcc = acc; }
	void GivePlayerAcc(float acc);
	float GetPlayerAcc() { return m_fAcc; }

	void SetIsPlayer(bool isPlayer) { m_bIsPlayer = isPlayer; }
	bool GetIsPlayer() { return m_bIsPlayer; }

	void SetPlayerOrder(int order) { m_playerorder = order; }
	const int& GetPlayerOrder() { return m_playerorder; }

	void Set_CurrentPlatformIndex(int idx) { m_iCurrentPlatformIndex = idx; }
	int Get_CurrentPlatformIndex() { return m_iCurrentPlatformIndex; }

	void Set_CurrentPlatformPos(XMFLOAT3 pos) { m_xmCurrentPlatformPos = pos; }
	XMFLOAT3 Get_CurrentPlatformPos() { return m_xmCurrentPlatformPos; }

	void Set_BeforeLookVector(XMFLOAT3 look) { m_xmBeforeLookVector = look; }
	XMFLOAT3 Get_BeforeLookVector() { return m_xmBeforeLookVector; }

	void SetScale(XMFLOAT3 scale) { m_xmScale = scale; }
	XMFLOAT3 GetScale() { return m_xmScale; }
	void GiveScale(float scale) {
		m_xmScale.x += scale;
		m_xmScale.y += scale;
		m_xmScale.z += scale;
	}

	void SetBig(bool flag);
	void SetSmall(bool flag);
	void ScaleChange(double TimeDelta);

	void Set_Lerp(bool l) { lerping = l; }

	void SetController(bool control) { m_bControll = control; }
	bool GetController() { return m_bControll; }

	HRESULT Release();

	//onemindmap 역할
	void Set_OneMindRoll(int roll) { m_cOnemindRoll = roll; }
	char Get_OneMindRoll() { return m_cOnemindRoll; }

	void Set_IsSanta(bool isSanta) { m_bSanta = isSanta; }
	bool Get_IsSanta() { return m_bSanta; }

	void SetSkinid(int skin);
	int GetSkinid() { return m_iSkinid; }

	void SetWinner(bool win) { m_bIsWinner = win; }
	bool GetWinner() { return m_bIsWinner; }

	void SetSmokeObject(CStaticObject* smoke[]);

	void setPlayerIndex(_uint nPlayerIndex) { m_nPlayerIndex = nPlayerIndex; }
	_uint getPlayerIndex() { return m_nPlayerIndex; }

protected:
	HRESULT Add_Component();

	void CreateShaderVariables();

protected:
	CTeddyBear* m_pObject = nullptr;
	CCamera* m_pCamera = nullptr;

	int m_iClientIndex;
	string m_strName;

	//위치 보간
	float LerpTimeDelta = 0.f;
	bool lerping = false;

	XMFLOAT3 StartPos;
	XMFLOAT3 StartLook;

	XMFLOAT3 DestPos;
	XMFLOAT3 DestLook;

	int m_playerorder;

	int m_iCurrentPlatformIndex = 0;
	XMFLOAT3 m_xmCurrentPlatformPos;
	XMFLOAT3 m_xmBeforeLookVector;

public:
	PLAYER_INFO m_tPlayerInfo;
	CPhysic* m_pPhysicCom = nullptr;
private:
	CTransform* m_pTransformCom = nullptr;
	XMFLOAT3 m_xmForce;
	XMFLOAT3 m_xmScale;
	bool m_bDeath = false;
	float m_fAcc;

	bool m_bIsWinner = false;

	bool m_bIsPlayer = false;

	bool m_bBig = false;
	bool m_bSmall = false;
	bool m_bControll = true;
	float scaleTime = 0.f;

	int m_cOnemindRoll = -1; //한마음맵 역할
	bool m_bSanta = false; //한마음맵 산타곰돌이인지 여부

	int m_iSkinid = 0;

	CStaticObject* m_SmokeObejct[MAX_DUST] = { nullptr, };

	double m_dSmokeTime[MAX_DUST] = { 0.f, };

	double m_dTimeDelta;
	
	_uint m_nPlayerIndex;
};