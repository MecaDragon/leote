#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CTransform;
class CMesh;
class CTexture;
class CRenderer;
class CShadowShader;
class CPlatformManager;
class CButton;

class CBalloon final : public CGameObject
{
private:
	explicit CBalloon();
	explicit CBalloon(const CBalloon& rhs);
	virtual ~CBalloon() = default;
public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);

	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;

	FbxScene* m_pfbxScene = NULL;
	std::unique_ptr<SkinnedModelInstance> mSkinnedModelInst;
	std::vector<FBXVertex> outSkinnedVertices;
	std::vector<std::uint32_t> outIndices;
	std::vector<Material> outMaterial;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	ID3D12Resource* m_pd3dVertexBuffer = NULL;
	ID3D12Resource* m_pd3dVertexUploadBuffer = NULL;

	ID3D12Resource* m_pd3dIndexBuffer = NULL;
	ID3D12Resource* m_pd3dIndexUploadBuffer = NULL;

	D3D12_VERTEX_BUFFER_VIEW		m_d3dVertexBufferView;
	D3D12_INDEX_BUFFER_VIEW			m_d3dIndexBufferView;

	D3D12_PRIMITIVE_TOPOLOGY		m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT							m_nSlot = 0;
	UINT							m_nVertices = 0;
	UINT							m_nStride = 0;
	UINT							m_nOffset = 0;

	UINT							m_nIndices = 0;
	UINT							m_nStartIndex = 0;
	int								m_nBaseVertex = 0;

	float	m_fTimer = 0.f;
	float	m_fSpeed = 0.3f;
	int		m_nUpDown = 1;

	CPlatformManager* m_pPlatformManager = nullptr;

	_uint	m_nCurrentIndex = 1;
	_uint	m_nNextIndex = 2;
	_uint	m_nDestIndex = 1;

	XMFLOAT3 m_xmLook = XMFLOAT3(0.f, 0.f, 1.f);

	CButton* m_pButton = nullptr;


private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.

	void moveToDest(double TimeDelta);

public:
	static CBalloon* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();

	void setDestIndex(_uint nDestIndex) { m_nDestIndex = nDestIndex; }

	void setButton(CButton* pButton) { m_pButton = pButton; }
};
