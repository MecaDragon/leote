#ifndef __SPHERECOLLIDER__
#define __SPHERECOLLIDER__

#include "stdafx.h"
#include "Collider.h"

class CSphereCollider : public CCollider
{
private:
	explicit CSphereCollider();
	explicit CSphereCollider(const CSphereCollider& rhs);
	virtual ~CSphereCollider(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);

	static CSphereCollider* Create();

	HRESULT Release();

	virtual CGameObject* get_GameObj() { return m_pTargetObj; };
	virtual float get_radius() { return m_fRadius; };

private:


};

#endif
