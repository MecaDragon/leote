#include "ComponentManager.h"

IMPLEMENT_SINGLETON(CComponentManager)

CComponentManager::CComponentManager()
{
}

CComponentManager::~CComponentManager()
{
}

HRESULT CComponentManager::Initialize_ComponentManager(_uint iNumScenes)
{
	if (nullptr != m_pMapPrototypes)
		return E_FAIL;
	m_pMapPrototypes = new PROTOTYPES[iNumScenes];

	m_iNumScenes = iNumScenes;

	return NOERROR;
}

HRESULT CComponentManager::Add_Prototype_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent* pPrototype)
{
	if (nullptr == pPrototype ||
		nullptr != Find_Prototype(iSceneIndex, pPrototypeTag))
		return E_FAIL;
		
	m_pMapPrototypes[iSceneIndex].insert(PROTOTYPES::value_type(pPrototypeTag, pPrototype));
		
	return NOERROR;
}

HRESULT CComponentManager::Clone_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent** ppComponent, void* pArg)
{
	CComponent* pPrototype = Find_Prototype(iSceneIndex, pPrototypeTag);
	if (nullptr == pPrototype)
		return E_FAIL;

	CComponent* pComponent = pPrototype->Clone_Component(pArg);
	if (nullptr == pComponent)
		return E_FAIL;

	*ppComponent = pComponent;

	return NOERROR;
}

HRESULT CComponentManager::Clear_ComponentManager(_uint iSceneIndex)
{
	if (m_iNumScenes <= iSceneIndex || nullptr == m_pMapPrototypes)
		return E_FAIL;

	for (auto& pair : m_pMapPrototypes[iSceneIndex])
		Safe_Release(pair.second);

	m_pMapPrototypes[iSceneIndex].clear();

	return NOERROR;
}

CComponent* CComponentManager::Find_Prototype(_uint iSceneIndex, const wchar_t* pPrototypeTag)
{
	auto iter = find_if(m_pMapPrototypes[iSceneIndex].begin(), m_pMapPrototypes[iSceneIndex].end(), CTag_Finder(pPrototypeTag));
	
	if (iter == m_pMapPrototypes[iSceneIndex].end())
		return nullptr;
	
	return iter->second;
}

HRESULT CComponentManager::Release()
{
	// scene마다 component 하나라도 추가한 뒤 주석 풀기
	for (size_t i = 0; i < m_iNumScenes; ++i)
	{
		for (auto& pair : m_pMapPrototypes[i])
			Safe_Release(pair.second);

		m_pMapPrototypes[i].clear();
	}

	delete[] m_pMapPrototypes;
	m_pMapPrototypes = nullptr;

	return NOERROR;
}
