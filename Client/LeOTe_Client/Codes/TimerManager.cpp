#include "TimerManager.h"

IMPLEMENT_SINGLETON(CTimerManager)

CTimerManager::~CTimerManager()
{
}

double CTimerManager::GetTimeDelta(const wchar_t* pTimerTag)
{
	CTimer* pInstance = Find_Timer(pTimerTag);

	if (nullptr == pInstance)
		return 0.0;

	return pInstance->Get_TimeDelta();
}

HRESULT CTimerManager::Initialize_Timer(const wchar_t* pTimerTag)
{
	CTimer* pInstance = Find_Timer(pTimerTag);

	if (nullptr != pInstance)
		return E_FAIL;

	pInstance = CTimer::Create();

	if (nullptr == pInstance)
		return E_FAIL;

	m_mapTimer.emplace(pTimerTag, pInstance);

	return S_OK;
}

CTimer* CTimerManager::Find_Timer(const wchar_t* pTimerTag)
{
	auto iter = find_if(m_mapTimer.begin(), m_mapTimer.end(), CTag_Finder(pTimerTag));

	if (iter == m_mapTimer.end())
		return nullptr;

	return iter->second;
}

HRESULT CTimerManager::Release()
{
	for (auto& pair : m_mapTimer)
		Safe_Release(pair.second);

	m_mapTimer.clear();

	return NOERROR;
}
