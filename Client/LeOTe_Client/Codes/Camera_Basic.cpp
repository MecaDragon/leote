#include "Camera_Basic.h"
#include "KeyManager.h"
#include "DeviceManager.h"
#include "Transform.h"

CCamera_Basic::CCamera_Basic() : CCamera()
{
}

CCamera_Basic::CCamera_Basic(const CCamera_Basic& rhs) : CCamera(rhs)
{
}

HRESULT CCamera_Basic::Initialize_GameObject_Prototype()
{
	m_pd3dcbCamera->SetName(L"Camera_Basic Resource");
	return NOERROR;
}

HRESULT CCamera_Basic::Initialize_GameObject(void* pArg)
{
	m_pd3dcbCamera->SetName(L"Camera_Basic Resource");
	return NOERROR;
}

short CCamera_Basic::Update_GameObject(double TimeDelta)
{
	GenerateFrustum();
	return 0;
}

short CCamera_Basic::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CCamera_Basic::Render_GameObject()
{
	return NOERROR;
}

CCamera_Basic* CCamera_Basic::Create()
{
	CCamera_Basic* pInstance = new CCamera_Basic();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CCamera_Basic Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CCamera_Basic::Clone_GameObject(void* pArg)
{
	CCamera_Basic* pInstance = new CCamera_Basic(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CCamera_Basic Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CCamera_Basic::Release()
{
	CCamera::Release();

	delete this;

	return NOERROR;
}
