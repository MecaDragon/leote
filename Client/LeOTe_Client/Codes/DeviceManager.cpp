#include "DeviceManager.h"
#include "DescriptorHeapManager.h"
IMPLEMENT_SINGLETON(CDeviceManager)

CDeviceManager::CDeviceManager()
{
	for (int i = 0; i < m_nSwapChainBuffers; ++i)
		m_pd3dSwapChainBackBuffers[i] = NULL;

	for (int i = 0; i < m_nSwapChainBuffers; ++i)
		m_nFenceValues[i] = 0;

}

CDeviceManager::~CDeviceManager()
{
}

HRESULT CDeviceManager::Initialize_Device()
{
	Create_Device();
	Create_CommandQueueAndList();
	CDescriptorHeapManager::GetInstance()->Create_DescriptorHeap(500, Get_Device());
	CDescriptorHeapManager::GetInstance()->Create_DsvDescriptorHeap(2, Get_Device());
	Create_RtvAndDsvDescriptorHeaps();
	Create_SwapChain();
	Create_DepthStencilView();

	//CoInitialize(NULL);

	return NOERROR;
}

HRESULT CDeviceManager::Create_Device()
{
	UINT nDXGIFactoryFlags = 0;

#if defined(_DEBUG)
	ID3D12Debug* pd3dDebugController = NULL;
	if (FAILED(D3D12GetDebugInterface(_uuidof(ID3D12Debug), (void**)&pd3dDebugController)))
	{
		MSG_BOX("D3D12GetDebugInterFace Failed");
		return E_FAIL;
	}

	pd3dDebugController->EnableDebugLayer();
	pd3dDebugController->Release();

	nDXGIFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
#endif

	if (FAILED(CreateDXGIFactory2(nDXGIFactoryFlags, __uuidof(IDXGIFactory4), (void**)&m_pdxgiFactory)))
	{
		MSG_BOX("CreateDXGIFactory2 Failed");
			return E_FAIL;
	}

	IDXGIAdapter1* pd3dAdapter = NULL;

	for (UINT i = 0; DXGI_ERROR_NOT_FOUND != m_pdxgiFactory->EnumAdapters1(i, &pd3dAdapter); ++i)
	{
		DXGI_ADAPTER_DESC1 dxgiAdapterDesc;
		pd3dAdapter->GetDesc1(&dxgiAdapterDesc);

		if (dxgiAdapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			continue;

		if (SUCCEEDED(D3D12CreateDevice(pd3dAdapter, D3D_FEATURE_LEVEL_12_0, _uuidof(ID3D12Device), (void**)&m_pd3dDevice)))
			break;
	}

	if (!pd3dAdapter)
	{
		m_pdxgiFactory->EnumWarpAdapter(_uuidof(IDXGIFactory4), (void**)pd3dAdapter);
		if (FAILED(D3D12CreateDevice(pd3dAdapter, D3D_FEATURE_LEVEL_12_0, _uuidof(ID3D12Device), (void**)&m_pd3dDevice))) {
			MSG_BOX("WARP Adapter CreateDevice Failed");
			return E_FAIL;
		}
	}

	g_CbvSrvDescriptorIncrementSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	g_RtvDescriptorIncrementSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	g_DsvDescriptorIncrementSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS d3dMsaaQualityLevels;
	d3dMsaaQualityLevels.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	d3dMsaaQualityLevels.SampleCount = 4;
	d3dMsaaQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	d3dMsaaQualityLevels.NumQualityLevels = 0;
	if (FAILED(m_pd3dDevice->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS, &d3dMsaaQualityLevels, sizeof(D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS))))
	{
		MSG_BOX("CheckFeatureSupport Failed");
		return E_FAIL;
	}

	m_nMsaa4xQualityLevels = d3dMsaaQualityLevels.NumQualityLevels;
	m_bMsaa4xEnable = (m_nMsaa4xQualityLevels > 1) ? true : false;

	if (FAILED(m_pd3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12Fence), (void**)&m_pd3dFence)))
	{
		MSG_BOX("CreateFence Failed");
		return E_FAIL;
	}

	m_hFenceEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	if (pd3dAdapter)
		pd3dAdapter->Release();

	return NOERROR;
}

HRESULT CDeviceManager::Create_CommandQueueAndList()
{
	D3D12_COMMAND_QUEUE_DESC d3dCommandQueueDesc;
	ZeroMemory(&d3dCommandQueueDesc, sizeof(D3D12_COMMAND_QUEUE_DESC));
	d3dCommandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	d3dCommandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	
	if (FAILED(m_pd3dDevice->CreateCommandQueue(&d3dCommandQueueDesc, __uuidof(ID3D12CommandQueue), (void**)&m_pd3dCommandQueue)))
	{
		MSG_BOX("CreateCommandQueue Failed");
		return E_FAIL;
	}

	for (int i = 0; i < COMMAND_END; i++)
	{
		if (FAILED(m_pd3dDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void**)&m_pd3dCommandAllocator[i])))
		{
			MSG_BOX("CreateCommandAllocator Failed");
			return E_FAIL;
		}
		if (FAILED(m_pd3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_pd3dCommandAllocator[i], NULL, __uuidof(ID3D12CommandList), (void**)&m_pd3dCommandList[i])))
		{
			MSG_BOX("CreateCommandList Failed");
			return E_FAIL;
		}

		if (FAILED(m_pd3dCommandList[i]->Close()))
		{
			MSG_BOX("m_pd3dCommandList->Close() Failed");
			return E_FAIL;
		}
	}

	return NOERROR;
}

HRESULT CDeviceManager::Create_RtvAndDsvDescriptorHeaps()
{
	D3D12_DESCRIPTOR_HEAP_DESC d3dDesCriptorHeapDesc;
	ZeroMemory(&d3dDesCriptorHeapDesc, sizeof(D3D12_DESCRIPTOR_HEAP_DESC));

	d3dDesCriptorHeapDesc.NumDescriptors = m_nSwapChainBuffers;
	d3dDesCriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	d3dDesCriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	d3dDesCriptorHeapDesc.NodeMask = 0;
	if (FAILED(m_pd3dDevice->CreateDescriptorHeap(&d3dDesCriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void**)&m_pd3dRtvDescriptorHeap)))
	{
		MSG_BOX("CreateDescriptorHeap m_pd3dRtvDesctriptorHeap Failed");
		return E_FAIL;
	}
	return NOERROR;
}

HRESULT CDeviceManager::Create_SwapChain()
{
	RECT rcClient;
	::GetClientRect(g_hWnd, &rcClient);
	m_nClientWidth = rcClient.right - rcClient.left;
	m_nClientHeight = rcClient.bottom - rcClient.top;

#ifdef _WITH_CREATE_SWAPCHAIN_FOR_HWND
	DXGI_SWAP_CHAIN_DESC1 dxgiSwapChainDesc;
	::ZeroMemory(&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC1));
	dxgiSwapChainDesc.Width = m_nClientWidth;
	dxgiSwapChainDesc.Height = m_nClientHeight;
	dxgiSwapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.SampleDesc.Count = (m_bMsaa4xEnable) ? 4 : 1;
	dxgiSwapChainDesc.SampleDesc.Quality = (m_bMsaa4xEnable) ? (m_nMsaa4xQualityLevels - 1) : 0;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.BufferCount = m_nSwapChainBuffers;
	dxgiSwapChainDesc.Scaling = DXGI_SCALING_NONE;
	dxgiSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	dxgiSwapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	dxgiSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	DXGI_SWAP_CHAIN_FULLSCREEN_DESC dxgiSwapChainFullScreenDesc;
	::ZeroMemory(&dxgiSwapChainFullScreenDesc, sizeof(DXGI_SWAP_CHAIN_FULLSCREEN_DESC));
	dxgiSwapChainFullScreenDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainFullScreenDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainFullScreenDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	dxgiSwapChainFullScreenDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	dxgiSwapChainFullScreenDesc.Windowed = TRUE;

	HRESULT hResult = m_pdxgiFactory->CreateSwapChainForHwnd(m_pd3dCommandQueue, m_hWnd, &dxgiSwapChainDesc, &dxgiSwapChainFullScreenDesc, NULL, (IDXGISwapChain1**)&m_pdxgiSwapChain);
#else
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	::ZeroMemory(&dxgiSwapChainDesc, sizeof(dxgiSwapChainDesc));
	dxgiSwapChainDesc.BufferCount = m_nSwapChainBuffers;
	dxgiSwapChainDesc.BufferDesc.Width = m_nClientWidth;
	dxgiSwapChainDesc.BufferDesc.Height = m_nClientHeight;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	dxgiSwapChainDesc.OutputWindow = g_hWnd;
	dxgiSwapChainDesc.SampleDesc.Count = (m_bMsaa4xEnable) ? 4 : 1;
	dxgiSwapChainDesc.SampleDesc.Quality = (m_bMsaa4xEnable) ? (m_nMsaa4xQualityLevels - 1) : 0;
	dxgiSwapChainDesc.Windowed = false;
	dxgiSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	HRESULT hResult = m_pdxgiFactory->CreateSwapChain(m_pd3dCommandQueue, &dxgiSwapChainDesc, (IDXGISwapChain**)&m_pdxgiSwapChain);
#endif
	m_nSwapChainBufferIndex = m_pdxgiSwapChain->GetCurrentBackBufferIndex();

	hResult = m_pdxgiFactory->MakeWindowAssociation(g_hWnd, DXGI_MWA_NO_ALT_ENTER);

#ifndef _WITH_SWAPCHAIN_FULLSCREEN_STATE
	Create_RenderTargetViews();
#endif

	return NOERROR;
}

HRESULT CDeviceManager::Create_DepthStencilView()
{
	m_pd3dCommandList[COMMAND_LOAD]->Reset(m_pd3dCommandAllocator[COMMAND_LOAD], NULL);

	D3D12_RESOURCE_DESC d3dResourceDesc;
	d3dResourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	d3dResourceDesc.Alignment = 0;
	d3dResourceDesc.Width = m_nClientWidth;
	d3dResourceDesc.Height = m_nClientHeight;
	d3dResourceDesc.DepthOrArraySize = 1;
	d3dResourceDesc.MipLevels = 1;
	d3dResourceDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dResourceDesc.SampleDesc.Count = (m_bMsaa4xEnable) ? 4 : 1;
	d3dResourceDesc.SampleDesc.Quality = (m_bMsaa4xEnable) ? (m_nMsaa4xQualityLevels - 1) : 0;
	d3dResourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	d3dResourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_HEAP_PROPERTIES d3dHeapProperties;
	::ZeroMemory(&d3dHeapProperties, sizeof(D3D12_HEAP_PROPERTIES));
	d3dHeapProperties.Type = D3D12_HEAP_TYPE_DEFAULT;
	d3dHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	d3dHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	d3dHeapProperties.CreationNodeMask = 1;
	d3dHeapProperties.VisibleNodeMask = 1;

	D3D12_CLEAR_VALUE d3dClearValue;
	d3dClearValue.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dClearValue.DepthStencil.Depth = 1.0f;
	d3dClearValue.DepthStencil.Stencil = 0;

	m_pd3dDevice->CreateCommittedResource(&d3dHeapProperties, D3D12_HEAP_FLAG_NONE, &d3dResourceDesc, D3D12_RESOURCE_STATE_COMMON, &d3dClearValue, __uuidof(ID3D12Resource), (void**)&m_pd3dDepthStencilBuffer);

	D3D12_DEPTH_STENCIL_VIEW_DESC d3dDepthStencilViewDesc;
	::ZeroMemory(&d3dDepthStencilViewDesc, sizeof(D3D12_DEPTH_STENCIL_VIEW_DESC));
	d3dDepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dDepthStencilViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	d3dDepthStencilViewDesc.Flags = D3D12_DSV_FLAG_NONE;
	d3dDepthStencilViewDesc.Texture2D.MipSlice = 0;

	CD3DX12_CPU_DESCRIPTOR_HANDLE CpuDsvDescriptorHandle = CDescriptorHeapManager::GetInstance()->get_CpuDsvDescriptorHandle();

	m_pd3dDevice->CreateDepthStencilView(m_pd3dDepthStencilBuffer, &d3dDepthStencilViewDesc, CpuDsvDescriptorHandle);
	d3dDsvCPUDescriptorHandle = CpuDsvDescriptorHandle;
	CpuDsvDescriptorHandle.Offset(1, g_DsvDescriptorIncrementSize);
	CDescriptorHeapManager::GetInstance()->set_CpuDsvDescriptorHandle(CpuDsvDescriptorHandle);
	
	m_pd3dCommandList[COMMAND_LOAD]->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_pd3dDepthStencilBuffer,
		D3D12_RESOURCE_STATE_COMMON, D3D12_RESOURCE_STATE_DEPTH_WRITE));

	if (FAILED(m_pd3dCommandList[COMMAND_LOAD]->Close()))
		MSG_BOX("CommandList close Failed");

	ID3D12CommandList* ppd3dCommandLists[] = { m_pd3dCommandList[COMMAND_LOAD] };
	m_pd3dCommandQueue->ExecuteCommandLists(1, ppd3dCommandLists);

	return NOERROR;
}

HRESULT CDeviceManager::Create_RenderTargetViews()
{
	D3D12_CPU_DESCRIPTOR_HANDLE d3dRtvCPUDescriptorHandle = m_pd3dRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

	for (UINT i = 0; i < m_nSwapChainBuffers; ++i)
	{
		m_pdxgiSwapChain->GetBuffer(i, __uuidof(ID3D12Resource), (void**)&m_pd3dSwapChainBackBuffers[i]);
		m_pd3dDevice->CreateRenderTargetView(m_pd3dSwapChainBackBuffers[i], NULL, d3dRtvCPUDescriptorHandle);
		d3dRtvCPUDescriptorHandle.ptr += g_RtvDescriptorIncrementSize;
	}
	return NOERROR;
}

HRESULT CDeviceManager::Change_SwapChainState()
{
	WaitForGpuComplete();

	BOOL bFullScreenState = FALSE;
	m_pdxgiSwapChain->GetFullscreenState(&bFullScreenState, NULL);
	m_pdxgiSwapChain->SetFullscreenState(!bFullScreenState, NULL);

	DXGI_MODE_DESC dxgiTargetParameters;
	dxgiTargetParameters.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiTargetParameters.Width = m_nClientWidth;
	dxgiTargetParameters.Height = m_nClientHeight;
	dxgiTargetParameters.RefreshRate.Numerator = 60;
	dxgiTargetParameters.RefreshRate.Denominator = 1;
	dxgiTargetParameters.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	dxgiTargetParameters.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	m_pdxgiSwapChain->ResizeTarget(&dxgiTargetParameters);

	for (int i = 0; i < m_nSwapChainBuffers; i++) if (m_pd3dSwapChainBackBuffers[i]) m_pd3dSwapChainBackBuffers[i]->Release();

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	m_pdxgiSwapChain->GetDesc(&dxgiSwapChainDesc);
	m_pdxgiSwapChain->ResizeBuffers(m_nSwapChainBuffers, m_nClientWidth, m_nClientHeight, dxgiSwapChainDesc.BufferDesc.Format, dxgiSwapChainDesc.Flags);

	m_nSwapChainBufferIndex = m_pdxgiSwapChain->GetCurrentBackBufferIndex();

	Create_RenderTargetViews();

	return NOERROR;
}

HRESULT CDeviceManager::WaitForGpuComplete()
{
	const UINT64 nFenceValue = ++m_nFenceValues[m_nSwapChainBufferIndex];
	if (FAILED(m_pd3dCommandQueue->Signal(m_pd3dFence, nFenceValue)))
		MSG_BOX("WaitForGpuComplete Signal Failed");

	if (m_pd3dFence->GetCompletedValue() < nFenceValue)
	{
		if (FAILED(m_pd3dFence->SetEventOnCompletion(nFenceValue, m_hFenceEvent)))
			MSG_BOX("WaitForGpuComplete GetCompletedValue Failed");
		WaitForSingleObject(m_hFenceEvent, INFINITE);
	}

	return NOERROR;
}

HRESULT CDeviceManager::MoveToNextFrame()
{
	m_nSwapChainBufferIndex = m_pdxgiSwapChain->GetCurrentBackBufferIndex();

	UINT64 nFenceValue = ++m_nFenceValues[m_nSwapChainBufferIndex];
	if (FAILED(m_pd3dCommandQueue->Signal(m_pd3dFence, nFenceValue)))
		MSG_BOX("MoveToNextFrame Signal Failed");

	if (m_pd3dFence->GetCompletedValue() < nFenceValue)
	{
		if (FAILED(m_pd3dFence->SetEventOnCompletion(nFenceValue, m_hFenceEvent)))
			MSG_BOX("MoveToNextFrame SetEventOnCompletion Failed");
		WaitForSingleObject(m_hFenceEvent, INFINITE);
	}

	return NOERROR;
}

HRESULT CDeviceManager::CommandList_Reset(COMMANDID eCommandId)
{
	return m_pd3dCommandList[eCommandId]->Reset(m_pd3dCommandAllocator[eCommandId], NULL);
}

HRESULT CDeviceManager::CommandList_Close(COMMANDID eCommandId)
{
	if (FAILED(m_pd3dCommandList[eCommandId]->Close()))
		MSG_BOX("CommandList close Failed");

	ID3D12CommandList* ppd3dCommandLists[] = { m_pd3dCommandList[eCommandId] };
	m_pd3dCommandQueue->ExecuteCommandLists(1, ppd3dCommandLists);

	WaitForGpuComplete();

	return NOERROR;
}

HRESULT CDeviceManager::Render_Start(COMMANDID eCommandId)
{
	HRESULT hResult = m_pd3dCommandAllocator[eCommandId]->Reset();
	hResult = CommandList_Reset(eCommandId);
	return NOERROR;
}

HRESULT CDeviceManager::Render_Middle(COMMANDID eCommandId)
{
	::ZeroMemory(&m_d3dResourceBarrier, sizeof(D3D12_RESOURCE_BARRIER));
	m_d3dResourceBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	m_d3dResourceBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	m_d3dResourceBarrier.Transition.pResource = m_pd3dSwapChainBackBuffers[m_nSwapChainBufferIndex];
	m_d3dResourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	m_d3dResourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	m_d3dResourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	m_pd3dCommandList[eCommandId]->ResourceBarrier(1, &m_d3dResourceBarrier);

	D3D12_CPU_DESCRIPTOR_HANDLE d3dRtvCPUDescriptorHandle = m_pd3dRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	d3dRtvCPUDescriptorHandle.ptr += (m_nSwapChainBufferIndex * g_RtvDescriptorIncrementSize);

	float pfClearColor[4] = { 0.0f, 0.0f, 0.f, 1.0f };
	m_pd3dCommandList[eCommandId]->ClearRenderTargetView(d3dRtvCPUDescriptorHandle, pfClearColor/*Colors::Azure*/, 0, NULL);

	m_pd3dCommandList[eCommandId]->ClearDepthStencilView(d3dDsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);

	m_pd3dCommandList[eCommandId]->OMSetRenderTargets(1, &d3dRtvCPUDescriptorHandle, TRUE, &d3dDsvCPUDescriptorHandle);

	return NOERROR;
}

HRESULT CDeviceManager::Render_End(COMMANDID eCommandId)
{
	m_d3dResourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	m_d3dResourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	m_d3dResourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	m_pd3dCommandList[eCommandId]->ResourceBarrier(1, &m_d3dResourceBarrier);

	CommandList_Close(eCommandId);

#ifdef _WITH_PRESENT_PARAMETERS
	DXGI_PRESENT_PARAMETERS dxgiPresentParameters;
	dxgiPresentParameters.DirtyRectsCount = 0;
	dxgiPresentParameters.pDirtyRects = NULL;
	dxgiPresentParameters.pScrollRect = NULL;
	dxgiPresentParameters.pScrollOffset = NULL;
	m_pdxgiSwapChain->Present1(1, 0, &dxgiPresentParameters);
#else
#ifdef _WITH_SYNCH_SWAPCHAIN
	m_pdxgiSwapChain->Present(1, 0);
#else
	m_pdxgiSwapChain->Present(0, 0);
#endif
#endif

	//   m_nSwapChainBufferIndex = m_pdxgiSwapChain->GetCurrentBackBufferIndex();
	MoveToNextFrame();
	return NOERROR;
}

HRESULT CDeviceManager::Release()
{
	for (int i = 0; i < COMMAND_END; ++i)
	{
		m_pd3dCommandAllocator[i]->Release();
		m_pd3dCommandAllocator[i] = nullptr;

		m_pd3dCommandList[i]->Release();
		m_pd3dCommandList[i] = nullptr;
	}

	m_pd3dCommandQueue->Release();

	//m_pd3dDepthStencilBuffer->Unmap(0, nullptr);
	m_pd3dDepthStencilBuffer->Release();

	m_pd3dDevice->Release();
	m_pd3dFence->Release();
	m_pd3dRtvDescriptorHeap->Release();

	for (int i = 0; i < m_nSwapChainBuffers; ++i)
	{
		//m_pd3dSwapChainBackBuffers[i]->Unmap(0, nullptr);
		m_pd3dSwapChainBackBuffers[i]->Release();
	}

	m_pdxgiFactory->Release();
	m_pdxgiSwapChain->Release();

	delete this;

	return NOERROR;
}

