#ifndef __BUFFER__
#define __BUFFER__

#include "stdafx.h"
#include "Component.h"

struct Vertex_Color
{
	XMFLOAT3 Pos;
	XMFLOAT4 Color;
};

struct Vertex_Tex
{
	XMFLOAT3 Pos;
	XMFLOAT2 Tex;
};

class CBuffer : public CComponent
{
protected:
	explicit CBuffer();
	explicit CBuffer(const CBuffer& rhs);
	virtual ~CBuffer(void) = default;

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg) = 0;

protected:
	void Release_UploadBuffer();

protected:
	ID3D12Resource* m_pd3dVertexBuffer = NULL;
	ID3D12Resource* m_pd3dVertexUploadBuffer = NULL;

	ID3D12Resource* m_pd3dIndexBuffer = NULL;
	ID3D12Resource* m_pd3dIndexUploadBuffer = NULL;

	D3D12_VERTEX_BUFFER_VIEW      m_d3dVertexBufferView;
	D3D12_INDEX_BUFFER_VIEW         m_d3dIndexBufferView;

	D3D12_PRIMITIVE_TOPOLOGY      m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT                     m_nSlot = 0;
	UINT                     m_nVertices = 0;
	UINT                     m_nStride = 0;
	UINT                     m_nOffset = 0;

	UINT                     m_nIndices = 0;
	UINT                     m_nStartIndex = 0;
	int                        m_nBaseVertex = 0;

};

#endif