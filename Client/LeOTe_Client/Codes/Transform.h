//------------------------------------------------------- ----------------------
// File: Object.h
//-----------------------------------------------------------------------------

#pragma once

#include "Mesh.h"
#include "Camera.h"
#include "Component.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CTransform final : public CComponent
{
private:
	explicit CTransform();
	explicit CTransform(const CTransform& rhs);
	virtual ~CTransform(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CTransform* Create();

	XMFLOAT3 GetPosition();
	XMFLOAT3 GetLook();
	XMFLOAT3 GetUp();
	XMFLOAT3 GetRight();

	XMFLOAT4X4 GetWorldMat();
	void SetWorldMat(XMFLOAT4X4 worldMat);

	XMFLOAT4X4 GetParentsWorldMat();
	void SetParentsWorldMat(XMFLOAT4X4 worldMat);

	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 xmf3Position);
	void SetPositionY(float y);
	void SetRight(float x, float y, float z);
	void SetRight(XMFLOAT3 xmf3Position);
	void SetUp(float x, float y, float z);
	void SetUp(XMFLOAT3 xmf3Position);
	void SetLook(float x, float y, float z);
	void SetLook(XMFLOAT3 xmf3Position);
	void SetScale(float x, float y, float z);

	void MoveStrafe(float fDistance = 1.0f);
	void MoveUp(float fDistance = 1.0f);
	void MoveForward(float fDistance = 1.0f);

	void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);
	void Rotate(XMFLOAT3* pxmf3Axis, float fAngle);
	void Rotate(XMFLOAT4* pxmf4Quaternion);

	HRESULT Release();

private:
	XMFLOAT4X4  m_xmf4x4World;
	XMFLOAT4X4	m_xmf4x4ParentsWorld;
};

