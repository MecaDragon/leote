#ifndef __LOADING__
#define __LOADING__

#include "stdafx.h"
#include "Defines.h"

class CLoading final
{
private:
	explicit CLoading();
	virtual ~CLoading();

public:
	SCENEID Get_LoadingID() const { return m_LoadingID; }
	CRITICAL_SECTION* Get_Crt() { return &m_Crt; }
	bool Get_Finish() { return m_bFinish; }
	const wchar_t* Get_String() const { return m_szLoading; }

public:
	static unsigned int APIENTRY Thread_Main(void* pArg);

public:
	HRESULT Initialize_Loading(SCENEID eLoadingId);
	unsigned int Loading_ForFallingmap();
	unsigned int Loading_ForBoardmap();
	unsigned int Loading_ForWaitingRoom();
	unsigned int Loading_ForLogin();
	unsigned int Loading_ForGgoggomap();
	unsigned int Loading_ForBullfightmap();
	unsigned int Loading_ForOneMindMap();
	unsigned int Loading_ForPrize();

private:
	HANDLE	m_hThread;
	CRITICAL_SECTION m_Crt;
	SCENEID	m_LoadingID;
	static bool m_bFinish;
	wchar_t m_szLoading[128];

public:
	static CLoading* Create(SCENEID eLoadingId);
	HRESULT Release();
};

#endif