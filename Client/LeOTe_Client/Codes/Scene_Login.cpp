#include "Scene_Login.h"
#include "Management.h"
#include "Scene_Boardmap.h"
#include "Scene_WaitingRoom.h"
#include "ServerManager.h"
#include "Camera_Basic.h"
#include "Logo.h"
#include "StartButton.h"
#include "Light.h"
#include "Texture.h"
#include "Transform.h"
#include "Plane_Buffer.h"
#include "Cube_Buffer.h"
#include "Font.h"

extern bool G_SC;

CScene_Login::CScene_Login()
{
}

CScene_Login::~CScene_Login()
{
}

HRESULT CScene_Login::Initialize_Scene()
{

	m_SceneId = SCENEID::SCENE_LOGIN;

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);


	m_pManagement = CManagement::GetInstance();

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_LOGIN, L"Layer_BackGround", L"GameObject_Font_forLogin", (CGameObject**)&m_pLoginText)))
		return E_FAIL;
	m_pLoginText->SetText(L"Input Name", XMFLOAT2(0.39f, 0.67f), XMFLOAT2(1.f, 1.f));

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_LOGIN, L"Layer_BackGround", L"GameObject_LoginImage", nullptr)))
		return E_FAIL;

	if (FAILED(Ready_GameObject_Prototype()))
		return E_FAIL;

	if (FAILED(Ready_Layer_BackGround(L"Layer_Camera")))
		return E_FAIL;

	m_pLoading = CLoading::Create(SCENE_WAITINGROOM);

	if (nullptr == m_pLoading)
		return E_FAIL;


	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	return NOERROR;
}

short CScene_Login::Update_Scene(double TimeDelta)
{
	if (SERVERCONNECT) {

		if (/*GetKeyState(VK_SPACE) & 0x8000 &&*/
			true == m_pLoading->Get_Finish() &&
			CServerManager::GetInstance()->Get_LoginState() == LOGIN_STATE::END)
		{
			CManagement* pManagement = CManagement::GetInstance();

			if (nullptr == pManagement)
				return -1;

			pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

			if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
				return -1;

			if (FAILED(pManagement->Clear_Scene(SCENE_LOGIN)))
				return -1;
		}
		else if (m_pManagement)
		{
			wstring w = L"";
			string name = m_pManagement->Get_InputID();
			if (name != " ")
			{
				w.assign(name.begin(), name.end());
				m_pLoginText->SetTextString(w);
			}
		}
	}
	else
	{
		if (true == m_pLoading->Get_Finish())
		{
			CManagement* pManagement = CManagement::GetInstance();

			if (nullptr == pManagement)
				return -1;

			pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

			if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
				return -1;

			if (FAILED(pManagement->Clear_Scene(SCENE_LOGIN)))
				return -1;
		}
		else if (m_pManagement)
		{
			wstring w = L"";
			string name = m_pManagement->Get_InputID();
			if (name != " ")
			{
				w.assign(name.begin(), name.end());
				m_pLoginText->SetTextString(w);
			}
		}
	}

	
	return 0;
}

short CScene_Login::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_Login::Render_Scene()
{
	return NOERROR;
}

HRESULT CScene_Login::Ready_GameObject_Prototype()
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;

	return NOERROR;
}

HRESULT CScene_Login::Ready_Layer_BackGround(const wchar_t* pLayerTag)
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;



	return NOERROR;
}


CScene_Login* CScene_Login::Create()
{
	CScene_Login* pInstance = new CScene_Login();

	if (pInstance->Initialize_Scene())
	{
		MSG_BOX("CScene_Login Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_Login::Release()
{
	Safe_Release(m_pLoading);

	delete this;

	return NOERROR;
}
