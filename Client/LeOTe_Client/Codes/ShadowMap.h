#pragma once
#include "Camera_Light.h"

class CLight;
class CShadowMap
{
public:
	CShadowMap(UINT width, UINT height);
		
	CShadowMap(const CShadowMap& rhs);
	CShadowMap& operator=(const CShadowMap& rhs)=delete;
	~CShadowMap();

    UINT Width()const;
    UINT Height()const;
	ID3D12Resource* Resource();
	CD3DX12_GPU_DESCRIPTOR_HANDLE Srv()const;
	CD3DX12_CPU_DESCRIPTOR_HANDLE Dsv()const;

	D3D12_VIEWPORT Viewport()const;
	D3D12_RECT ScissorRect()const;

	HRESULT BuildDescriptors();

	void OnResize(UINT newWidth, UINT newHeight);

	void DrawStart_ShadowMap();
	void DrawEnd_ShadowMap();
	void SetTargetPos(XMFLOAT3 pos);
	void SetDirectionPos(XMFLOAT3 pos);
	HRESULT Release();

	static	CShadowMap* Create(UINT width, UINT height);

private:
	void BuildResource();
	void CreateLightCamera(int nWidth, int nHeight);
private:

	ID3D12Device* md3dDevice = nullptr;
	CCamera_Light* m_pLightCamera = nullptr;

	D3D12_VIEWPORT mViewport;
	D3D12_RECT mScissorRect;

	UINT mWidth = 0;
	UINT mHeight = 0;
	DXGI_FORMAT mFormat = DXGI_FORMAT_R24G8_TYPELESS;

	CD3DX12_CPU_DESCRIPTOR_HANDLE mhCpuSrv;
	CD3DX12_GPU_DESCRIPTOR_HANDLE mhGpuSrv;
	CD3DX12_CPU_DESCRIPTOR_HANDLE mhCpuDsv;

	CD3DX12_CPU_DESCRIPTOR_HANDLE	m_d3dDsvShadowMapCPUHandle;
	CD3DX12_GPU_DESCRIPTOR_HANDLE	m_d3dSrvShadowMapGPUHandle;

	ID3D12Resource* mShadowMap = nullptr;

	ComPtr<ID3D12DescriptorHeap> mSrvDescriptorHeap = nullptr;

	CD3DX12_GPU_DESCRIPTOR_HANDLE mNullSrv;

};

 