#ifndef __COLLIDER__
#define __COLLIDER__

#include "Component.h"

class CGameObject;
class CCollider : public CComponent
{
protected:
	explicit CCollider();
	explicit CCollider(const CCollider& rhs);
	virtual ~CCollider(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CGameObject* get_GameObj() { return m_pTargetObj; };
	virtual float get_radius() { return m_fRadius; };

protected:
	CGameObject* m_pTargetObj = nullptr;
	float m_fRadius = 0.f;
};

#endif
