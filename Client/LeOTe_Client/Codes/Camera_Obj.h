#ifndef __CAMERA_OBJ__
#define __CAMERA_OBJ__

#include "stdafx.h"
#include "Camera.h"

class CGameObject;

class CCamera_Obj : public CCamera
{	
private:
	bool isClip = false;
	CGameObject* m_pTargetObj;
	XMFLOAT3 m_xmf3Offset = XMFLOAT3(-20.0f, -10.f, 15.0f);
private:
	explicit CCamera_Obj();
	explicit CCamera_Obj(const CCamera_Obj& rhs);
	virtual ~CCamera_Obj() = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	void SetOffCameraSet(XMFLOAT3 xmf3Offset) { m_xmf3Offset = xmf3Offset; m_tCameraInfo.xmPosition.x += xmf3Offset.x; m_tCameraInfo.xmPosition.y += xmf3Offset.y; m_tCameraInfo.xmPosition.z += xmf3Offset.z; }
	void SetOffset(XMFLOAT3 xmf3Offset) { m_xmf3Offset = xmf3Offset; }
	void SetTargetObj(CGameObject* pTarget) { m_pTargetObj = pTarget; }
	CGameObject* GetTargetObj() { return(m_pTargetObj); }
	virtual void SetLookAt(CTransform* pTargetTrans);

public:
	static CCamera_Obj* Create();
	void Rotate(float x, float y, float z);
	// CCamera을(를) 통해 상속됨
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	HRESULT Release();

};

#endif