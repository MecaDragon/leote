#ifndef __CAMERA_BASIC__
#define __CAMERA_BASIC__

#include "stdafx.h"
#include "Camera.h"

class CCamera_Basic : public CCamera
{	
private:
	bool isClip = true;

private:
	explicit CCamera_Basic();
	explicit CCamera_Basic(const CCamera_Basic& rhs);
	virtual ~CCamera_Basic() = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

public:
	static CCamera_Basic* Create();

	// CCamera을(를) 통해 상속됨
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	HRESULT Release();

};

#endif