
#include "stdafx.h"
#include "GameObject.h"

class CTransform;
class CCube_Buffer;
class CShader;
class CTexture;
class CMaterial;
class CRenderer;

class CBoundingBox : public CGameObject
{
private:
	explicit CBoundingBox();
	explicit CBoundingBox(const CBoundingBox& rhs);
	virtual ~CBoundingBox(void) = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	virtual CGameObject* Clone_GameObject(void* pArg);

	static CBoundingBox* Create();

	HRESULT Release();

private:
	HRESULT Add_Component();

private:
	CCube_Buffer* m_pCube_BufferCom = nullptr;
	CShader* m_pShaderCom = nullptr;
	CTexture* m_pTextureCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	ID3D12DescriptorHeap* m_pd3dCbvSrvDescriptorHeap = nullptr;
public:
	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

};
