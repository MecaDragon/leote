#pragma once
#ifndef __MAINAPP__
#define __MAINAPP__

#include "stdafx.h"
#include "Management.h"

class CRenderer;
class CMainApp final
{
private:
	explicit CMainApp();
	virtual ~CMainApp();

public:
	HRESULT Initialize_MainApp();
	short Update_MainApp(double TimeDelta);
	short LateUpdate_MainApp(double TimeDelta);
	HRESULT Render_MainApp();

	HRESULT Release();

public:
	LRESULT Login_IP(WPARAM wParam);
	LRESULT Login_ID(WPARAM wParam);

	SCENEID Get_CurrentScene() { return m_pManagement->Get_CurrentSceneId(); }

private:
	HRESULT Initialize_Device();
	HRESULT Initialize_Start_Scene(SCENEID eSceneId);
	HRESULT Ready_Component_Static();

public:
	static CMainApp* Create();

private:
	//Manegement
	CManagement* m_pManagement = nullptr;
	CRenderer* m_pRenderer = nullptr;

	CServerManager* m_pServerManager = nullptr;

	//FPS
	wchar_t				m_szFPS[MAX_PATH] = L"";
	unsigned int		m_iRenderCall = 0;
	double				m_TimerAcc = 0.0;

	//Login
	string	m_strID;
	string	m_strIP;



};

#endif // !__MAINAPP__

