#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CMaterial;
class CShadowShader;

class CTileLarge final : public CGameObject
{
private:
	explicit CTileLarge();
	explicit CTileLarge(const CTileLarge& rhs);
	virtual ~CTileLarge() = default;
public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);

	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

	void SetDelTime(bool delTime, bool myself = false) { m_bTimeDel = delTime;  m_bmyself = myself; }
	bool GetTimeDel() { return m_bTimeDel; }
	bool GetDel() { return m_bDel; }

	void SetChecked(bool checked) { m_bChekced = checked; }
	bool GetChecked() { return m_bChekced; }
private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	bool m_bTimeDel = false;
	bool m_bDel = false;
	float m_fTime = 0.f;
	bool m_bmyself = false;

	bool m_bChekced = false;

private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();
public:
	static CTileLarge* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();
	void SetMaterialDiffuseRGBA(short r, short g, short b, short a);

	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	BoundingBox m_xmBoundingBox;
};