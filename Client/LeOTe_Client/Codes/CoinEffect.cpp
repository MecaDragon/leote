#include "stdafx.h"
#include "CoinEffect.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"
#include "Player.h"

extern CPlayer* Player;

CCoinEffect::CCoinEffect()
	: CGameObject()
{

}

CCoinEffect::CCoinEffect(const CCoinEffect& rhs)
	: CGameObject(rhs)
{
}

HRESULT CCoinEffect::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CCoinEffect::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;

		m_iCoinType = objw->tag;

		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
	}

	m_pTransformCom->SetScale(0.8f, 0.8f, 0.8f);

	return NOERROR;
}

short CCoinEffect::Update_GameObject(double TimeDelta)
{
	XMFLOAT3 xmAxis = XMFLOAT3(0.f, 0.f, 1.f);
	m_pTransformCom->Rotate(&xmAxis, 1000 * TimeDelta);

	if (m_iCoinType == 0) // getCoin
	{
		m_pTransformCom->MoveForward(-8.f * TimeDelta);

		XMFLOAT3 xmCoinPos = m_pTransformCom->GetPosition();

		if (::Player->GetPosition().y + 1.8f > xmCoinPos.y)
		{
			CSoundManager::GetInstance()->Play_Sound(L"Coin_Get");
			return -1;
		}
	}
	else if (m_iCoinType == 1) // lostCoin
	{
		m_pTransformCom->MoveForward(8.f * TimeDelta);

		XMFLOAT3 xmCoinPos = m_pTransformCom->GetPosition();

		if (::Player->GetPosition().y + 5.8f < xmCoinPos.y)
		{
			CSoundManager::GetInstance()->Play_Sound(L"Coin_Lost");
			return -1;
		}
	}



	return short();
}

short CCoinEffect::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CCoinEffect::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CCoinEffect::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Coin_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXTex", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_Coin", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCoinEffect::SetUp_ConstantTable()
{
	return NOERROR;
}

CCoinEffect* CCoinEffect::Create()
{
	CCoinEffect* pInstance = new CCoinEffect();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CCoinEffect Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CCoinEffect::Clone_GameObject(void* pArg)
{
	CCoinEffect* pInstance = new CCoinEffect(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CCoinEffect Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CCoinEffect::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
