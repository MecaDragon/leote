//-----------------------------------------------------------------------------
// File: CGameObject.cpp
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "Mesh.h"
#include "Management.h"

CMesh::CMesh()
{
}

CMesh::CMesh(const CMesh& rhs)
	: CBuffer(rhs)
{
	outVertices = rhs.outVertices;
	outIndices = rhs.outIndices;
	outMaterial = rhs.outMaterial;
	boudingBox = rhs.boudingBox;
	
	UINT vCount = 0, iCount = 0;
	vCount = (UINT)outVertices.size();
	iCount = (UINT)outIndices.size();

	m_nSlot = 0;
	m_nIndices = iCount;

	const UINT vbByteSize = vCount * sizeof(FBXVertex);
	const UINT ibByteSize = iCount * sizeof(std::uint32_t);

	SCENEID eSceneId = CManagement::GetInstance()->Get_CurrentSceneId();
	m_pd3dIndexBuffer = ::CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
		outIndices.data(), ibByteSize, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	m_pd3dVertexBuffer = CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
		outVertices.data(), vbByteSize, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = sizeof(FBXVertex);
	m_d3dVertexBufferView.SizeInBytes = vbByteSize;
}

CMesh::~CMesh(void)
{
}

HRESULT CMesh::Initialize_Component_Prototype(void* pArg)
{
	FbxLoader fbx;

	//std::string FileName = "../Data/Resources/";
	std::string FileName = (char*)pArg;
	fbx.LoadFBX(outVertices, outIndices, outMaterial, FileName, boundingVertex);
	boudingBox.Center = Vector3::ScalarProduct(Vector3::Add(boundingVertex.Max, boundingVertex.Min), 0.5f, false);
	boudingBox.Extents = Vector3::Subtract(boundingVertex.Max, boudingBox.Center);
    return NOERROR;
}

HRESULT CMesh::Initialize_Component_Prototype()
{
	return NOERROR;
}

HRESULT CMesh::Initialize_Component(void* pArg)
{
	/*if (m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->AddRef();

	if (m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->AddRef();

	if (m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->AddRef();

	if (m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->AddRef();*/

    return NOERROR;
}

CComponent* CMesh::Clone_Component(void* pArg)
{
	CMesh* pInstance = new CMesh(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CMesh* CMesh::Create()
{
	CMesh* pInstance = new CMesh();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CMesh Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CMesh* CMesh::Create(void* pArg)
{
	CMesh* pInstance = new CMesh();

    if (pInstance->Initialize_Component_Prototype(pArg))
    {
        MSG_BOX("CMesh Created Failed");
        Safe_Release(pInstance);
    }

    return pInstance;
}

void CMesh::Render()
{
	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	pd3dCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);
	pd3dCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);

	pd3dCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
	pd3dCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
}

HRESULT CMesh::Release()
{
	CBuffer::Release_UploadBuffer();

	if (m_pd3dVertexBuffer) m_pd3dVertexBuffer->Release();
	if (m_pd3dIndexBuffer) m_pd3dIndexBuffer->Release();

	//CBuffer::Release_UploadBuffer();

	//if (m_pd3dVertexBuffer) m_pd3dVertexBuffer->Release();
	//if (m_pd3dIndexBuffer) m_pd3dIndexBuffer->Release();

	delete this;

	return NOERROR;
}