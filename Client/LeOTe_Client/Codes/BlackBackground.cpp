#include "BlackBackground.h"
#include "Shader.h"
#include "Transform.h"
#include "Management.h"
#include "Plane_Buffer.h"
#include "Texture.h"
#include "Renderer.h"

CBlackBackground::CBlackBackground()
{
}

CBlackBackground::CBlackBackground(const CBlackBackground& rhs)
{
}

HRESULT CBlackBackground::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CBlackBackground::Initialize_GameObject(void* pArg)
{
	ID3D12Device* pDevice = CDeviceManager::GetInstance()->Get_Device();

	if(FAILED(Add_Component()))
		return E_FAIL;
	//355x354

	CD3DX12_RANGE readRange(0, 0);	// We do not intend to read from this resource on the CPU. (so end is less than or equal to begin)

// map the resource heap to get a gpu virtual address to the beginning of the heap
	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	//m_pTransformCom->SetScale(1.0f, 0.3f, 0.1f);
	m_pTransformCom->SetPosition(0.f, 0.f, 0.f);
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
	}
	m_pGameObjResource->SetName(L"BlackBackground Object m_pGameObjResource");
	return NOERROR;
}

short CBlackBackground::Update_GameObject(double TimeDelta)
{

	return 0;
}

short CBlackBackground::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return 0;
}

HRESULT CBlackBackground::Render_GameObject()
{
	if (!isHide)
	{
		ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
		m_pShaderCom->SetPipeline(pCommandList);
		m_pTextureCom->UpdateShaderVariables(pCommandList);

		m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform);

		D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
		pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

		m_pPlane_BufferCom->Render_Buffer();
	}
	return NOERROR;
}

CGameObject* CBlackBackground::Clone_GameObject(void* pArg)
{
	CBlackBackground* pInstance = new CBlackBackground(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CBlackBackground Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CBlackBackground* CBlackBackground::Create()
{
	CBlackBackground* pInstance = new CBlackBackground();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CBlackBackground Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CBlackBackground::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pPlane_BufferCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}

HRESULT CBlackBackground::Add_Component()
{

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_UI", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_PlaneBuffer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Plane_Buffer", L"Com_Plane_Buffer", (CComponent**)&m_pPlane_BufferCom)))
		return E_FAIL;
	
	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_BlackBackground", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}
