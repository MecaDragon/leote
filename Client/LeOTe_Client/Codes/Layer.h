#ifndef __LAYER__
#define __LAYER__

#include "GameObject.h"

class CPlayer;

class CLayer final
{
private:
	explicit CLayer();
	virtual ~CLayer();

public:
	HRESULT Initialize_Layer();
	HRESULT Add_GameObject(CGameObject* pGameObject);
	short Update_Layer(double TimeDelta);
	short LateUpdate_Layer(double TimeDelta);

	HRESULT Render_ShadowMap();
	
	list<CGameObject*>& Get_GameObject();

	void Update_PlayerPosition(packet_move p);
	void Set_PlayerOrder(packet_order p, CPlayer** players);

	CGameObject* Search_Player(int id);
private:
	list<CGameObject*> m_listGameObj;
	typedef list<CGameObject*> LISTOBJECT;

public:
	static CLayer* Create();
	HRESULT Release();

	
};

#endif