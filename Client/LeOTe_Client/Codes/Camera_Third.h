#ifndef __CAMERA_THIRD__
#define __CAMERA_THIRD__

#include "stdafx.h"
#include "Camera.h"

class CPlayer;

class CCamera_Third : public CCamera
{	
private:
	bool isClip = false;
	CPlayer* m_pPlayer;
	XMFLOAT3 m_xmf3Offset;
private:
	explicit CCamera_Third();
	explicit CCamera_Third(const CCamera_Third& rhs);
	virtual ~CCamera_Third() = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	void SetOffCameraSet(XMFLOAT3 xmf3Offset) { m_xmf3Offset = xmf3Offset; m_tCameraInfo.xmPosition.x += xmf3Offset.x; m_tCameraInfo.xmPosition.y += xmf3Offset.y; m_tCameraInfo.xmPosition.z += xmf3Offset.z; }
	void SetOffset(XMFLOAT3 xmf3Offset) { m_xmf3Offset = xmf3Offset; }
	void SetPlayer(CPlayer* pPlayer) { m_pPlayer = pPlayer; }
	CPlayer* GetPlayer() { return(m_pPlayer); }
	virtual void SetLookAt(XMFLOAT3& vLookAt);

public:
	static CCamera_Third* Create();
	void Rotate(float x, float y, float z);
	// CCamera을(를) 통해 상속됨
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	HRESULT Release();

};

#endif