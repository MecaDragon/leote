#include "Management.h"
#include "stdafx.h"
#include "Renderer.h"

CRenderer::CRenderer()
{
}

CRenderer::~CRenderer(void)
{
}

HRESULT CRenderer::Initialize_Component_Prototype()
{
	

	return NOERROR;
}

CComponent* CRenderer::Clone_Component(void* pArg)
{
	return this;
}

CRenderer* CRenderer::Create()
{
	CRenderer* pInstance = new CRenderer();

	if (FAILED(pInstance->Initialize_Component_Prototype()))
	{
		MSG_BOX("CRenderer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CRenderer::Release()
{
	for (size_t i = 0; i < RENDER_END; ++i)
		m_RenderGroup[i].clear();

	delete this;

	return NOERROR;
}

bool Compare_Z(CGameObject* pSour, CGameObject* pDest)
{
	return pSour->Get_ViewZ() > pDest->Get_ViewZ();
}

HRESULT CRenderer::Add_RenderGroup(RENDERID eRenderID, CGameObject* pGameObject)
{
	if (RENDER_END <= eRenderID ||
		nullptr == pGameObject)
		return E_FAIL;

	m_RenderGroup[eRenderID].push_back(pGameObject);

	return NOERROR;
}

HRESULT CRenderer::Draw_RenderGroup()
{
	if (FAILED(Render_Priority()))
		return E_FAIL;

	if (FAILED(Render_NonAlpha())) // Diffuse, Normal�� �׸���.
		return E_FAIL;

	if (FAILED(Render_Alpha()))
		return E_FAIL;

	if (FAILED(Render_UI()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CRenderer::Render_Priority()
{
	for (auto& pGameObject : m_RenderGroup[RENDER_PRIORITY])
	{
		if (nullptr != pGameObject)
			pGameObject->Render_GameObject();
	}
	m_RenderGroup[RENDER_PRIORITY].clear();

	return NOERROR;
}

HRESULT CRenderer::Render_NonAlpha()
{
	for (auto& pGameObject : m_RenderGroup[RENDER_NONALPHA])
	{
		if (nullptr != pGameObject)
			pGameObject->Render_GameObject();
	}
	m_RenderGroup[RENDER_NONALPHA].clear();

	return NOERROR;
}

HRESULT CRenderer::Render_Alpha()
{
	m_RenderGroup[RENDER_ALPHA].sort(Compare_Z);

	for (auto& pGameObject : m_RenderGroup[RENDER_ALPHA])
	{
		if (nullptr != pGameObject)
			pGameObject->Render_GameObject();
	}
	m_RenderGroup[RENDER_ALPHA].clear();

	return NOERROR;
}

HRESULT CRenderer::Render_UI()
{
	for (auto& pGameObject : m_RenderGroup[RENDER_UI])
	{
		if (nullptr != pGameObject)
			pGameObject->Render_GameObject();
	}
	m_RenderGroup[RENDER_UI].clear();

	return NOERROR;
}

