#include "Cube_Buffer.h"
#include "Management.h"

CCube_Buffer::CCube_Buffer()
{
}

CCube_Buffer::CCube_Buffer(const CCube_Buffer& rhs)
	: CBuffer(rhs)
{
}

HRESULT CCube_Buffer::Initialize_Component_Prototype(CUBE_TYPE eCubeType)
{
	if (eCubeType == CUBE_COLOR)
	{
		m_nVertices = 8;
		m_nStride = sizeof(Vertex_Color);
		m_nOffset = 0;
		m_nSlot = 0;
		m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

		Vertex_Color vertices[8] =
		{
			Vertex_Color({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(Colors::White) }),
			Vertex_Color({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT4(Colors::Black) }),
			Vertex_Color({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT4(Colors::Red) }),
			Vertex_Color({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT4(Colors::Green) }),
			Vertex_Color({ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT4(Colors::Blue) }),
			Vertex_Color({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT4(Colors::Yellow) }),
			Vertex_Color({ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT4(Colors::Cyan) }),
			Vertex_Color({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT4(Colors::Magenta) })
		};

		m_pd3dVertexBuffer = CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
			vertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);
	}
	else if (eCubeType == CUBE_TEX)
	{
		m_nVertices = 37;
		m_nStride = sizeof(Vertex_Tex);
		m_nOffset = 0;
		m_nSlot = 0;
		m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

		Vertex_Tex vertices[37] =
		{
			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT2(1.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT2(1.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) }),
			
			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT2(0.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) }),
			Vertex_Tex({ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT2(0.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT2(1.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) }),
			Vertex_Tex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) }),

			Vertex_Tex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) })
		};

		m_pd3dVertexBuffer = CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
			vertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);
	}

	m_nIndices = 36;

	UINT indices[36] =
	{
		// front face
		0, 1, 2,
		0, 2, 3,
		// back face
		4, 6, 5,
		4, 7, 6,
		// left face
		4, 5, 1,
		4, 1, 0,
		// right face
		3, 2, 6,
		3, 6, 7,
		// top face
		1, 5, 6,
		1, 6, 2,
		// bottom face
		4, 0, 3,
		4, 3, 7
	};

	if (eCubeType == CUBE_COLOR)
	{
		m_pd3dIndexBuffer = ::CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
			indices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

		m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
		m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
		m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;
	}

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	return NOERROR;
}

HRESULT CCube_Buffer::Initialize_Component(void* pArg)
{
	if (m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->AddRef();

	if (m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->AddRef();

	if (m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->AddRef();

	if (m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->AddRef();

	m_pd3dVertexBuffer->SetName(L"Cube Buffer m_pd3dVertexBuffer");
	m_pd3dVertexUploadBuffer->SetName(L"Cube Buffer m_pd3dVertexUploadBuffer");
	return NOERROR;
}

HRESULT CCube_Buffer::Render_Buffer()
{
	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	
	pd3dCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);

	if (m_d3dIndexBufferView.SizeInBytes != 0)
	{
		pd3dCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
		pd3dCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	}
	else
		pd3dCommandList->DrawInstanced(m_nVertices, 1, m_nOffset, 0);


	return NOERROR;
}

CComponent* CCube_Buffer::Clone_Component(void* pArg)
{
	CCube_Buffer* pInstance = new CCube_Buffer(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CCube_Buffer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CCube_Buffer* CCube_Buffer::Create(CUBE_TYPE eCubeType)
{
	CCube_Buffer* pInstance = new CCube_Buffer();

	if (pInstance->Initialize_Component_Prototype(eCubeType))
	{
		MSG_BOX("CCube_Buffer Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CCube_Buffer::Release()
{
	CBuffer::Release_UploadBuffer();

	if(m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->Release();

	if(m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->Release();

	delete this;

	return NOERROR;
}
