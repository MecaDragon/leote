#include "ObjectManager.h"
#include "Management.h"

IMPLEMENT_SINGLETON(CObjectManager)

CObjectManager::CObjectManager()
{
}

CObjectManager::~CObjectManager()
{
}

HRESULT CObjectManager::Initialize_ObjectManager(_uint iNumScenes)
{
	if (nullptr != m_pLayers)
		return E_FAIL;

	m_pLayers = new LAYERS[iNumScenes];
	m_pPrototype = new PROTOTYPES[iNumScenes];

	m_iNumScenes = iNumScenes;

	return NOERROR;
}

HRESULT CObjectManager::Add_Prototype_GameObj(const wchar_t* pPrototypeTag, CGameObject* pPrototype)
{
	if (nullptr == pPrototype || nullptr != Find_Prototype(pPrototypeTag))
		return E_FAIL;

	SCENEID loadingSceneId = CManagement::GetInstance()->Get_LoadingSceneId();

	m_pPrototype[loadingSceneId].insert(PROTOTYPES::value_type(pPrototypeTag, pPrototype));

	return NOERROR;
}

HRESULT CObjectManager::Add_GameObjectToLayer(_uint iSceneIndex, const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, CGameObject** ppOutput, void* pArg)
{
	if (nullptr == m_pLayers)
		return E_FAIL;

	CGameObject* pPrototype = Find_Prototype(pPrototypeTag);
	if (nullptr == pPrototype)
		return E_FAIL;

	CGameObject* pGameObject = pPrototype->Clone_GameObject(pArg);
	if (nullptr == pGameObject)
		return E_FAIL;

	CLayer* pLayer = Find_Layer(iSceneIndex, pLayerTag);
	if (nullptr == pLayer)
	{
		CLayer* pLayer = CLayer::Create();
		pLayer->Add_GameObject(pGameObject);
		m_pLayers[iSceneIndex].insert(LAYERS::value_type(pLayerTag, pLayer));
	}
	else
		pLayer->Add_GameObject(pGameObject);

	if (ppOutput)
		*ppOutput = pGameObject;

	return NOERROR;
}

short CObjectManager::Update_ObjectManager(double TimeDelta)
{
	if (nullptr == m_pLayers)
		return -1;

	short nEvent = 0;
	SCENEID currentScene = CManagement::GetInstance()->Get_CurrentSceneId();

	for (size_t i = 0; i < m_iNumScenes; i++)
	{
		if ((currentScene != SCENEID::SCENE_BOARDMAP) &&
			i == SCENEID::SCENE_BOARDMAP)
			continue;

		for (auto& Pair : m_pLayers[i])
		{
			nEvent = Pair.second->Update_Layer(TimeDelta);
			if (nEvent != 0)
				return nEvent;
		}
	}

	return 0;
}

short CObjectManager::LateUpdate_ObjectManager(double TimeDelta)
{
	if (nullptr == m_pLayers)
		return -1;

	short nEvent = 0;
	SCENEID currentScene = CManagement::GetInstance()->Get_CurrentSceneId();

	for (size_t i = 0; i < m_iNumScenes; i++)
	{
		if ((currentScene != SCENEID::SCENE_BOARDMAP) &&
			i == SCENEID::SCENE_BOARDMAP)
			continue;

		for (auto& Pair : m_pLayers[i])
		{
			nEvent = Pair.second->LateUpdate_Layer(TimeDelta);
			if (nEvent != 0)
				return nEvent;
		}
	}

	return 0;
}

HRESULT CObjectManager::Render_ShadowMap()
{
	if (nullptr == m_pLayers)
		return -1;

	HRESULT hResult;
	SCENEID currentScene = CManagement::GetInstance()->Get_CurrentSceneId();

	for (size_t i = 0; i < m_iNumScenes; i++)
	{
		if ((currentScene != SCENEID::SCENE_BOARDMAP) &&
			i == SCENEID::SCENE_BOARDMAP)
			continue;

		for (auto& Pair : m_pLayers[i])
		{
			hResult = Pair.second->Render_ShadowMap();
			if (FAILED(hResult))
				return E_FAIL;
		}
	}

	return NOERROR;
}


HRESULT CObjectManager::Clear_ObjectManager(_uint iSceneIndex)
{
	if (m_iNumScenes <= iSceneIndex || nullptr == m_pLayers)
		return E_FAIL;

	for (auto& Pair : m_pLayers[iSceneIndex])
		Safe_Release(Pair.second);

	m_pLayers[iSceneIndex].clear();

	return NOERROR;
}

HRESULT CObjectManager::Clear_ObjectPrototype(_uint iSceneIndex)
{
	if (m_iNumScenes <= iSceneIndex || nullptr == m_pPrototype)
		return E_FAIL;

	//if (iSceneIndex == SCENEID::SCENE_BOARDMAP || iSceneIndex == SCENEID::SCENE_STATIC)
	//	return NOERROR;

	for (auto& Pair : m_pPrototype[iSceneIndex])
		Safe_Release(Pair.second);

	m_pPrototype[iSceneIndex].clear();

	return NOERROR;
}

CGameObject* CObjectManager::Find_Prototype(const wchar_t* pPrototypeTag)
{
	for (size_t i = 0; i < m_iNumScenes;)
	{
		auto	iter = find_if(m_pPrototype[i].begin(), m_pPrototype[i].end(), CTag_Finder(pPrototypeTag));

		if (iter == m_pPrototype[i].end())
			++i;
		else
			return iter->second;		
	}

	return nullptr;
}

CLayer* CObjectManager::Find_Layer(_uint iSceneIndex, const wchar_t* pLayerTag)
{
	if (nullptr == m_pLayers || m_iNumScenes <= iSceneIndex)
		return nullptr;

	auto	iter = find_if(m_pLayers[iSceneIndex].begin(), m_pLayers[iSceneIndex].end(), CTag_Finder(pLayerTag));

	if (iter == m_pLayers[iSceneIndex].end())
		return nullptr;

	return iter->second;
}

HRESULT CObjectManager::Release()
{
	for (size_t i = 0; i < m_iNumScenes; i++)
	{
		for (auto& Pair : m_pLayers[i])
			Safe_Release(Pair.second);
		m_pLayers[i].clear();
	}

	delete[] m_pLayers;

	for (size_t i = 0; i < m_iNumScenes; i++)
	{
		for (auto& Pair : m_pPrototype[i])
			Safe_Release(Pair.second);
	}

	delete[] m_pPrototype;

	delete this;

	return NOERROR;
}
