#ifndef __Scene_Ggoggomap__
#define __Scene_Ggoggomap__

#include "stdafx.h"
#include "Scene.h"
#include "Loading.h"
#include "queue"

#define GGOUI_MAX 10
class CPlayer;
class CBoundingBox;
class CFont;
class CPlayerIcon;
class CBillBoardObject;
class CCube;
class CTileLarge;
class CScene_Boardmap;
class CStaticObject;
class CUI;

class CScene_Ggoggomap final : public CScene
{
private:
	explicit CScene_Ggoggomap();
	virtual ~CScene_Ggoggomap();

public:
	virtual HRESULT Initialize_Scene(CScene_Boardmap* pSceneBoardmap);
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();
	HRESULT Scene_Change(SCENEID Scene);
	virtual short KeyEvent(double TimeDelta);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);

public:
	static CScene_Ggoggomap* Create(CScene_Boardmap* pSceneBoardmap);
	HRESULT Release();

private:
	void movePlayer(XMFLOAT3 forwardDir, XMFLOAT3 dotProduct, XMFLOAT3& XMShift, double TimeDelta);
	XMFLOAT3 slidingPlayer(XMFLOAT3& xmExcess);

private:
	CScene_Boardmap* m_pScene_Boardmap = nullptr;
	float moveDelta;
	bool isMoving = false;
	
	//게임 동시시작 flag
	bool gameStarted = false;
	bool gameEnd = false; //게임 종료시 키 입력 막기
private:
	CTileLarge* pTile[144] = { nullptr, };
	CTileLarge* pRedTile[144] = { nullptr, };
	CTileLarge* pYellowTile[144] = { nullptr, };
	CBoundingBox* pBoundingBox[2] = { nullptr, };

	CFont* m_pTimeText = nullptr;
	CFont* m_pChickenCountText = nullptr;
	CFont* m_pEndText = nullptr;
	CFont* m_pStatTimeText = nullptr;
	CFont* pButtonText = nullptr;

	CStaticObject* m_Windmill = nullptr;

	CBillBoardObject* pHerePlayer = nullptr;

	queue<char> UIlist;
	int UIindex = 0;
	int m_iStartTime = 0;

	float UITime = 0.f;

	CUI* pGGoGGoUI[GGOUI_MAX] = { nullptr, };

	bool loadcomplete = false;

	int m_iEndTime = 3;
	bool m_bEndAsk = false; //종료 요청 (3초)

	CLoading* m_pLoading = nullptr;

	CStaticObject* m_pArrowHead = nullptr;
};

#endif