#include "CollisionManager.h"
#include "Collider.h"
#include "GameObject.h"

IMPLEMENT_SINGLETON(CCollisionManager)

CCollisionManager::CCollisionManager()
{
}

CCollisionManager::~CCollisionManager()
{
}

HRESULT CCollisionManager::Initialize_CollisionManager(_uint iNumScenes)
{
	return NOERROR;
}


HRESULT CCollisionManager::Release()
{
	m_vecCollider.clear();

	return NOERROR;
}

void CCollisionManager::checkCollision(double TimeDelta)
{	
	CGameObject* pObj1 = nullptr;
	CGameObject* pObj2 = nullptr;

	for (int i = 0; i < m_vecCollider.size(); i++)
	{
		pObj1 = m_vecCollider[i]->get_GameObj();

		for (int j = 0; j < m_vecCollider.size(); j++)
		{
			if (i == j)
				continue;

			pObj2 = m_vecCollider[j]->get_GameObj();

			float x = pObj1->get_ObjPosition().x - pObj2->get_ObjPosition().x;
			float z = pObj1->get_ObjPosition().z - pObj2->get_ObjPosition().z;
			float dist = sqrtf(x * x + z * z);

			float radius1 = m_vecCollider[i]->get_radius();
			float radius2 = m_vecCollider[j]->get_radius();

			if (dist < radius1 + radius2)
			{
				pObj1->OnCollision(*(m_vecCollider[j]), TimeDelta);
				pObj2->OnCollision(*(m_vecCollider[i]), TimeDelta);
			}
		}
	}
}
