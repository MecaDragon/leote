#include "BillBoardObject.h"
#include "Shader.h"
#include "Transform.h"
#include "Management.h"
#include "Plane_Buffer.h"
#include "Texture.h"
#include "Renderer.h"

CBillBoardObject::CBillBoardObject()
{
}

CBillBoardObject::CBillBoardObject(const CBillBoardObject& rhs)
{
}

HRESULT CBillBoardObject::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CBillBoardObject::Initialize_GameObject(void* pArg)
{
	ID3D12Device* pDevice = CDeviceManager::GetInstance()->Get_Device();
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	if(FAILED(Add_Component()))
		return E_FAIL;
	//355x354

	pGeometryBillboardVertices = new CGeometryBillboardVertex;
	pGeometryBillboardVertices->m_xmf3Position = XMFLOAT4(-0.946197, 16.7219, 2.23977, 1);
	pGeometryBillboardVertices->m_xmf2Size = XMFLOAT2(8.0f, 8.0f);
	pGeometryBillboardVertices->m_xmf2ClipSize = XMFLOAT2(1.0f, 1.0f);
	pGeometryBillboardVertices->m_nIndexX = 0;
	pGeometryBillboardVertices->m_nIndexY = 0;


	m_nStride = sizeof(CGeometryBillboardVertex);

	m_pd3dPositionBuffer = CreateBufferResource(pDevice, pCommandList, pGeometryBillboardVertices, m_nStride, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dPositionUploadBuffer);

	m_d3dPositionBufferView.BufferLocation = m_pd3dPositionBuffer->GetGPUVirtualAddress();
	m_d3dPositionBufferView.StrideInBytes = m_nStride;
	m_d3dPositionBufferView.SizeInBytes = m_nStride;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, m_nStride);
	m_pGameObjResource->Map(0, nullptr, (void**)&pGeometryBillboardConstants);

	pGeometryBillboardConstants->m_xmf3Position = XMFLOAT4(-0.946197, 16.7219, 2.23977, 1);
	pGeometryBillboardConstants->m_xmf2Size = XMFLOAT2(8.0f, 8.0f);
	//pGeometryBillboardConstants->m_xmf2ClipSize = XMFLOAT2(0.1917f, 0.09981f);
	pGeometryBillboardConstants->m_xmf2ClipSize = XMFLOAT2(0.1917f, 0.099846390f);
	pGeometryBillboardConstants->m_xmf2StartPos = XMFLOAT2(0.204f, 0.f);
	pGeometryBillboardConstants->m_nIndexX = 0;
	pGeometryBillboardConstants->m_nIndexY = 0;
	return NOERROR;
}

short CBillBoardObject::Update_GameObject(double TimeDelta)
{
	if (m_bStoreItem == true)
	{
		if (m_bBuyItem == true)
		{
			m_fTimer += TimeDelta;
			if (m_fTimer < 1.5f)
			{
				pGeometryBillboardConstants->m_xmf3Position.y -= TimeDelta * 1.8f;
			}
			else if (m_fTimer >= 1.5f && m_fTimer < 2.5f)
			{
			}
			else
			{
				pGeometryBillboardConstants->m_xmf3Position.y -= TimeDelta * 1.8f;

				pGeometryBillboardConstants->m_xmf2Size.x -= TimeDelta * 1.4f;
				pGeometryBillboardConstants->m_xmf2Size.y -= TimeDelta * 1.4f;

				if (pGeometryBillboardConstants->m_xmf2Size.x < 0.f)
				{
					m_bBuyItem = false;
					pGeometryBillboardConstants->m_xmf3Position.y = 1000.f;
					pGeometryBillboardConstants->m_xmf2Size = XMFLOAT2(1.3f, 1.3f);
					m_fTimer = 0.f;
				}
			}
		}
	}


	return 0;
}

short CBillBoardObject::LateUpdate_GameObject(double TimeDelta)
{
	XMFLOAT3 result = XMFLOAT3(pGeometryBillboardConstants->m_xmf3Position.x,
		pGeometryBillboardConstants->m_xmf3Position.y,
		pGeometryBillboardConstants->m_xmf3Position.z);

	Compute_ViewZ(result);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return 0;
}

HRESULT CBillBoardObject::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);
	m_pTextureCom->UpdateShaderVariables(pCommandList);
	pGeometryBillboardVertices;
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(4, d3dcbGameObjectGpuVirtualAddress);

	pCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	pCommandList->IASetVertexBuffers(0, 1, &m_d3dPositionBufferView);
	pCommandList->DrawInstanced(1, 1, 0, 0);
	return NOERROR;
}

CGameObject* CBillBoardObject::Clone_GameObject(void* pArg)
{
	CBillBoardObject* pInstance = new CBillBoardObject(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CBillBoardObject Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CBillBoardObject* CBillBoardObject::Create()
{
	CBillBoardObject* pInstance = new CBillBoardObject();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CBillBoardObject Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CBillBoardObject::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTextureCom);

	if (pGeometryBillboardVertices) delete[] pGeometryBillboardVertices;

	delete this;

	return NOERROR;
}

void CBillBoardObject::SetPostion(XMFLOAT3 xmPosition)
{
	pGeometryBillboardConstants->m_xmf3Position = XMFLOAT4(xmPosition.x, xmPosition.y, xmPosition.z,1);
}

XMFLOAT3 CBillBoardObject::GetPostion()
{
	XMFLOAT3 tmp = XMFLOAT3(pGeometryBillboardConstants->m_xmf3Position.x, 
		pGeometryBillboardConstants->m_xmf3Position.y, pGeometryBillboardConstants->m_xmf3Position.z);

	return tmp;
}

void CBillBoardObject::SetSize(XMFLOAT2 xmSize)
{
	pGeometryBillboardConstants->m_xmf2Size = xmSize;
}

void CBillBoardObject::SetStartPos(XMFLOAT2 xmPos)
{
	pGeometryBillboardConstants->m_xmf2StartPos = xmPos;
}

void CBillBoardObject::SetClipSize(XMFLOAT2 xmSize)
{
	pGeometryBillboardConstants->m_xmf2ClipSize = xmSize;
}

int CBillBoardObject::GetIndexX()
{
	return pGeometryBillboardConstants->m_nIndexX;
}

int CBillBoardObject::GetIndexY()
{
	return pGeometryBillboardConstants->m_nIndexY;
}

void CBillBoardObject::SetIndexX(int index)
{
	pGeometryBillboardConstants->m_nIndexX = index;
}

void CBillBoardObject::SetIndexY(int index)
{
	pGeometryBillboardConstants->m_nIndexY = index;
}

void CBillBoardObject::GiveIndexX(int value)
{
	pGeometryBillboardConstants->m_nIndexX = pGeometryBillboardConstants->m_nIndexX + value;
}

void CBillBoardObject::GiveIndexY(int value)
{
	pGeometryBillboardConstants->m_nIndexY = pGeometryBillboardConstants->m_nIndexY + value;
}

HRESULT CBillBoardObject::SetTexture(const wchar_t* texturename, SCENEID scene)
{
	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(scene, texturename, L"Com_Texture2", (CComponent**)&m_pTextureCom)))
		return E_FAIL;
	return NOERROR;
}

HRESULT CBillBoardObject::Add_Component()
{

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_BillBoard", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_DiceCount", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;


	return NOERROR;
}
