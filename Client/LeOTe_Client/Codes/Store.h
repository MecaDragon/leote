#ifndef __STORE__
#define __STORE__

#include "stdafx.h"
#include "GameObject.h"

class CTransform;
class CPlane_Buffer;
class CShader;
class CTexture;
class CRenderer;

class CStore : public CGameObject
{
private:
	explicit CStore();
	explicit CStore(const CStore& rhs);
	virtual ~CStore(void) = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype(wstring strTexName);
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	virtual CGameObject* Clone_GameObject(void* pArg);

	static CStore* Create(wstring strTexName);

	HRESULT Release();

private:
	HRESULT Add_Component();

private:
	CTransform* m_pTransformCom = nullptr;
	CPlane_Buffer* m_pPlane_BufferCom = nullptr;
	CShader* m_pShaderCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	wstring m_strTexName = L" ";

};

#endif