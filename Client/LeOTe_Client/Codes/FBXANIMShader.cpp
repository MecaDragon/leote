#include "FBXANIMShader.h"

CFBXANIMShader::CFBXANIMShader()
	: CShader()
{
}

CFBXANIMShader::CFBXANIMShader(const CFBXANIMShader& rhs)
	: CShader(rhs)
{
}

CFBXANIMShader::~CFBXANIMShader(void)
{
}

HRESULT CFBXANIMShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"FBXANIM Shader PipelineState");
	return NOERROR;
}

HRESULT CFBXANIMShader::Initialize_Component(void* pArg)
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"FBXANIM Shader PipelineState");
	return NOERROR;
}

CComponent* CFBXANIMShader::Clone_Component(void* pArg)
{
	CFBXANIMShader* pInstance = new CFBXANIMShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CFBXANIMShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CFBXANIMShader* CFBXANIMShader::Create()
{
	CFBXANIMShader* pInstance = new CFBXANIMShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CFBXANIMShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CFBXANIMShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Animation.hlsl", "VSFBX", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CFBXANIMShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Animation.hlsl", "PSFBX", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CFBXANIMShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 7;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[5] = { "WEIGHTS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 56, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[6] = { "BONEINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, 68, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

HRESULT CFBXANIMShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
