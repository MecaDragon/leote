#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CMaterial;
class CTexture;
class CShadowShader;

struct ComName{
	SCENEID SceneName;
	wstring TexComStr;
	wstring MeshComStr;
	wstring ShaderComStr;
	XMINT3 MatrialRGB;
	bool ShadowRender;
};

class CStaticObject final : public CGameObject
{
private:
	explicit CStaticObject();
	explicit CStaticObject(const CStaticObject& rhs);
	virtual ~CStaticObject() = default;
public:
	HRESULT Initialize_GameObject_Prototype(ComName comName); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);

	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

	void SetRender(bool render) { m_bRender = render; }
	bool GetRender() { return m_bRender; }
	void SetShadowRender(bool render) { m_bShadowRender = render; }
	bool GetShadowRender() { return m_bShadowRender; }
	BoundingBox GetBoudingBox() { return m_BoundingBox; }

	void SetTexid(int id) { texid = id; }
	int GetTexid() { return texid; }

private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;
	CTexture* m_pTextureCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	BoundingBox	m_BoundingBox;
	
	bool m_bShadowRender = true;
	bool m_bRender = true;

	int texid = 0;

private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();
public:
	static CStaticObject* Create(SCENEID SceneName, wstring TexComStr, wstring MeshComStr, wstring ShaderComStr, XMINT3 MatrialRGB, bool ShadowRender = true);
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();

	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	ComName m_ComName;
};