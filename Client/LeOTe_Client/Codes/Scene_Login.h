#ifndef __SCENE_LOGIN__
#define __SCENE_LOGIN__

#include "stdafx.h"
#include "Scene.h"
#include "Loading.h"

class CManagement;
class CFont;
class CScene_Login : public CScene
{
private:
	explicit CScene_Login();
	virtual ~CScene_Login();

public:
	virtual HRESULT Initialize_Scene();
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();

private:
	HRESULT Ready_GameObject_Prototype();
	HRESULT Ready_Layer_BackGround(const wchar_t* pLayerTag);

private:
	CLoading* m_pLoading = nullptr;

	CManagement* m_pManagement = nullptr;
	CFont* m_pLoginText = nullptr;

public:
	static CScene_Login* Create();
	HRESULT Release();

};

#endif