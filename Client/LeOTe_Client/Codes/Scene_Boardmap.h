#ifndef __SCENE_BOARDMAP__
#define __SCENE_BOARDMAP__

#include "stdafx.h"
#include "Scene.h"
#include "PlatformManager.h"
#include "Loading.h"

class CPlayer;
class CBoundingBox;
class CFont;
class CUI;
class CBillBoardObject;
class CDice;
class CBlackBackground;
class CStaticObject;
class CBalloon;
class CButton;
class CCamera_Fixed;
class CBird;
class CGhost;

enum EVENTID { EVENT_NO, EVENT_MOVE, EVENT_LOST, EVENT_SWAP, EVENT_END };

class CScene_Boardmap final : public CScene
{
private:
	explicit CScene_Boardmap();
	virtual ~CScene_Boardmap();

public:
	virtual HRESULT Initialize_Scene();
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();
	HRESULT Scene_Change(SCENEID Scene, CScene_Boardmap* pSceneBoardmap);
	virtual short KeyEvent(double TimeDelta);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz, int _tag = 0);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale, SCENEID scene = SCENEID::SCENE_BOARDMAP);
public:
	static CScene_Boardmap* Create();
	HRESULT Release();

public:
	HRESULT BackTotheBoardmap(SCENEID sceneId);

private:
	short MoveToPlatform(double TimeDelta);

	void Send_ReadyState();

	void Send_MovePacket(double TimeDelta);

	void Camera_Change(double TimeDelta);

	short cameraAnimation(double TimeDelta);

	short buttonNpcFunc(double TimeDelta);

	void HidePlayerUI(bool hide);

	void HideScoreBoadUI(bool hide);

	void SetScoreBoardUI();

	void ShowTurnUI();

	void SetBuyItem(_uint nBuyItem);

	short ShowBuyItemMotion(double TimeDelta, _uint nBuyItem);

	void ReadyChangeScene();

	short ShowMyInven(double TimeDelta);

	short ShowUsingItemMotion(double TimeDelta);

	short DrawInven();

	void ItemUsed();

	void MovePlayerPlatform(CPlayer* movePlayer, int PlatformIndex);

	void TurnEnd(double TimeDelta);

	void MiniDiceSet();

	void ShowItemDiceNumber(double TimeDelta);

private:
	CLoading* m_pLoading = nullptr;
	CLoading* m_pDisconLoading = nullptr;

	float moveDelta;

	CPlatformManager* m_pPlatformManager = nullptr;

	bool m_bMoveTeddyBear = false;
	int m_iDiceNumber = 0;

	int m_iButtonIndex = 0;

	bool m_bDiceOn = false;

	bool m_bSelectPlatform = false;
	int m_iSelectPlatformDir = 0; // 0은 직진 1은 left 2는 right
	int m_iNextPlatformDir = 0, m_iNextPlatformDir2 = 0; // 0은 직진 1은 left 2는 right

	float m_fLength = 10000.f;

	bool m_bIsMove = false;

	bool m_bOtherDicenum = false; //다른 플레이어의 주사위 숫자 보이기

	double m_OrderTime = 0;
	bool m_bOrderView = true;

	int m_iTurn = -1; // 보드맵 턴

	bool m_bJumped = false; //점프를 했었는가

	bool m_bEndCameraAnim = false;

	SCENEID m_SceneChange = SCENE_FALLINGMAP;

	CBoundingBox* pBoundingBox[2] = { nullptr, };
	CFont* pStarText[3] = { nullptr, };
	CFont* pClothesButtonText[3] = { nullptr, };
	CFont* pRankText[3] = { nullptr, };
	CFont* pLineText[3] = { nullptr, };
	CFont* pNameText[3] = { nullptr, };
	CFont* pTurnText = nullptr;
	CUI* pPlayerIcon[3] = { nullptr, };
	CUI* pButtonIcon[3] = { nullptr, };
	CUI* pCoinIcon[3] = { nullptr, };

	CFont* pSocreBoadStarText[3] = { nullptr, };
	CFont* pSocreBoadClothesButtonText[3] = { nullptr, };
	CFont* pSocreBoadRankText[3] = { nullptr, };
	CFont* pSocreBoadNameText[3] = { nullptr, };
	CFont* pSocreBoadPlusText[3] = { nullptr, };
	CUI* pSocreBoadPlayerIcon[3] = { nullptr, };
	CUI* pSocreBoadButtonIcon[3] = { nullptr, };
	CUI* pSocreBoadCoinIcon[3] = { nullptr, };
	CUI* pSocreBoadStamp[3] = { nullptr, };

	CBillBoardObject* pBillBoardObject[2] = { nullptr, };
	CDice* pDice = nullptr;

	CBlackBackground* pBlack = nullptr;
	CBlackBackground* pScoreBlack = nullptr;
	CBillBoardObject* pHerePlayer = nullptr;
	CBillBoardObject* pConfetti = nullptr;
	CBillBoardObject* pBilStore = nullptr; // 상점 스프라이트
	CBillBoardObject* pBilMap = nullptr; // 맵 스프라이트

	CBillBoardObject* m_pPlus3Icon = nullptr;
	CBillBoardObject* m_pPlus5Icon = nullptr;

	CStaticObject* pSmokeObject[3] = { nullptr, };
	CStaticObject* pSmokeObject2[3] = { nullptr, };
	CStaticObject* pSmokeObject3[3] = { nullptr, };

	CUI* pJumpUI = nullptr;
	CUI* pRoadSignUI = nullptr;
	CUI* pDiceUI = nullptr;
	CUI* pMapUI = nullptr;
	CUI* pWhiteUI = nullptr;
	CUI* pWhiteUI2 = nullptr;
	CUI* pKeyWhiteUI = nullptr;
	CUI* pKeyWhiteUI2 = nullptr;
	CUI* pMiniTutoUI = nullptr;
	CFont* pKeyText = nullptr;
	CFont* pKeyText2 = nullptr;

	CFont* m_pPrice = nullptr;

	CCamera_Fixed* pCameraFixed = nullptr;
	CBird* pBird = nullptr;
	CGhost* pGhost = nullptr;

	_uint m_buttonIndex[4] = { 11, 22, 2, 19 };
	_uint m_beforebuttonIndex[4] = { 10, 23, 1, 18 };
	XMFLOAT3 m_lookbutton[4] = { XMFLOAT3(1.0f, 0.f, 0.f), XMFLOAT3(0.0f, 0.f, 1.f), XMFLOAT3(0.0f, 0.f, 1.f), XMFLOAT3(0.0f, 0.f, -1.f) };
	_uint m_nButtonRound = 0;

	CBalloon* m_pBalloon = nullptr;

	bool	m_isStartMoveBalloon = true;
	bool	m_isEnterBalloonFunc = false;
	bool	m_isBuyButton = false;

	bool	m_bMapCamera = false;
	XMFLOAT3 m_xmMapCameraPos;

	CButton* m_pButton = nullptr;

	_uint m_storeIndex[8] = {3, 10, 14, 18, 27, 29, 39, 49}; // 상점 발판
	_uint m_eventIndex[8] = {4, 8, 16, 24, 26, 31, 36, 47}; // 이벤트 발판 // 1,2,3,4,42,43
	CUI* m_pItems[4] = { nullptr, };
	_uint m_nItemPrice[4] = { 3, 20, 6, 10 };

	CUI* m_pBuyButton = nullptr;
	CUI* m_pXButton = nullptr;

	CUI* m_pBuyButtonforChat = nullptr;
	CUI* m_pXButtonforChat = nullptr;

	CUI* m_pChatBoxPurchase = nullptr;
	CUI* m_pChatBoxCoin = nullptr;

	CUI* pLastTurn = nullptr;

	float m_fChatBoxTimer = 0.f;
	bool m_bnBuyingButton = false;

	wstring m_strItemsName[4] = { L"GameObject_MiniDiceIcon", L"GameObject_ButtonIcon",
	L"GameObject_Plus3Icon", L"GameObject_Plus5Icon" };

	bool	m_bArriveStore = false;
	int	    m_iArriveEvent = 0;

	float	m_fEventTime = 0.f;
	float   m_fEventMonsterY;
	bool	m_bEventIsPlayer = false; // 이벤트 내가 발생 시킨 여부

	bool	m_isPushedButton = false;

	_uint	m_nStoreSelectItemIdx = 0;

	double m_dMSTime = 0.f; // 맵, 스토어 스프라이트 프레임 타임

	bool beforeShowMap = false;
	bool CurrentShowMap = false;

	double m_dScoreTime = 0.f; // 스코어 보드맵 보는 타임
	bool m_bNoUpdate = false; // 업데이트 안시키(키이벤트 x)

	CDice* m_pMiniDice = nullptr;
	CDice* m_pUsingMiniDice = nullptr;

	float m_fCameratime = 0.f;

	_uint m_nSelectItem = 0;

	bool m_bPushBuyButton = false;

	bool m_bShowCoinChatBox = false;
	float m_fStoreTimer = 0.f;

	bool m_bCheckButton = false;

	bool m_bPurchase = false;

	float m_fStoreEndTimer = 0.f;

	CButton* m_pStoreButton = nullptr;

	bool m_bBuyItemMotion = false;

	float m_fMiniTutoTime = 0.f;

	list<CUI*> m_listInven[3];
	XMFLOAT3 m_xmUIPos[9] = {};

	list<CUI*> m_listMyInven;

	bool m_bShowMyInven = false;
	bool m_bMyInvenEnd = false;
	bool m_bClickedInvenX = false;

	float m_fInvenSpeed = 1.f;

	CUI* m_pInvenXButton = nullptr;

	bool m_bUsingItemMotion = false;

	int m_nUsingItem = -1;

	//test 동점자 만들기
	float m_fTieCoolTime = 0.f;

	_uint m_nItemDiceNum = 0;

	int m_nEraseIndex = 0;

	bool m_bShowItemDiceNum = false;

	CBillBoardObject* m_pItemDiceBill = nullptr;
	CBillBoardObject* m_pPlusBill = nullptr;

	float m_fItemDiceTimer = 0.f;

	int m_iScorePlus[3];

	vector<CPlayer*> Rankers;

	float m_fScorePlusTime = 0.f;

	bool m_bMiniDiceSet = false;
	bool m_bShowStoreBubble = false;

	bool m_bScared = true; // 유령이 공격하고 넘어지는 플레그

	CStaticObject* m_pArrowHead = nullptr;

	bool m_bClickedButton = false;

	bool m_bBirdSound = false;


	int m_nScoreSound = -1;
	bool m_bScoreSound = false;
};

#endif