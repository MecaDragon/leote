#include "AnimShadowShader.h"
#include "Management.h"

CAnimShadowShader::CAnimShadowShader()
	: CShader()
{
}

CAnimShadowShader::CAnimShadowShader(const CAnimShadowShader& rhs)
	: CShader(rhs)
{
    m_pPipelineState->AddRef();
}

CAnimShadowShader::~CAnimShadowShader(void)
{
}

HRESULT CAnimShadowShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CAnimShadowShader::Create_PipeLineState();

	return NOERROR;
}

HRESULT CAnimShadowShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CAnimShadowShader::Clone_Component(void* pArg)
{
	CAnimShadowShader* pInstance = new CAnimShadowShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CAnimShadowShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CAnimShadowShader* CAnimShadowShader::Create()
{
	CAnimShadowShader* pInstance = new CAnimShadowShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CAnimShadowShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CAnimShadowShader::Create_VertexShader(ID3DBlob** ppBlob)
{
    return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shadows.hlsl", "AnimVS", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CAnimShadowShader::Create_PixelShader(ID3DBlob** ppBlob)
{
    return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shadows.hlsl", "AnimPS", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CAnimShadowShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

    nInputElementDescs = 7;
    pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
    pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[2] = { "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[4] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[5] = { "WEIGHTS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 56, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[6] = { "BONEINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, 68, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

D3D12_RASTERIZER_DESC CAnimShadowShader::Create_RasterizerState()
{
    D3D12_RASTERIZER_DESC d3dRasterizerDesc;
    ::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
    d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
    d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
    d3dRasterizerDesc.FrontCounterClockwise = TRUE;
    d3dRasterizerDesc.DepthBias = 1000;
    d3dRasterizerDesc.DepthBiasClamp = 0.0f;
    d3dRasterizerDesc.SlopeScaledDepthBias = 1.f;
    d3dRasterizerDesc.DepthClipEnable = TRUE;
    d3dRasterizerDesc.MultisampleEnable = FALSE;
    d3dRasterizerDesc.AntialiasedLineEnable = TRUE;
    d3dRasterizerDesc.ForcedSampleCount = 0;
    d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

    return(d3dRasterizerDesc);
}

D3D12_BLEND_DESC CAnimShadowShader::Create_BlendState()
{
    D3D12_BLEND_DESC d3dBlendDesc;
    ::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
    d3dBlendDesc.AlphaToCoverageEnable = FALSE;
    d3dBlendDesc.IndependentBlendEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
    d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
    d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
    d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
    d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
    d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
    d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
    d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

    return(d3dBlendDesc);
}

D3D12_DEPTH_STENCIL_DESC CAnimShadowShader::Create_DepthStencilState()
{
    D3D12_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
    ::ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
    d3dDepthStencilDesc.DepthEnable = TRUE;
    d3dDepthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
    d3dDepthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
    d3dDepthStencilDesc.StencilEnable = FALSE;
    d3dDepthStencilDesc.StencilReadMask = 0xff;
    d3dDepthStencilDesc.StencilWriteMask = 0xff;
    d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
    d3dDepthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

    return(d3dDepthStencilDesc);
}

ID3D12PipelineState* CAnimShadowShader::Create_PipeLineState()
{
    ID3D12PipelineState* pPipeLineState = nullptr;
    ID3DBlob* pd3dVertexShaderBlob = nullptr, * pd3dPixelShaderBlob = nullptr;

    D3D12_GRAPHICS_PIPELINE_STATE_DESC d3dPipelineStateDesc;
    ::ZeroMemory(&d3dPipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));

    d3dPipelineStateDesc.pRootSignature = CManagement::GetInstance()->Get_RootSignature();
    d3dPipelineStateDesc.VS = Create_VertexShader(&pd3dVertexShaderBlob);
    d3dPipelineStateDesc.PS = Create_PixelShader(&pd3dVertexShaderBlob);
    d3dPipelineStateDesc.RasterizerState = Create_RasterizerState();
    d3dPipelineStateDesc.BlendState = Create_BlendState();
    d3dPipelineStateDesc.DepthStencilState = Create_DepthStencilState();
    d3dPipelineStateDesc.InputLayout = Create_InputLayout();
    d3dPipelineStateDesc.SampleMask = UINT_MAX;
    d3dPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE; // topology type도 enum 값에 따라 다르게 설정
    d3dPipelineStateDesc.NumRenderTargets = 0;
    d3dPipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_UNKNOWN;
    d3dPipelineStateDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
    d3dPipelineStateDesc.SampleDesc.Count = 1;
    d3dPipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
    HRESULT hResult = CDeviceManager::GetInstance()->Get_Device()->CreateGraphicsPipelineState(&d3dPipelineStateDesc,
        __uuidof(ID3D12PipelineState), (void**)&pPipeLineState);

    if (pd3dVertexShaderBlob) pd3dVertexShaderBlob->Release();
    if (pd3dPixelShaderBlob) pd3dPixelShaderBlob->Release();

    if (d3dPipelineStateDesc.InputLayout.pInputElementDescs) delete[] d3dPipelineStateDesc.InputLayout.pInputElementDescs;

    return pPipeLineState;
}

HRESULT CAnimShadowShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
