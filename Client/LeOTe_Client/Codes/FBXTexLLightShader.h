#ifndef __FBXTexLLightShader__
#define __FBXTexLLightShader__

#include "Shader.h"

class CFBXTexLLightShader final : public CShader
{
private:
	explicit CFBXTexLLightShader();
	explicit CFBXTexLLightShader(const CFBXTexLLightShader& rhs);
	virtual ~CFBXTexLLightShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CFBXTexLLightShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();
	virtual D3D12_BLEND_DESC Create_BlendState();

};

#endif
