#include "Logo.h"
#include "Shader.h"
#include "Transform.h"
#include "Management.h"
#include "Plane_Buffer.h"
#include "Texture.h"
#include "PipeLine.h"
#include "Renderer.h"

CLogo::CLogo()
{
}

CLogo::CLogo(const CLogo& rhs)
{
}

HRESULT CLogo::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CLogo::Initialize_GameObject(void* pArg)
{
	ID3D12Device* pDevice = CDeviceManager::GetInstance()->Get_Device();

	if(FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	m_pTransformCom->SetScale(0.01f, 1.f, 0.1f);
	m_pTransformCom->SetPosition(0.f, 0.f, 0.1f);
	m_pGameObjResource->SetName(L"Logo Object m_pGameObjResource");
	return NOERROR;
}

short CLogo::Update_GameObject(double TimeDelta)
{

	return 0;
}

short CLogo::LateUpdate_GameObject(double TimeDelta)
{
	CPipeLine::GetInstance()->Set_ProjectionMatrix(ORTHOGRAPHIC_PROJ);

	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return 0;
}

HRESULT CLogo::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);
	m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform);
	m_pTextureCom->UpdateShaderVariables(pCommandList);

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pPlane_BufferCom->Render_Buffer();
	CPipeLine::GetInstance()->Set_ProjectionMatrix(PERSPECTIVE_PROJ);
	return NOERROR;
}

CGameObject* CLogo::Clone_GameObject(void* pArg)
{
	CLogo* pInstance = new CLogo(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CLogo Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CLogo* CLogo::Create()
{
	CLogo* pInstance = new CLogo();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CLogo Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CLogo::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pPlane_BufferCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}

HRESULT CLogo::Add_Component()
{

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Texture", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_PlaneBuffer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Plane_Buffer", L"Com_Plane_Buffer", (CComponent**)&m_pPlane_BufferCom)))
		return E_FAIL;
	
	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_LOGO, L"Component_Texture_LogoBackground", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;


	return NOERROR;
}
