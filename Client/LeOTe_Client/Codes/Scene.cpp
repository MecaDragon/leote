#include "Scene.h"

CScene::CScene()
{
}

CScene::~CScene()
{
}

HRESULT CScene::Initialize_Scene()
{
	return NOERROR;
}

short CScene::Update_Scene(double TimeDelta)
{
	return 0;
}

short CScene::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene::Render_Scene()
{
	return NOERROR;
}

short CScene::KeyEvent(double TimeDelta)
{
	return 0;
}

CGameObject* CScene::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz)
{
	return nullptr;
}

CGameObject* CScene::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	return nullptr;
}

HRESULT CScene::Release()
{
	delete this;

	return NOERROR;
}
