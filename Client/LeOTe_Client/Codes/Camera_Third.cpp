#include "Camera_Third.h"
#include "KeyManager.h"
#include "DeviceManager.h"
#include "Transform.h"
#include "Player.h"
#include "Management.h"

CCamera_Third::CCamera_Third() : CCamera()
{
	m_pPlayer = NULL;
}

CCamera_Third::CCamera_Third(const CCamera_Third& rhs) : CCamera(rhs)
{
}

HRESULT CCamera_Third::Initialize_GameObject_Prototype()
{
	m_pd3dcbCamera->SetName(L"Camera_Third Resource");
	return NOERROR;
}

HRESULT CCamera_Third::Initialize_GameObject(void* pArg)
{
	m_pd3dcbCamera->SetName(L"Camera_Third Resource");
	return NOERROR;
}

short CCamera_Third::Update_GameObject(double TimeDelta)
{
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();
	CManagement* pManagement = CManagement::GetInstance();
	GenerateFrustum();
	//if (pManagement->Get_CurrentSceneId() == SCENE_BOARDMAP)
	{
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

		if (m_pPlayer)
		{
			XMFLOAT4X4 xmf4x4Rotate = Matrix4x4::Identity();			
			XMFLOAT3 xmf3Up = m_pPlayer->GetUpVector();
			XMFLOAT3 xmf3Right, xmf3Look;

			xmf3Right = m_pPlayer->GetRightVector();
			xmf3Look = m_pPlayer->GetLookVector();
			
			xmf4x4Rotate._11 = xmf3Right.x; xmf4x4Rotate._21 = xmf3Up.x; xmf4x4Rotate._31 = xmf3Look.x;
			xmf4x4Rotate._12 = xmf3Right.y; xmf4x4Rotate._22 = xmf3Up.y; xmf4x4Rotate._32 = xmf3Look.y;
			xmf4x4Rotate._13 = xmf3Right.z; xmf4x4Rotate._23 = xmf3Up.z; xmf4x4Rotate._33 = xmf3Look.z;

			XMFLOAT3 xmf3Offset = Vector3::TransformCoord(m_xmf3Offset, xmf4x4Rotate);
			XMFLOAT3 xmf3Position = Vector3::Add(m_pPlayer->GetPosition(), xmf3Offset);
			XMFLOAT3 xmf3Direction = Vector3::Subtract(xmf3Position, m_tCameraInfo.xmPosition);
			float fLength = Vector3::Length(xmf3Direction);
			xmf3Direction = Vector3::Normalize(xmf3Direction);
			float fDistance = fLength * 1.f;
			if (fDistance > fLength) fDistance = fLength;
			if (fLength < 0.01f) fDistance = fLength;
			if (fDistance > 0)
			{
				m_tCameraInfo.xmPosition = Vector3::Add(m_tCameraInfo.xmPosition, xmf3Direction, fDistance);
				SetLookAt(m_pPlayer->GetPosition());
			}
		}

		Set_Position(m_tCameraInfo.xmPosition);
	}
	return 0;
}

void CCamera_Third::Rotate(float x, float y, float z)
{
	if (x != 0.0f)
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tCameraInfo.xmRight), XMConvertToRadians(x));
		m_tCameraInfo.xmLook = Vector3::TransformNormal(m_tCameraInfo.xmLook, xmmtxRotate);
		m_tCameraInfo.xmUp = Vector3::TransformNormal(m_tCameraInfo.xmUp, xmmtxRotate);
		m_tCameraInfo.xmRight = Vector3::TransformNormal(m_tCameraInfo.xmRight, xmmtxRotate);
	}
	if ((y != 0.0f))
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tCameraInfo.xmUp), XMConvertToRadians(y));
		m_tCameraInfo.xmLook = Vector3::TransformNormal(m_tCameraInfo.xmLook, xmmtxRotate);
		m_tCameraInfo.xmUp = Vector3::TransformNormal(m_tCameraInfo.xmUp, xmmtxRotate);
		m_tCameraInfo.xmRight = Vector3::TransformNormal(m_tCameraInfo.xmRight, xmmtxRotate);
	}
	if ((z != 0.0f))
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_tCameraInfo.xmLook), XMConvertToRadians(z));
		m_tCameraInfo.xmPosition = Vector3::Subtract(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt);
		m_tCameraInfo.xmPosition = Vector3::TransformCoord(m_tCameraInfo.xmPosition, xmmtxRotate);
		m_tCameraInfo.xmPosition = Vector3::Add(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt);
		m_tCameraInfo.xmLook = Vector3::TransformNormal(m_tCameraInfo.xmLook, xmmtxRotate);
		m_tCameraInfo.xmUp = Vector3::TransformNormal(m_tCameraInfo.xmUp, xmmtxRotate);
		m_tCameraInfo.xmRight = Vector3::TransformNormal(m_tCameraInfo.xmRight, xmmtxRotate);
	}
}

void CCamera_Third::SetLookAt(XMFLOAT3& xmf3LookAt)
{
	XMFLOAT4X4 mtxLookAt = Matrix4x4::LookAtLH(m_tCameraInfo.xmPosition, xmf3LookAt, m_pPlayer->GetUpVector());
	Set_ViewMatrix(mtxLookAt);
	m_tCameraInfo.xmRight = XMFLOAT3(mtxLookAt._11, mtxLookAt._21, mtxLookAt._31);
	m_tCameraInfo.xmUp = XMFLOAT3(mtxLookAt._12, mtxLookAt._22, mtxLookAt._32);
	m_tCameraInfo.xmLook = XMFLOAT3(mtxLookAt._13, mtxLookAt._23, mtxLookAt._33);
}

short CCamera_Third::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CCamera_Third::Render_GameObject()
{
	return NOERROR;
}

CCamera_Third* CCamera_Third::Create()
{
	CCamera_Third* pInstance = new CCamera_Third();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CCamera_Third Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CCamera_Third::Clone_GameObject(void* pArg)
{
	CCamera_Third* pInstance = new CCamera_Third(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CCamera_Third Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CCamera_Third::Release()
{
	CCamera::Release();

	delete this;

	return NOERROR;
}
