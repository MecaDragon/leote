#ifndef __KEYMANAGER__
#define __KEYMANAGER__

#include "stdafx.h"

const DWORD KEY_UP = 0x00000001;	//0000 0001
const DWORD KEY_DOWN = 0x00000002;	//0000 0010
const DWORD KEY_LEFT = 0x00000004;	//0000 0100
const DWORD KEY_RIGHT = 0x00000008;	//0000 1000

const DWORD KEY_LBUTTON = 0x00000010; //0001 0000
const DWORD KEY_RBUTTON = 0x00000020; //0010 0000

const DWORD KEY_W = 0x00000040;
const DWORD KEY_A = 0x00000080;
const DWORD KEY_S = 0x00000100;
const DWORD KEY_D = 0x00000200;

const DWORD KEY_SPACE = 0x00000400;
const DWORD KEY_SHIFT = 0x00000800;
const DWORD KEY_F1 = 0x00001000;
const DWORD KEY_ENTER = 0x00002000;

const DWORD KEY_M = 0x00004000;
const DWORD KEY_1 = 0x00008000;
const DWORD KEY_2 = 0x00010000;
const DWORD KEY_3 = 0x00020000;

const DWORD KEY_4 = 0x00040000;
const DWORD KEY_5 = 0x00080000;

class CKeyManager final
{
	DECLARE_SINGLETON(CKeyManager)

private:
	explicit CKeyManager();
	virtual ~CKeyManager();

public:
	void UpdateKey();

public:
	bool KeyDown(DWORD dwCurKey);
	bool KeyUp(DWORD dwCurKey);
	bool KeyPressing(DWORD dwCurKey);
	bool KeyCombined(DWORD dwFirstKey, DWORD dwSecondKey);

	HRESULT Release();

private:
	DWORD m_dwCurKey;
	DWORD m_dwKeyUp;
	DWORD m_dwKeyDown;
};

#endif // !__KEYMANAGER__

