#include "stdafx.h"
#include "SantaHat.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"

CSantaHat::CSantaHat()
	: CGameObject()
{

}

CSantaHat::CSantaHat(const CSantaHat& rhs)
	: CGameObject(rhs)
{
}

HRESULT CSantaHat::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CSantaHat::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };

	m_xmObjPosition = XMFLOAT3(0.f, 0.f, 117.f);
	m_pTransformCom->SetPosition(0.f, 0.f, 110.f);
	m_pTransformCom->Rotate(0.f, 90.f, 90.f);
	m_pTransformCom->SetScale(8.f, 8.f, 8.f);

	return NOERROR;
}

short CSantaHat::Update_GameObject(double TimeDelta)
{
	return short();
}

short CSantaHat::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CSantaHat::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMFLOAT4X4 worldMat = Matrix4x4::Multiply(m_pTransformCom->GetWorldMat(), m_pTransformCom->GetParentsWorldMat());
	

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(worldMat))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CSantaHat::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_SantaHat_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader //Component_Shader_FBXTexLight
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXTexLight", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Texture_SantaHat", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CSantaHat::SetUp_ConstantTable()
{
	return NOERROR;
}

CSantaHat* CSantaHat::Create()
{
	CSantaHat* pInstance = new CSantaHat();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CSantaHat Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CSantaHat::Clone_GameObject(void* pArg)
{
	CSantaHat* pInstance = new CSantaHat(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CSantaHat Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CSantaHat::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
