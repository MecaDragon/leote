#include "stdafx.h"
#include "StaticObject.h"
#include "Management.h"
#include "Material.h"
#include "ShadowShader.h"
#include "Texture.h"
#include "Renderer.h"
#include "Player.h"

extern CPlayer* Player;

CStaticObject::CStaticObject()
	: CGameObject()
{

}

CStaticObject::CStaticObject(const CStaticObject& rhs)
	: CGameObject(rhs)
	, m_ComName(rhs.m_ComName)
{
}

HRESULT CStaticObject::Initialize_GameObject_Prototype(ComName comName)
{
	m_ComName = comName;
	return NOERROR;
}

HRESULT CStaticObject::Initialize_GameObject(void * pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	//m_pMaterialCom->SetDiffuseRGBA(256, 256, 256, 256);
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
	}
	return NOERROR;
}

short CStaticObject::Update_GameObject(double TimeDelta)
{

	return short();
}

short CStaticObject::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	m_BoundingBox = m_pMeshCom->boudingBox;
	m_BoundingBox.Transform(m_BoundingBox, XMLoadFloat4x4(&m_pTransformCom->GetWorldMat()));
	if (CCameraManager::GetInstance()->GetCurrentCamera()->IsInFrustum(m_BoundingBox)) {

		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
			return -1;
	}

	return short();
}

HRESULT CStaticObject::Render_Shadow()
{
	if (m_ComName.ShadowRender)
	{
		ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

		m_pShadowShaderCom->SetPipeline(pCommandList);

		XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

		m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());

		D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
		pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

		if (m_pTextureCom)
		{
			m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_DIFFUSE, texid);
		}
		m_pMeshCom->Render();
	}
	return NOERROR;
}

HRESULT CStaticObject::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	if (m_pTextureCom)
		m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_DIFFUSE, texid);
	if (m_bRender)
	{
		m_pMeshCom->Render();
	}
	return NOERROR;
}

HRESULT CStaticObject::Add_Component()
{
	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(m_ComName.SceneName, m_ComName.MeshComStr.c_str(), L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, m_ComName.ShaderComStr.c_str(), L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	m_pMaterialCom->SetDiffuseRGBA(m_ComName.MatrialRGB.x, m_ComName.MatrialRGB.y, m_ComName.MatrialRGB.z, 256);

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	if (m_ComName.TexComStr.empty() == false) {
		// For.Com_Texture
		if (FAILED(CGameObject::Add_Component(m_ComName.SceneName, m_ComName.TexComStr.c_str(), L"Com_Texture", (CComponent**)&m_pTextureCom)))
			return E_FAIL;
	}

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CStaticObject::SetUp_ConstantTable()
{
	return NOERROR;
}

CStaticObject* CStaticObject::Create(SCENEID SceneName, wstring TexComStr, wstring MeshComStr, wstring ShaderComStr, XMINT3 MatrialRGB, bool ShadowRender)
{
	CStaticObject*		pInstance = new CStaticObject();
	ComName comName = { SceneName, TexComStr, MeshComStr, ShaderComStr, MatrialRGB, ShadowRender };
	if (pInstance->Initialize_GameObject_Prototype(comName))
	{
		MSG_BOX("CStaticObject Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject * CStaticObject::Clone_GameObject(void * pArg)
{
	CStaticObject*		pInstance = new CStaticObject(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CStaticObject Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CStaticObject::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pMaterialCom);
	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
