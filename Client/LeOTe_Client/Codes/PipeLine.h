#ifndef __PIPELINE__
#define __PIPELINE__

#include "stdafx.h"

class CPipeLine final
{
	DECLARE_SINGLETON(CPipeLine)

private:
	explicit CPipeLine();
	virtual ~CPipeLine();

public:
	void Set_ViewMatrix(const XMFLOAT4X4& Matrix);
	void Set_ProjectionMatrix(PROJECTION_TYPE eProjType);
	void Set_PerspectiveProjMatrix(const XMFLOAT4X4& Matrix) { m_PerspectiveProjMatrix = Matrix; }
	void Set_OrthographicProjMatrix(const XMFLOAT4X4& Matrix) { m_OrthographicProjMatrix = Matrix; }
	void Set_ScissorRect(const D3D12_RECT& ScissorRect) { m_ScissorRect = ScissorRect; }
	void Set_Viewport(const D3D12_VIEWPORT& Viewport) { m_Viewport = Viewport; }
	void Set_MappedCamera(VS_CB_CAMERA_INFO* pMappedCamera) { m_pcbMappedCamera = pMappedCamera; }
	void Set_Position(const XMFLOAT3& position) { m_CameraPosition = position; }
	void Set_CameraResource(ID3D12Resource* pd3dcbCamera) { m_pd3dcbCamera = pd3dcbCamera; }

	XMFLOAT4X4 Get_ViewMatrix() { return m_ViewMatrix; }
	XMFLOAT4X4 Get_ViewInverseMatrix() { return m_ViewInverseMatrix; }
	XMFLOAT4X4 Get_ProjectionMatrix() { return m_ProjectionMatrix; }

	void Set_ViewportsAndScissorRects(ID3D12GraphicsCommandList* pCommandList);
	void UpdateShaderVariables(ID3D12GraphicsCommandList* pCommandList);

	HRESULT Release();

private:
	XMFLOAT4X4			m_ViewMatrix;
	XMFLOAT4X4			m_ViewInverseMatrix;
	XMFLOAT4X4			m_ProjectionMatrix;
	XMFLOAT4X4			m_PerspectiveProjMatrix;
	XMFLOAT4X4			m_OrthographicProjMatrix;
	XMFLOAT3			m_CameraPosition;
	D3D12_RECT			m_ScissorRect;
	D3D12_VIEWPORT		m_Viewport;

	VS_CB_CAMERA_INFO*	m_pcbMappedCamera = nullptr;
	ID3D12Resource*		m_pd3dcbCamera = nullptr;
};

#endif