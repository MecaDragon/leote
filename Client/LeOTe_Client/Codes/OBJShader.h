#ifndef __OBJSHADER__
#define __OBJSHADER__

#include "Shader.h"

class COBJShader final : public CShader
{
private:
	explicit COBJShader();
	explicit COBJShader(const COBJShader& rhs);
	virtual ~COBJShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	COBJShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();

};

#endif
