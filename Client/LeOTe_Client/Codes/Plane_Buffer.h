#ifndef __PLANE_BUFFER__
#define __PLANE_BUFFER__

#include "stdafx.h"
#include "Buffer.h"

class CPlane_Buffer : public CBuffer
{
private:
	explicit CPlane_Buffer();
	explicit CPlane_Buffer(const CPlane_Buffer& rhs);
	virtual ~CPlane_Buffer(void) = default;

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	HRESULT Render_Buffer();

	virtual CComponent* Clone_Component(void* pArg);

	static CPlane_Buffer* Create();

	HRESULT Release();

private:
	

};

#endif