#ifndef __DEFINES__
#define __DEFINES__

#include <process.h>

//김지은 : 25.37.169.34
//김선중 : 25.48.188.204
//이소정 : 25.46.234.64
#define SERVERIP G_IP

#define WINDOWX 1280
#define WINDOWY 720

//서벼 연결 여부
#define SERVERCONNECT G_SC
#define PLAYERSPEED 6.f
#define MOVEPACKETDELTA 0.04f
#define DELETESPEED 0.06f

// teddybear y값
#define TEDDYBEAR_PIVOT 0.8f
// 중력
#define GRAVITY 9.8f

enum SCENEID { SCENE_LOGO, SCENE_LOGIN, SCENE_WAITINGROOM, SCENE_BOARDMAP, SCENE_FALLINGMAP, SCENE_GGOGGOMAP, SCENE_BULLFIGHTMAP, SCENE_ONEMINDMAP, SCENE_PRIZE, SCENE_STATIC, SCENE_ALL, SCENE_END };
// 커맨드 리스트 목록 
enum COMMANDID { COMMAND_RENDER, COMMAND_LOAD, COMMAND_END };
// 투영행렬 종류
enum PROJECTION_TYPE { PERSPECTIVE_PROJ, ORTHOGRAPHIC_PROJ };
// 서버 상황
enum LOGIN_STATE { IP, ID, END };
// 오브젝트 타입
enum OBJECT_TYPE { OBJTYPE_MOVABLE, OBJTYPE_FIXED, OBJTYPE_END };
// 오브젝트 상태
enum OBJECT_STATE { OBJSTATE_GROUND, OBJSTATE_FALLING, OBJSTATE_FLY, OBJSTATE_EMO, OBJSTATE_END };

extern HWND g_hWnd;
extern HINSTANCE g_hInst;

//힙 크기
extern UINT g_CbvSrvDescriptorIncrementSize;
extern UINT g_RtvDescriptorIncrementSize;
extern UINT g_DsvDescriptorIncrementSize;

#define MSG_BOX(_TEXT) MessageBox(NULL, TEXT(_TEXT),L"System Message",MB_OK)
#define	MSG_BOX_WSTR(_message)			MessageBox(NULL, _message, L"System Message", MB_OK)

#define NO_COPY(CLASSNAME)						\
	private:									\
	CLASSNAME(const CLASSNAME&);				\
	CLASSNAME& operator = (const CLASSNAME&);	\

#define DECLARE_SINGLETON(CLASSNAME)	\
	NO_COPY(CLASSNAME)					\
	private:							\
	static CLASSNAME* m_pInstance;		\
	public:								\
	static CLASSNAME* GetInstance();	\
	static bool DestroyInstance();		\

#define IMPLEMENT_SINGLETON(CLASSNAME)			\
	CLASSNAME* CLASSNAME::m_pInstance = NULL;	\
	CLASSNAME* CLASSNAME::GetInstance()			\
	{											\
		if(!m_pInstance)						\
		{										\
			m_pInstance	= new CLASSNAME;		\
		}										\
		return m_pInstance;						\
	}											\
	bool CLASSNAME::DestroyInstance()			\
	{											\
		if(m_pInstance != nullptr &&			\
			FAILED(m_pInstance->Release()))		\
		{										\
			m_pInstance = nullptr;				\
			return true;						\
		}										\
		return false;							\
	}

#endif // !__DEFINES__
