#include "GameObject.h"
#include "Management.h"

CGameObject::CGameObject()
{
}

CGameObject::CGameObject(const CGameObject& rhs)
{
}

CGameObject::~CGameObject()
{
}

HRESULT CGameObject::Initialize_GameObject_Prototype()
{
    return NOERROR;
}

HRESULT CGameObject::Initialize_GameObject(void* pArg)
{
    return NOERROR;
}

short CGameObject::Update_GameObject(double TimeDelta)
{
    return 0;
}

short CGameObject::LateUpdate_GameObject(double TimeDelta)
{
    return 0;
}

HRESULT CGameObject::Render_Shadow()
{
    return NOERROR;
}

HRESULT CGameObject::Render_GameObject()
{
    return NOERROR;
}

HRESULT CGameObject::Add_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, const wchar_t* pComponentTag, CComponent** ppComponent, void* pArg)
{
    if (nullptr != Find_Component(pComponentTag))
        return E_FAIL;

    CManagement* pManagement = CManagement::GetInstance();

    if (nullptr == pManagement)
        return E_FAIL;

    if (FAILED(pManagement->Clone_Component(iSceneIndex, pPrototypeTag, ppComponent, pArg)))
        return E_FAIL;

    m_mapComponents.insert(COMPONENTS::value_type(pComponentTag, *ppComponent));

    return NOERROR;
}

HRESULT CGameObject::Compute_ViewZ(XMFLOAT3 vWorldPos)
{
    CPipeLine* pPipeLine = CPipeLine::GetInstance();
    if (nullptr == pPipeLine)
        return E_FAIL;

   
    XMFLOAT4X4	cameraMatrix = pPipeLine->Get_ViewInverseMatrix();
    XMFLOAT3	vCamPosition = *(XMFLOAT3*)&cameraMatrix.m[3][0];
    XMFLOAT3 result;

    XMStoreFloat3(&result, XMLoadFloat3(&vWorldPos) - XMLoadFloat3(&vCamPosition));

    m_fViewZ = Vector3::Length(result);

    return NOERROR;
}

CGameObject* CGameObject::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz)
{
    CManagement* pManagement = CManagement::GetInstance();

    CGameObject* obj = nullptr;
    ObjWorld temp;
    temp.position.x = x;
    temp.position.y = y;
    temp.position.z = z;

    temp.rotation.x = rx;
    temp.rotation.y = ry;
    temp.rotation.z = rz;
    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_BOARDMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
        return nullptr;
    return obj;

}

CComponent* CGameObject::Find_Component(const wchar_t* pComponentTag)
{
    auto iter = find_if(m_mapComponents.begin(), m_mapComponents.end(), CTag_Finder(pComponentTag));

    if (iter == m_mapComponents.end())
        return nullptr;


    return iter->second;
}


HRESULT CGameObject::Release()
{
    delete this;

    return NOERROR;
}
