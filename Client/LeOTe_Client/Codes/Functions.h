#ifndef __FUNCTIONS__
#define __FUNCTIONS__

template<class T>
bool Safe_Release(T& pInstance)
{
	if (pInstance != nullptr)
	{
		if (SUCCEEDED(pInstance->Release()))
		{
			pInstance = nullptr;
			return true;
		}
		else
		{
			MessageBox(NULL, L"Safe_Release Failed", L"System Message", MB_OK);
			return false;
		}
	}

	return true;
}


class CTag_Finder
{
public:
	explicit CTag_Finder(const wchar_t* pTag)
		:m_pTargetTag(pTag) {}
	~CTag_Finder() {}
public:
	template<typename T>
	bool operator()(const T& pair)
	{
		if (0 == lstrcmpW(m_pTargetTag, pair.first))
			return true;
		
		return false;
	}
private:
	const wchar_t* m_pTargetTag = nullptr;
};

#endif // !__FUNCTIONS__
