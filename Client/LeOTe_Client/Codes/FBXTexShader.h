#ifndef __FBXTEXSHADER__
#define __FBXTEXSHADER__

#include "Shader.h"

class CFBXTexShader final : public CShader
{
private:
	explicit CFBXTexShader();
	explicit CFBXTexShader(const CFBXTexShader& rhs);
	virtual ~CFBXTexShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CFBXTexShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();
	virtual D3D12_BLEND_DESC Create_BlendState();

};

#endif
