#include "TextureShader.h"

CTextureShader::CTextureShader()
	: CShader()
{
}

CTextureShader::CTextureShader(const CTextureShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

CTextureShader::~CTextureShader(void)
{
}

HRESULT CTextureShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"Texture Shader PipelineState");
	return NOERROR;
}

HRESULT CTextureShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CTextureShader::Clone_Component(void* pArg)
{
	CTextureShader* pInstance = new CTextureShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CTextureShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CTextureShader* CTextureShader::Create()
{
	CTextureShader* pInstance = new CTextureShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CTextureShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CTextureShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shaders.hlsl", "VSTextured", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CTextureShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shaders.hlsl", "PSTextured", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CTextureShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 2;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

HRESULT CTextureShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
