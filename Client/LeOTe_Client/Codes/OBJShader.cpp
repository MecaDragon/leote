#include "OBJShader.h"

COBJShader::COBJShader()
	: CShader()
{
}

COBJShader::COBJShader(const COBJShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

COBJShader::~COBJShader(void)
{
}

HRESULT COBJShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"OBJ Shader PipelineState");
	return NOERROR;
}

HRESULT COBJShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* COBJShader::Clone_Component(void* pArg)
{
	COBJShader* pInstance = new COBJShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("COBJShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

COBJShader* COBJShader::Create()
{
	COBJShader* pInstance = new COBJShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("COBJShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE COBJShader::Create_VertexShader(ID3DBlob** ppBlob)
{
    return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shaders.hlsl", "VSLighting", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE COBJShader::Create_PixelShader(ID3DBlob** ppBlob)
{
    return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Shaders.hlsl", "PSLighting", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC COBJShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

    nInputElementDescs = 4;
    pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
    pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[2] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

HRESULT COBJShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
