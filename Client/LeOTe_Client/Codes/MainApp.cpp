#include "MainApp.h"
#include "Scene.h"
#include "Scene_Logo.h"
#include "Renderer.h"
#include "ServerManager.h"

bool G_SC;
string G_IP = "";

CMainApp::CMainApp()
{
}

CMainApp::~CMainApp()
{
}

HRESULT CMainApp::Initialize_MainApp()
{
	ifstream in{ "서버유무.txt" };
	vector<string> v{ istream_iterator<string>{ in }, {} };
	if (v[0] == "true")
	{
		G_SC = true;
		if (v[1].empty() == false)
			G_IP = v[1];
	} else {
		G_SC = false;
	}

	m_pManagement = CManagement::GetInstance();
	
 	Initialize_Device();

	m_pServerManager = CServerManager::GetInstance();

	if(FAILED(Ready_Component_Static()))
		return E_FAIL;

	if (FAILED(Initialize_Start_Scene(SCENE_LOGO)))
		return E_FAIL;

	return NOERROR;
}

short CMainApp::Update_MainApp(double TimeDelta)
{
	m_TimerAcc += TimeDelta;

	CKeyManager::GetInstance()->UpdateKey();

	m_pManagement->Update_Management(TimeDelta);

	return 0;
}

short CMainApp::LateUpdate_MainApp(double TimeDelta)
{
	m_pManagement->LateUpdate_Management(TimeDelta);

	m_pServerManager->RecvPacket();
	return 0;
}

HRESULT CMainApp::Render_MainApp()
{
 	CDeviceManager::GetInstance()->Render_Start(COMMAND_RENDER);

	m_pManagement->Render_Shadow();

	m_pManagement->Render_Management();
	CDeviceManager::GetInstance()->Render_Middle(COMMAND_RENDER);
	m_pRenderer->Draw_RenderGroup();

	CDeviceManager::GetInstance()->Render_End(COMMAND_RENDER);

	++m_iRenderCall;

	if (1.0 <= m_TimerAcc)
	{
		wsprintf(m_szFPS, L"LeOTe FPS:%d", m_iRenderCall);
		m_iRenderCall = 0;
		m_TimerAcc = 0.0;
	}

	SetWindowText(g_hWnd, m_szFPS);

	return NOERROR;
}

HRESULT CMainApp::Release()
{
	// Release
	// 접속 종료를 알림
	packet_leave p;
	p.id = CServerManager::GetInstance()->Get_ClientIndex();
	p.size = sizeof(p);
	p.type = C2S_LEAVEPLAYER;
	CServerManager::GetInstance()->SendData(C2S_LEAVEPLAYER, &p);
	m_pManagement->Release();

	delete this;

	return NOERROR;
}

LRESULT CMainApp::Login_IP(WPARAM wParam)
{	
	switch (wParam)
	{
	case VK_BACK:
		if (m_strIP.length() != 0)
			m_strIP.pop_back();
		break;
	case VK_RETURN:
		m_pServerManager->SetServerIP(m_strIP);
		break;

	default:
		m_strIP.push_back(wParam);
		break;
	}

	system("cls");

	return LRESULT();
}

LRESULT CMainApp::Login_ID(WPARAM wParam)
{
	if(m_pServerManager->Get_LoginState() == LOGIN_STATE::END)
		return LRESULT();

	switch (wParam)
	{
	case VK_BACK:
		if (m_strID.length() != 0)
			m_strID.pop_back();
		break;
	case VK_RETURN:
		if (!m_pServerManager->Get_SendID())
		{
			m_pServerManager->SetServerID(m_strID);

			packet_login p;
			p.size = sizeof(packet_login);
			p.type = C2S_LOGIN;
			strcpy_s(p.name, m_strID.c_str());

			m_pServerManager->SendData(C2S_LOGIN, &p);

			m_pServerManager->Set_SendID(true);
			m_strID.clear();
		}
		break;

	default:
		m_strID.push_back(wParam);
		break;
	}

	m_pManagement->Set_InputID(m_strID);

	return LRESULT();
}

HRESULT CMainApp::Initialize_Device()
{
	if(FAILED(m_pManagement->Initialize_Management(SCENE_END)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CMainApp::Initialize_Start_Scene(SCENEID eSceneId)
{
	CScene* pScene = nullptr;

	switch (eSceneId)
	{
	case SCENE_LOGO:
		pScene = CScene_Logo::Create();
		break;
	case SCENE_BOARDMAP:
		break;
	}

	if (nullptr == pScene)
		return E_FAIL;

	if (FAILED(m_pManagement->Initialize_Current_Scene(pScene)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CMainApp::Ready_Component_Static()
{
	if (nullptr == m_pManagement)
		return E_FAIL;

	// For.Component_Renderer
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Renderer", m_pRenderer = CRenderer::Create())))
		return E_FAIL;

	return NOERROR;
}

CMainApp* CMainApp::Create()
{
	CMainApp* pInstance = new CMainApp;

	if (FAILED(pInstance->Initialize_MainApp()))
	{
		MSG_BOX("CMainApp Created Failed");
		return nullptr;
	}

	return pInstance;
}
