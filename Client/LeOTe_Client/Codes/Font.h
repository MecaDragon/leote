#include "stdafx.h"
#include "Defines.h"
#include "GameObject.h"
#include "Camera.h"
#include "TextureLoader.h"

class CPipeLine;
class CTransform;
class CShader;
class CCamera;
class CRenderer;

struct FontChar
{
	// the unicode id
	int id;

	// these need to be converted to texture coordinates 
	// (where 0.0 is 0 and 1.0 is textureWidth of the font)
	float u; // u texture coordinate
	float v; // v texture coordinate
	float twidth; // width of character on texture
	float theight; // height of character on texture

	float width; // width of character in screen coords
	float height; // height of character in screen coords

	// these need to be normalized based on size of font
	float xoffset; // offset from current cursor pos to left side of character
	float yoffset; // offset from top of line to top of character
	float xadvance; // how far to move to right for next character
};

struct FontKerning
{
	int firstid; // the first character
	int secondid; // the second character
	float amount; // the amount to add/subtract to second characters x
};

struct TextVertex 
{
	TextVertex(float r, float g, float b, float a, float u, float v, float tw, float th, float x, float y, float w, float h) : color(r, g, b, a), texCoord(u, v, tw, th), pos(x, y, w, h) {}
	XMFLOAT4 pos;
	XMFLOAT4 texCoord;
	XMFLOAT4 color;
};

struct Font
{
	std::wstring name; // name of the font
	std::wstring fontImage;
	int size; // size of font, lineheight and baseheight will be based on this as if this is a single unit (1.0)
	float lineHeight; // how far to move down to next line, will be normalized
	float baseHeight; // height of all characters, will be normalized
	int textureWidth; // width of the font texture
	int textureHeight; // height of the font texture
	int numCharacters; // number of characters in the font
	FontChar* CharList; // list of characters
	int numKernings; // the number of kernings
	FontKerning* KerningsList; // list to hold kerning values
	ID3D12Resource* textureBuffer; // the font texture resource
	CD3DX12_GPU_DESCRIPTOR_HANDLE srvHandle; // the font srv

	// these are how much the character is padded in the texture. We
	// add padding to give sampling a little space so it does not accidentally
	// padd the surrounding characters. We will need to subtract these paddings
	// from the actual spacing between characters to remove the gaps you would otherwise see
	float leftpadding;
	float toppadding;
	float rightpadding;
	float bottompadding;

	// this will return the amount of kerning we need to use for two characters
	float GetKerning(wchar_t first, wchar_t second)
	{
		for (int i = 0; i < numKernings; ++i)
		{
			if ((wchar_t)KerningsList[i].firstid == first && (wchar_t)KerningsList[i].secondid == second)
				return KerningsList[i].amount;
		}
		return 0.0f;
	}

	// this will return a FontChar given a wide character
	FontChar* GetChar(wchar_t c)
	{
		for (int i = 0; i < numCharacters; ++i)
		{
			if (c == (wchar_t)CharList[i].id)
				return &CharList[i];
		}
		return nullptr;
	}
};

class CFont : public CGameObject
{
protected:
	explicit CFont();
	explicit CFont(const CFont& rhs);
	virtual ~CFont();

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	static CFont* Create();
	virtual CGameObject* Clone_GameObject(void* pArg) override;

	ID3D12Resource* fontTextureBufferUploadHeap;

	Font arialFont; // this will store our arial font information

	int maxNumTextCharacters = 1024; // the maximum number of characters you can render during a frame. This is just used to make sure
									// there is enough memory allocated for the text vertex buffer each frame

	ID3D12Resource* m_pGameObjResource = nullptr;
	TextVertex* textVertexs = nullptr;

	D3D12_VERTEX_BUFFER_VIEW		m_d3dVertexBufferView;

	Font LoadFont(LPCWSTR filename, int windowWidth, int windowHeight); // load a font

	void SetText(wstring str, XMFLOAT2 pos, XMFLOAT2 scale = XMFLOAT2(1.0f, 1.0f), XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	void SetTextString(wstring str);
	void SetTextPos(XMFLOAT2 pos);
	void SetTextScale(XMFLOAT2 scale);
	void SetTextColor(XMFLOAT4 color);
	void RenderText(Font font, std::wstring text, XMFLOAT2 pos, XMFLOAT2 scale = XMFLOAT2(1.0f, 1.0f), XMFLOAT2 padding = XMFLOAT2(0.5f, 0.0f), XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	HRESULT CreateLoadFont();

	wstring message;
	XMFLOAT2 xmPos = { 0.f, 0.f };
	XMFLOAT2 xmScale = { 1.f, 1.f };
	XMFLOAT4 xmColor = { 1.0f, 1.0f, 1.0f, 1.0f };

	void SetHide(bool hide) { m_bHide = hide; }
	bool GetHide() { return m_bHide; }
private:
	CShader* m_pShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	UINT srvHandleSize;

	bool m_bHide = false;
	//ID3D12DescriptorHeap* m_pd3dCbvSrvDescriptorHeap = nullptr;
protected:
	HRESULT Add_Component();

	void CreateShaderVariables();

public:
	HRESULT Release();

};