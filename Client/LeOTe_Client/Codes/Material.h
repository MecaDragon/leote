#ifndef __MATERIAL__
#define __MATERIAL__

#include "stdafx.h"
#include "Component.h"

class CMaterial final : public CComponent
{
private:
	explicit CMaterial();
	explicit CMaterial(const CMaterial& rhs);
	virtual ~CMaterial(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CMaterial* Create();

	void SetAmbient(float x, float y, float z, float w);
	void SetAmbient(XMFLOAT4 xmf4Ambient);
	void SetAmbientRGBA(int r, int g, int b, int a);

	void SetDiffuse(float x, float y, float z, float w);
	void SetDiffuse(XMFLOAT4 xmf4Diffuse);
	void SetDiffuseRGBA(int r, int g, int b, int a);

	void SetEmissive(float x, float y, float z, float w);
	void SetEmissive(XMFLOAT4 xmf4Emissive);
	void SetEmissiveRGBA(int r, int g, int b, int a);

	void SetSpecular(float x, float y, float z, float w);
	void SetSpecular(XMFLOAT4 xmf4Specular);
	void SetSpecularRGBA(int r, int g, int b, int a);

	XMFLOAT4 GetAmbient();
	XMFLOAT4 GetDiffuse();
	XMFLOAT4 GetEmissive();
	XMFLOAT4 GetSpecular();
	MATERIAL GetMaterialMat();

	HRESULT Release();

private:
	MATERIAL m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };

};

#endif