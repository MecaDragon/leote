#include "Scene_OneMindMap.h"
#include "Scene_Boardmap.h"
#include "Scene_WaitingRoom.h"
#include "Management.h"
#include "TeddyBear.h"
#include "Physic.h"
#include "Player.h"
#include "BoundingBox.h"
#include "Font.h"
#include "BillBoardObject.h"
#include "Camera_Fixed.h"
#include "Camera_Third.h"
#include "UI.h"
#include "StaticObject.h"

extern bool G_SC;

extern CPlayer* Player;
extern CPlayer* Players[3];

extern CTeddyBear* pTeddyBear[2];

CScene_OneMindMap::CScene_OneMindMap()
{
}

CScene_OneMindMap::~CScene_OneMindMap()
{
}

CGameObject* CScene_OneMindMap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz)
{
    CManagement* pManagement = CManagement::GetInstance();

    CGameObject* obj = nullptr;
    ObjWorld temp;
    temp.position.x = x;
    temp.position.y = y;
    temp.position.z = z;

    temp.rotation.x = rx;
    temp.rotation.y = ry;
    temp.rotation.z = rz;
    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
        return nullptr;
    return obj;

}

CGameObject* CScene_OneMindMap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
    CManagement* pManagement = CManagement::GetInstance();

    CGameObject* obj = nullptr;
    ObjWorld temp;
    temp.position = transform;
    temp.rotation = rotation;
    temp.scale = scale;
    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
        return nullptr;
    return obj;

}

HRESULT CScene_OneMindMap::Initialize_Scene(CScene_Boardmap* pSceneBoardmap)
{
    loadcomplete = false;
    m_pScene_Boardmap = pSceneBoardmap;

    moveDelta = 0.f;

    m_SceneId = SCENEID::SCENE_ONEMINDMAP;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);

    CManagement* pManagement = CManagement::GetInstance();

    CreateObject(L"Layer_BackGround", L"GameObject_SkyBox", 29.7602, 15.1374, -32.9217, -90.f, 0.f, 0.f);

    CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");


    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_Player", L"GameObject_Player", (CGameObject**)&m_pPlayer)))
        MSG_BOX("Player Create Fail");

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_SantaTeddyBear", (CGameObject**)&m_pSantaTeddyBear)))
        MSG_BOX("SantaTeddyBear Create Fail");

    m_pPlayer->SetObject(m_pSantaTeddyBear);
    m_pPlayer->Set_IsSanta(true);

    CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
    dynamic_cast<CCamera_Third*>(cam)->SetPlayer(m_pPlayer);


    XMFLOAT3 size = XMFLOAT3(0.367f, 0.367f, 0.367f);
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -140.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-4.9, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-4.9, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(4.9, 0.0, -142.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(6.2, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(4.9, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, -89.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, 2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, 4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -29.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(11.1, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(8.6, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -140.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-2.5, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, -142.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-2.5, 0.0, 4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-3.7, 0.0, 2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(6.2, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(4.9, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -85.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-2.5, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -142.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(11.1, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, -31.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, 4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-2.5, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(11.1, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-3.7, 0.0, -2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(6.2, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-7.4, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -34.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, -2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, 2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, -85.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, -87.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -31.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, 2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -89.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, -34.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-6.2, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(9.8, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(0.0, 0.0, -29.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-3.7, 0.0, -153.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-7.4, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-3.7, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(7.4, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(7.4, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(4.9, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -87.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(12.3, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(2.5, 0.0, 0.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-1.2, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(1.2, 0.0, -87.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(9.8, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -31.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(-4.9, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_snowHex", XMFLOAT3(3.7, 0.0, -2.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(2.5, 0.0, -4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -121.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-1.2, 0.0, -36.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -91.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(2.5, 0.0, -8.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-8.6, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(3.7, 0.0, -95.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(7.4, 0.0, -142.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -112.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-1.2, 0.0, -23.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -6.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-2.5, 0.0, -25.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(2.5, 0.0, -93.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-2.5, 0.0, -142.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(13.5, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-2.5, 0.0, -4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -83.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -10.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(1.2, 0.0, -36.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(0.0, 0.0, -21.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-1.2, 0.0, -27.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(0.0, 0.0, -4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-1.2, 0.0, -6.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(8.6, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(0.0, 0.0, -8.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(-6.2, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex0", XMFLOAT3(3.7, 0.0, -10.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(3.7, 0.0, -27.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(3.7, 0.0, -23.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(-6.2, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(2.5, 0.0, -21.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(-3.7, 0.0, -144.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(1.2, 0.0, -100.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(4.9, 0.0, -25.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(8.6, 0.0, -148.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(3.7, 0.0, -19.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(8.6, 0.0, -14.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(4.9, 0.0, -17.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(0.0, 0.0, -38.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(4.9, 0.0, -117.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(7.4, 0.0, -17.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(3.7, 0.0, -127.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(12.3, 0.0, -146.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(4.9, 0.0, -12.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(6.2, 0.0, -14.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(-2.5, 0.0, -38.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(2.5, 0.0, -97.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(0.0, 0.0, -80.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(6.2, 0.0, -10.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(6.2, 0.0, -19.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex1", XMFLOAT3(0.0, 0.0, -151.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(1.2, 0.0, -40.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(2.5, 0.0, -48.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(6.2, 0.0, -131.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(4.9, 0.0, -134.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(2.5, 0.0, -57.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(1.2, 0.0, -55.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(-2.5, 0.0, -102.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(-1.2, 0.0, -100.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(-1.2, 0.0, -63.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(3.7, 0.0, -55.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(3.7, 0.0, -72.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(2.5, 0.0, -44.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(4.9, 0.0, -138.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(4.9, 0.0, -129.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(4.9, 0.0, -48.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(0.0, 0.0, -44.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(1.2, 0.0, -72.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(1.2, 0.0, -63.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(-3.7, 0.0, -104.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(3.7, 0.0, -136.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(-1.2, 0.0, -40.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(0.0, 0.0, -65.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(-1.2, 0.0, -108.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_iceHex2", XMFLOAT3(2.5, 0.0, -74.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_waterHex", XMFLOAT3(2.5, -0.6, -25.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_waterHex", XMFLOAT3(1.2, -0.6, -23.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_waterHex", XMFLOAT3(0.0, -0.6, -25.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));
    CreateObject(L"Layer_Haxagon", L"GameObject_waterHex", XMFLOAT3(1.2, -0.6, -27.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.367, 0.3670001, 0.3670001));

    m_listHaxagon = CObjectManager::GetInstance()->Find_Layer(SCENE_ONEMINDMAP, L"Layer_Haxagon")->Get_GameObject();

    CreateObject(L"Layer_BackGround", L"GameObject_iceland0", XMFLOAT3(10.8, 0.4, -41.2), XMFLOAT3(270.0, 43.0, 0.0), XMFLOAT3(0.8966398, 0.5138345, 0.367));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland0", XMFLOAT3(11.0, 0.4, -111.1), XMFLOAT3(270.0, 199.8, 0.0), XMFLOAT3(0.8966403, 0.5138347, 0.3670001));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland0", XMFLOAT3(-0.9, 0.4, -15.6), XMFLOAT3(270.0, 43.0, 0.0), XMFLOAT3(0.5018132, 0.3593248, 0.367));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland0", XMFLOAT3(-7.0, 0.4, -95.4), XMFLOAT3(270.0, 43.0, 0.0), XMFLOAT3(0.5018132, 0.3593248, 0.367));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(-3.0, 0.4, -14.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2686617, 0.1737804, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(-9.1, 0.4, -93.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2686617, 0.1737804, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(13.6, 0.2, -67.5), XMFLOAT3(270.0, 26.2, 0.0), XMFLOAT3(1.614486, 1.252729, 0.4725808));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(-7.2, 0.4, -51.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.8664833, 0.5293227, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(-3.9, 0.4, -129.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.8664833, 0.5293227, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(14.9, 0.4, -113.4), XMFLOAT3(270.0, 156.8, 0.0), XMFLOAT3(0.3243555, 0.2902307, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(8.1, 0.4, -37.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.3243553, 0.2902307, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_iceland1", XMFLOAT3(13.2, 0.3, -6.0), XMFLOAT3(270.0, 26.2, 0.0), XMFLOAT3(1.614486, 1.252729, 0.4725808));
    CreateObject(L"Layer_BackGround", L"GameObject_igloo", XMFLOAT3(13.8, 1.9, -66.3), XMFLOAT3(270.0, 277.6, 0.0), XMFLOAT3(0.2621428, 0.2621428, 0.2621429));
    CreateObject(L"Layer_BackGround", L"GameObject_igloo", XMFLOAT3(13.3, 1.9, -4.8), XMFLOAT3(270.0, 277.6, 0.0), XMFLOAT3(0.2621428, 0.2621428, 0.2621429));
    CreateObject(L"Layer_BackGround", L"GameObject_igloo", XMFLOAT3(10.2, 2.0, -151.7), XMFLOAT3(270.0, 322.5, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144));
    CreateObject(L"Layer_BackGround", L"GameObject_snowman", XMFLOAT3(4.9, 0.8, -148.9), XMFLOAT3(270.0, 345.6, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144));
    snowman2 = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_snowman", XMFLOAT3(1.2, 0.8, -87.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144)));
    CreateObject(L"Layer_BackGround", L"GameObject_snowman", XMFLOAT3(2.5, 0.8, -149.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144));
    snowman1 = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_snowman", XMFLOAT3(1.2, 0.8, -31.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144)));
    CreateObject(L"Layer_BackGround", L"GameObject_snowman", XMFLOAT3(0.0, 0.8, -148.9), XMFLOAT3(270.0, 22.5, 0.0), XMFLOAT3(0.3932143, 0.3932143, 0.3932143));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball0", XMFLOAT3(-7.6, 0.6, -50.7), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2621429, 0.2621429, 0.2621429));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball0", XMFLOAT3(-7.6, 0.6, -51.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball0", XMFLOAT3(-4.2, 0.6, -129.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2621429, 0.2621429, 0.2621429));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball0", XMFLOAT3(-4.2, 0.6, -130.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.3932143, 0.3932144, 0.3932144));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball1", XMFLOAT3(12.9, 0.7, -69.6), XMFLOAT3(270.0, 65.0, 0.0), XMFLOAT3(0.5242857, 0.5242857, 0.5242857));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball1", XMFLOAT3(12.5, 0.7, -8.1), XMFLOAT3(270.0, 65.0, 0.0), XMFLOAT3(0.5242857, 0.5242857, 0.5242857));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball1", XMFLOAT3(12.9, 0.7, -7.0), XMFLOAT3(270.0, 65.0, 0.0), XMFLOAT3(1.048571, 1.048571, 0.6959684));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball1", XMFLOAT3(13.4, 0.7, -68.5), XMFLOAT3(270.0, 65.0, 0.0), XMFLOAT3(1.048571, 1.048571, 0.6959684));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball1", XMFLOAT3(-7.3, 0.7, -150.7), XMFLOAT3(270.0, 24.6, 0.0), XMFLOAT3(2.621428, 2.621429, 2.097143));
    CreateObject(L"Layer_BackGround", L"GameObject_snowball1", XMFLOAT3(-4.3, 0.7, -152.0), XMFLOAT3(270.0, 65.0, 0.0), XMFLOAT3(2.097143, 2.097143, 2.097143));
    CreateObject(L"Layer_BackGround", L"GameObject_truncatedTree", XMFLOAT3(11.6, 0.9, -6.8), XMFLOAT3(294.2, 75.0, 180.0), XMFLOAT3(0.1572858, 0.1572858, 0.1572858));
    CreateObject(L"Layer_BackGround", L"GameObject_truncatedTree", XMFLOAT3(12.0, 0.9, -68.3), XMFLOAT3(294.2, 75.0, 180.0), XMFLOAT3(0.1572858, 0.1572858, 0.1572858));
    CreateObject(L"Layer_BackGround", L"GameObject_truncatedTree", XMFLOAT3(-4.8, 0.5, -149.2), XMFLOAT3(270.0, 28.0, 0.0), XMFLOAT3(0.3145714, 0.3145715, 0.3145715));
    CreateObject(L"Layer_BackGround", L"GameObject_truncatedTree1", XMFLOAT3(-6.9, 0.9, -51.2), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2097143, 0.2097143, 0.2097143));
    CreateObject(L"Layer_BackGround", L"GameObject_truncatedTree1", XMFLOAT3(-1.8, 0.9, -152.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2621429, 0.2621429, 0.2621429));
    CreateObject(L"Layer_BackGround", L"GameObject_truncatedTree1", XMFLOAT3(-3.6, 0.9, -129.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.2097143, 0.2097143, 0.2097143));
    CreateObject(L"Layer_BackGround", L"GameObject_snowTree", XMFLOAT3(3.9, 0.7, -153.6), XMFLOAT3(270.0, 318.5, 0.0), XMFLOAT3(0.3932143, 0.3932143, 0.5242857));
    CreateObject(L"Layer_BackGround", L"GameObject_snowTree", XMFLOAT3(6.8, 0.7, -153.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.5242857, 0.5242859, 0.5242859));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pTimeText)))
        return E_FAIL;
    pTimeText->SetText(L"30", XMFLOAT2(0.81f, 0.02f), XMFLOAT2(0.9f, 0.9f));
    pTimeText->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
    CreateObject(L"Layer_BackGround", L"GameObject_StopWatch2", XMFLOAT3(0.55f, 0.91f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.06f, 0.1f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.6f, 0.91f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.12f, 0.08f, 0.1f));

    pBatteryUI[0] = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.795f, 0.905f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.03f, 0.042f, 0.1f)));
    pBatteryUI[1] = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.862f, 0.905f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.03f, 0.042f, 0.1f)));
    pBatteryUI[2] = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.927f, 0.905f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.03f, 0.042f, 0.1f)));
    CreateObject(L"Layer_BackGround", L"GameObject_Battery", XMFLOAT3(0.87f, 0.91f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.15f, 0.18f, 0.1f));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&m_pEndText)))
        return E_FAIL;
    m_pEndText->SetText(L"", XMFLOAT2(0.45f, 0.45f), XMFLOAT2(2.f, 2.f));
    m_pEndText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&m_pStatTimeText)))
        return E_FAIL;
    m_pStatTimeText->SetText(L"", XMFLOAT2(0.5f, 0.38f), XMFLOAT2(2.f, 2.f));
    m_pStatTimeText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));
    /*

    CreateObject(L"Layer_BackGround", L"GameObject_Light", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.04f, 0.07f, 0.1f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[0])))
       return E_FAIL;
    pButtonText[0]->SetText(L"Enter", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
    pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));



    CreateObject(L"Layer_BackGround", L"GameObject_Jump", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[0])))
       return E_FAIL;
    pButtonText[0]->SetText(L"Space", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
    pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

    CreateObject(L"Layer_BackGround", L"GameObject_Run", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[1])))
       return E_FAIL;
    pButtonText[1]->SetText(L"Arrow", XMFLOAT2(0.855f, 0.935f), XMFLOAT2(0.5f, 0.5f));
    pButtonText[1]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));
    */

    /*
    CreateObject(L"Layer_BackGround", L"GameObject_Telescope", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

    if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[0])))
       return E_FAIL;
    pButtonText[0]->SetText(L"Mouse", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
    pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
    CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));
    */

    pBoundingBox[0] = dynamic_cast<CBoundingBox*>(CreateObject(L"Layer_BackGround", L"GameObject_BoundingBox", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.8f, 1.2f, 0.7f)));
    pBoundingBox[1] = dynamic_cast<CBoundingBox*>(CreateObject(L"Layer_BackGround", L"GameObject_BoundingBox", XMFLOAT3(26.3, 36.6, -39.4), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.5f, 0.3f, 0.5f)));

    if (SERVERCONNECT == true)
    {
        for (int i = 0; i < 3; ++i)
        {
            Players[i]->m_tPlayerInfo.xmPosition = m_xmPositionList[i];
        }
    }
    else
    {
        Player->m_tPlayerInfo.xmPosition = m_xmPositionList[0];
    }

    m_pPlayer->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.65f, 0.f);

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

    CServerManager::GetInstance()->Set_Lighton(false);
    CManagement::GetInstance()->GetLight()->RightOff(m_pPlayer->m_tPlayerInfo.xmPosition);

    CServerManager::GetInstance()->Set_PositionOn(false);
    CServerManager::GetInstance()->Set_LookOn(false);

    if (!SERVERCONNECT)
        gameStarted = true;

    CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
    CSoundManager::GetInstance()->Play_Sound(L"Onemindmap", 1.f, 0, true); //배경음악 재생
    ShowCursor(false);

    return NOERROR;
}

short CScene_OneMindMap::KeyEvent(double TimeDelta)
{
    if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
        return 0;

    if (!gameStarted)
    {
        return 0;
    }

    CKeyManager* pKeyMgr = CKeyManager::GetInstance();

    static bool isClip = true;

    if (GetAsyncKeyState(VK_F2) & 0x8000)
    {
        if (isClip)
            isClip = false;
        else
            isClip = true;
    }

    if (Player->Get_OneMindRoll() == ROLL_DIR || !SERVERCONNECT) {
        if (isClip)
        {
            float cxDelta = 0.0f, cyDelta = 0.0f;
            POINT ptCursorPos, centerPt;
            centerPt.x = WINDOWX / 2;
            centerPt.y = WINDOWY / 2;

            GetCursorPos(&ptCursorPos);
            cxDelta = (float)(ptCursorPos.x - centerPt.x) / 3.0f;
            cyDelta = (float)(ptCursorPos.y - centerPt.y) / 3.0f;
            SetCursorPos(centerPt.x, centerPt.y);
            if (cxDelta || cyDelta)
            {
                m_pPlayer->Rotate(cyDelta, cxDelta, 0.0f);
                /*
                if (pKeyMgr->KeyPressing(KEY_RBUTTON))
                   Rotate(cyDelta, cxDelta, 0.0f);
                else
                   Rotate(cyDelta, 0.0f, -cxDelta);
                */
            }
        }
    }

    XMFLOAT3 axisX = XMFLOAT3(1.f, 0.f, 0.f);
    XMFLOAT3 axisZ = XMFLOAT3(0.f, 0.f, 1.f);

    XMFLOAT3 axisXZ = XMFLOAT3(1.f, 0.f, 1.f);
    XMFLOAT3 axisXmZ = XMFLOAT3(-1.f, 0.f, 1.f);
    XMFLOAT3 axisXZm = XMFLOAT3(1.f, 0.f, -1.f);
    XMFLOAT3 axisXmZm = XMFLOAT3(-1.f, 0.f, -1.f);

    XMFLOAT3 axisXm = XMFLOAT3(-1.f, 0.f, 0.f);
    XMFLOAT3 axisZm = XMFLOAT3(0.f, 0.f, -1.f);

    XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
    m_pPlayer->SetPlayerForceX(0.f);
    m_pPlayer->SetPlayerForceZ(0.f);

    float forceAmount = 280.f;

    if (Player->Get_OneMindRoll() == ROLL_MOVE || !SERVERCONNECT)
    {
        isMoving = true;
        //m_pPlayer->m_AnimState = ANIM_RUN;

        if (m_bJump == true)
            forceAmount = 90.f;

        if (pKeyMgr->KeyPressing(KEY_W))
        {

            if (!SERVERCONNECT)
            {
                if (pKeyMgr->KeyPressing(KEY_A))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_D))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, forceAmount);
                else
                    m_pPlayer->GivePlayerForce(0.f, 0.f, forceAmount);
            }
            else
            {
                if (pKeyMgr->KeyPressing(KEY_A))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_D))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, forceAmount);
                else
                    m_pPlayer->GivePlayerForce(0.f, 0.f, forceAmount);
            }

        }
        else if (pKeyMgr->KeyPressing(KEY_S))
        {

            if (!SERVERCONNECT)
            {
                if (pKeyMgr->KeyPressing(KEY_A))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, -forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_D))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, -forceAmount);
                else
                    m_pPlayer->GivePlayerForce(0.f, 0.f, -forceAmount);
            }
            {
                if (pKeyMgr->KeyPressing(KEY_A))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, -forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_D))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, -forceAmount);
                else
                    m_pPlayer->GivePlayerForce(0.f, 0.f, -forceAmount);
            }

        }
        else if (pKeyMgr->KeyPressing(KEY_A))
        {
            if (!SERVERCONNECT)
            {
                if (pKeyMgr->KeyPressing(KEY_S))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, -forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_W))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, forceAmount);
                else
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, 0.f);
            }
            else
            {
                if (pKeyMgr->KeyPressing(KEY_S))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, -forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_W))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, forceAmount);
                else
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, 0.f);
            }

        }
        else if (pKeyMgr->KeyPressing(KEY_D))
        {
            if (!SERVERCONNECT)
            {
                if (pKeyMgr->KeyPressing(KEY_W))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_S))
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, -forceAmount);
                else
                    m_pPlayer->GivePlayerForce(forceAmount, 0.f, 0.f);
            }
            else
            {
                if (pKeyMgr->KeyPressing(KEY_W))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, forceAmount);
                else if (pKeyMgr->KeyPressing(KEY_S))
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, -forceAmount);
                else
                    m_pPlayer->GivePlayerForce(-forceAmount, 0.f, 0.f);
            }

        }
        else if (m_pPlayer->GetState() == OBJECT_STATE::OBJSTATE_GROUND)
        {
            isMoving = false;
            //m_pPlayer->m_AnimState = ANIM_IDLE;
        }

        if (pKeyMgr->KeyPressing(KEY_SPACE)) //jump
        {
            if (m_pPlayer->GetState() == OBJECT_STATE::OBJSTATE_GROUND)
            {
                m_pPlayer->m_AnimState = ANIM_JUMP;
                m_pPlayer->SetAnimation("jump");
                m_pPlayer->SetState(OBJECT_STATE::OBJSTATE_FLY);
                //m_pPlayer->SetPlayerForceY(forceAmount * 5);
                m_bJump = true;
                isMoving = true;
            }
        }
    }
    if (Player->Get_OneMindRoll() == ROLL_LIGHT || !SERVERCONNECT)
    {
        m_fLightCoolTime += TimeDelta;

        if (CServerManager::GetInstance()->Get_BatteryHp() > 0) {
            batteryoff = false;
            if (pKeyMgr->KeyPressing(KEY_ENTER)) { //불키기, 여러번 입력 방지를 위해 쿨타임

                if (m_fLightCoolTime > 0.2f) {
                    m_fLightCoolTime = 0.f;

                    if (lighton)
                        lighton = false;
                    else
                        lighton = true;

                    packet_onemindlight p;
                    p.size = sizeof(packet_onemindlight);
                    p.type = C2S_ONEMINDLIGHT;
                    p.light = lighton;
                    CServerManager::GetInstance()->SendData(C2S_ONEMINDLIGHT, &p);
                }
            }
        }
    }


    //m_pPlayer->xmf3Shift = xmf3Shift;


    if (pKeyMgr->KeyPressing(KEY_F1))
    {
        if (SERVERCONNECT)
        {
            if (!Player->GetDeath())
            {
                Player->SetDeath(true);

                packet_scenechange p;
                p.id = Player->Get_ClientIndex();
                p.size = sizeof(packet_scenechange);
                p.type = C2S_SCENECHANGE;
                p.x = Player->Get_CurrentPlatformPos().x;
                p.y = Player->Get_CurrentPlatformPos().y;
                p.z = Player->Get_CurrentPlatformPos().z;
                p.lookx = Player->Get_BeforeLookVector().x;
                p.looky = Player->Get_BeforeLookVector().y;
                p.lookz = Player->Get_BeforeLookVector().z;
                p.map = S_BOARDMAP;
                CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);
                CCollisionManager::GetInstance()->clearColliderArray();

            }
        }
        else
        {
            Scene_Change(SCENE_BOARDMAP);
            CCollisionManager::GetInstance()->clearColliderArray();
        }
    }


    return NOERROR;
}

short CScene_OneMindMap::Update_Scene(double TimeDelta)
{
    CManagement* pManagement = CManagement::GetInstance();

    if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
    {
        if (m_pLoading == nullptr)
        {
            m_pLoading = CLoading::Create(SCENE_WAITINGROOM);
        }
        else
        {
            if (m_pLoading->Get_Finish())
            {
                pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

                if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
                    return -1;

                if (FAILED(pManagement->Clear_Scene(SCENE_ONEMINDMAP)))
                    return -1;

                if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
                    return -1;

                if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
                    return -1;
            }
        }
        return 0;
    }

    if (!loadcomplete) {

        m_iStartTime = CServerManager::GetInstance()->Get_StartTime();

        if (m_iStartTime > 0)
            m_pStatTimeText->SetTextString(to_wstring(m_iStartTime));
        else {
            packet_loadingsuccess p2;
            p2.size = sizeof(packet_loadingsuccess);
            p2.type = C2S_MINIGAMELOADING;
            p2.scene = S_ONEMINDMAP;
            CServerManager::GetInstance()->SendData(C2S_MINIGAMELOADING, &p2);
            CServerManager::GetInstance()->Set_StartTime(3);

            loadcomplete = true;

            m_pStatTimeText->SetTextString(L"");
        }
    }

    if (m_bEndAsk)
    {
        int m_iEndTime = CServerManager::GetInstance()->Get_StartTime();

        if (m_iEndTime == 0) {

            packet_scenechange p;
            p.id = Player->Get_ClientIndex();
            p.size = sizeof(packet_scenechange);
            p.type = C2S_SCENECHANGE;
            p.x = Player->Get_CurrentPlatformPos().x;
            p.y = Player->Get_CurrentPlatformPos().y;
            p.z = Player->Get_CurrentPlatformPos().z;
            p.lookx = Player->Get_BeforeLookVector().x;
            p.looky = Player->Get_BeforeLookVector().y;
            p.lookz = Player->Get_BeforeLookVector().z;
            p.map = S_BOARDMAP;
            CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

            CServerManager::GetInstance()->Set_StartTime(3);
        }
    }

    if (gameStarted)
    {
        wstring time;
        short count = 150 - CServerManager::GetInstance()->Get_PlayTime();
        time = to_wstring(count);

        pTimeText->SetTextString(time);

        if (CServerManager::GetInstance()->Get_BatteryHp() > 60)
        {
            //3칸
            pBatteryUI[0]->SetHide(false);
            pBatteryUI[1]->SetHide(false);
            pBatteryUI[2]->SetHide(false);
        }
        else if (CServerManager::GetInstance()->Get_BatteryHp() <= 60 && CServerManager::GetInstance()->Get_BatteryHp() > 30)
        {
            //2칸
            pBatteryUI[0]->SetHide(false);
            pBatteryUI[1]->SetHide(false);
            pBatteryUI[2]->SetHide(true);
        }
        else if (CServerManager::GetInstance()->Get_BatteryHp() <= 30 && CServerManager::GetInstance()->Get_BatteryHp() > 0)
        {
            //1칸
            pBatteryUI[0]->SetHide(false);
            pBatteryUI[1]->SetHide(true);
            pBatteryUI[2]->SetHide(true);
        }
        else if (CServerManager::GetInstance()->Get_BatteryHp() <= 0)
        {
            //0칸
            pBatteryUI[0]->SetHide(true);
            pBatteryUI[1]->SetHide(true);
            pBatteryUI[2]->SetHide(true);

            //불끄기, 배터리 없음
            if (!batteryoff) {
                CManagement::GetInstance()->GetLight()->RightOff(m_pPlayer->m_tPlayerInfo.xmPosition);
                CServerManager::GetInstance()->Set_Lighton(false);

                packet_onemindlight p;
                p.size = sizeof(packet_onemindlight);
                p.type = C2S_ONEMINDLIGHT;
                p.light = false;
                CServerManager::GetInstance()->SendData(C2S_ONEMINDLIGHT, &p);

                batteryoff = true;
            }
        }
    }

    if (CServerManager::GetInstance()->Get_MinigameReady())
    {
        CManagement::GetInstance()->GetLight()->DirectionOff();
        gameStarted = true;
        CServerManager::GetInstance()->Set_MinigameReady(false);

        if (Player->Get_OneMindRoll() == ROLL_DIR || !SERVERCONNECT) {
            CreateObject(L"Layer_BackGround", L"GameObject_Telescope", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

            if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[0])))
                return E_FAIL;
            pButtonText[0]->SetText(L"Mouse", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
            pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

            m_pPlayer->m_tPlayerInfo.xmLook = XMFLOAT3(0.f, 0.f, -1.f);
        }
        else if (Player->Get_OneMindRoll() == ROLL_MOVE || !SERVERCONNECT)
        {
            CreateObject(L"Layer_BackGround", L"GameObject_Jump", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

            if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[0])))
                return E_FAIL;
            pButtonText[0]->SetText(L"Space", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
            pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

            CreateObject(L"Layer_BackGround", L"GameObject_Run", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

            if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[1])))
                return E_FAIL;
            pButtonText[1]->SetText(L"Arrow", XMFLOAT2(0.855f, 0.935f), XMFLOAT2(0.5f, 0.5f));
            pButtonText[1]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));
        }
        else if (Player->Get_OneMindRoll() == ROLL_LIGHT || !SERVERCONNECT)
        {
            CreateObject(L"Layer_BackGround", L"GameObject_Light", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.04f, 0.07f, 0.1f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

            if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ONEMINDMAP, L"Layer_BackGround", L"GameObject_OneFont", (CGameObject**)&pButtonText[0])))
                return E_FAIL;
            pButtonText[0]->SetText(L"Enter", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
            pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
            CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));
        }

        m_pPlayer->Set_OneMindRoll(Player->Get_OneMindRoll());
    }

    if (CServerManager::GetInstance()->Get_MiniWinner() == -2)
    {
        gameEnd = true;
        Player->xmf3Shift = { 0.f,0.f,0.f };
        CSoundManager::GetInstance()->Play_Sound(L"Minigame_Lose");
        CServerManager::GetInstance()->Set_MiniWinner(-1);

        m_pEndText->SetTextString(L"Lose");

        for (int i = 0; i < 3; ++i)
        {
            Players[i]->SetWinner(false);
        }

        if (!m_bEndAsk) //종료 3초 요청
        {
            m_iEndTime = 3;
            m_bEndAsk = true;

            packet_starttimeask ps;
            ps.size = sizeof(packet_starttimeask);
            ps.type = C2S_STARTTIMER;
            ps.id = Player->Get_ClientIndex();

            CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
        }
    }
    else if (CServerManager::GetInstance()->Get_MiniWinner() == -3)
    {
        gameEnd = true;
        Player->xmf3Shift = { 0.f,0.f,0.f };

        CServerManager::GetInstance()->Set_MiniWinner(-1);
        CSoundManager::GetInstance()->Play_Sound(L"Mingame_Win");
        m_pEndText->SetTextString(L"Win");

        for (int i = 0; i < 3; ++i)
        {
            Players[i]->SetWinner(true);
        }

        if (!m_bEndAsk) //종료 3초 요청
        {
            m_iEndTime = 3;
            m_bEndAsk = true;

            packet_starttimeask ps;
            ps.size = sizeof(packet_starttimeask);
            ps.type = C2S_STARTTIMER;
            ps.id = Player->Get_ClientIndex();

            CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
        }
    }

    if (CServerManager::GetInstance()->Get_BoardmapReady() == 3)
    {
        CServerManager::GetInstance()->Set_Boardmapinit();
        Scene_Change(SCENE_BOARDMAP);
        return 0;
    }

    if (pBoundingBox[0] && pTeddyBear[0])
    {
        XMFLOAT3 xmPosition = pTeddyBear[0]->m_pTransformCom->GetPosition();
        pBoundingBox[0]->m_pTransformCom->SetPosition(xmPosition.x, xmPosition.y + 1.3f, xmPosition.z);
    }

    XMFLOAT3 xmShiftResult = m_pPlayer->m_pPhysicCom->UpdatePhysics(m_pPlayer->m_tPlayerInfo.xmPosition, m_pPlayer->m_tPlayerInfo.xmRight,
        m_pPlayer->m_tPlayerInfo.xmUp, m_pPlayer->m_tPlayerInfo.xmLook, m_pPlayer->GetPlayerForce(), m_pPlayer->GetState(), TimeDelta);

    m_pPlayer->xmf3Shift.x = xmShiftResult.x;
    m_pPlayer->xmf3Shift.z = xmShiftResult.z;

    if (m_pPlayer->GetPlayerForce().y > 0.f)
        m_pPlayer->GivePlayerForce(0.f, -100.f, 0.f);

    if (SERVERCONNECT) {

        if (!gameEnd) {
            if (CServerManager::GetInstance()->Get_Lighton() == false) //false
            {
                //불끄기
                CManagement::GetInstance()->GetLight()->RightOff(m_pPlayer->m_tPlayerInfo.xmPosition);
            }
            else
            {
                //불키기
                CManagement::GetInstance()->GetLight()->RightOn(m_pPlayer->m_tPlayerInfo.xmPosition);
            }


            if (CServerManager::GetInstance()->Get_PositionOn())
            {
                //위치 러프 함수
                m_isLerpingPos = true;
                m_fTimeDeltaPos = 0.f;

                m_xmLerpStartPos = m_pPlayer->m_tPlayerInfo.xmPosition;
                m_xmLerpDestPos = CServerManager::GetInstance()->Get_RobotPosition();

                CServerManager::GetInstance()->Set_PositionOn(false);
            }

            if (CServerManager::GetInstance()->Get_LookOn())
            {
                //룩 러프 함수
                m_isLerpingLook = true;
                m_fTimeDeltaLook = 0.f;

                m_xmLerpStartLook = m_pPlayer->m_tPlayerInfo.xmLook;
                m_xmLerpDestLook = CServerManager::GetInstance()->Get_RobotLook();

                CServerManager::GetInstance()->Set_LookOn(false);
            }

            updateLerpPosition(TimeDelta);
            updateLerpLook(TimeDelta);

            if (gameStarted) {
                moveDelta += TimeDelta;

                //respawn
                if (CServerManager::GetInstance()->Get_RobotRespawn())
                {
                    m_pPlayer->m_tPlayerInfo.xmPosition = CServerManager::GetInstance()->Get_RobotRespawnPosition();
                    m_pPlayer->xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
                    m_pPlayer->SetPlayerForce(0.f, 0.f, 0.f);
                    m_pPlayer->m_pPhysicCom->SetAcc(0.f, 0.f, 0.f);
                    m_pPlayer->m_pPhysicCom->SetVel(0.f, 0.f, 0.f);
                    //리스폰 도착 이펙트

                    CServerManager::GetInstance()->Set_RobotRespawn(false);
                }

                //savepoint 도착(눈사람과 충돌)
                if (CServerManager::GetInstance()->Get_SnowmanSavePoint(0))
                {
                    //첫번째 눈사람 사라지게하기
                    if (snowman1)
                        snowman1->SetRender(false);
                    CSoundManager::GetInstance()->Play_Sound(L"ppyong");
                    CServerManager::GetInstance()->Set_SnowmanSavePoint(0, false);
                }
                if (CServerManager::GetInstance()->Get_SnowmanSavePoint(1))
                {
                    //두번째 눈사람 사라지게하기
                    if (snowman2)
                        snowman2->SetRender(false);
                    CSoundManager::GetInstance()->Play_Sound(L"ppyong");
                    CServerManager::GetInstance()->Set_SnowmanSavePoint(1, false);
                }

                if (Player->Get_OneMindRoll() != ROLL_MOVE) {
                    int SantaAnim = CServerManager::GetInstance()->Get_SantaAnim();
                    if (SantaAnim >= 0) //서버에서 움직이는 애한텐 패킷 안보냄, 움직이는 애는 자체적으로 애니메이션 변경
                    {
                        m_pPlayer->SetAnimationState(SantaAnim);

                        CServerManager::GetInstance()->Set_SantaAnim(-1);
                    }
                }
                if (moveDelta > MOVEPACKETDELTA)
                {
                    switch (Player->Get_OneMindRoll())
                    {
                    case ROLL_DIR:
                    {
                        packet_onemindlook p;
                        p.size = sizeof(packet_onemindlook);
                        p.type = C2S_ONEMINDDIR;
                        p.lookx = m_pPlayer->m_tPlayerInfo.xmLook.x;
                        p.looky = m_pPlayer->m_tPlayerInfo.xmLook.y;
                        p.lookz = m_pPlayer->m_tPlayerInfo.xmLook.z;

                        CServerManager::GetInstance()->SendData(C2S_ONEMINDDIR, &p);
                    }
                    break;
                    case ROLL_MOVE:
                    {
                        packet_onemindmove p;
                        p.size = sizeof(packet_onemindmove);
                        p.type = C2S_ONEMINDMOVE;
                        p.mx = m_pPlayer->m_tPlayerInfo.xmPosition.x;
                        p.my = m_pPlayer->m_tPlayerInfo.xmPosition.y;
                        p.mz = m_pPlayer->m_tPlayerInfo.xmPosition.z;

                        CServerManager::GetInstance()->SendData(C2S_ONEMINDMOVE, &p);
                    }
                    break;
                    }

                    moveDelta = 0.f;
                }
            }
        }
    }
    return 0;
}

short CScene_OneMindMap::LateUpdate_Scene(double TimeDelta)
{
    if (!isMovePlayer)
    {
        if (Player->Get_OneMindRoll() == ROLL_MOVE) {
            m_pPlayer->SetIsPlayer(true);
            isMovePlayer = true;
        }
        else if (Player->Get_OneMindRoll() > 0) {
            isMovePlayer = true;
        }
    }

    if (Player->Get_OneMindRoll() != ROLL_MOVE && SERVERCONNECT == true)
        return 0;

    bool bCollision = false;
    float hexRadius = 0.9f;
    float playerRadius = 0.7f;

    for (auto hexagon : m_listHaxagon)
    {
        XMFLOAT3 hexPos = hexagon->get_ObjPosition();
        XMFLOAT3 hexPos1 = hexPos;
        hexPos1.y = 0.f;
        XMFLOAT3 playerPos = m_pPlayer->m_tPlayerInfo.xmPosition;
        XMFLOAT3 playerPos1 = playerPos;
        playerPos1.y = 0.f;
        XMFLOAT3 xmLength;
        XMStoreFloat3(&xmLength, XMLoadFloat3(&hexPos1) - XMLoadFloat3(&playerPos1));
        float dist = Vector3::Length(xmLength);
        float length = fabs(hexPos.y - playerPos.y);

        if (dist < hexRadius + playerRadius && length < 0.7f)
        {
            bCollision = true;
            break;
        }
    }

    if (bCollision == false)
    {
        if (m_pPlayer->GetState() != OBJECT_STATE::OBJSTATE_FLY)
        {
            m_pPlayer->SetAnimation("freefall");
            m_pPlayer->SetState(OBJECT_STATE::OBJSTATE_FALLING);
        }

        if(m_bJump == false)
            m_pPlayer->m_tPlayerInfo.xmPosition.y -= PLAYERSPEED * TimeDelta * 25.f;
        else
            m_pPlayer->m_tPlayerInfo.xmPosition.y -= PLAYERSPEED * TimeDelta;
    }
    else
    {
        m_pPlayer->SetState(OBJECT_STATE::OBJSTATE_GROUND);
        //Player->m_tPlayerInfo.xmPosition.y = 0.65f;
    }

    if (m_bJump == true)
    {
        startJump(TimeDelta, bCollision);
    }
    /*else
        Player->m_tPlayerInfo.xmPosition.y = 0.65f;*/

    return 0;
}

HRESULT CScene_OneMindMap::Render_Scene()
{
    return NOERROR;
}

HRESULT CScene_OneMindMap::Scene_Change(SCENEID Scene)
{
    CManagement* pManagement = CManagement::GetInstance();

    if (nullptr == pManagement)
        return -1;

    if (!SERVERCONNECT) {
        ::Player->m_tPlayerInfo.xmPosition = ::Player->Get_CurrentPlatformPos();
        ::Player->m_tPlayerInfo.xmLook = ::Player->Get_BeforeLookVector();
        ::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
        ::Player->m_AnimState = ANIM_IDLE;
        ::Player->SetAnimation("idle1");
        CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
        dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Player);
    }
    else
    {
        for (int i = 0; i < 3; ++i)
        {
            if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
            {
                CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
                dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);
            }

            //Players[i]->m_AnimState = ANIM_IDLE;
            Players[i]->SetAnimation("run");
            Players[i]->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

        }
    }

    CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
    CSoundManager::GetInstance()->Play_Sound(L"Boardmap", 1.f, 0, true); //배경음악 재생
    ShowCursor(true);

    CManagement::GetInstance()->GetLight()->DirectionOn();

    pManagement->Set_CurrentSceneId(Scene);

    if (FAILED(pManagement->Initialize_Current_Scene(m_pScene_Boardmap)))
        return -1;

    if (FAILED(pManagement->Clear_Scene(SCENE_ONEMINDMAP)))
        return -1;
}

CScene_OneMindMap* CScene_OneMindMap::Create(CScene_Boardmap* pSceneBoardmap)
{
    CScene_OneMindMap* pInstance = new CScene_OneMindMap();

    if (pInstance->Initialize_Scene(pSceneBoardmap))
    {
        MSG_BOX("CScene_OneMindMap Created Failed");
        Safe_Release(pInstance);
    }

    return pInstance;
}

HRESULT CScene_OneMindMap::Release()
{
    delete this;

    return NOERROR;
}

void CScene_OneMindMap::movePlayer(XMFLOAT3 forwardDir, XMFLOAT3 dotProduct, XMFLOAT3& XMShift, double TimeDelta)
{
    XMFLOAT3 xmf3dot;
    XMStoreFloat3(&xmf3dot, XMVector3AngleBetweenVectors(XMLoadFloat3(&forwardDir), XMLoadFloat3(&Player->m_tPlayerInfo.xmLook)));
    float tmpangle = XMConvertToDegrees(xmf3dot.y);

    float fDot = Vector3::DotProduct(Player->m_tPlayerInfo.xmLook, dotProduct);

    if (tmpangle > 7.f)
    {
        if (fDot > 0)
            ::Player->Rotate(0.f, -800.f * TimeDelta, 0.f);
        else
            ::Player->Rotate(0.f, 800.f * TimeDelta, 0.f);
    }
    else
    {
        ::Player->m_tPlayerInfo.xmLook = Vector3::Normalize(forwardDir);

    }
    //::Player->GivePlayerForce(Player->m_tPlayerInfo.xmLook.x * 300.f, 0.f, Player->m_tPlayerInfo.xmLook.z * 300.f);
    XMShift = Vector3::Add(XMShift, Player->m_tPlayerInfo.xmLook, PLAYERSPEED * TimeDelta);

    isMoving = true;

    ::Player->m_AnimState = ANIM_RUN;
}

void CScene_OneMindMap::updateLerpPosition(double TimeDelta)
{
    if (!m_isLerpingPos) {
        return;
    }

    m_fTimeDeltaPos += (float)(TimeDelta) * 2.f;

    XMVECTOR pos = XMVectorLerp(XMLoadFloat3(&m_xmLerpStartPos), XMLoadFloat3(&m_xmLerpDestPos), m_fTimeDeltaPos);
    XMStoreFloat3(&m_pPlayer->m_tPlayerInfo.xmPosition, pos);

    if (m_fTimeDeltaPos >= 1.f)
    {
        m_fTimeDeltaPos = 0.f;
        m_isLerpingPos = false;
    }
}

void CScene_OneMindMap::updateLerpLook(double TimeDelta)
{
    if (!m_isLerpingLook) {
        return;
    }

    m_fTimeDeltaLook += (float)(TimeDelta) * 2.f;

    XMVECTOR look = XMVectorLerp(XMLoadFloat3(&m_xmLerpStartLook), XMLoadFloat3(&m_xmLerpDestLook), m_fTimeDeltaLook);
    XMStoreFloat3(&m_pPlayer->m_tPlayerInfo.xmLook, look);

    if (m_fTimeDeltaLook >= 1.f)
    {
        m_fTimeDeltaLook = 0.f;
        m_isLerpingLook = false;
    }
}

short CScene_OneMindMap::startJump(double TimeDelta, bool bCollision)
{
    m_xmJumpTmp.y = m_fJumpPower - m_fGravity;
    m_pPlayer->xmf3Shift = m_xmJumpTmp;

    m_fJumpPower -= 5.7f * TimeDelta;

    m_fTimer += TimeDelta;

    if (bCollision == true && m_fTimer > 0.03f)
    {
        m_bJump = false;
        m_fTimer = 0.f;
        m_fJumpPower = 9.9f;
        m_pPlayer->xmf3Shift = XMFLOAT3{ 0.f,0.f,0.f };
        Player->m_tPlayerInfo.xmPosition.y = 0.65f;
    }

    return 0;
}