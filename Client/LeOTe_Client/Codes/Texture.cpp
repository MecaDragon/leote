#include "Texture.h"
#include "DDSTextureLoader.h"
#include "TextureLoader.h"
#include "Management.h"
#include "DescriptorHeapManager.h"

CTexture::CTexture(const wchar_t* pszFileName, const wchar_t* pszNameTag, COMMANDID eCommandId, TEXTURETYPE eTexType, MESHTYPE eMeshType, int nTexCnt)
{
	CDeviceManager* pDeviceMgr = CDeviceManager::GetInstance();
	ID3D12Device* pDevice = pDeviceMgr->Get_Device();
	ID3D12GraphicsCommandList* pCommandList = pDeviceMgr->Get_CommandList(eCommandId);

	m_vecTexture.reserve(nTexCnt);
	
	for (_uint i = 0; i < nTexCnt; ++i)
	{
		wchar_t	szFileName[128] = L"";

		wsprintf(szFileName, pszFileName, i);

		TEXTURE_INFO texInfo;
		HRESULT hResult;

		// 추후 여러 장 로드하는 코드 추가
		texInfo.fileName = pszFileName;
		texInfo.name = pszNameTag;

		wstring strTmp = texInfo.fileName.substr(texInfo.fileName.find_last_of(L".") + 1, texInfo.fileName.length() + 1);

		if (strTmp == L"dds")
			hResult = DirectX::CreateDDSTextureFromFile12(pDevice, pCommandList, szFileName, texInfo.Resource, texInfo.UploadHeap);
		else
			hResult = DirectX::CreateImageDataTextureFromFile(pDevice, pCommandList, szFileName, texInfo.Resource, texInfo.UploadHeap);

		if (FAILED(hResult))
			MSG_BOX("Texture Load Error");

		m_vecTexture.push_back(texInfo);

	}

	
	m_eMeshType = eMeshType;
	m_eTextureType = eTexType;
	
	if (m_eTextureType == CTexture::TEX_DIFFUSE)
	{
		SetMaterialType(MATERIAL_ALBEDO_MAP);
	}
	else if (m_eTextureType == CTexture::TEX_SPECULAR)
	{
		SetMaterialType(MATERIAL_SPECULAR_MAP);
	}
	else if (m_eTextureType == CTexture::TEX_NORMAL)
	{
		SetMaterialType(MATERIAL_NORMAL_MAP);
	}
	else if (m_eTextureType == CTexture::TEX_METALLIC)
	{
		SetMaterialType(MATERIAL_METALLIC_MAP);
	}
	else if (m_eTextureType == CTexture::TEX_EMISSIVE)
	{
		SetMaterialType(MATERIAL_EMISSION_MAP);
	}


	if(FAILED(BuildDescriptorHeaps(pDevice)))
		MSG_BOX("Texture Load Error");
}

CTexture::CTexture(const CTexture& rhs)
	/*: m_CpuDescriptorHandle(rhs.m_CpuDescriptorHandle)
	, m_GpuDescriptorHandle(rhs.m_GpuDescriptorHandle)*/
	: m_vecTexture(rhs.m_vecTexture)
	, m_eMeshType(rhs.m_eMeshType)
	, m_eTextureType(rhs.m_eTextureType)
	//, m_pSrvDescriptorHeap(rhs.m_pSrvDescriptorHeap)
	, m_nType(rhs.m_nType)
{
}

CTexture::~CTexture(void)
{
}

HRESULT CTexture::Initialize_Component_Prototype()
{
	return NOERROR;
}

HRESULT CTexture::Initialize_Component(void* pArg)
{
	for (auto& p : m_vecTexture)
	{
		p.Resource->AddRef();
		p.Resource->SetName(L"Texture Comopent Resource");
		p.UploadHeap->AddRef();
		p.UploadHeap->SetName(L"Texture Comopent UploadHeap");
	}


	//m_pSrvDescriptorHeap->AddRef();

	return NOERROR;
}

CComponent* CTexture::Clone_Component(void* pArg)
{
	CTexture* pInstance = new CTexture(*this);

	if (FAILED(pInstance->Initialize_Component(pArg)))
	{
		MSG_BOX("CTexture Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CTexture* CTexture::Create(const wchar_t* pszFileName, const wchar_t* pszNameTag, COMMANDID eCommandId, TEXTURETYPE eTexType, MESHTYPE eMeshType, int nTexCnt)
{
	CTexture* pInstance = new CTexture(pszFileName, pszNameTag, eCommandId, eTexType, eMeshType, nTexCnt);

	if (FAILED(pInstance->Initialize_Component_Prototype()))
	{
		MSG_BOX("CTexture Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CTexture::Release()
{
	for (size_t i = 0; i < m_vecTexture.size(); ++i)
	{
		/*m_vecTexture[i].UploadHeap->Unmap(0, nullptr);
		m_vecTexture[i].Resource->Unmap(0, nullptr);*/
		m_vecTexture[i].UploadHeap->Release();
		m_vecTexture[i].Resource->Release();
	}

	//m_pSrvDescriptorHeap->Release();

	m_vecTexture.clear();

	delete this;

	return NOERROR;
}

void CTexture::UpdateShaderVariables(ID3D12GraphicsCommandList* pCommandList, TEXTURETYPE eRootParameterIdx, _uint nTexIdx)
{
	//pCommandList->SetGraphicsRoot32BitConstants(14, 1, &m_nType, 0);
	ID3D12DescriptorHeap* pSrvDescriptorHeap = CDescriptorHeapManager::GetInstance()->get_srvDescriptorHeap();

	pCommandList->SetDescriptorHeaps(1, &pSrvDescriptorHeap);
	pCommandList->SetGraphicsRootDescriptorTable(eRootParameterIdx, m_vecTexture[nTexIdx].GpuDescriptorHandle);
}

HRESULT CTexture::BuildDescriptorHeaps(ID3D12Device* pDevice)
{
	/*D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
	srvHeapDesc.NumDescriptors = m_vecTexture.size();
	srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	if (FAILED(pDevice->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(&m_pSrvDescriptorHeap))))
		return E_FAIL;

	m_CpuDescriptorHandle = m_pSrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	m_GpuDescriptorHandle = m_pSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart();*/

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	CD3DX12_CPU_DESCRIPTOR_HANDLE CpuDescriptorHandle = CDescriptorHeapManager::GetInstance()->get_CpuDescriptorHandle();
	CD3DX12_GPU_DESCRIPTOR_HANDLE GpuDescriptorHandle = CDescriptorHeapManager::GetInstance()->get_GpuDescriptorHandle();
	for (size_t i = 0; i < m_vecTexture.size(); ++i)
	{
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format = m_vecTexture[i].Resource->GetDesc().Format;

		if (m_eMeshType == MESH_GENERAL)
		{
			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
			srvDesc.Texture2D.MostDetailedMip = 0;
			srvDesc.Texture2D.MipLevels = m_vecTexture[i].Resource->GetDesc().MipLevels;
			srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
		}
		else if (m_eMeshType == MESH_CUBE)
		{
			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
			srvDesc.TextureCube.MostDetailedMip = 0;
			srvDesc.TextureCube.MipLevels = m_vecTexture[i].Resource->GetDesc().MipLevels;
			srvDesc.TextureCube.ResourceMinLODClamp = 0.0f;
		}

		
		pDevice->CreateShaderResourceView(m_vecTexture[i].Resource, &srvDesc, CpuDescriptorHandle);
		CpuDescriptorHandle.Offset(1, g_CbvSrvDescriptorIncrementSize);

		m_vecTexture[i].GpuDescriptorHandle = GpuDescriptorHandle;
		GpuDescriptorHandle.Offset(1, g_CbvSrvDescriptorIncrementSize);
		
	}
	CDescriptorHeapManager::GetInstance()->set_CpuDescriptorHandle(CpuDescriptorHandle);
	CDescriptorHeapManager::GetInstance()->set_GpuDescriptorHandle(GpuDescriptorHandle);
	return NOERROR;
}
