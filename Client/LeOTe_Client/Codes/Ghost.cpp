#include "stdafx.h"
#include "Ghost.h"
#include "Management.h"
#include "Texture.h"
#include "Material.h"
#include "ShadowShader.h"
#include "Renderer.h"

CGhost::CGhost()
	: CGameObject()
{

}

CGhost::CGhost(const CGhost& rhs)
	: CGameObject(rhs)
	, m_nSlot(rhs.m_nSlot)
	, m_nIndices(rhs.m_nIndices)
	, mSkinnedInfo(rhs.mSkinnedInfo)
	, outSkinnedVertices(rhs.outSkinnedVertices)
	, outIndices(rhs.outIndices)
	, outMaterial(rhs.outMaterial)
	, skinnedConstants(rhs.skinnedConstants)
	, m_d3dIndexBufferView(rhs.m_d3dIndexBufferView)
	, m_d3dPrimitiveTopology(rhs.m_d3dPrimitiveTopology)
	, m_d3dVertexBufferView(rhs.m_d3dVertexBufferView)
	, m_nBaseVertex(rhs.m_nBaseVertex)
	, m_nOffset(rhs.m_nOffset)
	, m_nStartIndex(rhs.m_nStartIndex)
	, m_nStride(rhs.m_nStride)
	, m_nVertices(rhs.m_nVertices)
	, m_pd3dIndexBuffer(rhs.m_pd3dIndexBuffer)
	, m_pd3dIndexUploadBuffer(rhs.m_pd3dIndexUploadBuffer)
	, m_pd3dVertexBuffer(rhs.m_pd3dVertexBuffer)
	, m_pd3dVertexUploadBuffer(rhs.m_pd3dVertexUploadBuffer)
	, m_pfbxScene(rhs.m_pfbxScene)
	, m_xmBoundingBox(rhs.m_xmBoundingBox)
{
	mSkinnedModelInst = std::make_unique<SkinnedModelInstance>();
	mSkinnedModelInst->SkinnedInfo = &mSkinnedInfo;
	mSkinnedModelInst->FinalTransforms.resize(mSkinnedInfo.BoneCount());
	mSkinnedModelInst->BeforeTransforms.resize(mSkinnedInfo.BoneCount());
	mSkinnedModelInst->AfterTransforms.resize(mSkinnedInfo.BoneCount());
	mSkinnedModelInst->TimePos = 0.0f;
}

HRESULT CGhost::Initialize_GameObject_Prototype()
{
	
	FbxLoader fbx;
	SkinnedData outSkinnedInfo;

	std::string FileName = "../Data/Resources/Ghost/FBX/";
	fbx.LoadFBX(outSkinnedVertices, outIndices, outSkinnedInfo, "Ghost", outMaterial, FileName);

	fbx.LoadFBX(outSkinnedInfo, "idle1", FileName);
	fbx.LoadFBX(outSkinnedInfo, "attack", FileName);

	mSkinnedInfo = outSkinnedInfo;

	UINT vCount = 0, iCount = 0;
	vCount = (UINT)outSkinnedVertices.size();
	iCount = (UINT)outIndices.size();

	m_nSlot = 0;
	m_nIndices = iCount;

	//==============================================================================

	vCount = (UINT)outSkinnedVertices.size();
	iCount = (UINT)outIndices.size();

	const UINT vbByteSize = vCount * sizeof(CharacterVertex);
	const UINT ibByteSize = iCount * sizeof(std::uint32_t);

	m_pd3dIndexBuffer = ::CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_LOAD),
		outIndices.data(), ibByteSize, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	m_pd3dVertexBuffer = CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_LOAD),
		outSkinnedVertices.data(), vbByteSize, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = sizeof(CharacterVertex);
	m_d3dVertexBufferView.SizeInBytes = vbByteSize;

	m_xmBoundingBox = BoundingBox(XMFLOAT3(0.f, 0.0f, 1.3f), XMFLOAT3(0.4f, -0.35f, 1.3f));


	return NOERROR;
}

HRESULT CGhost::Initialize_GameObject(void * pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CharacterConstants));
	m_pGameObjResource->Map(0, nullptr, (void**)&skinnedConstants);
	//m_pTransformCom->SetPosition(0.f, 0.f, 0.f);
	m_pTransformCom->SetScale(2.f, 2.f, 2.f);
	m_pTransformCom->Rotate(-90.0f,0.0f,0.f);
	mClipName = "idle1";
	mNextClipName = "idle1";

	if (m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->AddRef();

	if (m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->AddRef();

	if (m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->AddRef();

	if (m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->AddRef();

	m_pd3dVertexBuffer->SetName(L"Ghost m_pd3dVertexBuffer");
	m_pd3dVertexUploadBuffer->SetName(L"Ghost m_pd3dVertexUploadBuffer");
	m_pd3dIndexBuffer->SetName(L"Ghost m_pd3dIndexBuffer");
	m_pd3dIndexUploadBuffer->SetName(L"Ghost m_pd3dIndexUploadBuffer");
	m_pGameObjResource->SetName(L"Ghost m_pGameObjResource");
	return NOERROR;
}

void CGhost::ChangeAnimation(string sClipName)
{
	mNextClipName = sClipName;
	mSkinnedModelInst->Blending = true;
	mSkinnedModelInst->BlendTime = 0.f;
}

short CGhost::Update_GameObject(double TimeDelta)
{
	if (mClipName == "attack")
	{
		if (mSkinnedModelInst->IsAnimationEnd("attack"))
		{
			ChangeAnimation("idle1");
		}
	}
	return short();
}

short CGhost::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CGhost::Render_Shadow()
{
	
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	pCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);

	pCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
	pCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	return NOERROR;
}

HRESULT CGhost::Render_GameObject()
{
	skinnedConstants->m_xmf4x4Material = m_pMaterialCom->GetMaterialMat();
	mSkinnedModelInst->UpdateSkinnedAnimation(mClipName, TimeDelta*1.5);
	if (mSkinnedModelInst->Blending)
	{
		mSkinnedModelInst->AnimationBlend(mClipName, mNextClipName, TimeDelta, 0.5f);
	}
	else
	{
		mClipName = mNextClipName;
		mSkinnedModelInst->UpdateSkinnedAnimation(mClipName, TimeDelta);

		//애니메이션 끝났나 여부 검사
		m_aimationEnd = mSkinnedModelInst->IsAnimationEnd(mClipName);
		m_aimationMiddle = mSkinnedModelInst->IsAnimationMiddle(mClipName);
	}


	std::copy(
		std::begin(mSkinnedModelInst->FinalTransforms),
		std::end(mSkinnedModelInst->FinalTransforms),
		&skinnedConstants->BoneTransforms[0]);
	XMStoreFloat4x4(&(skinnedConstants->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(skinnedConstants->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER));
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();

	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);


	//m_pTextureCom->UpdateShaderVariables(pCommandList);
	m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_DIFFUSE);
	//m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_NORMAL, 1);


	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	pd3dCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);

	pd3dCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
	pd3dCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	return NOERROR;
}

HRESULT CGhost::Add_Component()
{
	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXANIM", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Texture_Ghost", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_AnimShadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;
	return NOERROR;
}

HRESULT CGhost::SetUp_ConstantTable()
{

	return NOERROR;
}

CGhost* CGhost::Create()
{
	CGhost*		pInstance = new CGhost();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CGhost Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject * CGhost::Clone_GameObject(void * pArg)
{
	CGhost*		pInstance = new CGhost(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CGhost Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CGhost::Release()
{
	//Safe_Release(m_pMeshCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pMaterialCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	if (m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->Release();

	if (m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->Release();

	m_pd3dVertexUploadBuffer = nullptr;
	m_pd3dIndexUploadBuffer = nullptr;

	if (m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->Release();

	if (m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->Release();


	delete this;

	return NOERROR;
}