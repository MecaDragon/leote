#ifndef __BOUNDINGBOXSHADER__
#define __BOUNDINGBOXSHADER__

#include "Shader.h"

class CBoudingBoxShader final : public CShader
{
private:
	explicit CBoudingBoxShader();
	explicit CBoudingBoxShader(const CBoudingBoxShader& rhs);
	virtual ~CBoudingBoxShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CBoudingBoxShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();
};

#endif
