#include "stdafx.h"
#include "Chicken.h"
#include "Management.h"
#include "Texture.h"
#include "Material.h"
#include "ShadowShader.h"
#include "Renderer.h"
#include "Player.h"
#include "CollisionManager.h"
#include "Collider.h"

extern bool G_SC;

CChicken::CChicken()
	: CGameObject()
{

}

CChicken::CChicken(const CChicken& rhs)
	: CGameObject(rhs)
	, m_nSlot(rhs.m_nSlot)
	, m_nIndices(rhs.m_nIndices)
	, mSkinnedInfo(rhs.mSkinnedInfo)
	, outSkinnedVertices(rhs.outSkinnedVertices)
	, outIndices(rhs.outIndices)
	, outMaterial(rhs.outMaterial)
	, skinnedConstants(rhs.skinnedConstants)
	, m_d3dIndexBufferView(rhs.m_d3dIndexBufferView)
	, m_d3dPrimitiveTopology(rhs.m_d3dPrimitiveTopology)
	, m_d3dVertexBufferView(rhs.m_d3dVertexBufferView)
	, m_nBaseVertex(rhs.m_nBaseVertex)
	, m_nOffset(rhs.m_nOffset)
	, m_nStartIndex(rhs.m_nStartIndex)
	, m_nStride(rhs.m_nStride)
	, m_nVertices(rhs.m_nVertices)
	, m_pd3dIndexBuffer(rhs.m_pd3dIndexBuffer)
	, m_pd3dIndexUploadBuffer(rhs.m_pd3dIndexUploadBuffer)
	, m_pd3dVertexBuffer(rhs.m_pd3dVertexBuffer)
	, m_pd3dVertexUploadBuffer(rhs.m_pd3dVertexUploadBuffer)
	, m_pfbxScene(rhs.m_pfbxScene)
	, m_xmBoundingBox(rhs.m_xmBoundingBox)
{
	mSkinnedModelInst = std::make_unique<SkinnedModelInstance>();
	mSkinnedModelInst->SkinnedInfo = &mSkinnedInfo;
	mSkinnedModelInst->FinalTransforms.resize(mSkinnedInfo.BoneCount());
	mSkinnedModelInst->BeforeTransforms.resize(mSkinnedInfo.BoneCount());
	mSkinnedModelInst->AfterTransforms.resize(mSkinnedInfo.BoneCount());
	mSkinnedModelInst->TimePos = 0.0f;
	
	m_animArray[0] = "Chicken_Idle";
	m_animArray[1] = "Chicken_Eat";
	m_animArray[2] = "Chicken_TurnHead";
	m_animArray[3] = "Chicken_Run";
	m_animArray[4] = "Chicken_Run";

}

HRESULT CChicken::Initialize_GameObject_Prototype()
{
	
	FbxLoader fbx;
	SkinnedData outSkinnedInfo;

	std::string FileName = "../Data/Resources/Chicken/";
	fbx.LoadFBX(outSkinnedVertices, outIndices, outSkinnedInfo, "Chicken", outMaterial, FileName);

	m_animArray[0] = "Chicken_Idle";
	m_animArray[1] = "Chicken_Eat";
	m_animArray[2] = "Chicken_TurnHead";
	m_animArray[3] = "Chicken_Run";
	m_animArray[4] = "Chicken_Run";

	for (int i = 0; i < 5; ++i)
	{
		fbx.LoadFBX(outSkinnedInfo, m_animArray[i], FileName);
	}

	mSkinnedInfo = outSkinnedInfo;

	UINT vCount = 0, iCount = 0;
	vCount = (UINT)outSkinnedVertices.size();
	iCount = (UINT)outIndices.size();

	m_nSlot = 0;
	m_nIndices = iCount;

	//==============================================================================

	vCount = (UINT)outSkinnedVertices.size();
	iCount = (UINT)outIndices.size();

	const UINT vbByteSize = vCount * sizeof(CharacterVertex);
	const UINT ibByteSize = iCount * sizeof(std::uint32_t);

	m_pd3dIndexBuffer = ::CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_LOAD),
		outIndices.data(), ibByteSize, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	m_pd3dVertexBuffer = CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_LOAD),
		outSkinnedVertices.data(), vbByteSize, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = sizeof(CharacterVertex);
	m_d3dVertexBufferView.SizeInBytes = vbByteSize;

	m_xmBoundingBox = BoundingBox(XMFLOAT3(0.f, 0.0f, 1.3f), XMFLOAT3(0.4f, -0.35f, 1.3f));


	return NOERROR;
}

HRESULT CChicken::Initialize_GameObject(void * pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	packet_Initchicken p;

	if (pArg)
	{
		if (!SERVERCONNECT)
			m_pPlayer = (CPlayer*)pArg;
		else
		{
			p = *reinterpret_cast<packet_Initchicken*>(pArg);
			m_iIndex = p.c_id;
			m_fAngle = p.angle;
			m_cState = p.state;
			m_xmObjPosition.x = p.x;
			m_xmObjPosition.y = p.y;
			m_xmObjPosition.z = p.z;
		}
	}

	if (!SERVERCONNECT) {
		m_xmObjPosition = XMFLOAT3((float)(rand() % 26 - 13), 0.5f, (float)(rand() % 14 - 7));

		m_xmLook = XMFLOAT3(0.f, 0.f, 1.f);
		XMFLOAT3 xmY = XMFLOAT3(0.f, 1.f, 0.f);
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&xmY), XMConvertToRadians((float)(rand() % 360)));
		m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
		m_fAngle = (float)(rand() % 360);

		mClipName = m_animArray[rand() % 5];
		mNextClipName = m_animArray[rand() % 5];

		CCollisionManager::GetInstance()->addCollider(m_pSphereColliderCom);

		m_fTimerLimit = rand() % 5 + 3;

		m_fSpeed = 1.3f;
	}
	else
	{
		mClipName = m_animArray[m_cState];
		mNextClipName = m_animArray[m_cState];
		XMFLOAT3 xmY = XMFLOAT3(0.f, 1.f, 0.f);
		m_xmLook = XMFLOAT3(0.f, 0.f, 1.f);
		XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&xmY), XMConvertToRadians(m_fAngle));
		m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
	}


	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CharacterConstants));
	m_pGameObjResource->Map(0, nullptr, (void**)&skinnedConstants);


	if (m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->AddRef();

	if (m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->AddRef();

	if (m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->AddRef();

	if (m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->AddRef();

	m_pd3dVertexBuffer->SetName(L"CChicken m_pd3dVertexBuffer");
	m_pd3dVertexUploadBuffer->SetName(L"CChicken m_pd3dVertexUploadBuffer");
	m_pd3dIndexBuffer->SetName(L"CChicken m_pd3dIndexBuffer");
	m_pd3dIndexUploadBuffer->SetName(L"CChicken m_pd3dIndexUploadBuffer");
	m_pGameObjResource->SetName(L"CChicken m_pGameObjResource");
	return NOERROR;
}

void CChicken::ChangeAnimation(string sClipName)
{
	mNextClipName = sClipName;
	mSkinnedModelInst->Blending = true;
	mSkinnedModelInst->BlendTime = 0.f;
}

void CChicken::SetChickenState(char state)
{
	 m_cState = state; 

	 ChangeAnimation(m_animArray[m_cState]);
}

void CChicken::SetLerpStart(packet_chicken p)
{
	m_isLerping = true;
	ChangeAnimation(m_animArray[3]);
	m_fLerpTimeDelta = 0.f;

	m_xmLerpStartPos = m_xmObjPosition;
	m_xmLerpStartLook = m_xmLook;

	m_xmLerpDestPos = XMFLOAT3(p.x, p.y, p.z);
	m_xmLerpDestLook = XMFLOAT3(p.lookx, p.looky, p.lookz);
}

void CChicken::updateLerp(double TimeDelta)
{
	if (!m_isLerping) {
		return;
	}

	m_fLerpTimeDelta += (float)(TimeDelta) * 2.25f;

	XMVECTOR pos = XMVectorLerp(XMLoadFloat3(&m_xmLerpStartPos), XMLoadFloat3(&m_xmLerpDestPos), m_fLerpTimeDelta);
	XMStoreFloat3(&m_xmObjPosition, pos);

	XMVECTOR look = XMVectorLerp(XMLoadFloat3(&m_xmLerpStartLook), XMLoadFloat3(&m_xmLerpDestLook), m_fLerpTimeDelta * 1.25f);
	XMStoreFloat3(&m_xmLook, look);

	if (m_fLerpTimeDelta >= 1.f)
	{
		m_fLerpTimeDelta = 0.f;
		m_isLerping = false;

		ChangeAnimation(m_animArray[rand() % 3]);
	}
}

short CChicken::Update_GameObject(double TimeDelta)
{

	if (!SERVERCONNECT)
	{
		XMFLOAT3 chickenPos = m_pTransformCom->GetPosition();
		XMFLOAT3 AxisY = XMFLOAT3(0.f, 1.f, 0.f);
		XMFLOAT3 AxisX = XMFLOAT3(1.f, 0.f, 0.f);
		XMFLOAT3 AxisZ = XMFLOAT3(0.f, 0.f, 1.f);

		string nextAnimation;

		checkPlayerCollision(TimeDelta, nextAnimation);

		if (mClipName == "Chicken_Eat")
		{
			m_fTimerLimit = 4;
		}
		else if (mClipName == "Chicken_Idle")
		{
			m_fTimerLimit = 6;
		}
		else if (mClipName == "Chicken_Run")
		{
			m_fTimerLimit = 4;
			m_xmObjPosition = Vector3::Add(m_xmObjPosition, m_xmLook, TimeDelta * 3.6f);
		}
		else if (mClipName == "Chicken_TurnHead")
		{
			m_fTimerLimit = 2.f;
		}
		else if (mClipName == "Chicken_Walk")
		{
			if (m_iRotate == 0) // �ȱ⸸
			{
				m_fTimerLimit = 4;

				m_xmObjPosition = Vector3::Add(m_xmObjPosition, m_xmLook, TimeDelta * 0.9f);
			}
			else if (m_iRotate == 1) // ȸ��
			{
				m_fTimerLimit = 0.7f;
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&AxisY), XMConvertToRadians(TimeDelta * 100.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
		}

		XMFLOAT3 temp = XMFLOAT3(0.f, 0.f, 0.f);
		if ((chickenPos.x >= 13.5f || chickenPos.x <= -13.5f) && m_bCheckx == false)
		{
			m_bCheckx = true;

			m_fLimitAngle = (float)(rand() % 60 + 40);

			m_fDot = slidingVector(AxisZ, m_xmLook, m_xmSlidingVector);

		}

		if ((chickenPos.z >= 7.5f || chickenPos.z <= -8.5f) && m_bCheckz == false)
		{
			m_bCheckz = true;

			m_fLimitAngle = (float)(rand() % 80 + 20);

			m_fDot = slidingVector(AxisX, m_xmLook, m_xmSlidingVector);

		}

		if (m_xmObjPosition.x >= 13.5f)
			temp.x = m_xmObjPosition.x - 13.5f;
		else if (m_xmObjPosition.x <= -13.5f)
			temp.x = m_xmObjPosition.x - (-13.5f);

		if (m_xmObjPosition.z >= 7.5f)
			temp.z = m_xmObjPosition.z - 7.5f;
		else if (m_xmObjPosition.z <= -8.5f)
			temp.z = m_xmObjPosition.z - (-8.5f);

		m_xmObjPosition = Vector3::Subtract(m_xmObjPosition, temp);

		if (m_bCheckx || m_bCheckz)
		{
			if (m_bCheckx == true && m_bCheckz == true && m_isRotating == false)
			{
				m_fLimitAngle = 110.f;
			}

			m_isRotating = true;

			XMFLOAT3 xmf3dot;
			XMStoreFloat3(&xmf3dot, XMVector3AngleBetweenVectors(XMLoadFloat3(&m_xmLook), XMLoadFloat3(&m_xmSlidingVector)));
			m_fAngle = XMConvertToDegrees(xmf3dot.y);

			if (m_fAngle > m_fLimitAngle)
			{
				m_bCheckx = false;
				m_bCheckz = false;
				m_isRotating = false;
			}

			if (m_fDot > 0) // ��
			{
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&AxisY), XMConvertToRadians(TimeDelta * 100.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
			else if (m_fDot < 0) // ��
			{
				XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&AxisY), XMConvertToRadians(-TimeDelta * 100.f));
				m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
			}
		}

		m_fTimer += TimeDelta;
		if (m_fTimer > m_fTimerLimit)
		{
			m_fTimer = 0.f;
			m_iRotate = rand() % 2;

			while (true)
			{
				nextAnimation = m_animArray[rand() % 5];

				if (nextAnimation != mClipName)
					break;
			}
			if (m_isRotating)
			{
				if (mClipName == "Chicken_Walk")
					nextAnimation = "Chicken_Walk";
				else if (mClipName == "Chicken_Run")
					nextAnimation = "Chicken_Run";

				m_iRotate = 0;
			}

			ChangeAnimation(nextAnimation);
		}
	}
	else
	{
		updateLerp(TimeDelta);
	}


	return short();
}

short CChicken::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());


	m_pTransformCom->SetUp(XMFLOAT3(0.f, 1.f, 0.f));
	m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_xmLook, true));
	m_pTransformCom->SetLook(m_xmLook);
	m_pTransformCom->SetPosition(m_xmObjPosition.x, m_xmObjPosition.y, m_xmObjPosition.z);
	m_pTransformCom->SetScale(2.f, 2.f, 2.f);
	m_pTransformCom->Rotate(-90.f, 0.f, 0.f);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CChicken::Render_Shadow()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	skinnedConstants->m_xmf4x4Material = m_pMaterialCom->GetMaterialMat();
	mSkinnedModelInst->UpdateSkinnedAnimation(mClipName, RedenrTimeDelta);

	std::copy(
		std::begin(mSkinnedModelInst->FinalTransforms),
		std::end(mSkinnedModelInst->FinalTransforms),
		&skinnedConstants->BoneTransforms[0]);
	XMStoreFloat4x4(&(skinnedConstants->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(skinnedConstants->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	pCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);

	pCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
	pCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	return NOERROR;
}

HRESULT CChicken::Render_GameObject()
{
	skinnedConstants->m_xmf4x4Material = m_pMaterialCom->GetMaterialMat();
	mSkinnedModelInst->UpdateSkinnedAnimation(mClipName, RedenrTimeDelta * m_fSpeed);
	if (mSkinnedModelInst->Blending)
	{
		mSkinnedModelInst->AnimationBlend(mClipName, mNextClipName, RedenrTimeDelta, 0.1f);
	}
	else
	{
		mClipName = mNextClipName;
		mSkinnedModelInst->UpdateSkinnedAnimation(mClipName, RedenrTimeDelta);

		//�ִϸ��̼� ������ ���� �˻�
		m_aimationEnd = mSkinnedModelInst->IsAnimationEnd(mClipName);
		m_aimationMiddle = mSkinnedModelInst->IsAnimationMiddle(mClipName);

		if (m_aimationMiddle)
		{
			mSkinnedModelInst->Blending = true;
		}
		if (m_aimationEnd)
		{
			mSkinnedModelInst->Blending = true;
			mSkinnedModelInst->BlendTime = 0.f;
		}
	}


	std::copy(
		std::begin(mSkinnedModelInst->FinalTransforms),
		std::end(mSkinnedModelInst->FinalTransforms),
		&skinnedConstants->BoneTransforms[0]);
	XMStoreFloat4x4(&(skinnedConstants->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(skinnedConstants->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER));
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();

	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);


	m_pTextureCom->UpdateShaderVariables(pCommandList);


	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	pd3dCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);

	pd3dCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
	pd3dCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	return NOERROR;
}

void CChicken::OnCollision(CCollider& collisionTarget, double TimeDelta)
{
	CGameObject* pObj = collisionTarget.get_GameObj();
	XMFLOAT3 objPos = pObj->get_ObjPosition();

	XMFLOAT3 dir = Vector3::Subtract(m_xmObjPosition, objPos);
	dir = Vector3::Normalize(dir);
	m_xmObjPosition = Vector3::Add(m_xmObjPosition, dir, TimeDelta*20.f);
}

HRESULT CChicken::Add_Component()
{
	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXANIM", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_GGOGGOMAP, L"Component_Texture_Chicken", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_AnimShadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	sphereCollider tCollider;
	tCollider.pGameObj = this;
	tCollider.fRadius = 0.4f;

	// For.Com_SphereCollider
	if (FAILED(CGameObject::Add_Component(SCENE_GGOGGOMAP, L"Component_SphereCollider", L"Com_SphereCollider", (CComponent**)&m_pSphereColliderCom, &tCollider)))
		return E_FAIL;
	
	return NOERROR;
}

HRESULT CChicken::SetUp_ConstantTable()
{

	return NOERROR;
}

float CChicken::slidingVector(XMFLOAT3 xmNormal, XMFLOAT3 xmLook, XMFLOAT3& outputSlidingVector)
{
	XMFLOAT3 normal = xmNormal;
	XMFLOAT3 look = xmLook;
	look.x *= -1.f;
	look.y *= -1.f;
	look.z *= -1.f;
	XMFLOAT3 slidingVector = Vector3::CrossProduct(look, normal);
	slidingVector = Vector3::CrossProduct(normal, slidingVector);
	slidingVector = Vector3::Add(m_xmLook, slidingVector);

	slidingVector = Vector3::Normalize(slidingVector);

	XMFLOAT3 xmf3dot;
	XMStoreFloat3(&xmf3dot, XMVector3AngleBetweenVectors(XMLoadFloat3(&m_xmLook), XMLoadFloat3(&slidingVector)));
	float tmpangle = XMConvertToDegrees(xmf3dot.y);

	XMFLOAT3 xmf3Cross = Vector3::CrossProduct(m_xmLook, slidingVector);

	XMFLOAT3 AxisY = XMFLOAT3(0.f, 1.f, 0.f);
	float fDot = Vector3::DotProduct(xmf3Cross, AxisY);

	outputSlidingVector = slidingVector;

	return fDot;
}

void CChicken::checkPlayerCollision(double TimeDelta, string& nextAnimation)
{
	XMFLOAT3 playerPos = m_pPlayer->m_tPlayerInfo.xmPosition;
	float playerRadius = 3.f;
	float chickenRadius = m_pSphereColliderCom->get_radius();

	float x = playerPos.x - m_xmObjPosition.x;
	float z = playerPos.z - m_xmObjPosition.z;
	float dist = sqrtf(x * x + z * z);

	if (dist < playerRadius + chickenRadius)
	{
		XMFLOAT3 xmDirection;
		XMStoreFloat3(&xmDirection, XMLoadFloat3(&m_xmObjPosition) - XMLoadFloat3(&playerPos));
		XMFLOAT3 xmf3Cross = Vector3::CrossProduct(m_xmLook, xmDirection);

		XMFLOAT3 AxisY = XMFLOAT3(0.f, 1.f, 0.f);
		float fDot = Vector3::DotProduct(xmf3Cross, AxisY);

		if (fDot > 0) // ��
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&AxisY), XMConvertToRadians(TimeDelta * 200.f));
			m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
		}
		else if (fDot < 0) // ��
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&AxisY), XMConvertToRadians(-TimeDelta * 200.f));
			m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
		}

		mClipName = "Chicken_Run";

		m_iRotate = 0;
	}

	
}

CChicken* CChicken::Create()
{
	CChicken*		pInstance = new CChicken();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CChicken Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject * CChicken::Clone_GameObject(void * pArg)
{
	CChicken*		pInstance = new CChicken(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CChicken Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CChicken::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pMaterialCom);
	Safe_Release(m_pSphereColliderCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	if (m_pd3dVertexUploadBuffer)
		m_pd3dVertexUploadBuffer->Release();

	if (m_pd3dIndexUploadBuffer)
		m_pd3dIndexUploadBuffer->Release();

	m_pd3dVertexUploadBuffer = nullptr;
	m_pd3dIndexUploadBuffer = nullptr;

	if (m_pd3dVertexBuffer)
		m_pd3dVertexBuffer->Release();

	if (m_pd3dIndexBuffer)
		m_pd3dIndexBuffer->Release();


	delete this;

	return NOERROR;
}