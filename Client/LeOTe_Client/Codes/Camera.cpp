#include "Camera.h"
#include "PipeLine.h"
#include "DeviceManager.h"
#include "Management.h"

CCamera::CCamera()
	: m_pPipeLine(CPipeLine::GetInstance())
{
	Add_Component();

	m_pd3dcbCamera = nullptr;
	m_pcbMappedCamera = nullptr;

	m_Viewport = D3D12_VIEWPORT{ 0, 0, WINDOWX , WINDOWY, 0.0f, 1.0f };
	m_ScissorRect = D3D12_RECT{ 0, 0, WINDOWX , WINDOWY };

	m_PerspectiveProjMatrix = Set_PerspectiveFovLH();
	m_OrthographicProjMatrix = Set_OrthogrphicLH();

	m_pd3dcbCamera = m_pShaderCom->CreateShaderVariables(m_pd3dcbCamera, sizeof(VS_CB_CAMERA_INFO));

	m_pd3dcbCamera->Map(0, NULL, (void**)&m_pcbMappedCamera);

	//m_tCameraInfo.xmPosition = XMFLOAT3(0.f, 40.f, -80.f);
	m_tCameraInfo.xmPosition = XMFLOAT3(0.f, 0.f, -80.f);
	m_matView = Matrix4x4::LookAtLH(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt, m_tCameraInfo.xmUp);

	Set_ViewMatrix(m_matView);
	Set_ScissorRect(m_ScissorRect);
	Set_PerspectiveProjMatrix(m_PerspectiveProjMatrix);
	Set_OrthographicProjMatrix(m_OrthographicProjMatrix);
	Set_ProjectionMatrix(PERSPECTIVE_PROJ);
	Set_Viewport(m_Viewport);
	Set_MappedCamera(m_pcbMappedCamera);
	Set_Position(m_tCameraInfo.xmPosition);
	Set_CameraResource(m_pd3dcbCamera);
}

CCamera::CCamera(const CCamera& rhs)
	: m_pPipeLine(CPipeLine::GetInstance())
{
	Add_Component();

	m_pd3dcbCamera = nullptr;
	m_pcbMappedCamera = nullptr;

	m_Viewport = D3D12_VIEWPORT{ 0, 0, WINDOWX , WINDOWY, 0.0f, 1.0f };
	m_ScissorRect = D3D12_RECT{ 0, 0, WINDOWX , WINDOWY };

	m_PerspectiveProjMatrix = Set_PerspectiveFovLH();
	m_OrthographicProjMatrix = Set_OrthogrphicLH();

	m_pd3dcbCamera = m_pShaderCom->CreateShaderVariables(m_pd3dcbCamera, sizeof(VS_CB_CAMERA_INFO));

	m_pd3dcbCamera->Map(0, NULL, (void**)&m_pcbMappedCamera);

	//m_tCameraInfo.xmPosition = XMFLOAT3(0.f, 40.f, -80.f);
	m_tCameraInfo.xmPosition = XMFLOAT3(0.f, 0.f, -80.f);
	m_matView = Matrix4x4::LookAtLH(m_tCameraInfo.xmPosition, m_tCameraInfo.xmLookAt, m_tCameraInfo.xmUp);

	Set_ViewMatrix(m_matView);
	Set_ScissorRect(m_ScissorRect);
	Set_PerspectiveProjMatrix(m_PerspectiveProjMatrix);
	Set_OrthographicProjMatrix(m_OrthographicProjMatrix);
	Set_ProjectionMatrix(PERSPECTIVE_PROJ);
	Set_Viewport(m_Viewport);
	Set_MappedCamera(m_pcbMappedCamera);
	Set_Position(m_tCameraInfo.xmPosition);
	Set_CameraResource(m_pd3dcbCamera);
}

CCamera::~CCamera()
{
}

HRESULT CCamera::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CCamera::Initialize_GameObject(void* pArg)
{
	return NOERROR;
}

short CCamera::Update_GameObject(double TimeDelta)
{
	return 0;
}

short CCamera::LateUpdate_GameObject(double TimeDelta)
{
	return 0;
}

HRESULT CCamera::Render_GameObject()
{
	return NOERROR;
}

HRESULT CCamera::Add_Component()
{
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Shader_Basic", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCamera::Set_ViewMatrix(const XMFLOAT4X4& matView)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_ViewMatrix(matView);

	return NOERROR;
}

void CCamera::LookAt(const XMFLOAT3& pos, const XMFLOAT3& target, const XMFLOAT3& up)
{
	XMVECTOR P = XMLoadFloat3(&pos);
	XMVECTOR T = XMLoadFloat3(&target);
	XMVECTOR U = XMLoadFloat3(&up);

	LookAt(P, T, U);
}

void CCamera::LookAt(FXMVECTOR pos, FXMVECTOR target, FXMVECTOR worldUp)
{
	XMVECTOR L = XMVector3Normalize(XMVectorSubtract(target, pos));
	XMVECTOR R = XMVector3Normalize(XMVector3Cross(worldUp, L));
	XMVECTOR U = XMVector3Cross(L, R);

	XMStoreFloat3(&m_tCameraInfo.xmPosition, pos);
	XMStoreFloat3(&m_tCameraInfo.xmLook, L);
	XMStoreFloat3(&m_tCameraInfo.xmRight, R);
	XMStoreFloat3(&m_tCameraInfo.xmUp, U);

}

void CCamera::UpdateViewMatrix()
{
	XMVECTOR R = XMLoadFloat3(&m_tCameraInfo.xmRight);
	XMVECTOR U = XMLoadFloat3(&m_tCameraInfo.xmUp);
	XMVECTOR L = XMLoadFloat3(&m_tCameraInfo.xmLook);
	XMVECTOR P = XMLoadFloat3(&m_tCameraInfo.xmPosition);

	// Keep camera's axes orthogonal to each other and of unit length.
	L = XMVector3Normalize(L);
	U = XMVector3Normalize(XMVector3Cross(L, R));

	// U, L already ortho-normal, so no need to normalize cross product.
	R = XMVector3Cross(U, L);

	// Fill in the view matrix entries.
	float x = -XMVectorGetX(XMVector3Dot(P, R));
	float y = -XMVectorGetX(XMVector3Dot(P, U));
	float z = -XMVectorGetX(XMVector3Dot(P, L));

	XMStoreFloat3(&m_tCameraInfo.xmRight, R);
	XMStoreFloat3(&m_tCameraInfo.xmUp, U);
	XMStoreFloat3(&m_tCameraInfo.xmLook, L);

	m_matView(0, 0) = m_tCameraInfo.xmRight.x;
	m_matView(1, 0) = m_tCameraInfo.xmRight.y;
	m_matView(2, 0) = m_tCameraInfo.xmRight.z;
	m_matView(3, 0) = x;

	m_matView(0, 1) = m_tCameraInfo.xmUp.x;
	m_matView(1, 1) = m_tCameraInfo.xmUp.y;
	m_matView(2, 1) = m_tCameraInfo.xmUp.z;
	m_matView(3, 1) = y;

	m_matView(0, 2) = m_tCameraInfo.xmLook.x;
	m_matView(1, 2) = m_tCameraInfo.xmLook.y;
	m_matView(2, 2) = m_tCameraInfo.xmLook.z;
	m_matView(3, 2) = z;

	m_matView(0, 3) = 0.0f;
	m_matView(1, 3) = 0.0f;
	m_matView(2, 3) = 0.0f;
	m_matView(3, 3) = 1.0f;
}

void CCamera::GenerateViewMatrix()
{
	//카메라의 z-축을 기준으로 카메라의 좌표축들이 직교하도록 카메라 변환 행렬을 갱신한다. 
	//카메라의 z-축 벡터를 정규화한다. 
	m_tCameraInfo.xmLook = Vector3::Normalize(m_tCameraInfo.xmLook);

	//카메라의 z-축과 y-축에 수직인 벡터를 x-축으로 설정한다. 
	m_tCameraInfo.xmRight = Vector3::CrossProduct(m_tCameraInfo.xmUp, m_tCameraInfo.xmLook, true);

	//카메라의 z-축과 x-축에 수직인 벡터를 y-축으로 설정한다. 
	m_tCameraInfo.xmUp = Vector3::CrossProduct(m_tCameraInfo.xmLook, m_tCameraInfo.xmRight, true);

	m_matView._11 = m_tCameraInfo.xmRight.x; m_matView._12 = m_tCameraInfo.xmUp.x; m_matView._13 = m_tCameraInfo.xmLook.x;
	m_matView._21 = m_tCameraInfo.xmRight.y; m_matView._22 = m_tCameraInfo.xmUp.y; m_matView._23 = m_tCameraInfo.xmLook.y;
	m_matView._31 = m_tCameraInfo.xmRight.z; m_matView._32 = m_tCameraInfo.xmUp.z; m_matView._33 = m_tCameraInfo.xmLook.z;

	XMFLOAT3 xmf3Position = m_tCameraInfo.xmPosition;

	m_matView._41 = -Vector3::DotProduct(xmf3Position, m_tCameraInfo.xmRight);
	m_matView._42 = -Vector3::DotProduct(xmf3Position, m_tCameraInfo.xmUp);
	m_matView._43 = -Vector3::DotProduct(xmf3Position, m_tCameraInfo.xmLook);

	m_xmf4x4ViewProjection = Matrix4x4::Multiply(m_matView, m_PerspectiveProjMatrix);
}

void CCamera::GenerateFrustum()
{
	//원근 투영 변환 행렬에서 절두체를 생성한다(절두체는 카메라 좌표계로 표현된다).
	m_xmFrustum.CreateFromMatrix(m_xmFrustum, XMLoadFloat4x4(&m_pPipeLine->Get_ProjectionMatrix()));
	//카메라 변환 행렬의 역행렬을 구한다. 
	XMMATRIX xmmtxInversView = XMMatrixInverse(NULL, XMLoadFloat4x4(&m_matView));
	//절두체를 카메라 변환 행렬의 역행렬로 변환한다(이제 절두체는 월드 좌표계로 표현된다).
	m_xmFrustum.Transform(m_xmFrustum, XMLoadFloat4x4(&m_pPipeLine->Get_ViewInverseMatrix()));
}

HRESULT CCamera::Set_ProjectionMatrix(PROJECTION_TYPE eProjType)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_ProjectionMatrix(eProjType);

	return NOERROR;
}

HRESULT CCamera::Set_PerspectiveProjMatrix(const XMFLOAT4X4& Matrix)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_PerspectiveProjMatrix(Matrix);

	return NOERROR;
}

HRESULT CCamera::Set_OrthographicProjMatrix(const XMFLOAT4X4& Matrix)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_OrthographicProjMatrix(Matrix);

	return NOERROR;
}

HRESULT CCamera::Set_ScissorRect(const D3D12_RECT& ScissorRect)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_ScissorRect(ScissorRect);

	return NOERROR;
}

HRESULT CCamera::Set_Viewport(const D3D12_VIEWPORT& Viewport)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_Viewport(Viewport);

	return NOERROR;
}

HRESULT CCamera::Set_MappedCamera(VS_CB_CAMERA_INFO* pMappedCamera)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_MappedCamera(pMappedCamera);

	return NOERROR;
}

HRESULT CCamera::Set_Position(const XMFLOAT3& position)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_Position(position);

	return NOERROR;
}

HRESULT CCamera::Set_CameraResource(ID3D12Resource* pd3dcbCamera)
{
	if (m_pPipeLine == nullptr)
		return E_FAIL;

	m_pPipeLine->Set_CameraResource(pd3dcbCamera);

	return NOERROR;
}

XMFLOAT4X4 CCamera::Set_PerspectiveFovLH()
{
	return Matrix4x4::PerspectiveFovLH(XMConvertToRadians(m_tCameraInfo.fFovY), m_tCameraInfo.fAspect, m_tCameraInfo.fNear, m_tCameraInfo.fFar);
}

XMFLOAT4X4 CCamera::Set_OrthogrphicLH()
{
	return Matrix4x4::OrthographicLH(XMConvertToRadians(XMConvertToRadians(m_tCameraInfo.fFovY)), m_tCameraInfo.fAspect, m_tCameraInfo.fNear, m_tCameraInfo.fFar);
}

void CCamera::CreateShaderVariables()
{
	UINT ncbElementBytes = ((sizeof(VS_CB_CAMERA_INFO) + 255) & ~255); //256의 배수
	m_pd3dcbCamera = ::CreateBufferResource(CDeviceManager::GetInstance()->Get_Device(), CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER),
		NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

	m_pd3dcbCamera->Map(0, nullptr, (void**)&m_pcbMappedCamera);

	GenerateFrustum();
}

bool CCamera::IsInFrustum(BoundingBox& xmBoundingBox)
{
	return(m_xmFrustum.Intersects(xmBoundingBox));
}

HRESULT CCamera::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);

	if (m_pd3dcbCamera)
	{
		m_pd3dcbCamera->Unmap(0, nullptr);
		m_pd3dcbCamera->Release();
	}

	//delete m_pcbMappedCamera;
	m_pcbMappedCamera = nullptr;

	return NOERROR;
}
