#pragma once
#include "stdafx.h"
#include "Platform.h"

class CPlatformManager final
{
private:
	explicit CPlatformManager();
	virtual ~CPlatformManager() = default;

	HRESULT Initialize_PlatformManager();



public:
	static CPlatformManager* Create();

	HRESULT Release();

	void Add_Platform(CPlatform* pPlatform) { m_vecPlatform.push_back(pPlatform); }
	CPlatform* Get_Platform(int idx) { return m_vecPlatform[idx]; }
	size_t Get_MaxPlatformSize() { return m_vecPlatform.size(); }

private:
	vector<CPlatform*> m_vecPlatform;

};