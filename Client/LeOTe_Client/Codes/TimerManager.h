#ifndef __TIMERMANAGER__
#define __TIMERMANAGER__

#include "stdafx.h"
#include "Timer.h"

class CTimerManager final
{
	DECLARE_SINGLETON(CTimerManager)

private:
	explicit CTimerManager() {};
	virtual ~CTimerManager();

public:
	double GetTimeDelta(const wchar_t* pTimerTag);
public:
	HRESULT Initialize_Timer(const wchar_t* pTimerTag);
private:
	CTimer* Find_Timer(const wchar_t* pTimerTag);
public:
	HRESULT Release();
private:
	map<const wchar_t*, CTimer*> m_mapTimer;

};

#endif // !__TIMERMANAGER__

