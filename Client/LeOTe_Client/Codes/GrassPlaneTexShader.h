#ifndef __GRASSPLANETEXSHADER__
#define __GRASSPLANETEXSHADER__

#include "Shader.h"

class CGrassPlaneTexShader final : public CShader
{
private:
	explicit CGrassPlaneTexShader();
	explicit CGrassPlaneTexShader(const CGrassPlaneTexShader& rhs);
	virtual ~CGrassPlaneTexShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CGrassPlaneTexShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();

};

#endif
