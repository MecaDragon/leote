#ifndef __Scene_Bullfightmap__
#define __Scene_Bullfightmap__

#include "stdafx.h"
#include "Scene.h"
#include "PlatformManager.h"
#include "Loading.h"

class CPlayer;
class CBoundingBox;
class CFont;
class CPlayerIcon;
class CBillBoardObject;
class CCube;
class CTileLarge;
class CScene_Boardmap;
class CStaticObject;

class CScene_Bullfightmap final : public CScene
{
private:
	explicit CScene_Bullfightmap();
	virtual ~CScene_Bullfightmap();

public:
	virtual HRESULT Initialize_Scene(CScene_Boardmap* pSceneBoardmap);
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();
	HRESULT Scene_Change(SCENEID Scene);
	virtual short KeyEvent(double TimeDelta);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);
	void IntersectTileTest(float TimeDelta);
	int CheckTile(BoundingBox(&xmBoundingBox)[2], CTileLarge* tileObject, float yLoaction, XMINT3 rgb, float TimeDelta);
	void AdjustPosition(CPlayer* pPlayer, CTileLarge* pTileObj);
	void SetGameOver(bool over) { gameOver = over; }
public:
	static CScene_Bullfightmap* Create(CScene_Boardmap* pSceneBoardmap);
	short cameraAnimation(double TimeDelta);
	HRESULT Release();

private:
	CScene_Boardmap* m_pScene_Boardmap = nullptr;
	float moveDelta;
	bool isMoving = false;
	bool gameOver = false;

	int winner = -1;
	bool gameStarted = false;
	bool loadcomplete = false;

	bool GoUp = false;
private:
	CTileLarge* pTile[144] = { nullptr, };
	CTileLarge* pRedTile[144] = { nullptr, };
	CTileLarge* pYellowTile[144] = { nullptr, };
	CBoundingBox* pBoundingBox[2] = { nullptr, };

	CStaticObject* pPosion = nullptr;
	CStaticObject* pMagicCircle = nullptr;

	list<CGameObject*> m_listColObject;

	CFont* m_pEndText = nullptr;
	CFont* m_pStatTimeText = nullptr;
	CFont* pButtonText = nullptr;

	int m_iStartTime = 0;
	bool m_bStartAsk = false;

	int m_iEndTime = 3;
	bool m_bEndAsk = false; //종료 요청 (3초)

	CLoading* m_pLoading = nullptr;

	float m_fCameratime = 0.f;
};

#endif