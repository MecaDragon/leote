#include "Scene_Logo.h"
#include "Management.h"
#include "ServerManager.h"
#include "Scene_Login.h"
#include "Camera_Basic.h"
#include "Camera_Third.h"
#include "Camera_Fixed.h"
#include "Camera_Obj.h"
#include "Player.h"
#include "Logo.h"
#include "StartButton.h"
#include "Light.h"
#include "Texture.h"
#include "Transform.h"
#include "Physic.h"
#include "Plane_Buffer.h"
#include "Cube_Buffer.h"
#include "Material.h"
#include "SoundManager.h"

#include "OBJShader.h"
#include "FBXOBJShader.h"
#include "FBXANIMShader.h"
#include "BoundingBoxShader.h"
#include "SkyBoxShader.h"
#include "FontShader.h"
#include "TextureShader.h"
#include "ShadowShader.h"
#include "AnimShadowShader.h"
#include "UIShader.h"
#include "BillBoardShader.h"
#include "FBXTexShader.h"
#include "FBXTexLightShader.h"
#include "GrassPlaneTexShader.h"
#include "FBXTexLLightShader.h"
#include "FBXTexMLightShader.h"

extern bool G_SC;

CCamera_Third* Camera_Third;
extern CPlayer* Player;

CScene_Logo::CScene_Logo()
{
}

CScene_Logo::~CScene_Logo()
{
}

HRESULT CScene_Logo::Initialize_Scene()
{
	m_SceneId = SCENEID::SCENE_LOGO;

	CServerManager* pServerManager = CServerManager::GetInstance();

	if (SERVERCONNECT) {
		pServerManager->WSAStart();
		pServerManager->CreateSocket();
		pServerManager->ConnectToServer();
	}

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);

	m_pManagement = CManagement::GetInstance();


	if (FAILED(Ready_Component_Static()))
		return E_FAIL;

	if (FAILED(Ready_Light_Material()))
		return E_FAIL;

	if (m_pManagement->Create_ShadowMap())
		return E_FAIL;

	if (FAILED(Ready_GameObject_Prototype()))
		return E_FAIL;

	// For.Layer_BackGround
	if (FAILED(Ready_Layer_BackGround(L"Layer_Camera")))
		return E_FAIL;

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Basic");

	m_pLoading = CLoading::Create(SCENE_LOGIN);

	if (nullptr == m_pLoading)
		return E_FAIL;


	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	return NOERROR;
}

short CScene_Logo::Update_Scene(double TimeDelta)
{
	if (SERVERCONNECT) {
		if (/*CServerManager::GetInstance()->Get_LoginState() == LOGIN_STATE::ID
			&& */m_pLoading->Get_Finish())
		{
			CManagement* pManagement = CManagement::GetInstance();

			if (nullptr == pManagement)
				return -1;

			pManagement->Set_CurrentSceneId(SCENE_LOGIN);

			if (FAILED(pManagement->Initialize_Current_Scene(CScene_Login::Create())))
				return -1;

			if (FAILED(pManagement->Clear_Scene(SCENE_LOGO)))
				return -1;
		}
	}
	else
	{
		if (m_pLoading->Get_Finish())
		{
			CManagement* pManagement = CManagement::GetInstance();

			if (nullptr == pManagement)
				return -1;

			pManagement->Set_CurrentSceneId(SCENE_LOGIN);

			if (FAILED(pManagement->Initialize_Current_Scene(CScene_Login::Create())))
				return -1;

			if (FAILED(pManagement->Clear_Scene(SCENE_LOGO)))
				return -1;
		}
	}

	return 0;
}

short CScene_Logo::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_Logo::Render_Scene()
{
	return NOERROR;
}

HRESULT CScene_Logo::Ready_GameObject_Prototype()
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;

	// For.Prototype_BackGround_Logo
	//if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Camera_Basic", CCamera_Basic::Create())))
	//	return E_FAIL;

	//if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Camera_Third", CCamera_Third::Create())))
	//	return E_FAIL;

	pManagement->Set_LoadingSceneId(SCENE_ALL);

	if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Player", CPlayer::Create())))
		return E_FAIL;

	pManagement->Set_LoadingSceneId(SCENE_LOGO);

	if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Logo", CLogo::Create())))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_StartButton", CStartButton::Create())))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOGO, L"Component_Texture_LogoBackground", CTexture::Create(L"../Data/Resources/Logo/Logo.png", L"LogoBackground", COMMAND_RENDER, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOGO, L"Component_Texture_StartButton", CTexture::Create(L"../Data/Resources/Logo/StartButton.png", L"StartButton", COMMAND_RENDER, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
		return E_FAIL;

	return NOERROR;
}

HRESULT CScene_Logo::Ready_Layer_BackGround(const wchar_t* pLayerTag)
{
	CManagement* pManagement = CManagement::GetInstance();
	CCameraManager* pCameraManager = CCameraManager::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;

	CCamera* pCameraBasic = CCamera_Basic::Create();
	pCameraManager->Add_Camera(pCameraBasic, "Camera_Basic");

	CCamera* pCameraFixed = CCamera_Fixed::Create();
	pCameraManager->Add_Camera(pCameraFixed, "Camera_Fixed");

	CCamera* pCameraObj = CCamera_Obj::Create();
	pCameraManager->Add_Camera(pCameraObj, "Camera_Obj");

	if (!SERVERCONNECT)
	{
		Camera_Third = CCamera_Third::Create();
		pCameraManager->Add_Camera(Camera_Third, "Camera_Third");

		if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_ALL, pLayerTag, L"GameObject_Player", (CGameObject**)&Player)))
			return E_FAIL;

		Camera_Third->SetPlayer(Player);
		Camera_Third->SetOffCameraSet(XMFLOAT3(0.0f, 7.2f, -12.0f));
		Player->SetCamera(Camera_Third);
	}

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_LOGO, pLayerTag, L"GameObject_Logo", nullptr)))
		return E_FAIL;

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_LOGO, pLayerTag, L"GameObject_StartButton", nullptr)))
		return E_FAIL;

	CSoundManager::GetInstance()->AddSoundFile_Folder(L"../Data/Resources/Sound/BackGround/*.*", L"../Data/Resources/Sound/BackGround/", CSoundManager::CHANNELID::BGM);
	CSoundManager::GetInstance()->AddSoundFile_Folder(L"../Data/Resources/Sound/Effect/*.*", L"../Data/Resources/Sound/Effect/", CSoundManager::CHANNELID::ENV);

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Waitroom", 1.f, 0, true); //배경음악 재생

	return NOERROR;
}

HRESULT CScene_Logo::Ready_Light_Material()
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;

	if (FAILED(pManagement->Create_Light_Material()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CScene_Logo::Ready_Component_Static()
{
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Transform", CTransform::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Physic", CPhysic::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Material", CMaterial::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_Basic", COBJShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_FBXOBJ", CFBXOBJShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_FBXANIM", CFBXANIMShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_Texture", CTextureShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_GrassPlaneTex", CGrassPlaneTexShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_BoundingBox", CBoudingBoxShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_SkyBox", CSkyBoxShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_Font", CFontShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_Shadow", CShadowShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_AnimShadow", CAnimShadowShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_UI", CUIShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_BillBoard", CBillBoardShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_FBXTex", CFBXTexShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_FBXTexLight", CFBXTexLightShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_FBXTexLLight", CFBXTexLLightShader::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Shader_FBXTexMLight", CFBXTexMLightShader::Create())))
		return E_FAIL;




	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Plane_Buffer", CPlane_Buffer::Create())))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Cube_Buffer", CCube_Buffer::Create(CCube_Buffer::CUBE_TEX))))
		return E_FAIL;

	return NOERROR;
}

CScene_Logo* CScene_Logo::Create()
{
	CScene_Logo* pInstance = new CScene_Logo();

	if (pInstance->Initialize_Scene())
	{
		MSG_BOX("CScene_Logo Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_Logo::Release()
{
	Safe_Release(m_pLoading);

	delete this;

	return NOERROR;
}
