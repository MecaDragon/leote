#include "CameraManager.h"
#include "Camera.h"

IMPLEMENT_SINGLETON(CCameraManager)

CCameraManager::CCameraManager()
{
}

CCameraManager::~CCameraManager()
{
}

HRESULT CCameraManager::Initialize_CameraManager(_uint iNumScenes)
{
	return NOERROR;
}

short CCameraManager::Update_CameraManager(double TimeDelta)
{
	if (m_pCurrentCamera->Update_GameObject(TimeDelta) == -1)
		return -1;

	return 0;
}

short CCameraManager::LateUpdate_CameraManager(double TimeDelta)
{
	if (m_pCurrentCamera->LateUpdate_GameObject(TimeDelta) == -1)
		return -1;

	return 0;
}

HRESULT CCameraManager::Release()
{
	for (auto& Pair : m_mapCamera)
		Safe_Release(Pair.second);

	delete this;

	return NOERROR;
}

HRESULT CCameraManager::Add_Camera(CCamera* pCamera, string strTag)
{
	if(pCamera != nullptr)
		m_mapCamera.insert(make_pair(strTag, pCamera));

	return NOERROR;
}

CCamera* CCameraManager::Find_Camera(string strTag)
{
	auto	iter = m_mapCamera.find(strTag);

	if (iter == m_mapCamera.end())
		return nullptr;

	return iter->second;
}

HRESULT CCameraManager::Set_CurrentCamera(string strTag)
{
	m_pCurrentCamera = Find_Camera(strTag);

	if (m_pCurrentCamera == nullptr)
		return E_FAIL;

	return NOERROR;
}

HRESULT CCameraManager::Delete_Camera(string strTag)
{


	return NOERROR;
}

