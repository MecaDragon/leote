#pragma once

#include "stdafx.h"
#include "MathHelper.h"

///<summary>
/// A Keyframe defines the bone transformation at an instant in time.
///</summary>
struct Keyframe
{
	Keyframe();
	~Keyframe();

	float TimePos;
	DirectX::XMFLOAT3 Translation;
	DirectX::XMFLOAT3 Scale;
	DirectX::XMFLOAT4 RotationQuat;

	bool operator == (const Keyframe& key)
	{
		if (Translation.x != key.Translation.x || Translation.y != key.Translation.y || Translation.z != key.Translation.z)
			return false;

		if (Scale.x != key.Scale.x || Scale.y != key.Scale.y || Scale.z != key.Scale.z)
			return false;

		if (RotationQuat.x != key.RotationQuat.x || RotationQuat.y != key.RotationQuat.y || RotationQuat.z != key.RotationQuat.z || RotationQuat.w != key.RotationQuat.w)
			return false;

		return true;
	}
};

///<summary>
/// A BoneAnimation is defined by a list of keyframes.  For time
/// values inbetween two keyframes, we interpolate between the
/// two nearest keyframes that bound the time.  
///
/// We assume an animation always has two keyframes.
///</summary>
struct BoneAnimation
{
	float GetStartTime()const;
	float GetEndTime()const;

	void Interpolate(float t, DirectX::XMFLOAT4X4 & M) const;

	std::vector<Keyframe> Keyframes;
};

///<summary>
/// Examples of AnimationClips are "Walk", "Run", "Attack", "Defend".
/// An AnimationClip requires a BoneAnimation for every bone to form
/// the animation clip.    
///</summary>
struct AnimationClip
{
	float GetClipStartTime()const;
	float GetClipEndTime()const;

	void Interpolate(float t, std::vector<DirectX::XMFLOAT4X4>& boneTransforms) const;

	std::vector<BoneAnimation> BoneAnimations;
};

class SkinnedData
{
public:
	UINT BoneCount()const;

	float GetClipStartTime(const std::string& clipName)const;
	float GetClipEndTime(const std::string& clipName)const;
	std::string GetAnimationName(int num) const;
	std::vector<int> GetBoneHierarchy() const;
	std::vector<DirectX::XMFLOAT4X4> GetBoneOffsets() const;
	AnimationClip GetAnimation(std::string clipName) const;
	std::vector<int> GetSubmeshOffset() const;
	DirectX::XMFLOAT4X4 getBoneOffsets(int num) const;
	std::vector<std::string> GetBoneName() const;

	void Set(
		std::vector<int>& boneHierarchy,
		std::vector<DirectX::XMFLOAT4X4>& boneOffsets,
		std::unordered_map<std::string, AnimationClip>* animations = nullptr);
	void SetAnimation(AnimationClip inAnimation, std::string inClipName);
	void SetAnimationName(const std::string& clipName);
	void SetBoneName(std::string boneName);
	void SetSubmeshOffset(int num);

	void clear();

	// In a real project, you'd want to cache the result if there was a chance
	// that you were calling this several times with the same clipName at 
	// the same timePos.
	void GetFinalTransforms(const std::string& clipName, float timePos,
		std::vector<DirectX::XMFLOAT4X4>& finalTransforms)const;


private:
	std::vector<std::string> mBoneName;

	// Gives parentIndex of ith bone.
	std::vector<int> mBoneHierarchy;

	std::vector<DirectX::XMFLOAT4X4> mBoneOffsets;

	std::vector<std::string> mAnimationName;
	std::unordered_map<std::string, AnimationClip> mAnimations;

	std::vector<int> mSubmeshOffset;
};

struct CharacterConstants : CB_GAMEOBJECT_INFO
{
	DirectX::XMFLOAT4X4 BoneTransforms[96];
};

struct BoudingVertex
{
	XMFLOAT3 Max;
	XMFLOAT3 Min;
};

struct FBXVertex
{
	DirectX::XMFLOAT3 Pos;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT3 Binormal;
	DirectX::XMFLOAT3 Targent;
	DirectX::XMFLOAT2 TexC;

	bool operator==(const FBXVertex& other) const
	{
		if (Pos.x != other.Pos.x || Pos.y != other.Pos.y || Pos.z != other.Pos.z)
			return false;

		if (Normal.x != other.Normal.x || Normal.y != other.Normal.y || Normal.z != other.Normal.z)
			return false;

		if (Binormal.x != other.Binormal.x || Binormal.y != other.Binormal.y || Binormal.z != other.Binormal.z)
			return false;

		if (Targent.x != other.Targent.x || Targent.y != other.Targent.y || Targent.z != other.Targent.z)
			return false;

		if (TexC.x != other.TexC.x || TexC.y != other.TexC.y)
			return false;

		return true;
	}
};

struct CharacterVertex : FBXVertex
{
	DirectX::XMFLOAT3 BoneWeights;
	BYTE BoneIndices[4];

	uint16_t MaterialIndex;
};


namespace std {
	template <>
	struct hash<DirectX::XMFLOAT2>
	{
		std::size_t operator()(const DirectX::XMFLOAT2& k) const
		{
			using std::size_t;
			using std::hash;

			// Compute individual hash values for first,
			// second and third and combine them using XOR
			// and bit shifting:

			return ((hash<float>()(k.x)
				^ (hash<float>()(k.y) << 1)) >> 1);
		}
	};

	template <>
	struct hash<DirectX::XMFLOAT3>
	{
		std::size_t operator()(const DirectX::XMFLOAT3& k) const
		{
			using std::size_t;
			using std::hash;

			// Compute individual hash values for first,
			// second and third and combine them using XOR
			// and bit shifting:

			return ((hash<float>()(k.x)
				^ (hash<float>()(k.y) << 1)) >> 1)
				^ (hash<float>()(k.z) << 1);
		}
	};

	template <>
	struct hash<FBXVertex>
	{
		std::size_t operator()(const FBXVertex& k) const
		{
			using std::size_t;
			using std::hash;

			// Compute individual hash values for first,
			// second and third and combine them using XOR
			// and bit shifting:

			return ((hash<DirectX::XMFLOAT3>()(k.Pos)
				^ (hash<DirectX::XMFLOAT3>()(k.Normal) << 1)) >> 1)
				^ (hash<DirectX::XMFLOAT2>()(k.TexC) << 1);
		}
	};
}

struct SkinnedModelInstance
{
	SkinnedData* SkinnedInfo = nullptr;
	std::vector<DirectX::XMFLOAT4X4> FinalTransforms;
	std::vector<DirectX::XMFLOAT4X4> BeforeTransforms;
	std::vector<DirectX::XMFLOAT4X4> AfterTransforms;
	float TimePos = 0.0f;
	float BlendTime = 0.f;
	bool Blending = false;
	//eClipList mState;

	void UpdateSkinnedAnimation(std::string ClipName, float dt)
	{
		TimePos += dt;

		// Loop animation
		if (ClipName == "walk" || ClipName == "run" || ClipName == "idle1" || ClipName == "idle2" || ClipName == "idle3" || ClipName == "freefall")
		{
			if (TimePos > SkinnedInfo->GetClipEndTime(ClipName))
			{
				TimePos = 0.0f;
			}
		}

		/*
		eClipList state = eClipList::Idle;
		if (ClipName == "Idle")
		{
			state = eClipList::Idle;
		}
		else if (ClipName == "StartWalking")
			state = eClipList::StartWalking;
		else if (ClipName == "Walking" || ClipName == "WalkingBackward" || ClipName == "run" || ClipName == "playerWalking")
			state = eClipList::Walking;
		else if (ClipName == "StopWalking")
			state = eClipList::StopWalking;
		else if (ClipName == "Kick")
			state = eClipList::Kick;
		else if (ClipName == "FlyingKick")
			state = eClipList::FlyingKick;

		if (state != mState)
		{
			TimePos = 0.0f;
			mState = state;
		}
		*/
		// Compute the final transforms for this time position.
		SkinnedInfo->GetFinalTransforms(ClipName, TimePos, FinalTransforms);
	}

	bool IsAnimationEnd(std::string ClipName)
	{
		if (TimePos > SkinnedInfo->GetClipEndTime(ClipName))
		{
			return true;
		}
		return false;
	}

	bool IsAnimationMiddle(std::string ClipName)
	{
		if (TimePos > SkinnedInfo->GetClipEndTime(ClipName) / 2.3f)
		{
			return true;
		}
		return false;
	}

	void AnimationBlend(std::string ClipName, std::string NextName, float dt, float weight)
	{
		TimePos += dt;
		BlendTime += dt;
		// Loop animation
		if (TimePos > SkinnedInfo->GetClipEndTime(ClipName))
		{
			TimePos = 0.0f;
		}

		SkinnedInfo->GetFinalTransforms(ClipName, TimePos, BeforeTransforms);
		SkinnedInfo->GetFinalTransforms(NextName, BlendTime, AfterTransforms);

		for (int i = 0; i < FinalTransforms.size(); i++)
		{
			//FinalTransforms[i] = Matrix4x4::Interpolate(BeforeTransforms[i], AfterTransforms[i], weight);
			//FinalTransforms[i] = Matrix4x4::Add(Matrix4x4::Scale(BeforeTransforms[i], weight), Matrix4x4::Scale(AfterTransforms[i], (1 - weight)));
			FinalTransforms[i] = Matrix4x4::Add(Matrix4x4::Scale(BeforeTransforms[i], 1.f - BlendTime * 5.0f), Matrix4x4::Scale(AfterTransforms[i], BlendTime * 5.0f));
		}

		if (BlendTime > 0.2f)
		{
			Blending = false;
			ClipName = NextName;
			TimePos = BlendTime;
			BlendTime = 0.f;
		}
	}
};