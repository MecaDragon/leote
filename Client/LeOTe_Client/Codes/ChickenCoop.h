#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CMaterial;
class CShadowShader;

class CChickenCoop final : public CGameObject
{
private:
	explicit CChickenCoop();
	explicit CChickenCoop(const CChickenCoop& rhs);
	virtual ~CChickenCoop() = default;
public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);

	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();
public:
	static CChickenCoop* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();

	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;
};