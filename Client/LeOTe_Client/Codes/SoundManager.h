#ifndef __SOUNDMANAGER__
#define __SOUNDMANAGER__

//#ifdef _DEBUG
//#define _ATL_DISABLE_NOTHROW_NEW
//#endif // !_DEBUG
//
//#include "Base.h"
//
//#ifdef _DEBUG
//#define _ATL_DISABLE_NOTHROW_NEW
//#endif // !_DEBUG
//
//// 사운드 매니저를 사용하기 위해 추가한 해더들. 
//#ifdef _DEBUG
//#define _ATL_DISABLE_NOTHROW_NEW
//#endif // !_DEBUG

#include <io.h>
#include "fmod.hpp"
#include "stdafx.h"

// SoundMgr
// 사운드 파일 읽고 관리

class CCamera;

class CSoundManager
{
public:
	// 사운드 채널
	enum class CHANNELID
	{
		BGM,	// 배경음(배경음)
		SE,		// 효과음(총 발사음 등)
		ENV,	// 환경음(불타는 소리, 걷는 소리 등)
		VOICE,	// 목소리
		IDEND
	};
	typedef struct tagSoundInfo
	{
		FMOD::Sound* pSound;
		FMOD::ChannelGroup* pChannelGroup;
		tagSoundInfo(FMOD::Sound* Sound = nullptr, FMOD::ChannelGroup* ChannelGroup = nullptr) : pSound(Sound), pChannelGroup(ChannelGroup) {}
	}SOUNDINFO;
	typedef struct tag3DListInfo
	{
		vector<wstring> vecList;
		_uint			iPlayIndex;
		FMOD_VECTOR		vPos;
	}PLAY3DLISTINFO;
private:
	DECLARE_SINGLETON(CSoundManager)
	////////////////////////////////////
	// 생성자, 소멸자, Free, 생성함수 /////
	////////////////////////////////////
private:
	explicit CSoundManager();
	virtual ~CSoundManager();
public:
	HRESULT Release();
	////////////////////////////////////
	// 게임 루프 함수 ////////////////////
	////////////////////////////////////
public:
	HRESULT Initialize_SoundManager();
	short Update_SoundManager(CCamera* pCamera);
protected:

private:

	////////////////////////////////////
	// 기타 함수,변수 ////////////////////
	////////////////////////////////////
public:
	void			Play_Sound(wstring wstrSoundKey, float fVolume = 1.f, int iFadeInSec = 0, bool bLoop = false);		// 소리 재생
	void			Play_Sound_3D(wstring wstrSoundKey, XMFLOAT3 xmPos, float fVolume = 1.f, FMOD::Channel** pChannel = nullptr);								// 소리 3D 재생
	void			Play_Sound_List(vector<wstring>& vecSoundKey);									// 소리 순차 재생
	void			Play_Sound_3D_List(vector<wstring>& vecSoundKey, XMFLOAT3 xmPos);									// 소리 순차 재생
	void			Pause_Channel(CHANNELID eChannelIndex);											// 채널(BGM/SE 등) 일시정지
	void			Resume_Channel(CHANNELID eChannelIndex);										// 채널(BGM/SE 등) 재개
	void			Stop_Channel(CHANNELID eChannelIndex);											// 채널(BGM/SE 등) 정지
	void			Pause_Sound(wstring wstrSoundKey);												// 해당 소리 채널 일시정지
	void			Resume_Sound(wstring wstrSoundKey);												// 해당 소리 채널 재개
	void			Stop_Sound(wstring wstrSoundKey, int iFadeOutSec = 0);							// 해당 소리 채널 정지
	void			SetSpeed_Sound(wstring wstrSoundKey, float fSpeed);							// 소리의 속도(피치) 변경(일반적으로 보이스는 0.7배임)
	void			SetLoopPoints_Sound(wstring wstrSoundKey, _uint iStartMS, _uint iEndMS);		// 소리의 A-B루프 구간 정하기(milSecond단위. B 루프 구간 지나면 A로 진입함)
	void			Stop_All();																		// 모든 채널(BGM/SE 등 전체) 정지

	float			Get_Volume(CHANNELID eChannelIndex);											// 소리 볼륨 가져오기
	void			Set_Volume(CHANNELID eChannelIndex, float fVolume);							// 소리 볼륨 설정
	void			Set_Position(wstring wstrSoundKey, XMFLOAT3 xmPos);
	void			Set_PlaySecPosition(wstring wstrSoundKey, _uint iPosMS);	// 소리 재생 위치 변경
	bool			Is_Playing(wstring wstrSoundKey);												// 소리 재생중 여부(1개 이상 채널에서 재생중 여부)
	_uint			Get_Length(wstring wstrSoundKey);												// 전체 길이 반환(milSecond 단위)
	void			Set_3DCustomRolloff(wstring wstrSoundKey, vector<pair<float, float>>& vecLevel);

	HRESULT			AddSoundFile(wstring wstrFilePath, wstring wstrSoundKey, CHANNELID eChannelIndex, bool b3D = false, float fDefaultSpeed = 1.f);				// 사운드 읽고 추가
	HRESULT			AddSoundFile_Folder(wstring wstrFileName, wstring wstrFolderPath, CHANNELID eChannelIndex, bool b3D = false, float fDefaultSpeed = 1.f);		// 사운드 폴더 내 전체 읽고 추가
protected:

private:
	void			Play_2D_List();																															// Update에서 사용
	void			Play_3D_List();																															// Update에서 사용
	void			FindFiles(const wstring& rFile, const wstring& rFolder, CHANNELID eChannelIndex, bool b3D, float fDefaultSpeed = 1.f);				// AddSoundFile_Folder에서 사용

	FMOD::System* m_pSystem;									// FMOD 관리 객체
	//map<wstring, SOUNDINFO>				m_mapSound;									// 사운드 정보
	unordered_map<wstring, SOUNDINFO>				m_mapSound;									// 사운드 정보
	FMOD::ChannelGroup* m_pChannelGroup[(_uint)CHANNELID::IDEND];	// 분류 채널
	float								m_fVolume[(_uint)CHANNELID::IDEND];			// 채널별 사운드 정보
	vector<pair<vector<wstring>, _uint>> m_vecPlay2DList;							// 순차 2D 재생 리스트 저장용 (second 인자는 현재 재생중 인덱스)
	vector<PLAY3DLISTINFO>				m_vecPlay3DList;							// 순차 3D 재생 리스트 저장용 (second 인자는 현재 재생중 인덱스)
	vector<FMOD_VECTOR*>				m_vecCustomRollOffLevel;					// 커스텀레벨 저장용
	FMOD_VECTOR							m_vListenerUp;
	FMOD_VECTOR							m_vListenerLook;
	FMOD_VECTOR							m_vListenerPos;
	vector<FMOD::Channel*>				m_vecStartChannel;							// 설정이 완료된 후 재생
};

#endif // !SoundMgr_h__
