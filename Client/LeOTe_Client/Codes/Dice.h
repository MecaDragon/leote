#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CTransform;
class CMesh;
class CTexture;
class CRenderer;

class CPlayer;

class CDice final : public CGameObject
{
private:
	explicit CDice();
	explicit CDice(const CDice& rhs);
	virtual ~CDice() = default;
public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	CTransform* m_pTransformCom = nullptr;

private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	FbxScene* m_pfbxScene = NULL;
	std::unique_ptr<SkinnedModelInstance> mSkinnedModelInst;
	std::vector<FBXVertex> outSkinnedVertices;
	std::vector<std::uint32_t> outIndices;
	std::vector<Material> outMaterial;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

	ID3D12Resource* m_pd3dVertexBuffer = NULL;
	ID3D12Resource* m_pd3dVertexUploadBuffer = NULL;

	ID3D12Resource* m_pd3dIndexBuffer = NULL;
	ID3D12Resource* m_pd3dIndexUploadBuffer = NULL;

	D3D12_VERTEX_BUFFER_VIEW		m_d3dVertexBufferView;
	D3D12_INDEX_BUFFER_VIEW			m_d3dIndexBufferView;

	D3D12_PRIMITIVE_TOPOLOGY		m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT							m_nSlot = 0;
	UINT							m_nVertices = 0;
	UINT							m_nStride = 0;
	UINT							m_nOffset = 0;

	UINT							m_nIndices = 0;
	UINT							m_nStartIndex = 0;
	int								m_nBaseVertex = 0;

	XMFLOAT3 position = { 0.f, 0.f, 0.f };
	float y_rotation = 0.f;

	float m_fTimer = 0.f;

	int m_iCoinType = 0;

	_uint m_nTexIndex = 0;

	bool m_bMiniDice = false;
	bool m_bBuyDice = false;

	CPlayer* m_pPlayer = nullptr;

	XMFLOAT3 m_xmLook = XMFLOAT3(0.f, 0.f, 1.f);
	XMFLOAT3 m_xmScale = XMFLOAT3(0.0001f, 0.0001f, 0.0001f);

private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();

public:
	static CDice* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();

	void setTexIndex(_uint nTexIndex) { m_nTexIndex = nTexIndex; }
	_uint getTexIndex() { return m_nTexIndex; }

	void setMiniDice(bool bMiniDice) { m_bMiniDice = bMiniDice; }
	bool getMiniDice() { return m_bMiniDice; }

	void setBuyDice(bool bBuyDice) { m_bBuyDice = bBuyDice; }
	bool getBuyDice() { return m_bBuyDice; }

	void setPlayer(CPlayer* pPlayer) { m_pPlayer = pPlayer; }

	void setScale(XMFLOAT3 xmScale) { m_xmScale = xmScale; }

	void setLook(XMFLOAT3 xmLook) { m_xmLook = xmLook; }
	XMFLOAT3 getLook() { return m_xmLook; }
};