#ifndef __COMPONENT__
#define __COMPONENT__

#include "stdafx.h"

class CComponent
{
protected:
	explicit CComponent();
	explicit CComponent(const CComponent& rhs);
	virtual ~CComponent() = default;

public:
	bool Get_Clone() const { return m_bClone; }

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg) = 0;

	virtual HRESULT Release();

private:
	bool	m_bClone = false;

	
};

#endif