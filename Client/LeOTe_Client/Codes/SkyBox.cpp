#include "stdafx.h"
#include "SkyBox.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"

CSkyBox::CSkyBox()
	: CGameObject()
{

}

CSkyBox::CSkyBox(const CSkyBox& rhs)
	: CGameObject(rhs)
{
}

HRESULT CSkyBox::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CSkyBox::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
	}

	m_pTransformCom->SetScale(2.f, 2.f, 2.f);

	return NOERROR;
}

short CSkyBox::Update_GameObject(double TimeDelta)
{
	XMFLOAT3 rotateAxis = XMFLOAT3(0.f, 0.f, 1.f);
	y_rotation = 0.7f * TimeDelta;
	m_pTransformCom->Rotate(&rotateAxis, y_rotation);
	return short();
}

short CSkyBox::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_PRIORITY, this)))
		return -1;

	return short();
}

HRESULT CSkyBox::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList, CTexture::TEX_SKYBOX, CManagement::GetInstance()->Get_CurrentSceneId() - 3);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CSkyBox::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_SkySphere_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_SkyBox", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_STATIC, L"Component_Texture_SkySphere", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CSkyBox::SetUp_ConstantTable()
{
	return NOERROR;
}

CSkyBox* CSkyBox::Create()
{
	CSkyBox* pInstance = new CSkyBox();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CSkyBox Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CSkyBox::Clone_GameObject(void* pArg)
{
	CSkyBox* pInstance = new CSkyBox(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CSkyBox Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CSkyBox::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
