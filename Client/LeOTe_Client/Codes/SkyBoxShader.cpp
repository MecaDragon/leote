#include "SkyBoxShader.h"

CSkyBoxShader::CSkyBoxShader()
	: CShader()
{
}

CSkyBoxShader::CSkyBoxShader(const CSkyBoxShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

CSkyBoxShader::~CSkyBoxShader(void)
{
}

HRESULT CSkyBoxShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"SkyBox Shader PipelineState");
	return NOERROR;
}

HRESULT CSkyBoxShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CSkyBoxShader::Clone_Component(void* pArg)
{
	CSkyBoxShader* pInstance = new CSkyBoxShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CSkyBoxShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CSkyBoxShader* CSkyBoxShader::Create()
{
	CSkyBoxShader* pInstance = new CSkyBoxShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CSkyBoxShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_SHADER_BYTECODE CSkyBoxShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Skybox.hlsl", "VSSkyBox", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CSkyBoxShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/Skybox.hlsl", "PSSkyBox", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CSkyBoxShader::Create_InputLayout()
{
	UINT nInputElementDescs;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 5;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TANNGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;


    return(d3dInputLayoutDesc);
}

D3D12_RASTERIZER_DESC CSkyBoxShader::Create_RasterizerState()
{
	D3D12_RASTERIZER_DESC d3dRasterizerDesc;
	::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0;
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(d3dRasterizerDesc);
}

HRESULT CSkyBoxShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
