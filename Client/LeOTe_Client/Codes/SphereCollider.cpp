#include "SphereCollider.h"

CSphereCollider::CSphereCollider()
{
}

CSphereCollider::CSphereCollider(const CSphereCollider& rhs)
	: CCollider(rhs)
{
}

CSphereCollider::~CSphereCollider(void)
{
}

HRESULT CSphereCollider::Initialize_Component_Prototype()
{
	return NOERROR;
}

HRESULT CSphereCollider::Initialize_Component(void* pArg)
{
	if (pArg != nullptr)
	{
		sphereCollider* pColl = (sphereCollider*)pArg;
		m_pTargetObj = pColl->pGameObj;
		m_fRadius = pColl->fRadius;
	}

	return NOERROR;
}

CComponent* CSphereCollider::Clone_Component(void* pArg)
{
	CSphereCollider* pInstance = new CSphereCollider(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CSphereCollider Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CSphereCollider* CSphereCollider::Create()
{
	CSphereCollider* pInstance = new CSphereCollider();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CSphereCollider Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CSphereCollider::Release()
{
	delete this;

	return NOERROR;
}
