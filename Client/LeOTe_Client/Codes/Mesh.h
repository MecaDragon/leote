//------------------------------------------------------- ----------------------
// File: Mesh.h
//-----------------------------------------------------------------------------
#pragma once

#include "Component.h"
#include "FbxLoader.h"
#include "Buffer.h"

class CMesh final : public CBuffer
{
private:
	explicit CMesh();
	explicit CMesh(const CMesh& rhs);
	virtual ~CMesh(void);

public:
	virtual HRESULT Initialize_Component_Prototype(void* pArg);
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CMesh* Create(void* pArg);
	static	CMesh* Create();

	virtual void Render();

	HRESULT Release();


public:
	XMFLOAT4						*m_pxmf4MappedPositions = NULL;

	std::vector<FBXVertex> outVertices;
	std::vector<std::uint32_t> outIndices;
	std::vector<Material> outMaterial;
	BoudingVertex boundingVertex;
	BoundingBox boudingBox;
	/*ID3D12Resource* m_pd3dVertexBuffer = NULL;
	ID3D12Resource* m_pd3dVertexUploadBuffer = NULL;
	ID3D12Resource* m_pd3dIndexBuffer = NULL;
	ID3D12Resource* m_pd3dIndexUploadBuffer = NULL;*/

};
