#include "Scene_Fallingmap.h"
#include "Scene_Boardmap.h"
#include "Scene_WaitingRoom.h"
#include "Management.h"
#include "TeddyBear.h"
#include "Physic.h"
#include "Player.h"
#include "BoundingBox.h"
#include "Font.h"
#include "BillBoardObject.h"
#include "Camera_Third.h"
#include "TileLarge.h"
#include "UI.h"

extern bool G_SC;

/*
#include "PxPhysicsAPI.h"

using namespace physx;

#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3_x64.lib" )
#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Common_x64.lib" )
#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Cooking_x64.lib" )
#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Extensions.lib" )
#pragma comment( lib, "../Codes/Physx/PxShared/lib/PxFoundation_x64.lib" )
#pragma comment( lib, "../Codes/Physx/PxShared/lib/PxPvdSDK_x64.lib" )
#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Vehicle.lib" )
// 
//#pragma comment( lib,"../Codes/Physx/PxShared/lib/PxFoundation_x64")
//#pragma comment( lib,"../Codes/Physx/PxShared/lib/PxPvdSDK_x64.lib" )
//#pragma comment( lib,"../Codes/Physx/PxShared/lib/PhysX3_x64.lib" )
//#pragma comment( lib,"../Codes/Physx/PxShared/lib/PhysX3Extensions.lib" )
//
//
//#pragma comment( lib,"../Codes/Physx/PxShared/lib/PhysX3Common_x64.lib" )
////#pragma comment( lib, "PhysX/lib/debug_x64/PhysXFoundation_64.lib" )
////#pragma comment( lib, "PhysX/lib/debug_x64/PhysXPvdSDK_static_64.lib" )
////#pragma comment( lib, "PhysX/lib/debug_x64/PhysXVehicle_static_64.lib" )

//#if ( defined( _WIN64 ) & defined( _DEBUG ) )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysX_64.lib" )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysXCommon_64.lib" )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysXCooking_64.lib" )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysXExtensions_static_64.lib" )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysXFoundation_64.lib" )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysXPvdSDK_static_64.lib" )
//#pragma comment( lib, "PhysX/lib/debug_x64/PhysXVehicle_static_64.lib" )
//#elif ( defined ( _WIN64 ) & defined( NDEBUG ) )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3_x64.lib" )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Common_x64.lib" )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Cooking_x64.lib" )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Extensions.lib" )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PxFoundation_x64.lib" )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PxPvdSDK_x64.lib" )
//#pragma comment( lib, "../Codes/Physx/PxShared/lib/PhysX3Vehicle.lib" )
//#endif

class CCPhysX
{
private:
	PxDefaultAllocator		m_Allocator;
	PxDefaultErrorCallback	m_ErrorCallback;
	PxFoundation* m_pFoundation;
	PxPhysics* m_pPhysics;
	PxDefaultCpuDispatcher* m_pDispatcher;
	PxPvd* m_pPVD;
	PxScene* m_pScene;
public:
	bool Initialize();
	bool CreateScene();
};

bool CCPhysX::Initialize()
{
	char* strTransport = "127.0.0.1";

	m_pFoundation = PxCreateFoundation(PX_FOUNDATION_VERSION, m_Allocator, m_ErrorCallback);

	m_pPVD = PxCreatePvd(*m_pFoundation);
	PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate(strTransport, 5425, 10);
	bool bPVDConnectionResult = m_pPVD->connect(*transport, PxPvdInstrumentationFlag::eALL);
	if (!bPVDConnectionResult)
	{
		//PVD 연결에 실패함
	}

	m_pPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_pFoundation, PxTolerancesScale(), true, m_pPVD);
	m_pDispatcher = PxDefaultCpuDispatcherCreate(2);
	return true;
}

bool CCPhysX::CreateScene()
{
	PxSceneDesc sceneDesc(m_pPhysics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0.f, -9.81f, 0.f);
	sceneDesc.cpuDispatcher = m_pDispatcher;
	//sceneDesc.simulationEventCallback = this;
	//sceneDesc.filterShader = FilterShadr

	m_pScene = m_pPhysics->createScene(sceneDesc);
	m_pScene->setFlag(PxSceneFlag::eENABLE_KINEMATIC_PAIRS, true);
	m_pScene->setFlag(PxSceneFlag::eENABLE_KINEMATIC_STATIC_PAIRS, true);

	PxPvdSceneClient* pvdClient = m_pScene->getScenePvdClient();
	if (pvdClient)
	{
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
	}
	PxMaterial* pMaterial = m_pPhysics->createMaterial(0.5f, 0.5f, 0.5f);
	PxRigidStatic* groundPlane = PxCreatePlane(*m_pPhysics, PxPlane(0, 1, 0, 0), *pMaterial);
	m_pScene->addActor(*groundPlane);
	return true;
}
*/

extern CPlayer* Player;
extern CPlayer* Players[3];

extern CTeddyBear* pTeddyBear[2];

CScene_Fallingmap::CScene_Fallingmap()
{
}

CScene_Fallingmap::~CScene_Fallingmap()
{
}
//
//void IntersectObject(CPlayer* pHit, CPlayer* pTeddy, float TimeDelta)
//{
//	BoundingBox xmBoundingBox[2];
//	xmBoundingBox[0] = pHit->Get_TeddyBear()->m_xmBoundingBox;
//	xmBoundingBox[1] = pTeddy->Get_TeddyBear()->m_xmBoundingBox;
//	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&pHit->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
//	xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&pTeddy->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
//
//	if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
//	{
//		XMFLOAT3 xmLookVector = Vector3::Normalize(Vector3::Subtract(pTeddy->GetPosition(), pHit->GetPosition()));
//		pTeddy->xmf3Shift = Vector3::Add(XMFLOAT3(0.f, 0.f, 0.f), xmLookVector, PLAYERSPEED * TimeDelta);
//	}
//}
//
//void IntersectObjectTest(CPlayer* pHit, CTeddyBear* pTeddy, float TimeDelta)
//{
//	BoundingBox xmBoundingBox[2];
//	xmBoundingBox[0] = pHit->Get_TeddyBear()->m_xmBoundingBox;
//	xmBoundingBox[1] = pTeddy->m_xmBoundingBox;
//	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&pHit->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));
//	xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&pTeddy->m_pTransformCom->GetWorldMat()));
//
//	if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
//	{
//		XMFLOAT3 xmLookVector = Vector3::Normalize(Vector3::Subtract(pTeddy->m_pTransformCom->GetPosition(), pHit->GetPosition()));
//		xmLookVector.y = 0.f;
//		pTeddy->m_pTransformCom->SetPosition(Vector3::Add(pTeddy->m_pTransformCom->GetPosition(), xmLookVector, PLAYERSPEED * TimeDelta));
//	}
//}
//

int CScene_Fallingmap::CheckTile(BoundingBox(&xmBoundingBox)[2], CTileLarge* tileObject, float yLoaction, XMINT3 rgb, float TimeDelta, int blockindex)
{
	if (!tileObject->GetDel())
	{
		xmBoundingBox[1] = tileObject->m_xmBoundingBox;
		//xmBoundingBox[1].Transform(xmBoundingBox[1], XMLoadFloat4x4(&tileObject->m_pTransformCom->GetWorldMat()));
		if (xmBoundingBox[0].Intersects(xmBoundingBox[1]))
		{
			if (Player->GetPosition().y >= tileObject->m_pTransformCom->GetPosition().y)
			{
				if (!SERVERCONNECT)
				{
					//AdjustPosition(Player, tileObject);
					tileObject->SetMaterialDiffuseRGBA(255, 255, 255, 256);
					tileObject->m_pTransformCom->SetPosition(tileObject->m_pTransformCom->GetPosition().x, yLoaction - 0.1f, tileObject->m_pTransformCom->GetPosition().z);
					tileObject->SetDelTime(true);
					tileObject->SetChecked(true);
				}

				if (Player->GetState() != OBJECT_STATE::OBJSTATE_GROUND)
				{
					//Player->SetAnimation("idle1");
					Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);
				}

				return 1;
			}
			
		}
		//tileObject->SetMaterialDiffuseRGBA(rgb.x, rgb.y, rgb.z, 256);
		tileObject->m_pTransformCom->SetPosition(tileObject->m_pTransformCom->GetPosition().x, yLoaction, tileObject->m_pTransformCom->GetPosition().z);
	}
	return 0;
}

void CScene_Fallingmap::IntersectTileTest(float TimeDelta)
{
	BoundingBox xmBoundingBox[2];
	xmBoundingBox[0] = ::Player->Get_TeddyBear()->m_xmBoundingBox;
	xmBoundingBox[0].Transform(xmBoundingBox[0], XMLoadFloat4x4(&::Player->Get_TeddyBear()->m_pTransformCom->GetWorldMat()));

	int colcount = 0;

	float TSCALE = 1.5f;
	int TileNum = 12;

	int x = Player->m_tPlayerInfo.xmPosition.x / (TSCALE * 2.f); //x 인덱스
	int y = (33.f - Player->m_tPlayerInfo.xmPosition.z) / (TSCALE * 2.f); //y 인덱스

	int index = x + (y * TileNum); // 12 - 타일 내의 인덱스 가로 갯수

	if (index <= 143 && index >= 0)
	{
		colcount += CheckTile(xmBoundingBox, pTile[index], 15.f, XMINT3(0, 165, 255), TimeDelta, index);
		colcount += CheckTile(xmBoundingBox, pRedTile[index], 0.f, XMINT3(244, 67, 54), TimeDelta, index);
		colcount += CheckTile(xmBoundingBox, pYellowTile[index], -15.f, XMINT3(255, 255, 59), TimeDelta, index);

		if (index % 12 != 0) //왼쪽 있음
		{
			colcount += CheckTile(xmBoundingBox, pTile[index - 1], 15.f, XMINT3(0, 165, 255), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pRedTile[index - 1], 0.f, XMINT3(244, 67, 54), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pYellowTile[index - 1], -15.f, XMINT3(255, 255, 59), TimeDelta, index);
		}
		if (index % 12 != 11) //오른쪽 있음
		{
			colcount += CheckTile(xmBoundingBox, pTile[index + 1], 15.f, XMINT3(0, 165, 255), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pRedTile[index + 1], 0.f, XMINT3(244, 67, 54), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pYellowTile[index + 1], -15.f, XMINT3(255, 255, 59), TimeDelta, index);
		}
		if (index > 11) //위 있음
		{
			colcount += CheckTile(xmBoundingBox, pTile[index - 12], 15.f, XMINT3(0, 165, 255), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pRedTile[index - 12], 0.f, XMINT3(244, 67, 54), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pYellowTile[index - 12], -15.f, XMINT3(255, 255, 59), TimeDelta, index);
		}
		if (index < 132) //아래 있음
		{
			colcount += CheckTile(xmBoundingBox, pTile[index + 12], 15.f, XMINT3(0, 165, 255), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pRedTile[index + 12], 0.f, XMINT3(244, 67, 54), TimeDelta, index);
			colcount += CheckTile(xmBoundingBox, pYellowTile[index + 12], -15.f, XMINT3(255, 255, 59), TimeDelta, index);
		}
	}
	else {
		colcount++; //인덱스 내부에 없으면 공중인 것
	}

	if (colcount == 0)
	{
		if (Player->GetState() == OBJECT_STATE::OBJSTATE_FLY && Player->m_AnimState == ANIM_JUMP && !Player->IsPlayerAnimMiddle())
		{

		}
		else if (Player->GetState() != OBJECT_STATE::OBJSTATE_FLY || Player->m_AnimState != ANIM_JUMP )
		{
			Player->SetAnimation("freefall");
			Player->SetState(OBJECT_STATE::OBJSTATE_FALLING);
		}
		Player->m_tPlayerInfo.xmPosition.y -= PLAYERSPEED * TimeDelta;
	}
	
}

void CScene_Fallingmap::AdjustPosition(CPlayer* pPlayer, CTileLarge* pTileObj)
{
	CTeddyBear* pTed = pPlayer->Get_TeddyBear();

	XMFLOAT3 xmPlayerPos = pPlayer->m_tPlayerInfo.xmPosition;
	//XMFLOAT3 xmPlayerPos = pTed->m_pTransformCom->GetPosition();
	XMFLOAT3 xmTilePos = pTileObj->m_pTransformCom->GetPosition();

	XMFLOAT3 xmPlayerSize(pTed->m_xmBoundingBox.Extents.x, pTed->m_xmBoundingBox.Extents.z, -pTed->m_xmBoundingBox.Extents.y);
	XMFLOAT3 xmTileSize = pTileObj->m_xmBoundingBox.Extents;

	XMFLOAT3 xmPlayerMax;
	XMFLOAT3 xmPlayerMin;

	XMFLOAT3 xmTileMax;
	XMFLOAT3 xmTileMin;

	xmPlayerMin = Vector3::Subtract(xmPlayerPos, xmPlayerSize);
	xmPlayerMax = Vector3::Add(xmPlayerPos, xmPlayerSize);

	xmTileMin = Vector3::Subtract(xmTilePos, xmTileSize);
	xmTileMax = Vector3::Add(xmTilePos, xmTileSize);

	if (xmPlayerMax.y > xmTileMax.y&& xmPlayerMin.y < xmTileMin.y)
	{
		if (xmPlayerMin.x < xmTileMax.x)
		{
			xmPlayerPos.x = xmPlayerPos.x + (xmTileMax.x - xmPlayerMin.x);
		}
		else if (xmTileMin.x < xmPlayerMax.x)
		{
			xmPlayerPos.x = xmPlayerPos.x - (xmTileMax.x - xmPlayerMin.x);
		}

		if (xmPlayerMin.z < xmTileMax.z)
		{
			xmPlayerPos.z = xmPlayerPos.z + (xmTileMax.z - xmPlayerMin.z);
		}
		else if (xmTileMin.z < xmPlayerMax.z)
		{
			xmPlayerPos.z = xmPlayerPos.z - (xmTileMax.z - xmPlayerMin.z);
		}
		/*xmPlayerPos.y = xmPlayerPos.y + (xmTileMax.y - xmPlayerMin.y);*/

		XMFLOAT3 Vel;
		Vel = pPlayer->m_pPhysicCom->GetVel();
		pPlayer->m_pPhysicCom->SetVel(Vel.x, 0.f, Vel.z);
	}
	else
	{
		/*xmPlayerPos.y = xmPlayerPos.y - (xmTileMax.y - xmPlayerMin.y);*/

		XMFLOAT3 Vel;
		Vel = pPlayer->m_pPhysicCom->GetVel();
		pPlayer->m_pPhysicCom->SetVel(Vel.x, 0.f, Vel.z);
	}
	pPlayer->m_tPlayerInfo.xmPosition = XMFLOAT3(xmPlayerPos.x, xmPlayerPos.y, xmPlayerPos.z);
}

void CScene_Fallingmap::DeleteBlock()
{
	if (CServerManager::GetInstance()->Get_Blockid() == -1)
		return;

	switch (CServerManager::GetInstance()->Get_BlockColor())
	{
	case BLOCK_BLUE:
	{
		if (pTile[CServerManager::GetInstance()->Get_Blockid()]->GetChecked())
			return;
		pTile[CServerManager::GetInstance()->Get_Blockid()]->SetMaterialDiffuseRGBA(255, 255, 255, 256);
		pTile[CServerManager::GetInstance()->Get_Blockid()]->SetDelTime(true, true);
		pTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->SetPosition(pTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->GetPosition().x, 15.f - 0.1f, pTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->GetPosition().z);
		pTile[CServerManager::GetInstance()->Get_Blockid()]->SetChecked(true);
	}
	break; 
	case BLOCK_RED:
	{
		if (pRedTile[CServerManager::GetInstance()->Get_Blockid()]->GetChecked())
			return;
		pRedTile[CServerManager::GetInstance()->Get_Blockid()]->SetMaterialDiffuseRGBA(255, 255, 255, 256);
		pRedTile[CServerManager::GetInstance()->Get_Blockid()]->SetDelTime(true, true);
		pRedTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->SetPosition(pRedTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->GetPosition().x, -0.1f, pRedTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->GetPosition().z);
		pRedTile[CServerManager::GetInstance()->Get_Blockid()]->SetChecked(true);

	}
	break;
	case BLOCK_YELLOW:
	{
		if (pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->GetChecked())
			return;
		pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->SetMaterialDiffuseRGBA(255, 255, 255, 256);
		pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->SetDelTime(true, true);
		pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->SetPosition(pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->GetPosition().x, -15.f - 0.1f, pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->m_pTransformCom->GetPosition().z);
		pYellowTile[CServerManager::GetInstance()->Get_Blockid()]->SetChecked(true);
	}
	break;
	default:
		break;
	}

	CServerManager::GetInstance()->BlockInit();
}

void CScene_Fallingmap::PlayerFallingDead()
{
	if (Player != nullptr)
	{
		if (!SendDead) {
			if (Player->GetPosition().y < -20.f)
			{
				packet_playerdead p;
				p.id = Player->Get_ClientIndex();
				p.size = sizeof(packet_playerdead);
				p.type = C2S_PLAYERDEAD;

				CServerManager::GetInstance()->SendData(C2S_PLAYERDEAD, &p);

				Player->SetDeath(true);
				SendDead = true;
			}
		}
	}
}

CGameObject* CScene_Fallingmap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position.x = x;
	temp.position.y = y;
	temp.position.z = z;

	temp.rotation.x = rx;
	temp.rotation.y = ry;
	temp.rotation.z = rz;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_FALLINGMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

CGameObject* CScene_Fallingmap::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position = transform;
	temp.rotation = rotation;
	temp.scale = scale;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_FALLINGMAP, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

HRESULT CScene_Fallingmap::Initialize_Scene(CScene_Boardmap* pSceneBoardmap)
{
	m_pScene_Boardmap = pSceneBoardmap;

	moveDelta = 0.f;

	deleteDelta = 0.f;

	m_SceneId = SCENEID::SCENE_FALLINGMAP;

	CServerManager::GetInstance()->Set_MiniWinner(-1);

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);

	CManagement* pManagement = CManagement::GetInstance();

	CreateObject(L"Layer_BackGround", L"GameObject_Jump", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
	CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_FALLINGMAP, L"Layer_BackGround", L"GameObject_FallingFont", (CGameObject**)&pButtonText[0])))
		return E_FAIL;
	pButtonText[0]->SetText(L"Space", XMFLOAT2(0.93f, 0.935f), XMFLOAT2(0.5f, 0.5f));
	pButtonText[0]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.9f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

	CreateObject(L"Layer_BackGround", L"GameObject_Run", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.08f, 0.1f));
	CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.75f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.1f, 0.1f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_FALLINGMAP, L"Layer_BackGround", L"GameObject_FallingFont", (CGameObject**)&pButtonText[1])))
		return E_FAIL;
	pButtonText[1]->SetText(L"Arrow", XMFLOAT2(0.855f, 0.935f), XMFLOAT2(0.5f, 0.5f));
	pButtonText[1]->SetTextColor(XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	CreateObject(L"Layer_BackGround", L"GameObject_White", XMFLOAT3(0.75f, -0.9f, 0.f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.04f, 0.1f));

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Third");

	CreateObject(L"Layer_BackGround", L"GameObject_SkyBox", 29.7602, 15.1374, -32.9217, -90.f, 0.f, 0.f);

	int i = 0;
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < 12; ++x) {
			pTile[i] = dynamic_cast<CTileLarge*>(CreateObject(L"Layer_BackGround", L"GameObject_TileLarge", XMFLOAT3((float)x * 3 + 1.5f, 15.0f, 33.f - ((float)y * 3)), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.5, 1.0, 0.5)));
			i++;
		}
	}

	i = 0;
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < 12; ++x) {
			pRedTile[i] = dynamic_cast<CTileLarge*>(CreateObject(L"Layer_BackGround", L"GameObject_TileLarge", XMFLOAT3((float)x * 3 + 1.5f, 0.0f,33.f - ((float)y * 3)), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.5, 1.0, 0.5)));
			pRedTile[i]->SetMaterialDiffuseRGBA(244, 67, 54, 256);
			i++;
		}
	}

	i = 0;
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < 12; ++x) {
			pYellowTile[i] = dynamic_cast<CTileLarge*>(CreateObject(L"Layer_BackGround", L"GameObject_TileLarge", XMFLOAT3((float)x * 3 + 1.5f, -15.0f,33.f - ((float)y * 3)), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.5, 1.0, 0.5)));
			pYellowTile[i]->SetMaterialDiffuseRGBA(255, 255, 59, 256);
			i++;
		}
	}

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_FALLINGMAP, L"Layer_BackGround", L"GameObject_FallingFont", (CGameObject**)&m_pEndText)))
		return E_FAIL;
	m_pEndText->SetText(L"", XMFLOAT2(0.45f, 0.45f), XMFLOAT2(2.f, 2.f));
	m_pEndText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_FALLINGMAP, L"Layer_BackGround", L"GameObject_FallingFont", (CGameObject**)&m_pStatTimeText)))
		return E_FAIL;
	m_pStatTimeText->SetText(L"", XMFLOAT2(0.5f, 0.38f), XMFLOAT2(2.f, 2.f));
	m_pStatTimeText->SetTextColor(XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_pStatTimeText->SetHide(true);

	::Player->m_tPlayerInfo.xmPosition = XMFLOAT3(11.8047, 16.7839, 12.2751);

	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	loadcomplete = false;

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Fallingmap", 1.f, 0, true); //배경음악 재생

	ShowCursor(false);

	return NOERROR;
}

short CScene_Fallingmap::KeyEvent(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
		return 0;

	if (winner > 0) //승자 발생, 게임 종료 시 
		return 0;

	if (!gameStarted && SERVERCONNECT == true)
		return 0;
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();

	static bool isClip = true;


	if (GetAsyncKeyState(VK_MENU) & 0x8000)
	{
		if (isClip)
			isClip = false;
		else
			isClip = true;
	}

	if (isClip)
	{
		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos, centerPt;
		centerPt.x = WINDOWX / 2;
		centerPt.y = WINDOWY / 2;

		GetCursorPos(&ptCursorPos);
		cxDelta = (float)(ptCursorPos.x - centerPt.x) / 3.0f;
		cyDelta = (float)(ptCursorPos.y - centerPt.y) / 3.0f;
		SetCursorPos(centerPt.x, centerPt.y);
		if (cxDelta || cyDelta)
		{
			Player->Rotate(cyDelta, cxDelta, 0.0f);
			/*
			if (pKeyMgr->KeyPressing(KEY_RBUTTON))
				Rotate(cyDelta, cxDelta, 0.0f);
			else
				Rotate(cyDelta, 0.0f, -cxDelta);
			*/
		}
	}

	if (!Player->GetDeath()) //죽지 않앗을 때
	{
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
		Player->SetPlayerForceX(0.f);
		Player->SetPlayerForceZ(0.f);
		float forceAmount = 300.f;
		if (pKeyMgr->KeyPressing(KEY_W))
		{
			Player->GivePlayerForce(0.f, 0.f, forceAmount);
			//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmLook, PLAYERSPEED * TimeDelta);
			isMoving = true;
		}
		if (pKeyMgr->KeyPressing(KEY_S))
		{
			Player->GivePlayerForce(0.f, 0.f, -forceAmount);
			//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmLook, -PLAYERSPEED * TimeDelta);
			isMoving = true;
		}
		if (pKeyMgr->KeyPressing(KEY_A))
		{
			Player->GivePlayerForce(-forceAmount, 0.f, 0.f);
			//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmRight, -PLAYERSPEED * TimeDelta);
			isMoving = true;
		}
		if (pKeyMgr->KeyPressing(KEY_D))
		{
			Player->GivePlayerForce(forceAmount, 0.f, 0.f);
			//xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmRight, PLAYERSPEED * TimeDelta);
			isMoving = true;
		}
		if (pKeyMgr->KeyPressing(KEY_UP))
		{
			xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmUp, PLAYERSPEED * TimeDelta);
		}
		else if (pKeyMgr->KeyPressing(KEY_DOWN))
		{
			xmf3Shift = Vector3::Add(xmf3Shift, Player->m_tPlayerInfo.xmUp, -PLAYERSPEED * TimeDelta);
		}

		if (pKeyMgr->KeyPressing(KEY_SPACE))
		{
			if (Player->GetState() == OBJECT_STATE::OBJSTATE_GROUND)
			{
				Player->SetAnimation("jump");
				Player->SetState(OBJECT_STATE::OBJSTATE_FLY);
				Player->SetPlayerForceY(forceAmount * 5);
				isMoving = true;
			}
		}

		Player->xmf3Shift = xmf3Shift;

		//키입력이 없으면서 진행중인 애니메이션이 idle이 아니면 Player의 m_IsMoving을 false로 바꾸도록하는 패킷 전송
	}
	if (pKeyMgr->KeyPressing(KEY_F1))
	{
		if (SERVERCONNECT)
		{
			if (!Player->GetDeath())
			{
				Player->SetDeath(true);

				packet_scenechange p;
				p.id = Player->Get_ClientIndex();
				p.size = sizeof(packet_scenechange);
				p.type = C2S_SCENECHANGE;
				p.x = Player->Get_CurrentPlatformPos().x;
				p.y = Player->Get_CurrentPlatformPos().y;
				p.z = Player->Get_CurrentPlatformPos().z;
				p.lookx = Player->Get_BeforeLookVector().x;
				p.looky = Player->Get_BeforeLookVector().y;
				p.lookz = Player->Get_BeforeLookVector().z;
				p.map = S_BOARDMAP;
				CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);
			}
		}
		else
		{
			Scene_Change(SCENE_BOARDMAP);
		}
	}


	return NOERROR;
}

short CScene_Fallingmap::Update_Scene(double TimeDelta)
{
	if (CServerManager::GetInstance()->Get_Disconnect()) // 플레이어 중 한명이 나가면 게임 중단
	{
		if (m_pLoading == nullptr)
		{
			m_pLoading = CLoading::Create(SCENE_WAITINGROOM);
		}
		else
		{
			if (m_pLoading->Get_Finish())
			{
				CManagement* pManagement = CManagement::GetInstance();

				pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

				if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_FALLINGMAP)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_BOARDMAP)))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
					return -1;
			}
		}
		return 0;
	}

	if (!m_bStartAsk)
	{
		cameraAnimation(TimeDelta);
		return 0;
	}

	if (!loadcomplete) {

		m_iStartTime = CServerManager::GetInstance()->Get_StartTime();

		if (m_iStartTime > 0)
			m_pStatTimeText->SetTextString(to_wstring(m_iStartTime));
		else {
			packet_loadingsuccess p2;
			p2.size = sizeof(packet_loadingsuccess);
			p2.type = C2S_MINIGAMELOADING;
			p2.scene = S_FALLINGMAP;
			CServerManager::GetInstance()->SendData(C2S_MINIGAMELOADING, &p2);
			CServerManager::GetInstance()->Set_StartTime(3);

			loadcomplete = true;

			m_pStatTimeText->SetTextString(L"");
		}
	}

	//종료 3초
	if (m_bEndAsk)
	{
		int m_iEndTime = CServerManager::GetInstance()->Get_StartTime();

		if (m_iEndTime == 0) {

			packet_scenechange p;
			p.id = Player->Get_ClientIndex();
			p.size = sizeof(packet_scenechange);
			p.type = C2S_SCENECHANGE;
			p.x = Player->Get_CurrentPlatformPos().x;
			p.y = Player->Get_CurrentPlatformPos().y;
			p.z = Player->Get_CurrentPlatformPos().z;
			p.lookx = Player->Get_BeforeLookVector().x;
			p.looky = Player->Get_BeforeLookVector().y;
			p.lookz = Player->Get_BeforeLookVector().z;
			p.map = S_BOARDMAP;
			CServerManager::GetInstance()->SendData(C2S_SCENECHANGE, &p);

			CServerManager::GetInstance()->Set_StartTime(3);
		}
	}

	if (CServerManager::GetInstance()->Get_BoardmapReady() == 3)
	{
		//미니게임 승자 ui 삭제

		CServerManager::GetInstance()->Set_Boardmapinit();
		Scene_Change(SCENE_BOARDMAP);
		return 0;
	}

	if (CServerManager::GetInstance()->Get_MinigameReady())
	{
		gameStarted = true; //게임 동시시작 플래그 
		CServerManager::GetInstance()->Set_MinigameReady(false);
	}

	if (!gameStarted && SERVERCONNECT == true)
		return 0;//모든 플레이어가 준비 됐으면 시작

	if (SERVERCONNECT) {

		if (CServerManager::GetInstance()->Get_MiniWinner() >= 0)
		{
			//게임 끝, 승자 나옴
			Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

			if (!m_bEndAsk) //종료 3초 요청
			{
				m_iEndTime = 3;
				m_bEndAsk = true;

				packet_starttimeask ps;
				ps.size = sizeof(packet_starttimeask);
				ps.type = C2S_STARTTIMER;
				ps.id = Player->Get_ClientIndex();

				CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
			}
			

			//미니게임 승자 ui 띄우기
			winner = CServerManager::GetInstance()->Get_MiniWinner();
			for (int i = 0; i < 3; ++i)
			{
				if (Players[i]->Get_ClientIndex() == winner)
				{
					wstring w = L"";
					string name = Players[i]->Get_Name();

					w.assign(name.begin(), name.end());
					w += L" Win";
					m_pEndText->SetTextString(w);
					Players[i]->SetWinner(true);
				}
				else
				{
					Players[i]->SetWinner(false);
				}
			}

			if (Player->Get_ClientIndex() == winner)
			{
				CSoundManager::GetInstance()->Play_Sound(L"Mingame_Win");
			}
			else
			{
				CSoundManager::GetInstance()->Play_Sound(L"Minigame_Lose");
			}

			CServerManager::GetInstance()->Set_MiniWinner(-1);
			return 0;

		}

		PlayerFallingDead();

		deleteDelta += TimeDelta;

		DeleteBlock();

		if (Player->GetDeath()) //플레이어가 이미 밑으로 쭉 떨어짐
		{
			return 0;
		}

		if (winner >= 0) //승자 등장
			return 0;

	}

	Player->xmf3Shift = Player->m_pPhysicCom->UpdatePhysics(Player->m_tPlayerInfo.xmPosition, Player->m_tPlayerInfo.xmRight,
		Player->m_tPlayerInfo.xmUp, Player->m_tPlayerInfo.xmLook, Player->GetPlayerForce(), Player->GetState(), TimeDelta);

	if(Player->GetPlayerForce().y > 0.f)
		Player->GivePlayerForce(0.f, -100.f, 0.f);

	IntersectTileTest(TimeDelta);

	if (SERVERCONNECT) {

		int deathplayer = CServerManager::GetInstance()->Get_DeathPlayer();

		moveDelta += TimeDelta;

		if (moveDelta > MOVEPACKETDELTA)
		{
			packet_move p;
			ZeroMemory(&p, sizeof(p));
			p.type = C2S_MOVE;
			p.size = sizeof(packet_move);
			p.id = Player->Get_ClientIndex();
			p.TimeDelta = TimeDelta;

			XMFLOAT3 look = Vector3::Normalize(Player->GetLookVector());

			p.mx = Player->m_tPlayerInfo.xmPosition.x;
			p.my = Player->m_tPlayerInfo.xmPosition.y;
			p.mz = Player->m_tPlayerInfo.xmPosition.z;

			p.lookx = look.x;
			p.looky = look.y;
			p.lookz = look.z;

			CServerManager::GetInstance()->SendData(C2S_MOVE, &p);
			moveDelta = 0.f;

			isMoving = false;
		}
	}

	return 0;
}

short CScene_Fallingmap::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_Fallingmap::Render_Scene()
{
	return NOERROR;
}

HRESULT CScene_Fallingmap::Scene_Change(SCENEID Scene)
{
	CManagement* pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return -1;

	if (!SERVERCONNECT) {
		::Player->m_tPlayerInfo.xmPosition = ::Player->Get_CurrentPlatformPos();
		::Player->m_tPlayerInfo.xmLook = ::Player->Get_BeforeLookVector();
		::Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);
		::Player->m_AnimState = ANIM_IDLE;
		::Player->SetAnimation("idle1");
	}
	else
	{
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i]->GetPlayerOrder() == CServerManager::GetInstance()->Get_CurrentOrder())
			{
				CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
				dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);
			}

			//Players[i]->m_AnimState = ANIM_IDLE;
			Players[i]->SetAnimation("run");
			Players[i]->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

		}
	}
	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Boardmap", 1.f, 0, true); //배경음악 재생
	ShowCursor(true);

	pManagement->Set_CurrentSceneId(Scene);

	if (FAILED(pManagement->Initialize_Current_Scene(m_pScene_Boardmap)))
		return -1;

	if (FAILED(pManagement->Clear_Scene(SCENE_FALLINGMAP)))
		return -1;
}

short CScene_Fallingmap::cameraAnimation(double TimeDelta)
{
	m_fCameratime += TimeDelta;

	CCamera* pCamera = CCameraManager::GetInstance()->Find_Camera("Camera_Third");
	CCamera_Third* pCameraThird = dynamic_cast<CCamera_Third*>(pCamera);
	pCameraThird->GetPlayer()->Rotate(0.f, 21 * TimeDelta, 0.f);
	pCameraThird->SetLookAt(XMFLOAT3(20.f, 10.f, 30.f - m_fCameratime * 2.f));
	pCameraThird->SetOffset(XMFLOAT3(50.f, 10.f, 25.f));

	if (m_fCameratime > 10.f)
	{
		pCameraThird->SetOffset(XMFLOAT3(0.0f, 7.2f, -12.0f));
		m_bStartAsk = true;
		m_pStatTimeText->SetHide(false);
		if (SERVERCONNECT) {
			//게임 시작 완료 패킷 서버에 보내기
			packet_starttimeask ps;
			ps.size = sizeof(packet_starttimeask);
			ps.type = C2S_STARTTIMER;
			ps.id = Player->Get_ClientIndex();

			CServerManager::GetInstance()->SendData(C2S_STARTTIMER, &ps);
		}
	}

	return 0;
}

CScene_Fallingmap* CScene_Fallingmap::Create(CScene_Boardmap* pSceneBoardmap)
{
	CScene_Fallingmap* pInstance = new CScene_Fallingmap();

	if (pInstance->Initialize_Scene(pSceneBoardmap))
	{
		MSG_BOX("CScene_Fallingmap Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_Fallingmap::Release()
{
	delete this;

	return NOERROR;
}
