﻿#include "Loading.h"
#include "Management.h"
#include "Shader.h"
#include "Transform.h"
#include "Mesh.h"
#include "DeviceManager.h"
#include "Camera_Basic.h"
#include "Texture.h"
#include "Font.h"
#include "SphereCollider.h"
#include "BoundingBox.h"
#include "TeddyBear.h"
#include "SkyBox.h"
#include "StaticObject.h"
#include "TileLarge.h"
#include "Platform.h"
#include "BlackBackground.h"
#include "BillBoardObject.h"
#include "CoinEffect.h"
#include "Dice.h"
#include "LoginImage.h"
#include "Chicken.h"
#include "Bird.h"
#include "Ghost.h"
#include "GrassPlane.h"
#include "UI.h"
#include "Hexagon.h"
#include "SantaTeddyBear.h"
#include "SantaHat.h"
#include "Balloon.h"
#include "Button.h"
#include "Store.h"

extern bool G_SC;

bool CLoading::m_bFinish = false;

CLoading::CLoading()
{
    ZeroMemory(m_szLoading, sizeof(wchar_t) * 128);
    m_bFinish = false;
}

CLoading::~CLoading()
{
    WaitForSingleObject(m_hThread, INFINITE);
    CloseHandle(m_hThread);
    DeleteCriticalSection(&m_Crt);
}

_uint CLoading::Thread_Main(void* pArg)
{
    CLoading* pLoading = (CLoading*)pArg;

    _uint iFlag = 0;

    EnterCriticalSection(pLoading->Get_Crt());

    switch (pLoading->Get_LoadingID())
    {
    case SCENE_LOGIN:
        iFlag = pLoading->Loading_ForLogin();
        break;
    case SCENE_WAITINGROOM:
        iFlag = pLoading->Loading_ForWaitingRoom();
        break;
    case SCENE_BOARDMAP:
        iFlag = pLoading->Loading_ForBoardmap();
        break;
    case SCENE_FALLINGMAP:
        iFlag = pLoading->Loading_ForFallingmap();
        break;
    case SCENE_GGOGGOMAP:
        iFlag = pLoading->Loading_ForGgoggomap();
        break;
    case SCENE_BULLFIGHTMAP:
        iFlag = pLoading->Loading_ForBullfightmap();
        break;
    case SCENE_ONEMINDMAP:
        iFlag = pLoading->Loading_ForOneMindMap();
        break;
    case SCENE_PRIZE:
        iFlag = pLoading->Loading_ForPrize();
        break;
    }

    LeaveCriticalSection(pLoading->Get_Crt());

    m_bFinish = true;

    _endthreadex(0);

    return iFlag;
}

HRESULT CLoading::Initialize_Loading(SCENEID eLoadingId)
{
 
    InitializeCriticalSection(&m_Crt);

    m_hThread = (HANDLE)_beginthreadex(NULL, 0, Thread_Main, this, 0, NULL);

    m_LoadingID = eLoadingId;

    return S_OK;
}

_uint CLoading::Loading_ForPrize()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);

    CManagement* pManagement = CManagement::GetInstance();

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow01_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow02_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow02"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow03_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow03"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow04_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow04"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow05_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow05"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow06_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow06"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow07_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow07"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow08_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow08"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow09_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow09"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow10_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow10"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow11_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow11"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow12_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow12"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow13_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow13"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow14_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow14"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow16_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow16"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_ChairsRow17_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/ChairsRow17"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Cups_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Cups"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Doors_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Doors"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Floor_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Floor"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_HDScreen_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/HDScreen"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Glasses_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Glasses"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Loudspeakers_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Loudspeakers"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Popcorn_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Popcorn"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_PopcornBucket_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/PopcornBucket"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Rail_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Rail"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_SpotLight_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/SpotLight"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Tickets_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Tickets"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Walls_Mesh", CMesh::Create("../Data/Resources/Objects/Prize/Walls"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Snowman_Mesh2", CMesh::Create("../Data/Resources/OneMind/Snowman"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SantaTeddyBearE", CSantaTeddyBear::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_PrizeUnityPlane_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Plane"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_PrizeLogo", CTexture::Create(L"../Data/Resources/UI/Robby/Logo.png", L"RobbyLogo", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_OneMind_etcc", CTexture::Create(L"../Data/Resources/OneMind/OneMind_etc.png", L"OneMind_etcc", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Chair", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/ChairTexture.png", L"ChairTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Cups", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/SodaTexture.png", L"CupTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Doors", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/DoorTexture.png", L"DoorTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Floor", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/Floor_Fabric_Texture.png", L"Floor_Fabric_Texture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Glasses", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/GlassesTexture.png", L"Floor_Fabric_Texture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Loudspeakers", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/Loudspeaker.png", L"LoudspeakerTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Popcorn", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/PopcornSingle.png", L"PopcornSingleTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_PopcornBucket", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/PopcornTexture.png", L"PopcornTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Tickets", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/Ticket.png", L"TicketsTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Walls", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/Carpet_Black.png", L"WallsTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_PRIZE, L"Component_Texture_Counts", CTexture::Create(L"../Data/Resources/Objects/Prize/Texture/Count/count_%d.png", L"Counts", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 25))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_DiceNum", CTexture::Create(L"../Data/Resources/DiceNum/BlueNumber0%d.png", L"DiceNum", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 6))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_VS", CTexture::Create(L"../Data/Resources/UI/VS.png", L"VS", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_KSJ", CTexture::Create(L"../Data/Resources/UI/Prize/ksj.png", L"KSJ", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_LSJ", CTexture::Create(L"../Data/Resources/UI/Prize/lsj.png", L"LSJ", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_KJU", CTexture::Create(L"../Data/Resources/UI/Prize/kju.png", L"KSJ", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_Thanks", CTexture::Create(L"../Data/Resources/UI/Prize/Thanks.png", L"KSJ", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    //if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Backroom", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Walls", L"Component_Backroom_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
    //    return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_snowman2", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_OneMind_etcc", L"Component_Snowman_Mesh2", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow01", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow01_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow02", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow02_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow03", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow03_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow04", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow04_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow05", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow05_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow06", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow06_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow07", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow07_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow08", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow08_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow09", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow09_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow10", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow10_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow11", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow11_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow12", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow12_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow13", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow13_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow14", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow14_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow16", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow16_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ChairsRow17", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Chair", L"Component_ChairsRow17_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Cups", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Cups", L"Component_Cups_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Doors", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Doors", L"Component_Doors_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Floor", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Floor", L"Component_Floor_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_HDScreen", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Counts", L"Component_HDScreen_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_HDScreenBack", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"", L"Component_HDScreen_Mesh", L"Component_Shader_FBXOBJ", XMINT3(100, 100, 100)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Glasses", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Glasses", L"Component_Glasses_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Loudspeakers", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Loudspeakers", L"Component_Loudspeakers_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Popcorn", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Popcorn", L"Component_Popcorn_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_PopcornBucket", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_PopcornBucket", L"Component_PopcornBucket_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Rail", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"", L"Component_Rail_Mesh", L"Component_Shader_FBXOBJ", XMINT3(0, 0, 0)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SpotLight", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Walls", L"Component_SpotLight_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Tickets", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"Component_Texture_Tickets", L"Component_Tickets_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Walls", CStaticObject::Create(SCENEID::SCENE_PRIZE, L"", L"Component_Walls_Mesh", L"Component_Shader_FBXOBJ", XMINT3(0, 0, 0)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WinScreen", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_PlayerIcon", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_PrizeLogo", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_PrizeLogo", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_DiceScreen", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_DiceNum", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_VS", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_VS", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_KSJ", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_KSJ", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_LSJ", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_LSJ", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_KJU", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_KJU", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Thanks", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_Thanks", L"Component_PrizeUnityPlane_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_PrizeFont", CFont::Create())))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);

    return 0;
}

_uint CLoading::Loading_ForBullfightmap()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);

    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_BULLFIGHTMAP);

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Caeser_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_Caesar"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_ClothUpper_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Cloth_Upper.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand02_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand02.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand02Piece_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand02_Piece.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand03_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand03.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand03Piece_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand03_Piece.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand04_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand04.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_BasicStand04004_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_BasicStand04.004"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock002_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlock02_low.002"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock003_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlock02_low.003"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock004_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlock02_low.004"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock005_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlock02_low.005"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock007_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlock02_low.007"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock008_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlock02_low.008"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_SunBlock001_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_SunBlocks.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Plane.002_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Plane.002"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_UnityPlane2_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Plane"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Caeser_Stairs_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_Caesar_Stairs"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_MainGate.001_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/ColosseumMainGate.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_MainGate.002_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/ColosseumMainGate.002"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_MainGate.003_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/ColosseumMainGate.003"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_MainGateStand001_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/ColosseumMainGate_Stand.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_MainGateWood001_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Maingate_Wood.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_MainGateWood002_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Maingate_Wood.002"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Flag_01_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_Flag_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_GoldenLionhead_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/GoldenLionhead"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Column_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/Colosseum_Column_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_CaeserPilar_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/CaesarPilar_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Accessories_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/CaesarPilar_Accessories"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_ColosseumPiece_01_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/ColosseumPiece_01.001"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Potion_Mesh", CMesh::Create("../Data/Resources/Objects/BullFight2/mf_potion_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_MagicCircle", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/magic_circle.png", L"MagicCircle", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Accessories", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Accessories_AlbedoTransparency.png", L"Accessories", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Collumns_01", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Collumns_01_AlbedoTransparency.png", L"Collumns_01", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces02", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Pieces02_AlbedoTransparency.png", L"Arena_Pieces02", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Pieces03_AlbedoTransparency.png", L"Arena_Pieces03", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces04", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Pieces04_AlbedoTransparency.png", L"Arena_Pieces04", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Pieces05_AlbedoTransparency.png", L"Arena_Pieces05", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces06", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Pieces06_AlbedoTransparency.png", L"Arena_Pieces06", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Caesar_Cabin", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Caesar_Cabin_AlbedoTransparency.png", L"Caesar_Cabin", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_CauldronTorch", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/CauldronTorch_AlbedoTransparency.png", L"CauldronTorch", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_PropAtlas01", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/PropAtlas01_AlbedoTransparency.png", L"PropAtlas01", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock01", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/SunBlock01_AlbedoTransparency.png", L"SunBlock01", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/SunBlock02_AlbedoTransparency.png", L"SunBlock02", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02_Blue", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/SunBlock02_Blue_AlbedoTransparency.png", L"SunBlock02_Blue", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Ground", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Arena_Ground_AlbedoTransparency.png", L"Arena_Ground", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BULLFIGHTMAP, L"Component_Texture_Potion", CTexture::Create(L"../Data/Resources/Objects/BullFight2/Textures/Potion.png", L"PotionTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Caeser", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", L"Component_Caeser_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ClothUpper", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Accessories", L"Component_ClothUpper_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_BasicStand_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand02", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_BasicStand02_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand02Piece", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", L"Component_BasicStand02Piece_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand03", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_BasicStand03_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand03Piece", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", L"Component_BasicStand03Piece_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand04", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_BasicStand04_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BasicStand04004", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_BasicStand04004_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock002", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02", L"Component_SunBlock002_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock003", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02_Blue", L"Component_SunBlock003_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock004", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02_Blue", L"Component_SunBlock004_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock005", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02", L"Component_SunBlock005_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock007", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02", L"Component_SunBlock007_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock008", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock02", L"Component_SunBlock008_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunBlock001", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_SunBlock01", L"Component_SunBlock001_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_UnityPlane2", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Ground", L"Component_UnityPlane2_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MagicCircle", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_MagicCircle", L"Component_UnityPlane2_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 0, 0)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Caeser_Stairs", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_Caeser_Stairs_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniGate.001", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", L"Component_MainGate.001_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniGate.002", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces06", L"Component_MainGate.002_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniGate.003", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", L"Component_MainGate.003_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniGateStand001", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces05", L"Component_MainGateStand001_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniGateWood001", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces04", L"Component_MainGateWood001_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniGateWood002", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces04", L"Component_MainGateWood002_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Flag_01", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces04", L"Component_Flag_01_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GoldenLionhead", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Accessories", L"Component_GoldenLionhead_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Column", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Collumns_01", L"Component_Column_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_CaeserPilar", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Collumns_01", L"Component_CaeserPilar_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Accessories", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Accessories", L"Component_Accessories_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Plane.002", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces04", L"Component_Plane.002_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ColosseumPiece_01", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Arena_Pieces03", L"Component_ColosseumPiece_01_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Item", CStaticObject::Create(SCENEID::SCENE_BULLFIGHTMAP, L"Component_Texture_Potion", L"Component_Potion_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BullFont", CFont::Create())))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);



    return 0;
}

unsigned int CLoading::Loading_ForOneMindMap()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);


    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_ONEMINDMAP);


    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_SnowHex_Mesh", CMesh::Create("../Data/Resources/OneMind/SnowHex"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_IceLand0_Mesh", CMesh::Create("../Data/Resources/OneMind/IceLand0"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_IceLand1_Mesh", CMesh::Create("../Data/Resources/OneMind/IceLand1"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Igloo_Mesh", CMesh::Create("../Data/Resources/OneMind/Igloo"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Snowman_Mesh", CMesh::Create("../Data/Resources/OneMind/Snowman"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Snowball0_Mesh", CMesh::Create("../Data/Resources/OneMind/Snowball0"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Snowball1_Mesh", CMesh::Create("../Data/Resources/OneMind/Snowball1"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_TruncatedTree_Mesh", CMesh::Create("../Data/Resources/OneMind/TruncatedTree"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_TruncatedTree1_Mesh", CMesh::Create("../Data/Resources/OneMind/TruncatedTree1"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_SnowTree_Mesh", CMesh::Create("../Data/Resources/OneMind/SnowTree"))))
        return E_FAIL;



    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_SnowHex", CTexture::Create(L"../Data/Resources/OneMind/SnowHex.PNG", L"SnowHex", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_IceHex0", CTexture::Create(L"../Data/Resources/OneMind/IceHex0.PNG", L"IceHex0", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_IceHex1", CTexture::Create(L"../Data/Resources/OneMind/IceHex1.PNG", L"IceHex1", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_IceHex2", CTexture::Create(L"../Data/Resources/OneMind/IceHex2.PNG", L"IceHex2", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_WaterHex", CTexture::Create(L"../Data/Resources/OneMind/WaterHex.PNG", L"WaterHex", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", CTexture::Create(L"../Data/Resources/OneMind/OneMind_etc.png", L"OneMind_etc", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc1", CTexture::Create(L"../Data/Resources/OneMind/OneMind_etc1.png", L"OneMind_etc1", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_Telescope", CTexture::Create(L"../Data/Resources/UI/telescope.png", L"Telescope", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_Light", CTexture::Create(L"../Data/Resources/UI/light.png", L"Light", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_Stopwatch2", CTexture::Create(L"../Data/Resources/UI/Stopwatch2.png", L"Stopwatch2", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ONEMINDMAP, L"Component_Texture_Battery", CTexture::Create(L"../Data/Resources/UI/battery.png", L"Battery", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_snowHex", CHexagon::Create(L"Component_Texture_SnowHex"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_iceHex0", CHexagon::Create(L"Component_Texture_IceHex0"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_iceHex1", CHexagon::Create(L"Component_Texture_IceHex1"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_iceHex2", CHexagon::Create(L"Component_Texture_IceHex2"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_waterHex", CHexagon::Create(L"Component_Texture_WaterHex"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_iceland0", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_IceLand0_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_iceland1", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_IceLand1_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_igloo", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_Igloo_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_snowman", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_Snowman_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_snowball0", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_Snowball0_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_snowball1", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_Snowball1_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_truncatedTree", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_TruncatedTree_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_truncatedTree1", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc", L"Component_TruncatedTree1_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_snowTree", CStaticObject::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_OneMind_etc1", L"Component_SnowTree_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SantaTeddyBear", CSantaTeddyBear::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_OneFont", CFont::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Telescope", CUI::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_Telescope"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Light", CUI::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_Light"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_StopWatch2", CUI::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_Stopwatch2"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Battery", CUI::Create(SCENEID::SCENE_ONEMINDMAP, L"Component_Texture_Battery"))))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);

    return 0;
}

_uint CLoading::Loading_ForGgoggomap()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);

    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_GGOGGOMAP);

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Barn_Mesh", CMesh::Create("../Data/Resources/Objects/barn2"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_GroundFlat_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Generic_Ground_Flat_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_DirtSkirt_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Env_Dirt_Skirt_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Fence_Mesh", CMesh::Create("../Data/Resources/Objects/Fence"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Fence2_Mesh", CMesh::Create("../Data/Resources/Objects/Fence2"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_DirtRow_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Env_Dirt_Rows_End_Top_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Well_Mesh", CMesh::Create("../Data/Resources/Objects/Well"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_VegaRow02_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Env_Vege_Rows_02"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_VegaRow03_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Env_Vege_Rows_03"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_TreeApple_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Env_Tree_Apple_Grown_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Silo_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Bld_Silo_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_PlantCorn_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Prop_Plant_Corn_01_L"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_BaleSquare_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Prop_Hay_Bale_Square_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Windmill_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Prop_Windmill_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Ground_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Generic_Ground_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_PickUpCar_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Veh_Pickup_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_SunFlower_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Prop_Sunflower_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Flower_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Env_Flowers_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_WaterTower_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Bld_WaterTower_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_WindmillBlade_Mesh", CMesh::Create("../Data/Resources/Objects/SM_Prop_Windmill_Blades_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Texture_Chicken", CTexture::Create(L"../Data/Resources/Chicken/Chicken.png", L"Chicken", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", CTexture::Create(L"../Data/Resources/Objects/PolygonFarm.png", L"PolygonFarm", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm3", CTexture::Create(L"../Data/Resources/Objects/PolygonFarm_Texture_03_A.png", L"PolygonFarm", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Texture_GGoGGo", CTexture::Create(L"../Data/Resources/UI/GGoGGo.png", L"GGoGGo", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Texture_StopWatch", CTexture::Create(L"../Data/Resources/UI/Stopwatch.png", L"StopWatch", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_Texture_Num", CTexture::Create(L"../Data/Resources/UI/plusOne_3.png", L"Num", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_GGOGGOMAP, L"Component_SphereCollider", CSphereCollider::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Well", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"", L"Component_Well_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Barn", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_Barn_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GroundFlat", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_GroundFlat_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_DirtSkirt", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_DirtSkirt_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_VegaRow02", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_VegaRow02_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_VegaRow03", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_VegaRow03_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TreeApple", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_TreeApple_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Silo", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_Silo_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_PlantCorn", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_PlantCorn_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BaleSquare", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_BaleSquare_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Windmill", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_Windmill_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Ground", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm3", L"Component_Ground_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_PickUpCar", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_PickUpCar_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SunFlower", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_SunFlower_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Flower", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_Flower_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WaterTower", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_WaterTower_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WindmillBlade", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_PolygonFarm", L"Component_WindmillBlade_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(265, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Fence", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"", L"Component_Fence_Mesh", L"Component_Shader_FBXOBJ", XMINT3(27, 20, 10)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Fence2", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"", L"Component_Fence2_Mesh", L"Component_Shader_FBXOBJ", XMINT3(27, 20, 10)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Grass1", CStaticObject::Create(SCENEID::SCENE_GGOGGOMAP, L"", L"Component_Grass1_Mesh", L"Component_Shader_FBXOBJ", XMINT3(27, 20, 10)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Chicken", CChicken::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GGoGGoFont", CFont::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GGoGGoIcon", CUI::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_GGoGGo"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_StopWatch", CUI::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_StopWatch"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GGoGGoPlus", CUI::Create(SCENEID::SCENE_GGOGGOMAP, L"Component_Texture_Num"))))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);



    return 0;
}

_uint CLoading::Loading_ForFallingmap()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);

    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_FALLINGMAP);

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_FallingFont", CFont::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TileLarge", CTileLarge::Create())))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);



    return 0;
}

_uint CLoading::Loading_ForBoardmap()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);


    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_BOARDMAP);

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_SantaHat_Mesh", CMesh::Create("../Data/Resources/SantaHat/SantaHat"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_SkySphere_Mesh", CMesh::Create("../Data/Resources/SkyBox/SkySphere"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Smoke_Mesh", CMesh::Create("../Data/Resources/Particle/sphere"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_House01_Mesh", CMesh::Create("../Data/Resources/Objects/House"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Wall_Mesh", CMesh::Create("../Data/Resources/Objects/wall"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_WallCorner_Mesh", CMesh::Create("../Data/Resources/Objects/wallCorner"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_WallCornerHalf_Mesh", CMesh::Create("../Data/Resources/Objects/wallCornerHalf"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_WallHalf_Mesh", CMesh::Create("../Data/Resources/Objects/wallHalf"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerTopCorner_Mesh", CMesh::Create("../Data/Resources/Objects/towerTopCorner"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_WallCornerHalfTower_Mesh", CMesh::Create("../Data/Resources/Objects/wallCornerHalfTower"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquareMidWindow_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquareMidWindows"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquare_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquare"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquareTopRoofHigh_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquareTopRoofHigh"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquareArch_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquareArch"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquareTop_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquareTop"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquareRoof_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquareRoof"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerBase_Mesh", CMesh::Create("../Data/Resources/Objects/towerBase"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerBalcony_Mesh", CMesh::Create("../Data/Resources/Objects/towerBalcony"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerTopRoof_Mesh", CMesh::Create("../Data/Resources/Objects/towerTopRoof"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_TowerSquareMidOpen_Mesh", CMesh::Create("../Data/Resources/Objects/towerSquareMidOpen"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Platform_Mesh", CMesh::Create("../Data/Resources/Objects/Platform"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Coin_Mesh", CMesh::Create("../Data/Resources/Coin/Coin"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Dice_Mesh", CMesh::Create("../Data/Resources/Dice/dice"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_UnityPlane_Mesh", CMesh::Create("../Data/Resources/Objects/UnityPlane"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Arrow_Mesh", CMesh::Create("../Data/Resources/Arrow/Arrow"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_HotAirBalloon_Mesh", CMesh::Create("../Data/Resources/HotAirBalloon/HotAirBalloon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Button_Mesh", CMesh::Create("../Data/Resources/Button/Button"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverStraight01_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Straight_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverStraight02_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Straight_02"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverStraight03_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Straight_03"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverStraight04_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Straight_04"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverStraight05_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Straight_05"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverCorner01_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Corner_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_RiverCorner05_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_River_Corner_05"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_GrassTile_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_Tile_Grass_01"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_WaterTile2_Mesh", CMesh::Create("../Data/Resources/Objects/BoardMap/SF_Env_Tile_Water_02"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Bird", CBird::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Ghost", CGhost::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BoundingBox", CBoundingBox::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SkyBox", CSkyBox::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Wall", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_Wall_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WallCorner", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_WallCorner_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WallCornerHalf", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_WallCornerHalf_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WallHalf", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_WallHalf_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerTopCorner", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerTopCorner_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WallCornerHalfTower", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_WallCornerHalfTower_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquareMidWindow", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerSquareMidWindow_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquare", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerSquare_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquareMidOpen", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerSquareMidOpen_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquareTopRoofHigh", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_towerSquareTopRoofHigh", L"Component_TowerSquareTopRoofHigh_Mesh", L"Component_Shader_FBXTexLight", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquareArch", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerSquareArch_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquareTop", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerSquareTop_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerSquareRoof", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_towerSquareRoof", L"Component_TowerSquareRoof_Mesh", L"Component_Shader_FBXTexLight", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerBase", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerBase_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerBalcony", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_TowerBalcony_Mesh", L"Component_Shader_FBXOBJ", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TowerTopRoof", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_towerTopRoof", L"Component_TowerTopRoof_Mesh", L"Component_Shader_FBXTexLight", XMINT3(186, 178, 151)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_UnityPlane", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_UnityPlane_Mesh", L"Component_Shader_FBXOBJ", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Arrow", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Arrow", L"Component_UnityPlane_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ArrowHead", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_ArrowHead", L"Component_Arrow_Mesh", L"Component_Shader_FBXTex", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverStraight01", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverStraight01_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverStraight02", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverStraight02_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverStraight03", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverStraight03_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverStraight04", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverStraight04_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverStraight05", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverStraight05_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverCorner01", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverCorner01_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RiverCorner05", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_RiverCorner05_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GrassTile", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_GrassTile_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GrassPlane", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"", L"Component_UnityPlane_Mesh", L"Component_Shader_FBXOBJ", XMINT3(96, 190, 4), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WaterTile2", CStaticObject::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Medieval", L"Component_WaterTile2_Mesh", L"Component_Shader_FBXTexLight", XMINT3(256, 256, 256), false))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_SantaHat", CSantaHat::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Platform", CPlatform::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_LastTurn", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_LastTurn"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ClothesButton", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_ClothesButton"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_StarCoin", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_StarCoin"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BlackBackground", CBlackBackground::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_PlayerIcon", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_PlayerIcon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniDiceIcon", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_MiniDiceIcon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_ButtonIcon", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_ButtonIcon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Plus3Icon", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Plus3Icons"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Plus5Icon", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Plus5Icons"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BuyButton", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_BuyButton"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_xButton", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_xButton"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_BillBoard", CBillBoardObject::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_CoinEffect", CCoinEffect::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Dice", CDice::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Smoke", CStaticObject::Create(SCENEID::SCENE_STATIC, L"Component_Texture_Smoke", L"Component_Smoke_Mesh", L"Component_Shader_FBXTexLLight", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Font", CFont::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_White", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_White"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_DiceIcon", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_DiceIcon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MapIcon", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_MapIcon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Run", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_Run"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RoadSign", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_RoadSign"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Jump", CUI::Create(SCENEID::SCENE_STATIC, L"Component_Texture_Jump"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Stamp", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Stamp"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_chatBox_Coin", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_chatBox_Coin"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_chatBox_Purchase", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_chatBox_Purchase"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Inven", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_Inven"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_MiniTuto", CUI::Create(SCENEID::SCENE_BOARDMAP, L"Component_Texture_MiniTuto"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Balloon", CBalloon::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Button", CButton::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Store", CStore::Create(L"Component_Texture_StoreIcon"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Event", CStore::Create(L"Component_Texture_EventMark"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_SantaHat", CTexture::Create(L"../Data/Resources/SantaHat/SantaHat.png", L"SantaHat", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_WhiteTeddyBear", CTexture::Create(L"../Data/Resources/Character/Texture/teddybear4.png", L"teddybear4", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_Ghost", CTexture::Create(L"../Data/Resources/Ghost/Textures/Ghost.png", L"GhostTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Bird", CTexture::Create(L"../Data/Resources/Bird/Textures/Bird.png", L"BirdTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_SkySphere", CTexture::Create(L"../Data/Resources/SkyBox/SkySphere%d.png", L"SkySphere", COMMAND_LOAD, CTexture::TEX_SKYBOX, CTexture::MESH_GENERAL, 5))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_chatBox_Purchase", CTexture::Create(L"../Data/Resources/Store/chatBox_Purchase.png", L"chatBox_Purchase", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_chatBox_Coin", CTexture::Create(L"../Data/Resources/Store/chatBox_Coin.png", L"chatBox_Coin", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_HotAirBalloon", CTexture::Create(L"../Data/Resources/HotAirBalloon/HotAirBalloon.png", L"HotAirBalloon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Button", CTexture::Create(L"../Data/Resources/Button/Button.png", L"Button", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_StoreIcon", CTexture::Create(L"../Data/Resources/Store/StoreIcon.png", L"StoreIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_MiniDiceIcon", CTexture::Create(L"../Data/Resources/Store/MiniDiceIcon%d.png", L"MiniDiceIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Plus5Icons", CTexture::Create(L"../Data/Resources/Store/Plus5Icon%d.png", L"Plus5Icon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Plus3Icons", CTexture::Create(L"../Data/Resources/Store/Plus3Icon%d.png", L"Plus3Icon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_ButtonIcon", CTexture::Create(L"../Data/Resources/Store/ButtonIcon%d.png", L"ButtonIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_BuyButton", CTexture::Create(L"../Data/Resources/Store/BuyButton%d.png", L"BuyButton", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_xButton", CTexture::Create(L"../Data/Resources/Store/xButton%d.png", L"xButton", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_towerSquareTopRoofHigh", CTexture::Create(L"../Data/Resources/Objects/towerSquareTopRoofHigh.png", L"towerSquareTopRoofHigh", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_towerTopRoof", CTexture::Create(L"../Data/Resources/Objects/towerTopRoof.png", L"towerTopRoof", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_towerSquareRoof", CTexture::Create(L"../Data/Resources/Objects/towerSquareRoof.png", L"towerSquareRoof", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Plus5Icon", CTexture::Create(L"../Data/Resources/Store/Plus5Icon.png", L"Plus5Icon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Plus3Icon", CTexture::Create(L"../Data/Resources/Store/Plus3Icon.png", L"Plus3Icon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_ClothesButton", CTexture::Create(L"../Data/Resources/UI/ClothesButton.png", L"ClothesButton", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_StarCoin", CTexture::Create(L"../Data/Resources/UI/StarCoin.png", L"StarCoin", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_LastTurn", CTexture::Create(L"../Data/Resources/UI/lastturn.png", L"LastTurn", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Medieval", CTexture::Create(L"../Data/Resources/Objects/BoardMap/Medieval_Texture.png", L"Medieval", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_PlayerIcon", CTexture::Create(L"../Data/Resources/UI/Player_%d.png", L"PlayerIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 4))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_PlayerDieIcon", CTexture::Create(L"../Data/Resources/UI/DiePlayer_%d.png", L"PlayerDieIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 4))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_Smoke", CTexture::Create(L"../Data/Resources/Particle/PolygonParticles_Smoke_01.png", L"Smoke", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_White", CTexture::Create(L"../Data/Resources/UI/white.png", L"White", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_DiceIcon", CTexture::Create(L"../Data/Resources/UI/diceicon.png", L"DiceIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_RoadSign", CTexture::Create(L"../Data/Resources/UI/signpost.png", L"RoadSign", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_MapIcon", CTexture::Create(L"../Data/Resources/UI/mapicon.png", L"MapIcon", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_Jump", CTexture::Create(L"../Data/Resources/UI/Jump.png", L"Jump", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_Run", CTexture::Create(L"../Data/Resources/UI/run.png", L"Run", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_DiceCount", CTexture::Create(L"../Data/Resources/UI/DiceCount.png", L"DiceCount", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_SmokeSprite", CTexture::Create(L"../Data/Resources/UI/smoke_sprite.png", L"SmokeSprite", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_ShopSprite", CTexture::Create(L"../Data/Resources/UI/speachBubble_store.png", L"ShopSprite", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_MapSprite", CTexture::Create(L"../Data/Resources/UI/speachBubble_map.png", L"MapSprite", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Coin", CTexture::Create(L"../Data/Resources/Coin/Coin.png", L"Coin", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Dice", CTexture::Create(L"../Data/Resources/Dice/dice_%d.png", L"Dice", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_BlackBackground", CTexture::Create(L"../Data/Resources/UI/BlackBackground.png", L"BlackBackground", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_HerePlayer", CTexture::Create(L"../Data/Resources/UI/HerePlayer.png", L"HerePlayer", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Arrow", CTexture::Create(L"../Data/Resources/Objects/arrow.png", L"Arrow", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Confetti", CTexture::Create(L"../Data/Resources/UI/confetti.png", L"Confetti", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Stamp", CTexture::Create(L"../Data/Resources/UI/stamp.png", L"Stamp", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Inven", CTexture::Create(L"../Data/Resources/Store/Inven%d.png", L"Inven", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 3))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_MiniTuto", CTexture::Create(L"../Data/Resources/UI/MiniTuto/MinitutoUI_%d.png", L"MiniTuto", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 4))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_Plus", CTexture::Create(L"../Data/Resources/Store/Plus.png", L"Plus", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_BOARDMAP, L"Component_Texture_EventMark", CTexture::Create(L"../Data/Resources/UI/EventMark.png", L"EventMark", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_ArrowHead", CTexture::Create(L"../Data/Resources/Arrow/Arrow.png", L"Arrow", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;
   
    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);



    return 0;
}

unsigned int CLoading::Loading_ForWaitingRoom()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);

    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_WAITINGROOM);

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_WaitingFont", CFont::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_RobbyLogo", CTexture::Create(L"../Data/Resources/UI/Robby/Logo.png", L"RobbyLogo", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_Black", CTexture::Create(L"../Data/Resources/UI/Robby/Black.png", L"BlackTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_Loading", CTexture::Create(L"../Data/Resources/UI/Robby/Loading/Loading0%d.png", L"LoadingTexture", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 12))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameStart", CTexture::Create(L"../Data/Resources/UI/Robby/GameStart%d.png", L"GameStart", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameProfile", CTexture::Create(L"../Data/Resources/UI/Robby/GameProfile%d.png", L"GameProfile", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 2))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameCredit", CTexture::Create(L"../Data/Resources/UI/Robby/GameCredit.png", L"GameCredit", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameEnd", CTexture::Create(L"../Data/Resources/UI/Robby/GameEnd.png", L"GameEnd", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameSearch", CTexture::Create(L"../Data/Resources/UI/Robby/GameSearch.png", L"GameSearch", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameSelect", CTexture::Create(L"../Data/Resources/UI/Robby/Select.png", L"Select", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameLeft", CTexture::Create(L"../Data/Resources/UI/Robby/left.png", L"Left", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameRight", CTexture::Create(L"../Data/Resources/UI/Robby/right.png", L"Right", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_RobbyBackGround", CTexture::Create(L"../Data/Resources/UI/Robby/bg1.jpg", L"RobbyBackground1", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_RobbyBackGround2", CTexture::Create(L"../Data/Resources/UI/Robby/bg2.jpg", L"RobbyBackground2", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RobbyLogo", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_RobbyLogo"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameStart", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameStart"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameProfile", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameProfile"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameCredit", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameCredit"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameEnd", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameEnd"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameSearch", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameSearch"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameSelect", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameSelect"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameLeft", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameLeft"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameRight", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_GameRight"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameBlack", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_Black"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_GameLoading", CUI::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_Loading"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_WAITINGROOM, L"Component_UnityPlane_Meshw", CMesh::Create("../Data/Resources/Objects/UnityPlane"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RobbyBackGround", CStaticObject::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_RobbyBackGround", L"Component_UnityPlane_Meshw", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_RobbyBackGround2", CStaticObject::Create(SCENEID::SCENE_WAITINGROOM, L"Component_Texture_RobbyBackGround2", L"Component_UnityPlane_Meshw", L"Component_Shader_FBXTex", XMINT3(256, 256, 256)))))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);

    return 0;
}

unsigned int CLoading::Loading_ForLogin()
{
    m_bFinish = false;

    CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_LOAD);

    CManagement* pManagement = CManagement::GetInstance();

    pManagement->Set_LoadingSceneId(SCENE_ALL);

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_TeddyBear", CTeddyBear::Create())))
        return E_FAIL;

    pManagement->Set_LoadingSceneId(SCENE_LOGIN);

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_Texture_TeddyBear", CTexture::Create(L"../Data/Resources/Character/Texture/teddybear%d.png", L"TeddyBear", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL, 4))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_ALL, L"Component_TileLarge_Mesh", CMesh::Create("../Data/Resources/Objects/tileLarge"))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOGIN, L"Component_Texture_LoginImage", CTexture::Create(L"../Data/Resources/Logo/login.png", L"LoginImage", COMMAND_LOAD, CTexture::TEX_DIFFUSE, CTexture::MESH_GENERAL))))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_LoginImage", CLoginImage::Create())))
        return E_FAIL;

    if (FAILED(pManagement->Add_Prototype_GameObj(L"GameObject_Font_forLogin", CFont::Create())))
        return E_FAIL;

    CDeviceManager::GetInstance()->CommandList_Close(COMMAND_LOAD);

    return 0;
}

CLoading* CLoading::Create(SCENEID eLoadingId)
{
    CLoading* pInstance = new CLoading();

    if (FAILED(pInstance->Initialize_Loading(eLoadingId)))
        Safe_Release(pInstance);

    return pInstance;
}

HRESULT CLoading::Release()
{
    WaitForSingleObject(m_hThread, INFINITE);
    CloseHandle(m_hThread);
    DeleteCriticalSection(&m_Crt);

    delete this;

    return NOERROR;
}
