#ifndef __SCENE_FALLINGMAP__
#define __SCENE_FALLINGMAP__

#include "stdafx.h"
#include "Scene.h"
#include "PlatformManager.h"
#include "Loading.h"

class CPlayer;
class CBoundingBox;
class CFont;
class CPlayerIcon;
class CBillBoardObject;
class CCube;
class CTileLarge;
class CScene_Boardmap;
class CUI;

class CScene_Fallingmap final : public CScene
{
private:
	explicit CScene_Fallingmap();
	virtual ~CScene_Fallingmap();

public:
	virtual HRESULT Initialize_Scene(CScene_Boardmap* pSceneBoardmap);
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();
	HRESULT Scene_Change(SCENEID Scene);
	short cameraAnimation(double TimeDelta);
	virtual short KeyEvent(double TimeDelta);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);
	void IntersectTileTest(float TimeDelta);
	int CheckTile(BoundingBox(&xmBoundingBox)[2], CTileLarge* tileObject, float yLoaction, XMINT3 rgb, float TimeDelta, int blockindex);
	void AdjustPosition(CPlayer* pPlayer, CTileLarge* pTileObj);

	void DeleteBlock();

	void PlayerFallingDead();

public:
	static CScene_Fallingmap* Create(CScene_Boardmap* pSceneBoardmap);
	HRESULT Release();

private:
	CScene_Boardmap* m_pScene_Boardmap = nullptr;
	float moveDelta;
	float deleteDelta;
	bool isMoving = false;

	bool isDead = false;
	bool SendDead = false;

	int winner = -1;
	bool loadcomplete = false;

	//게임 동시시작 flag
	bool gameStarted = false;
private:
	CTileLarge* pTile[144] = { nullptr, };
	CTileLarge* pRedTile[144] = { nullptr, };
	CTileLarge* pYellowTile[144] = { nullptr, };
	CBoundingBox* pBoundingBox[2] = { nullptr, };

	CFont* m_pEndText = nullptr;
	CFont* m_pStatTimeText = nullptr;
	CFont* pButtonText[2] = { nullptr, };

	int m_iStartTime = 0;
	bool m_bStartAsk = false; //시작 요청

	int m_iEndTime = 3;
	bool m_bEndAsk = false; //종료 요청 (3초)
	float m_fCameratime = 0.f;
	CLoading* m_pLoading = nullptr;
};

#endif