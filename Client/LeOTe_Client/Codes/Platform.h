#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"
#include "Material.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CMaterial;
class CShadowShader;

class CPlatform final : public CGameObject
{
public:
	enum PLATFORM_COLOR { platform_red, platform_blue, platform_green, platform_button, platform_store, platform_event };

private:
	explicit CPlatform();
	explicit CPlatform(const CPlatform& rhs);
	virtual ~CPlatform() = default;

	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();

public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);

	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();

	static CPlatform* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();

private:
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CB_GAMEOBJECT_INFO* m_pMappedGameObj = nullptr;

public:

	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

private: // platform 정보
	bool m_isNextPlatformContinue = true; // 너의 넥스트 플랫폼이 이어지는가?
	bool m_isSinglePlatform = true; // 너의 넥스트 플랫폼이 한 개인가?
	_uint m_iNextPlatformIndex; // 위의 bool값들 중 하나라도 false라면, 본래 이어지지 않는 플랫폼 인덱스 값
	_uint m_iNextPlatformIndex2; // 위의 bool값이 둘 다 false라면, 이어지지 않는 플랫폼 인덱스 두번째 값
	int m_iNextPlatformDir = 0; // 0이면 직진, 1이면 left, 2이면 right
	int m_iNextPlatformDir2 = 0; // 두번째 값의 dir
	PLATFORM_COLOR m_ePlatformColor = platform_green;
	
	XMFLOAT3 m_xmDefaultPos;

	bool m_isStartHide = false;
	bool m_isHideEnd = false;

public: // 위의 변수 get, set
	void Set_IsNextPlatformContinue(bool _isNextPlatformContinue) { m_isNextPlatformContinue = _isNextPlatformContinue; }
	void Set_IsSinglePlatform(bool _isSinglePlatform) { m_isSinglePlatform = _isSinglePlatform; }
	void Set_NextPlatformIndex(_uint iIndex, int iNextPlatformDir = 0) { m_iNextPlatformIndex = iIndex; m_iNextPlatformDir = iNextPlatformDir; }
	void Set_NextPlatformIndex2(_uint iIndex2, int iNextPlatformDir2 = 0) { m_iNextPlatformIndex2 = iIndex2; m_iNextPlatformDir2 = iNextPlatformDir2; }
	void Set_PlatformColor(PLATFORM_COLOR ePlatformColor, int r, int g, int b, int a)
	{
		m_ePlatformColor = ePlatformColor;
		m_pMaterialCom->SetDiffuseRGBA(r, g, b, a);
	}

	bool getStartHide() { return m_isStartHide; }
	bool getHideEnd() { return m_isHideEnd; }
	void setStartHide(bool isStartHide) { m_isStartHide = isStartHide; }
	void setHideEnd(bool isHideEnd) { m_isHideEnd = isHideEnd; }
	void setDefaultPos();

	bool Get_IsNextPlatformContinue() { return m_isNextPlatformContinue; }
	bool Get_IsSinglePlatform() { return m_isSinglePlatform; }
	_uint Get_NextPlatformIndex() { return m_iNextPlatformIndex; }
	int Get_NextPlatformDir() { return m_iNextPlatformDir; }
	_uint Get_NextPlatformIndex2() { return m_iNextPlatformIndex2; }
	int Get_NextPlatformDir2() { return m_iNextPlatformDir2; }
	PLATFORM_COLOR Get_PlatformColor() { return m_ePlatformColor; }
	

};