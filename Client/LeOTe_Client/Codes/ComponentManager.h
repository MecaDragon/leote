#ifndef __COMPONENTMANAGER__
#define __COMPONENTMANAGER__

#include "stdafx.h"
#include "Component.h"
#include "Shader.h"
#include "Mesh.h"
#include "Transform.h"

class CComponentManager final
{
	DECLARE_SINGLETON(CComponentManager)

public:
	explicit CComponentManager();
	virtual ~CComponentManager();

public:
	HRESULT Initialize_ComponentManager(_uint iNumScenes);
	HRESULT Add_Prototype_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent* pPrototype);
	HRESULT Clone_Component(_uint iSceneIndex, const wchar_t* pPrototypeTag, CComponent** ppComponent, void* pArg);
	HRESULT Clear_ComponentManager(_uint iSceneIndex);

private:
	CComponent* Find_Prototype(_uint iSceneIndex, const wchar_t* pPrototypeTag);

public:
	HRESULT Release();

private:
	unordered_map<const wchar_t*, CComponent*>* m_pMapPrototypes = nullptr;
	typedef unordered_map<const wchar_t*, CComponent*> PROTOTYPES;

	_uint m_iNumScenes = 0;

};

#endif