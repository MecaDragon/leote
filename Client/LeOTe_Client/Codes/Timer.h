#ifndef __TIMER__
#define __TIMER__

#include "stdafx.h"

class CTimer final
{
private:
	explicit CTimer();
	virtual ~CTimer();

public:
	HRESULT Initialize_Timer();
	double Get_TimeDelta();

	HRESULT Release();

private:
	LARGE_INTEGER m_FrameTime;
	LARGE_INTEGER m_LastTime;
	LARGE_INTEGER m_FixTime;
	LARGE_INTEGER m_CpuTick;

	double m_TimeDelta = 0.0;

public:
	static CTimer* Create();
};

#endif // !__TIMER__

