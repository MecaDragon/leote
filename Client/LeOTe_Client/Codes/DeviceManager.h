#ifndef __DEVICEMANAGER__
#define __DEVICEMANAGER__

#include "stdafx.h"

class CDeviceManager final
{
	DECLARE_SINGLETON(CDeviceManager)

private:
	explicit CDeviceManager();
	virtual ~CDeviceManager();

public:
	HRESULT Initialize_Device();

	HRESULT Create_Device();
	HRESULT Create_CommandQueueAndList();
	HRESULT Create_RtvAndDsvDescriptorHeaps();
	HRESULT Create_SwapChain();
	HRESULT Create_DepthStencilView();

	HRESULT Create_RenderTargetViews();

	HRESULT Change_SwapChainState();
	HRESULT WaitForGpuComplete();

	HRESULT MoveToNextFrame();

public:
	HRESULT CommandList_Reset(COMMANDID eCommandId);
	HRESULT CommandList_Close(COMMANDID eCommandId);


	HRESULT Render_Start(COMMANDID eCommandId);
	HRESULT Render_Middle(COMMANDID eCommandId);
	HRESULT Render_End(COMMANDID eCommandId);

	ID3D12Device* Get_Device() { return m_pd3dDevice; }
	ID3D12GraphicsCommandList* Get_CommandList(COMMANDID eCommandId) { return m_pd3dCommandList[eCommandId]; }

	ID3D12DescriptorHeap* Get_DsvDescriptorHeap() { return m_pd3dDsvDescriptorHeap; }

	HRESULT Release();

private:
	D3D12_CPU_DESCRIPTOR_HANDLE d3dDsvCPUDescriptorHandle;

	//Device
	ID3D12Device* m_pd3dDevice = nullptr;

	//IDXGIFactory4
	IDXGIFactory4* m_pdxgiFactory = NULL;

	//Multisampling
	bool m_bMsaa4xEnable = false;
	UINT m_nMsaa4xQualityLevels = 0;

	//Swapchain
	IDXGISwapChain3*		m_pdxgiSwapChain = nullptr;
	static const UINT		m_nSwapChainBuffers = 2;
	UINT					m_nSwapChainBufferIndex;
	
	//Discriptor
	ID3D12Resource*			m_pd3dSwapChainBackBuffers[m_nSwapChainBuffers];
	ID3D12DescriptorHeap*	m_pd3dRtvDescriptorHeap = nullptr;

	ID3D12Resource*			m_pd3dDepthStencilBuffer = nullptr;
	ID3D12DescriptorHeap*	m_pd3dDsvDescriptorHeap = nullptr;

	// Command 
	ID3D12CommandAllocator*		m_pd3dCommandAllocator[COMMAND_END];
	ID3D12CommandQueue*			m_pd3dCommandQueue = nullptr;
	ID3D12GraphicsCommandList*	m_pd3dCommandList[COMMAND_END];

	//Fence
	ID3D12Fence*				m_pd3dFence = nullptr;
	UINT64						m_nFenceValues[m_nSwapChainBuffers];
	HANDLE						m_hFenceEvent;

	//ResourceBarrier
	D3D12_RESOURCE_BARRIER m_d3dResourceBarrier;

#if defined(_DEBUG)
	//ID3D12Debug*				m_pd3dDebugController;
#endif // 

	int m_nClientWidth = 0;
	int m_nClientHeight = 0;
	


};

#endif // !__DEVICEMANAGER__

