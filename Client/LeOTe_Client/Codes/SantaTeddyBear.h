#pragma once
#include "TeddyBear.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CTexture;
class CMaterial;
class CShadowShader;
class CSantaHat;

class CSantaTeddyBear final : public CTeddyBear
{
private:
	explicit CSantaTeddyBear();
	explicit CSantaTeddyBear(const CSantaTeddyBear& rhs);
	virtual ~CSantaTeddyBear() = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();
	void ChangeAnimation(string sClipName);
	bool IsEndAnimation() { return m_aimationEnd; }
	bool IsMiddleAnimation() { return m_aimationMiddle; }

private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();
public:
	static CSantaTeddyBear* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();

private:
	CSantaHat* m_pSantaHat = nullptr;

};