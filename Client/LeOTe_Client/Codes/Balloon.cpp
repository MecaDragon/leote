#include "stdafx.h"
#include "Balloon.h"
#include "Management.h"
#include "Texture.h"
#include "Renderer.h"
#include "ShadowShader.h"
#include "PlatformManager.h"
#include "Button.h"

CBalloon::CBalloon()
	: CGameObject()
{

}

CBalloon::CBalloon(const CBalloon& rhs)
	: CGameObject(rhs)
{
}

HRESULT CBalloon::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CBalloon::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMappedGameObj->m_xmf4x4Material = { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };

	if (pArg)
	{
		m_pPlatformManager = (CPlatformManager*)pArg;
	}

	m_xmObjPosition = XMFLOAT3(-9, 23.6, -80.5);
	m_pTransformCom->SetPosition(m_xmObjPosition);
	//m_pTransformCom->Rotate(-90.f, 0.f, 0.f);
	//m_pTransformCom->SetScale(1.f, 1.f, 1.f);

	

	return NOERROR;
}

short CBalloon::Update_GameObject(double TimeDelta)
{
	m_fTimer += (float)(TimeDelta * m_fSpeed * m_nUpDown);

	if (m_fTimer > 0.32f || 0.f > m_fTimer)
		m_nUpDown *= -1;

	m_xmObjPosition.y = 23.6 + m_fTimer;

	if(m_nCurrentIndex != m_nDestIndex)
		moveToDest(TimeDelta);

	return short();
}

short CBalloon::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	m_pTransformCom->SetUp(XMFLOAT3(0.f, 1.f, 0.f));
	m_pTransformCom->SetRight(Vector3::CrossProduct(XMFLOAT3(0.f, 1.f, 0.f), m_xmLook, true));
	m_pTransformCom->SetLook(m_xmLook);
	m_pTransformCom->SetPosition(m_xmObjPosition.x, m_xmObjPosition.y, m_xmObjPosition.z);
	m_pTransformCom->SetScale(1.f, 1.f, 1.f);
	m_pTransformCom->Rotate(-90.f, 0.f, 0.f);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CBalloon::Render_Shadow()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform);

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	if (m_pTextureCom)
		m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();

	return NOERROR;
}

HRESULT CBalloon::Render_GameObject()
{
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4World), XMMatrixTranspose(XMLoadFloat4x4(&(m_pTransformCom->GetWorldMat()))));
	XMStoreFloat4x4(&(m_pMappedGameObj->m_xmf4x4TexTransform), XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Identity())));

	m_pShaderCom->SetPipeline(pCommandList);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pTextureCom->UpdateShaderVariables(pCommandList);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CBalloon::Add_Component()
{

	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_HotAirBalloon_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader //Component_Shader_FBXTexLight
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXTexMLight", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Texture
	if (FAILED(CGameObject::Add_Component(SCENE_BOARDMAP, L"Component_Texture_HotAirBalloon", L"Com_Texture", (CComponent**)&m_pTextureCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

void CBalloon::moveToDest(double TimeDelta)
{
	CPlatform* pCurrentPlatform = m_pPlatformManager->Get_Platform(m_nCurrentIndex); // ���� �÷����� ������
	CPlatform* pNextPlatform = m_pPlatformManager->Get_Platform(m_nNextIndex); // next �÷����� ������

	XMFLOAT3 currentPlatformPos, nextPlatformPos, direction;

	currentPlatformPos = pCurrentPlatform->m_pTransformCom->GetPosition();
	nextPlatformPos = pNextPlatform->m_pTransformCom->GetPosition();
	nextPlatformPos.y = 0.f;

	XMFLOAT3 xmLength, xmBalloonPos;
	xmBalloonPos = m_xmObjPosition;
	xmBalloonPos.y = 0.f;

	// �ؽ�Ʈ �÷������� ���ϴ� ���⺤�� ���ϱ�
	XMStoreFloat3(&direction, XMLoadFloat3(&nextPlatformPos) - XMLoadFloat3(&xmBalloonPos));
	xmLength = direction;
	direction = Vector3::Normalize(direction);

	XMFLOAT3 xmf3dot;
	XMStoreFloat3(&xmf3dot, XMVector3AngleBetweenVectors(XMLoadFloat3(&m_xmLook), XMLoadFloat3(&direction)));
	float tmpangle = XMConvertToDegrees(xmf3dot.y);

	XMFLOAT3 xmf3Cross = Vector3::CrossProduct(m_xmObjPosition, direction);
	float fDot = Vector3::DotProduct(xmf3Cross, m_pTransformCom->GetUp());

	XMFLOAT3 upVector = XMFLOAT3(0.f, 1.f, 0.f);

	if (tmpangle >= 2.f)
	{
		if (fDot > 0) // ��
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * 150.f));
			m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
		}
		else if (fDot < 0) // ��
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&upVector), XMConvertToRadians(TimeDelta * -150.f));
			m_xmLook = Vector3::TransformNormal(m_xmLook, xmmtxRotate);
		}
	}
	else
		m_xmLook = direction;

	direction = Vector3::Normalize(direction);
	m_xmObjPosition = Vector3::Add(m_xmObjPosition, direction, TimeDelta * 10.f);

	float fLength = Vector3::Length(xmLength);

	if (fLength < 0.2f)
	{
		m_nCurrentIndex = m_nCurrentIndex + 1;
		m_nNextIndex += 1;

		if (m_nNextIndex == 1)
			m_nCurrentIndex = 0;
		else if (m_nCurrentIndex == 33)
			m_nNextIndex = 0;
	}

	if (m_nCurrentIndex == m_nDestIndex)
	{
		m_pPlatformManager->Get_Platform(m_nDestIndex)->setStartHide(true);
		m_pButton->setStart(true);
	}
}

CBalloon* CBalloon::Create()
{
	CBalloon* pInstance = new CBalloon();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CBalloon Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject* CBalloon::Clone_GameObject(void* pArg)
{
	CBalloon* pInstance = new CBalloon(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CBalloon Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}
HRESULT CBalloon::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTextureCom);

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
