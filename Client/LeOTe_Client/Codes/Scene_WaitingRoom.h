#ifndef __Scene_WaitingRoom__
#define __Scene_WaitingRoom__

#include "stdafx.h"
#include "Scene.h"
#include "Loading.h"

class CManagement;
class CFont;
class CUI;
class CStaticObject;

class CScene_WaitingRoom : public CScene
{
private:
	explicit CScene_WaitingRoom();
	virtual ~CScene_WaitingRoom();

	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);

public:
	virtual HRESULT Initialize_Scene();
	virtual short KeyEvent(double TimeDelta);
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();

private:
	CLoading* m_pLoading = nullptr;

	CManagement* m_pManagement = nullptr;
	CFont* m_pLoginText = nullptr;
	CUI* m_pGameLogo = nullptr;
	CUI* m_pGameStart = nullptr;
	CUI* m_pGameProfile = nullptr;
	CUI* m_pGameCredit = nullptr;
	CUI* m_pGameEnd = nullptr;
	CUI* m_pGameSearch = nullptr;
	CUI* m_pGameBlack = nullptr;
	CUI* m_pGameLoading = nullptr;

	CUI* m_pGameSelect = nullptr;
	CUI* m_pGameLeft = nullptr;
	CUI* m_pGameRight = nullptr;

	CStaticObject* m_pGameBG = nullptr;
	CStaticObject* m_pGameBG2 = nullptr;

	bool m_bGameStartScale = false;
	bool m_bGameProfileScale = false;
	bool m_bGameCreditScale = false;
	bool m_bGameEndScale = false;
	bool m_bMatchClick = false;

	int	m_iLoadingIndex = 0;

	float m_fFrameTime = 0.f;
public:
	static CScene_WaitingRoom* Create();
	HRESULT Release();

};

#endif