#include "Font.h"
#include "PipeLine.h"
#include "DeviceManager.h"
#include "Management.h"
#include "Renderer.h"
#include "DescriptorHeapManager.h"

CFont::CFont()
{
}

CFont::CFont(const CFont& rhs)
	: CGameObject(rhs)
	, arialFont(rhs.arialFont)
	, textVertexs(rhs.textVertexs)
	, fontTextureBufferUploadHeap(rhs.fontTextureBufferUploadHeap)
	, srvHandleSize(rhs.srvHandleSize)
	//, m_pd3dCbvSrvDescriptorHeap(rhs.m_pd3dCbvSrvDescriptorHeap)
{
	// create text vertex buffer committed resources
	// create upload heap. We will fill this with data for our text
	//ID3D12Resource* vBufferUploadHeap;

	CDeviceManager* pDeviceMgr = CDeviceManager::GetInstance();
	ID3D12Device* pDevice = pDeviceMgr->Get_Device();
	pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), // upload heap
		D3D12_HEAP_FLAG_NONE, // no flags
		&CD3DX12_RESOURCE_DESC::Buffer(maxNumTextCharacters * sizeof(TextVertex)), // resource description for a buffer
		D3D12_RESOURCE_STATE_GENERIC_READ, // GPU will read from this buffer and copy its contents to the default heap
		nullptr,
		IID_PPV_ARGS(&m_pGameObjResource));
	m_pGameObjResource->SetName(L"Text Vertex Buffer Upload Resource Heap");

	CD3DX12_RANGE readRange(0, 0);	// We do not intend to read from this resource on the CPU. (so end is less than or equal to begin)

	// map the resource heap to get a gpu virtual address to the beginning of the heap
	m_pGameObjResource->Map(0, &readRange, (void**)&textVertexs);

	// set the text vertex buffer view
	m_d3dVertexBufferView.BufferLocation = m_pGameObjResource->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = sizeof(TextVertex);
	m_d3dVertexBufferView.SizeInBytes = maxNumTextCharacters * sizeof(TextVertex);

	message = L"Hello World";
	xmPos = { 0.f, 0.f };
	xmScale = { 1.f, 1.f };
	xmColor = { 1.0f, 1.0f, 1.0f, 1.0f };
}

CFont::~CFont()
{
}

HRESULT CFont::Initialize_GameObject_Prototype()
{
	CreateLoadFont();
	return NOERROR;
}

HRESULT CFont::Initialize_GameObject(void* pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	//m_pd3dCbvSrvDescriptorHeap->AddRef();
	//m_pGameObjResource->AddRef();

	return NOERROR;
}

short CFont::Update_GameObject(double TimeDelta)
{
	return 0;
}

short CFont::LateUpdate_GameObject(double TimeDelta)
{
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return 0;
}

HRESULT CFont::Render_GameObject()
{
	//RenderText(arialFont, std::wstring(L"Hello World"), XMFLOAT2(0.02f, 0.01f), XMFLOAT2(2.0f, 2.0f));
	if(m_bHide == false)
		RenderText(arialFont, message, xmPos, xmScale, XMFLOAT2(0.5f, 0.5f), xmColor);
	
	return NOERROR;
}

CFont* CFont::Create()
{
	CFont* pInstance = new CFont();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CFont Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject* CFont::Clone_GameObject(void* pArg)
{
	CFont* pInstance = new CFont(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CFont Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

Font CFont::LoadFont(LPCWSTR filename, int windowWidth, int windowHeight)
{
	std::wifstream fs;
	fs.open(filename);

	Font font;
	std::wstring tmp;
	int startpos;

	// extract font name
	fs >> tmp >> tmp; // info face="Arial"
	startpos = tmp.find(L"\"") + 1;
	font.name = tmp.substr(startpos, tmp.size() - startpos - 1);

	// get font size
	fs >> tmp; // size=73
	startpos = tmp.find(L"=") + 1;
	font.size = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

	// bold, italic, charset, unicode, stretchH, smooth, aa, padding, spacing
	fs >> tmp >> tmp >> tmp >> tmp >> tmp >> tmp >> tmp; // bold=0 italic=0 charset="" unicode=0 stretchH=100 smooth=1 aa=1 

	// get padding
	fs >> tmp; // padding=5,5,5,5 
	startpos = tmp.find(L"=") + 1;
	tmp = tmp.substr(startpos, tmp.size() - startpos); // 5,5,5,5

	// get up padding
	startpos = tmp.find(L",") + 1;
	font.toppadding = std::stoi(tmp.substr(0, startpos)) / (float)windowWidth;

	// get right padding
	tmp = tmp.substr(startpos, tmp.size() - startpos);
	startpos = tmp.find(L",") + 1;
	font.rightpadding = std::stoi(tmp.substr(0, startpos)) / (float)windowWidth;

	// get down padding
	tmp = tmp.substr(startpos, tmp.size() - startpos);
	startpos = tmp.find(L",") + 1;
	font.bottompadding = std::stoi(tmp.substr(0, startpos)) / (float)windowWidth;

	// get left padding
	tmp = tmp.substr(startpos, tmp.size() - startpos);
	font.leftpadding = std::stoi(tmp) / (float)windowWidth;

	fs >> tmp; // spacing=0,0

	// get lineheight (how much to move down for each line), and normalize (between 0.0 and 1.0 based on size of font)
	fs >> tmp >> tmp; // common lineHeight=95
	startpos = tmp.find(L"=") + 1;
	font.lineHeight = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)windowHeight;

	// get base height (height of all characters), and normalize (between 0.0 and 1.0 based on size of font)
	fs >> tmp; // base=68
	startpos = tmp.find(L"=") + 1;
	font.baseHeight = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)windowHeight;

	// get texture width
	fs >> tmp; // scaleW=512
	startpos = tmp.find(L"=") + 1;
	font.textureWidth = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

	// get texture height
	fs >> tmp; // scaleH=512
	startpos = tmp.find(L"=") + 1;
	font.textureHeight = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

	// get pages, packed, page id
	fs >> tmp >> tmp; // pages=1 packed=0
	fs >> tmp >> tmp; // page id=0

	// get texture filename
	std::wstring wtmp;
	fs >> wtmp; // file="Arial.png"
	startpos = wtmp.find(L"\"") + 1;
	font.fontImage = wtmp.substr(startpos, wtmp.size() - startpos - 1);

	// get number of characters
	fs >> tmp >> tmp; // chars count=97
	startpos = tmp.find(L"=") + 1;
	font.numCharacters = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

	// initialize the character list
	font.CharList = new FontChar[font.numCharacters];

	for (int c = 0; c < font.numCharacters; ++c)
	{
		// get unicode id
		fs >> tmp >> tmp; // char id=0
		startpos = tmp.find(L"=") + 1;
		font.CharList[c].id = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

		// get x
		fs >> tmp; // x=392
		startpos = tmp.find(L"=") + 1;
		font.CharList[c].u = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)font.textureWidth;

		// get y
		fs >> tmp; // y=340
		startpos = tmp.find(L"=") + 1;
		font.CharList[c].v = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)font.textureHeight;

		// get width
		fs >> tmp; // width=47
		startpos = tmp.find(L"=") + 1;
		tmp = tmp.substr(startpos, tmp.size() - startpos);
		font.CharList[c].width = (float)std::stoi(tmp) / (float)windowWidth;
		font.CharList[c].twidth = (float)std::stoi(tmp) / (float)font.textureWidth;

		// get height
		fs >> tmp; // height=57
		startpos = tmp.find(L"=") + 1;
		tmp = tmp.substr(startpos, tmp.size() - startpos);
		font.CharList[c].height = (float)std::stoi(tmp) / (float)windowHeight;
		font.CharList[c].theight = (float)std::stoi(tmp) / (float)font.textureHeight;

		// get xoffset
		fs >> tmp; // xoffset=-6
		startpos = tmp.find(L"=") + 1;
		font.CharList[c].xoffset = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)windowWidth;

		// get yoffset
		fs >> tmp; // yoffset=16
		startpos = tmp.find(L"=") + 1;
		font.CharList[c].yoffset = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)windowHeight;

		// get xadvance
		fs >> tmp; // xadvance=65
		startpos = tmp.find(L"=") + 1;
		font.CharList[c].xadvance = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos)) / (float)windowWidth;

		// get page
		// get channel
		fs >> tmp >> tmp; // page=0    chnl=0
	}

	// get number of kernings
	fs >> tmp >> tmp; // kernings count=96
	startpos = tmp.find(L"=") + 1;
	font.numKernings = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

	// initialize the kernings list
	font.KerningsList = new FontKerning[font.numKernings];

	for (int k = 0; k < font.numKernings; ++k)
	{
		// get first character
		fs >> tmp >> tmp; // kerning first=87
		startpos = tmp.find(L"=") + 1;
		font.KerningsList[k].firstid = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

		// get second character
		fs >> tmp; // second=45
		startpos = tmp.find(L"=") + 1;
		font.KerningsList[k].secondid = std::stoi(tmp.substr(startpos, tmp.size() - startpos));

		// get amount
		fs >> tmp; // amount=-1
		startpos = tmp.find(L"=") + 1;
		int t = (float)std::stoi(tmp.substr(startpos, tmp.size() - startpos));
		font.KerningsList[k].amount = (float)t / (float)windowWidth;
	}

	return font;
}

HRESULT CFont::CreateLoadFont()
{
	HRESULT hr = S_OK;

	CDeviceManager* pDeviceMgr = CDeviceManager::GetInstance();
	ID3D12Device* pDevice = pDeviceMgr->Get_Device();
	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_LOAD);

	// create the descriptor heap that will store our srv
	//D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	//heapDesc.NumDescriptors = 1; // we now have an srv for the font as well as the cube's srv
	//heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	//heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	//hr = pDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&m_pd3dCbvSrvDescriptorHeap));
	// Load Font

	arialFont = LoadFont(L"Arial.fnt", WINDOWX, WINDOWY);

	// Load the image from file
	D3D12_RESOURCE_DESC fontTextureDesc;
	int fontImageBytesPerRow;
	BYTE* fontImageData;
	int fontImageSize = LoadImageDataFromFile(&fontImageData, fontTextureDesc, arialFont.fontImage.c_str(), fontImageBytesPerRow);

	// create the font texture resource
	hr = pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&fontTextureDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&arialFont.textureBuffer));
	if (FAILED(hr))
	{
		return false;
	}
	arialFont.textureBuffer->SetName(L"Font Texture Buffer Resource Heap");

	//ID3D12Resource* fontTextureBufferUploadHeap;
	UINT64 fontTextureUploadBufferSize;
	pDevice->GetCopyableFootprints(&fontTextureDesc, 0, 1, 0, nullptr, nullptr, nullptr, &fontTextureUploadBufferSize);

	// create an upload heap to copy the texture to the gpu
	hr = pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE, // no flags
		&CD3DX12_RESOURCE_DESC::Buffer(fontTextureUploadBufferSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&fontTextureBufferUploadHeap));

	if (FAILED(hr))
	{
		return false;
	}
	fontTextureBufferUploadHeap->SetName(L"Font Texture Buffer Upload Resource Heap");

	// store font image in upload heap
	D3D12_SUBRESOURCE_DATA fontTextureData = {};
	fontTextureData.pData = &fontImageData[0]; // pointer to our image data
	fontTextureData.RowPitch = fontImageBytesPerRow; // size of all our triangle vertex data
	fontTextureData.SlicePitch = fontImageBytesPerRow * fontTextureDesc.Height; // also the size of our triangle vertex data

	// Now we copy the upload buffer contents to the default heap
	UpdateSubresources(pd3dCommandList, arialFont.textureBuffer, fontTextureBufferUploadHeap, 0, 0, 1, &fontTextureData);

	// transition the texture default heap to a pixel shader resource (we will be sampling from this heap in the pixel shader to get the color of pixels)
	pd3dCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(arialFont.textureBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	// create an srv for the font
	D3D12_SHADER_RESOURCE_VIEW_DESC fontsrvDesc = {};
	fontsrvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	fontsrvDesc.Format = fontTextureDesc.Format;
	fontsrvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	fontsrvDesc.Texture2D.MipLevels = 1;

	// we need to get the next descriptor location in the descriptor heap to store this srv
	srvHandleSize = pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	//arialFont.srvHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_pd3dCbvSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), 0, srvHandleSize);

	CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle = CDescriptorHeapManager::GetInstance()->get_CpuDescriptorHandle();
	CD3DX12_GPU_DESCRIPTOR_HANDLE srvGpuHandle = CDescriptorHeapManager::GetInstance()->get_GpuDescriptorHandle();

	pDevice->CreateShaderResourceView(arialFont.textureBuffer, &fontsrvDesc, srvHandle);
	srvHandle.Offset(1, g_CbvSrvDescriptorIncrementSize);

	arialFont.srvHandle = srvGpuHandle;
	srvGpuHandle.Offset(1, g_CbvSrvDescriptorIncrementSize);

	CDescriptorHeapManager::GetInstance()->set_CpuDescriptorHandle(srvHandle);
	CDescriptorHeapManager::GetInstance()->set_GpuDescriptorHandle(srvGpuHandle);

	delete fontImageData;
	//fontTextureBufferUploadHeap->Release();
	fontTextureBufferUploadHeap->AddRef();
	arialFont.textureBuffer->AddRef();
}

void CFont::SetText(const wstring str, XMFLOAT2 pos, XMFLOAT2 scale, XMFLOAT4 color)
{
	message = str;
	xmPos = pos;
	xmScale = scale;
	xmColor = color;
}

void CFont::SetTextString(const wstring str)
{
	message = str;
}

void CFont::SetTextPos(XMFLOAT2 pos)
{
	xmPos = pos;
}

void CFont::SetTextScale(XMFLOAT2 scale)
{
	xmScale = scale;
}

void CFont::SetTextColor(XMFLOAT4 color)
{
	xmColor = color;
}

void CFont::RenderText(Font font, std::wstring text, XMFLOAT2 pos, XMFLOAT2 scale, XMFLOAT2 padding, XMFLOAT4 color)
{
	ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	CDeviceManager* pDeviceMgr = CDeviceManager::GetInstance();

	ID3D12DescriptorHeap* pSrvDescriptorHeap = CDescriptorHeapManager::GetInstance()->get_srvDescriptorHeap();

	pd3dCommandList->SetDescriptorHeaps(1, &pSrvDescriptorHeap);
	//m_pd3dCommandList[eCommandId]->ClearDepthStencilView(d3dDsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
	//pd3dCommandList->ClearDepthStencilView(pDeviceMgr->GetDescripotHeapHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, NULL);
	m_pShaderCom->SetPipeline(pd3dCommandList);
	pd3dCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	pd3dCommandList->IASetVertexBuffers(0, 1, &m_d3dVertexBufferView);
	pd3dCommandList->SetGraphicsRootDescriptorTable(13, font.srvHandle);

	int numCharacters = 0;

	float topLeftScreenX = (pos.x * 2.0f) - 1.0f;
	float topLeftScreenY = ((1.0f - pos.y) * 2.0f) - 1.0f;

	float x = topLeftScreenX;
	float y = topLeftScreenY;

	float horrizontalPadding = (font.leftpadding + font.rightpadding) * padding.x;
	float verticalPadding = (font.toppadding + font.bottompadding) * padding.y;

	// cast the gpu virtual address to a textvertex, so we can directly store our vertices there
	TextVertex* vert = (TextVertex*)textVertexs;

	wchar_t lastChar = -1; // no last character to start with

	for (int i = 0; i < text.size(); ++i)
	{
		wchar_t c = text[i];

		FontChar* fc = font.GetChar(c);

		// character not in font char set
		if (fc == nullptr)
			continue;

		// end of string
		if (c == L'\0')
			break;

		// new line
		if (c == L'\n')
		{
			x = topLeftScreenX;
			y -= (font.lineHeight + verticalPadding) * scale.y;
			continue;
		}

		// don't overflow the buffer. In your app if this is true, you can implement a resize of your text vertex buffer
		if (numCharacters >= maxNumTextCharacters)
			break;

		float kerning = 0.0f;
		if (i > 0)
			kerning = font.GetKerning(lastChar, c);

		vert[numCharacters] = TextVertex(color.x,
			color.y,
			color.z,
			color.w,
			fc->u,
			fc->v,
			fc->twidth,
			fc->theight,
			x + ((fc->xoffset + kerning) * scale.x),
			y - (fc->yoffset * scale.y),
			fc->width * scale.x,
			fc->height * scale.y);

		numCharacters++;

		// remove horrizontal padding and advance to next char position
		x += (fc->xadvance - horrizontalPadding) * scale.x;

		lastChar = c;
	}

	// we are going to have 4 vertices per character (trianglestrip to make quad), and each instance is one character
	pd3dCommandList->DrawInstanced(4, numCharacters, 0, 0);
}

HRESULT CFont::Add_Component()
{

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Font", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

void CFont::CreateShaderVariables()
{
	
}

HRESULT CFont::Release()
{
	Safe_Release(m_pShaderCom);

	//m_pd3dCbvSrvDescriptorHeap->Release();
	//if (fontTextureBufferUploadHeap)
	//{
	//	fontTextureBufferUploadHeap->Release();
	//	fontTextureBufferUploadHeap = nullptr;
	//}
	//if (textVertexs)
	//{
	//	delete textVertexs;
	//	textVertexs = nullptr;
	//}

	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	//delete[] arialFont.KerningsList;
	//delete[] arialFont.CharList;

	//arialFont.textureBuffer->Unmap(0, nullptr);
	//if (arialFont.textureBuffer)
	//{
	//	arialFont.textureBuffer->Release();
	//	arialFont.textureBuffer = nullptr;
	//}
	delete this;

	return NOERROR;
}
