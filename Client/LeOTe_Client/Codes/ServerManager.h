#ifndef __SERVERMANAGER__
#define __SERVERMANAGER__
#define _CRT_SECURE_NO_WARNINGS         // 최신 VC++ 컴파일 시 경고 방지
#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#pragma comment(lib,"ws2_32")
#include <WS2tcpip.h>
#define SERVERPORT 9000

class CManagement;
class CPlayer;
class CChicken;
class CTeddyBear;

enum RANK { FIRST, SECOND, THIRD };

struct MatchTiePlayer {
	int index;
	int dicenum;
};

class CServerManager final
{
	DECLARE_SINGLETON(CServerManager)

private:
	CServerManager() {};
	~CServerManager();

public:
	//err msg 
	void err_quit(const WCHAR* msg);
	void err_display(const WCHAR* msg);

	//server
	int WSAStart();
	void CreateSocket();
	int ConnectToServer();

	void SetServerIP(string strIP);
	void SetServerID(string strID);

	//send receive
	void SendData(char datainfo, LPVOID packet);
	void RecvPacket();

	void Process_Packet(char* buf);

	//end
	void DisconnectServer();

	LOGIN_STATE Get_LoginState() { return m_eLoginState; }

	bool Get_SendID() { return m_bSendID; }
	void Set_SendID(bool bSendID) { m_bSendID = bSendID; }

	int Get_ClientIndex() { return ClientIndex; }
	int Get_PlayerNum() { return playerindex; }//플레이어에게 순서가 다 정해졌는 지
	void Set_PlayerNum(int num) { playerindex = num; } 

	int Get_DiceNum() { return dicenum; }
	void Set_DiceNum(int num) { dicenum = num; }

	int Get_CurrentOrder() { return currentorder; }
	int Get_PrevOrder() { return prevorder; }
	void Set_PrevOrder(int order) { prevorder = order; }

	CPlayer* Get_CurPlayer() { return pCurPlayer; }
	void Set_CurPlayer(CPlayer* player) { pCurPlayer = player; }

	bool Get_DiceEnter() { return diceenter; }
	void Set_DiceEnter(bool d) { diceenter = d; }
	//
	const int& IsCoinEvent() { return coinevent; }
	void SetCoinEvent(int coin) { coinevent = coin; }

	const bool& IsAbleNickNameInit() { return ableNickanmeInit; }
	void SetAbleNickNameInit(bool init) { ableNickanmeInit = init; }

	void Set_Boardmapinit() { boardmapReady = 0; }
	const int& Get_BoardmapReady() { return boardmapReady; }
	HRESULT Release();

	//falling
	void BlockInit() { blockid = -1; blockcolor = -1; }
	const int& Get_Blockid() { return blockid; }
	const char& Get_BlockColor() { return blockcolor; }

	//ggoggo
	void InitChickens();
	void SetChickenNum(int n) { chickennum = n; }
	const int& GetChickenNum() { return chickennum; }

	void SetEscapeUIOn(bool escape) { escapeUI = escape; }
	bool GetEscapeUIOn() { return escapeUI; }

	const int& Get_CreatedChickens() {	return createchickens;	}

	//minigame
	const int& Get_MiniWinner() { return minigamewinner; }
	void Set_MiniWinner(int i) { minigamewinner = i; }

	void Set_MinigameReady(bool n) { minigameReady = n; }
	const bool& Get_MinigameReady() { return minigameReady; }

	void Set_PlayTime(short time) { minigame_playtime = time; }
	const short& Get_PlayTime() { return minigame_playtime; }

	bool Get_PotionCreate() { return potionCreated; }
	void Set_PotionCreate(bool p) { potionCreated = p; }
	XMFLOAT3 Get_PotionPosition() { return potionPosition; }

	const int& Get_DeathPlayer() { return deathplayer; }
	void Set_DeathPlayer(int deathnum) { deathplayer = deathnum; }

	//onemind
	bool Get_Lighton() { return lighton; }
	void Set_Lighton(bool l) { lighton = l; }

	bool Get_LookOn() { return lookon; }
	void Set_LookOn(bool l) { lookon = l; }

	bool Get_PositionOn() { return positionon; }
	void Set_PositionOn(bool p) { positionon = p; }

	XMFLOAT3 Get_RobotLook() { return robotlook; }
	XMFLOAT3 Get_RobotPosition() { return robotposition; }

	char Get_BatteryHp() { return batteryhp; }

	int Get_SantaAnim() { return santaAnim; }
	void Set_SantaAnim(int anim) { santaAnim = anim; }

	void Set_RobotRespawn(bool res) { robotrespawnon = res; }
	bool Get_RobotRespawn() { return robotrespawnon; }

	XMFLOAT3 Get_RobotRespawnPosition() { return robotRespawnPos; }

	bool Get_SnowmanSavePoint(int index) { return snowman[index]; }
	void Set_SnowmanSavePoint(int index, bool b) { snowman[index] = b; }

	//match
	bool Get_Matching() {return matching;}
	void Set_Matching(bool mat) { matching = mat; }

	//showmap
	bool Get_ShowMap() { return showmap; }
	void Set_ShowMap(bool sh) { showmap = sh; }

	//starttime
	const short& Get_StartTime() { return starttime; }
	void Set_StartTime(short time) { starttime = time; }

	//disconnect
	const bool& Get_Disconnect() { return disconnect; }
	void Set_Disconnect(bool con) { disconnect = con; }

	//prize
	void Init_TiedPlayer() { isTied = false; tiedPlayer[0] = -1; tiedPlayer[1] = -1; }
	short Get_Tied() { return isTied; } //0 - 아직 안옴 1 - 동점자 있음 2 - 동점자 없음
	void Set_Tied(short tied) { isTied = tied; }

	int Get_TiedPlayer(int index) { return tiedPlayer[index]; }

	//event
	int Get_BoardmapEvent() { return boardmap_event; }
	void Set_BoardmapEvent(int boardmap) { boardmap_event = boardmap; }

	char Get_ItemBuy() { return itembuy; }
	void Set_ItemBuy(char buy) { itembuy = buy; }

	ITEM Get_ItemType() { return eItem; }
	void Set_ItemType(ITEM item) { eItem = item; }

	//item use
	bool Get_isItemUse() { return itemUse; }
	void Set_isItemUse(bool use) { itemUse = use; }

	ITEM Get_UsedItemType() { return usedItem; }
	void Set_UsedItemType(ITEM type) { usedItem = type; }

	char Get_MiniDiceNum() { return miniDice; }
	void Set_MiniDiceNum(char num) { miniDice = num; }

	char Get_InventoryIndex() {	return inventoryIndex; }
	void Set_InventoryIndeX(char i) { inventoryIndex = i; }

	int Get_UsedPlayerIndex() { return usedPlayerIndex; } //아이템 사용한사람 인덱스

	//동점자
	bool Get_isTiedDice() { return matchTieInfo; }
	void Set_isTiedDice(bool tie) { matchTieInfo = tie; }
	void Set_MatchTieCount(int cnt) { matchtieCount = cnt; }

	MatchTiePlayer Get_MatchTiedPlayer() { return matchTiedPlayers; }
	void Init_MatchTiedPlayer() { matchTiedPlayers.dicenum = 0; matchTiedPlayers.index = 0; }

	//함정, 이벤트 발판
	int Get_isSpecialEvent() {	return special_event;}
	void Set_isSpecialEvent(int sp = 0) { special_event = sp; }

	int Get_SpecialEventPlayerindex() { return special_event_player; }
	void Set_SpecialEventPlayerindex(int sp = -1) {special_event_player = sp; }

	
private:
	//start
	SOCKET sock;
	SOCKADDR_IN serveraddr;
	char serverip[15];
	char serverid[MAX_NAME_LEN];

	int ClientIndex;

	LOGIN_STATE m_eLoginState = LOGIN_STATE::IP;

	int enterCount = 0;

	bool m_bSendID = false;

	CManagement* m_pManagement = nullptr;

	int playerindex = 0;
	//boardmap
	int dicenum = -1;

	int prevorder = 0; // 이전 순서
	int currentorder = 0; //현재 순서

	CPlayer* pCurPlayer = nullptr;

	bool diceenter = false; //주사위 굴리기 시작
	int coinevent = 0; //0 - 아무일도없음 1 - 코인 증가 2 - 코인 하락

	bool ableNickanmeInit = false;
	
	//씬전환
	int boardmapReady = 0;
	bool minigameReady = false;

	//falling map
	int	blockid = -1;
	char blockcolor = -1;

	//ggoggo map
	int createchickens = 0;
	CChicken* Chickens[MAX_CHICKEN];
	int chickennum = 20;
	bool escapeUI = false;

	//bullfight
	bool potionCreated = false;
	XMFLOAT3 potionPosition;

	//minigame
	int minigamewinner = -1;
	int deathplayer = -1;
	short minigame_playtime = 0;

	//onemind
	int loadnum = 0; //로딩완료
	bool lighton = false; //불켜기
	bool lookon = false; //look패킷 옴
	bool positionon = false; //position패킷 옴
	char batteryhp = 90;

	int santaAnim = -1;

	XMFLOAT3 robotposition = XMFLOAT3(0.f,0.f,0.f);
	XMFLOAT3 robotlook = XMFLOAT3(0.f,0.f,0.f);

	XMFLOAT3 robotRespawnPos = XMFLOAT3(0.f, 0.f, 0.f);
	bool robotrespawnon = false;

	bool snowman[2] = {false, false};

	//match
	bool matching = false;

	//showmap
	bool showmap = false;

	//starttime
	short starttime = 3;

	//disconnect
	bool disconnect = false;

	//prize
	short isTied = 0; //동점자 발생 여부
	int tiedPlayer[2] = { -1,-1 };

	//event
	int boardmap_event = -1;
	char itembuy = 0; //0 - 아직 안옴, 1 - 구매, 2 - 안구매
	ITEM eItem = ITEM_END;

	//itemuse
	bool itemUse = false;
	ITEM usedItem = ITEM_END;
	char miniDice = 0;
	char inventoryIndex = -1;
	int usedPlayerIndex = -1;

	//동점자
	int matchtieCount = 0;
	bool matchTieInfo = false;
	MatchTiePlayer matchTiedPlayers;
	
	//special event - 함정,이벤트 발판
	int special_event = 0; //0 -아직 온 이벤트 없음 0~ 이벤트 발생
	int special_event_player = -1;
};

#endif // !__SERVERMANAGER__