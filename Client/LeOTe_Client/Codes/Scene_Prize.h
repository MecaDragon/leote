#ifndef __Scene_Prize__
#define __Scene_Prize__

#include "stdafx.h"
#include "Scene.h"
#include "Loading.h"

enum PRIZEORDER { PRIZE_WAIT, PRIZE_DUAL, PRIZE_DUALRESULT, PRIZE_WIN, PRIZE_WINNER, PRIZE_PRECREDIT, PREIZE_CREDIT, PRIZE_ENDING, PRIZE_END };

class CManagement;
class CFont;
class CUI;
class CStaticObject;
class CTeddyBear;
class CPlayer;
class CGhost;
class CBird;

class CScene_Prize : public CScene
{
private:
	explicit CScene_Prize();
	virtual ~CScene_Prize();

	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);

public:
	virtual HRESULT Initialize_Scene();
	virtual short KeyEvent(double TimeDelta);
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();

private:
	CLoading* m_pLoading = nullptr;

	CManagement* m_pManagement = nullptr;

	XMFLOAT3 m_xmCameraPos{ -6.13, 6.45, -22.67 };
	XMFLOAT3 m_xmLookAtPos{ -6.13, 5.22, 4.03 };

	XMFLOAT3 m_xmPlayerPos[3] = { XMFLOAT3(-8.358,-0.2054321,0.97), XMFLOAT3(5.789146,-0.2054321,0.97), XMFLOAT3(-3.145562,-0.2054321,0.97)};

	CStaticObject* m_pScreen;
	CStaticObject* m_pWinScreen;

	CStaticObject* m_pDualIcon[2];
	CStaticObject* m_pDualDice[2];

	CStaticObject* m_pVs;

	CStaticObject* m_pFloor;

	CFont* pWinnerText;

	CFont* pDualText[2];

	CTeddyBear* m_pTeddyBear;
	CTeddyBear* m_pTeddyBear2;
	CTeddyBear* m_pTeddyBear3;
	CTeddyBear* m_pTeddyBear4;
	CTeddyBear* m_pTeddyBear5;
	CTeddyBear* m_pSantaTeddyBear;
	CTeddyBear* m_pActor;
	CGhost* m_pGhost;
	CBird* m_pBird;

	double m_pFrameTime = 0.f;
	double m_dDualTime = 0.f;

	int m_pScreenNum = 0;

	CPlayer* m_pDualPlayer[2] = { nullptr, };
	CPlayer* m_pWinPlayer = nullptr;
	bool m_bStartAsk = false; //시작 요청
	int m_iDualNum[2] = { 0, }; // 두명의 주사위 숫자
	bool isDualPlayer = false; // 듀얼리스트임?
	short prizeOrder = -1;

	bool m_bTeleCamera = true;
public:
	static CScene_Prize* Create();
	HRESULT Release();

};

#endif