#ifndef __SCENE__
#define __SCENE__

#include "stdafx.h"
class CGameObject;

class CScene
{
protected:
	explicit CScene();
	virtual ~CScene();

public:
	virtual HRESULT Initialize_Scene();
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();
	virtual short KeyEvent(double TimeDelta);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);
public:
	HRESULT Release();
	SCENEID Get_SceneId() { return m_SceneId; }
	void Set_SceneId(SCENEID sceneId) { m_SceneId = sceneId; }

protected:
	SCENEID m_SceneId;

};

#endif