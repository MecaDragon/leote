#include "FontShader.h"

CFontShader::CFontShader()
	: CShader()
{
}

CFontShader::CFontShader(const CFontShader& rhs)
	: CShader(rhs)
{
	m_pPipelineState->AddRef();
}

CFontShader::~CFontShader(void)
{
}

HRESULT CFontShader::Initialize_Component_Prototype()
{
    m_pPipelineState = CShader::Create_PipeLineState();
	m_pPipelineState->SetName(L"Font Shader PipelineState");
	return NOERROR;
}

HRESULT CFontShader::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CFontShader::Clone_Component(void* pArg)
{
	CFontShader* pInstance = new CFontShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CFontShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CFontShader* CFontShader::Create()
{
	CFontShader* pInstance = new CFontShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CFontShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

D3D12_BLEND_DESC CFontShader::Create_BlendState()
{
	D3D12_BLEND_DESC d3dBlendDesc;
	::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
	d3dBlendDesc.AlphaToCoverageEnable = FALSE;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = TRUE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	//d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(d3dBlendDesc);
}

D3D12_SHADER_BYTECODE CFontShader::Create_VertexShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/FontText.hlsl", "VSTextFont", "vs_5_1", ppBlob));
}

D3D12_SHADER_BYTECODE CFontShader::Create_PixelShader(ID3DBlob** ppBlob)
{
	return (CShader::CompileShaderFromFile(L"../Data/Shaderfiles/FontText.hlsl", "PSTextFont", "ps_5_1", ppBlob));
}

D3D12_INPUT_LAYOUT_DESC CFontShader::Create_InputLayout()
{
    UINT nInputElementDescs;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

	nInputElementDescs = 3;
	pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[2] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

HRESULT CFontShader::Release()
{
    if(m_pPipelineState)
        m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
