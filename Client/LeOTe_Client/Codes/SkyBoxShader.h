#ifndef __SKYBOXSHADER__
#define __SKYBOXSHADER__

#include "Shader.h"

class CSkyBoxShader final : public CShader
{
private:
	explicit CSkyBoxShader();
	explicit CSkyBoxShader(const CSkyBoxShader& rhs);
	virtual ~CSkyBoxShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CSkyBoxShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();
	virtual D3D12_RASTERIZER_DESC Create_RasterizerState();

};

#endif
