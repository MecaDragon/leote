#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "FbxLoader.h"

class CShader;
class CRenderer;
class CTransform;
class CMesh;
class CTexture;
class CMaterial;
class CShadowShader;

class CTeddyBear : public CGameObject
{
protected:
	explicit CTeddyBear();
	explicit CTeddyBear(const CTeddyBear& rhs);
	virtual ~CTeddyBear() = default;
public:
	virtual HRESULT Initialize_GameObject_Prototype(); // 원형객체 생성 시 호출.
	virtual HRESULT Initialize_GameObject(void* pArg); // 복사객체 생성 시 호출.
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_Shadow();
	virtual HRESULT Render_GameObject();
	void ChangeAnimation(string sClipName);
	bool IsEndAnimation() { return m_aimationEnd; }
	bool IsMiddleAnimation() { return m_aimationMiddle; }

public:
	CTransform* m_pTransformCom = nullptr;
	CMaterial* m_pMaterialCom = nullptr;
	CShadowShader* m_pShadowShaderCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	BoundingBox m_xmBoundingBox;
	float TimeDelta = 0.01f;

	std::string mClipName;
	std::string mNextClipName;
	int m_iSkinid;
protected:
	//CRenderer*			m_pRendererCom = nullptr;
	CShader* m_pShaderCom = nullptr;
	CMesh* m_pMeshCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CTexture* m_pNormalTextureCom = nullptr;

	FbxScene* m_pfbxScene = NULL;
	SkinnedData mSkinnedInfo;
	std::unique_ptr<SkinnedModelInstance> mSkinnedModelInst;

	ID3D12Resource* m_pGameObjResource = nullptr;
	CharacterConstants* skinnedConstants = nullptr;
	std::vector<CharacterVertex> outSkinnedVertices;
	std::vector<std::uint32_t> outIndices;
	std::vector<Material> outMaterial;

	ID3D12Resource* m_pd3dVertexBuffer = NULL;
	ID3D12Resource* m_pd3dVertexUploadBuffer = NULL;

	ID3D12Resource* m_pd3dIndexBuffer = NULL;
	ID3D12Resource* m_pd3dIndexUploadBuffer = NULL;

	D3D12_VERTEX_BUFFER_VIEW		m_d3dVertexBufferView;
	D3D12_INDEX_BUFFER_VIEW			m_d3dIndexBufferView;

	D3D12_PRIMITIVE_TOPOLOGY		m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT							m_nSlot = 0;
	UINT							m_nVertices = 0;
	UINT							m_nStride = 0;
	UINT							m_nOffset = 0;

	UINT							m_nIndices = 0;
	UINT							m_nStartIndex = 0;
	int								m_nBaseVertex = 0;


	bool m_aimationEnd = false;
	bool m_aimationMiddle = false;

	double m_dRunSoundDelta = 0.0;
	bool m_bJumpSound = false;
private:
	HRESULT Add_Component(); // 각 객체가 가지고 있어야할 컴포넌트를 추가하는 기능.
	HRESULT SetUp_ConstantTable();
public:
	static CTeddyBear* Create();
	virtual CGameObject* Clone_GameObject(void* pArg);
	HRESULT Release();
};