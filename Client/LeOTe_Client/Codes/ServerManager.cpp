#include "stdafx.h"
#include "Defines.h"
#include "ServerManager.h"
#include "TeddyBear.h"
#include "SantaTeddyBear.h"
#include "Management.h"
#include "Player.h"
#include "Camera_Third.h"
#include "ObjectManager.h"
#include "CameraManager.h"
#include "Camera_Fixed.h"
#include "Chicken.h"
#include "SoundManager.h"

extern bool G_SC;
extern string G_IP;

CPlayer* Player;
CPlayer* Players[3];

IMPLEMENT_SINGLETON(CServerManager)

CServerManager::~CServerManager()
{

}

void CServerManager::err_quit(const WCHAR* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}

void CServerManager::err_display(const WCHAR* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	if (SERVERCONNECT)
	{
		cout << "[" << msg << "]" << (char*)lpMsgBuf << endl;
	}
	LocalFree(lpMsgBuf);
}

int CServerManager::WSAStart()
{
	m_pManagement = CManagement::GetInstance();

	ClientIndex = -1;
	minigameReady = false;
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(1, 1), &wsa) != 0)
		return 1;
	return 0;
}

void CServerManager::CreateSocket()
{
	//Non-Blocking
	sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, 0);
	if (sock == INVALID_SOCKET)
		err_quit(L"socket()");

	unsigned long noblock = 1;
	if (ioctlsocket(sock, FIONBIO, &noblock) == SOCKET_ERROR)
	{
		cout << "ioctlsocket err" << endl;
	}
}

int CServerManager::ConnectToServer()
{
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	//serveraddr.sin_addr.s_addr = inet_addr(serverip);
	serveraddr.sin_port = htons(SERVERPORT);
	inet_pton(AF_INET, SERVERIP.c_str(), &serveraddr.sin_addr);

	int retval = 0;
	retval = connect(sock, (struct sockaddr FAR*) & serveraddr, sizeof(serveraddr));

	if (retval == 0)
		goto success;

	//0반환시 연결 성공 그러나..
	if (retval != 0)
	{
		int err = WSAGetLastError(); //가장 최근에 발생한 소켓 함수 에러를 알려줌
		if (err != EINPROGRESS && err != EWOULDBLOCK && err != WSAEWOULDBLOCK) {
			cout << "connect() Failed : " << err << endl;
			return -1;
		}

		fd_set rset, wset;
		FD_ZERO(&rset);
		timeval timeout = { 5, 0 }; //5초 동안 기다려본다. 연결이 됐는지.
		FD_SET(sock, &rset);
		wset = rset;

		int result = select(1, &rset, &wset, NULL, 5 ? &timeout : NULL);

		if (result == 0) { //성공 시 준비된 fd 개수 반환
			cout << "select / connect() Failed : timeout" << WSAGetLastError() << endl;
			return -1;
		}
		else if (result < 0)
		{
			cout << "select / connect() Failed : err" << WSAGetLastError() << endl;
			return -1;
		}

		err = 0;
		socklen_t len = sizeof(err);
		if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)&err, &len) < 0 || err != 0) {
			cout << "getsockopt() error: " << err << endl;
			return -1;
		}
	}
success:

	return 0;
}

void CServerManager::SetServerIP(string strIP)
{
	strcpy_s(serverip, strIP.c_str());
	m_eLoginState = LOGIN_STATE::ID;
}

void CServerManager::SetServerID(string strID)
{
	strcpy_s(serverid, strID.c_str());
}

void CServerManager::SendData(char datainfo, LPVOID packet)
{
	switch (datainfo)
	{
	case C2S_LOGIN:
	{
		packet_login p = *(packet_login*)packet;
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_LOGIN send() Failed");
			return;
		}
	}
	break;
	case C2S_WAITROOMENTER:
	{

	}
	break;
	case C2S_MATCHINGSTART:
	{
		packet_matching p = *reinterpret_cast<packet_matching*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_MATCHINGSTART send() Failed");
			return;
		}
	}
	break;
	case C2S_ROOMOUT:
	{

	}
	break;
	case C2S_MOVE:
	{
		packet_move p = *reinterpret_cast<packet_move*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_MOVE send() Failed");
			return;
		}
	}
	break;
	case C2S_LEAVEPLAYER:
	{
		packet_leave p = *reinterpret_cast<packet_leave*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_LEAVEPLAYER send() Failed");
			return;
		}
	}
	break;
	case C2S_BOARDMAPREADY:
	{
		packet_boardmap_ready p = *reinterpret_cast<packet_boardmap_ready*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_BOARDMAPREADY send() Failed");
			return;
		}
	}
	break;
	case C2S_ORDER:
	{

	}
	break;
	case C2S_JUMPDICE:
	{
		packet_jumpdice p = *reinterpret_cast<packet_jumpdice*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_BOARDMAPREADY send() Failed");
			return;
		}
	}
	break;
	case C2S_DICEMOVEEND:
	{
		packet_dicemoveend p = *reinterpret_cast<packet_dicemoveend*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_DICEMOVEEND send() Failed");
			return;
		}
	}
	break;
	case C2S_CURRENTORDER:
	{
		packet_currentorder p = *reinterpret_cast<packet_currentorder*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_CURRENTORDER send() Failed");
			return;
		}
	}
	break;
	case C2S_DICEENTER:
	{
		packet_diceenter p = *reinterpret_cast<packet_diceenter*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_DICEENTER send() Failed");
			return;
		}
	}
	break;
	case C2S_COINEVENT:
	{
		packet_coin p = *reinterpret_cast<packet_coin*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_COINEVENT send() Failed");
			return;
		}
	}
	break;
	case C2S_BUTTONEVENT:
	{
		packet_button p = *reinterpret_cast<packet_button*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_BUTTONEVENT send() Failed");
			return;
		}
	}
	break;
	case C2S_DICENUMDOWN:
	{
		packet_dicenumdown p = *reinterpret_cast<packet_dicenumdown*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_DICENUMDOWN send() Failed");
			return;
		}
	}
	case C2S_PLAYERSCOL:
	{
		packet_playercollision p = *reinterpret_cast<packet_playercollision*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_PLAYERSCOL send() Failed");
			return;
		}
	}
	break;
	case C2S_SCENECHANGE:
	{
		packet_scenechange p = *reinterpret_cast<packet_scenechange*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_SCENECHANGE send() Failed");
			return;
		}
	}
	break;
	//falling
	case C2S_FALLINGMAPREADY:
	{

	}
	break;
	case C2S_ANIMATION:
	{
		packet_animation p = *reinterpret_cast<packet_animation*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_ANIMATION send() Failed");
			return;
		}
	}
	break;
	case C2S_DELETEBLOCK:
	{
		packet_deleteblock p = *reinterpret_cast<packet_deleteblock*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_DELETEBLOCK send() Failed");
			return;
		}
	}
	break;
	case C2S_PLAYERDEAD:
	{
		packet_playerdead p = *reinterpret_cast<packet_playerdead*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_PLAYERDEAD send() Failed");
			return;
		}
	}
	break;
	case C2S_MINIGAMELOADING:
	{
		packet_loadingsuccess p = *reinterpret_cast<packet_loadingsuccess*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_MINIGAMELOADING send() Failed");
			return;
		}
	}
	break;
	case C2S_ONEMINDDIR:
	{
		packet_onemindlook p = *reinterpret_cast<packet_onemindlook*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_ONEMINDDIR send() Failed");
			return;
		}
	}
	break;
	case C2S_ONEMINDLIGHT:
	{
		packet_onemindlight p = *reinterpret_cast<packet_onemindlight*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_ONEMINDLIGHT send() Failed");
			return;
		}
	}
	break;
	case C2S_ONEMINDMOVE:
	{
		packet_onemindmove p = *reinterpret_cast<packet_onemindmove*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_ONEMINDMOVE send() Failed");
			return;
		}
	}
	break;
	case C2S_ONEMINDANIM:
	{
		packet_animation p = *reinterpret_cast<packet_animation*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_ONEMINDANIM send() Failed");
			return;
		}
	}
	break;
	case C2S_SHOWMAP:
	{
		packet_showmap p = *reinterpret_cast<packet_showmap*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_SHOWMAP send() Failed");
			return;
		}
	}
	break;
	case C2S_STARTTIMER:
	{
		packet_starttimeask p = *reinterpret_cast<packet_starttimeask*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_STARTTIMER send() Failed");
			return;
		}
	}
	break;
	case C2S_MATCHTIE:
	{
		packet_matchtie p = *reinterpret_cast<packet_matchtie*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_MATCHTIE send() Failed");
			return;
		}
	}
	break;
	case C2S_EVENT:
	{
		packet_event p = *reinterpret_cast<packet_event*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_EVENT send() Failed");
			return;
		}
	}
	break;
	case C2S_EVENTSELECT:
	{
		packet_eventselect p = *reinterpret_cast<packet_eventselect*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_EVENTSELECT send() Failed");
			return;
		}
	}
	break;
	case C2S_ITEMUSE:
	{
		packet_itemuse p = *reinterpret_cast<packet_itemuse*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_ITEMUSE send() Failed");
			return;
		}
	}
	break;
	case C2S_TESTTIERANK:
	{
		packet_testtie p = *reinterpret_cast<packet_testtie*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_TESTTIERANK send() Failed");
			return;
		}
	}
	break;
	case C2S_SPECIALEVENT:
	{
		packet_specialevent p = *reinterpret_cast<packet_specialevent*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_SPECIALEVENT send() Failed");
			return;
		}
	}
	break;
	case C2S_TELEPORT:
	{
		packet_teleport p = *reinterpret_cast<packet_teleport*>(packet);
		int retval = send(sock, (char*)&p, p.size, 0);
		if (retval == SOCKET_ERROR)
		{
			err_display(L"C2S_TELEPORT send() Failed");
			return;
		}
	}
	break;
	default:
		break;
	}
}

void CServerManager::RecvPacket()
{
	char buf[MAX_BUF_SIZE];
	char* packet = buf;
	int received = 0;
	int left = 2; // 처음에 size, type를 미리 받기 위함

	bool init_come = true;

	while (left > 0 || init_come)
	{
		received = recv(sock, packet, left, 0);
		if (received == SOCKET_ERROR)//에러 발생
			return;
		else if (received == 0) //받은게 없음
			break;
	
		if (init_come)
		{
			left = buf[0] - received;
			packet += received;
			init_come = false;
		}
		else
		{
			left -= received;
			packet += received;
	
			if (left == 0) {
				Process_Packet(buf);
				return;
			}
		}
	}

	//while (true) {
	//	received = recv(sock, packet, MAX_BUF_SIZE, 0);
	//	if (received == SOCKET_ERROR)//에러 발생
	//		return;
	//	else if (received == 0) //받은게 없음
	//		break;

	//	if (init_come)
	//	{
	//		left = buf[0] - received;
	//		packet += received;
	//		init_come = false;
	//	}
	//	else
	//	{
	//		left -= received;
	//		packet += received;
	//	}

	//	if (left == 0) {
	//		Process_Packet(buf);
	//		return;
	//	}
	//}
}

void CServerManager::Process_Packet(char* buf)
{
	//받은 패킷에 대한 처리
	switch (buf[1])
	{
	case S2C_SCENECHANGE:
	{
		packet_scenechange p = *reinterpret_cast<packet_scenechange*>(buf);
		switch (p.map)
		{
		case S_FALLINGMAP:
		{
			for (int i = 0; i < 3; ++i)
			{
				if (Players[i] != nullptr)
				{
					if (Players[i]->Get_ClientIndex() == p.id) {
						Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(p.x, p.y, p.z);
						return;
					}
				}
			}
		}
		break;
		case S_BOARDMAP:
		{
			++boardmapReady;
			starttime = 3;

			if (boardmapReady == 3) {
				InitChickens();
			}

			for (int i = 0; i < 3; ++i)
			{
				if (Players[i] != nullptr)
				{
					if (Players[i]->Get_ClientIndex() == p.id) {
						Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(p.x, p.y, p.z);
						Players[i]->m_tPlayerInfo.xmLook = XMFLOAT3(p.lookx, p.looky, p.lookz);
						Players[i]->SetDeath(false);
						return;
					}
				}
			}
		}
		break;
		case S_GGOGGOMAP:
		{
			minigame_playtime = 0;

			for (int i = 0; i < 3; ++i)
			{
				if (Players[i] != nullptr)
				{
					if (Players[i]->Get_ClientIndex() == p.id) {
						Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(p.x, p.y, p.z);
						return;
					}
				}
			}
		}
		break;
		case S_BULLFIGHTMAP:
		{
			for (int i = 0; i < 3; ++i)
			{
				if (Players[i] != nullptr)
				{
					if (Players[i]->Get_ClientIndex() == p.id) {
						Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(p.x, p.y, p.z);
						return;
					}
				}
			}
		}
		break;
		case S_ONEMINDMAP:
		{
			minigame_playtime = 0;
			batteryhp = 90;
		}
		break;
		case S_PRIZEMAP:
		{

		}
		break;
		}
	}
	break;
	case S2C_LOGIN_OK:
	{
		packet_login_result p = *reinterpret_cast<packet_login_result*>(buf);
	
		ClientIndex = p.id; //내 서버상 고유 클라이언트 인덱스
		m_eLoginState = LOGIN_STATE::END;

		CCamera_Third* pCameraThird = CCamera_Third::Create();
		CTeddyBear* pTeddyBear = nullptr;

		if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_ALL, L"Layer_Player", L"GameObject_Player", (CGameObject**)&Player)))
			MSG_BOX("Player Create Fail");

		if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_ALL, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&pTeddyBear)))
			MSG_BOX("TeddyBear Create Fail");

		::Player->SetObject(pTeddyBear);

		string str = serverid;
		Player->Set_ClientIndex(ClientIndex);
		Player->Set_Name(str);
		Player->bCoin = 20;
		Player->bButton = 0;
		Player->SetIsPlayer(true);

		CCameraManager::GetInstance()->Add_Camera(pCameraThird, "Camera_Third");

		pCameraThird->SetPlayer(Player);
		pCameraThird->SetOffCameraSet(XMFLOAT3(0.0f, 7.2f, -12.0f));
		Player->SetCamera(pCameraThird);
		Player->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.f, 0.f);
		Players[0] = Player;
	}
	break;
	case S2C_LOGIN_FAIL:
	{
		ZeroMemory(serverid, sizeof(char) * MAX_NAME_LEN);
		m_bSendID = false;
	}
	break;
	case S2C_ENTERPLAYER:
	{
		++enterCount;

		packet_enter p = *reinterpret_cast<packet_enter*>(buf);

		CPlayer* pPlayer = nullptr;
		CTeddyBear* pTeddyBear = nullptr;

		if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_STATIC, L"Layer_Player", L"GameObject_Player", (CGameObject**)&pPlayer)))
			MSG_BOX("Player Create Fail");

		if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_STATIC, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&pTeddyBear)))
			MSG_BOX("TeddyBear Create Fail");

		pPlayer->SetObject(pTeddyBear);

		string str = p.name;

		pPlayer->Set_ClientIndex(p.id);
		pPlayer->bButton = 0;
		pPlayer->bCoin = 20;
		pPlayer->Set_Name(str);
		pPlayer->SetSkinid(p.skinid);
		int initpos = enterCount % 2;
		switch (initpos)
		{
		case 0:
		{
			pPlayer->m_tPlayerInfo.xmPosition = XMFLOAT3(-8.5f,-13.6f, -80.f);
		}
		break;
		case 1:
		{
			pPlayer->m_tPlayerInfo.xmPosition = XMFLOAT3(-9.5f, -13.6f, -80.f);
		}
		break;
		}

	}
	break;
	case S2C_LEAVEPLAYER: //게임 중에 나갔으면 대기실로 돌아가기
	{
		//WaitRoom으로 가라
		disconnect = true;
	}
	break;
	case S2C_ROOMCREATE:
	{

	}
	break;
	case S2C_ROOMENTER:
	{

	}
	break;
	case S2C_ROOMOUT:
	{

	}
	break;
	case S2C_MOVE:
	{
		packet_move p = *reinterpret_cast<packet_move*>(buf);

		CLayer* Layer = CObjectManager::GetInstance()->Find_Layer(SCENE_STATIC, L"Layer_Player");
		if (Layer == nullptr)
			return;

		Layer->Update_PlayerPosition(p);
	}
	break;
	case S2C_BOARDMAPREADY:
	{

	}
	break;
	case S2C_ORDER:
	{
		packet_order p = *reinterpret_cast<packet_order*>(buf);

		if (Players[0] != nullptr) {
			if (p.id == Players[0]->Get_ClientIndex()) {
				Players[0]->SetPlayerOrder(p.order);
				Players[0]->m_tPlayerInfo.xmPosition = XMFLOAT3{ p.x,p.y,p.z };
			}
		}

		CLayer* Layer = CObjectManager::GetInstance()->Find_Layer(SCENE_STATIC, L"Layer_Player");
		if (Layer == nullptr)
			return;

		Layer->Set_PlayerOrder(p, Players);
		playerindex = 3;

		//첫번째 순서 플레이어로 카메라 변경
		CCamera* cam = CCameraManager::GetInstance()->Find_Camera("Camera_Third");

		for (int i = 0; i < 3; ++i)
		{
			if (currentorder == Players[i]->GetPlayerOrder()) {
				dynamic_cast<CCamera_Third*>(cam)->SetPlayer(Players[i]);
				pCurPlayer = Players[i];
			}
		}
		ableNickanmeInit = true;
		prevorder = currentorder;
	}
	break;
	case S2C_JUMPDICE:
	{
		packet_jumpdice p = *reinterpret_cast<packet_jumpdice*>(buf);
		dicenum = p.dicenum;
	}
	break;
	case S2C_DICEMOVEEND:
	{
		packet_dicemoveend p = *reinterpret_cast<packet_dicemoveend*>(buf);

		for (int i = 0; i < 3; ++i) {
			if (Players[i]->Get_ClientIndex() == p.id)
			{
				Players[i]->Set_Lerp(false);
				break;
			}
		}
	}
	break;
	case S2C_CURRENTORDER:
	{
		packet_currentorder p = *reinterpret_cast<packet_currentorder*>(buf);
		currentorder = p.currentorder;

		for (int i = 0; i < 3; ++i)
		{
			if (p.id == Players[i]->Get_ClientIndex())
			{
				Players[i]->Set_CurrentPlatformIndex(p.currentplatform);
			}
		}
	}
	break;
	case S2C_DICEENTER:
	{
		packet_diceenter p = *reinterpret_cast<packet_diceenter*>(buf);
		diceenter = true;
	}
	break;
	case S2C_COINEVENT:
	{
		packet_coin p = *reinterpret_cast<packet_coin*>(buf);
		for (int i = 0; i < 3; ++i) {
			if (Players[i]->Get_ClientIndex() == p.id)
			{
				if (p.add)
				{
					Players[i]->bCoin = p.num;
					coinevent = 1;
				}
				else
				{
					Players[i]->bCoin = p.num;
					coinevent = 2;
				}
				break;
			}
		}
	}
	break;
	case S2C_BUTTONEVENT:
	{
		packet_button p = *reinterpret_cast<packet_button*>(buf);
		for (int i = 0; i < 3; ++i) {
			if (Players[i]->Get_ClientIndex() == p.id)
			{
				if (p.add)
					Players[i]->bButton = p.num;
				else
					Players[i]->bButton = p.num;
				break;
			}
		}
	}
	break;
	case S2C_DICENUMDOWN:
	{
		packet_dicenumdown p = *reinterpret_cast<packet_dicenumdown*>(buf);
		dicenum = p.num;
	}
	break;
	case S2C_PLAYERCOL:
	{
		packet_playercollision p = *reinterpret_cast<packet_playercollision*>(buf);
		for (int i = 0; i < 3; ++i)
		{
			if (p.id == Players[i]->Get_ClientIndex())
			{
				Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(p.shiftx, p.shifty, p.shiftz);
				return;
			}
		}
	}
	break;
	//falling
	case S2C_FALLINGMAPREADY:
	{

	}
	break;
	case S2C_ANIMATION:
	{
		packet_animation p = *reinterpret_cast<packet_animation*>(buf);
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] == nullptr)
				continue;
			if (p.id == Players[i]->Get_ClientIndex())
			{
				Players[i]->SetAnimationState(p.anim);
				return;
			}
		}
	}
	break;
	case S2C_DELETEBLOCK:
	{
		packet_deleteblock p = *reinterpret_cast<packet_deleteblock*>(buf);
		blockcolor = p.blockcolor;
		blockid = p.blockid;
	}
	break;
	case S2C_PLAYERDEAD:
	{
		packet_playerdead p = *reinterpret_cast<packet_playerdead*>(buf);
		deathplayer = p.id;
	}
	break;
	case S2C_MINIGAMEEND:
	{
		packet_minigameend p = *reinterpret_cast<packet_minigameend*>(buf);
		minigamewinner = p.id;
	}
	break;
	case S2C_CHICKENINIT:
	{
		packet_Initchicken p = *reinterpret_cast<packet_Initchicken*>(buf);
		CGameObject* pChick = nullptr;
		chickennum = 20;
		if (FAILED(CManagement::GetInstance()->Add_GameObjectToLayer(SCENE_GGOGGOMAP, L"Layer_BackGround", L"GameObject_Chicken", &pChick, &p)))
			break;

		Chickens[createchickens++] = dynamic_cast<CChicken*>(pChick);

		if (createchickens == 20)
		{
			packet_starttimeask ps;
			ps.size = sizeof(packet_starttimeask);
			ps.type = C2S_STARTTIMER;
			ps.id = Player->Get_ClientIndex();

			SendData(C2S_STARTTIMER, &ps);	
		}
	}
	break;
	case S2C_CHICKEN:
	{
		packet_chicken p = *reinterpret_cast<packet_chicken*>(buf);

		if (p.state == C_ESCAPE) {
			escapeUI = true;
			--chickennum;

			wstring chick = L"chicken";
			int randstep = rand() % 4; //0 1 2 3 
			chick += to_wstring(randstep);
			CSoundManager::GetInstance()->Play_Sound(chick);
		}
		//얘에 대한 처리
		if (Chickens[p.c_id] != nullptr)
		{
			Chickens[p.c_id]->SetLerpStart(p);			
		}

	}
	break;
	case S2C_CHICKENSTATE:
	{
		packet_chickenstate p = *reinterpret_cast<packet_chickenstate*>(buf);

		if (Chickens[p.c_id] != nullptr)
		{
			Chickens[p.c_id]->SetChickenState(p.state);
		}
	}
	break;
	case S2C_TIMESECONDS:
	{
		packet_timeseconds p = *reinterpret_cast<packet_timeseconds*>(buf);
		minigame_playtime = p.seconds; //흐른 초(시간)
	}
	break;
	case S2C_MINIGAMELOADING:
	{
		packet_loadingsuccess p = *reinterpret_cast<packet_loadingsuccess*>(buf);
		minigameReady = true;
	}
	break;
	case S2C_POTION:
	{
		packet_potion p = *reinterpret_cast<packet_potion*>(buf);
		potionPosition = XMFLOAT3(p.x, p.y, p.z);
		potionCreated = true;
	}
	break;
	case S2C_BIGGER:
	{
		packet_scale p = *reinterpret_cast<packet_scale*>(buf);

		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] == nullptr)
				continue;
			if (p.id == Players[i]->Get_ClientIndex())
			{
				//커지는 플레이어 
				Players[i]->SetBig(true);
				Set_PotionCreate(false);
				CSoundManager::GetInstance()->Play_Sound_3D(L"PotionDrink", Players[i]->m_tPlayerInfo.xmPosition);
				CSoundManager::GetInstance()->Play_Sound_3D(L"MagicCircle", Players[i]->m_tPlayerInfo.xmPosition);

				break;
			}
		}
	}
	break;
	case S2C_SMALLER:
	{
		packet_scale p = *reinterpret_cast<packet_scale*>(buf);
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] == nullptr)
				continue;
			if (p.id == Players[i]->Get_ClientIndex())
			{
				//작아지는 플레이어 
				Players[i]->SetSmall(true);
			}
		}
	}
	break;
	case S2C_PUNCH:
	{
		packet_punch p = *reinterpret_cast<packet_punch*>(buf);
		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] == nullptr)
				continue;
			if (p.id == Players[i]->Get_ClientIndex())
			{
				//충돌한 플레이어 -> 충돌했으면 키입력 막고 shift값 0으로 해주기
				if (Players[i]->GetDeath() == false) {
					Players[i]->SetPlayerForce(0.f, 700.f, 0.f);
					Players[i]->SetDeath(true);
					if (ClientIndex == p.id) {
						CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
						CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));
						pCameraFixed->settingCamera(XMFLOAT3(-2.17, 56.23, 1.14), XMFLOAT3(-2.17, 0.f, 1.14), XMFLOAT3(0.f, 0.f, 1.f));
					}
				}
			}
		}
	}
	break;
	case S2C_ROLL:
	{
		++loadnum;
		//역할 보내기
		packet_roll p = *reinterpret_cast<packet_roll*>(buf);

		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] == nullptr)
				continue;

			if (p.id == Players[i]->Get_ClientIndex())
			{
				Players[i]->Set_OneMindRoll(p.roll);
			}

		}

		if (loadnum == 3)//3명의 역할이 모두 지정됨 
		{
			//로딩완료 패킷 보내기
			packet_starttimeask ps;
			ps.size = sizeof(packet_starttimeask);
			ps.type = C2S_STARTTIMER;
			ps.id = Player->Get_ClientIndex();

			SendData(C2S_STARTTIMER, &ps);

			loadnum = 0;
			snowman[0] = false;
			snowman[1] = false;
		} 
	}
	break;
	case S2C_ONEMINDDIR: //lookx, looky, lookz
	{
		packet_onemindlook dir = *reinterpret_cast<packet_onemindlook*>(buf);
		lookon = true;

		robotlook = XMFLOAT3(dir.lookx, dir.looky, dir.lookz);
	}
	break;
	case S2C_ONEMINDLIGHT: //y
	{
		packet_onemindlight l = *reinterpret_cast<packet_onemindlight*>(buf);
		lighton = l.light;
	}
	break;
	case S2C_ONEMINDMOVE: // x,z
	{
		packet_onemindmove move = *reinterpret_cast<packet_onemindmove*>(buf);
		positionon = true;

		robotposition = XMFLOAT3(move.mx, move.my, move.mz);
	}
	break;
	case S2C_BATTERY:
	{
		packet_onemindbattery b = *reinterpret_cast<packet_onemindbattery*>(buf);
		batteryhp = b.hp;
	}
	break;
	case S2C_ONEMINDANIM:
	{
		packet_animation p = *reinterpret_cast<packet_animation*>(buf);
		santaAnim = p.anim;
	}
	break;
	case S2C_ONEMINDRESPAWN:
	{
		packet_respawn p = *reinterpret_cast<packet_respawn*>(buf);
		robotrespawnon = true;

		robotRespawnPos = XMFLOAT3(p.x, p.y, p.z);
	}
	break;
	case S2C_ONEMINDSAVE:
	{
		packet_savepoint p = *reinterpret_cast<packet_savepoint*>(buf);
		snowman[p.savepoint - 1] = true;
	}
	break;
	case S2C_MATCHING:
	{
		matching = true;
	}
	break;
	case S2C_BOARDMAPRANK:
	{
		//0-동점자 없음 1-1등2등 동점 2- 2등3등 동점 3- 1등 2등 3등 동점
		packet_rank p = *reinterpret_cast<packet_rank*>(buf);
		for (int i = 0; i < 3; ++i) {
			switch (p.matchtie)
			{
			case 0:
			{
				if (Players[i] == nullptr)
					continue;
				if (Players[i]->Get_ClientIndex() == p.id1)
					Players[i]->bRank = 1;
				else if (Players[i]->Get_ClientIndex() == p.id2)
					Players[i]->bRank = 2;
				else if (Players[i]->Get_ClientIndex() == p.id3)
					Players[i]->bRank = 3;
			}
				break;
			case 1:
			{
				if (Players[i] == nullptr)
					continue;
				if (Players[i]->Get_ClientIndex() == p.id1)
					Players[i]->bRank = 1;
				else if (Players[i]->Get_ClientIndex() == p.id2)
					Players[i]->bRank = 1;
				else if (Players[i]->Get_ClientIndex() == p.id3)
					Players[i]->bRank = 3;
			}
				break;
			case 2:
			{
				if (Players[i] == nullptr)
					continue;
				if (Players[i]->Get_ClientIndex() == p.id1)
					Players[i]->bRank = 1;
				else if (Players[i]->Get_ClientIndex() == p.id2)
					Players[i]->bRank = 2;
				else if (Players[i]->Get_ClientIndex() == p.id3)
					Players[i]->bRank = 2;
			}
				break;
			case 3:
			{
				if (Players[i] == nullptr)
					continue;
				if (Players[i]->Get_ClientIndex() == p.id1)
					Players[i]->bRank = 1;
				else if (Players[i]->Get_ClientIndex() == p.id2)
					Players[i]->bRank = 1;
				else if (Players[i]->Get_ClientIndex() == p.id3)
					Players[i]->bRank = 1;
			}
				break;
			default:
				break;
			}
			
		}
	}
	break;
	case S2C_SHOWMAP:
	{
		packet_showmap p  = *reinterpret_cast<packet_showmap*>(buf);
		showmap = p.show;
	}
	break;
	case S2C_STARTTIMER:
	{
		packet_starttimer p = *reinterpret_cast<packet_starttimer*>(buf);
		starttime = p.time;
	}
	break;
	case S2C_TIEPLAYER:
	{
		packet_istied p = *reinterpret_cast<packet_istied*>(buf);
		isTied = p.tied;
		if (p.tied == 1) //동점자 있음
		{
			tiedPlayer[0] = p.id1;
			tiedPlayer[1] = p.id2;
		}
	}
	break;
	case S2C_MATCHTIE:
	{
		//동점자 주사위 
		packet_matchtie p = *reinterpret_cast<packet_matchtie*>(buf);
		//동점자 id, 주사위 숫자 보내진 것 반영하기
		matchTieInfo = true;
		matchTiedPlayers.index = p.id;
		matchTiedPlayers.dicenum = p.dicenum;
	}
	break;
	case S2C_MINIGAMENDCOIN:
	{
		packet_coin p = *reinterpret_cast<packet_coin*>(buf);
		for (int i = 0; i < 3; ++i) {
			if (Players[i]->Get_ClientIndex() == p.id)
			{
				if (p.add)
				{
					Players[i]->bCoin = p.num;
				}
				else
				{
					Players[i]->bCoin = p.num;
				}
				break;
			}
		}
	}
	break;
	case S2C_EVENT:
	{
		packet_event p = *reinterpret_cast<packet_event*>(buf);
		boardmap_event = p.E_event;
	}
	break;
	case S2C_EVENTSELECT:
	{
		packet_eventselect p = *reinterpret_cast<packet_eventselect*>(buf);
		if (p.buy)
			itembuy = 1;
		else
			itembuy = 2;
		eItem = ITEM(p.item);

		for (int i = 0; i < 3; ++i) {
			if (Players[i]->Get_ClientIndex() == p.id)
			{
				if (itembuy == 1)
				{
					switch (eItem)
					{
					case ITEM_MINIDICE:
					{
						Players[i]->bCoin = p.coin;

					}
						break;
					case ITEM_BUTTON:
					{
						Players[i]->bCoin = p.coin;
						Players[i]->bButton = p.button;
					}
						break;
					case ITEM_PLUS3:
					{
						Players[i]->bCoin = p.coin;

					}
						break;
					case ITEM_PLUS5:
					{
						Players[i]->bCoin = p.coin;

					}
						break;
					case ITEM_END:
						break;
					default:
						break;
					}
				}
				break;
			}
		}
	}
	break;
	case S2C_ITEMUSE:
	{
		packet_itemuse p = *reinterpret_cast<packet_itemuse*>(buf);
		itemUse = true;
		usedItem = ITEM(p.item);
		miniDice = p.dice;
		inventoryIndex = p.invenindex;
		usedPlayerIndex = p.id;
	}
	break;
	case S2C_TESTTIERANK:
	{
		packet_testtie p = *reinterpret_cast<packet_testtie*>(buf);

		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] == nullptr)
				continue;

			if (Players[i]->Get_ClientIndex() == p.id)
			{
				Players[i]->bCoin = p.coin;
				Players[i]->bButton = p.button;
				break;
			}
		}
	}
	break;
	case S2C_SPECIALEVENT:
	{
		packet_specialevent p = *reinterpret_cast<packet_specialevent*>(buf);
		special_event = p.spacialevent; //0 ~ 이벤트발생, -1 - 이벤트 아직온거 없음
		special_event_player = p.id;
	}
	break;
	case S2C_TELEPORT:
	{
		packet_teleport p = *reinterpret_cast<packet_teleport*>(buf);

		for (int i = 0; i < 3; ++i)
		{
			if (Players[i] != nullptr)
			{
				if (Players[i]->Get_ClientIndex() == p.id)
				{
					Players[i]->Set_Lerp(false);
					Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(p.x, p.y, p.z);
					break;
				}
			}
		}
	}
	break;
	default:
		break;
	}
}



void CServerManager::DisconnectServer()
{
	closesocket(sock);
	WSACleanup();
}

HRESULT CServerManager::Release()
{
	DisconnectServer();

	return NOERROR;
}

void CServerManager::InitChickens()
{
	createchickens = 0;
}
