#ifndef __Scene_OneMindmap__
#define __Scene_OneMindmap__

#include "stdafx.h"
#include "Scene.h"
#include "Loading.h"

class CPlayer;
class CBoundingBox;
class CFont;
class CPlayerIcon;
class CBillBoardObject;
class CCube;
class CTileLarge;
class CScene_Boardmap;
class CTeddyBear;
class CUI;
class CStaticObject;

class CScene_OneMindMap final : public CScene
{
private:
	explicit CScene_OneMindMap();
	virtual ~CScene_OneMindMap();

public:
	virtual HRESULT Initialize_Scene(CScene_Boardmap* pSceneBoardmap);
	virtual short Update_Scene(double TimeDelta);
	virtual short LateUpdate_Scene(double TimeDelta);
	virtual HRESULT Render_Scene();
	HRESULT Scene_Change(SCENEID Scene);
	virtual short KeyEvent(double TimeDelta);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, float x, float y, float z, float rx, float ry, float rz);
	virtual CGameObject* CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale);

public:
	static CScene_OneMindMap* Create(CScene_Boardmap* pSceneBoardmap);
	HRESULT Release();

private:
	void movePlayer(XMFLOAT3 forwardDir, XMFLOAT3 dotProduct, XMFLOAT3& XMShift, double TimeDelta);

	void updateLerpPosition(double TimeDelta);
	void updateLerpLook(double TimeDelta);


	short startJump(double TimeDelta, bool bCollision);

private:
	CScene_Boardmap* m_pScene_Boardmap = nullptr;
	float moveDelta;
	bool isMoving = false;

	//게임 동시시작 flag
	bool gameStarted = false;

	CBoundingBox* pBoundingBox[2] = { nullptr, };

	list<CGameObject*> m_listHaxagon;

	bool m_isFall = false;

	bool loadcomplete = false;
	bool lighton = false; //불키기

	bool isMovePlayer = false;

	CPlayer* m_pPlayer = nullptr;
	CTeddyBear* m_pSantaTeddyBear = nullptr;

	bool m_isLerpingPos = false;
	bool m_isLerpingLook = false;

	float m_fTimeDeltaPos = 0.f;
	float m_fTimeDeltaLook = 0.f;

	XMFLOAT3 m_xmLerpStartPos = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmLerpStartLook = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmLerpDestPos = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmLerpDestLook = XMFLOAT3(0.f, 0.f, 0.f);

	CFont* pButtonText[2] = { nullptr, };
	CFont* pTimeText = nullptr;

	CUI* pBatteryUI[3] = { nullptr, };

	//light
	float m_fLightCoolTime = 0.f;
	bool batteryoff = false;
	//flag
	bool gameEnd = false;

	CFont* m_pEndText = nullptr;
	CFont* m_pStatTimeText = nullptr;

	int m_iStartTime = 0;

	int m_iEndTime = 3;
	bool m_bEndAsk = false; //종료 요청 (3초)

	XMFLOAT3 m_xmPositionList[3] = { XMFLOAT3(6.84653,0.877261,-147.719), XMFLOAT3(-1.51241,0.86365,-147.321), XMFLOAT3(-2.36548,0.86365,-146.233) };

	CLoading* m_pLoading = nullptr;

	float m_fJumpPower = 10.f;
	float m_fJumpAccel = 0.f;
	float m_fGravity = 9.8f;

	bool m_bJump = false;

	XMFLOAT3 m_xmJumpTmp = XMFLOAT3(0.f, 0.f, 0.f);

	float m_fTimer = 0.f;

	CStaticObject* snowman1 = nullptr;
	CStaticObject* snowman2 = nullptr;
};

#endif