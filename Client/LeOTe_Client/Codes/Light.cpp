#include "Light.h"
#include "Management.h"
#include "Player.h"

extern CPlayer* Player;

CLight::CLight()
{
}

HRESULT CLight::Initialize_Light()
{
    m_LightsInfo.m_xmf4GlobalAmbient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);

    m_LightsInfo.m_pLights[0].m_bEnable = false;
    m_LightsInfo.m_pLights[0].m_nType = POINT_LIGHT;
    m_LightsInfo.m_pLights[0].m_fRange = 8.0f;
    m_LightsInfo.m_pLights[0].m_xmf4Ambient = XMFLOAT4(1.f, 1.0f, 1.0f, 1.0f);
    m_LightsInfo.m_pLights[0].m_xmf4Diffuse = XMFLOAT4(1.f, 1.0f, 1.0f, 1.0f);
    m_LightsInfo.m_pLights[0].m_xmf4Specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
    m_LightsInfo.m_pLights[0].m_xmf3Position = XMFLOAT3(0.0f, 5.f, 0.0f);
    m_LightsInfo.m_pLights[0].m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_LightsInfo.m_pLights[0].m_xmf3Attenuation = XMFLOAT3(0.2, 0.001, 0.2);
    //m_LightsInfo.m_pLights[0].m_xmf3Attenuation = XMFLOAT3(0.45f, 0.005f, 0.45f);

    m_LightsInfo.m_pLights[1].m_bEnable = false;
    m_LightsInfo.m_pLights[1].m_nType = POINT_LIGHT;
    m_LightsInfo.m_pLights[1].m_fRange = 10.f;
    m_LightsInfo.m_pLights[1].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_LightsInfo.m_pLights[1].m_xmf4Diffuse = XMFLOAT4(0.4f, 0.4f, 0.4f, 1.0f);
    m_LightsInfo.m_pLights[1].m_xmf4Specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
    m_LightsInfo.m_pLights[1].m_xmf3Position = XMFLOAT3(-0.94366, 2.877261, -145.914);
    m_LightsInfo.m_pLights[1].m_xmf3Direction = XMFLOAT3(0.0f, -1.0f, 0.0f);
    m_LightsInfo.m_pLights[1].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
    m_LightsInfo.m_pLights[1].m_fFalloff = 8.0f;
    m_LightsInfo.m_pLights[1].m_fPhi = (float)cos(XMConvertToRadians(40.0f));
    m_LightsInfo.m_pLights[1].m_fTheta = (float)cos(XMConvertToRadians(20.0f));

    XMFLOAT4X4 mtxRotate;
    XMStoreFloat4x4(&mtxRotate, XMMatrixRotationRollPitchYaw(XMConvertToRadians(70.f), XMConvertToRadians(0.f), XMConvertToRadians(0.f)));

    m_LightsInfo.m_pLights[2].m_bEnable = true;
    m_LightsInfo.m_pLights[2].m_nType = DIRECTIONAL_LIGHT;
    m_LightsInfo.m_pLights[2].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_LightsInfo.m_pLights[2].m_xmf4Diffuse = XMFLOAT4(1.f, 1.f, 1.f, 1.0f);
    m_LightsInfo.m_pLights[2].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
    m_LightsInfo.m_pLights[2].m_xmf3Direction = Vector3::Normalize(XMFLOAT3(mtxRotate._31, mtxRotate._32, mtxRotate._33));
    m_LightsInfo.m_pLights[2].m_xmf3Position = XMFLOAT3(89.8112, 100.2391, -97.7247);

    m_LightsInfo.m_pLights[3].m_bEnable = false;
    m_LightsInfo.m_pLights[3].m_nType = POINT_LIGHT;
    m_LightsInfo.m_pLights[3].m_fRange = 60.0f;
    m_LightsInfo.m_pLights[3].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_LightsInfo.m_pLights[3].m_xmf4Diffuse = XMFLOAT4(1.f, 1.f, 1.f, 1.0f);
    m_LightsInfo.m_pLights[3].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
    m_LightsInfo.m_pLights[3].m_xmf3Position = XMFLOAT3(-6.13, 6.45, -19.84);
    m_LightsInfo.m_pLights[3].m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 1.0f);
    m_LightsInfo.m_pLights[3].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
    m_LightsInfo.m_pLights[3].m_fFalloff = 8.0f;
    m_LightsInfo.m_pLights[3].m_fPhi = (float)cos(XMConvertToRadians(90.0f));
    m_LightsInfo.m_pLights[3].m_fTheta = (float)cos(XMConvertToRadians(30.0f));

    m_LightsInfo.m_pLights[4].m_bEnable = false;
    m_LightsInfo.m_pLights[4].m_nType = SPOT_LIGHT;
    m_LightsInfo.m_pLights[4].m_fRange = 60.0f;
    m_LightsInfo.m_pLights[4].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_LightsInfo.m_pLights[4].m_xmf4Diffuse = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);
    m_LightsInfo.m_pLights[4].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
    m_LightsInfo.m_pLights[4].m_xmf3Position = XMFLOAT3(-150.0f, 30.0f, 30.0f);
    m_LightsInfo.m_pLights[4].m_xmf3Direction = XMFLOAT3(0.0f, 1.0f, 1.0f);
    m_LightsInfo.m_pLights[4].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
    m_LightsInfo.m_pLights[4].m_fFalloff = 8.0f;
    m_LightsInfo.m_pLights[4].m_fPhi = (float)cos(XMConvertToRadians(90.0f));
    m_LightsInfo.m_pLights[4].m_fTheta = (float)cos(XMConvertToRadians(30.0f));

   
    CreatShaderVariables();

	return NOERROR;
}

short CLight::Update_Light(double TimeDelta)
{
	return 0;
}

HRESULT CLight::Render_Light()
{
	return NOERROR;
}

void CLight::RightOn(XMFLOAT3 position)
{
    m_LightsInfo.m_pLights[0].m_fRange = 10.0f;
    position.y += 5.f;
    m_LightsInfo.m_pLights[0].m_xmf3Position = position;
    m_LightsInfo.m_pLights[0].m_xmf3Attenuation = XMFLOAT3(0.5, 0.001, 0.05);
}

void CLight::RightOff(XMFLOAT3 position)
{
    m_LightsInfo.m_pLights[0].m_fRange = 8.0f;
    position.y += 5.f;
    m_LightsInfo.m_pLights[0].m_xmf3Position = position;
    m_LightsInfo.m_pLights[0].m_xmf3Attenuation = XMFLOAT3(0.2, 0.001, 0.2);
}

void CLight::DirectionOn()
{
    m_LightsInfo.m_pLights[0].m_bEnable = false;
    m_LightsInfo.m_pLights[1].m_bEnable = false;
    m_LightsInfo.m_pLights[2].m_bEnable = true;
}

void CLight::DirectionOff()
{
    m_LightsInfo.m_pLights[0].m_bEnable = true;
    m_LightsInfo.m_pLights[1].m_bEnable = true;
    m_LightsInfo.m_pLights[2].m_bEnable = false;
}

void CLight::CinemaOn()
{
    m_LightsInfo.m_pLights[3].m_bEnable = true;
    m_LightsInfo.m_pLights[2].m_bEnable = false;
}

void CLight::CinemaOff()
{
    m_LightsInfo.m_pLights[3].m_bEnable = false;
    m_LightsInfo.m_pLights[2].m_bEnable = true;
}

void CLight::UpdateShaderVariables(ID3D12GraphicsCommandList* pCommandList)
{
    ::memcpy(m_pcbMappedLights, &m_LightsInfo, sizeof(LIGHTS));

    D3D12_GPU_VIRTUAL_ADDRESS d3dcbLightsGpuVirtualAddress = m_pd3dcbLights->GetGPUVirtualAddress();
    pCommandList->SetGraphicsRootConstantBufferView(3, d3dcbLightsGpuVirtualAddress);
    /*
    float Radius = 50.f;
    // Only the first "main" light casts a shadow.
    XMVECTOR lightDir = XMLoadFloat3(&m_LightsInfo.m_pLights[2].m_xmf3Direction);
    //XMVECTOR lightPos = -2.0f * Radius * lightDir;
    XMVECTOR lightPos = XMLoadFloat3(&m_LightsInfo.m_pLights[2].m_xmf3Position);
    XMVECTOR targetPos = XMVectorSet(-14.0604, 15.6328, 8.74705, 0.0f);
    XMVECTOR lightUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    XMMATRIX lightView = XMMatrixLookAtLH(lightPos, targetPos, lightUp);

    // Transform bounding sphere to light space.
    XMFLOAT3 sphereCenterLS;
    XMStoreFloat3(&sphereCenterLS, XMVector3TransformCoord(targetPos, lightView));

    // Ortho frustum in light space encloses scene.
    float l = sphereCenterLS.x - Radius;
    float b = sphereCenterLS.y - Radius;
    float n = sphereCenterLS.z - Radius;
    float r = sphereCenterLS.x + Radius;
    float t = sphereCenterLS.y + Radius;
    float f = sphereCenterLS.z + Radius;

    XMFLOAT4X4 lightProj; 
    XMStoreFloat4x4(&lightProj, XMMatrixOrthographicOffCenterLH(l, r, b, t, n, f));

    // Transform NDC space [-1,+1]^2 to texture space [0,1]^2
    XMFLOAT4X4 T(
        0.5f, 0.0f, 0.0f, 0.0f,
        0.0f, -0.5f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.5f, 0.5f, 0.0f, 1.0f);
    XMFLOAT4X4 VP = Matrix4x4::Multiply(lightView, lightProj);
    XMFLOAT4X4 VPT = Matrix4x4::Multiply(VP, T);
    XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4ShadowTransform, XMMatrixTranspose(XMLoadFloat4x4(&VPT)));
    XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4ViewProjection, XMMatrixTranspose(XMLoadFloat4x4(&VP)));
    m_pcbMappedShadow->m_xmf4x4ShadowTransform = T;
    m_pcbMappedShadow->m_xmf4x4ViewProjection = T;
    D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbShadow->GetGPUVirtualAddress();
    pCommandList->SetGraphicsRootConstantBufferView(2, d3dGpuVirtualAddress);
    */
}

CLight* CLight::Create()
{
	CLight* pInstance = new CLight();

	if (pInstance->Initialize_Light())
	{
		MSG_BOX("CLight Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CLight::Release()
{
    if (m_pd3dcbLights)
    {
        m_pd3dcbLights->Unmap(0, nullptr);
        m_pd3dcbLights->Release();
    }

    //delete m_pcbMappedLights;
    m_pcbMappedLights = nullptr;

	delete this;

	return NOERROR;
}

void CLight::CreatShaderVariables()
{
    ID3D12Device* pd3dDevice = CDeviceManager::GetInstance()->Get_Device();
    ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

    _uint ncbElementBytes = ((sizeof(LIGHTS) + 255) & ~255); //256의 배수
    m_pd3dcbLights = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

    m_pd3dcbLights->Map(0, nullptr, (void**)&m_pcbMappedLights);
    /*
    ncbElementBytes = ((sizeof(VS_CB_LIGHT_INFO) + 255) & ~255); //256의 배수

    m_pd3dcbShadow = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

    m_pd3dcbShadow->Map(0, nullptr, (void**)&m_pcbMappedShadow);
    */
}
