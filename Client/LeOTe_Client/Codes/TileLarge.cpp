#include "stdafx.h"
#include "TileLarge.h"
#include "Management.h"
#include "Material.h"
#include "ShadowShader.h"
#include "Renderer.h"

CTileLarge::CTileLarge()
	: CGameObject()
{

}

CTileLarge::CTileLarge(const CTileLarge& rhs)
	: CGameObject(rhs)
{
}

HRESULT CTileLarge::Initialize_GameObject_Prototype()
{
	return NOERROR;
}

HRESULT CTileLarge::Initialize_GameObject(void * pArg)
{
	if (FAILED(Add_Component()))
		return E_FAIL;

	m_pGameObjResource = m_pShaderCom->CreateShaderVariables(m_pGameObjResource, sizeof(CB_GAMEOBJECT_INFO));
	m_pGameObjResource->Map(0, nullptr, (void**)&m_pMappedGameObj);
	m_pMaterialCom->SetDiffuseRGBA(0, 165, 255, 256);
	if (pArg)
	{
		ObjWorld* objw = (ObjWorld*)pArg;
		m_pTransformCom->SetPosition(objw->position.x, objw->position.y, objw->position.z);
		m_pTransformCom->Rotate(objw->rotation.x, objw->rotation.y, objw->rotation.z);
		m_pTransformCom->SetScale(objw->scale.x, objw->scale.y, objw->scale.z);
	}

	m_xmBoundingBox = BoundingBox(XMFLOAT3(0.f, 0.5f, 0.0f), XMFLOAT3(1.55f, 1.f, 1.55f)); //1.5 0.8 1.5
	m_xmBoundingBox.Transform(m_xmBoundingBox, XMLoadFloat4x4(&m_pTransformCom->GetWorldMat()));
	m_bmyself = false;
	m_bChekced = false;
	
	return NOERROR;
}

short CTileLarge::Update_GameObject(double TimeDelta)
{
	if (m_bDel)
		return short();

	if (m_bTimeDel)
		m_fTime += TimeDelta;
	if (m_bmyself) {
		if (m_fTime > 0.8f + DELETESPEED && !m_bDel)
		{
			m_bDel = true;
			m_pTransformCom->SetPosition(0.f, 999.f, 0.f);
		}
	}
	else
	{
		if (m_fTime > 0.8f && !m_bDel)
		{
			m_bDel = true;
			m_pTransformCom->SetPosition(0.f, 999.f, 0.f);
		}
	}
	return short();
}

short CTileLarge::LateUpdate_GameObject(double TimeDelta)
{
	Compute_ViewZ(m_pTransformCom->GetPosition());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONALPHA, this)))
		return -1;

	return short();
}

HRESULT CTileLarge::Render_Shadow()
{
	/*
	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

	m_pShadowShaderCom->SetPipeline(pCommandList);

	m_pShadowShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());
	
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pMeshCom->Render();
	*/
	return NOERROR;
}

HRESULT CTileLarge::Render_GameObject()
{
	if (m_bDel)
		return NOERROR;

	ID3D12GraphicsCommandList* pCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);
	m_pShaderCom->SetPipeline(pCommandList);
	m_pShaderCom->UpdateShaderVariables(m_pMappedGameObj, m_pTransformCom->GetWorldMat(), m_pMappedGameObj->m_xmf4x4TexTransform, m_pMaterialCom->GetMaterialMat());

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbGameObjectGpuVirtualAddress = m_pGameObjResource->GetGPUVirtualAddress();
	pCommandList->SetGraphicsRootConstantBufferView(1, d3dcbGameObjectGpuVirtualAddress);

	m_pMeshCom->Render();
	return NOERROR;
}

HRESULT CTileLarge::Add_Component()
{
	// For.Com_Mesh
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_TileLarge_Mesh", L"Com_Mesh", (CComponent**)&m_pMeshCom)))
		return E_FAIL;

	// For.Com_Shader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_FBXOBJ", L"Com_Shader", (CComponent**)&m_pShaderCom)))
		return E_FAIL;

	// For.Com_Transform
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Transform", L"Com_Transform", (CComponent**)&m_pTransformCom)))
		return E_FAIL;

	// For.Com_Material
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Material", L"Com_Material", (CComponent**)&m_pMaterialCom)))
		return E_FAIL;

	// For.Com_ShadowShader
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Shader_Shadow", L"Com_Shader1", (CComponent**)&m_pShadowShaderCom)))
		return E_FAIL;

	// For.Com_Renderer
	if (FAILED(CGameObject::Add_Component(SCENE_ALL, L"Component_Renderer", L"Com_Renderer", (CComponent**)&m_pRendererCom)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CTileLarge::SetUp_ConstantTable()
{
	return NOERROR;
}

CTileLarge* CTileLarge::Create()
{
	CTileLarge*		pInstance = new CTileLarge();

	if (pInstance->Initialize_GameObject_Prototype())
	{
		MSG_BOX("CTileLarge Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}


CGameObject * CTileLarge::Clone_GameObject(void * pArg)
{
	CTileLarge*		pInstance = new CTileLarge(*this);

	if (pInstance->Initialize_GameObject(pArg))
	{
		MSG_BOX("CTileLarge Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

void CTileLarge::SetMaterialDiffuseRGBA(short r, short g, short b, short a)
{
	m_pMaterialCom->SetDiffuseRGBA(r, g, b, a);
}

HRESULT CTileLarge::Release()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pMaterialCom);
	if (m_pGameObjResource)
	{
		m_pGameObjResource->Unmap(0, nullptr);
		m_pGameObjResource->Release();
	}

	delete this;

	return NOERROR;
}
