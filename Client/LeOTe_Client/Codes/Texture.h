#ifndef __TEXTURE__
#define __TEXTURE__

#include "Component.h"

#define MATERIAL_ALBEDO_MAP			0x01
#define MATERIAL_SPECULAR_MAP		0x02
#define MATERIAL_NORMAL_MAP			0x04
#define MATERIAL_METALLIC_MAP		0x08
#define MATERIAL_EMISSION_MAP		0x10
#define MATERIAL_DETAIL_ALBEDO_MAP	0x20
#define MATERIAL_DETAIL_NORMAL_MAP	0x40

struct TEXTURE_INFO
{
	wstring name;
	wstring fileName;

	ID3D12Resource* Resource;
	ID3D12Resource* UploadHeap;

	CD3DX12_GPU_DESCRIPTOR_HANDLE GpuDescriptorHandle;
};

class CTexture final : public CComponent
{
public:
	enum TEXTURETYPE { TEX_DIFFUSE = 5, TEX_SPECULAR, TEX_NORMAL, TEX_METALLIC, TEX_EMISSIVE, TEX_SKYBOX = 12, TEX_END };
	enum MESHTYPE { MESH_GENERAL, MESH_CUBE, MESH_END };

private:
	explicit CTexture(const wchar_t* pszFileName, const wchar_t* pszNameTag, COMMANDID eCommandId, TEXTURETYPE eTexType, MESHTYPE eMeshType, int nTexCnt);
	explicit CTexture(const CTexture& rhs);
	virtual ~CTexture(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CTexture* Create(const wchar_t* pszFileName, const wchar_t* pszNameTag, COMMANDID eCommandId, TEXTURETYPE eTexType, MESHTYPE eMeshType, int nTexCnt = 1);

	HRESULT Release();

	void UpdateShaderVariables(ID3D12GraphicsCommandList* pCommandList, TEXTURETYPE eRootParameterIdx = TEX_DIFFUSE, _uint nTexIdx = 0);
	void SetMaterialType(UINT nType) { m_nType |= nType; }
private:
	HRESULT BuildDescriptorHeaps(ID3D12Device* pDevice);

private:
	vector<TEXTURE_INFO> m_vecTexture;

	/*ID3D12DescriptorHeap* m_pSrvDescriptorHeap = nullptr;

	CD3DX12_CPU_DESCRIPTOR_HANDLE m_CpuDescriptorHandle;
	CD3DX12_GPU_DESCRIPTOR_HANDLE m_GpuDescriptorHandle;*/

	TEXTURETYPE m_eTextureType;
	MESHTYPE m_eMeshType;

	UINT m_nType = 0x00;

};

#endif
