#ifndef __LIGHT__
#define __LIGHT__

#include "stdafx.h"

struct VS_CB_LIGHT_INFO
{
	XMFLOAT4X4	m_xmf4x4ViewProjection;
	XMFLOAT4X4	m_xmf4x4ShadowTransform;
};

class CLight
{
private:
	explicit CLight();
	virtual ~CLight(void) = default;

public:
	HRESULT Initialize_Light();
	short	Update_Light(double TimeDelta);
	HRESULT Render_Light();
	void	RightOn(XMFLOAT3 position);
	void	RightOff(XMFLOAT3 position);
	void	DirectionOn();
	void	DirectionOff();
	void	CinemaOn();
	void	CinemaOff();

	void UpdateShaderVariables(ID3D12GraphicsCommandList* pCommandList);

	static CLight* Create();

	HRESULT Release();

	LIGHTS GetLights() { return m_LightsInfo; }

private:
	void CreatShaderVariables();

private:
	LIGHTS m_LightsInfo;

	ID3D12Resource* m_pd3dcbLights = nullptr;
	LIGHTS* m_pcbMappedLights = nullptr;

	VS_CB_LIGHT_INFO m_ShadowInfo;
	ID3D12Resource* m_pd3dcbShadow;
	VS_CB_LIGHT_INFO* m_pcbMappedShadow;

};

#endif