#ifndef __COLLISIONMANAGER__
#define __COLLISIONMANAGER__

#include "stdafx.h"

class CCollider;
class CCollisionManager final
{
	DECLARE_SINGLETON(CCollisionManager)

public:
	explicit CCollisionManager();
	virtual ~CCollisionManager();

public:
	HRESULT Initialize_CollisionManager(_uint iNumScenes);

	HRESULT Release();

	void checkCollision(double TimeDelta);

	void addCollider(CCollider* pColl) { m_vecCollider.push_back(pColl); }
	void clearColliderArray() { m_vecCollider.clear(); }

private:
	vector<CCollider*> m_vecCollider;


};

#endif