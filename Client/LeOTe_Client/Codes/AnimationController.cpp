#include "stdafx.h"
#include "AnimationController.h"

CAnimationController::CAnimationController()
{
}

CAnimationController::CAnimationController(const CAnimationController& rhs)
{
}

CAnimationController::~CAnimationController(void)
{
}

HRESULT CAnimationController::Initialize_Component_Prototype()
{
	
	return NOERROR;
}

HRESULT CAnimationController::Initialize_Component(void* pArg)
{
	return NOERROR;
}

CComponent* CAnimationController::Clone_Component(void* pArg)
{
	CAnimationController* pInstance = new CAnimationController(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CAnimationController Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CAnimationController* CAnimationController::Create()
{
	CAnimationController* pInstance = new CAnimationController();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CAnimationController Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

void CAnimationController::SetFBXScene(FbxScene* pfbxScene)
{
	FbxArray<FbxString*> fbxAnimationStackNames;
	pfbxScene->FillAnimStackNameArray(fbxAnimationStackNames);

	m_nAnimationStacks = fbxAnimationStackNames.Size();

	m_ppfbxAnimationStacks = new FbxAnimStack * [m_nAnimationStacks];
	m_pfbxStartTimes = new FbxTime[m_nAnimationStacks];
	m_pfbxStopTimes = new FbxTime[m_nAnimationStacks];
	m_pfbxCurrentTimes = new FbxTime[m_nAnimationStacks];

	for (int i = 0; i < m_nAnimationStacks; i++)
	{
		FbxString* pfbxStackName = fbxAnimationStackNames[i];
		FbxAnimStack* pfbxAnimationStack = pfbxScene->FindMember<FbxAnimStack>(pfbxStackName->Buffer());
		m_ppfbxAnimationStacks[i] = pfbxAnimationStack;

		FbxTakeInfo* pfbxTakeInfo = pfbxScene->GetTakeInfo(*pfbxStackName);
		FbxTime fbxStartTime, fbxStopTime;
		if (pfbxTakeInfo)
		{
			fbxStartTime = pfbxTakeInfo->mLocalTimeSpan.GetStart();
			fbxStopTime = pfbxTakeInfo->mLocalTimeSpan.GetStop();
		}
		else
		{
			FbxTimeSpan fbxTimeLineTimeSpan;
			pfbxScene->GetGlobalSettings().GetTimelineDefaultTimeSpan(fbxTimeLineTimeSpan);
			fbxStartTime = fbxTimeLineTimeSpan.GetStart();
			fbxStopTime = fbxTimeLineTimeSpan.GetStop();
		}

		m_pfbxStartTimes[i] = fbxStartTime;
		m_pfbxStopTimes[i] = fbxStopTime;
		m_pfbxCurrentTimes[i] = FbxTime(0);
	}

	FbxArrayDelete(fbxAnimationStackNames);
}

void CAnimationController::SetAnimationStack(FbxScene *pfbxScene, int nAnimationStack)
{
	m_nAnimationStack = nAnimationStack;
	pfbxScene->SetCurrentAnimationStack(m_ppfbxAnimationStacks[nAnimationStack]);
}

void CAnimationController::SetPosition(int nAnimationStack, float fPosition)
{
	m_pfbxCurrentTimes[nAnimationStack].SetSecondDouble(fPosition);;
}

void CAnimationController::AdvanceTime(float fTimeElapsed) 
{
	m_fTime += fTimeElapsed; 

	FbxTime fbxElapsedTime;
	fbxElapsedTime.SetSecondDouble(fTimeElapsed);

	m_pfbxCurrentTimes[m_nAnimationStack] += fbxElapsedTime;
	if (m_pfbxCurrentTimes[m_nAnimationStack] > m_pfbxStopTimes[m_nAnimationStack]) m_pfbxCurrentTimes[m_nAnimationStack] = m_pfbxStartTimes[m_nAnimationStack];
} 

void CAnimationController::Animate(FbxScene* pfbxScene, float fTimeElapsed)
{
	AdvanceTime(fTimeElapsed);
	FbxTime fbxCurrentTime = GetCurrentTime();
	::AnimateFbxNodeHierarchy(pfbxScene->GetRootNode(), fbxCurrentTime);
}

HRESULT CAnimationController::Release()
{

	delete this;

	if (m_ppfbxAnimationStacks) delete[] m_ppfbxAnimationStacks;
	if (m_pfbxStartTimes) delete[] m_pfbxStartTimes;
	if (m_pfbxStopTimes) delete[] m_pfbxStopTimes;
	if (m_pfbxCurrentTimes) delete[] m_pfbxCurrentTimes;

	return NOERROR;
}
