#include "Shader.h"
#include "Management.h"

CShader::CShader()
{
}

CShader::CShader(const CShader& rhs)
    : m_pPipelineState(rhs.m_pPipelineState)
{
}

CShader::~CShader(void)
{
}

HRESULT CShader::Initialize_Component_Prototype()
{
    m_pPipelineState = Create_PipeLineState();
    m_pPipelineState->SetName(L"Shader PipelineState");
	return NOERROR;
}

HRESULT CShader::Initialize_Component(void* pArg)
{
    //m_pPipelineState = Create_PipeLineState();
    //m_pPipelineState->SetName(L"Shader PipelineState");
	return NOERROR;
}

CComponent* CShader::Clone_Component(void* pArg)
{
	CShader* pInstance = new CShader(*this);

	if (pInstance->Initialize_Component(pArg))
	{
		MSG_BOX("CShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

CShader* CShader::Create()
{
	CShader* pInstance = new CShader();

	if (pInstance->Initialize_Component_Prototype())
	{
		MSG_BOX("CShader Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

ID3D12PipelineState* CShader::Create_PipeLineState(D3D12_PRIMITIVE_TOPOLOGY_TYPE d3dPrimitiveTopology)     // 후에 enum으로 쉐이더 구분
{
    ID3D12PipelineState* pPipeLineState = nullptr;
    ID3DBlob* pd3dVertexShaderBlob = nullptr, * pd3dPixelShaderBlob = nullptr, *pd3dGeometryShaderBlob = nullptr, * pd3dHullShaderBlob = nullptr, * pd3dDomainShaderBlob = nullptr;

    D3D12_GRAPHICS_PIPELINE_STATE_DESC d3dPipelineStateDesc;
    ::ZeroMemory(&d3dPipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));

    d3dPipelineStateDesc.pRootSignature = CManagement::GetInstance()->Get_RootSignature();
    d3dPipelineStateDesc.VS = Create_VertexShader(&pd3dVertexShaderBlob);
    d3dPipelineStateDesc.PS = Create_PixelShader(&pd3dPixelShaderBlob);
    d3dPipelineStateDesc.GS = Create_GeometryShader(&pd3dGeometryShaderBlob);
    d3dPipelineStateDesc.HS = Create_HullShader(&pd3dHullShaderBlob);
    d3dPipelineStateDesc.DS = Create_DomainShader(&pd3dDomainShaderBlob);
    d3dPipelineStateDesc.RasterizerState = Create_RasterizerState();
    d3dPipelineStateDesc.BlendState = Create_BlendState();
    d3dPipelineStateDesc.DepthStencilState = Create_DepthStencilState();
    d3dPipelineStateDesc.InputLayout = Create_InputLayout();
    d3dPipelineStateDesc.SampleMask = UINT_MAX;
    d3dPipelineStateDesc.PrimitiveTopologyType = d3dPrimitiveTopology; // topology type도 enum 값에 따라 다르게 설정
    d3dPipelineStateDesc.NumRenderTargets = 1;
    d3dPipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
    d3dPipelineStateDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
    d3dPipelineStateDesc.SampleDesc.Count = 1;
    d3dPipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
    HRESULT hResult = CDeviceManager::GetInstance()->Get_Device()->CreateGraphicsPipelineState(&d3dPipelineStateDesc, 
        __uuidof(ID3D12PipelineState), (void**)&pPipeLineState);

    if (pd3dVertexShaderBlob) pd3dVertexShaderBlob->Release();
    if (pd3dPixelShaderBlob) pd3dPixelShaderBlob->Release();
    if (pd3dGeometryShaderBlob) pd3dGeometryShaderBlob->Release();
    if (pd3dHullShaderBlob) pd3dHullShaderBlob->Release();
    if (pd3dDomainShaderBlob) pd3dDomainShaderBlob->Release();

    if (d3dPipelineStateDesc.InputLayout.pInputElementDescs) delete[] d3dPipelineStateDesc.InputLayout.pInputElementDescs;
    return pPipeLineState;
}

D3D12_SHADER_BYTECODE CShader::Create_VertexShader(ID3DBlob** ppBlob)
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode;
    d3dShaderByteCode.BytecodeLength = 0;
    d3dShaderByteCode.pShaderBytecode = NULL;
    return d3dShaderByteCode;
}

D3D12_SHADER_BYTECODE CShader::Create_PixelShader(ID3DBlob** ppBlob)
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode;
    d3dShaderByteCode.BytecodeLength = 0;
    d3dShaderByteCode.pShaderBytecode = NULL;
    return d3dShaderByteCode;
}

D3D12_SHADER_BYTECODE CShader::Create_GeometryShader(ID3DBlob** ppBlob)
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode;
    d3dShaderByteCode.BytecodeLength = 0;
    d3dShaderByteCode.pShaderBytecode = NULL;
    return d3dShaderByteCode;
}

D3D12_SHADER_BYTECODE CShader::Create_HullShader(ID3DBlob** ppBlob)
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode;
    d3dShaderByteCode.BytecodeLength = 0;
    d3dShaderByteCode.pShaderBytecode = NULL;
    return d3dShaderByteCode;
}

D3D12_SHADER_BYTECODE CShader::Create_DomainShader(ID3DBlob** ppBlob)
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode;
    d3dShaderByteCode.BytecodeLength = 0;
    d3dShaderByteCode.pShaderBytecode = NULL;
    return d3dShaderByteCode;
}

D3D12_RASTERIZER_DESC CShader::Create_RasterizerState()
{
    D3D12_RASTERIZER_DESC d3dRasterizerDesc;
    ::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
    d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
    d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
    d3dRasterizerDesc.FrontCounterClockwise = FALSE;
    d3dRasterizerDesc.DepthBias = 0;
    d3dRasterizerDesc.DepthBiasClamp = 0.0f;
    d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
    d3dRasterizerDesc.DepthClipEnable = TRUE;
    d3dRasterizerDesc.MultisampleEnable = FALSE;
    d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
    d3dRasterizerDesc.ForcedSampleCount = 0;
    d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

    return(d3dRasterizerDesc);
}

D3D12_BLEND_DESC CShader::Create_BlendState()
{
    D3D12_BLEND_DESC d3dBlendDesc;
    ::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
    d3dBlendDesc.AlphaToCoverageEnable = FALSE;
    d3dBlendDesc.IndependentBlendEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
    d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
    d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
    d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
    d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
    d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
    d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
    d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

    return(d3dBlendDesc);
}

D3D12_DEPTH_STENCIL_DESC CShader::Create_DepthStencilState()
{
    D3D12_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
    ::ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
    d3dDepthStencilDesc.DepthEnable = TRUE;
    d3dDepthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
    d3dDepthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
    d3dDepthStencilDesc.StencilEnable = FALSE;
    d3dDepthStencilDesc.StencilReadMask = 0x00;
    d3dDepthStencilDesc.StencilWriteMask = 0x00;
    d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
    d3dDepthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

    return(d3dDepthStencilDesc);
}

D3D12_INPUT_LAYOUT_DESC CShader::Create_InputLayout()
{
    UINT nInputElementDescs = 0;
    D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = nullptr;

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CShader::CompileShaderFromFile(const wchar_t* pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob** ppd3dShaderBlob)
{
    UINT nCompileFlags = 0;
#if defined(_DEBUG)
    nCompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    ID3DBlob* pd3dErrorBlob = NULL;
    char* pErrorString = nullptr;
    HRESULT hResult = ::D3DCompileFromFile(pszFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, pszShaderName, pszShaderProfile, nCompileFlags, 0, ppd3dShaderBlob, &pd3dErrorBlob);
    
    if(pd3dErrorBlob)
        pErrorString = (char*)pd3dErrorBlob->GetBufferPointer();

    D3D12_SHADER_BYTECODE d3dShaderByteCode;
    d3dShaderByteCode.BytecodeLength = (*ppd3dShaderBlob)->GetBufferSize();
    d3dShaderByteCode.pShaderBytecode = (*ppd3dShaderBlob)->GetBufferPointer();

    return(d3dShaderByteCode);
}

ID3D12Resource* CShader::CreateShaderVariables(ID3D12Resource* pd3dcbResource, _uint icbSize)
{
    ID3D12Device* pd3dDevice = CDeviceManager::GetInstance()->Get_Device();
    ID3D12GraphicsCommandList* pd3dCommandList = CDeviceManager::GetInstance()->Get_CommandList(COMMAND_RENDER);

    _uint ncbElementBytes = ((icbSize + 255) & ~255); //256의 배수
    pd3dcbResource = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

    return pd3dcbResource;
}

void CShader::UpdateShaderVariables(CB_GAMEOBJECT_INFO* pcbGameObjInfo, XMFLOAT4X4 matWorld, XMFLOAT4X4 matTexTransform, MATERIAL matMaterial)
{
    UINT ncbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    XMFLOAT4X4 xmf4x4World, xmf4x4TexTransform;
 
    XMStoreFloat4x4(&xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&matWorld)));
    XMStoreFloat4x4(&xmf4x4TexTransform, XMMatrixTranspose(XMLoadFloat4x4(&matTexTransform)));
  
    ::memcpy(&pcbGameObjInfo->m_xmf4x4World, &xmf4x4World, sizeof(XMFLOAT4X4));
    ::memcpy(&pcbGameObjInfo->m_xmf4x4TexTransform, &xmf4x4TexTransform, sizeof(XMFLOAT4X4));
    ::memcpy(&pcbGameObjInfo->m_xmf4x4Material, &matMaterial, sizeof(MATERIAL));
    
}

void CShader::SetPipeline(ID3D12GraphicsCommandList* pd3dCommandList, int nPipelineState)
{
    if (m_pPipelineState) pd3dCommandList->SetPipelineState(m_pPipelineState);
}

HRESULT CShader::Release()
{
    //if(m_pPipelineState)
    //    m_pPipelineState->Release();

	delete this;

	return NOERROR;
}
