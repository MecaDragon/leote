#include "Scene_Prize.h"
#include "Management.h"
#include "Scene_Boardmap.h"
#include "Scene_WaitingRoom.h"
#include "ServerManager.h"
#include "Camera_Basic.h"
#include "Logo.h"
#include "StartButton.h"
#include "Light.h"
#include "Texture.h"
#include "Transform.h"
#include "Plane_Buffer.h"
#include "Cube_Buffer.h"
#include "Font.h"
#include "Player.h"
#include "UI.h"
#include "Camera_Fixed.h"
#include "CameraManager.h"
#include "StaticObject.h"
#include "TeddyBear.h"
#include "Ghost.h"
#include "Bird.h"


extern bool G_SC;
extern CPlayer* Player;
extern CPlayer* Players[3];

CScene_Prize::CScene_Prize()
{
}

CScene_Prize::~CScene_Prize()
{
}

CGameObject* CScene_Prize::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position = transform;
	temp.rotation = rotation;
	temp.scale = scale;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_PRIZE, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

HRESULT CScene_Prize::Initialize_Scene()
{
	m_SceneId = SCENEID::SCENE_PRIZE;

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);


	m_pManagement = CManagement::GetInstance();

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
	CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));

	CreateObject(L"Layer_BackGround", L"GameObject_Backroom", XMFLOAT3(-5.4, 6.8, -20.1), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow01", XMFLOAT3(0.0, 0.0, -1.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow02", XMFLOAT3(0.0, 0.2, -2.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow03", XMFLOAT3(0.0, 0.4, -3.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow04", XMFLOAT3(0.0, 0.6, -4.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow05", XMFLOAT3(0.0, 0.8, -5.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow06", XMFLOAT3(0.0, 1.0, -6.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow07", XMFLOAT3(0.0, 1.2, -7.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow08", XMFLOAT3(0.0, 1.4, -8.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow09", XMFLOAT3(0.0, 1.6, -9.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow10", XMFLOAT3(0.0, 1.8, -10.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow11", XMFLOAT3(0.0, 2.3, -14.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow12", XMFLOAT3(0.0, 2.5, -15.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow13", XMFLOAT3(0.0, 2.7, -16.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow14", XMFLOAT3(0.0, 2.9, -17.4), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow16", XMFLOAT3(0.0, 3.3, -19.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_ChairsRow17", XMFLOAT3(0.0, 3.1, -18.3), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Cups", XMFLOAT3(-0.3, 0.4, -1.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Doors", XMFLOAT3(-5.4, 2.0, -5.4), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.0100000047683716, 0.0100000023841858, 0.0100000023841858));
	m_pFloor = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_Floor", XMFLOAT3(-5.4, -0.2, -0.6), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858)));
	m_pScreen = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_HDScreen", XMFLOAT3(-5.4, 4.6, 5.2), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.00935835540294647, 0.00935835540294647, 0.00935835540294647)));
	CreateObject(L"Layer_BackGround", L"GameObject_Glasses", XMFLOAT3(-3.9, 0.4, -1.2), XMFLOAT3(273.6, 247.9, 124.4), XMFLOAT3(0.01, 0.01, 0.01));
	CreateObject(L"Layer_BackGround", L"GameObject_Loudspeakers", XMFLOAT3(-5.4, 4.2, -9.8), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Popcorn", XMFLOAT3(0.4, 0.2, -2.5), XMFLOAT3(334.7, 317.5, 162.3), XMFLOAT3(0.0109505796432495, 0.0109505820274353, 0.0109505796432495));
	CreateObject(L"Layer_BackGround", L"GameObject_PopcornBucket", XMFLOAT3(-2.1, 0.0, -1.0), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Rail", XMFLOAT3(2.1, 2.3, -8.9), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_SpotLight", XMFLOAT3(-5.4, 8.1, -5.5), XMFLOAT3(270.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Tickets", XMFLOAT3(-0.3, -0.2, -0.5), XMFLOAT3(270.0, 303.9, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.0100000023841858));
	CreateObject(L"Layer_BackGround", L"GameObject_Walls", XMFLOAT3(-5.3, 5.3, -17.0), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.01, 0.01, 0.01));

	CreateObject(L"Layer_BackGround", L"GameObject_PrizeLogo", XMFLOAT3(-6.1, -5.0, 2.5), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00150000065565109, 0.0100000023841858, 0.00100000031292439));

	CreateObject(L"Layer_BackGround", L"GameObject_snowman2", XMFLOAT3(-7.2, -15.5, 3.8), XMFLOAT3(270.0, 180.0, 0.0), XMFLOAT3(0.3, 0.3, 0.3));
	CreateObject(L"Layer_BackGround", L"GameObject_snowman2", XMFLOAT3(-8.8, -15.5, 3.9), XMFLOAT3(270.0, 153.9, 0.0), XMFLOAT3(0.3, 0.3, 0.3));

	m_pScreen->SetRender(false);
	m_pDualDice[0] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_DiceScreen", XMFLOAT3(-2.5, 6.1, 5.2), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00150000065565109, 0.0100000023841858, 0.00150000035762787)));
	m_pDualDice[1] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_DiceScreen", XMFLOAT3(-8.2, 6.1, 5.2), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00150000065565109, 0.0100000023841858, 0.00150000035762787)));
	m_pDualIcon[0] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_WinScreen", XMFLOAT3(-2.5, 3.4, 5.2), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0100000023841858, 0.00300000071525574)));
	m_pDualIcon[1] = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_WinScreen", XMFLOAT3(-8.1, 3.4, 5.2), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0100000023841858, 0.00300000071525574)));
	
	m_pVs = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_VS", XMFLOAT3(-5.3, 3.4, 5.2), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00200000092387199, 0.0100000023841858, 0.00200000062584877)));
	m_pVs->SetRender(false);
	for (int i = 0; i < 2; ++i)
	{
		m_pDualDice[i]->SetRender(false);
		m_pDualIcon[i]->SetRender(false);
	}

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pWinnerText)))
		return E_FAIL;
	pWinnerText->SetTextPos(XMFLOAT2(0.53f, 0.48f));
	pWinnerText->SetTextScale(XMFLOAT2(1.4f, 1.4f));
	pWinnerText->SetTextString(L"kkk win");
	pWinnerText->SetHide(true);

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pDualText[0])))
		return E_FAIL;
	pDualText[0]->SetTextPos(XMFLOAT2(0.2f, 0.4f));
	pDualText[0]->SetTextString(L"kkkkk");
	pDualText[0]->SetHide(true);

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_Font", (CGameObject**)&pDualText[1])))
		return E_FAIL;
	pDualText[1]->SetTextPos(XMFLOAT2(0.7f, 0.4f));
	pDualText[1]->SetTextString(L"aaaa");
	pDualText[1]->SetHide(true);
	
	m_pWinScreen = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_WinScreen", XMFLOAT3(-8.1, 4.8, 5.2), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0100000023841858, 0.00300000071525574)));
	m_pWinScreen->SetRender(false);
	
	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_Ghost", (CGameObject**)&m_pGhost)))
		return E_FAIL;
	m_pGhost->m_pTransformCom->SetPosition(-8.f, -6.9f, 3.f);
	m_pGhost->m_pTransformCom->Rotate(0.f, 0.f, 90.f);
	m_pGhost->m_pTransformCom->SetScale(0.4f, 0.4f, 0.4f);
	m_pGhost->ChangeAnimation("idle1");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&m_pTeddyBear)))
		return E_FAIL;
	m_pTeddyBear->m_pTransformCom->SetPosition(-6.2f, -7.723, 3.f);
	m_pTeddyBear->m_pTransformCom->Rotate(0.f, 0.f, 270.f);
	m_pTeddyBear->m_pTransformCom->SetScale(0.5f, 0.5f, 0.5f);
	m_pTeddyBear->ChangeAnimation("idle1");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&m_pTeddyBear2)))
		return E_FAIL;
	m_pTeddyBear2->m_pTransformCom->SetPosition(-5.f, -11.6, 2.5f);
	m_pTeddyBear2->m_pTransformCom->Rotate(0.f, 0.f, 180.f);
	m_pTeddyBear2->m_pTransformCom->SetScale(0.5f, 0.5f, 0.5f);
	m_pTeddyBear2->m_iSkinid = 2;
	m_pTeddyBear2->ChangeAnimation("freefall");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_Bird", (CGameObject**)&m_pBird)))
		return E_FAIL;
	m_pBird->m_pTransformCom->SetPosition(-5.f, -9.48, 3.f);
	m_pBird->m_pTransformCom->Rotate(0.f, 0.f, 180.f);
	m_pBird->m_pTransformCom->SetScale(0.4f, 0.4f, 0.4f);
	m_pBird->ChangeAnimation("idle1");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_SantaTeddyBearE", (CGameObject**)&m_pSantaTeddyBear)))
		MSG_BOX("SantaTeddyBear Create Fail");
	m_pSantaTeddyBear->m_pTransformCom->SetPosition(-8.f, -15.48, 3.f);
	m_pSantaTeddyBear->m_pTransformCom->Rotate(0.f, 0.f, 180.f);
	m_pSantaTeddyBear->m_pTransformCom->SetScale(0.5f, 0.5f, 0.5f);
	m_pSantaTeddyBear->ChangeAnimation("idle3");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&m_pTeddyBear3)))
		return E_FAIL;
	m_pTeddyBear3->m_pTransformCom->SetPosition(-5.922835, -19.471, 3.f);
	m_pTeddyBear3->m_pTransformCom->Rotate(0.f, 0.f, 180.f);
	m_pTeddyBear3->m_iSkinid = 1;
	m_pTeddyBear3->m_pTransformCom->SetScale(0.5f, 0.5f, 0.5f);
	m_pTeddyBear3->ChangeAnimation("idle2");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&m_pTeddyBear4)))
		return E_FAIL;
	m_pTeddyBear4->m_pTransformCom->SetPosition(-7.f, -19.471, 3.f);
	m_pTeddyBear4->m_pTransformCom->Rotate(0.f, 0.f, 180.f);
	m_pTeddyBear4->m_iSkinid = 3;
	m_pTeddyBear4->m_pTransformCom->SetScale(0.5f, 0.5f, 0.5f);
	m_pTeddyBear4->ChangeAnimation("idle3");

	if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_PRIZE, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&m_pTeddyBear5)))
		return E_FAIL;
	m_pTeddyBear5->m_pTransformCom->SetPosition(-5.f, -19.471, 3.f);
	m_pTeddyBear5->m_pTransformCom->Rotate(0.f, 0.f, 180.f);
	m_pTeddyBear5->m_iSkinid = 0;
	m_pTeddyBear5->m_pTransformCom->SetScale(0.5f, 0.5f, 0.5f);
	m_pTeddyBear5->ChangeAnimation("idle1");

	CreateObject(L"Layer_BackGround", L"GameObject_Thanks", XMFLOAT3(-5.9, -20.3, 3.0), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0001, 0.00060000031292439));
	CreateObject(L"Layer_BackGround", L"GameObject_KJU", XMFLOAT3(-6.7, -11.0, 3.0), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0001, 0.00060000031292439));
	CreateObject(L"Layer_BackGround", L"GameObject_LSJ", XMFLOAT3(-5.5, -15.0, 3.0), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0001, 0.00060000031292439));
	CreateObject(L"Layer_BackGround", L"GameObject_KSJ", XMFLOAT3(-4.5, -7.1, 3.0), XMFLOAT3(90.0, 180.0, 0.0), XMFLOAT3(0.00300000131130219, 0.0001, 0.00060000031292439));

	//CreateObject(L"Layer_BackGround", L"GameObject_HDScreenBack", XMFLOAT3(-5.4, 4.6, 5.5), XMFLOAT3(0.0, 0.0, 0.0), XMFLOAT3(0.0135835540294647, 0.0135835540294647, 0.00935835540294647));
	Player->m_tPlayerInfo.xmPosition = XMFLOAT3(-7.015, 0.611, 3.97);
	Player->SetScale(XMFLOAT3(1.f, 1.f, 1.f));
	//CManagement::GetInstance()->GetLight()->CinemaOn();

	pCameraFixed->settingCamera(m_xmCameraPos, m_xmLookAtPos);

	m_pLoading = CLoading::Create(SCENE_WAITINGROOM);

	if (nullptr == m_pLoading)
		return E_FAIL;

	if (SERVERCONNECT)
	{
		for (int i = 0; i < 3; ++i)
		{
			Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, -50.f, 0.f);
			Players[i]->SetScale(XMFLOAT3(1.f, 1.f, 1.f));
		}
	}

	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
	CSoundManager::GetInstance()->Play_Sound(L"Prizemap", 1.f, 0, false); //배경음악 루프 x
	CSoundManager::GetInstance()->Play_Sound(L"Clap");

	if (SERVERCONNECT == false)
	{
		prizeOrder = PRIZE_WIN;
	}
	return NOERROR;
}

short CScene_Prize::KeyEvent(double TimeDelta)
{
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();

	if (pKeyMgr->KeyDown(KEY_SPACE))
	{
		if (prizeOrder == PRIZE_DUAL)
		{
			if (isDualPlayer)
			{
				//서버에다 주사위 번호 받겠다고 협박으로 요청함
				packet_matchtie pm;
				pm.id = Player->Get_ClientIndex();
				pm.size = sizeof(packet_matchtie);
				pm.type = C2S_MATCHTIE;
				pm.dicenum = 0;

				CServerManager::GetInstance()->SendData(C2S_MATCHTIE, &pm);

				isDualPlayer = false; // 돌렸으니 니 역할은 여기까지란다.
			}
		}
	}

	CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));
	pCameraFixed->settingCamera(m_xmCameraPos, m_xmLookAtPos);

	if (pKeyMgr->KeyDown(KEY_SHIFT) && true == m_pLoading->Get_Finish())
	{
		CManagement* pManagement = CManagement::GetInstance();

		if (nullptr == pManagement)
			return -1;

		pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);
		CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
		CSoundManager::GetInstance()->Play_Sound(L"Waitroom", 1.f, 0, true); //배경음악 재생
		if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
			return -1;

		if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
			return -1;

		if (FAILED(pManagement->Clear_Scene(SCENE_PRIZE)))
			return -1;
	}
	return NOERROR;
}

short CScene_Prize::Update_Scene(double TimeDelta)
{
	if (!m_bStartAsk && SERVERCONNECT) {
		packet_loadingsuccess p2;
		p2.size = sizeof(packet_loadingsuccess);
		p2.type = C2S_MINIGAMELOADING;
		p2.scene = S_PRIZEMAP;
		CServerManager::GetInstance()->SendData(C2S_MINIGAMELOADING, &p2);

		m_bStartAsk = true;
		//cameraAnimation(TimeDelta);
		return 0;
	}

	if (CServerManager::GetInstance()->Get_MinigameReady())
	{
		prizeOrder = PRIZE_WAIT; //게임 동시시작 플래그 
		CServerManager::GetInstance()->Set_MinigameReady(false);
	}
	

	switch (prizeOrder)
	{
	case PRIZE_WAIT:
	{
		if (CServerManager::GetInstance()->Get_Tied() == 1)
		{
			if (CServerManager::GetInstance()->Get_TiedPlayer(0) >= 0 && CServerManager::GetInstance()->Get_TiedPlayer(1) >= 0)
			{
				for (int i = 0; i < 3; ++i)
				{
					if (Players[i]->Get_ClientIndex() == CServerManager::GetInstance()->Get_TiedPlayer(0))
						m_pDualPlayer[0] = Players[i];

					if (Players[i]->Get_ClientIndex() == CServerManager::GetInstance()->Get_TiedPlayer(1))
						m_pDualPlayer[1] = Players[i];
				}
				if (Player->Get_ClientIndex() == CServerManager::GetInstance()->Get_TiedPlayer(0) || Player->Get_ClientIndex() == CServerManager::GetInstance()->Get_TiedPlayer(1)) {
					isDualPlayer = true; // 듀얼리스트 구나
				}

				m_pDualPlayer[1]->m_tPlayerInfo.xmPosition = XMFLOAT3(-8.165, 0.614, 4.26);
				m_pDualPlayer[1]->m_tPlayerInfo.xmLook = XMFLOAT3(1.0f, 0.f, 0.f);
				m_pDualPlayer[1]->m_tPlayerInfo.xmUp = XMFLOAT3(0.0f, 1.f, 0.f);
				m_pDualPlayer[1]->m_tPlayerInfo.xmRight = XMFLOAT3(0.0f, 0.f, 1.f);
				m_pDualPlayer[0]->m_tPlayerInfo.xmPosition = XMFLOAT3(-2.37, 0.614, 4.151961);
				m_pDualPlayer[0]->m_tPlayerInfo.xmLook = XMFLOAT3(-1.0f, 0.f, 0.f);
				m_pDualPlayer[0]->m_tPlayerInfo.xmUp = XMFLOAT3(0.0f, 1.f, 0.f);
				m_pDualPlayer[0]->m_tPlayerInfo.xmRight = XMFLOAT3(0.0f, 0.f, -1.f);

				m_xmCameraPos = XMFLOAT3{ -5.13, 2.85, -5.76995 };
				m_xmLookAtPos = XMFLOAT3{ -5.13, 1.62, 20.93 };

				m_pVs->SetRender(true);

				wstring w = L"";
				string name = m_pDualPlayer[1]->Get_Name();
				w.assign(name.begin(), name.end());
				pDualText[0]->SetTextString(w);
				pDualText[0]->SetHide(false);

				name = m_pDualPlayer[0]->Get_Name();
				w.assign(name.begin(), name.end());
				pDualText[1]->SetTextString(w);
				pDualText[1]->SetHide(false);

				for (int i = 0; i < 2; ++i)
				{
					m_pDualDice[i]->SetRender(true);

					m_pDualIcon[i]->SetTexid(m_pDualPlayer[i]->GetSkinid());
					m_pDualIcon[i]->SetRender(true);
				}

				prizeOrder = PRIZE_DUAL; // 불 ------ 편 
				CServerManager::GetInstance()->Init_TiedPlayer();
			}
		}
		else if (CServerManager::GetInstance()->Get_Tied() == 2)
		{
			for (int i = 0; i < 3; ++i)
			{
				Players[i]->m_tPlayerInfo.xmPosition = m_xmPlayerPos[i];
				Players[i]->m_tPlayerInfo.xmLook = XMFLOAT3(0.f, 0.f, 1.f);
			}
			m_pScreen->SetRender(true); // 카운트 스타트
			prizeOrder = PRIZE_WIN; // 편-안
		}
		break;
	}
	case PRIZE_DUAL:
	{
		if (CServerManager::GetInstance()->Get_isTiedDice())
		{
			CSoundManager::GetInstance()->Stop_Sound(L"DiceRotate");
			CSoundManager::GetInstance()->Play_Sound(L"ppyong");

			for (int i = 0; i < 2; ++i)
			{
				if (m_pDualPlayer[i]->Get_ClientIndex() == CServerManager::GetInstance()->Get_MatchTiedPlayer().index)
				{
					m_iDualNum[i] = CServerManager::GetInstance()->Get_MatchTiedPlayer().dicenum;
					CServerManager::GetInstance()->Init_MatchTiedPlayer();
					break;
				}
			}
			

			CServerManager::GetInstance()->Set_isTiedDice(false);
		}

		int cnt = 0; // 몇 명이나 돌렸을까
		for (int i = 0; i < 2; ++i)
		{
			//m_iDualNum[i] <- 서버에서 받은 주사위 번호 갱신
			if (m_iDualNum[i] == 0) // 왜 안 돌려!
			{
				m_pDualDice[i]->SetTexid(rand()%6);
				CSoundManager::GetInstance()->Play_Sound(L"DiceRotate", 1.0f, 0, true);
			}
			else // 돌렸넹
			{
				m_pDualDice[i]->SetTexid(m_iDualNum[i] - 1);
				cnt++;
			}
		}

		if (cnt >= 2) // 2명 다 주사위 돌림
		{
			m_dDualTime += TimeDelta;
			if (m_dDualTime > 3.f)
			{
				prizeOrder = PRIZE_DUALRESULT;
				m_dDualTime = 0.f;
			}
		}
		break;
	}
	case PRIZE_DUALRESULT:
	{
		// 승자 계산
		if (m_iDualNum[0] > m_iDualNum[1])
		{
			m_pDualPlayer[0]->bRank = 1;
			m_pDualPlayer[1]->bRank = 2;
		}
		else
		{
			m_pDualPlayer[0]->bRank = 2;
			m_pDualPlayer[1]->bRank = 1;
		}

		m_pVs->SetRender(false);
		pDualText[0]->SetHide(true);
		pDualText[1]->SetHide(true);
		for (int i = 0; i < 2; ++i)
		{
			m_pDualDice[i]->SetRender(false);
			m_pDualIcon[i]->SetRender(false);
		}

		m_pScreen->SetRender(true);

		m_xmCameraPos = XMFLOAT3{ -6.13, 6.45, -22.67 };
		m_xmLookAtPos = XMFLOAT3{ -6.13, 5.22, 4.03 };

		prizeOrder = PRIZE_WIN;
		break;
	}
	case PRIZE_WIN:
	{
		m_pFrameTime += TimeDelta;

		if (m_pScreenNum < 24)
		{
			m_xmCameraPos.z += 0.05f;
			m_xmLookAtPos.z += 0.05f;
		}

		if (m_pFrameTime > 0.2)
		{
			if (m_pScreenNum < 24)
			{
				m_pScreen->SetRender(true);
				m_pFrameTime = 0.f;
				m_pScreenNum++;
				m_pScreen->SetTexid(m_pScreenNum);
			}
			else
			{
				if (SERVERCONNECT)
				{
					for (int i = 0; i < 3; ++i)
					{
						if (Players[i]->bRank == 1)
						{
							m_pWinPlayer = Players[i];
						}
					}
				}
				else
				{
					m_pWinPlayer = Player;
				}
				m_pScreen->SetRender(false);
				m_pWinScreen->SetTexid(m_pWinPlayer->GetSkinid());
				m_pWinScreen->SetRender(true);
				wstring w = L"";
				string name = m_pWinPlayer->Get_Name();
				w.assign(name.begin(), name.end());
				pWinnerText->SetTextString(w + L" Win");
				pWinnerText->SetHide(false);
				prizeOrder = PRIZE_WINNER;
				m_pFrameTime = 0.f;
			}
		}
		break;
	}
	case PRIZE_WINNER:
	{
		m_pFrameTime += TimeDelta;
		if (m_pFrameTime > 5.f)
		{
			pWinnerText->SetHide(true);
			m_xmCameraPos.z += 0.05f;
			m_xmLookAtPos.z += 0.05f;
		}
		if (m_pFrameTime > 7.f)
		{
			m_pFloor->SetRender(false);
			if (SERVERCONNECT)
			{
				for (int i = 0; i < 3; ++i)
				{
					Players[i]->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.f, 0.f);
				}
			}
			prizeOrder = PRIZE_PRECREDIT;
			m_pFrameTime = 0.f;
		}
		break;
	}
	case PRIZE_PRECREDIT:
	{
		m_pFrameTime += TimeDelta;

		if (m_pFrameTime < 2.5f)
		{
			m_xmCameraPos.y -= 0.05f;
			m_xmLookAtPos.y -= 0.05f;
		}
		else if (m_pFrameTime < 28.f && m_pFrameTime > 2.5f)
		{
			m_xmCameraPos.y -= 0.01f;
			m_xmLookAtPos.y -= 0.01f;
		}

		if (m_pFrameTime > 2.5f)
		{
			if (m_bTeleCamera)
			{
				m_xmCameraPos.z = 0.4f;
				m_xmLookAtPos.z = 0.5f;
				m_xmCameraPos.y = -4.f;
				m_xmLookAtPos.y = -4.f;
				m_bTeleCamera = false;
			}
		}

		if (m_pFrameTime > 5.f && m_pFrameTime < 6.f)
		{
			m_pTeddyBear->ChangeAnimation("fall2");
			m_pGhost->ChangeAnimation("attack");
		}

		if (m_pFrameTime > 33.f)
		{
			prizeOrder = PRIZE_ENDING;
			m_pFrameTime = 0.f;
		}
		break;
	}
	case PRIZE_ENDING:
	{
		Player->SetScale(XMFLOAT3(2.f, 2.f, 2.f));

		CManagement* pManagement = CManagement::GetInstance();

		if (nullptr == pManagement)
			return -1;

		pManagement->Set_CurrentSceneId(SCENE_WAITINGROOM);

		CSoundManager::GetInstance()->Stop_Channel(CSoundManager::CHANNELID::BGM); //배경음악 재생 종료
		CSoundManager::GetInstance()->Play_Sound(L"Waitroom", 1.f, 0, true); //배경음악 재생

		if (FAILED(pManagement->Initialize_Current_Scene(CScene_WaitingRoom::Create())))
			return -1;

		if (FAILED(pManagement->Clear_Scene(SCENE_PRIZE)))
			return -1;

		if (FAILED(pManagement->Clear_Scene(SCENE_STATIC)))
			return -1;
		break;
	}
	default:
		break;
	}
	return 0;
}

short CScene_Prize::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_Prize::Render_Scene()
{
	return NOERROR;
}


CScene_Prize* CScene_Prize::Create()
{
	CScene_Prize* pInstance = new CScene_Prize();

	if (pInstance->Initialize_Scene())
	{
		MSG_BOX("CScene_Prize Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_Prize::Release()
{
	Safe_Release(m_pLoading);

	delete this;

	return NOERROR;
}
