#include "Scene_WaitingRoom.h"
#include "Management.h"
#include "Scene_Boardmap.h"
#include "ServerManager.h"
#include "Camera_Basic.h"
#include "Logo.h"
#include "StartButton.h"
#include "Light.h"
#include "Texture.h"
#include "Transform.h"
#include "Plane_Buffer.h"
#include "Cube_Buffer.h"
#include "Font.h"
#include "Player.h"
#include "UI.h"
#include "Camera_Fixed.h"
#include "CameraManager.h"
#include "StaticObject.h"

extern bool G_SC;
extern CPlayer* Player;

CScene_WaitingRoom::CScene_WaitingRoom()
{
}

CScene_WaitingRoom::~CScene_WaitingRoom()
{
}

CGameObject* CScene_WaitingRoom::CreateObject(const wchar_t* pLayerTag, const wchar_t* pPrototypeTag, XMFLOAT3 transform, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	CManagement* pManagement = CManagement::GetInstance();

	CGameObject* obj = nullptr;
	ObjWorld temp;
	temp.position = transform;
	temp.rotation = rotation;
	temp.scale = scale;
	if (FAILED(pManagement->Add_GameObjectToLayer(SCENE_WAITINGROOM, pLayerTag, pPrototypeTag, (CGameObject**)&obj, &temp)))
		return nullptr;
	return obj;

}

HRESULT CScene_WaitingRoom::Initialize_Scene()
{
	m_SceneId = SCENEID::SCENE_WAITINGROOM;

	CDeviceManager::GetInstance()->CommandList_Reset(COMMAND_RENDER);


	m_pManagement = CManagement::GetInstance();

	m_pGameLogo = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_RobbyLogo", XMFLOAT3(-0.65f, 0.75f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.3f, 0.2f, 0.1f)));
	
	m_pGameStart = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameStart", XMFLOAT3(-0.65f, 0.35f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.15f, 0.08f, 0.1f)));
	m_pGameStart->SetTexIndex(1);
	m_pGameProfile = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameProfile", XMFLOAT3(-0.65f, 0.15f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.12f, 0.08f, 0.1f)));
	m_pGameCredit = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameCredit", XMFLOAT3(-0.65f, -0.05f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.12f, 0.08f, 0.1f)));
	m_pGameCredit->SetHide(true);
	m_pGameEnd = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameEnd", XMFLOAT3(-0.65f, -0.35f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.08f, 0.08f, 0.1f)));

	m_pGameSearch = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameSearch", XMFLOAT3(0.1, 0.85f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.2f, 0.1f, 0.1f)));
	m_pGameSearch->SetHide(true);
	m_pGameLoading = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameLoading", XMFLOAT3(0.3, 0.85f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.06f, 0.06f, 0.1f)));
	m_pGameLoading->SetHide(true);
	m_pGameBlack = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameBlack", XMFLOAT3(0.13, 0.85f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.24f, 0.1f, 0.1f)));
	m_pGameBlack->SetHide(true);

	m_pGameSelect = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameSelect", XMFLOAT3(0.0, -0.85f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.15f, 0.07f, 0.1f)));
	m_pGameLeft = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameLeft", XMFLOAT3(-0.3, -0.85f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.07f, 0.1f)));
	m_pGameRight = dynamic_cast<CUI*>(CreateObject(L"Layer_BackGround", L"GameObject_GameRight", XMFLOAT3(0.3, -0.85f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.05f, 0.07f, 0.1f)));

	m_pGameSelect->SetHide(true);
	m_pGameLeft->SetHide(true);
	m_pGameRight->SetHide(true);
	//if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_WAITINGROOM, L"Layer_BackGround", L"GameObject_WaitingFont", (CGameObject**)&m_pLoginText)))
	//	return E_FAIL;
	//m_pLoginText->SetText(L"Game Start", XMFLOAT2(0.39f, 0.67f), XMFLOAT2(1.f, 1.f));

	if (SERVERCONNECT == false)
	{
		CTeddyBear* pTeddyBear;
		if (FAILED(m_pManagement->Add_GameObjectToLayer(SCENE_ALL, L"Layer_BackGround", L"GameObject_TeddyBear", (CGameObject**)&pTeddyBear)))
			return E_FAIL;
		::Player->SetObject(pTeddyBear);
		::Player->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.f, 0.f);
		::Player->m_tPlayerInfo.xmLook = XMFLOAT3(0.f, 0.f, 1.f);
	}

	m_pGameBG = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_RobbyBackGround", XMFLOAT3(2.f, 2.2, -0.7), XMFLOAT3(90.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.00600000143051148)));
	m_pGameBG2 = dynamic_cast<CStaticObject*>(CreateObject(L"Layer_BackGround", L"GameObject_RobbyBackGround2", XMFLOAT3(2.f, 2.2, -0.7), XMFLOAT3(90.0, 0.0, 0.0), XMFLOAT3(0.01, 0.0100000023841858, 0.00600000143051148)));
	//CreateObject(L"Layer_BackGround", L"GameObject_RobbyBackground", XMFLOAT3(0.f, 0.f, 0.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(1.5, 1.6, 0.1f));
	m_pGameBG2->SetRender(false);

	CCameraManager::GetInstance()->Set_CurrentCamera("Camera_Fixed");
	CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));

	pCameraFixed->settingCamera(XMFLOAT3(1.7f, 1.85f, 3.5f), XMFLOAT3(1.7f, 1.85f, -1.f), XMFLOAT3(0.f, 1.f, 0.f));

	Player->xmf3Shift = XMFLOAT3(0.f, 0.f, 0.f);

	Player->SetState(OBJECT_STATE::OBJSTATE_GROUND);
	Player->m_tPlayerInfo.xmRight = XMFLOAT3(1.f, 0.f, 0.f);
	Player->m_tPlayerInfo.xmUp = XMFLOAT3(0.f, 1.f, 0.f);
	Player->m_tPlayerInfo.xmLook = XMFLOAT3(0.f, 0.f, 1.f);
	Player->m_tPlayerInfo.xmPosition = XMFLOAT3(0, 0, 0);
	m_pLoading = CLoading::Create(SCENE_BOARDMAP);

	if (nullptr == m_pLoading)
		return E_FAIL;

	CServerManager::GetInstance()->Set_Disconnect(false);
	CServerManager::GetInstance()->Set_Matching(false);
	CServerManager::GetInstance()->Init_TiedPlayer();
	CServerManager::GetInstance()->Set_PlayerNum(0);
	CServerManager::GetInstance()->Set_MatchTieCount(0);
	CDeviceManager::GetInstance()->CommandList_Close(COMMAND_RENDER);

	m_bMatchClick = false;
	ShowCursor(true);
	return NOERROR;
}

short CScene_WaitingRoom::KeyEvent(double TimeDelta)
{
	CKeyManager* pKeyMgr = CKeyManager::GetInstance();

	if (pKeyMgr->KeyDown(KEY_LBUTTON))
	{
		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos;

		GetCursorPos(&ptCursorPos);
		::ScreenToClient(g_hWnd, &ptCursorPos);
		//GameStart
		if (ptCursorPos.x >= 124.f && ptCursorPos.x <= 320.f && ptCursorPos.y >= 206.f && ptCursorPos.y <= 260.f && m_pGameStart->GetHide() == false)
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");

			if (false == m_pLoading->Get_Finish())
				return 0;

			if (Player == nullptr)
				return 0;
			if (m_bMatchClick == false) {
				Player->bButton = 0;
				Player->bCoin = 20;

				if (SERVERCONNECT) {
					packet_matching p;
					p.size = sizeof(packet_matching);
					p.type = C2S_MATCHINGSTART;
					p.id = Player->Get_ClientIndex();
					p.skinid = Player->GetSkinid();

					CServerManager::GetInstance()->SendData(C2S_MATCHINGSTART, &p);
				}
				m_bMatchClick = true;
			}
			else {
				CManagement* pManagement = CManagement::GetInstance();

				if (nullptr == pManagement)
					return -1;

				Player->m_tPlayerInfo.xmPosition = XMFLOAT3(-9.f, -13.6f, -80.5f);

				pManagement->Set_CurrentSceneId(SCENE_BOARDMAP);

				if (FAILED(pManagement->Initialize_Current_Scene(CScene_Boardmap::Create())))
					return -1;

				if (FAILED(pManagement->Clear_Scene(SCENE_WAITINGROOM)))
					return -1;
			}
			if(m_pGameSearch)
				m_pGameSearch->SetHide(false);
			m_pGameBlack->SetHide(false);
			m_pGameLoading->SetHide(false);

			m_pGameProfile->SetTexIndex(1);
		}

		//Quit
		if (ptCursorPos.x >= 176.f && ptCursorPos.x <= 275.f && ptCursorPos.y >= 459.f && ptCursorPos.y <= 515.f && m_pGameEnd->GetHide() == false)
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
			DestroyWindow(g_hWnd);
		}

		//Profile
		if (ptCursorPos.x >= 149.f && ptCursorPos.x <= 300.f && ptCursorPos.y >= 282.f && ptCursorPos.y <= 332.f && m_pGameProfile->GetHide() == false && m_pGameProfile->GetTexIndex() == 0)
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
			//CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));

			//pCameraFixed->settingCamera(XMFLOAT3(0.f, 1.f, 5.f), XMFLOAT3(0.f, 1.f, -1.f), XMFLOAT3(0.f, 1.f, 0.f));

			m_pGameLogo->SetHide(true);
			m_pGameStart->SetHide(true);
			m_pGameProfile->SetHide(true);
			m_pGameCredit->SetHide(true);
			m_pGameEnd->SetHide(true);

			m_pGameSelect->SetHide(false);
			m_pGameLeft->SetHide(false);
			m_pGameRight->SetHide(false);

			m_pGameBG->SetRender(false);
			m_pGameBG2->SetRender(true);

			::Player->m_tPlayerInfo.xmPosition = XMFLOAT3(1.7f, 0.f, 0.f);
		}

		//프로필 영역
		//Select
		if (ptCursorPos.x >= 542.f && ptCursorPos.x <= 737.f && ptCursorPos.y >= 642.f && ptCursorPos.y <= 690.f && m_pGameSelect->GetHide() == false)
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
			//CCamera_Fixed* pCameraFixed = dynamic_cast<CCamera_Fixed*>(CCameraManager::GetInstance()->Find_Camera("Camera_Fixed"));

			//pCameraFixed->settingCamera(XMFLOAT3(1.7f, 1.85f, 3.5f), XMFLOAT3(1.7f, 1.85f, -1.f), XMFLOAT3(0.f, 1.f, 0.f));

			m_pGameLogo->SetHide(false);
			m_pGameStart->SetHide(false);
			m_pGameProfile->SetHide(false);
			m_pGameCredit->SetHide(true);
			m_pGameEnd->SetHide(false);

			m_pGameSelect->SetHide(true);
			m_pGameLeft->SetHide(true);
			m_pGameRight->SetHide(true);

			m_pGameBG->SetRender(true);
			m_pGameBG2->SetRender(false);

			::Player->m_tPlayerInfo.xmPosition = XMFLOAT3(0.f, 0.f, 0.f);
		}

		//Left
		if (ptCursorPos.x >= 416.f && ptCursorPos.x <= 479.f && ptCursorPos.y >= 642.f && ptCursorPos.y <= 690.f && m_pGameLeft->GetHide() == false)
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
			int skin = Player->GetSkinid();
			skin--;
			if (skin < 0)
				skin = 0;
			Player->SetSkinid(skin);
		}

		//Right
		if (ptCursorPos.x >= 799.f && ptCursorPos.x <= 863.f && ptCursorPos.y >= 642.f && ptCursorPos.y <= 690.f && m_pGameRight->GetHide() == false)
		{
			CSoundManager::GetInstance()->Play_Sound(L"ButtonClick");
			int skin = Player->GetSkinid();
			skin++;
			if (skin > 3)
				skin = 3;
			Player->SetSkinid(skin);
		}
	}
	return NOERROR;
}

short CScene_WaitingRoom::Update_Scene(double TimeDelta)
{
	m_fFrameTime += TimeDelta;
	if (m_fFrameTime > 0.1f)
	{
		if (m_pGameLoading->GetHide() == false)
		{
			m_pGameLoading->SetTexIndex(m_iLoadingIndex);
			m_iLoadingIndex++;
			if (m_iLoadingIndex > 11)
				m_iLoadingIndex = 0;
		}
		m_fFrameTime = 0.f;
	}
	POINT ptCursorPos;
	GetCursorPos(&ptCursorPos);
	::ScreenToClient(g_hWnd, &ptCursorPos);
	
	if (ptCursorPos.x >= 124.f && ptCursorPos.x <= 320.f && ptCursorPos.y >= 206.f && ptCursorPos.y <= 260.f)
	{
		if (m_bGameStartScale == false) {
			m_pGameStart->SetScale(XMFLOAT3(1.2f, 1.2f, 0.1f));
			m_bGameStartScale = true;
		}
	}
	else
	{
		if (m_bGameStartScale == true) {
			m_pGameStart->SetScale(XMFLOAT3(0.83333f, 0.83333f, 0.1f));
			m_bGameStartScale = false;
		}
	}

	if (ptCursorPos.x >= 149.f && ptCursorPos.x <= 300.f && ptCursorPos.y >= 282.f && ptCursorPos.y <= 332.f)
	{
		if (m_bGameProfileScale == false) {
			m_pGameProfile->SetScale(XMFLOAT3(1.2f, 1.2f, 0.1f));
			m_bGameProfileScale = true;
		}
	}
	else
	{
		if (m_bGameProfileScale == true) {
			m_pGameProfile->SetScale(XMFLOAT3(0.83333f, 0.83333f, 0.1f));
			m_bGameProfileScale = false;
		}
	}

	if (ptCursorPos.x >= 149.f && ptCursorPos.x <= 297.f && ptCursorPos.y >= 351.f && ptCursorPos.y <= 404.f)
	{
		if (m_bGameCreditScale == false) {
			m_pGameCredit->SetScale(XMFLOAT3(1.2f, 1.2f, 0.1f));
			m_bGameCreditScale = true;
		}
	}
	else
	{
		if (m_bGameCreditScale == true) {
			m_pGameCredit->SetScale(XMFLOAT3(0.83333f, 0.83333f, 0.1f));
			m_bGameCreditScale = false;
		}
	}

	if (ptCursorPos.x >= 176.f && ptCursorPos.x <= 275.f && ptCursorPos.y >= 459.f && ptCursorPos.y <= 515.f)
	{
		if (m_bGameEndScale == false) {
			m_pGameEnd->SetScale(XMFLOAT3(1.2f, 1.2f, 0.1f));
			m_bGameEndScale = true;
		}
	}
	else
	{
		if (m_bGameEndScale == true) {
			m_pGameEnd->SetScale(XMFLOAT3(0.83333f, 0.83333f, 0.1f));
			m_bGameEndScale = false;
		}
	}

	if (SERVERCONNECT) {

		if (GetKeyState(VK_SPACE) & 0x8000 &&
			true == m_pLoading->Get_Finish())
		{
			/*
			if (Player == nullptr)
				return 0;

			packet_matching p;
			p.size = sizeof(packet_matching);
			p.type = C2S_MATCHINGSTART;
			p.id = Player->Get_ClientIndex();

			CServerManager::GetInstance()->SendData(C2S_MATCHINGSTART, &p);
			*/
		}

		if (CServerManager::GetInstance()->Get_Matching()) //큐잡힘 -> 보드맵으로
		{
			CManagement* pManagement = CManagement::GetInstance();

			if (nullptr == pManagement)
				return -1;

			pManagement->Set_CurrentSceneId(SCENE_BOARDMAP);

			if (FAILED(pManagement->Initialize_Current_Scene(CScene_Boardmap::Create())))
				return -1;

			if (FAILED(pManagement->Clear_Scene(SCENE_WAITINGROOM)))
				return -1;
		}
	}
	if (true == m_pLoading->Get_Finish() && m_pGameStart->GetTexIndex() == 1)
	{
		m_pGameStart->SetTexIndex(0);
	}


	return 0;
}

short CScene_WaitingRoom::LateUpdate_Scene(double TimeDelta)
{
	return 0;
}

HRESULT CScene_WaitingRoom::Render_Scene()
{
	return NOERROR;
}


CScene_WaitingRoom* CScene_WaitingRoom::Create()
{
	CScene_WaitingRoom* pInstance = new CScene_WaitingRoom();

	if (pInstance->Initialize_Scene())
	{
		MSG_BOX("CScene_WaitingRoom Created Failed");
		Safe_Release(pInstance);
	}

	return pInstance;
}

HRESULT CScene_WaitingRoom::Release()
{
	Safe_Release(m_pLoading);

	delete this;

	return NOERROR;
}
