#ifndef __TEXTRUESHADER__
#define __TEXTRUESHADER__

#include "Shader.h"

class CTextureShader final : public CShader
{
private:
	explicit CTextureShader();
	explicit CTextureShader(const CTextureShader& rhs);
	virtual ~CTextureShader(void);

public:
	virtual HRESULT Initialize_Component_Prototype();
	virtual HRESULT Initialize_Component(void* pArg);

	virtual CComponent* Clone_Component(void* pArg);
	static	CTextureShader* Create();

	HRESULT Release();

public:
	virtual D3D12_SHADER_BYTECODE Create_VertexShader(ID3DBlob** ppBlob);
	virtual D3D12_SHADER_BYTECODE Create_PixelShader(ID3DBlob** ppBlob);
	virtual D3D12_INPUT_LAYOUT_DESC Create_InputLayout();

};

#endif
