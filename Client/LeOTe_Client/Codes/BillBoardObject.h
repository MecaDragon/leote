#ifndef __BillBoardObject__
#define __BillBoardObject__

#include "stdafx.h"
#include "GameObject.h"

class CTransform;
class CPlane_Buffer;
class CShader;
class CTexture;
class CRenderer;

struct CGeometryBillboardVertex
{
	XMFLOAT4						m_xmf3Position;
	XMFLOAT2						m_xmf2Size;
	XMFLOAT2						m_xmf2ClipSize;
	XMFLOAT2						m_xmf2StartPos;
	UINT							m_nIndexX;
	UINT							m_nIndexY;
};

class CBillBoardObject : public CGameObject
{
private:
	explicit CBillBoardObject();
	explicit CBillBoardObject(const CBillBoardObject& rhs);
	virtual ~CBillBoardObject(void) = default;

public:
	virtual HRESULT Initialize_GameObject_Prototype();
	virtual HRESULT Initialize_GameObject(void* pArg);
	virtual short Update_GameObject(double TimeDelta);
	virtual short LateUpdate_GameObject(double TimeDelta);
	virtual HRESULT Render_GameObject();

	virtual CGameObject* Clone_GameObject(void* pArg);

	static CBillBoardObject* Create();

	HRESULT Release();

	HRESULT SetTexture(const wchar_t* texturename, SCENEID scene = SCENEID::SCENE_BOARDMAP);

	void SetPostion(XMFLOAT3 xmPosition);
	void SetSize(XMFLOAT2 xmSize);
	void SetStartPos(XMFLOAT2 xmPos);
	void SetClipSize(XMFLOAT2 xmSize);
	void SetIndexX(int index);
	XMFLOAT3 GetPostion();
	void GiveIndexX(int value);
	int GetIndexX();

	void SetIndexY(int index);
	void GiveIndexY(int value);
	int GetIndexY();

	CGeometryBillboardVertex* pGeometryBillboardVertices;
	CGeometryBillboardVertex* pGeometryBillboardConstants = nullptr;

private:
	HRESULT Add_Component();

private:
	CShader* m_pShaderCom = nullptr;
	CTexture* m_pTextureCom = nullptr;
	CRenderer* m_pRendererCom = nullptr;

	ID3D12Resource* m_pGameObjResource = nullptr;
	ID3D12Resource* m_pd3dPositionBuffer = NULL;
	ID3D12Resource* m_pd3dPositionUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		m_d3dPositionBufferView;
	UINT							m_nStride = 0;

	bool m_bStoreItem = false;
	bool m_bBuyItem = false;

	float m_fTimer = 0.f;

	float m_fSpeed = 1.f;

	bool m_bHide = false;

public:
	void setStoreItem(bool bStoreItem) { m_bStoreItem = bStoreItem; }
	bool getStoreItem() { return m_bStoreItem; }

	void setBuyItem(bool bBuyItem) { m_bBuyItem = bBuyItem; }
	bool getBuyItem() { return m_bBuyItem; }

	void setHide(bool bHide) { m_bHide = bHide; }
	bool getHide() { return m_bHide; }
};

#endif