#pragma once

#include "Component.h"

class CRenderer final : public CComponent
{
public:
	enum RENDERID { RENDER_PRIORITY, RENDER_NONALPHA, RENDER_ALPHA, RENDER_UI, RENDER_END };

private:
	explicit CRenderer();
	virtual ~CRenderer(void);

public:
	virtual HRESULT Initialize_Component_Prototype();

	virtual CComponent* Clone_Component(void* pArg);
	static	CRenderer* Create();

	HRESULT Release();

public:
	HRESULT Add_RenderGroup(RENDERID eRenderID, CGameObject* pGameObject);
	HRESULT Draw_RenderGroup();

private:
	HRESULT Render_Priority();
	HRESULT Render_NonAlpha();
	HRESULT Render_Alpha();
	HRESULT Render_UI();

private:
	list<CGameObject*>			m_RenderGroup[RENDER_END];
	typedef list<CGameObject*>	RENDERGROUP;


};

