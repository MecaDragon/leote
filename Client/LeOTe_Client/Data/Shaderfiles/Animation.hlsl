#include "Light.hlsl"

struct VertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float3 BinormalL : BINORMAL;
	float3 TargentL : TANGENT;
	float2 TexC    : TEXCOORD;
	float3 BoneWeights : WEIGHTS;
	uint4 BoneIndices  : BONEINDICES;
};

struct VertexOut
{
	float4 PosH    : SV_POSITION;
	float3 PosW    : POSITION;
	float3 NormalW : NORMAL;
	float3 BinormalW : BINORMAL;
	float3 TargentW : TANGENT;
	float2 TexC    : TEXCOORD;
	uint4 BoneIndices : BONEINDICES;
	float4 shadowPosition : TEXCOORD3;
};

VertexOut VSFBX(VertexIn vin)
{

	VertexOut vout = (VertexOut)0.0f;

	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = vin.BoneWeights.x;
	weights[1] = vin.BoneWeights.y;
	weights[2] = vin.BoneWeights.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	float3 posL = float3(0.0f, 0.0f, 0.0f);
	float3 normalL = float3(0.0f, 0.0f, 0.0f);
	float3 binormalL = float3(0.0f, 0.0f, 0.0f);
	float3 tangentL = float3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < 4; ++i)
	{
		posL += weights[i] * mul(float4(vin.PosL, 1.0f), gBoneTransforms[vin.BoneIndices[i]]).xyz;
		normalL += weights[i] * mul(vin.NormalL, (float3x3)gBoneTransforms[vin.BoneIndices[i]]).xyz;
		binormalL += weights[i] * mul(vin.BinormalL, (float3x3)gBoneTransforms[vin.BoneIndices[i]]).xyz;
		tangentL += weights[i] * mul(vin.TargentL, (float3x3)gBoneTransforms[vin.BoneIndices[i]]).xyz;
		/*
		normalL += weights[i] * mul(float4(vin.NormalL, 1.0f), gBoneTransforms[vin.BoneIndices[i]]).xyz;
		binormalL += weights[i] * mul(float4(vin.BinormalL, 1.0f), gBoneTransforms[vin.BoneIndices[i]]).xyz;
		tangentL += weights[i] * mul(float4(vin.TargentL, 1.0f), gBoneTransforms[vin.BoneIndices[i]]).xyz;
		*/
	}

	vin.PosL = posL;
	vin.NormalL = normalL;
	vin.BinormalL = binormalL;
	vin.TargentL = tangentL;

	vout.BoneIndices = vin.BoneIndices;

	float4 posW = mul(float4(vin.PosL, 1.0f), gChaWorld);
	vout.PosW = posW.xyz;
	vout.NormalW =  mul(vin.NormalL, (float3x3)gChaWorld);
	vout.BinormalW = mul(vin.BinormalL, (float3x3)gChaWorld);
	vout.TargentW = mul(vin.TargentL, (float3x3)gChaWorld);
	/*
	vout.NormalW = mul(float4(vin.NormalL, 1.0f), gChaWorld).xyz;
	vout.BinormalW = mul(float4(vin.BinormalL, 1.0f), gChaWorld).xyz;
	vout.TargentW = mul(float4(vin.TargentL, 1.0f), gChaWorld).xyz;
	*/
	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);

	//vout.TexC = mul(texC, gMatTransform).xy;
	vout.TexC = texC.xy;
	// Transform to homogeneous clip space.
	vout.PosH = mul(posW, mul(gmtxView, gmtxProjection));

	/*
	matrix shadowProject = mul(mul(gChaWorld, gmtxShadowTransform), gChaTexTransform);
	vout.shadowPosition = mul(float4(vin.PosL, 1.0f), shadowProject);
	*/
	vout.shadowPosition = mul(posW, gmtxShadowTransform);
	return vout;
}

float4 PSFBX(VertexOut pin) : SV_Target
{
	
	float4 diffuseAlbedo = gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, pin.TexC);
	//float4 diffuseAlbedo = float4(pin.Binormal, 0.0f) * 2.0f;
	//float4 diffuseAlbedo = gNormalMap.Sample(gsamAnisotropicWrap, pin.TexC);
	//float4 normalMap = gNormalMap.Sample(gsamAnisotropicWrap, pin.TexC);
	// 0.0f ~ 1.0f -> -1.0f ~ 1.0f

	//normalMap = (normalMap * 2.0f) - 1.0f;



	//float3 normal = normalMap.xyz * pin.NormalW;
	//float3 normal = mul(pin.NormalW, normalMap.xyz);
	//pin.NormalW = normalize(normal);
	//pin.NormalW = normalize(pin.NormalW);

	//float3 toEyeW = gEyePosW - pin.PosW;
	//float distanceToEye = length(toEyeW);
	//toEyeW /= distanceToEye; // Normalize

	//float4 ambient = gAmbientLight * diffuseAlbedo;

	//const float shininess = 1.0f - gRoughness;
	//Material mat = { diffuseAlbedo, gFresnelR0, shininess };
	//float3 shadowFactor = 1.0f;
	//float4 directLight = ComputeLighting(gLights, mat, pin.PosW,
	//	pin.NormalW, toEyeW, shadowFactor);

	//float4 litColor = ambient + directLight;
	/*
		float fogAmount = saturate((distanceToEye - gFogStart) / gFogRange);
		litColor = lerp(litColor, gFogColor, fogAmount);

		litColor.a = diffuseAlbedo.a;

		if (diffuseAlbedo.a == 0.0f)
		{
			discard;
		}
	return litColor;*/

	//float4 cColor = float4(0.0f, 1.0f, 0.0f, 1.0f);

	float3x3 TBN = float3x3(normalize(pin.TargentW), normalize(pin.BinormalW), normalize(pin.NormalW));

	float4 cNormal = gtxtNormalTexture.SampleLevel(gsamAnisotropicWrap, pin.TexC, 0);
	float3 vNormal = normalize(cNormal.rgb * 2.0f - 1.0f); //[0, 1] �� [-1, 1]
	float3 normalW = normalize(mul(vNormal, TBN));

	/*
	pin.shadowPosition.xyz /= pin.shadowPosition.w;
	float fShadowFactor = 0.3f, fBias = 0.006f;
	float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, pin.shadowPosition.xy).r;
	if (pin.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;
	*/
	float3 shadowFactor = float3(1.0f, 1.0f, 1.0f);
	shadowFactor = CalcShadowFactor(pin.shadowPosition);
	//float4 cIllumination = Lighting(pin.PosW, normalW, shadowFactor[0]);
	//return cIllumination;
	float4 cIllumination = Lighting(pin.PosW, normalize(pin.NormalW), shadowFactor);
	float4 result;
	if (gLights[2].m_bEnable)
	{
		result = lerp(diffuseAlbedo, cIllumination, 0.1f);
	}
	else
	{
		result = diffuseAlbedo * cIllumination;
	}
	return result;
	//return(diffuseAlbedo);
}