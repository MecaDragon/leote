
#include "Light.hlsl"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	//vector		vColor : COLOR0;
};

VS_TEXTURED_OUTPUT VSTextured(VS_TEXTURED_INPUT input, uint vertexID : SV_VertexID)
{
	VS_TEXTURED_OUTPUT output;
	float2 uv = float2(vertexID & 1, (vertexID >> 1) & 1);
	//output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxGameObject), gmtxView), gmtxProjection);
	/*
	output.position = float4(input.position.x + (input.position.z * uv.x), input.position.y - (input.position.w * uv.y), 0, 1);
	output.uv = float2(input.uv.x + (input.uv.z * uv.x), input.uv.y + (input.uv.w * uv.y));
	*/
	float4 pos = mul(float4(input.position, 1.0f), gChaWorld);
	output.position = float4(pos.x, pos.y, 0, 1);
	output.uv = input.uv;

	return(output);
}

float4 PSTextured(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
	float3 uvw = float3(input.uv, 0);
	float4 cColor = gtxtAlbedoTexture.Sample(gsamLinearWrap, uvw);
	//cColor.a = input.uv.y;
	if (cColor.a <= 0.3f) discard;
	return(cColor);
}