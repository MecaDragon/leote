
#include "Light.hlsl"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//struct VertexIn
//{
//	float3 PosL    : POSITION;
//	float3 NormalL : NORMAL;
//	float2 TexC    : TEXCOORD;
//	float3 TangentU : TANGENT;
//};
//
//struct VertexOut
//{
//	float4 PosH    : SV_POSITION;
//	float3 PosW    : POSITION;
//	float3 NormalW : NORMAL;
//	float3 TangentW : TANGENT;
//	float2 TexC    : TEXCOORD;
//};

//VertexOut VS(VertexIn vin)
//{
//	VertexOut vout = (VertexOut)0.0f;
//
//	// Fetch the material data.
//	MaterialData matData = gMaterialData[gMaterialIndex];
//
//	// Transform to world space.
//	float4 posW = mul(float4(vin.PosL, 1.0f), gWorld);
//	vout.PosW = posW.xyz;
//
//	// Assumes nonuniform scaling; otherwise, need to use inverse-transpose of world matrix.
//	vout.NormalW = mul(vin.NormalL, (float3x3)gWorld);
//
//	vout.TangentW = mul(vin.TangentU, (float3x3)gWorld);
//
//	// Transform to homogeneous clip space.
//	vout.PosH = mul(posW, gViewProj);
//
//	// Output vertex attributes for interpolation across triangle.
//	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gTexTransform);
//	vout.TexC = mul(texC, matData.MatTransform).xy;
//
//	return vout;
//}
//
//float4 PS(VertexOut pin) : SV_Target
//{
//	// Fetch the material data.
//	MaterialData matData = gMaterialData[gMaterialIndex];
//	float4 diffuseAlbedo = matData.DiffuseAlbedo;
//	float3 fresnelR0 = matData.FresnelR0;
//	float  roughness = matData.Roughness;
//	uint diffuseMapIndex = matData.DiffuseMapIndex;
//	uint normalMapIndex = matData.NormalMapIndex;
//
//	// Interpolating normal can unnormalize it, so renormalize it.
//	pin.NormalW = normalize(pin.NormalW);
//
//	float4 normalMapSample = gTextureMaps[normalMapIndex].Sample(gsamAnisotropicWrap, pin.TexC);
//	float3 bumpedNormalW = NormalSampleToWorldSpace(normalMapSample.rgb, pin.NormalW, pin.TangentW);
//
//	// Uncomment to turn off normal mapping.
//	//bumpedNormalW = pin.NormalW;
//
//	// Dynamically look up the texture in the array.
//	diffuseAlbedo *= gTextureMaps[diffuseMapIndex].Sample(gsamAnisotropicWrap, pin.TexC);
//
//	// Vector from point being lit to eye. 
//	float3 toEyeW = normalize(gEyePosW - pin.PosW);
//
//	// Light terms.
//	float4 ambient = gAmbientLight * diffuseAlbedo;
//
//	const float shininess = (1.0f - roughness) * normalMapSample.a;
//	Material mat = { diffuseAlbedo, fresnelR0, shininess };
//	float3 shadowFactor = 1.0f;
//	float4 directLight = ComputeLighting(gLights, mat, pin.PosW,
//		bumpedNormalW, toEyeW, shadowFactor);
//
//	float4 litColor = ambient + directLight;
//
//	// Add in specular reflections.
//	float3 r = reflect(-toEyeW, bumpedNormalW);
//	float4 reflectionColor = gCubeMap.Sample(gsamLinearWrap, r);
//	float3 fresnelFactor = SchlickFresnel(fresnelR0, bumpedNormalW, r);
//	litColor.rgb += shininess * fresnelFactor * reflectionColor.rgb;
//
//	// Common convention to take alpha from diffuse albedo.
//	litColor.a = diffuseAlbedo.a;
//
//	return litColor;
//}

struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	//vector		vColor : COLOR0;
};

VS_TEXTURED_OUTPUT VSTextured(VS_TEXTURED_INPUT input)
{
	VS_TEXTURED_OUTPUT output;

	//output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxGameObject), gmtxView), gmtxProjection);
	output.position = mul(mul(mul(float4(input.position, 1.0f), gChaWorld), gmtxView), gmtxProjection);
	output.uv = input.uv;

	return(output);
}

float4 PSTextured(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
	float3 uvw = float3(input.uv, 0);

	float4 cColor = gtxtAlbedoTexture.Sample(gsamLinearWrap, uvw);
	//cColor.a = input.uv.y;
	if (cColor.a <= 0.3f) discard;
	return(cColor);
}



//#define _WITH_VERTEX_LIGHTING

struct VS_LIGHTING_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
};

struct VS_LIGHTING_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	//	nointerpolation float3 normalW : NORMAL;
#ifdef _WITH_VERTEX_LIGHTING
	float4 color : COLOR;
#else
	float3 normalW : NORMAL;
#endif
	float4 shadowPosition : TEXCOORD3;
};

VS_LIGHTING_OUTPUT VSLighting(VS_LIGHTING_INPUT input)
{
	VS_LIGHTING_OUTPUT output;

	output.positionW = (float3)mul(float4(input.position, 1.0f), gChaWorld);
	output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
	float3 normalW = mul(input.normal, (float3x3)gChaWorld);

	/*
	matrix shadowProject = mul(mul(gChaWorld, gmtxShadowTransform), gChaTexTransform);
	output.shadowPosition = mul(float4(input.position, 1.0f), shadowProject);
	*/
	output.shadowPosition = mul(input.position, gmtxShadowTransform);
#ifdef _WITH_VERTEX_LIGHTING
	/*
	output.shadowPosition.xyz /= vout.shadowPosition.w;
	float fShadowFactor = 0.3f, fBias = 0.006f;
	float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, output.shadowPosition.xy).r;
	if (output.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;

	output.color = Lighting(output.positionW, normalize(normalW), fShadowFactor);
	*/
#else
	output.normalW = normalW;
#endif
	return(output);
}

float4 PSLighting(VS_LIGHTING_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
	return(input.color);
#else
	//	float3 normalW = normalize(input.normalW);
	//	float3 cNormal = normalW * 0.5f + 0.5f;
	//	float4 color = float4(cNormal, 0.0f);
		float3 normalW = normalize(input.normalW);

		/*
		input.shadowPosition.xyz /= input.shadowPosition.w;
		float fShadowFactor = 0.3f, fBias = 0.006f;
		float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, input.shadowPosition.xy).r;
		if (input.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;
		*/

		float3 shadowFactor = float3(1.0f, 1.0f, 1.0f);
		shadowFactor = CalcShadowFactor(input.shadowPosition);
		float4 color = Lighting(input.positionW, normalW, shadowFactor);
		return(color);
	#endif
}