#include "Light.hlsl"


struct VS_FBXTEX_INPUT
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC    : TEXCOORD;
};

struct VS_FBXTEX_OUTPUT
{
	float4 PosH    : SV_POSITION;
	float3 PosW    : POSITION;
	float2 TexC    : TEXCOORD;
	float4 color : COLOR;
	float3 NormalW : NORMAL;
	float4 shadowPosition : TEXCOORD3;
};

VS_FBXTEX_OUTPUT VSFBXTexture(VS_FBXTEX_INPUT vin)
{
	VS_FBXTEX_OUTPUT vout = (VS_FBXTEX_OUTPUT)0.0f;

	float4 posW = mul(float4(vin.PosL, 1.0f), gChaWorld);
	vout.PosW = posW.xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)gChaWorld);
	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);
	vout.TexC = texC.xy;
	vout.PosH = mul(mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxView), gmtxProjection);

	return(vout);
}

float4 PSFBXTexture(VS_FBXTEX_OUTPUT input) : SV_TARGET
{

	float4 cAlbedoColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
	cAlbedoColor = gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, input.TexC);

	return cAlbedoColor;

}

VS_FBXTEX_OUTPUT VSFBXTextureLight(VS_FBXTEX_INPUT vin)
{
	VS_FBXTEX_OUTPUT vout = (VS_FBXTEX_OUTPUT)0.0f;

	float4 posW = mul(float4(vin.PosL, 1.0f), gChaWorld);
	vout.PosW = posW.xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)gChaWorld);
	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);
	vout.TexC = texC.xy;
	vout.PosH = mul(mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxView), gmtxProjection);
	vout.shadowPosition = mul(posW, gmtxShadowTransform);
	return(vout);
}

float4 PSFBXTextureLight(VS_FBXTEX_OUTPUT input) : SV_TARGET
{

	float4 cAlbedoColor = gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, input.TexC);

	
	float3 shadowFactor = float3(0.0f, 0.0f, 0.0f);
	shadowFactor = CalcShadowFactor(input.shadowPosition);
	//float4 cIllumination = Lighting(pin.PosW, normalW, shadowFactor[0]);
	//return cIllumination;
	float4 cIllumination = Lighting(input.PosW, normalize(input.NormalW), shadowFactor);

	if (cAlbedoColor.a <= 0.3f) discard;

	return cAlbedoColor * cIllumination;
	float4 result;
	if (gLights[2].m_bEnable)
	{
		result = lerp(cAlbedoColor, cIllumination, 0.1f);
	}
	else
	{
		result = cAlbedoColor * cIllumination;
	}
	return result;
}

float4 PSFBXTextureLLight(VS_FBXTEX_OUTPUT input) : SV_TARGET
{

	float4 cAlbedoColor = gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, input.TexC);


	float3 shadowFactor = float3(0.0f, 0.0f, 0.0f);
	shadowFactor = CalcShadowFactor(input.shadowPosition);
	//float4 cIllumination = Lighting(pin.PosW, normalW, shadowFactor[0]);
	//return cIllumination;
	float4 cIllumination = Lighting(input.PosW, normalize(input.NormalW), shadowFactor);

	if (cAlbedoColor.a <= 0.3f) discard;

	return lerp(cAlbedoColor, cIllumination, 0.1f);
}

float4 PSFBXTextureMLight(VS_FBXTEX_OUTPUT input) : SV_TARGET
{

	float4 cAlbedoColor = gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, input.TexC);


	float3 shadowFactor = float3(0.0f, 0.0f, 0.0f);
	shadowFactor = CalcShadowFactor(input.shadowPosition);
	//float4 cIllumination = Lighting(pin.PosW, normalW, shadowFactor[0]);
	//return cIllumination;
	float4 cIllumination = Lighting(input.PosW, normalize(input.NormalW), shadowFactor);

	if (cAlbedoColor.a <= 0.3f) discard;

	return lerp(cAlbedoColor, cIllumination, 0.4f);
}