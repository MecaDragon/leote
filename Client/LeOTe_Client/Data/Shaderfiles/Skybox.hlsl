#include "Light.hlsl"


struct VS_SKYBOX_INPUT
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC    : TEXCOORD;
};

struct VS_SKYBOX_OUTPUT
{
	float4 PosH    : SV_POSITION;
	float3 PosW    : POSITION;
	float2 TexC    : TEXCOORD;
	float4 color : COLOR;
	float3 NormalW : NORMAL;
};

VS_SKYBOX_OUTPUT VSSkyBox(VS_SKYBOX_INPUT vin)
{
	VS_SKYBOX_OUTPUT vout = (VS_SKYBOX_OUTPUT)0.0f;

	float4 posW = mul(float4(vin.PosL, 1.0f), gChaWorld);
	vout.PosW = posW.xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)gChaWorld);
	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);
	vout.TexC = texC.xy;
	vout.PosH = mul(mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxView), gmtxProjection);

	return(vout);
}

float4 PSSkyBox(VS_SKYBOX_OUTPUT input) : SV_TARGET
{

	float4 cAlbedoColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
	cAlbedoColor = gtxtSkyBoxTexture.Sample(gsamAnisotropicWrap, input.TexC);

	return cAlbedoColor;

}