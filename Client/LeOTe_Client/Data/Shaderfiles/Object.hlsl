
#include "Light.hlsl"

struct VertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float3 BinormalL : BINORMAL;
	float3 TargentL : TANGENT;
	float2 TexC    : TEXCOORD;
	//float3 BoneWeights : WEIGHTS;
	//uint4 BoneIndices  : BONEINDICES;
};

struct VertexOut
{
	float4 PosH    : SV_POSITION;
	float3 PosW    : POSITION;
	float3 NormalW : NORMAL;
	float3 BinormalW : BINORMAL;
	float3 TargentW : TANGENT;
	float2 TexC    : TEXCOORD;
	//uint4 BoneIndices : BONEINDICES;
};

VertexOut VSOBJ(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;
	//vin.PosL = posL;
	//vin.NormalL = normalL;

	float4 posW = mul(float4(vin.PosL, 1.0f), gChaWorld);
	vout.PosW = posW.xyz;

	vout.NormalW = mul(vin.NormalL, (float3x3)gChaWorld);

	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);

	//vout.TexC = mul(texC, gMatTransform).xy;
	vout.TexC = texC.xy;
	vout.PosH = mul(mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxView), gmtxProjection);
	return vout;
}

float4 PSOBJ(VertexOut pin) : SV_Target
{
	/*
	float4 diffuseAlbedo = gDiffuseMap.Sample(gsamAnisotropicWrap, pin.TexC) * gDiffuseAlbedo;
	//float4 diffuseAlbedo = float4(pin.Binormal, 0.0f) * 2.0f;
	//float4 diffuseAlbedo = gNormalMap.Sample(gsamAnisotropicWrap, pin.TexC);
	float4 normalMap = gNormalMap.Sample(gsamAnisotropicWrap, pin.TexC);
	// 0.0f ~ 1.0f -> -1.0f ~ 1.0f

	normalMap = (normalMap * 2.0f) - 1.0f;



	//float3 normal = normalMap.xyz * pin.NormalW;
	float3 normal = mul(pin.NormalW, normalMap.xyz);
	pin.NormalW = normalize(normal);
	//pin.NormalW = normalize(pin.NormalW);

	float3 toEyeW = gEyePosW - pin.PosW;
	float distanceToEye = length(toEyeW);
	toEyeW /= distanceToEye; // Normalize

	float4 ambient = gAmbientLight * diffuseAlbedo;

	const float shininess = 1.0f - gRoughness;
	Material mat = { diffuseAlbedo, gFresnelR0, shininess };
	float3 shadowFactor = 1.0f;
	float4 directLight = ComputeLighting(gLights, mat, pin.PosW,
		pin.NormalW, toEyeW, shadowFactor);

	float4 litColor = ambient + directLight;
	/*
		float fogAmount = saturate((distanceToEye - gFogStart) / gFogRange);
		litColor = lerp(litColor, gFogColor, fogAmount);

		litColor.a = diffuseAlbedo.a;

		if (diffuseAlbedo.a == 0.0f)
		{
			discard;
		}
	return litColor;*/

	float4 cColor = float4(0.0f, 1.0f, 1.0f, 1.0f);

	return(cColor);
}

struct VS_LIGHTING_INPUT
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC    : TEXCOORD;
};

struct VS_LIGHTING_OUTPUT
{
	float4 PosH    : SV_POSITION;
	float3 PosW    : POSITION;
	float2 TexC    : TEXCOORD;
	float4 color : COLOR;
	float3 NormalW : NORMAL;
	float4 shadowPosition : TEXCOORD3;
};

VS_LIGHTING_OUTPUT VSLighting(VS_LIGHTING_INPUT vin)
{
	VS_LIGHTING_OUTPUT vout = (VS_LIGHTING_OUTPUT)0.0f;

	float4 posW = mul(float4(vin.PosL, 1.0f), gChaWorld);
	vout.PosW = posW.xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)gChaWorld);
	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);
	vout.TexC = texC.xy;
	vout.PosH = mul(mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxView), gmtxProjection);

	/*
	matrix shadowProject = mul(mul(gChaWorld, gmtxShadowTransform), gChaTexTransform);
	vout.shadowPosition = mul(float4(vin.PosL, 1.0f), shadowProject);

	vout.shadowPosition.xyz /= vout.shadowPosition.w;
	float fShadowFactor = 0.3f, fBias = 0.006f;
	float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, vout.shadowPosition.xy).r;
	//if (vout.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;
	vout.color = Lighting(vout.PosW, normalize(vout.NormalW), fShadowFactor);
	*/
	vout.shadowPosition = mul(posW, gmtxShadowTransform);
	return(vout);
}

float4 PSLighting(VS_LIGHTING_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
	return(input.color);
#else
	//	float3 normalW = normalize(input.normalW);
	//	float3 cNormal = normalW * 0.5f + 0.5f;
	//	float4 color = float4(cNormal, 0.0f);
		float3 normalW = normalize(input.NormalW);
		/*
		input.shadowPosition.xyz /= input.shadowPosition.w;
		float fShadowFactor = 1.0f, fBias = 0.006f;
		float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, input.shadowPosition.xy).r;
		if (input.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;
		*/

		// Only the first light casts a shadow.
		float3 shadowFactor = float3(0.0f, 0.0f, 0.0f);
		shadowFactor = CalcShadowFactor(input.shadowPosition);

		float4 color = Lighting(input.PosW, normalW, shadowFactor);
		return(color);
	#endif
}