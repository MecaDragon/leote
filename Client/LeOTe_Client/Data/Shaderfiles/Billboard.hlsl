
#include "Light.hlsl"


struct VS_BILLBOARD_INPUT
{
	float3 center : POSITION;
	float2 size : TEXCOORD;
	uint index : TEXTURE;
};

VS_BILLBOARD_INPUT VSBillboard(VS_BILLBOARD_INPUT input)
{
	return(input);
}


struct GS_BILLBOARD_GEOMETRY_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
};
//326 651
//66, 1     129, 64

static float2 pf2UVs[4] = { float2(0.0f,1.0f), float2(0.0f,0.0f), float2(1.0f,1.0f), float2(1.0f,0.0f) };
//static float2 pf2UVs2[4] = { float2(0.204f,0.09831f), float2(0.204f,0.0f), float2(0.3957f,0.09831f), float2(0.3957f,0.0f) };

[maxvertexcount(4)]
void GSBillboard(point VS_BILLBOARD_INPUT input[1], inout TriangleStream<GS_BILLBOARD_GEOMETRY_OUTPUT> outStream)
{
	float3 f3Up = float3(0.0f, 1.0f, 0.0f);
	float3 f3Look = normalize(gf3CameraPosition - gPositionW.xyz);
	float3 f3Right = cross(f3Up, f3Look);
	float fHalfWidth = gSize.x * 0.5f;
	float fHalfHeight = gSize.y * 0.5f;

	float4 pf4Vertices[4];

	pf4Vertices[0] = float4(gPositionW.xyz + (fHalfWidth * f3Right) - (fHalfHeight * f3Up), 1.0f);
	pf4Vertices[1] = float4(gPositionW.xyz + (fHalfWidth * f3Right) + (fHalfHeight * f3Up), 1.0f);
	pf4Vertices[2] = float4(gPositionW.xyz - (fHalfWidth * f3Right) - (fHalfHeight * f3Up), 1.0f);
	pf4Vertices[3] = float4(gPositionW.xyz - (fHalfWidth * f3Right) + (fHalfHeight * f3Up), 1.0f);
	
	static float2 pf2UVs2[4] = {
							float2(gStartPos.x				, gStartPos.y + gClipSize.y),
							float2(gStartPos.x				, gStartPos.y),
							float2(gStartPos.x + gClipSize.x, gStartPos.y + gClipSize.y),
							float2(gStartPos.x + gClipSize.x, gStartPos.y)
	};
	
	GS_BILLBOARD_GEOMETRY_OUTPUT output;
	for (int i = 0; i < 4; i++)
	{
		output.positionW = pf4Vertices[i].xyz;
		output.position = mul(mul(pf4Vertices[i], gmtxView), gmtxProjection);
		output.normal = f3Look;
		output.uv = pf2UVs2[i] + float2((float)(gClipSize.x * gIndexX), (float)(gClipSize.y * gIndexY));
		outStream.Append(output);
	}
}

float4 PSBillboard(GS_BILLBOARD_GEOMETRY_OUTPUT input) : SV_TARGET
{
	float4 cColor = gtxtAlbedoTexture.Sample(gsamLinearClamp, input.uv);
	if (cColor.a <= 0.3f) discard; //clip(cColor.a - 0.3f);

	return(cColor);
}