#include "Light.hlsl"

struct VertexIn
{
	float3 PosL    : POSITION;
	float2 TexC    : TEXCOORD;
};

struct VertexOut
{
	float4 PosH    : SV_POSITION;
	float2 TexC    : TEXCOORD;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;

	MATERIAL matData = gChaMaterial;


	// Transform to homogeneous clip space.
	vout.PosH = mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxViewProjection);

	// Output vertex attributes for interpolation across triangle.
	/*float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);
	vout.TexC = mul(texC, matData.MatTransform).xy;*/

	vout.TexC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform).xy;

	return vout;
}

void PS(VertexOut pin)
{
	// Fetch the material data.
	MATERIAL matData = gChaMaterial;
	float4 diffuseAlbedo = matData.m_f4Diffuse;
	//uint diffuseMapIndex = matData.DiffuseMapIndex;

	// Dynamically look up the texture in the array.
	diffuseAlbedo *= gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, pin.TexC);

#ifdef ALPHA_TEST
	clip(diffuseAlbedo.a - 0.1f);
#endif
}

struct AnimVertexIn
{
	float3 PosL    : POSITION;
	float2 TexC    : TEXCOORD;
	float3 BoneWeights : WEIGHTS;
	uint4 BoneIndices  : BONEINDICES;
};

struct AnimVertexOut
{
	float4 PosH    : SV_POSITION;
	float2 TexC    : TEXCOORD;
};

AnimVertexOut AnimVS(AnimVertexIn vin)
{
	AnimVertexOut vout = (AnimVertexOut)0.0f;

	MATERIAL matData = gChaMaterial;

	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = vin.BoneWeights.x;
	weights[1] = vin.BoneWeights.y;
	weights[2] = vin.BoneWeights.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	float3 posL = float3(0.0f, 0.0f, 0.0f);
	float3 normalL = float3(0.0f, 0.0f, 0.0f);
	float3 binormalL = float3(0.0f, 0.0f, 0.0f);
	float3 tangentL = float3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < 4; ++i)
	{
		posL += weights[i] * mul(float4(vin.PosL, 1.0f), gBoneTransforms[vin.BoneIndices[i]]).xyz;
	}

	vin.PosL = posL;
	// Transform to homogeneous clip space.
	vout.PosH = mul(mul(float4(vin.PosL, 1.0f), gChaWorld), gmtxViewProjection);

	// Output vertex attributes for interpolation across triangle.
	/*float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform);
	vout.TexC = mul(texC, matData.MatTransform).xy;*/

	vout.TexC = mul(float4(vin.TexC, 0.0f, 1.0f), gChaTexTransform).xy;

	return vout;
}

void AnimPS(AnimVertexOut pin)
{
	// Fetch the material data.
	MATERIAL matData = gChaMaterial;
	float4 diffuseAlbedo = matData.m_f4Diffuse;
	//uint diffuseMapIndex = matData.DiffuseMapIndex;

	// Dynamically look up the texture in the array.
	diffuseAlbedo *= gtxtAlbedoTexture.Sample(gsamAnisotropicWrap, pin.TexC);

#ifdef ALPHA_TEST
	clip(diffuseAlbedo.a - 0.1f);
#endif
}
