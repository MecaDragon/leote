#define MAX_LIGHTS			8 
#define MAX_MATERIALS		8 

#define POINT_LIGHT			1
#define SPOT_LIGHT			2
#define DIRECTIONAL_LIGHT	3

#define _WITH_LOCAL_VIEWER_HIGHLIGHTING
#define _WITH_THETA_PHI_CONES
//#define _WITH_REFLECT

struct MATERIAL
{
	float4				m_f4Ambient;
	float4				m_f4Diffuse;
	float4				m_f4Specular; //a = power
	float4				m_f4Emissive;
};

struct LIGHT
{
	float4				m_f4Ambient;
	float4				m_f4Diffuse;
	float4				m_f4Specular;
	float3				m_f3Position;
	float 				m_fFalloff;
	float3				m_f3Direction;
	float 				m_fTheta; //cos(m_fTheta)
	float3				m_f3Attenuation;
	float				m_fPhi; //cos(m_fPhi)
	bool				m_bEnable;
	int 				m_nType;
	float				m_fRange;
	float				padding;
};

cbuffer cbCameraInfo : register(b0)
{
	matrix					gmtxView : packoffset(c0);
	matrix					gmtxProjection : packoffset(c4);
	float3					gf3CameraPosition : packoffset(c8);
};

cbuffer cbGameObjectInfo : register(b1)
{
	matrix gChaWorld;
	matrix gChaTexTransform;
	MATERIAL gChaMaterial;
	matrix gBoneTransforms[96];
};

cbuffer cbShadow : register(b2)
{
	matrix gmtxViewProjection : packoffset(c0);
	matrix gmtxShadowTransform : packoffset(c4);
};

cbuffer cbLights : register(b3)
{
	LIGHT				gLights[MAX_LIGHTS];
	float4				gcGlobalAmbientLight;
};

cbuffer cbBillBoard : register(b4)
{
	float4				gPositionW;
	float2				gSize;
	float2				gClipSize;
	float2				gStartPos;
	int					gIndexX;
	int					gIndexY;
};

Texture2D gtxtAlbedoTexture : register(t6);
Texture2D gtxtSpecularTexture : register(t7);
Texture2D gtxtNormalTexture : register(t8);
Texture2D gtxtMetallicTexture : register(t9);
Texture2D gtxtEmissionTexture : register(t10);
Texture2D gtxtDetailAlbedoTexture : register(t11);
Texture2D gtxtDetailNormalTexture : register(t12);
Texture2D gtxtSkyBoxTexture : register(t13);

Texture2D gtxtFontTexture : register(t15);
Texture2D gShadowMap : register(t16);

SamplerState gsamPointWrap        : register(s0);
SamplerState gsamPointClamp       : register(s1);
SamplerState gsamLinearWrap       : register(s2);
SamplerState gsamLinearClamp      : register(s3);
SamplerState gsamAnisotropicWrap  : register(s4);
SamplerState gsamAnisotropicClamp : register(s5);
SamplerState gsamFontWrap : register(s6);
SamplerComparisonState gsamShadow : register(s7);

float CalcShadowFactor(float4 shadowPosH)
{
	// Complete projection by doing division by w.
	shadowPosH.xyz /= shadowPosH.w;

	// Depth in NDC space.
	float depth = shadowPosH.z;

	uint width, height, numMips;
	gShadowMap.GetDimensions(0, width, height, numMips);

	// Texel size.
	float dx = 1.0f / (float)width;

	float percentLit = 0.0f;
	const float2 offsets[9] =
	{
		float2(-dx,  -dx), float2(0.0f,  -dx), float2(dx,  -dx),
		float2(-dx, 0.0f), float2(0.0f, 0.0f), float2(dx, 0.0f),
		float2(-dx,  +dx), float2(0.0f,  +dx), float2(dx,  +dx)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		percentLit += gShadowMap.SampleCmpLevelZero(gsamShadow,
			shadowPosH.xy + offsets[i], depth).r;
	}

	return percentLit / 9.0f + 0.3f;
}