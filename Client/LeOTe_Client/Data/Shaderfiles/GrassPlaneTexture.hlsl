
#include "Light.hlsl"

struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	//vector		vColor : COLOR0;
};

VS_TEXTURED_OUTPUT VSGrassPlane(VS_TEXTURED_INPUT input)
{
	VS_TEXTURED_OUTPUT output;

	//output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxGameObject), gmtxView), gmtxProjection);
	output.position = mul(mul(mul(float4(input.position, 1.0f), gChaWorld), gmtxView), gmtxProjection);
	output.uv = input.uv;

	return(output);
}

float4 PSGrassPlane(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
	float3 uvw = float3(input.uv, 0);
	uvw.x *= 5.f;
	uvw.y *= 7.f;

	float4 cColor = gtxtAlbedoTexture.Sample(gsamLinearWrap, uvw);
	//cColor.a = input.uv.y;
	if (cColor.a <= 0.3f) discard;
	return(cColor);
}



//#define _WITH_VERTEX_LIGHTING

struct VS_LIGHTING_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
};

struct VS_LIGHTING_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	//	nointerpolation float3 normalW : NORMAL;
#ifdef _WITH_VERTEX_LIGHTING
	float4 color : COLOR;
#else
	float3 normalW : NORMAL;
#endif
	float4 shadowPosition : TEXCOORD3;
};

VS_LIGHTING_OUTPUT VSLighting(VS_LIGHTING_INPUT input)
{
	VS_LIGHTING_OUTPUT output;

	output.positionW = (float3)mul(float4(input.position, 1.0f), gChaWorld);
	output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
	float3 normalW = mul(input.normal, (float3x3)gChaWorld);

	/*
	matrix shadowProject = mul(mul(gChaWorld, gmtxShadowTransform), gChaTexTransform);
	output.shadowPosition = mul(float4(input.position, 1.0f), shadowProject);
	*/
	output.shadowPosition = mul(input.position, gmtxShadowTransform);
#ifdef _WITH_VERTEX_LIGHTING
	/*
	output.shadowPosition.xyz /= vout.shadowPosition.w;
	float fShadowFactor = 0.3f, fBias = 0.006f;
	float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, output.shadowPosition.xy).r;
	if (output.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;

	output.color = Lighting(output.positionW, normalize(normalW), fShadowFactor);
	*/
#else
	output.normalW = normalW;
#endif
	return(output);
}

float4 PSLighting(VS_LIGHTING_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
	return(input.color);
#else
	//	float3 normalW = normalize(input.normalW);
	//	float3 cNormal = normalW * 0.5f + 0.5f;
	//	float4 color = float4(cNormal, 0.0f);
		float3 normalW = normalize(input.normalW);

		/*
		input.shadowPosition.xyz /= input.shadowPosition.w;
		float fShadowFactor = 0.3f, fBias = 0.006f;
		float fsDepth = gtxtShadowMap.Sample(gsamAnisotropicWrap, input.shadowPosition.xy).r;
		if (input.shadowPosition.z <= (fsDepth + fBias)) fShadowFactor = 1.0f;
		*/

		float3 shadowFactor = float3(1.0f, 1.0f, 1.0f);
		shadowFactor[0] = CalcShadowFactor(input.shadowPosition);
		float4 color = Lighting(input.positionW, normalW, shadowFactor[0]);
		return(color);
	#endif
}